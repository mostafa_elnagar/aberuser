package grand.app.aber_user.model;

import com.google.gson.annotations.SerializedName;

public class PaginateMain {
    @SerializedName("meta")
    private Meta meta;

    @SerializedName("links")
    private Links links;

    public Meta getMeta(){
        return meta;
    }

    public Links getLinks(){
        return links;
    }
}
