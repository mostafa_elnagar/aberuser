
package grand.app.aber_user.model.base;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StatusMessage {

    @SerializedName("message")
    @Expose
    public String mMessage;
    @SerializedName("code")
    @Expose
    public int mStatus;

    public StatusMessage(String mMessage, int mStatus) {
        this.mMessage = mMessage;
        this.mStatus = mStatus;
    }

    public StatusMessage() {
    }
}
