package grand.app.aber_user.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DropDownsObject {
    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String name;
    @SerializedName("price")
    private String price;
    @SerializedName("currency")
    private String currency;
    @SerializedName("values")
    private List<DropDownsObject> valuesList;
    private boolean checked;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPrice() {
        return price;
    }

    public String getCurrency() {
        return currency;
    }

    public List<DropDownsObject> getValuesList() {
        return valuesList;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }
}
