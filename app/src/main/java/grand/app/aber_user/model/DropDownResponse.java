package grand.app.aber_user.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import grand.app.aber_user.model.base.StatusMessage;

public class DropDownResponse extends StatusMessage {
    @SerializedName("data")
    private List<DropDownsObject> dropDownsObjects;

    public List<DropDownsObject> getDropDownsObjects() {
        return dropDownsObjects;
    }
}
