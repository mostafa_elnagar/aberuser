package grand.app.aber_user;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;

import java.util.ArrayList;
import java.util.List;

import grand.app.aber_user.customViews.views.CustomTextViewMedium;
import grand.app.aber_user.model.base.StatusMessage;

public class BaseAdapterAuto extends BaseAdapter implements Filterable {
    public List<StatusMessage> statusMessageList, statusMessageListFiltered;
    Context context;

    public BaseAdapterAuto(List<StatusMessage> statusMessageList) {
        this.statusMessageList = statusMessageList;
        this.statusMessageListFiltered = this.statusMessageList;
    }

    @Override
    public int getCount() {
        return statusMessageListFiltered != null ? statusMessageListFiltered.size() : statusMessageList.size();
    }

    @Override
    public StatusMessage getItem(int position) {
        return statusMessageListFiltered.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        this.context = parent.getContext();
        // inflate the layout for each list row
        if (convertView == null) {
            convertView = LayoutInflater.from(context).
                    inflate(R.layout.item_drop, parent, false);
        }

        // get current item to be displayed
        StatusMessage currentItem = getItem(position);

        // get the TextView for item name and item description
        CustomTextViewMedium textViewItemName = convertView.findViewById(R.id.tv_item);

        //sets the text for item name and item description from the current item object
        textViewItemName.setText(currentItem.mMessage);

        // returns the view for the current row
        return convertView;
    }

    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    Filter nameFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {
            return ((StatusMessage) resultValue).mMessage;
        }

        @Override
        protected Filter.FilterResults performFiltering(CharSequence charSequence) {
            String charString = charSequence.toString();
            if (charString.isEmpty()) {
                statusMessageListFiltered = statusMessageList;
            } else {
                List<StatusMessage> filteredList = new ArrayList<>();
                for (StatusMessage row : statusMessageList) {
                    if (row.mMessage.toLowerCase().contains(charString.toLowerCase())) {
                        filteredList.add(row);
                    }
                }

                statusMessageListFiltered = filteredList;
            }
            Filter.FilterResults filterResults = new Filter.FilterResults();
            filterResults.values = statusMessageListFiltered;
            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence charSequence, Filter.FilterResults
                filterResults) {
            statusMessageListFiltered = (ArrayList<StatusMessage>) filterResults.values;
            notifyDataSetChanged();
        }
    };
}
