package grand.app.aber_user.base;

import android.graphics.Color;
import android.text.TextUtils;
import android.widget.ImageView;
import android.widget.RatingBar;

import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import grand.app.aber_user.R;
import grand.app.aber_user.utils.helper.AppHelper;
import grand.app.aber_user.utils.images.PhotoFullPopupWindow;

public class ApplicationBinding {
    private static final String TAG = "ApplicationBinding";

    @BindingAdapter("imageUrl")
    public static void loadImage(ImageView imageView, Object image) {
        if (image instanceof String) {
            Glide
                    .with(imageView.getContext())
                    .load((String) image)
                    .placeholder(R.drawable.splash)
                    .into(imageView);
        } else if (image instanceof Integer) {
            imageView.setImageResource((Integer) image);
        }
    }

    @BindingAdapter("imageMarketUrl")
    public static void loadMarketImage(ImageView imageView, Object image) {
        if (image instanceof String) {
            Picasso.get().load((String) image).placeholder(R.drawable.splash).into(imageView);
        }
    }

    @BindingAdapter("imgColor")
    public static void loadColorImage(ImageView imageView, Object image) {
        if (image instanceof String) {
            try {
                imageView.setColorFilter(Color.parseColor((String) image));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @BindingAdapter("imageCommentUrl")
    public static void loadCommentImage(ImageView imageView, Object image) {
        if (image instanceof String) {
            Glide.with(imageView.getContext()).load((String) image).placeholder(R.drawable.splash).into(imageView);
            imageView.setOnClickListener(v -> new PhotoFullPopupWindow(imageView.getContext(), R.layout.popup_photo_full, imageView, (String) image, null));
        }
    }
    @BindingAdapter("splash")
    public static void splash(ImageView imageView,int drawable) {
        Glide.with(imageView.getContext()).load(R.raw.splash).into(imageView);
    }
    @BindingAdapter("color")
    public static void color(ImageView imageView, String color) {
        if (color != null && !color.equals("") && color.charAt(0) == '#') {
            imageView.setBackgroundColor(Color.parseColor(color));
        }
    }

//    @BindingAdapter("android:drawableStart")
//    public static void drawableStart(TextView view, int drawable) {
//        view.setCompoundDrawables(null, null, drawable, null);
//    }

    @BindingAdapter({"app:adapter", "app:span", "app:orientation"})
    public static void getItemsV2Binding(RecyclerView recyclerView, RecyclerView.Adapter<?> itemsAdapter, String spanCount, String orientation) {
        if (recyclerView.getAdapter() != null && recyclerView.getAdapter().getItemCount() > 0)
            return;
        if (orientation.equals("1"))
            AppHelper.initVerticalRV(recyclerView, recyclerView.getContext(), Integer.parseInt(spanCount));
        else
            AppHelper.initHorizontalRV(recyclerView, recyclerView.getContext(), Integer.parseInt(spanCount));
        recyclerView.setItemAnimator(null);
        recyclerView.setAdapter(itemsAdapter);
    }

    @BindingAdapter({"app:adapter"})
    public static void getItemsBinding(RecyclerView recyclerView, RecyclerView.Adapter<?> itemsAdapter) {
        GridLayoutManager layoutManager = new GridLayoutManager(recyclerView.getContext(), 2);
        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if (itemsAdapter.getItemCount() - 1 == position) {
                    return 2;
                } else {
                    return 1;
                }
            }
        });
        recyclerView.setItemAnimator(null);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(itemsAdapter);
    }

    @BindingAdapter("rate")
    public static void setRate(final RatingBar ratingBar, String rate) {
        if (!TextUtils.isEmpty(rate))
            ratingBar.setRating(Float.parseFloat(rate));
    }

}
