package grand.app.aber_user.base;

import javax.inject.Singleton;

import dagger.Component;
import grand.app.aber_user.activity.BaseActivity;
import grand.app.aber_user.activity.MainActivity;
import grand.app.aber_user.connection.ConnectionModule;
import grand.app.aber_user.pages.appWallet.AppWalletFragment;
import grand.app.aber_user.pages.auth.changePassword.ChangePasswordFragment;
import grand.app.aber_user.pages.auth.confirmCode.ConfirmCodeFragment;
import grand.app.aber_user.pages.auth.countries.CountriesFragment;
import grand.app.aber_user.pages.auth.forgetPassword.ForgetPasswordFragment;
import grand.app.aber_user.pages.auth.login.LoginFragment;
import grand.app.aber_user.pages.auth.register.RegisterFragment;
import grand.app.aber_user.pages.cart.CartFragment;
import grand.app.aber_user.pages.cart.ConfirmCartFragment;
import grand.app.aber_user.pages.cart.PaymentFragment;
import grand.app.aber_user.pages.chat.view.ChatFragment;
import grand.app.aber_user.pages.conversations.ConversationsFragment;
import grand.app.aber_user.pages.myLocations.AddPlaceFragment;
import grand.app.aber_user.pages.myLocations.MyLocationsFragment;
import grand.app.aber_user.pages.myOrders.FollowUpOrderFragment;
import grand.app.aber_user.pages.myOrders.MyOrderDetailsFragment;
import grand.app.aber_user.pages.myOrders.MyOrdersFragment;
import grand.app.aber_user.pages.myOrders.MyServicesOrdersFragment;
import grand.app.aber_user.pages.parts.FilterFragment;
import grand.app.aber_user.pages.parts.PartServicesFragment;
import grand.app.aber_user.pages.reviews.ReviewsFragment;
import grand.app.aber_user.pages.services.AberBoxFragment;
import grand.app.aber_user.pages.services.BatteriesFragment;
import grand.app.aber_user.pages.services.CarCheckFragment;
import grand.app.aber_user.pages.services.CarWashFragment;
import grand.app.aber_user.pages.services.ChooseServiceTimeFragment;
import grand.app.aber_user.pages.services.FragmentConfirmOrder;
import grand.app.aber_user.pages.services.FuelFragment;
import grand.app.aber_user.pages.services.HiddenFragment;
import grand.app.aber_user.pages.services.OilsFragment;
import grand.app.aber_user.pages.services.OpenCarFragment;
import grand.app.aber_user.pages.services.TiersFragment;
import grand.app.aber_user.pages.services.WaterBallonFragment;
import grand.app.aber_user.pages.services.WinchFragment;
import grand.app.aber_user.pages.home.SearchFragment;
import grand.app.aber_user.pages.parts.PartsFragment;
import grand.app.aber_user.pages.favorites.FavoritesFragment;
import grand.app.aber_user.pages.parts.ProductDetailsFragment;
import grand.app.aber_user.pages.home.HomeFragment;
import grand.app.aber_user.pages.notifications.NotificationsFragment;
import grand.app.aber_user.pages.onBoard.OnBoardFragment;
import grand.app.aber_user.pages.profile.EditProfileFragment;
import grand.app.aber_user.pages.settings.AboutAppFragment;
import grand.app.aber_user.pages.settings.ContactUsFragment;
import grand.app.aber_user.pages.settings.LangFragment;
import grand.app.aber_user.pages.settings.SocialMediaFragment;
import grand.app.aber_user.pages.settings.MyAccountSettingsFragment;
import grand.app.aber_user.pages.settings.TermsFragment;
import grand.app.aber_user.pages.splash.SplashFragment;
import grand.app.aber_user.pages.myOrders.MyServiceOrderDetailsFragment;
import grand.app.aber_user.utils.locations.MapAddressActivity;

//Component refer to an interface or waiter for make an coffee cup to me
@Singleton
@Component(modules = {ConnectionModule.class, LiveData.class})
public interface IApplicationComponent {
    void inject(MainActivity mainActivity);

    void inject(BaseActivity tmpActivity);

    void inject(MapAddressActivity addressActivity);

    void inject(SplashFragment splashFragment);

    void inject(OnBoardFragment onBoardFragment);

    void inject(LoginFragment loginFragment);

    void inject(CountriesFragment countriesFragment);

    void inject(ForgetPasswordFragment forgetPasswordFragment);

    void inject(ConfirmCodeFragment confirmCodeFragment);

    void inject(ChangePasswordFragment changePasswordFragment);

    void inject(RegisterFragment registerFragment);

    void inject(HomeFragment normalOrdersFragment);

    void inject(SearchFragment searchFragment);

    void inject(NotificationsFragment notificationsFragment);

    void inject(MyAccountSettingsFragment myAccountSettingsFragment);

    void inject(EditProfileFragment profileFragment);

    void inject(MyServicesOrdersFragment profileFragment);

    void inject(ContactUsFragment contactUsFragment);

    void inject(FragmentConfirmOrder previousOrdersFragment);

    void inject(ProductDetailsFragment sendOfferFragment);

    void inject(LangFragment langFragment);

    void inject(MyServiceOrderDetailsFragment appMoneyFragment);

    void inject(FavoritesFragment newPostFragment);

    void inject(WinchFragment liveFragment);

    void inject(SocialMediaFragment moreFragment);

    void inject(PartsFragment newLiveFragment);

    void inject(ChooseServiceTimeFragment chooseLiveTimeFragment);

    void inject(CarCheckFragment addAnswerFragment);

    void inject(AboutAppFragment aboutAppFragment);

    void inject(TermsFragment termsFragment);

    void inject(PartServicesFragment partsServicesFragment);

    void inject(CarWashFragment carWashFragment);

    void inject(TiersFragment tiersFragment);

    void inject(OilsFragment oilsFragment);

    void inject(HiddenFragment hiddenFragment);

    void inject(FuelFragment fuelFragment);

    void inject(WaterBallonFragment ballonFragment);

    void inject(OpenCarFragment openCarFragment);

    void inject(BatteriesFragment batteriesFragment);

    void inject(AberBoxFragment aberBoxFragment);

    void inject(MyOrdersFragment myOrdersFragment);

    void inject(MyOrderDetailsFragment myOrderDetailsFragment);

    void inject(AddPlaceFragment addPlaceFragment);

    void inject(MyLocationsFragment myLocationsFragment);

    void inject(AppWalletFragment appWalletFragment);

    void inject(ReviewsFragment reviewsFragment);

    void inject(CartFragment cartFragment);

    void inject(ConfirmCartFragment confirmCartFragment);

    void inject(PaymentFragment paymentFragment);

    void inject(FilterFragment filterFragment);

    void inject(FollowUpOrderFragment followUpOrderFragment);

    void inject(ConversationsFragment conversationsFragment);

    void inject(ChatFragment chatFragment);

    @Component.Builder
    interface Builder {
        IApplicationComponent build();
    }
}
