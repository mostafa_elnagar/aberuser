package grand.app.aber_user.utils.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.gson.Gson;

import grand.app.aber_user.pages.conversations.models.ConversationsData;
import grand.app.aber_user.utils.Constants;

public class RealTimeReceiver extends BroadcastReceiver {
    public static MessageReceiverListener messageReceiverListene;
    public static NotificationCounterListener notificationCounterListener;

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent != null) {
            if (intent.getAction().equals("app.te.receiver")) {
                if (intent.getSerializableExtra(Constants.MESSAGE) != null) {
                    String details = String.valueOf(intent.getSerializableExtra(Constants.MESSAGE));
                    Log.e("onReceive", "onReceive: " + details);
                    ConversationsData messagesItem = new Gson().fromJson(details, ConversationsData.class);
                    messageReceiverListene.onMessageChanged(messagesItem);
                }
                if (intent.getSerializableExtra("counter") != null) {
                    int counter = (int) intent.getSerializableExtra("counter");
                    Log.e("onReceive", "onReceive: " + counter);
                    notificationCounterListener.onNotificationsCounter(counter);
                }
            }
        } else
            Log.i("onReceive", "onReceive: null");
    }

    public interface MessageReceiverListener {
        void onMessageChanged(ConversationsData messagesItem);
    }

    public interface NotificationCounterListener {
        void onNotificationsCounter(int count);
    }
}
