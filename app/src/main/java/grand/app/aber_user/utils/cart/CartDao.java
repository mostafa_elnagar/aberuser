package grand.app.aber_user.utils.cart;


import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import grand.app.aber_user.pages.parts.models.ProductsItem;


@Dao
public interface CartDao {
    //Product Operations
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long addProduct(ProductsItem productDetails);

    @Query("select * from productsitem")
    LiveData<List<ProductsItem>> getProducts();

    @Query("select sum(price) from productsitem")
    LiveData<String> getCartTotal();

    @Query("DELETE FROM productsitem WHERE product_room_id=:productId")
    void deleteItem(int productId);

    @Query("DELETE FROM productsitem")
    void emptyProductCart();

    @Update
    void updateProduct(ProductsItem productDetails);

    @Query("UPDATE productsitem SET quantity=quantity+:quantity where product_room_id=:productId")
    void updateProductQuantity(int quantity, int productId);

    @Query("SELECT EXISTS (SELECT * FROM productsitem WHERE product_room_id=:productId)")
    int getIfMealExists(int productId);

}
