package grand.app.aber_user.utils;

public class Constants {

    public static final String FILTER = "FILTER";
    public static final String CANCEL_SERVICE = "FOLLOWERS";
    public static final String WALLET = "WALLET";
    public static final String RAISE_WALLET = "RAISE_WALLET";
    public final static String CALL = "CALL";
    public static final String REVIEWS = "REVIEWS";
    public static final String COUNTRIES = "COUNTRIES";

    public final static String GOOGLE_SIGN_IN = "GOOGLE_SIGN_IN";
    public final static String FACE_BOOK = "FACE_BOOK";
    public final static String CONFIRM_CODE = "CONFIRM_CODE";
    public final static String NOT_VERIFIED = "NOT_VERIFIED";
    public final static String DELETE = "DELETE";

    public final static String ADD_PLACE = "ADD_PLACE";
    public final static String EMAIL = "email";
    public final static String FIELD = "FIELD";
    public final static String NOT_MATCH_PASSWORD = "NOT_MATCH_PASSWORD";
    public static final String ORDER = "ORDER";
    public static final int ORDER_REQUEST = 123;
    public static final String TYRE_DESC = "TYRE_DESC";

    public final static String UPDATE_PROFILE = "update_profile";
    public static final int RC_SIGN_IN = 175;


    public final static String IMAGE = "image";
    public final static String REVIEW_NEW = "REVIEW_NEW";
    public final static String HOME = "HOME";
    public final static String CONVERSATIONS = "CONVERSATIONS";

    public final static String LOGOUT = "logout";
    public final static String LOGOUT_API = "LOGOUT_API";
    public final static String CART = "CART";

    public final static String SHOW_LOGOUT_WARNING = "SHOW_LOGOUT_WARNING";
    public final static String PRODUCTS = "PRODUCTS";
    public final static String NEW_RATE = "NEW_RATE";
    public final static int LOCATION_REQUEST = 6001;
    public final static int FROM_LOCATION_REQUEST = 6002;
    public final static String ERROR_TOAST = "error_toast";

    public final static String ERROR = "error";
    public final static String BACK = "BACK";
    public final static String SHOW_PROGRESS = "showProgress";
    public final static String HIDE_PROGRESS = "hideProgress";
    public final static String SERVER_ERROR = "serverError";
    public final static String ERROR_NOT_FOUND = "not_found";
    public final static String RESPONSE_URL_CHANGED = "changed";
    public final static String TYRE_TYPE = "TYRE_TYPE";
    public final static String FAILURE_CONNECTION = "failure_connection";
    public final static String SERVICE_DETAILS = "ORDER_DETAILS";
    public final static String EDIT = "EDIT";
    public final static String CAR_MODEL = "CAR_MODEL";

    public final static String LANGUAGE = "language";
    public static final String DEFAULT_LANGUAGE = "ar";
    public static final String MENU_ACCOUNT = "MENU_ACCOUNT";
    public static final String MENU_LIVE = "MENU_LIVE";
    public static final String PAGE = "page";
    public final static String NAME_BAR = "name_bar";
    public static final String BUNDLE = "bundle";
    public static final String CAR_CAT = "CAR_CAT";
    public static final String CAR_TYPE = "CAR_TYPE";
    public static final String CHILD_VEHICLE = "CHILD_VEHICLE";
    public static final String SHARE_POST = "SHARE_POST";
    public static final String TYRE_DESC_DATA = "TYRE_DESC_DATA";
    public static final String TYRE_BY_MODEL = "TYRE_BY_MODEL";
    public static final String SERVICE_ORDER = "SERVICE_ORDER";
    public static final String GET_USER_DOCUMENTS = "GET_USER_DOCUMENTS";
    public static final String SHARE_BAR = "SHARE_BAR";
    public static final String PROFILE = "PROFILE";
    public static boolean DATA_CHANGED = false;


    //REQUESTS
    public final static int POST_REQUEST = 200;
    public final static int GET_REQUEST = 201;
    public final static int DELETE_REQUEST = 202;

    //RESPONSES
    public static final int RESPONSE_SUCCESS = 200;
    public static final int RESPONSE_JWT_EXPIRE = 403;
    public static final int RESPONSE_404 = 404;
    public static final int RESPONSE_CHANGE = 300;
    public final static int AUTOCOMPLETE_REQUEST_CODE = 203;
    public final static int FILE_TYPE_IMAGE = 378;
    public final static int FILE_TYPE_VIDEO = 379;
    public final static int FILE_TYPE_FRONT_OWNER = 381;
    public final static int FILE_TYPE_BACk_OWNER = 382;
    public final static int CHECK_CONFIRM_NAV_REGISTER = 0;
    public final static int CHECK_CONFIRM_NAV_FORGET = 1;

    public static final String front_car_image = "front_car_image";
    public static final String front_car_ownership = "front_car_ownership";
    public static final String back_car_ownership = "back_car_ownership";
    public static final int DELIVERY_TIME_REQUEST = 380;

    public static final String back_car_image = "back_car_image";
    public static final String COMPANY = "COMPANY";

    public static final String MY_LOCATIONS = "MY_LOCATIONS";
    public static final String LIKES_REACTION = "like";
    public static final String CITIES = "CITIES";
    public static final String SERVICES_ORDERS = "SERVICES_ORDERS";

    public static final String license_image = "license_image";

    public static final String GOVERN = "GOVERN";
    public static final String CHECK_PROMO = "CHECK_PROMO";
    public static final String GOVERNS = "GOVERNS";

    public static final String SEARCH_LOCATION = "SEARCH_LOCATION";
    public static final String FROM_SEARCH_LOCATION = "FROM_SEARCH_LOCATION";
    public static final String ABOUT = "about";
    public static final String TERMS = "terms";
    public static final String PRIVACY = "PRIVACY";
    public static final String SUGGESTION = "SUGGESTION";
    public static final String FOLLOW_ORDER = "FOLLOW_ORDER";
    public static final String LOGIN = "login";
    public static final String LAT = "LAT";
    public static final String LNG = "LNG";
    public static final String DIALOG_CLOSE = "DIALOG_CLOSE";
    public static final String FORGET_PASSWORD = "forget_password";
    public static final String DIALOG_SHOW = "DIALOG_SHOW";
    public static final String SIZE = "SIZE";
    public static final String SOCIAL = "SOCIAL";
    public static final String PAYMENT = "PAYMENT";
    public static final int CUSTOM_REQUEST_CODE = 532;


    public static final String REGISTER = "register";
    public static final String DELIVERY_TIME = "LIVE_TIME";
    public static final String SEARCH = "SEARCH";
    public static final int RESPONSE_405 = 405;


    public static final String PRICES_LIST = "PRICES_LIST";

    public static final String SHOW_COUNTRY = "SHOW_COUNTRY";
    public static final String RATE_APP = "RATE_APP";
    public static final String OIL = "OIL";
    public static final String OIL_LIQUID = "OIL_LIQUID";
    public static final String KM = "KM";
    public static final String OIL_LIQUID_REQUEST = "OIL_LIQUID_REQUEST";
    public static final String CONTACT = "CONTACT";
    public static final String GET_CONTACT = "GET_CONTACT";
    public static final String HIDDEN_TYPE_DROP = "HIDDEN_TYPE_DROP";

    public static final String HIDDEN_COLOR_DROP = "HIDDEN_COLOR_DROP";
    public static final String CITY = "CITY";
    public static final String ADDRESS = "ADDRESS";
    public static final String NOTIFICATIONS = "notifications";
    public static final String PICKED_SUCCESSFULLY = "PICKED_SUCCESSFULLY";

    public static final String CHANGE_PASSWORD = "forget-password";
    public static final String SERVICES = "services";
    public static final String HIDDEN_PERCENTAGE_DROP = "HIDDEN_PERCENTAGE_DROP";

    public static final String HIDDEN_COLOR_REQUEST = "HIDDEN_COLOR_REQUEST";
    public static final String HIDDEN_PERCENTAGE_REQUEST = "HIDDEN_PERCENTAGE_REQUEST";
    public static final String BATTERY_SIZE = "BATTERY_SIZE";
    public static final String BATTERY_SIZE_REQUEST = "BATTERY_SIZE_REQUEST";
    public static final String MENU_HOME = "menu_home";
    public static final String FAVORITE = "FAVORITE";
    public static final String PROVIDER = "PROVIDER";
    public static final String MENU_FOLLOWERS = "menu_categories";
    public static final String NEXT = "next";
    public static final String PREVIOUS = "PREVIOUS";
    public static final String START_APP = "START_APP";
    public static final String BOARD = "board";
    public static final String MENu = "menu";
    public static final String BATTERY_TYPE = "BATTERY_TYPE";
    // NOTIFICATIONS
    public static final String TYPE = "type";
    public static final String SERVICE_ID = "order_service_id";
    public static final String ORDER_SERVICE = "order_service";
    public static final String ORDERS = "order";
    //NOTIFICATIONS TYPES
    public static final String My_ORDERS = "My_ORDERS";
    public static final String My_ORDER_DETAILS = "My_ORDER_DETAILS";
    public static final String LITER = "LITER";
    public static final String GALLON = "GALLON";
    public static final String TINKER_SERVICE_TYPE = "TINKER_SERVICE_TYPE";
    public static final String FUEL_TYPE = "FUEL_TYPE";
    public static final String FUEL_CAT = "FUEL_CAT";
    public static final String FUEL_CAT_REQUEST = "FUEL_CAT_REQUEST";
    public static final String SORT_BY = "SORT_BY";
    public static final String FILTER_RESULT = "FILTER_RESULT";
    public static final String SEND_MESSAGE = "SEND_MESSAGE";
    public static final String CHAT = "CHAT";
    public static final String LOGIN_WARNING = "LOGIN_WARNING";
    public static final String GENERATE_CODE = "GENERATE_CODE";
    public static final String RATE_SERVICE_PROVIDER = "RATE_SERVICE_PROVIDER";
    public static final String MESSAGE = "message";
    public static final String BOX_TYPES = "BOX_TYPES";
    public static final String DISTANCE = "DISTANCE";
}

