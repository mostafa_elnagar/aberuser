package grand.app.aber_user.utils.cart;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import grand.app.aber_user.pages.parts.models.ProductsItem;


@Database(entities = {ProductsItem.class}, version = 3)
public abstract class CartDataBase extends RoomDatabase {
    private static CartDataBase instance;

    public abstract CartDao cartDao();

    public static synchronized CartDataBase getInstance(Context context) {
        if (instance == null)
            instance = Room.databaseBuilder(context.getApplicationContext(), CartDataBase.class, "cart_db")
                    .fallbackToDestructiveMigration() // we do it for increase db version
                    .build();
        return instance;
    }
}
