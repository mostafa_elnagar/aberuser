package grand.app.aber_user.utils.locations;

import androidx.databinding.ObservableField;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;

import javax.inject.Inject;

import grand.app.aber_user.base.BaseViewModel;
import grand.app.aber_user.utils.Constants;


public class MapAddressViewModel extends BaseViewModel {
    public ObservableField<LatLng> mMapLatLng;
    public static ObservableField<Double> latitude;
    public static ObservableField<Double> longitude;
    public static ObservableField<String> address;
    public static ObservableField<String> arabicAddress;
    public GoogleMap mMap;

    public void submit() {
        getLiveData().setValue(Constants.PICKED_SUCCESSFULLY);
    }

    @Inject
    public MapAddressViewModel() {
        latitude = new ObservableField<>();
        longitude = new ObservableField<>();
        address = new ObservableField<>();
        arabicAddress = new ObservableField<>();
    }

    public void toSearchPlace() {
        getLiveData().setValue(Constants.SEARCH_LOCATION);
    }


    public void reset() {
//        mMapLatLng.set(null);
        latitude.set(null);
        longitude.set(null);
        address.set(null);
    }

    public void back() {
        getLiveData().setValue(Constants.BACK);
    }
}