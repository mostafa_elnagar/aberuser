package grand.app.aber_user.repository;

import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;

import javax.inject.Inject;
import javax.inject.Singleton;

import grand.app.aber_user.base.MyApplication;
import grand.app.aber_user.connection.ConnectionHelper;
import grand.app.aber_user.connection.FileObject;
import grand.app.aber_user.model.DropDownResponse;
import grand.app.aber_user.model.base.Mutable;
import grand.app.aber_user.model.base.StatusMessage;
import grand.app.aber_user.pages.cart.models.CheckPromoRequest;
import grand.app.aber_user.pages.cart.models.CheckPromoResponse;
import grand.app.aber_user.pages.cart.models.NewOrderRequest;
import grand.app.aber_user.pages.favorites.models.FavoritesResponse;
import grand.app.aber_user.pages.home.models.HomeResponse;
import grand.app.aber_user.pages.myOrders.models.MyOrderDetailsResponse;
import grand.app.aber_user.pages.myOrders.models.MyOrdersResponse;
import grand.app.aber_user.pages.myOrders.models.orderServices.generateCode.GenerateCodeRequest;
import grand.app.aber_user.pages.myOrders.models.orderServices.MyOrderServicesResponse;
import grand.app.aber_user.pages.myOrders.models.orderServices.OrderServiceDetailsResponse;
import grand.app.aber_user.pages.myOrders.models.orderServices.generateCode.GenerateCodeResponse;
import grand.app.aber_user.pages.parts.models.ProductsResponse;
import grand.app.aber_user.pages.parts.models.filter.FilterDataResponse;
import grand.app.aber_user.pages.parts.models.filter.FilterRequest;
import grand.app.aber_user.pages.parts.models.productDetails.ProductDetailsResponse;
import grand.app.aber_user.pages.reviews.models.RateRequest;
import grand.app.aber_user.pages.reviews.models.ReviewsResponse;
import grand.app.aber_user.pages.reviews.models.SendReviewResponse;
import grand.app.aber_user.pages.services.models.CreateServiceOrder;
import grand.app.aber_user.pages.services.models.ServiceDetailsResponse;
import grand.app.aber_user.utils.Constants;
import grand.app.aber_user.utils.URLS;
import grand.app.aber_user.utils.session.UserHelper;
import io.reactivex.disposables.Disposable;

@Singleton
public class ServicesRepository extends BaseRepository {

    @Inject
    ConnectionHelper connectionHelper;

    MutableLiveData<Mutable> liveData;

    @Inject
    public ServicesRepository(ConnectionHelper connectionHelper) {
        this.connectionHelper = connectionHelper;
    }

    public void setLiveData(MutableLiveData<Mutable> liveData) {
        this.liveData = liveData;
        connectionHelper.liveData = liveData;
    }

    public Disposable getHome() {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.HOME + UserHelper.getInstance(MyApplication.getInstance()).getCountriesData().getId(), new Object(), HomeResponse.class,
                Constants.HOME, true);
    }

    public Disposable getServiceDetails(int serviceId, int countryId) {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.SERVICE_DETAILS + serviceId + "&country_id=" + countryId, new Object(), ServiceDetailsResponse.class,
                Constants.SERVICE_DETAILS, true);
    }

    public Disposable getCarChildData(int childId) {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.CHILD_VEHICLE + childId, new Object(), DropDownResponse.class,
                Constants.CHILD_VEHICLE, true);
    }

    public Disposable getCarModel(int childId) {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.CHILD_VEHICLE + childId, new Object(), DropDownResponse.class,
                Constants.CAR_MODEL, true);
    }


    public Disposable getTyredData(int childId) {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.TYRE_DESC + childId, new Object(), DropDownResponse.class,
                Constants.TYRE_DESC_DATA, true);
    }

    public Disposable getTyres(int parentId) {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.TYRE_BY_MODEL + parentId, new Object(), DropDownResponse.class,
                Constants.TYRE_BY_MODEL, true);
    }

    public Disposable getOilLiquidData(int childId) {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.OIL_LIQUID + childId, new Object(), DropDownResponse.class,
                Constants.OIL_LIQUID_REQUEST, true);
    }

    public Disposable getHiddenColorData(int childId, String type) {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.HIDDEN_COLOR + childId, new Object(), DropDownResponse.class,
                type, true);
    }

    public Disposable getPartServices(int serviceId, int countryId, int isNew, int page, boolean showProgress) {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.PRODUCTS + serviceId + "&country_id=" + countryId + "&is_new=" + isNew + "&page=" + page, new Object(), ProductsResponse.class,
                Constants.PRODUCTS, showProgress);
    }

    public Disposable sortBy(FilterRequest filterRequest) {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.SORT_BY + filterRequest.getOrder_by() + "&country_id=" + filterRequest.getCountry_id() + "&is_new=" + filterRequest.getIsNew() + "&sub_service_id=" + filterRequest.getSub_service_id(), new Object(), ProductsResponse.class,
                Constants.SORT_BY, true);
    }

    public Disposable getFuelCatData(int childId) {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.FUEL + childId, new Object(), DropDownResponse.class,
                Constants.FUEL_CAT_REQUEST, true);
    }

    public Disposable getBatteryData(int childId) {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.BATTERY_SIZE + childId, new Object(), DropDownResponse.class,
                Constants.BATTERY_SIZE_REQUEST, true);
    }

    public Disposable getCompanyPartServices(int companyId, int countryId, int isNew, int page, boolean showProgress) {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.PRODUCTS_COMPANY + companyId + "/products?country_id=" + countryId + "&is_new=" + isNew + "&page=" + page, new Object(), ProductsResponse.class,
                Constants.PRODUCTS, showProgress);
    }


    public Disposable getProductDetails(int productId) {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.PRODUCT_DETAILS + productId, new Object(), ProductDetailsResponse.class,
                Constants.SERVICE_DETAILS, true);
    }

    public Disposable getClientReviews(int productId, int page, boolean showProgress) {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.CLIENT_REVIEWS + productId + "/rates?page=" + page, new Object(), ReviewsResponse.class,
                Constants.REVIEWS, showProgress);
    }

    public Disposable sendReview(RateRequest rateRequest) {
        return connectionHelper.requestApi(Constants.POST_REQUEST, URLS.RATE_PRODUCT + rateRequest.getOrderId() + "/rate", rateRequest, SendReviewResponse.class,
                Constants.NEW_RATE, true);
    }

    public Disposable changeLike(int productId) {
        return connectionHelper.requestApi(Constants.POST_REQUEST, URLS.CHANGE_LIKE, new RateRequest(String.valueOf(productId)), StatusMessage.class,
                Constants.LIKES_REACTION, false);
    }

    public Disposable checkPromo(CheckPromoRequest checkPromoRequest) {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.CHECK_PROMO + checkPromoRequest.getCode(), new Object(), CheckPromoResponse.class,
                Constants.CHECK_PROMO, true);
    }


    public Disposable orderProduct(NewOrderRequest newOrderRequest) {
        return connectionHelper.requestApi(Constants.POST_REQUEST, URLS.ORDER_PRODUCTS, newOrderRequest, StatusMessage.class,
                Constants.ORDER, false);
    }

    public Disposable myOrders(int page, boolean showProgress) {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.MY_ORDERS + page, new Object(), MyOrdersResponse.class,
                Constants.My_ORDERS, showProgress);
    }

    public Disposable myOrderDetails(int orderId) {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.MY_ORDER_DETAILS + orderId, new Object(), MyOrderDetailsResponse.class,
                Constants.My_ORDER_DETAILS, true);
    }

    public Disposable search(int page, boolean showProgress, String search, String type) {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.SEARCH_PRODUCTS + type + "&search=" + search + "&page=" + page, new Object(), ProductsResponse.class,
                Constants.SEARCH, showProgress);
    }

    public Disposable createService(CreateServiceOrder serviceOrder, ArrayList<FileObject> fileObjectList) {
        if (fileObjectList.size() == 0)
            return connectionHelper.requestApi(Constants.POST_REQUEST, URLS.SERVICE_ORDER, serviceOrder, StatusMessage.class,
                    Constants.SERVICE_ORDER, false);
        else
            return connectionHelper.requestApi(URLS.SERVICE_ORDER, serviceOrder, fileObjectList, StatusMessage.class,
                    Constants.SERVICE_ORDER, false);
    }

    public Disposable myOrderServices(int status, int page, boolean showProgress) {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.GET_SERVICES_ORDERS + status + "&page=" + page, new Object(), MyOrderServicesResponse.class,
                Constants.SERVICES_ORDERS, showProgress);
    }

    public Disposable myOrderServicesByCat(int status, int cat, int page, boolean showProgress) {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.GET_SERVICES_ORDERS + status + "&main_service_id=" + cat + "&page=" + page, new Object(), MyOrderServicesResponse.class,
                Constants.SERVICES_ORDERS, showProgress);
    }

    public Disposable myOrderServiceDetails(int orderId) {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.GET_ORDER_SERVICE_DETAILS + orderId, new Object(), OrderServiceDetailsResponse.class,
                Constants.My_ORDER_DETAILS, true);
    }

    public Disposable cancelOrder(int orderId, int countryId) {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.CANCEL_ORDER_SERVICE + orderId + "?country_id=" + countryId, new Object(), StatusMessage.class,
                Constants.CANCEL_SERVICE, true);
    }

    public Disposable generateCode(int orderId, int providerId) {
        return connectionHelper.requestApi(Constants.POST_REQUEST, URLS.GENERATE_CODE, new GenerateCodeRequest(providerId, orderId), GenerateCodeResponse.class,
                Constants.GENERATE_CODE, true);
    }

    public Disposable getFavorites() {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.FAVORITES, new Object(), FavoritesResponse.class,
                Constants.FAVORITE, true);
    }

    public Disposable getFilterData(int catId, int countryId) {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.FILTER_DATA + countryId + "&sub_service_id=" + catId, new Object(), FilterDataResponse.class,
                Constants.FILTER, true);
    }

    public Disposable filter(FilterRequest filterRequest) {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.FILTER_RESULT + filterRequest.getCountry_id() + "&sub_service_id=" + filterRequest.getSub_service_id() + "&child_service_ids=" + filterRequest.getChild_service_ids()
                        + "&min_price=" + filterRequest.getMin_price() + "&max_price=" + filterRequest.getMax_price() + "&color_id=" + filterRequest.getParent_id() + "&is_new=" + filterRequest.getIsNew(), new Object(), ProductsResponse.class,
                Constants.FILTER_RESULT, true);
    }

    public Disposable rateServiceProvider(RateRequest rateRequest) {
        return connectionHelper.requestApi(Constants.POST_REQUEST, URLS.RATE_SERVICE_PROVIDER, rateRequest, StatusMessage.class,
                Constants.RATE_SERVICE_PROVIDER, false);
    }

}