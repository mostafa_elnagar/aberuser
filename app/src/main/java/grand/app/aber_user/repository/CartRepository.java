package grand.app.aber_user.repository;

import android.app.Application;
import android.os.AsyncTask;
import android.util.Log;

import androidx.lifecycle.LiveData;
import java.util.List;
import grand.app.aber_user.pages.parts.models.ProductsItem;
import grand.app.aber_user.utils.cart.CartDao;
import grand.app.aber_user.utils.cart.CartDataBase;


public class CartRepository {
    CartDao cartDao;
    LiveData<List<ProductsItem>> allProducts;
    LiveData<String> cartTotal;

    public CartRepository(Application application) {
        CartDataBase cartDataBase = CartDataBase.getInstance(application);
        cartDao = cartDataBase.cartDao();
        allProducts = cartDao.getProducts();
        cartTotal = cartDao.getCartTotal();
    }

    public void insert(ProductsItem product) {
        new InsertProductAsyncTask(cartDao).execute(product);
    }

    public void update(ProductsItem product) {
        new UpdateProductAsyncTask(cartDao).execute(product);

    }

    public void deleteItem(int productId) {
        new DeleteProductAsyncTask(cartDao).execute(productId);

    }

    public void emptyCart() {
        new EmptyCartAsyncTask(cartDao).execute();
    }

    public LiveData<List<ProductsItem>> getAllProducts() {
        return allProducts;
    }

    public LiveData<String> getCartTotal() {
        return cartTotal;
    }

    private static final String TAG = "CartRepository";

    private static class InsertProductAsyncTask extends AsyncTask<ProductsItem, Void, Void> {
        CartDao cartDao;

        public InsertProductAsyncTask(CartDao cartDao) {
            this.cartDao = cartDao;
        }

        @Override
        protected Void doInBackground(ProductsItem... products) {
            int exist = cartDao.getIfMealExists(products[0].getProduct_room_id());
            if (exist == 1)
                cartDao.updateProductQuantity(products[0].getQuantity(), products[0].getProduct_room_id());
            else
                cartDao.addProduct(products[0]);
            return null;
        }
    }

    private static class UpdateProductAsyncTask extends AsyncTask<ProductsItem, Void, Void> {
        CartDao cartDao;

        public UpdateProductAsyncTask(CartDao cartDao) {
            this.cartDao = cartDao;
        }

        @Override
        protected Void doInBackground(ProductsItem... products) {
            cartDao.updateProduct(products[0]);
            return null;
        }
    }

    private static class DeleteProductAsyncTask extends AsyncTask<Integer, Void, Void> {
        CartDao cartDao;

        public DeleteProductAsyncTask(CartDao cartDao) {
            this.cartDao = cartDao;
        }

        @Override
        protected Void doInBackground(Integer... integers) {
            cartDao.deleteItem(integers[0]);
            return null;
        }
    }

    private static class EmptyCartAsyncTask extends AsyncTask<Void, Void, Void> {
        CartDao cartDao;

        public EmptyCartAsyncTask(CartDao cartDao) {
            this.cartDao = cartDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            cartDao.emptyProductCart();
            return null;
        }
    }

}
