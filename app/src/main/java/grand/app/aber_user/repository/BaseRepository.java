package grand.app.aber_user.repository;


import androidx.lifecycle.MutableLiveData;

import javax.inject.Inject;

import grand.app.aber_user.connection.ConnectionHelper;
import grand.app.aber_user.model.base.Mutable;
import grand.app.aber_user.model.base.StatusMessage;
import grand.app.aber_user.pages.auth.countries.models.CountriesResponse;
import grand.app.aber_user.pages.myLocations.models.LocationsResponse;
import grand.app.aber_user.utils.Constants;
import grand.app.aber_user.utils.URLS;
import io.reactivex.disposables.Disposable;


public class BaseRepository {
    @Inject
    ConnectionHelper connectionHelper;

    MutableLiveData<Mutable> liveData;

    public void setLiveData(MutableLiveData<Mutable> liveData) {
        this.liveData = liveData;
        connectionHelper.liveData = liveData;
    }

    public Disposable myLocations() {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.MY_LOCATIONS, new Object(), LocationsResponse.class,
                Constants.MY_LOCATIONS, true);
    }

    public Disposable getCountries() {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.COUNTRIES, new Object(), CountriesResponse.class,
                Constants.COUNTRIES, true);
    }

    public Disposable getCities(int governId) {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.CITIES+governId, new Object(), CountriesResponse.class,
                Constants.CITIES, true);
    }

    public Disposable getGovern(int countryId) {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.GOVERN+countryId, new Object(), CountriesResponse.class,
                Constants.GOVERNS, true);
    }
}
