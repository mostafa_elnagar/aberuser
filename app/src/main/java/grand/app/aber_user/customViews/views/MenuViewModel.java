package grand.app.aber_user.customViews.views;

import androidx.databinding.Bindable;
import androidx.databinding.ObservableBoolean;

import grand.app.aber_user.BR;
import grand.app.aber_user.base.BaseViewModel;
import grand.app.aber_user.model.base.Mutable;
import grand.app.aber_user.pages.home.adapters.MenuServicesAdapter;
import grand.app.aber_user.pages.home.models.MainData;
import grand.app.aber_user.pages.settings.adapters.MenuSocialAdapter;

public class MenuViewModel extends BaseViewModel {
    MainData mainData;
    MenuServicesAdapter menuServicesAdapter;
    MenuSocialAdapter socialAdapter;
    public ObservableBoolean showMenu = new ObservableBoolean();

    public void liveDataActions(String action) {
        getLiveData().setValue(new Mutable(action));
    }

    public void showCategories() {
        showMenu.set(!showMenu.get());
    }

    @Bindable
    public MainData getMainData() {
        return mainData;
    }

    @Bindable
    public void setMainData(MainData mainData) {
        getMenuServicesAdapter().update(mainData.getHomeServicesList());
        getSocialAdapter().update(mainData.getSocialMediaDataList());
        notifyChange(BR.menuServicesAdapter);
        notifyChange(BR.socialAdapter);
        notifyChange(BR.mainData);
        this.mainData = mainData;
    }

    @Bindable
    public MenuServicesAdapter getMenuServicesAdapter() {
        return this.menuServicesAdapter == null ? this.menuServicesAdapter = new MenuServicesAdapter() : this.menuServicesAdapter;
    }

    @Bindable
    public MenuSocialAdapter getSocialAdapter() {
        return this.socialAdapter == null ? this.socialAdapter = new MenuSocialAdapter() : this.socialAdapter;
    }

}
