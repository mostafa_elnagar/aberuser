package grand.app.aber_user.customViews.views;

import static grand.app.aber_user.utils.resources.ResourceManager.getString;

import android.app.Activity;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;

import grand.app.aber_user.PassingObject;
import grand.app.aber_user.R;
import grand.app.aber_user.activity.MainActivity;
import grand.app.aber_user.base.ParentActivity;
import grand.app.aber_user.customViews.actionbar.HomeActionBarView;
import grand.app.aber_user.databinding.MenuBinding;
import grand.app.aber_user.model.base.Mutable;
import grand.app.aber_user.pages.auth.countries.CountriesFragment;
import grand.app.aber_user.pages.cart.CartFragment;
import grand.app.aber_user.pages.favorites.FavoritesFragment;
import grand.app.aber_user.pages.home.HomeFragment;
import grand.app.aber_user.pages.myOrders.MyOrdersFragment;
import grand.app.aber_user.pages.myOrders.MyServicesOrdersFragment;
import grand.app.aber_user.pages.settings.AboutAppFragment;
import grand.app.aber_user.pages.settings.ContactUsFragment;
import grand.app.aber_user.pages.settings.LangFragment;
import grand.app.aber_user.pages.settings.MyAccountSettingsFragment;
import grand.app.aber_user.pages.settings.SocialMediaFragment;
import grand.app.aber_user.pages.settings.TermsFragment;
import grand.app.aber_user.utils.Constants;
import grand.app.aber_user.utils.helper.AppHelper;
import grand.app.aber_user.utils.helper.MovementHelper;
import grand.app.aber_user.utils.resources.ResourceManager;

public class NavigationDrawerView extends RelativeLayout {
    public MutableLiveData<Mutable> liveData;
    public MenuBinding layoutNavigationDrawerBinding;
    AppCompatActivity context;
    HomeActionBarView homeActionBarView;
    public MenuViewModel menuViewModel;

    public NavigationDrawerView(AppCompatActivity context) {
        super(context);
        this.context = context;
        liveData = new MutableLiveData<>();
        init();
    }

    public NavigationDrawerView(AppCompatActivity context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init();

    }

    public NavigationDrawerView(AppCompatActivity context, AttributeSet attrs, int defStyle) {
        super(context, attrs);
        this.context = context;
        init();
    }

    private void init() {
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        layoutNavigationDrawerBinding = DataBindingUtil.inflate(layoutInflater, R.layout.menu, this, true);
        menuViewModel = new MenuViewModel();
        layoutNavigationDrawerBinding.setMenuViewModel(menuViewModel);
        setEvents();
    }

    public void setActionBar(HomeActionBarView homeActionBarView) {
        this.homeActionBarView = homeActionBarView;
    }

    private void setEvents() {
        menuViewModel.getLiveData().observe(context, o -> {
            ((MainActivity) context).menuBuilder.closeMenu(true);
            Mutable mutable = (Mutable) o;
            ((ParentActivity) context).handleActions(mutable);
            switch (((Mutable) o).message) {
                case Constants.MENU_HOME:
                    ((MainActivity) context).setHomeActionTitle(getResources().getString(R.string.menuHome), "Visible");
                    MovementHelper.replaceFragment(context, new HomeFragment(), "");
                    break;
                case Constants.ABOUT:
                    MovementHelper.startActivity(context, AboutAppFragment.class.getName(), getString(R.string.about), null);
                    break;
                case Constants.TERMS:
                    MovementHelper.startActivityWithBundle(context, new PassingObject(Constants.TERMS), getResources().getString(R.string.terms), TermsFragment.class.getName(), null);
                    break;
                case Constants.SUGGESTION:
                    MovementHelper.startActivityWithBundle(context, new PassingObject(Constants.SUGGESTION), getString(R.string.tv_account_suggest), ContactUsFragment.class.getName(), null);
                    break;
                case Constants.CONTACT:
                    MovementHelper.startActivityWithBundle(context, new PassingObject(Constants.CONTACT), getString(R.string.tv_account_contact), ContactUsFragment.class.getName(), null);
                    break;
                case Constants.MENU_ACCOUNT:
                    MovementHelper.startActivity(context, MyAccountSettingsFragment.class.getName(), getString(R.string.menu_account), null);
                    break;
                case Constants.SOCIAL:
                    MovementHelper.startActivity(context, SocialMediaFragment.class.getName(), getString(R.string.social_media), null);
                    break;
                case Constants.CART:
                    MovementHelper.startActivity(context, CartFragment.class.getName(), getString(R.string.menuCart), null);
                    break;
                case Constants.FAVORITE:
                    MovementHelper.startActivity(context, FavoritesFragment.class.getName(), getString(R.string.menuFavorite), null);
                    break;
                case Constants.COUNTRIES:
                    MovementHelper.startActivity(context, CountriesFragment.class.getName(), getString(R.string.country), null);
                    break;
                case Constants.SERVICES:
                    MovementHelper.startActivity(context, MyServicesOrdersFragment.class.getName(), getString(R.string.my_services), null);
                    break;
                case Constants.LANGUAGE:
                    MovementHelper.startActivity(context, LangFragment.class.getName(), getString(R.string.change_lang), null);
                    break;
                case Constants.SHARE_BAR:
                    AppHelper.shareApp(context);
                    break;
                case Constants.RATE_APP:
                    AppHelper.rateApp(context);
                    break;
                case Constants.My_ORDERS:
                    MovementHelper.startActivity(context, MyOrdersFragment.class.getName(), getResources().getString(R.string.my_orders), null);
                    break;
                case Constants.SHOW_LOGOUT_WARNING:
                    ((ParentActivity) context).exitDialog(getString(R.string.logout_warning));
                    break;

            }
        });
    }

}
