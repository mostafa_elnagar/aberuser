package grand.app.aber_user.customViews.actionbar;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;

import grand.app.aber_user.R;
import grand.app.aber_user.base.ParentActivity;
import grand.app.aber_user.databinding.LayoutActionBarHomeBinding;
import grand.app.aber_user.model.base.Mutable;
import grand.app.aber_user.pages.conversations.ConversationsFragment;
import grand.app.aber_user.pages.home.SearchFragment;
import grand.app.aber_user.pages.notifications.NotificationsFragment;
import grand.app.aber_user.utils.Constants;
import grand.app.aber_user.utils.helper.MovementHelper;
import grand.app.aber_user.utils.resources.ResourceManager;
import grand.app.aber_user.utils.session.UserHelper;

public class HomeActionBarView extends RelativeLayout {
    public LayoutActionBarHomeBinding layoutActionBarHomeBinding;
    Context context;

    public HomeActionBarView(Context context) {
        super(context);
        this.context = context;
        init();
    }


    private void init() {
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        layoutActionBarHomeBinding = DataBindingUtil.inflate(layoutInflater, R.layout.layout_action_bar_home, this, true);
        layoutActionBarHomeBinding.badge.setText(UserHelper.getInstance(context).getCountNotification() > 0 ? String.valueOf(UserHelper.getInstance(context).getCountNotification()) : "");

        setEvents();
        ((ParentActivity) context).notificationsCount.observe((LifecycleOwner) context, count -> {
            layoutActionBarHomeBinding.badge.setText(count > 0 ? String.valueOf(count) : "");
        });
    }

    private void setEvents() {
        layoutActionBarHomeBinding.imgHomeBarMenuNotifications.setOnClickListener(view -> {
            UserHelper.getInstance(context).addCountNotification(0);
            layoutActionBarHomeBinding.badge.setText("");
            if (UserHelper.getInstance(context).getUserData() != null)
                MovementHelper.startActivity(context, NotificationsFragment.class.getName(), ResourceManager.getString(R.string.notifications), null);
            else
                ((ParentActivity) context).handleActions(new Mutable(Constants.LOGOUT));
        });
        layoutActionBarHomeBinding.imgHomeBarMenuSearch.setOnClickListener(view -> {
            MovementHelper.startActivity(context, SearchFragment.class.getName(), ResourceManager.getString(R.string.search), null);
        });
        layoutActionBarHomeBinding.imgHomeBarMenuChat.setOnClickListener(view -> {
            if (UserHelper.getInstance(context).getUserData() != null)
                MovementHelper.startActivity(context, ConversationsFragment.class.getName(), null, null);
            else
                ((ParentActivity) context).handleActions(new Mutable(Constants.LOGOUT));
        });
    }

    public void setTitle(String string) {
//        layoutActionBarHomeBinding.tvHomeBarText.setText(string);
    }

    public void notificationVisible(int visible) {
        layoutActionBarHomeBinding.imgHomeBarMenuNotifications.setVisibility(visible);
        layoutActionBarHomeBinding.badge.setVisibility(visible);
    }
}
