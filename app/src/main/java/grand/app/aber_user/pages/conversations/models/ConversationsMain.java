package grand.app.aber_user.pages.conversations.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import grand.app.aber_user.model.PaginateMain;

public class ConversationsMain extends PaginateMain {
    @SerializedName("data")
    private List<ConversationsData> data;

    public List<ConversationsData> getData() {
        return data;
    }
}
