package grand.app.aber_user.pages.myOrders.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.recyclerview.widget.RecyclerView;
import org.jetbrains.annotations.NotNull;
import java.util.ArrayList;
import java.util.List;
import grand.app.aber_user.PassingObject;
import grand.app.aber_user.R;
import grand.app.aber_user.databinding.ItemMainServiceOrderBinding;
import grand.app.aber_user.pages.myOrders.MyServiceOrderDetailsFragment;
import grand.app.aber_user.pages.myOrders.models.orderServices.ServicesOrderDetails;
import grand.app.aber_user.pages.myOrders.viewModels.ItemMyOrderServiceViewModel;
import grand.app.aber_user.utils.helper.MovementHelper;
import grand.app.aber_user.utils.resources.ResourceManager;

public class MyServicesOrdersAdapter extends RecyclerView.Adapter<MyServicesOrdersAdapter.MenuView> {
    List<ServicesOrderDetails> orderDetailsList;
    Context context;

    public MyServicesOrdersAdapter() {
        this.orderDetailsList = new ArrayList<>();
    }

    public List<ServicesOrderDetails> getOrderDetailsList() {
        return orderDetailsList;
    }

    @NonNull
    @Override
    public MenuView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_main_service_order,
                parent, false);
        this.context = parent.getContext();
        return new MenuView(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull final MenuView holder, final int position) {
        ServicesOrderDetails menuModel = orderDetailsList.get(position);
        ItemMyOrderServiceViewModel itemMenuViewModel = new ItemMyOrderServiceViewModel(menuModel);
        itemMenuViewModel.getLiveData().observe((LifecycleOwner) context, o -> MovementHelper.startActivityWithBundle(context, new PassingObject(menuModel.getId()), ResourceManager.getString(R.string.service_details), MyServiceOrderDetailsFragment.class.getName(), "VISABLE"));
        holder.setViewModel(itemMenuViewModel);
    }

    public void update(@NotNull List<ServicesOrderDetails> dataList) {
        this.orderDetailsList.clear();
        orderDetailsList.addAll(dataList);
        notifyDataSetChanged();
    }

    public void loadMore(@NotNull List<ServicesOrderDetails> dataList) {
        int start = orderDetailsList.size();
        orderDetailsList.addAll(dataList);
        notifyItemRangeInserted(start, dataList.size());
    }

    @Override
    public void onViewAttachedToWindow(@NotNull MenuView holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NotNull MenuView holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    @Override
    public int getItemCount() {
        return orderDetailsList.size();
    }

    public class MenuView extends RecyclerView.ViewHolder {
        public ItemMainServiceOrderBinding itemMenuBinding;

        MenuView(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemMenuBinding == null) {
                itemMenuBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemMenuBinding != null) {
                itemMenuBinding.unbind();
            }
        }

        void setViewModel(ItemMyOrderServiceViewModel itemViewModels) {
            if (itemMenuBinding != null) {
                itemMenuBinding.setItemOrderViewModel(itemViewModels);
            }
        }
    }
}
