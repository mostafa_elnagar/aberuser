package grand.app.aber_user.pages.services.models;

import com.google.gson.annotations.SerializedName;

public class Additions {
    @SerializedName("emergency_cost")
    private double emergencyCost;
    @SerializedName("transfer_service")
    private double transferService;
    @SerializedName("kilo_cost")
    private float kiloCost;
    @SerializedName("tax")
    private float taxes;

    public double getEmergencyCost() {
        return emergencyCost;
    }

    public double getTransferService() {
        return transferService;
    }

    public float getKiloCost() {
        return kiloCost;
    }

    public float getTaxes() {
        return taxes;
    }
}
