package grand.app.aber_user.pages.conversations.viewModels;

import androidx.databinding.Bindable;

import grand.app.aber_user.base.BaseViewModel;
import grand.app.aber_user.pages.conversations.models.ConversationsData;
import grand.app.aber_user.utils.Constants;

public class ItemConversationsViewModel extends BaseViewModel {
    public ConversationsData conversationsData;

    public ItemConversationsViewModel(ConversationsData conversationsData) {
        this.conversationsData = conversationsData;

    }

    @Bindable
    public ConversationsData getConversationsData() {
        return conversationsData;
    }

    public void itemAction() {
        getLiveData().setValue(Constants.MENu);
    }

}
