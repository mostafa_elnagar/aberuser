package grand.app.aber_user.pages.parts.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.smarteist.autoimageslider.SliderViewAdapter;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import grand.app.aber_user.R;
import grand.app.aber_user.pages.parts.models.productDetails.ImagesItem;
import grand.app.aber_user.utils.images.PhotoFullPopupWindow;


public class ProductImagesSliderAdapter extends SliderViewAdapter<ProductImagesSliderAdapter.SliderAdapterVH> {
    public List<ImagesItem> pagerList;
    private Context context;

    public ProductImagesSliderAdapter() {
        pagerList = new ArrayList<>();
    }

    @Override
    public SliderAdapterVH onCreateViewHolder(ViewGroup parent) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_slider_item, null);
        context = parent.getContext();
        return new SliderAdapterVH(inflate);
    }

    @Override
    public void onBindViewHolder(SliderAdapterVH viewHolder, int position) {
        ImagesItem introItem = pagerList.get(position);
        Glide.with(context).load(introItem.getImage()).placeholder(R.color.overlayBackground).into(viewHolder.imageViewBackground);
        viewHolder.imageViewBackground.setOnClickListener(v -> new PhotoFullPopupWindow(context, R.layout.popup_photo_full, viewHolder.imageViewBackground, introItem.getImage(), null));
    }

    @Override
    public int getCount() {
        return pagerList.size();
    }

    public void updateData(@NotNull List<ImagesItem> data) {
        this.pagerList.clear();
        this.pagerList.addAll(data);
        notifyDataSetChanged();
    }

    class SliderAdapterVH extends SliderViewAdapter.ViewHolder {
        View itemView;
        ImageView imageViewBackground;

        public SliderAdapterVH(View itemView) {
            super(itemView);
            imageViewBackground = itemView.findViewById(R.id.iv_auto_image_slider);
            this.itemView = itemView;
        }
    }
}
