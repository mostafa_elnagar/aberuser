package grand.app.aber_user.pages.home.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.smarteist.autoimageslider.SliderViewAdapter;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import grand.app.aber_user.PassingObject;
import grand.app.aber_user.R;
import grand.app.aber_user.pages.home.models.HomeSliderData;
import grand.app.aber_user.pages.parts.PartsFragment;
import grand.app.aber_user.pages.services.AberBoxFragment;
import grand.app.aber_user.pages.services.BatteriesFragment;
import grand.app.aber_user.pages.services.CarCheckFragment;
import grand.app.aber_user.pages.services.CarWashFragment;
import grand.app.aber_user.pages.services.FuelFragment;
import grand.app.aber_user.pages.services.HiddenFragment;
import grand.app.aber_user.pages.services.OilsFragment;
import grand.app.aber_user.pages.services.OpenCarFragment;
import grand.app.aber_user.pages.services.TiersFragment;
import grand.app.aber_user.pages.services.WaterBallonFragment;
import grand.app.aber_user.pages.services.WinchFragment;
import grand.app.aber_user.utils.helper.MovementHelper;


public class HomeSliderAdapter extends SliderViewAdapter<HomeSliderAdapter.SliderAdapterVH> {
    public List<HomeSliderData> pagerList;
    private Context context;

    public HomeSliderAdapter() {
        pagerList = new ArrayList<>();
    }

    @Override
    public SliderAdapterVH onCreateViewHolder(ViewGroup parent) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_slider_item, null);
        context = parent.getContext();
        return new SliderAdapterVH(inflate);
    }

    @Override
    public void onBindViewHolder(SliderAdapterVH viewHolder, int position) {
        HomeSliderData introItem = pagerList.get(position);
        Glide.with(context).load(introItem.getImage()).placeholder(R.color.overlayBackground).into(viewHolder.imageViewBackground);
        viewHolder.imageViewBackground.setOnClickListener(v -> {
            if (introItem.getServiceId() == 1) {
                MovementHelper.startActivityWithBundle(MovementHelper.unwrap(context), new PassingObject(introItem.getServiceId()), introItem.getServiceName(), WinchFragment.class.getName(), null);
            } else if (introItem.getServiceId() == 3) {
                MovementHelper.startActivityWithBundle(MovementHelper.unwrap(context), new PassingObject(introItem.getServiceId()), introItem.getServiceName(), CarCheckFragment.class.getName(), null);
            } else if (introItem.getServiceId() == 5) {
                MovementHelper.startActivityWithBundle(MovementHelper.unwrap(context), new PassingObject(introItem.getServiceId()), introItem.getServiceName(), PartsFragment.class.getName(), null);
            } else if (introItem.getServiceId() == 7) {
                MovementHelper.startActivityWithBundle(MovementHelper.unwrap(context), new PassingObject(introItem.getServiceId()), introItem.getServiceName(), CarWashFragment.class.getName(), null);
            } else if (introItem.getServiceId() == 9) {
                MovementHelper.startActivityWithBundle(MovementHelper.unwrap(context), new PassingObject(introItem.getServiceId()), introItem.getServiceName(), TiersFragment.class.getName(), null);
            } else if (introItem.getServiceId() == 11) {
                MovementHelper.startActivityWithBundle(MovementHelper.unwrap(context), new PassingObject(introItem.getServiceId()), introItem.getServiceName(), OilsFragment.class.getName(), null);
            } else if (introItem.getServiceId() == 13) {
                MovementHelper.startActivityWithBundle(MovementHelper.unwrap(context), new PassingObject(introItem.getServiceId()), introItem.getServiceName(), HiddenFragment.class.getName(), null);
            } else if (introItem.getServiceId() == 15) {
                MovementHelper.startActivityWithBundle(MovementHelper.unwrap(context), new PassingObject(introItem.getServiceId()), introItem.getServiceName(), FuelFragment.class.getName(), null);
            } else if (introItem.getServiceId() == 17) {
                MovementHelper.startActivityWithBundle(MovementHelper.unwrap(context), new PassingObject(introItem.getServiceId()), introItem.getServiceName(), WaterBallonFragment.class.getName(), null);
            } else if (introItem.getServiceId() == 19) {
                MovementHelper.startActivityWithBundle(MovementHelper.unwrap(context), new PassingObject(introItem.getServiceId()), introItem.getServiceName(), OpenCarFragment.class.getName(), null);
            } else if (introItem.getServiceId() == 21) {
                MovementHelper.startActivityWithBundle(MovementHelper.unwrap(context), new PassingObject(introItem.getServiceId()), introItem.getServiceName(), BatteriesFragment.class.getName(), null);
            } else if (introItem.getServiceId() == 23) {
                MovementHelper.startActivityWithBundle(MovementHelper.unwrap(context), new PassingObject(introItem.getServiceId()), introItem.getServiceName(), AberBoxFragment.class.getName(), null);
            }
        });
    }

    @Override
    public int getCount() {
        //slider view count could be dynamic size
        return pagerList.size();
    }

    public void updateData(@NotNull List<HomeSliderData> data) {
        this.pagerList.clear();
        this.pagerList.addAll(data);
        notifyDataSetChanged();
    }

    class SliderAdapterVH extends SliderViewAdapter.ViewHolder {
        View itemView;
        ImageView imageViewBackground;

        public SliderAdapterVH(View itemView) {
            super(itemView);
            imageViewBackground = itemView.findViewById(R.id.iv_auto_image_slider);
            this.itemView = itemView;
        }
    }
}
