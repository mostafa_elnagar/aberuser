package grand.app.aber_user.pages.parts.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import grand.app.aber_user.R;
import grand.app.aber_user.databinding.ItemColorBinding;
import grand.app.aber_user.pages.parts.models.productDetails.AttributesItem;
import grand.app.aber_user.pages.parts.viewModels.ItemProductColorsViewModel;

public class ProductColorsAdapter extends RecyclerView.Adapter<ProductColorsAdapter.MenuView> {
    List<AttributesItem> colorsItemList;
    Context context;
    public int lastSelected = -1, lastPosition = -1;
    public MutableLiveData<Object> liveData = new MutableLiveData<>();

    public ProductColorsAdapter() {
        this.colorsItemList = new ArrayList<>();
    }

    public List<AttributesItem> getColorsItemList() {
        return colorsItemList;
    }

    @NonNull
    @Override
    public MenuView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_color,
                parent, false);
        this.context = parent.getContext();
        return new MenuView(itemView);
    }

    @SuppressLint("RecyclerView")
    @Override
    public void onBindViewHolder(@NonNull final MenuView holder, final int position) {
        AttributesItem menuModel = colorsItemList.get(position);
        ItemProductColorsViewModel itemMenuViewModel = new ItemProductColorsViewModel(menuModel);
        itemMenuViewModel.getLiveData().observe((LifecycleOwner) context, o -> {
            lastPosition = position;
            lastSelected = menuModel.getId();
            liveData.setValue(o);
            notifyDataSetChanged();
        });
        menuModel.setChecked(lastSelected == menuModel.getId());
        holder.setViewModel(itemMenuViewModel);
    }

    public void update(@NotNull List<AttributesItem> dataList) {
        this.colorsItemList.clear();
        colorsItemList.addAll(dataList);
        lastSelected = dataList.get(0).getId();
        lastPosition = 0;
        notifyDataSetChanged();
    }

    @Override
    public void onViewAttachedToWindow(@NotNull MenuView holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NotNull MenuView holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    @Override
    public int getItemCount() {
        return colorsItemList.size();
    }

    public class MenuView extends RecyclerView.ViewHolder {
        public ItemColorBinding itemMenuBinding;

        MenuView(View itemView) {
            super(itemView);
            bind();
        }

        void bind() {
            if (itemMenuBinding == null) {
                itemMenuBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemMenuBinding != null) {
                itemMenuBinding.unbind();
            }
        }

        void setViewModel(ItemProductColorsViewModel itemViewModels) {
            if (itemMenuBinding != null) {
                itemMenuBinding.setItemViewModel(itemViewModels);
            }
        }
    }
}
