package grand.app.aber_user.pages.parts.models.filter;

import com.google.gson.annotations.SerializedName;

import grand.app.aber_user.model.base.StatusMessage;

public class FilterDataResponse extends StatusMessage {

    @SerializedName("data")
    private FilterData data;

    public FilterData getData() {
        return data;
    }

}