package grand.app.aber_user.pages.reviews;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;


import java.util.Objects;

import javax.inject.Inject;

import grand.app.aber_user.PassingObject;
import grand.app.aber_user.R;
import grand.app.aber_user.base.BaseFragment;
import grand.app.aber_user.base.IApplicationComponent;
import grand.app.aber_user.base.MyApplication;
import grand.app.aber_user.databinding.FragmentReviewsBinding;
import grand.app.aber_user.databinding.ReviewDialogBinding;
import grand.app.aber_user.model.base.Mutable;
import grand.app.aber_user.pages.auth.login.LoginFragment;
import grand.app.aber_user.pages.reviews.models.RateRequest;
import grand.app.aber_user.pages.reviews.models.ReviewsResponse;
import grand.app.aber_user.pages.reviews.viewModels.ReviewsViewModel;
import grand.app.aber_user.utils.Constants;
import grand.app.aber_user.utils.helper.MovementHelper;


public class ReviewsFragment extends BaseFragment {
    private Dialog dialog;
    @Inject
    ReviewsViewModel viewModel;
    FragmentReviewsBinding binding;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_reviews, container, false);
        IApplicationComponent component = ((MyApplication) requireActivity().getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setViewmodel(viewModel);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            String passingObject = bundle.getString(Constants.BUNDLE);
            viewModel.setPassingObject(new Gson().fromJson(passingObject, PassingObject.class));
            viewModel.clientReviews(1, true);
        }
        setEvent();
        return binding.getRoot();
    }

    private void setEvent() {
        viewModel.liveData.observe(requireActivity(), (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            switch (((Mutable) o).message) {
                case Constants.REVIEWS:
                    viewModel.setReviewMainData(((ReviewsResponse) mutable.object).getReviewMainData());
                    break;
                case Constants.REVIEW_NEW:
                    if (viewModel.userData != null)
                        showRateDialog();
                    else
                        MovementHelper.startActivity(requireActivity(), LoginFragment.class.getName(), null, null);
                    break;
                case Constants.DIALOG_CLOSE:
                    dialog.dismiss();
                    break;
                case Constants.NEW_RATE:
                    viewModel.getReviewsAdapter().ratesItems.clear();
                    viewModel.clientReviews(1, true);
                    viewModel.setRateRequest(new RateRequest());
                    Constants.DATA_CHANGED = true;
                    dialog.dismiss();
                    closeKeyboard();
                    break;
            }
        });
        binding.rcReviews.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                if (!viewModel.searchProgressVisible.get() && !TextUtils.isEmpty(viewModel.getReviewMainData().getLinks().getNext())) {
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == viewModel.getReviewsAdapter().getRatesItems().size() - 1) {
                        viewModel.searchProgressVisible.set(true);
                        viewModel.clientReviews((viewModel.getReviewMainData().getMeta().getCurrentPage() + 1), false);
                    }
                }
            }
        });
    }

    private void showRateDialog() {
        dialog = new Dialog(requireActivity(), R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Objects.requireNonNull(dialog.getWindow()).getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        ReviewDialogBinding binding = DataBindingUtil.inflate(LayoutInflater.from(dialog.getContext()), R.layout.review_dialog, null, false);
        dialog.setContentView(binding.getRoot());
        binding.setViewModel(viewModel);
        dialog.setOnDismissListener(dialog -> {
            viewModel.setRateRequest(new RateRequest());
            viewModel.notifyChange();
        });

        dialog.show();
    }

    @Override
    public void onResume() {
        super.onResume();
        viewModel.getServicesRepository().setLiveData(viewModel.liveData);
    }
}
