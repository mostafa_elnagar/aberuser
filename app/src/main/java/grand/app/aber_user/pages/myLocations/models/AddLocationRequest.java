package grand.app.aber_user.pages.myLocations.models;


import androidx.databinding.ObservableField;

import com.google.gson.annotations.SerializedName;

import grand.app.aber_user.utils.Constants;
import grand.app.aber_user.utils.validation.Validate;

public class AddLocationRequest {
    @SerializedName("first_name")
    private String name;
    @SerializedName("last_name")
    private String last_name;
    @SerializedName("special_marque")
    private String special_marque;
    @SerializedName("phone")
    private String phone;
    @SerializedName("additional_phone")
    private String additional_phone;
    @SerializedName("building_no")
    private String building_no;
    @SerializedName("street")
    private String street;
    @SerializedName("floor")
    private String floor;
    @SerializedName("city_id")
    private String city_id;
    @SerializedName("country_id")
    private transient String countryId;
    @SerializedName("govern_id")
    private transient String governId;
    @SerializedName("location_id")
    private String location_id;
    public transient ObservableField<String> nameError = new ObservableField<>();
    public transient ObservableField<String> lastNameError = new ObservableField<>();
    public transient ObservableField<String> streetError = new ObservableField<>();
    public transient ObservableField<String> floorError = new ObservableField<>();
    public transient ObservableField<String> specialMarqueError = new ObservableField<>();
    public transient ObservableField<String> cityError = new ObservableField<>();
    public transient ObservableField<String> countryError = new ObservableField<>();
    public transient ObservableField<String> governError = new ObservableField<>();
    public transient ObservableField<String> regionError = new ObservableField<>();
    public transient ObservableField<String> buildingNoError = new ObservableField<>();
    public transient ObservableField<String> phoneError = new ObservableField<>();

    public boolean isValid() {
        boolean valid = true;
        if (!Validate.isValid(name, Constants.FIELD)) {
            nameError.set(Validate.error);
            valid = false;
        } else if (!Validate.isValid(last_name, Constants.FIELD)) {
            lastNameError.set(Validate.error);
            valid = false;
        } else if (!Validate.isValid(countryId, Constants.FIELD)) {
            countryError.set(Validate.error);
            valid = false;
        } else if (!Validate.isValid(governId, Constants.FIELD)) {
            governError.set(Validate.error);
            valid = false;
        } else if (!Validate.isValid(city_id, Constants.FIELD)) {
            cityError.set(Validate.error);
            valid = false;
        } else if (!Validate.isValid(street, Constants.FIELD)) {
            streetError.set(Validate.error);
            valid = false;
        } else if (!Validate.isValid(building_no, Constants.FIELD)) {
            buildingNoError.set(Validate.error);
            valid = false;
        } else if (!Validate.isValid(floor, Constants.FIELD)) {
            floorError.set(Validate.error);
            valid = false;
        } else if (!Validate.isValid(phone, Constants.FIELD)) {
            phoneError.set(Validate.error);
            valid = false;
        }
        return valid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        nameError.set(null);
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        phoneError.set(null);
        this.phone = phone;
    }

    public String getBuilding_no() {
        return building_no;
    }

    public void setBuilding_no(String building_no) {
        buildingNoError.set(null);
        this.building_no = building_no;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        streetError.set(null);
        this.street = street;
    }

    public String getFloor() {
        return floor;
    }

    public void setFloor(String floor) {
        floorError.set(null);
        this.floor = floor;
    }

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        cityError.set(null);
        this.city_id = city_id;
    }

    public String getLocation_id() {
        return location_id;
    }

    public void setLocation_id(String location_id) {
        this.location_id = location_id;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        lastNameError.set(null);
        this.last_name = last_name;
    }

    public String getSpecial_marque() {
        return special_marque;
    }

    public void setSpecial_marque(String special_marque) {
        specialMarqueError.set(null);
        this.special_marque = special_marque;
    }

    public String getAdditional_phone() {
        return additional_phone;
    }

    public void setAdditional_phone(String additional_phone) {
        this.additional_phone = additional_phone;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        countryError.set(null);
        this.countryId = countryId;
    }

    public String getGovernId() {
        return governId;
    }

    public void setGovernId(String governId) {
        governError.set(null);
        this.governId = governId;
    }
}