package grand.app.aber_user.pages.parts;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;

import javax.inject.Inject;

import grand.app.aber_user.PassingObject;
import grand.app.aber_user.R;
import grand.app.aber_user.base.BaseFragment;
import grand.app.aber_user.base.IApplicationComponent;
import grand.app.aber_user.base.MyApplication;
import grand.app.aber_user.databinding.FragmentPartServicesBinding;
import grand.app.aber_user.databinding.ProductSortBinding;
import grand.app.aber_user.model.base.Mutable;
import grand.app.aber_user.pages.parts.models.ProductMain;
import grand.app.aber_user.pages.parts.models.ProductsResponse;
import grand.app.aber_user.pages.parts.viewModels.PartServicesViewModels;
import grand.app.aber_user.utils.Constants;
import grand.app.aber_user.utils.helper.MovementHelper;

public class PartServicesFragment extends BaseFragment {
    @Inject
    PartServicesViewModels viewModel;
    FragmentPartServicesBinding binding;
    BottomSheetDialog sheetDialog;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_part_services, container, false);
        IApplicationComponent component = ((MyApplication) requireActivity().getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setViewModel(viewModel);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            String passingObject = bundle.getString(Constants.BUNDLE);
            viewModel.setPassingObject(new Gson().fromJson(passingObject, PassingObject.class));
            viewModel.partServices(1, true, 1);
        }
        binding.tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0) {
                    viewModel.getFilterRequest().setIsNew("1");
                    viewModel.partServices(1, true, 1);
                } else if (tab.getPosition() == 1) {
                    viewModel.getFilterRequest().setIsNew("0");
                    viewModel.partServices(1, true, 0);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        setEvent();
        return binding.getRoot();
    }

    private void setEvent() {
        viewModel.liveData.observe(requireActivity(), (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            viewModel.setMessage(mutable.message.equals(Constants.HIDE_PROGRESS) ? mutable.message : "");
            if (Constants.PRODUCTS.equals(((Mutable) o).message)) {
                viewModel.setMainData(((ProductsResponse) mutable.object).getProductMain());
            } else if (Constants.DIALOG_SHOW.equals(((Mutable) o).message)) {
                ProductSortBinding sortBinding = DataBindingUtil.inflate(LayoutInflater.from(requireActivity()), R.layout.product_sort, null, false);
                sheetDialog = new BottomSheetDialog(requireActivity(), R.style.CustomBottomSheetDialog);
                sheetDialog.setContentView(sortBinding.getRoot());
                sortBinding.setViewModel(viewModel);
                sheetDialog.show();
            } else if (Constants.DIALOG_CLOSE.equals(((Mutable) o).message)) {
                sheetDialog.dismiss();
            } else if (Constants.FILTER.equals(((Mutable) o).message)) {
                MovementHelper.startActivityForResultWithBundle(requireActivity(), new PassingObject(viewModel.getPassingObject().getId(), !TextUtils.isEmpty(viewModel.getFilterRequest().getIsNew()) ? viewModel.getFilterRequest().getIsNew() : "1"), getString(R.string.filter), FilterFragment.class.getName(), Constants.ORDER_REQUEST);
            } else if (Constants.SORT_BY.equals(((Mutable) o).message)) {
                viewModel.getProductsAdapter().getProductsItemList().clear();
                viewModel.setMainData(((ProductsResponse) mutable.object).getProductMain());
            }
        });
        binding.rcProducts.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                if (!viewModel.searchProgressVisible.get() && !TextUtils.isEmpty(viewModel.getMainData().getProducts().getLinks().getNext())) {
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == viewModel.getProductsAdapter().getProductsItemList().size() - 1) {
                        viewModel.searchProgressVisible.set(true);
                        viewModel.partServices((viewModel.getMainData().getProducts().getMeta().getCurrentPage() + 1), false, 1);
                    }
                }
            }
        });
        viewModel.getProductsAdapter().liveData.observe(requireActivity(), integer -> viewModel.changeLike(integer));
    }

    @Override
    public void launchActivityResult(int request, int resultCode, Intent result) {
        super.launchActivityResult(request, resultCode, result);
        if (result != null) {
            if (request == Constants.ORDER_REQUEST) {
                Bundle bundle = result.getBundleExtra(Constants.BUNDLE);
                if (bundle != null && bundle.containsKey(Constants.BUNDLE)) {
                    PassingObject passingObject = (PassingObject) bundle.getSerializable(Constants.BUNDLE);
                    viewModel.getProductsAdapter().getProductsItemList().clear();
                    viewModel.setMainData(new Gson().fromJson(String.valueOf(passingObject.getObjectClass()), ProductMain.class));
                }
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        viewModel.getPostRepository().setLiveData(viewModel.liveData);
        if (Constants.DATA_CHANGED) {
            Constants.DATA_CHANGED = false;
            viewModel.getProductsAdapter().getProductsItemList().clear();
            viewModel.partServices(1, true, 1);
        }
    }
}
