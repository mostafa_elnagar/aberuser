package grand.app.aber_user.pages.myLocations.viewModels;

import androidx.databinding.Bindable;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import grand.app.aber_user.base.BaseViewModel;
import grand.app.aber_user.model.base.Mutable;
import grand.app.aber_user.pages.auth.countries.models.CountriesData;
import grand.app.aber_user.pages.myLocations.adapters.LocationsAdapters;
import grand.app.aber_user.pages.myLocations.models.AddLocationRequest;
import grand.app.aber_user.pages.myLocations.models.LocationsData;
import grand.app.aber_user.repository.AuthRepository;
import grand.app.aber_user.utils.Constants;
import io.reactivex.disposables.CompositeDisposable;

public class MyLocationsViewModel extends BaseViewModel {
    public MutableLiveData<Mutable> liveData;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    @Inject
    AuthRepository repository;
    LocationsAdapters locationsAdapters;
    AddLocationRequest addLocationRequest;
    LocationsData locationsData;
    public List<CountriesData> countriesDataList, governList, cityList;

    @Inject
    public MyLocationsViewModel(AuthRepository repository) {
        countriesDataList = new ArrayList<>();
        governList = new ArrayList<>();
        cityList = new ArrayList<>();
        locationsData = new LocationsData();
        this.repository = repository;
        this.liveData = new MutableLiveData<>();
        repository.setLiveData(liveData);
    }


    public void getLocations() {
        compositeDisposable.add(repository.myLocations());
    }


    public void getCountries() {
        compositeDisposable.add(repository.getCountries());
    }

    public void getGoverns(int countryId) {
        compositeDisposable.add(repository.getGovern(countryId));
    }

    public void getCities(int governId) {
        compositeDisposable.add(repository.getCities(governId));
    }

    public void addPlace() {
        if (getAddLocationRequest().isValid()) {
            compositeDisposable.add(repository.addLocation(getAddLocationRequest()));
        }
    }

    public void deleteLocation(int locationId) {
        compositeDisposable.add(repository.deleteLocation(locationId));
    }

    public void toAddPlace() {
        liveData.setValue(new Mutable(Constants.ADD_PLACE));
    }

    public void inputAction(String action) {
        liveData.setValue(new Mutable(action));
    }

    @Bindable
    public AddLocationRequest getAddLocationRequest() {
        return this.addLocationRequest == null ? this.addLocationRequest = new AddLocationRequest() : this.addLocationRequest;
    }

    public void setLocationsData(LocationsData locationsData) {
//        getAddLocationRequest().setTitle(locationsData.getTitle());
//        getAddLocationRequest().setStreet(locationsData.getStreet());
//        getAddLocationRequest().setFloor(locationsData.getFloor());
//        getAddLocationRequest().setCity_id(locationsData.getCity_id());
//        getAddLocationRequest().setRegion_id(locationsData.getRegion_id());
//        getAddLocationRequest().setLocation_id(String.valueOf(locationsData.getId()));
//        getAddLocationRequest().setLat(locationsData.getLat());
//        getAddLocationRequest().setLng(locationsData.getLng());
//        notifyChange();
        this.locationsData = locationsData;
    }

    public LocationsData getLocationsData() {
        return locationsData;
    }

    @Bindable
    public LocationsAdapters getLocationsAdapters() {
        return this.locationsAdapters == null ? this.locationsAdapters = new LocationsAdapters() : this.locationsAdapters;
    }

    protected void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        unSubscribeFromObservable();
    }


    public AuthRepository getRepository() {
        return repository;
    }

}