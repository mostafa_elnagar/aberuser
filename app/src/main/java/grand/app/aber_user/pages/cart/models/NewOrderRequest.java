package grand.app.aber_user.pages.cart.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import grand.app.aber_user.pages.parts.models.ProductsItem;


public class NewOrderRequest {
    @SerializedName("discount")
    private String discount;
    @SerializedName("total")
    private String total;
    @SerializedName("delivery_fees")
    private String deliveryFee;
    @SerializedName("promo_id")
    private String promoId;
    @SerializedName("description")
    private String description;
    @SerializedName("address_id")
    private String locationId;
    @SerializedName("address")
    private String address;
    private String currency;
    @SerializedName("subtotal")
    private String totalCart;
    @SerializedName("pay_type")
    private String paymentMethod;
    @SerializedName("products")
    private List<ProductsItem> productsItems;

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getDeliveryFee() {
        return deliveryFee;
    }

    public void setDeliveryFee(String deliveryFee) {
        this.deliveryFee = deliveryFee;
    }

    public String getPromoId() {
        return promoId;
    }

    public void setPromoId(String promoId) {
        this.promoId = promoId;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public List<ProductsItem> getProductsItems() {
        return productsItems;
    }

    public void setProductsItems(List<ProductsItem> productsItems) {
        this.productsItems = productsItems;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getTotalCart() {
        return totalCart;
    }

    public void setTotalCart(String totalCart) {
        this.totalCart = totalCart;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
