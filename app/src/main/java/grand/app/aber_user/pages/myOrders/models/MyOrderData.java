package grand.app.aber_user.pages.myOrders.models;

import java.util.List;

import com.google.gson.annotations.SerializedName;

import grand.app.aber_user.pages.parts.models.ProductsItem;

public class MyOrderData {

    @SerializedName("id")
    private int id;

    @SerializedName("status")
    private int status;

    @SerializedName("products")
    private List<ProductsItem> products;

    public int getId() {
        return id;
    }

    public List<ProductsItem> getProducts() {
        return products;
    }

    public int getStatus() {
        return status;
    }
}