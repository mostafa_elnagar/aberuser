package grand.app.aber_user.pages.services.viewModels;


import android.text.TextUtils;
import android.util.Log;
import android.widget.RadioGroup;

import androidx.databinding.Bindable;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import grand.app.aber_user.BR;
import grand.app.aber_user.R;
import grand.app.aber_user.base.BaseViewModel;
import grand.app.aber_user.connection.FileObject;
import grand.app.aber_user.model.base.Mutable;
import grand.app.aber_user.pages.services.adapters.ExtraConfirmAdapter;
import grand.app.aber_user.pages.services.adapters.OrderConfirmAdapter;
import grand.app.aber_user.pages.services.models.CreateServiceOrder;
import grand.app.aber_user.pages.services.models.ServiceOrderDetails;
import grand.app.aber_user.repository.ServicesRepository;
import grand.app.aber_user.utils.Constants;
import grand.app.aber_user.utils.helper.AppHelper;
import io.reactivex.disposables.CompositeDisposable;

public class ServiceConfirmOrderViewModels extends BaseViewModel {
    public MutableLiveData<Mutable> liveData;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    OrderConfirmAdapter detailsAdapter;
    ExtraConfirmAdapter extraAdapter;
    @Inject
    ServicesRepository postRepository;
    ServiceOrderDetails serviceOrderRequest;
    CreateServiceOrder createServiceOrder;
    ArrayList<FileObject> fileObjectList = new ArrayList<>();

    @Inject
    public ServiceConfirmOrderViewModels(ServicesRepository postRepository) {
        serviceOrderRequest = new ServiceOrderDetails();
        this.postRepository = postRepository;
        this.liveData = new MutableLiveData<>();
        postRepository.setLiveData(liveData);
    }


    public void confirmServiceOrder() {
        setMessage(Constants.SHOW_PROGRESS);
        compositeDisposable.add(postRepository.createService(getCreateServiceOrder(), fileObjectList));
    }

    public void liveDataActions(String action) {
        liveData.setValue(new Mutable(action));
    }

    @Bindable
    public OrderConfirmAdapter getDetailsAdapter() {
        return this.detailsAdapter == null ? this.detailsAdapter = new OrderConfirmAdapter() : this.detailsAdapter;
    }

    @Bindable
    public ExtraConfirmAdapter getExtraAdapter() {
        return this.extraAdapter == null ? this.extraAdapter = new ExtraConfirmAdapter() : this.extraAdapter;
    }

    public ServiceOrderDetails getServiceOrderRequest() {
        return serviceOrderRequest;
    }

    public void setServiceOrderRequest(ServiceOrderDetails serviceOrderRequest) {
        if (serviceOrderRequest.getServicesIds() != null)
            getDetailsAdapter().update(serviceOrderRequest.getServicesIds());
        if (serviceOrderRequest.getExtraList() != null)
            getExtraAdapter().update(serviceOrderRequest.getExtraList());
        this.serviceOrderRequest = serviceOrderRequest;
        // setting data for creating order service
        // if services ids not null
        List<Integer> servicesIds = new ArrayList<>();
        if (serviceOrderRequest.getServicesIds() != null) {
            for (int i = 0; i < serviceOrderRequest.getServicesIds().size(); i++) {
                servicesIds.add(serviceOrderRequest.getServicesIds().get(i).getId());
            }
            getCreateServiceOrder().setSubServiceIdsList(servicesIds);
        }
        // if extra not null
        List<Integer> extraIds = new ArrayList<>();
        List<Integer> childIds = new ArrayList<>();
        if (serviceOrderRequest.getExtraList() != null) {
            for (int i = 0; i < serviceOrderRequest.getExtraList().size(); i++) {
                extraIds.add(serviceOrderRequest.getExtraList().get(i).getId());
            }
            getCreateServiceOrder().setExtraIdsList(extraIds);
        }
        // adding additions to extra list just any child
        if (serviceOrderRequest.getCarModelId() != 0)
            childIds.add(serviceOrderRequest.getCarModelId());
        if (serviceOrderRequest.getTyerDescId() != 0)
            childIds.add(serviceOrderRequest.getTyerDescId());
        if (serviceOrderRequest.getOilLiquidId() != 0)
            childIds.add(serviceOrderRequest.getOilLiquidId());
        if (serviceOrderRequest.getKmId() != 0)
            extraIds.add(serviceOrderRequest.getKmId());
        if (serviceOrderRequest.getHiddenPercentageId() != 0)
            childIds.add(serviceOrderRequest.getHiddenPercentageId());
        if (serviceOrderRequest.getFuelCatId() != 0)
            childIds.add(serviceOrderRequest.getFuelCatId());
        if (serviceOrderRequest.getGallonId() != 0)
            extraIds.add(serviceOrderRequest.getGallonId());
        if (serviceOrderRequest.getLitreId() != 0)
            extraIds.add(serviceOrderRequest.getLitreId());
        if (serviceOrderRequest.getTinkerServiceId() != 0)
            extraIds.add(serviceOrderRequest.getTinkerServiceId());
        if (!TextUtils.isEmpty(serviceOrderRequest.getBatterySizeId()))
            childIds.add(Integer.parseInt(serviceOrderRequest.getBatterySizeId()));
        if (!TextUtils.isEmpty(serviceOrderRequest.getBatterySizeId()))
            childIds.add(Integer.parseInt(serviceOrderRequest.getBatterySizeId()));
        if (!TextUtils.isEmpty(serviceOrderRequest.getBoxType()))
            childIds.add(serviceOrderRequest.getBoxTypeId());

        getCreateServiceOrder().setExtraIdsList(extraIds);
        getCreateServiceOrder().setChildIdsList(childIds);
// texts
        getCreateServiceOrder().setMainServiceId(serviceOrderRequest.getMainService());
        getCreateServiceOrder().setExtraText(serviceOrderRequest.getCarMotor());
        getCreateServiceOrder().setDescription(serviceOrderRequest.getDesc());
        getCreateServiceOrder().setExtraFees(serviceOrderRequest.getExtraTotalServices());
        getCreateServiceOrder().setSubtotal(serviceOrderRequest.getTotalServices());
        getCreateServiceOrder().setSubtotal(serviceOrderRequest.getTotalServices());
        getCreateServiceOrder().setIs_emergency(serviceOrderRequest.isEmerengcy() ? 1 : 0);
        getCreateServiceOrder().setAddress(serviceOrderRequest.getFromAddress());
        getCreateServiceOrder().setLatitude(serviceOrderRequest.getFromLatitude());
        getCreateServiceOrder().setLongitude(serviceOrderRequest.getFromLongitude());
        getCreateServiceOrder().setToAddress(serviceOrderRequest.getToAddress());
        getCreateServiceOrder().setToLatitude(serviceOrderRequest.getToLatitude());
        getCreateServiceOrder().setToLongitude(serviceOrderRequest.getToLongitude());
        getCreateServiceOrder().setKiloCost(serviceOrderRequest.getKiloCost());

        if (!TextUtils.isEmpty(serviceOrderRequest.getDate()) && !TextUtils.isEmpty(serviceOrderRequest.getTime()))
            getCreateServiceOrder().setScheduled_at(serviceOrderRequest.getDate().concat(" ").concat(serviceOrderRequest.getTime()));
        if (!TextUtils.isEmpty(serviceOrderRequest.getTyreImage()))
            fileObjectList.add(new FileObject(Constants.IMAGE, serviceOrderRequest.getTyreImage(), Constants.FILE_TYPE_IMAGE));
        if (!TextUtils.isEmpty(serviceOrderRequest.getFrontOwnerImage()))
            fileObjectList.add(new FileObject(Constants.front_car_ownership, serviceOrderRequest.getFrontOwnerImage(), Constants.FILE_TYPE_IMAGE));
        if (!TextUtils.isEmpty(serviceOrderRequest.getBackOwnerImage()))
            fileObjectList.add(new FileObject(Constants.back_car_ownership, serviceOrderRequest.getBackOwnerImage(), Constants.FILE_TYPE_IMAGE));
        if (serviceOrderRequest.getFromLatitude() != 0.0) {
            setMessage(Constants.SHOW_PROGRESS);
            liveData.setValue(new Mutable(Constants.DISTANCE));
        } else { // only one location
            getCreateServiceOrder().setTotal(serviceOrderRequest.getTotal());
            getCreateServiceOrder().setDeliveryFees(serviceOrderRequest.getMovingPriceServices());
            getCreateServiceOrder().setTotal(AppHelper.convertToDecimal(getCreateServiceOrder().getTotal() + taxes())); // add new value after taxes
        }
    }

    public void calcTransferPrice() {
        float pricePerKilo = getCreateServiceOrder().getDistance() * getCreateServiceOrder().getKiloCost();
        getCreateServiceOrder().setTransferService((float) (pricePerKilo + (getServiceOrderRequest().getMovingPriceServices())));
        getCreateServiceOrder().setTotal(serviceOrderRequest.getTotal() + pricePerKilo); // add new value after distance
        getCreateServiceOrder().setTotal(AppHelper.convertToDecimal(getCreateServiceOrder().getTotal() + taxes())); // add new value after taxes
        setMessage(Constants.HIDE_PROGRESS);
        notifyChange(BR.createServiceOrder);
    }

    private double taxes() {
        getCreateServiceOrder().setTaxes(AppHelper.convertToDecimal(getCreateServiceOrder().getTotal() * (getServiceOrderRequest().getTaxes() / 100)));
        return getCreateServiceOrder().getTaxes();
    }

    @Bindable
    public CreateServiceOrder getCreateServiceOrder() {
        return this.createServiceOrder == null ? this.createServiceOrder = new CreateServiceOrder() : this.createServiceOrder;
    }

    public void onPaymentChange(RadioGroup radioGroup, int id) {
        getCreateServiceOrder().setPayType(id == R.id.radioOnHand ? "cash" : "online");
    }

    public ServicesRepository getPostRepository() {
        return postRepository;
    }

    protected void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        unSubscribeFromObservable();
    }

}
