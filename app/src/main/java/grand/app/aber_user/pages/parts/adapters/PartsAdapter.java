package grand.app.aber_user.pages.parts.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import grand.app.aber_user.PassingObject;
import grand.app.aber_user.R;
import grand.app.aber_user.databinding.ItemPartBinding;
import grand.app.aber_user.pages.parts.PartServicesFragment;
import grand.app.aber_user.pages.services.models.ServicesItem;
import grand.app.aber_user.pages.services.viewModels.ItemServicesViewModel;
import grand.app.aber_user.utils.Constants;
import grand.app.aber_user.utils.helper.MovementHelper;

public class PartsAdapter extends RecyclerView.Adapter<PartsAdapter.MenuView> {
    List<ServicesItem> servicesItemList;
    Context context;

    public PartsAdapter() {
        this.servicesItemList = new ArrayList<>();
    }

    @NonNull
    @Override
    public MenuView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_part,
                parent, false);
        this.context = parent.getContext();
        return new MenuView(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull final MenuView holder, final int position) {
        ServicesItem menuModel = servicesItemList.get(position);
        ItemServicesViewModel itemMenuViewModel = new ItemServicesViewModel(menuModel);
        itemMenuViewModel.getLiveData().observe((LifecycleOwner) context, o -> MovementHelper.startActivityWithBundle(context, new PassingObject(menuModel.getId(), Constants.PRODUCTS), menuModel.getName(), PartServicesFragment.class.getName(), null));
        holder.setViewModel(itemMenuViewModel);
    }

    public void update(@NotNull List<ServicesItem> dataList) {
        this.servicesItemList.clear();
        servicesItemList.addAll(dataList);
        notifyDataSetChanged();
    }

    @Override
    public void onViewAttachedToWindow(@NotNull MenuView holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NotNull MenuView holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    @Override
    public int getItemCount() {
        return servicesItemList.size();
    }

    public class MenuView extends RecyclerView.ViewHolder {
        public ItemPartBinding itemMenuBinding;

        MenuView(View itemView) {
            super(itemView);
            bind();
        }

        void bind() {
            if (itemMenuBinding == null) {
                itemMenuBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemMenuBinding != null) {
                itemMenuBinding.unbind();
            }
        }

        void setViewModel(ItemServicesViewModel itemViewModels) {
            if (itemMenuBinding != null) {
                itemMenuBinding.setItemViewModel(itemViewModels);
            }
        }
    }
}
