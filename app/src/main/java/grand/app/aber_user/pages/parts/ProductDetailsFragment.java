package grand.app.aber_user.pages.parts;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;

import com.google.gson.Gson;

import javax.inject.Inject;

import grand.app.aber_user.PassingObject;
import grand.app.aber_user.R;
import grand.app.aber_user.base.BaseFragment;
import grand.app.aber_user.base.IApplicationComponent;
import grand.app.aber_user.base.MyApplication;
import grand.app.aber_user.databinding.FragmentProductDetailsBinding;
import grand.app.aber_user.model.base.Mutable;
import grand.app.aber_user.pages.parts.models.productDetails.ProductDetailsResponse;
import grand.app.aber_user.pages.parts.viewModels.ProductDetailsViewModels;
import grand.app.aber_user.pages.reviews.ReviewsFragment;
import grand.app.aber_user.utils.Constants;
import grand.app.aber_user.utils.helper.AppHelper;
import grand.app.aber_user.utils.helper.MovementHelper;


public class ProductDetailsFragment extends BaseFragment {
    FragmentProductDetailsBinding binding;
    @Inject
    ProductDetailsViewModels viewModel;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_product_details, container, false);
        IApplicationComponent component = ((MyApplication) requireContext().getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setViewModel(viewModel);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            String passingObject = bundle.getString(Constants.BUNDLE);
            viewModel.setPassingObject(new Gson().fromJson(passingObject, PassingObject.class));
            viewModel.productDetails();
        }
        setEvent();
        return binding.getRoot();
    }


    private void setEvent() {
        viewModel.liveData.observe((LifecycleOwner) requireContext(), (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            Log.e("setEvent", "setEvent: " + mutable.message);
            viewModel.setMessage(mutable.message.equals(Constants.HIDE_PROGRESS) ? mutable.message : "");
            if (Constants.SERVICE_DETAILS.equals(((Mutable) o).message)) {
                viewModel.setProductDetails((((ProductDetailsResponse) (mutable).object).getData()));
                viewModel.setupSlider(binding.imageSlider);
            } else if (Constants.RATE_APP.equals(((Mutable) o).message)) {
                MovementHelper.startActivityWithBundle(requireActivity(), new PassingObject(viewModel.getPassingObject().getId()), getString(R.string.client_reviews), ReviewsFragment.class.getName(), null);
            } else if (Constants.PRODUCTS.equals(((Mutable) o).message)) {
                MovementHelper.startActivityWithBundle(requireActivity(), new PassingObject(viewModel.getProductDetails().getCompany().getId(), Constants.COMPANY), viewModel.getProductDetails().getCompany().getName(), PartServicesFragment.class.getName(), null);
            } else if (Constants.CART.equals(((Mutable) o).message)) {
                toastMessage(getString(R.string.add_to_cart_successfully));
            } else if (Constants.SHARE_BAR.equals(((Mutable) o).message)) {
                AppHelper.shareCustom(requireActivity(), viewModel.getProductDetails().getName(), viewModel.getProductDetails().getDescription());
            }
        });
        viewModel.getProductColorsAdapter().liveData.observe(requireActivity(), o -> viewModel.updateSizes());
    }

    @Override
    public void onResume() {
        viewModel.getServicesRepository().setLiveData(viewModel.liveData);
        if (Constants.DATA_CHANGED) {
            Constants.DATA_CHANGED = false;
            viewModel.productDetails();
        }
        super.onResume();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Constants.DATA_CHANGED = true;
    }
}
