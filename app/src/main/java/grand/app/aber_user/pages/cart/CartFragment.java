package grand.app.aber_user.pages.cart;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;

import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.util.Objects;

import javax.inject.Inject;

import grand.app.aber_user.BR;
import grand.app.aber_user.PassingObject;
import grand.app.aber_user.R;
import grand.app.aber_user.base.BaseFragment;
import grand.app.aber_user.base.IApplicationComponent;
import grand.app.aber_user.base.MyApplication;
import grand.app.aber_user.databinding.FragmentCartBinding;
import grand.app.aber_user.databinding.LowerBuyingWarningSheetBinding;
import grand.app.aber_user.model.base.Mutable;
import grand.app.aber_user.pages.cart.viewModels.CartViewModel;
import grand.app.aber_user.pages.parts.PartsFragment;
import grand.app.aber_user.utils.Constants;
import grand.app.aber_user.utils.helper.MovementHelper;
import grand.app.aber_user.utils.session.UserHelper;

public class CartFragment extends BaseFragment {
    FragmentCartBinding binding;
    @Inject
    CartViewModel viewModel;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_cart, container, false);
        IApplicationComponent component = ((MyApplication) requireActivity().getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setViewmodel(viewModel);
        setEvent();
        return binding.getRoot();
    }

    private void setEvent() {
        viewModel.liveData.observe(requireActivity(), (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            if (Constants.SERVICES.equals(((Mutable) o).message)) {
                MovementHelper.startActivityWithBundle(MovementHelper.unwrap(requireActivity()), new PassingObject(5), getString(R.string.continue_shopping), PartsFragment.class.getName(), null);
            } else if (Constants.CART.equals(((Mutable) o).message)) {
                if (Double.parseDouble(Objects.requireNonNull(viewModel.getCartTotal().getValue())) >= Double.parseDouble(UserHelper.getInstance(requireActivity()).getBuyingWarning()))
                    MovementHelper.startActivityForResultWithBundle(requireActivity(), new PassingObject(viewModel.getNewOrderRequest()), getString(R.string.continue_shopping), ConfirmCartFragment.class.getName(), Constants.ORDER_REQUEST);
                else
                    showBuyingWarning();
            }

        });
        viewModel.getCartLiveData().observe(requireActivity(), productDetails -> {
            viewModel.getCartAdapter().update(productDetails, false);
            viewModel.notifyChange(BR.cartAdapter);
        });
        viewModel.getCartTotal().observe(requireActivity(), s -> {
            if (s != null) {
                viewModel.cartTotalCurrency.set(getString(R.string.total_cart).concat(" ").concat(s).concat(" ").concat(viewModel.currency));
                viewModel.getNewOrderRequest().setTotalCart(s);
                viewModel.getNewOrderRequest().setCurrency(viewModel.currency);
                viewModel.notifyChange();
            } else
                viewModel.cartTotalCurrency.set("");
        });
        viewModel.getCartAdapter().getLiveDataAdapter().observe(requireActivity(), this::showAlertCancel);
    }

    private void showBuyingWarning() {
        LowerBuyingWarningSheetBinding sortBinding = DataBindingUtil.inflate(LayoutInflater.from(requireActivity()), R.layout.lower_buying_warning_sheet, null, false);
        BottomSheetDialog sheetDialog = new BottomSheetDialog(requireActivity(), R.style.BottomSheetDialogStyle);
        sheetDialog.setContentView(sortBinding.getRoot());
        sortBinding.setViewmodel(viewModel);
        sheetDialog.show();
    }

    @Override
    public void onResume() {
        viewModel.getServicesRepository().setLiveData(viewModel.liveData);
        super.onResume();
    }

    public void showAlertCancel(int productId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(requireActivity());
        builder
                .setMessage(getString(R.string.delete_title))
                .setCancelable(false)
                .setNegativeButton(getString(R.string.cancel), (dialog, which) -> {
                    viewModel.notifyChange();
                    dialog.cancel();
                })
                .setPositiveButton(getString(R.string.send), (dialog, id) -> {
                    viewModel.getCartRepository().deleteItem(productId);
                    dialog.cancel();
                });
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    public void launchActivityResult(int request, int resultCode, Intent result) {
        super.launchActivityResult(request, resultCode, result);
        if (result != null) {
            if (request == Constants.ORDER_REQUEST) {
                MovementHelper.finishWithResult(new PassingObject(), requireActivity(), Constants.ORDER_REQUEST);
            }
        }
    }
}
