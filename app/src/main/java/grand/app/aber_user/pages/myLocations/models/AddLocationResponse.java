package grand.app.aber_user.pages.myLocations.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import grand.app.aber_user.model.base.StatusMessage;

public class AddLocationResponse extends StatusMessage {
    @SerializedName("data")
    private LocationsData locationsData;

    public LocationsData getLocationsData() {
        return locationsData;
    }
}
