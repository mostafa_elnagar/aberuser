package grand.app.aber_user.pages.parts.models.productDetails;

import com.google.gson.annotations.SerializedName;

import grand.app.aber_user.model.base.StatusMessage;

public class ProductDetailsResponse extends StatusMessage {

    @SerializedName("data")
    private ProductDetails data;

    public ProductDetails getData() {
        return data;
    }

}