package grand.app.aber_user.pages.myOrders.viewModels;

import androidx.databinding.Bindable;
import androidx.databinding.ObservableBoolean;
import androidx.lifecycle.MutableLiveData;

import javax.inject.Inject;

import grand.app.aber_user.BR;
import grand.app.aber_user.base.BaseViewModel;
import grand.app.aber_user.model.base.Mutable;
import grand.app.aber_user.pages.myOrders.adapter.MyOrderProductsAdapter;
import grand.app.aber_user.pages.myOrders.adapter.MyOrdersAdapter;
import grand.app.aber_user.pages.myOrders.models.MyOrderDetails;
import grand.app.aber_user.pages.myOrders.models.MyOrdersMainData;
import grand.app.aber_user.repository.ServicesRepository;
import io.reactivex.disposables.CompositeDisposable;

public class MyOrdersViewModels extends BaseViewModel {

    public MutableLiveData<Mutable> liveData;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    @Inject
    ServicesRepository servicesRepository;
    public ObservableBoolean searchProgressVisible = new ObservableBoolean();
    MyOrdersMainData mainData;
    MyOrdersAdapter myOrdersAdapter;
    MyOrderDetails myOrderDetails;
    MyOrderProductsAdapter productsAdapter;

    @Inject
    public MyOrdersViewModels(ServicesRepository servicesRepository) {
        this.servicesRepository = servicesRepository;
        this.liveData = new MutableLiveData<>();
        servicesRepository.setLiveData(liveData);
    }

    public void myOrders(int page, boolean showProgress) {
        compositeDisposable.add(servicesRepository.myOrders(page, showProgress));
    }

    public void orderDetails() {
        compositeDisposable.add(servicesRepository.myOrderDetails(getPassingObject().getId()));
    }

    public void liveDataActions(String action) {
        liveData.setValue(new Mutable(action));
    }

    @Bindable
    public MyOrdersAdapter getMyOrdersAdapter() {
        return this.myOrdersAdapter == null ? this.myOrdersAdapter = new MyOrdersAdapter() : this.myOrdersAdapter;
    }

    @Bindable
    public MyOrderProductsAdapter getProductsAdapter() {
        return this.productsAdapter == null ? this.productsAdapter = new MyOrderProductsAdapter() : this.productsAdapter;
    }

    @Bindable
    public MyOrdersMainData getMainData() {
        return this.mainData == null ? this.mainData = new MyOrdersMainData() : this.mainData;
    }

    @Bindable
    public MyOrderDetails getMyOrderDetails() {
        return this.myOrderDetails == null ? this.myOrderDetails = new MyOrderDetails() : this.myOrderDetails;
    }

    public void setMyOrderDetails(MyOrderDetails myOrderDetails) {
        getProductsAdapter().update(myOrderDetails.getProducts());
        notifyChange(BR.productsAdapter);
        notifyChange(BR.myOrderDetails);
        this.myOrderDetails = myOrderDetails;
    }

    @Bindable
    public void setMainData(MyOrdersMainData mainData) {
        if (getMyOrdersAdapter().getMenuModels().size() > 0) {
            getMyOrdersAdapter().loadMore(mainData.getData());
        } else {
            getMyOrdersAdapter().update(mainData.getData());
            notifyChange(BR.myOrdersAdapter);
        }
        searchProgressVisible.set(false);
        notifyChange(BR.mainData);
        this.mainData = mainData;
    }

    public ServicesRepository getServicesRepository() {
        return servicesRepository;
    }

    protected void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        unSubscribeFromObservable();
    }

}
