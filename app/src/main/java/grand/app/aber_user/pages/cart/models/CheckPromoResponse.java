package grand.app.aber_user.pages.cart.models;

import com.google.gson.annotations.SerializedName;

import grand.app.aber_user.model.base.StatusMessage;


public class CheckPromoResponse extends StatusMessage {

    @SerializedName("data")
    private PromoData data;

    public PromoData getData() {
        return data;
    }
}