package grand.app.aber_user.pages.settings.viewModels;


import androidx.databinding.Bindable;

import grand.app.aber_user.base.BaseViewModel;
import grand.app.aber_user.pages.home.models.HomeServices;
import grand.app.aber_user.pages.settings.models.SocialMediaData;
import grand.app.aber_user.utils.Constants;

public class ItemSocialViewModel extends BaseViewModel {
    public SocialMediaData socialMediaData;

    public ItemSocialViewModel(SocialMediaData socialMediaData) {
        this.socialMediaData = socialMediaData;
    }

    @Bindable
    public SocialMediaData getSocialMediaData() {
        return socialMediaData;
    }

    public void itemAction() {
        getLiveData().setValue(Constants.MENu);
    }

}
