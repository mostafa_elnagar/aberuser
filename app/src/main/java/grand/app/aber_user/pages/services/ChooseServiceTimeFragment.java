package grand.app.aber_user.pages.services;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import javax.inject.Inject;
import grand.app.aber_user.PassingObject;
import grand.app.aber_user.R;
import grand.app.aber_user.base.BaseFragment;
import grand.app.aber_user.base.IApplicationComponent;
import grand.app.aber_user.base.MyApplication;
import grand.app.aber_user.databinding.FragmentChooseServiceTimeBinding;
import grand.app.aber_user.model.base.Mutable;
import grand.app.aber_user.pages.services.viewModels.ServicesViewModels;
import grand.app.aber_user.utils.Constants;
import grand.app.aber_user.utils.helper.AppHelper;
import grand.app.aber_user.utils.helper.MovementHelper;

public class ChooseServiceTimeFragment extends BaseFragment {
    @Inject
    ServicesViewModels viewModel;
    FragmentChooseServiceTimeBinding binding;
    private String time, date;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_choose_service_time, container, false);
        IApplicationComponent component = ((MyApplication) requireActivity().getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setViewModel(viewModel);
        setEvent();
        return binding.getRoot();
    }

    private void setEvent() {
        viewModel.liveData.observe(requireActivity(), (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            viewModel.setMessage(mutable.message.equals(Constants.HIDE_PROGRESS) ? mutable.message : "");
            if (((Mutable) o).message.equals(Constants.DELIVERY_TIME)) {
                toastMessage(getString(R.string.delivery_time_success));
                MovementHelper.finishWithResult(new PassingObject(date, time), requireActivity(), Constants.DELIVERY_TIME_REQUEST);
            }
        });
        binding.calendar.setMinDate(System.currentTimeMillis());
        date = AppHelper.selectDate(0, 0, 0, binding.calendar.getDate());
        time = AppHelper.selectTime(binding.timePicker.getHour(), binding.timePicker.getMinute());
        binding.dateNumber.setText(AppHelper.getDay(binding.calendar.getDate()));
        binding.dateText.setText(AppHelper.getDayText(binding.calendar.getDate()));
        binding.calendar.setOnDateChangeListener((view, year, month, dayOfMonth) -> date = AppHelper.selectDate(year, month, dayOfMonth, 0L));
        binding.timePicker.setOnTimeChangedListener((view, hourOfDay, minute) -> time = AppHelper.selectTime(hourOfDay, minute));
    }

    @Override
    public void onResume() {
        super.onResume();
        viewModel.getPostRepository().setLiveData(viewModel.liveData);
    }
}
