package grand.app.aber_user.pages.home.viewModels;


import androidx.databinding.Bindable;
import androidx.lifecycle.MutableLiveData;

import com.smarteist.autoimageslider.SliderView;

import javax.inject.Inject;

import grand.app.aber_user.BR;
import grand.app.aber_user.base.BaseViewModel;
import grand.app.aber_user.base.MyApplication;
import grand.app.aber_user.model.base.Mutable;
import grand.app.aber_user.pages.home.adapters.HomeServicesAdapter;
import grand.app.aber_user.pages.home.adapters.HomeSliderAdapter;
import grand.app.aber_user.pages.home.models.MainData;
import grand.app.aber_user.repository.ServicesRepository;
import grand.app.aber_user.utils.session.UserHelper;
import io.reactivex.disposables.CompositeDisposable;

public class HomeViewModels extends BaseViewModel {

    public MutableLiveData<Mutable> liveData;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    HomeServicesAdapter homeServicesAdapter;
    HomeSliderAdapter homeSliderAdapter;
    @Inject
    ServicesRepository postRepository;
    MainData mainData;

    @Inject
    public HomeViewModels(ServicesRepository postRepository) {
        mainData = new MainData();
        this.postRepository = postRepository;
        this.liveData = new MutableLiveData<>();
        postRepository.setLiveData(liveData);
    }

    public void home() {
        compositeDisposable.add(postRepository.getHome());
    }


    public void liveDataActions(String action) {
        liveData.setValue(new Mutable(action));
    }

    @Bindable
    public HomeServicesAdapter getHomeServicesAdapter() {
        return this.homeServicesAdapter == null ? this.homeServicesAdapter = new HomeServicesAdapter() : this.homeServicesAdapter;
    }

    @Bindable
    public HomeSliderAdapter getHomeSliderAdapter() {
        return this.homeSliderAdapter == null ? this.homeSliderAdapter = new HomeSliderAdapter() : this.homeSliderAdapter;
    }

    @Bindable
    public MainData getMainData() {
        return mainData;
    }

    @Bindable
    public void setMainData(MainData mainData) {
        getHomeSliderAdapter().updateData(mainData.getHomeSliderDataList());
        getHomeServicesAdapter().update(mainData.getHomeServicesList());
        notifyChange(BR.homeServicesAdapter);
        notifyChange(BR.homeSliderAdapter);
        notifyChange(BR.mainData);
        UserHelper.getInstance(MyApplication.getInstance()).addBuyingWarning(mainData.getMinimumPurchases());
        this.mainData = mainData;
    }

    public void setupSlider(SliderView sliderView) {
        sliderView.setSliderAdapter(homeSliderAdapter);
    }

    public ServicesRepository getPostRepository() {
        return postRepository;
    }

    protected void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        unSubscribeFromObservable();
    }

}
