package grand.app.aber_user.pages.myOrders;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;

import java.util.Objects;

import javax.inject.Inject;

import grand.app.aber_user.BR;
import grand.app.aber_user.PassingObject;
import grand.app.aber_user.R;
import grand.app.aber_user.base.BaseFragment;
import grand.app.aber_user.base.IApplicationComponent;
import grand.app.aber_user.base.MyApplication;
import grand.app.aber_user.databinding.CancelWarningLayoutBinding;
import grand.app.aber_user.databinding.FragmentOrderServiceDetailsBinding;
import grand.app.aber_user.databinding.RateDialogBinding;
import grand.app.aber_user.model.base.Mutable;
import grand.app.aber_user.model.base.StatusMessage;
import grand.app.aber_user.pages.chat.view.ChatFragment;
import grand.app.aber_user.pages.myOrders.models.orderServices.OrderServiceDetailsResponse;
import grand.app.aber_user.pages.myOrders.models.orderServices.generateCode.GenerateCodeResponse;
import grand.app.aber_user.pages.myOrders.viewModels.MyOrdersServicesViewModels;
import grand.app.aber_user.utils.Constants;
import grand.app.aber_user.utils.helper.AppHelper;
import grand.app.aber_user.utils.helper.MovementHelper;

public class MyServiceOrderDetailsFragment extends BaseFragment {
    @Inject
    MyOrdersServicesViewModels viewModel;
    FragmentOrderServiceDetailsBinding binding;
    BottomSheetDialog sheetDialog;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_order_service_details, container, false);
        IApplicationComponent component = ((MyApplication) requireActivity().getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setViewmodel(viewModel);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            String passingObject = bundle.getString(Constants.BUNDLE);
            viewModel.setPassingObject(new Gson().fromJson(passingObject, PassingObject.class));
            viewModel.orderDetails();
        }
        setEvent();
        return binding.getRoot();
    }

    private void setEvent() {
        viewModel.liveData.observe(requireActivity(), (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            viewModel.setMessage(mutable.message.equals(Constants.HIDE_PROGRESS) ? mutable.message : "");
            if (Constants.My_ORDER_DETAILS.equals(((Mutable) o).message)) {
                viewModel.setMyOrderDetails(((OrderServiceDetailsResponse) mutable.object).getOrderDetailsMain());
                if (viewModel.getMyOrderDetails().getOrders().getHistory() == 4 || viewModel.getMyOrderDetails().getOrders().getCanceled() == 1) {
                    baseActivity().backActionBarView.layoutActionBarBackBinding.tvActionBarCancel.setVisibility(View.INVISIBLE);
                } else
                    baseActivity().backActionBarView.layoutActionBarBackBinding.tvActionBarCancel.setVisibility(View.VISIBLE);
            } else if (Constants.CANCEL_SERVICE.equals(((Mutable) o).message)) {
                toastMessage(((StatusMessage) mutable.object).mMessage);
                finishActivity();
                Constants.DATA_CHANGED = true;
            } else if (Constants.GENERATE_CODE.equals(((Mutable) o).message)) {
                viewModel.getMyOrderDetails().getOrders().setFinishCode(((GenerateCodeResponse) mutable.object).getGenerateCodeData().getCode());
                viewModel.notifyChange(BR.myOrderDetails);
            } else if (Constants.CALL.equals(((Mutable) o).message)) {
                AppHelper.openDialNumber(requireActivity(), viewModel.getMyOrderDetails().getOrders().getProvider().getPhone());
            } else if (Constants.FOLLOW_ORDER.equals(((Mutable) o).message)) {
                MovementHelper.startActivityWithBundle(requireActivity(), new PassingObject(viewModel.getMyOrderDetails()), getString(R.string.follow_provider), FollowUpOrderFragment.class.getName(), null);
            } else if (Constants.CHAT.equals(((Mutable) o).message)) {
                MovementHelper.startActivityWithBundle(requireActivity(), new PassingObject(viewModel.getMyOrderDetails().getOrders().getProvider()), null, ChatFragment.class.getName(), null);
            } else if (Constants.RATE_APP.equals(((Mutable) o).message)) {
                showRateDialog();
            } else if (Constants.RATE_SERVICE_PROVIDER.equals(((Mutable) o).message)) {
                toastMessage(((StatusMessage) mutable.object).mMessage);
                viewModel.updateReview();
                sheetDialog.dismiss();
            }
        });
        baseActivity().backActionBarView.layoutActionBarBackBinding.tvActionBarCancel.setOnClickListener(v -> {
            if (viewModel.getMyOrderDetails().getOrders().getHistory() == 2) {
                cancelDialog(2);
            } else
                cancelDialog(0);
        });
    }

    public void cancelDialog(int status) {
        Dialog dialog = new Dialog(requireActivity(), R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Objects.requireNonNull(dialog.getWindow()).getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        CancelWarningLayoutBinding binding = DataBindingUtil.inflate(LayoutInflater.from(dialog.getContext()), R.layout.cancel_warning_layout, null, false);
        dialog.setContentView(binding.getRoot());
        if (status == 0)
            binding.logoutTxt1.setText(getString(R.string.cancel_warning_not_value));
        binding.agree.setOnClickListener(v -> {
            dialog.dismiss();
            viewModel.cancelOrder();
        });
        binding.decline.setOnClickListener(v -> dialog.dismiss());
        dialog.show();
    }

    public void showRateDialog() {
        RateDialogBinding sortBinding = DataBindingUtil.inflate(LayoutInflater.from(requireActivity()), R.layout.rate_dialog, null, false);
        sheetDialog = new BottomSheetDialog(requireActivity(), R.style.BottomSheetDialogStyle);
        sortBinding.setViewmodel(viewModel);
        sheetDialog.setContentView(sortBinding.getRoot());
        sheetDialog.show();
    }

    @Override
    public void onResume() {
        super.onResume();
        viewModel.getServicesRepository().setLiveData(viewModel.liveData);
        baseActivity().enableRefresh(false);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (viewModel.getMyOrderDetails().getOrders() != null && viewModel.getMyOrderDetails().getOrders().getHistory() == 4)
            Constants.DATA_CHANGED = true;
    }
}
