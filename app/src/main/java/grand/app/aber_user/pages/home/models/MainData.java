package grand.app.aber_user.pages.home.models;

import java.util.List;

import com.google.gson.annotations.SerializedName;

import grand.app.aber_user.pages.settings.models.SocialMediaData;

public class MainData {

    @SerializedName("services")
    private List<HomeServices> homeServicesList;
    @SerializedName("slider")
    private List<HomeSliderData> homeSliderDataList;
    @SerializedName("social_media")
    private List<SocialMediaData> socialMediaDataList;
    @SerializedName("minimum_purchases")
    private String minimumPurchases;

    public List<HomeServices> getHomeServicesList() {
        return homeServicesList;
    }

    public List<HomeSliderData> getHomeSliderDataList() {
        return homeSliderDataList;
    }

    public List<SocialMediaData> getSocialMediaDataList() {
        return socialMediaDataList;
    }

    public String getMinimumPurchases() {
        return minimumPurchases;
    }
}