package grand.app.aber_user.pages.myOrders.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import grand.app.aber_user.R;
import grand.app.aber_user.databinding.ItemOrderServiceBinding;
import grand.app.aber_user.pages.home.models.HomeServices;
import grand.app.aber_user.pages.home.viewModels.ItemMainViewModel;
import grand.app.aber_user.utils.resources.ResourceManager;

public class HomeServicesAdapter extends RecyclerView.Adapter<HomeServicesAdapter.MenuView> {
    List<HomeServices> postDataList;
    private Context context;
    public int lastSelected = -1;
    public MutableLiveData<Integer> liveData = new MutableLiveData<>();

    public HomeServicesAdapter() {
        this.postDataList = new ArrayList<>();
    }

    @NonNull
    @Override
    public MenuView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_order_service,
                parent, false);
        this.context = parent.getContext();
        return new MenuView(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull final MenuView holder, @SuppressLint("RecyclerView") final int position) {
        HomeServices menuModel = postDataList.get(position);
        ItemMainViewModel itemMenuViewModel = new ItemMainViewModel(menuModel);
        itemMenuViewModel.getLiveData().observe(((LifecycleOwner) context), o -> {
            this.lastSelected = menuModel.getId();
            lastSelected = menuModel.getId();
            liveData.setValue(menuModel.getId());
            notifyDataSetChanged();
        });
        if (lastSelected == menuModel.getId()) {
            holder.itemMenuBinding.sortBtn.setBackgroundColor(ResourceManager.getColor(R.color.colorPrimary));
            holder.itemMenuBinding.sortBtn.setTextColor(ResourceManager.getColor(R.color.white));
        } else {
            holder.itemMenuBinding.sortBtn.setBackgroundColor(ResourceManager.getColor(R.color.white));
            holder.itemMenuBinding.sortBtn.setTextColor(ResourceManager.getColor(R.color.colorPrimaryDark));
        }
        holder.setViewModel(itemMenuViewModel);
    }

    public void update(List<HomeServices> dataList) {
        HomeServices homeServices = new HomeServices();
        homeServices.setId(0);
        homeServices.setName(ResourceManager.getString(R.string.all));
        dataList.add(0, homeServices);
        if (lastSelected == -1)
            lastSelected = 0; // for select first item
        this.postDataList.clear();
        postDataList.addAll(dataList);
        notifyDataSetChanged();
    }

    @Override
    public void onViewAttachedToWindow(@NotNull MenuView holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NotNull MenuView holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    @Override
    public int getItemCount() {
        return postDataList.size();
    }

    public class MenuView extends RecyclerView.ViewHolder {
        public ItemOrderServiceBinding itemMenuBinding;

        MenuView(View itemView) {
            super(itemView);
            bind();
        }

        void bind() {
            if (itemMenuBinding == null) {
                itemMenuBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemMenuBinding != null) {
                itemMenuBinding.unbind();
            }
        }

        void setViewModel(ItemMainViewModel itemViewModels) {
            if (itemMenuBinding != null) {
                itemMenuBinding.setItemViewModel(itemViewModels);
            }
        }
    }
}
