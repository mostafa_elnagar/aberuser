package grand.app.aber_user.pages.myLocations;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;

import com.google.gson.Gson;

import javax.inject.Inject;

import grand.app.aber_user.BR;
import grand.app.aber_user.PassingObject;
import grand.app.aber_user.R;
import grand.app.aber_user.base.BaseFragment;
import grand.app.aber_user.base.IApplicationComponent;
import grand.app.aber_user.base.MyApplication;
import grand.app.aber_user.databinding.FragmentAddPlaceBinding;
import grand.app.aber_user.model.base.Mutable;
import grand.app.aber_user.model.base.StatusMessage;
import grand.app.aber_user.pages.auth.countries.models.CountriesResponse;
import grand.app.aber_user.pages.myLocations.models.AddLocationResponse;
import grand.app.aber_user.pages.myLocations.models.LocationsData;
import grand.app.aber_user.pages.myLocations.viewModels.MyLocationsViewModel;
import grand.app.aber_user.utils.Constants;
import grand.app.aber_user.utils.PopUp.PopUpMenuHelper;
import grand.app.aber_user.utils.helper.MovementHelper;


public class AddPlaceFragment extends BaseFragment {
    FragmentAddPlaceBinding binding;
    @Inject
    MyLocationsViewModel viewModel;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_add_place, container, false);
        IApplicationComponent component = ((MyApplication) requireActivity().getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setViewModel(viewModel);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            String passingObject = bundle.getString(Constants.BUNDLE);
            viewModel.setPassingObject(new Gson().fromJson(passingObject, PassingObject.class));
            if (viewModel.getPassingObject().getObjectClass() != null) {
                viewModel.setLocationsData(new Gson().fromJson(String.valueOf(viewModel.getPassingObject().getObjectClass()), LocationsData.class));
            }
            viewModel.getCountries();
        }
        setEvent();
        return binding.getRoot();
    }

    private void setEvent() {
        viewModel.liveData.observe(requireActivity(), (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            if (Constants.CITIES.equals(((Mutable) o).message)) {
                viewModel.cityList = ((CountriesResponse) (mutable).object).getCountriesDataList();
            } else if (Constants.CITY.equals(((Mutable) o).message)) {
                showCities();
            } else if (Constants.COUNTRIES.equals(((Mutable) o).message)) {
                viewModel.countriesDataList = ((CountriesResponse) (mutable).object).getCountriesDataList();
            } else if (Constants.SHOW_COUNTRY.equals(((Mutable) o).message)) {
                showCountry();
            } else if (Constants.GOVERNS.equals(((Mutable) o).message)) {
                viewModel.governList = ((CountriesResponse) (mutable).object).getCountriesDataList();
            } else if (Constants.GOVERN.equals(((Mutable) o).message)) {
                showGovern();
            } else if (Constants.ADD_PLACE.equals(((Mutable) o).message)) {
                toastMessage(((AddLocationResponse) mutable.object).mMessage);
                MovementHelper.finishWithResult(new PassingObject(((AddLocationResponse) mutable.object).getLocationsData()), requireActivity(), Constants.LOCATION_REQUEST);
            }
        });
    }

    private void showGovern() {
        PopUpMenuHelper.showGovernPopUp(requireActivity(), binding.inputGovern, viewModel.governList).setOnMenuItemClickListener(item -> {
            viewModel.getAddLocationRequest().setGovernId(String.valueOf(viewModel.governList.get(item.getItemId()).getId()));
            binding.govern.setText(viewModel.governList.get(item.getItemId()).getName());
            viewModel.getCities(viewModel.governList.get(item.getItemId()).getId());
            viewModel.notifyChange(BR.addLocationRequest);
            return false;
        });
    }

    private void showCountry() {
        PopUpMenuHelper.showCountriesPopUp(requireActivity(), binding.inputCountry, viewModel.countriesDataList).setOnMenuItemClickListener(item -> {
            viewModel.getAddLocationRequest().setCountryId(String.valueOf(viewModel.countriesDataList.get(item.getItemId()).getId()));
            binding.country.setText(viewModel.countriesDataList.get(item.getItemId()).getName());
            viewModel.getGoverns(viewModel.countriesDataList.get(item.getItemId()).getId());
            viewModel.notifyChange(BR.addLocationRequest);
            return false;
        });
    }

    private void showCities() {
        PopUpMenuHelper.showCityPopUp(requireActivity(), binding.inputCity, viewModel.cityList).setOnMenuItemClickListener(item -> {
            viewModel.getAddLocationRequest().setCity_id(String.valueOf(viewModel.cityList.get(item.getItemId()).getId()));
            binding.location.setText(viewModel.cityList.get(item.getItemId()).getName());
            viewModel.notifyChange(BR.addLocationRequest);
            return false;
        });
    }


    @Override
    public void onResume() {
        super.onResume();
        viewModel.getRepository().setLiveData(viewModel.liveData);
    }


}