package grand.app.aber_user.pages.appWallet.models;

import com.google.gson.annotations.SerializedName;

import grand.app.aber_user.model.base.StatusMessage;


public class RaiseWalletResponse extends StatusMessage {

    @SerializedName("data")
    private WalletHistoryItem data;

    public WalletHistoryItem getData() {
        return data;
    }

}