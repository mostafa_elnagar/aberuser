package grand.app.aber_user.pages.home.models;

import com.google.gson.annotations.SerializedName;

import grand.app.aber_user.model.base.StatusMessage;

public class NewPostResponse extends StatusMessage {
    @SerializedName("data")
    private HomeServices postData;

    public HomeServices getPostData() {
        return postData;
    }
}
