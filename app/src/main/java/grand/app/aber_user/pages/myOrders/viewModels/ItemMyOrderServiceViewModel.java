package grand.app.aber_user.pages.myOrders.viewModels;


import androidx.databinding.Bindable;

import grand.app.aber_user.BR;
import grand.app.aber_user.R;
import grand.app.aber_user.base.BaseViewModel;
import grand.app.aber_user.pages.myOrders.adapter.ServicesRequiredAdapter;
import grand.app.aber_user.pages.myOrders.models.orderServices.ServicesOrderDetails;
import grand.app.aber_user.utils.Constants;
import grand.app.aber_user.utils.resources.ResourceManager;

public class ItemMyOrderServiceViewModel extends BaseViewModel {
    public ServicesOrderDetails productsItem;
    public String status;
    ServicesRequiredAdapter servicesRequiredAdapter;

    public ItemMyOrderServiceViewModel(ServicesOrderDetails productsItem) {
        this.productsItem = productsItem;
        if (productsItem.getCanceled() == 1) {
            status = ResourceManager.getString(R.string.cancel_service);
        } else {
            if (productsItem.getHistory() == 0)
                status = ResourceManager.getString(R.string.service_not_accepted);
            else if (productsItem.getHistory() == 1)
                status = ResourceManager.getString(R.string.order_accepted);
            else if (productsItem.getHistory() == 2)
                status = ResourceManager.getString(R.string.on_way);
            else if (productsItem.getHistory() == 3)
                status = ResourceManager.getString(R.string.order_arrived);
            else if (productsItem.getHistory() == 4)
                status = ResourceManager.getString(R.string.order_finished);
            else if (productsItem.getHistory() == 5)
                status = ResourceManager.getString(R.string.order_not_finished);
        }
        getServicesRequiredAdapter().update(productsItem.getSubServices());
        notifyChange(BR.servicesRequiredAdapter);
    }

    @Bindable
    public ServicesOrderDetails getProductsItem() {
        return productsItem;
    }

    @Bindable
    public ServicesRequiredAdapter getServicesRequiredAdapter() {
        return this.servicesRequiredAdapter == null ? this.servicesRequiredAdapter = new ServicesRequiredAdapter() : this.servicesRequiredAdapter;
    }

    public void itemAction() {
        getLiveData().setValue(Constants.DELETE);
    }
}
