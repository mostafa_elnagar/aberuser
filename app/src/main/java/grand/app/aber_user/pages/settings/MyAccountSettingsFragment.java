package grand.app.aber_user.pages.settings;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;

import grand.app.aber_user.PassingObject;
import grand.app.aber_user.databinding.FragmentMyAccountSettingsBinding;
import grand.app.aber_user.model.base.Mutable;
import grand.app.aber_user.model.base.StatusMessage;
import grand.app.aber_user.pages.appWallet.AppWalletFragment;
import grand.app.aber_user.pages.auth.models.UsersResponse;
import grand.app.aber_user.pages.myLocations.MyLocationsFragment;
import grand.app.aber_user.pages.myOrders.MyOrdersFragment;
import grand.app.aber_user.pages.myOrders.MyServicesOrdersFragment;
import grand.app.aber_user.pages.profile.EditProfileFragment;
import grand.app.aber_user.pages.settings.viewModels.MyAccountSettingsViewModel;
import grand.app.aber_user.R;
import grand.app.aber_user.base.BaseFragment;
import grand.app.aber_user.base.IApplicationComponent;
import grand.app.aber_user.base.MyApplication;
import grand.app.aber_user.pages.myOrders.MyServiceOrderDetailsFragment;
import grand.app.aber_user.utils.Constants;
import grand.app.aber_user.utils.URLS;
import grand.app.aber_user.utils.helper.MovementHelper;
import grand.app.aber_user.utils.resources.ResourceManager;
import grand.app.aber_user.utils.session.UserHelper;

public class MyAccountSettingsFragment extends BaseFragment {

    private Context context;
    @Inject
    MyAccountSettingsViewModel viewModel;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FragmentMyAccountSettingsBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_my_account_settings, container, false);
        IApplicationComponent component = ((MyApplication) context.getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setViewmodel(viewModel);
        setEvent();
        return binding.getRoot();
    }

    private void setEvent() {
        viewModel.liveData.observe((LifecycleOwner) context, (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            switch (((Mutable) o).message) {
                case Constants.My_ORDERS:
                    MovementHelper.startActivity(context, MyOrdersFragment.class.getName(), getResources().getString(R.string.my_orders), null);
                    break;
                case Constants.ADDRESS:
                    MovementHelper.startActivity(context, MyLocationsFragment.class.getName(), getResources().getString(R.string.my_address), null);
                    break;
                case Constants.PROFILE:
                    MovementHelper.startActivity(context, EditProfileFragment.class.getName(), getString(R.string.edit_account), null);
                    break;
                case Constants.UPDATE_PROFILE:
                    toastMessage(((StatusMessage) mutable.object).mMessage);
                    UserHelper.getInstance(context).userLogin(((UsersResponse) ((Mutable) o).object).getData());
                    break;
                case Constants.LANGUAGE:
                    MovementHelper.startActivity(context, LangFragment.class.getName(), getString(R.string.lang), null);
                    break;
                case Constants.SERVICES:
                    MovementHelper.startActivity(context, MyServicesOrdersFragment.class.getName(), ResourceManager.getString(R.string.my_services), null);
                    break;
                case Constants.WALLET:
                    MovementHelper.startActivity(context, AppWalletFragment.class.getName(), getString(R.string.my_wallet), null);
                    break;
                case Constants.SHOW_LOGOUT_WARNING:
                    getActivityBase().exitDialog(getString(R.string.logout_warning));
                    break;

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
//        ((MainActivity) context).enableRefresh(false);
        viewModel.getRepository().setLiveData(viewModel.liveData);
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        this.context = context;
    }
}
