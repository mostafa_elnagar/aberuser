package grand.app.aber_user.pages.myOrders.models.orderServices;

import java.util.List;

import com.google.gson.annotations.SerializedName;

import grand.app.aber_user.model.PaginateMain;

public class Orders extends PaginateMain {

    @SerializedName("data")
    private List<ServicesOrderDetails> data;

    public List<ServicesOrderDetails> getData() {
        return data;
    }

}