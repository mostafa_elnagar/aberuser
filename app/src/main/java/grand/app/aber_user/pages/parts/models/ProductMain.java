package grand.app.aber_user.pages.parts.models;

import com.google.gson.annotations.SerializedName;

public class ProductMain {

	@SerializedName("products_count")
	private String productsCount;

	@SerializedName("products")
	private Products products;

	public String getProductsCount(){
		return productsCount;
	}

	public Products getProducts(){
		return products;
	}
}