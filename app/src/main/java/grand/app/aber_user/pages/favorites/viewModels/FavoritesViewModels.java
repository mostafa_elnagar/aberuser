package grand.app.aber_user.pages.favorites.viewModels;

import androidx.databinding.Bindable;
import androidx.databinding.ObservableBoolean;
import androidx.lifecycle.MutableLiveData;

import javax.inject.Inject;

import grand.app.aber_user.base.BaseViewModel;
import grand.app.aber_user.model.base.Mutable;
import grand.app.aber_user.pages.favorites.adapters.FavoritesAdapter;
import grand.app.aber_user.pages.parts.adapters.PartProductsAdapter;
import grand.app.aber_user.repository.ServicesRepository;
import io.reactivex.disposables.CompositeDisposable;

public class FavoritesViewModels extends BaseViewModel {

    public MutableLiveData<Mutable> liveData;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    FavoritesAdapter productsAdapter;
    @Inject
    ServicesRepository postRepository;
    public ObservableBoolean searchProgressVisible = new ObservableBoolean();

    @Inject
    public FavoritesViewModels(ServicesRepository postRepository) {
        this.postRepository = postRepository;
        this.liveData = new MutableLiveData<>();
        postRepository.setLiveData(liveData);
    }

    public void getFavorites() {
        compositeDisposable.add(postRepository.getFavorites());
    }

    public void changeLike(int productId) {
        compositeDisposable.add(postRepository.changeLike(productId));
    }

    public void liveDataActions(String action) {
        liveData.setValue(new Mutable(action));
    }

    @Bindable
    public FavoritesAdapter getProductsAdapter() {
        return this.productsAdapter == null ? this.productsAdapter = new FavoritesAdapter() : this.productsAdapter;
    }


    public ServicesRepository getPostRepository() {
        return postRepository;
    }

    protected void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        unSubscribeFromObservable();
    }

}
