package grand.app.aber_user.pages.parts.models;

import com.google.gson.annotations.SerializedName;

import grand.app.aber_user.model.base.StatusMessage;

public class ProductsResponse extends StatusMessage {
    @SerializedName("data")
    private ProductMain productMain;

    public ProductMain getProductMain() {
        return productMain;
    }
}