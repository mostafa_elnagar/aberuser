package grand.app.aber_user.pages.parts.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import grand.app.aber_user.R;
import grand.app.aber_user.databinding.ItemSizeBinding;
import grand.app.aber_user.pages.parts.models.productDetails.SizesItem;
import grand.app.aber_user.pages.parts.viewModels.ItemProductColorSizeViewModel;
import grand.app.aber_user.utils.resources.ResourceManager;

public class ProductColorSizesAdapter extends RecyclerView.Adapter<ProductColorSizesAdapter.MenuView> {
    List<SizesItem> sizesItemList;
    Context context;
    public int lastSelected = -1, lastPosition = -1;

    public ProductColorSizesAdapter() {
        this.sizesItemList = new ArrayList<>();
    }

    @NonNull
    @Override
    public MenuView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_size,
                parent, false);
        this.context = parent.getContext();
        return new MenuView(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull final MenuView holder, @SuppressLint("RecyclerView") final int position) {
        SizesItem menuModel = sizesItemList.get(position);
        ItemProductColorSizeViewModel itemMenuViewModel = new ItemProductColorSizeViewModel(menuModel);
        itemMenuViewModel.getLiveData().observe((LifecycleOwner) context, o -> {
            lastPosition = position;
            lastSelected = menuModel.getId();
            notifyDataSetChanged();
        });
        holder.itemMenuBinding.size.setBackground(lastSelected == menuModel.getId() ? ResourceManager.getDrawable(R.drawable.corner_view_primary_border) : ResourceManager.getDrawable(R.drawable.corner_view_gray_borde));
        holder.setViewModel(itemMenuViewModel);
    }

    public void update(@NotNull List<SizesItem> dataList) {
        this.sizesItemList.clear();
        sizesItemList.addAll(dataList);
        lastSelected = dataList.size() > 0 ? dataList.get(0).getId() : -1;
        notifyDataSetChanged();
    }

    @Override
    public void onViewAttachedToWindow(@NotNull MenuView holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NotNull MenuView holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    @Override
    public int getItemCount() {
        return sizesItemList.size();
    }

    public class MenuView extends RecyclerView.ViewHolder {
        public ItemSizeBinding itemMenuBinding;

        MenuView(View itemView) {
            super(itemView);
            bind();
        }

        void bind() {
            if (itemMenuBinding == null) {
                itemMenuBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemMenuBinding != null) {
                itemMenuBinding.unbind();
            }
        }

        void setViewModel(ItemProductColorSizeViewModel itemViewModels) {
            if (itemMenuBinding != null) {
                itemMenuBinding.setItemViewModel(itemViewModels);
            }
        }
    }
}
