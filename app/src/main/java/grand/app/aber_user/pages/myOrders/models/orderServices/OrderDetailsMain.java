package grand.app.aber_user.pages.myOrders.models.orderServices;

import com.google.gson.annotations.SerializedName;

public class OrderDetailsMain {
    @SerializedName("cancel_fees")
    private double cancelFees;
    @SerializedName("order")
    private ServicesOrderDetails orders;

    public double getCancelFees() {
        return cancelFees;
    }

    public ServicesOrderDetails getOrders() {
        return orders;
    }
}
