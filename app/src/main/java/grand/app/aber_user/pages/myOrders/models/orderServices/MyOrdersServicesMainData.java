package grand.app.aber_user.pages.myOrders.models.orderServices;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import grand.app.aber_user.pages.home.models.HomeServices;

public class MyOrdersServicesMainData {

	@SerializedName("orders")
	private Orders orders;

	@SerializedName("main_services")
	private List<HomeServices> mainServices;

	public Orders getOrders(){
		return orders;
	}

	public List<HomeServices> getMainServices(){
		return mainServices;
	}
}