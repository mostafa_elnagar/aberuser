package grand.app.aber_user.pages.parts;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;

import com.google.gson.Gson;

import javax.inject.Inject;

import grand.app.aber_user.PassingObject;
import grand.app.aber_user.R;
import grand.app.aber_user.base.BaseFragment;
import grand.app.aber_user.base.IApplicationComponent;
import grand.app.aber_user.base.MyApplication;
import grand.app.aber_user.databinding.FragmentPartsBinding;
import grand.app.aber_user.model.base.Mutable;
import grand.app.aber_user.model.base.StatusMessage;
import grand.app.aber_user.pages.parts.viewModels.PartsViewModels;
import grand.app.aber_user.pages.services.ChooseServiceTimeFragment;
import grand.app.aber_user.pages.services.models.ServiceDetailsResponse;
import grand.app.aber_user.utils.Constants;
import grand.app.aber_user.utils.helper.AppHelper;
import grand.app.aber_user.utils.helper.MovementHelper;

public class PartsFragment extends BaseFragment {
    @Inject
    PartsViewModels viewModel;
    FragmentPartsBinding binding;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_parts, container, false);
        IApplicationComponent component = ((MyApplication) requireActivity().getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setViewModel(viewModel);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            String passingObject = bundle.getString(Constants.BUNDLE);
            viewModel.setPassingObject(new Gson().fromJson(passingObject, PassingObject.class));
            viewModel.getServiceDetails();
        }
        setEvent();
        return binding.getRoot();
    }

    private void setEvent() {
        viewModel.liveData.observe(requireActivity(), (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            switch (((Mutable) o).message) {
                case Constants.SERVICE_DETAILS:
                    viewModel.setDetails(((ServiceDetailsResponse) (mutable).object).getData());
                    break;
                case Constants.SEARCH_LOCATION:
                    MovementHelper.startMapActivityForResultWithBundle(requireActivity(), new PassingObject(), getResources().getString(R.string.detect_location), Constants.LOCATION_REQUEST);
                    break;
                case Constants.DELIVERY_TIME:
                    MovementHelper.startActivityForResultWithBundle(requireActivity(), new PassingObject(), null, ChooseServiceTimeFragment.class.getName(), Constants.DELIVERY_TIME_REQUEST);
                    break;
                case Constants.SHARE_POST:
                    toastMessage(((StatusMessage) mutable.object).mMessage);
                    AppHelper.makeActionSound(requireActivity(), Constants.SHARE_POST);
                    break;

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        viewModel.getPostRepository().setLiveData(viewModel.liveData);
    }

}
