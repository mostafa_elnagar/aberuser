package grand.app.aber_user.pages.services;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;

import com.google.gson.Gson;

import javax.inject.Inject;

import grand.app.aber_user.BR;
import grand.app.aber_user.PassingObject;
import grand.app.aber_user.R;
import grand.app.aber_user.base.BaseFragment;
import grand.app.aber_user.base.IApplicationComponent;
import grand.app.aber_user.base.MyApplication;
import grand.app.aber_user.databinding.FragmentFuelBinding;
import grand.app.aber_user.model.DropDownResponse;
import grand.app.aber_user.model.base.Mutable;
import grand.app.aber_user.pages.services.models.ServiceDetailsResponse;
import grand.app.aber_user.pages.services.viewModels.ServicesViewModels;
import grand.app.aber_user.utils.Constants;
import grand.app.aber_user.utils.PopUp.PopUpMenuHelper;
import grand.app.aber_user.utils.helper.MovementHelper;


public class FuelFragment extends BaseFragment {
    @Inject
    ServicesViewModels viewModel;
    FragmentFuelBinding binding;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_fuel, container, false);
        IApplicationComponent component = ((MyApplication) requireActivity().getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setViewModel(viewModel);
        binding.searchLocation.setIconTint(null);
        binding.picTime.setIconTint(null);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            String passingObject = bundle.getString(Constants.BUNDLE);
            viewModel.setPassingObject(new Gson().fromJson(passingObject, PassingObject.class));
            viewModel.getServiceDetails();
        }
        setEvent();
        return binding.getRoot();
    }

    private void setEvent() {
        viewModel.liveData.observe(requireActivity(), (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            viewModel.setMessage(mutable.message.equals(Constants.HIDE_PROGRESS) ? mutable.message : "");
            switch (((Mutable) o).message) {
                case Constants.SERVICE_DETAILS:
                    viewModel.setDetails(((ServiceDetailsResponse) (mutable).object).getData());
                    break;
                case Constants.FUEL_CAT_REQUEST:
                    viewModel.fuelCatList = ((DropDownResponse) mutable.object).getDropDownsObjects();
                    break;
                case Constants.FUEL_TYPE:
                    showFuelType();
                    break;
                case Constants.FUEL_CAT:
                    showFuelCategories();
                    break;
                case Constants.SEARCH_LOCATION:
                    MovementHelper.startMapActivityForResultWithBundle(requireActivity(), new PassingObject(), getResources().getString(R.string.detect_location), Constants.LOCATION_REQUEST);
                    break;
                case Constants.DELIVERY_TIME:
                    MovementHelper.startActivityForResultWithBundle(requireActivity(), new PassingObject(), null, ChooseServiceTimeFragment.class.getName(), Constants.DELIVERY_TIME_REQUEST);
                    break;
                case Constants.ORDER:
                    MovementHelper.startActivityForResultWithBundle(requireActivity(), new PassingObject(viewModel.getServiceOrderRequest()), null, FragmentConfirmOrder.class.getName(), Constants.ORDER_REQUEST);
                    break;

            }
        });
    }

    private void showFuelType() {
        PopUpMenuHelper.showCarTypePopUp(requireActivity(), binding.inputFuelTypes, viewModel.getDetails().getFuelList()).setOnMenuItemClickListener(item -> {
            viewModel.getServiceOrderRequest().setFuelType(viewModel.getDetails().getFuelList().get(item.getItemId()).getName());
            viewModel.getServiceOrderRequest().setFuelTypeId(viewModel.getDetails().getFuelList().get(item.getItemId()).getId());
            viewModel.getFuelData(viewModel.getDetails().getFuelList().get(item.getItemId()).getId());
            viewModel.notifyChange(BR.serviceOrderRequest);
            return false;
        });
    }

    private void showFuelCategories() {
        PopUpMenuHelper.showCarTypePopUp(requireActivity(), binding.inputFuelCategories, viewModel.fuelCatList).setOnMenuItemClickListener(item -> {
            viewModel.getServiceOrderRequest().setFuelCat(viewModel.fuelCatList.get(item.getItemId()).getName());
            viewModel.getServiceOrderRequest().setFuelCatId(viewModel.fuelCatList.get(item.getItemId()).getId());
            viewModel.getServiceOrderRequest().setTotalServices(Double.parseDouble(viewModel.fuelCatList.get(item.getItemId()).getPrice()));
            viewModel.getServiceOrderRequest().setCurrency(viewModel.fuelCatList.get(item.getItemId()).getCurrency());
            viewModel.notifyChange(BR.serviceOrderRequest);
            return false;
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        viewModel.getPostRepository().setLiveData(viewModel.liveData);
    }

    @Override
    public void launchActivityResult(int request, int resultCode, Intent result) {
        super.launchActivityResult(request, resultCode, result);
        if (request == Constants.LOCATION_REQUEST) {
            viewModel.getServiceOrderRequest().setFromLatitude(result.getDoubleExtra(Constants.LAT, 0.0));
            viewModel.getServiceOrderRequest().setFromLongitude(result.getDoubleExtra(Constants.LNG, 0.0));
            viewModel.getServiceOrderRequest().setFromAddress(result.getStringExtra(Constants.ADDRESS));
        } else if (request == Constants.DELIVERY_TIME_REQUEST) {
            Bundle bundle = result.getBundleExtra(Constants.BUNDLE);
            if (bundle != null && bundle.containsKey(Constants.BUNDLE)) {
                PassingObject passingObject = (PassingObject) bundle.getSerializable(Constants.BUNDLE);
                viewModel.getServiceOrderRequest().setTime(passingObject.getObject2());
                viewModel.getServiceOrderRequest().setDate(passingObject.getObject());
            }
        } else if (request == Constants.ORDER_REQUEST) {
            finishActivity();
        }
    }
}
