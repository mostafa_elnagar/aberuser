package grand.app.aber_user.pages.cart;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;

import com.google.gson.Gson;

import java.util.Objects;

import javax.inject.Inject;

import grand.app.aber_user.PassingObject;
import grand.app.aber_user.R;
import grand.app.aber_user.base.BaseFragment;
import grand.app.aber_user.base.IApplicationComponent;
import grand.app.aber_user.base.MyApplication;
import grand.app.aber_user.databinding.FragmentPaymentBinding;
import grand.app.aber_user.databinding.OrderSuccessDialogBinding;
import grand.app.aber_user.model.base.Mutable;
import grand.app.aber_user.pages.cart.models.NewOrderRequest;
import grand.app.aber_user.pages.cart.viewModels.CartViewModel;
import grand.app.aber_user.utils.Constants;
import grand.app.aber_user.utils.helper.MovementHelper;

public class PaymentFragment extends BaseFragment {
    FragmentPaymentBinding binding;
    @Inject
    CartViewModel viewModel;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_payment, container, false);
        IApplicationComponent component = ((MyApplication) requireActivity().getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setViewmodel(viewModel);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            String passingObject = bundle.getString(Constants.BUNDLE);
            viewModel.setPassingObject(new Gson().fromJson(passingObject, PassingObject.class));
            viewModel.setNewOrderRequest(new Gson().fromJson(String.valueOf(viewModel.getPassingObject().getObjectClass()), NewOrderRequest.class));
        }
        setEvent();
        return binding.getRoot();
    }

    private void setEvent() {
        viewModel.liveData.observe(requireActivity(), (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            viewModel.setMessage(mutable.message.equals(Constants.HIDE_PROGRESS) ? mutable.message : "");
            if (Constants.ORDER.equals(((Mutable) o).message)) {
                showSuccessDialog();
            }
        });
    }

    private void showSuccessDialog() {
        Dialog deleteDialog = new Dialog(requireContext(), R.style.PauseDialog);
        deleteDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Objects.requireNonNull(deleteDialog.getWindow()).getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        deleteDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        OrderSuccessDialogBinding binding = DataBindingUtil.inflate(LayoutInflater.from(deleteDialog.getContext()), R.layout.order_success_dialog, null, false);
        deleteDialog.setContentView(binding.getRoot());
        deleteDialog.setOnCancelListener(dialog -> {
            viewModel.getCartRepository().emptyCart();
            MovementHelper.finishWithResult(new PassingObject(), requireActivity(), Constants.ORDER_REQUEST);
        });
        deleteDialog.show();
    }

    @Override
    public void onResume() {
        viewModel.getServicesRepository().setLiveData(viewModel.liveData);
        super.onResume();
    }
}
