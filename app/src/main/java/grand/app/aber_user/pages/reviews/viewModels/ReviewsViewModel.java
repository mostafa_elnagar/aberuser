package grand.app.aber_user.pages.reviews.viewModels;


import android.widget.RatingBar;

import androidx.databinding.Bindable;
import androidx.databinding.ObservableBoolean;
import androidx.lifecycle.MutableLiveData;

import javax.inject.Inject;

import grand.app.aber_user.BR;
import grand.app.aber_user.base.BaseViewModel;
import grand.app.aber_user.model.base.Mutable;
import grand.app.aber_user.pages.reviews.adapters.ClientReviewsAdapter;
import grand.app.aber_user.pages.reviews.models.RateRequest;
import grand.app.aber_user.pages.reviews.models.ReviewMainData;
import grand.app.aber_user.repository.ServicesRepository;
import grand.app.aber_user.utils.Constants;
import io.reactivex.disposables.CompositeDisposable;

public class ReviewsViewModel extends BaseViewModel {
    ClientReviewsAdapter reviewsAdapter;
    public MutableLiveData<Mutable> liveData;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    @Inject
    ServicesRepository servicesRepository;
    private ReviewMainData reviewMainData;
    RateRequest rateRequest;
    public ObservableBoolean searchProgressVisible = new ObservableBoolean();

    @Inject
    public ReviewsViewModel(ServicesRepository servicesRepository) {
        rateRequest = new RateRequest();
        reviewMainData = new ReviewMainData();
        this.servicesRepository = servicesRepository;
        this.liveData = new MutableLiveData<>();
        servicesRepository.setLiveData(liveData);
    }

    public void clientReviews(int page, boolean showProgress) {
        compositeDisposable.add(servicesRepository.getClientReviews(getPassingObject().getId(),page,showProgress));
    }

    public void sendRate() {
        getRateRequest().setOrderId(String.valueOf(getPassingObject().getId()));
        if (getRateRequest().isValid())
            compositeDisposable.add(servicesRepository.sendReview(getRateRequest()));
    }

    public void toRate() {
        liveData.setValue(new Mutable(Constants.REVIEW_NEW));
    }

    public void close() {
        liveData.setValue(new Mutable(Constants.DIALOG_CLOSE));
    }

    public void onRateChange(RatingBar ratingBar, float rating, boolean fromUser) {
        getRateRequest().setRate(String.valueOf(rating));
    }

    protected void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        unSubscribeFromObservable();
    }

    public ServicesRepository getServicesRepository() {
        return servicesRepository;
    }

    @Bindable
    public ClientReviewsAdapter getReviewsAdapter() {
        return this.reviewsAdapter == null ? this.reviewsAdapter = new ClientReviewsAdapter() : this.reviewsAdapter;
    }

    @Bindable
    public ReviewMainData getReviewMainData() {
        return reviewMainData;
    }

    @Bindable
    public void setReviewMainData(ReviewMainData reviewMainData) {
        if (getReviewsAdapter().getRatesItems().size() > 0) {
            getReviewsAdapter().loadMore(reviewMainData.getRates());
        } else {
            getReviewsAdapter().update(reviewMainData.getRates());
            notifyChange(BR.reviewsAdapter);
        }
        searchProgressVisible.set(false);
        notifyChange(BR.reviewMainData);
        this.reviewMainData = reviewMainData;
    }

    public RateRequest getRateRequest() {
        return rateRequest;
    }

    @Bindable
    public void setRateRequest(RateRequest rateRequest) {
        notifyChange(BR.rateRequest);
        this.rateRequest = rateRequest;
    }
}
