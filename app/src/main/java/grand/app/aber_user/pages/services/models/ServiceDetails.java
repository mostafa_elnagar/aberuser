package grand.app.aber_user.pages.services.models;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.aber_user.model.DropDownsObject;

public class ServiceDetails {
    @SerializedName("note")
    @Expose
    private String note;
    @SerializedName("services")
    @Expose
    private List<ServicesItem> services;
    @SerializedName("extra")
    @Expose
    private List<Extra> extraList;
    @SerializedName("vehicle")
    @Expose
    private List<DropDownsObject> vehicleList;
    @SerializedName("tyre")
    @Expose
    private List<DropDownsObject> tyreList;
    @SerializedName("oil")
    @Expose
    private List<DropDownsObject> oilList;
    @SerializedName("kilo")
    @Expose
    private List<DropDownsObject> kiloList;
    @SerializedName("glass")
    @Expose
    private List<DropDownsObject> glassList;
    @SerializedName("fuel")
    @Expose
    private List<DropDownsObject> fuelList;
    @SerializedName("liter_gallon")
    @Expose
    private List<DropDownsObject> literGallonList;
    @SerializedName("battery")
    @Expose
    private List<DropDownsObject> batteryList;
    @SerializedName("oil_prices")
    @Expose
    private List<DropDownsObject> oilPrices;
    @SerializedName("types")
    @Expose
    private List<DropDownsObject> boxTypes;

    @SerializedName("additions")
    @Expose
    private Additions additions;

    public String getNote() {
        return note;
    }

    public List<ServicesItem> getServices() {
        return services;
    }

    public List<Extra> getExtraList() {
        return extraList;
    }

    public List<DropDownsObject> getVehicleList() {
        return vehicleList;
    }

    public List<DropDownsObject> getTyreList() {
        return tyreList;
    }

    public List<DropDownsObject> getOilList() {
        return oilList;
    }

    public List<DropDownsObject> getKiloList() {
        return kiloList;
    }

    public List<DropDownsObject> getGlassList() {
        return glassList;
    }

    public List<DropDownsObject> getFuelList() {
        return fuelList;
    }

    public List<DropDownsObject> getLiterGallonList() {
        return literGallonList;
    }

    public List<DropDownsObject> getBatteryList() {
        return batteryList;
    }

    public List<DropDownsObject> getOilPrices() {
        return oilPrices;
    }

    public Additions getAdditions() {
        return additions;
    }

    public List<DropDownsObject> getBoxTypes() {
        return boxTypes;
    }
}