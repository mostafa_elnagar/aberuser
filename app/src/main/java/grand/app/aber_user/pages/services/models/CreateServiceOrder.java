package grand.app.aber_user.pages.services.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CreateServiceOrder {
    @SerializedName("main_service_id")
    private int mainServiceId;
    @SerializedName("sub_service_ids")
    private List<Integer> subServiceIdsList;
    @SerializedName("child_ids")
    private List<Integer> childIdsList;
    @SerializedName("extra_ids")
    private List<Integer> extraIdsList;
    @SerializedName("description")
    private String description;
    @SerializedName("total")
    private double total;
    @SerializedName("delivery_fees")
    private double deliveryFees;
    @SerializedName("extra_fees")
    private double extraFees;
    @SerializedName("subtotal")
    private double subtotal;
    @SerializedName("is_emergency")
    private int is_emergency;
    @SerializedName("from_address")
    private String address;
    @SerializedName("from_longitude")
    private double longitude;
    @SerializedName("from_latitude")
    private double latitude;
    @SerializedName("to_latitude")
    private double toLatitude;
    @SerializedName("to_longitude")
    private double toLongitude;
    @SerializedName("to_address")
    private String toAddress;
    @SerializedName("pay_type")
    private String payType = "cash";
    @SerializedName("extra_text")
    private String extraText;
    @SerializedName("scheduled_at")
    private String scheduled_at;
    @SerializedName("distance")
    private float distance;
    @SerializedName("transfer_service")
    private float transferService;
    @SerializedName("kilo_cost")
    private float kiloCost;
    private double taxes;

    public int getMainServiceId() {
        return mainServiceId;
    }

    public void setMainServiceId(int mainServiceId) {
        this.mainServiceId = mainServiceId;
    }

    public List<Integer> getSubServiceIdsList() {
        return subServiceIdsList;
    }

    public void setSubServiceIdsList(List<Integer> subServiceIdsList) {
        this.subServiceIdsList = subServiceIdsList;
    }

    public List<Integer> getChildIdsList() {
        return childIdsList;
    }

    public void setChildIdsList(List<Integer> childIdsList) {
        this.childIdsList = childIdsList;
    }

    public List<Integer> getExtraIdsList() {
        return extraIdsList;
    }

    public void setExtraIdsList(List<Integer> extraIdsList) {
        this.extraIdsList = extraIdsList;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double getDeliveryFees() {
        return deliveryFees;
    }

    public void setDeliveryFees(double deliveryFees) {
        this.deliveryFees = deliveryFees;
    }

    public double getExtraFees() {
        return extraFees;
    }

    public void setExtraFees(double extraFees) {
        this.extraFees = extraFees;
    }

    public double getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(double subtotal) {
        this.subtotal = subtotal;
    }

    public int getIs_emergency() {
        return is_emergency;
    }

    public void setIs_emergency(int is_emergency) {
        this.is_emergency = is_emergency;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    public String getExtraText() {
        return extraText;
    }

    public void setExtraText(String extraText) {
        this.extraText = extraText;
    }

    public String getScheduled_at() {
        return scheduled_at;
    }

    public void setScheduled_at(String scheduled_at) {
        this.scheduled_at = scheduled_at;
    }

    public double getToLatitude() {
        return toLatitude;
    }

    public void setToLatitude(double toLatitude) {
        this.toLatitude = toLatitude;
    }

    public double getToLongitude() {
        return toLongitude;
    }

    public void setToLongitude(double toLongitude) {
        this.toLongitude = toLongitude;
    }

    public String getToAddress() {
        return toAddress;
    }

    public void setToAddress(String toAddress) {
        this.toAddress = toAddress;
    }

    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }

    public float getTransferService() {
        return transferService;
    }

    public void setTransferService(float transferService) {
        this.transferService = transferService;
    }

    public float getKiloCost() {
        return kiloCost;
    }

    public void setKiloCost(float kiloCost) {
        this.kiloCost = kiloCost;
    }

    public double getTaxes() {
        return taxes;
    }

    public void setTaxes(double taxes) {
        this.taxes = taxes;
    }
}
