package grand.app.aber_user.pages.home.viewModels;

import android.text.TextUtils;

import androidx.databinding.Bindable;
import androidx.databinding.ObservableBoolean;
import androidx.lifecycle.MutableLiveData;

import javax.inject.Inject;

import grand.app.aber_user.BR;
import grand.app.aber_user.base.BaseViewModel;
import grand.app.aber_user.base.MyApplication;
import grand.app.aber_user.model.base.Mutable;
import grand.app.aber_user.pages.parts.adapters.PartProductsAdapter;
import grand.app.aber_user.pages.parts.models.ProductMain;
import grand.app.aber_user.repository.ServicesRepository;
import grand.app.aber_user.utils.session.UserHelper;
import io.reactivex.disposables.CompositeDisposable;

public class SearchViewModels extends BaseViewModel {

    public MutableLiveData<Mutable> liveData;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    PartProductsAdapter productsAdapter;
    @Inject
    ServicesRepository postRepository;
    public ObservableBoolean searchProgressVisible = new ObservableBoolean();
    public String search;
    ProductMain mainData;

    @Inject
    public SearchViewModels(ServicesRepository postRepository) {
        this.postRepository = postRepository;
        this.liveData = new MutableLiveData<>();
        postRepository.setLiveData(liveData);
    }

    public void search(int page, boolean showProgress) {
        compositeDisposable.add(postRepository.search(page, showProgress, search, String.valueOf(UserHelper.getInstance(MyApplication.getInstance()).getCountriesData().getId())));
    }

    @Bindable
    public PartProductsAdapter getProductsAdapter() {
        return this.productsAdapter == null ? this.productsAdapter = new PartProductsAdapter() : this.productsAdapter;
    }

    @Bindable
    public ProductMain getMainData() {
        return this.mainData == null ? this.mainData = new ProductMain() : this.mainData;
    }

    @Bindable
    public void setMainData(ProductMain mainData) {
        if (getProductsAdapter().getProductsItemList().size() > 0) {
            getProductsAdapter().loadMore(mainData.getProducts().getData());
        } else {
            getProductsAdapter().update(mainData.getProducts().getData());
            notifyChange(BR.productsAdapter);
        }
        searchProgressVisible.set(false);
        notifyChange(BR.mainData);
        this.mainData = mainData;
    }


    public void clear() {
        search = "";
        notifyChange();
    }

    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (!TextUtils.isEmpty(s)) {
            searchProgressVisible.set(true);
            search = s.toString();
            search(1, false);
        } else
            searchProgressVisible.set(false);
    }

    public ServicesRepository getPostRepository() {
        return postRepository;
    }


    protected void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        unSubscribeFromObservable();
    }

}
