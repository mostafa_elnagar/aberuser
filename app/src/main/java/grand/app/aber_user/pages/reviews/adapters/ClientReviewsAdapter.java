package grand.app.aber_user.pages.reviews.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import grand.app.aber_user.R;
import grand.app.aber_user.databinding.ItemReviewBinding;
import grand.app.aber_user.pages.reviews.itemViewModels.ItemClientReviewsViewModel;
import grand.app.aber_user.pages.reviews.models.RatesItem;

public class ClientReviewsAdapter extends RecyclerView.Adapter<ClientReviewsAdapter.ViewHolder> {
    public List<RatesItem> ratesItems;

    public ClientReviewsAdapter() {
        this.ratesItems = new ArrayList<>();
    }

    public List<RatesItem> getRatesItems() {
        return ratesItems;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_review,
                parent, false);
        return new ViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        ItemClientReviewsViewModel itemMenuViewModel = new ItemClientReviewsViewModel(ratesItems.get(position));
        holder.setViewModel(itemMenuViewModel);
    }


    public void update(List<RatesItem> dataList) {
        this.ratesItems.clear();
        ratesItems.addAll(dataList);
        notifyDataSetChanged();
    }
    public void loadMore(@NotNull List<RatesItem> dataList) {
        int start = ratesItems.size();
        ratesItems.addAll(dataList);
        notifyItemRangeInserted(start, dataList.size());
    }
    @Override
    public void onViewAttachedToWindow(@NotNull ViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NotNull ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    @Override
    public int getItemCount() {
        return ratesItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ItemReviewBinding binding;

        ViewHolder(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (binding == null) {
                binding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (binding != null) {
                binding.unbind();
            }
        }

        void setViewModel(ItemClientReviewsViewModel itemViewModels) {
            if (binding != null) {
                binding.setItemViewModel(itemViewModels);
            }
        }
    }
}
