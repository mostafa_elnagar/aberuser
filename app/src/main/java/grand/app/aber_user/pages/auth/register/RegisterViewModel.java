package grand.app.aber_user.pages.auth.register;

import android.widget.CompoundButton;

import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import grand.app.aber_user.R;
import grand.app.aber_user.base.BaseViewModel;
import grand.app.aber_user.base.MyApplication;
import grand.app.aber_user.connection.FileObject;
import grand.app.aber_user.model.base.Mutable;
import grand.app.aber_user.pages.auth.models.RegisterRequest;
import grand.app.aber_user.repository.AuthRepository;
import grand.app.aber_user.utils.Constants;
import grand.app.aber_user.utils.resources.ResourceManager;
import grand.app.aber_user.utils.session.UserHelper;
import grand.app.aber_user.utils.validation.Validate;
import io.reactivex.disposables.CompositeDisposable;

public class RegisterViewModel extends BaseViewModel {
    MutableLiveData<Mutable> liveData;
    List<FileObject> fileObject;
    @Inject
    AuthRepository repository;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    RegisterRequest request;
    boolean isTermsAccepted = false;

    @Inject
    public RegisterViewModel(AuthRepository repository) {
        fileObject = new ArrayList<>();
        this.repository = repository;
        this.liveData = new MutableLiveData<>();
        repository.setLiveData(liveData);
        request = new RegisterRequest();
    }

    public void register() {
        getRequest().setToken(UserHelper.getInstance(MyApplication.getInstance()).getToken());
        if (getRequest().isValid()) {
            if (Validate.isMatchPassword(getRequest().getPassword(), getRequest().getConfirmPassword())) {
                if (isTermsAccepted) {
                    setMessage(Constants.SHOW_PROGRESS);
                    compositeDisposable.add(repository.register(request));
                } else
                    liveData.setValue(new Mutable(Constants.ERROR_TOAST, ResourceManager.getString(R.string.terms_warning)));
            } else
                liveData.setValue(new Mutable(Constants.ERROR_TOAST, ResourceManager.getString(R.string.password_not_match)));
        } else
            liveData.setValue(new Mutable(Constants.ERROR, ResourceManager.getString(R.string.empty_warning)));
    }

    public void onCheckedChange(CompoundButton button, boolean check) {
        if (check)
            liveData.setValue(new Mutable(Constants.TERMS));
        isTermsAccepted = check;
    }

    public RegisterRequest getRequest() {
        return request;
    }

    public List<FileObject> getFileObject() {
        return fileObject;
    }

    private void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        unSubscribeFromObservable();
    }

}
