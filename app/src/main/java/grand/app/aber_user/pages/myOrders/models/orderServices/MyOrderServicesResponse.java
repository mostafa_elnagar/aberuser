package grand.app.aber_user.pages.myOrders.models.orderServices;

import com.google.gson.annotations.SerializedName;

import grand.app.aber_user.model.base.StatusMessage;

public class MyOrderServicesResponse extends StatusMessage {

	@SerializedName("data")
	private MyOrdersServicesMainData data;

	public MyOrdersServicesMainData getData(){
		return data;
	}

}