package grand.app.aber_user.pages.parts.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import grand.app.aber_user.PassingObject;
import grand.app.aber_user.R;
import grand.app.aber_user.base.ParentActivity;
import grand.app.aber_user.databinding.ItemPartServiceBinding;
import grand.app.aber_user.pages.parts.ProductDetailsFragment;
import grand.app.aber_user.pages.parts.models.ProductsItem;
import grand.app.aber_user.pages.parts.viewModels.ItemProductsViewModel;
import grand.app.aber_user.utils.Constants;
import grand.app.aber_user.utils.helper.MovementHelper;
import grand.app.aber_user.utils.resources.ResourceManager;

public class PartProductsAdapter extends RecyclerView.Adapter<PartProductsAdapter.MenuView> {
    List<ProductsItem> productsItemList;
    Context context;
    public MutableLiveData<Integer> liveData = new MutableLiveData<>();

    public PartProductsAdapter() {
        this.productsItemList = new ArrayList<>();
    }


    public List<ProductsItem> getProductsItemList() {
        return productsItemList;
    }

    @NonNull
    @Override
    public MenuView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_part_service,
                parent, false);
        this.context = parent.getContext();
        return new MenuView(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull final MenuView holder, final int position) {
        ProductsItem menuModel = productsItemList.get(position);
        ItemProductsViewModel itemMenuViewModel = new ItemProductsViewModel(menuModel);
        itemMenuViewModel.getLiveData().observe((LifecycleOwner) context, o -> {
            if (o.equals(Constants.SERVICE_DETAILS))
                MovementHelper.startActivityWithBundle(context, new PassingObject(menuModel.getId()), menuModel.getName(), ProductDetailsFragment.class.getName(), null);
            else if (o.equals(Constants.LIKES_REACTION))
                liveData.setValue(menuModel.getId());
            else if (o.equals(Constants.CART))
                ((ParentActivity) context).toastMessage(ResourceManager.getString(R.string.add_to_cart_successfully), R.drawable.ic_check_white_24dp, R.color.successColor);
        });
        holder.setViewModel(itemMenuViewModel);
    }

    public void update(@NotNull List<ProductsItem> dataList) {
        this.productsItemList.clear();
        productsItemList.addAll(dataList);
        notifyDataSetChanged();
    }

    public void loadMore(@NotNull List<ProductsItem> dataList) {
        int start = productsItemList.size();
        productsItemList.addAll(dataList);
        notifyItemRangeInserted(start, dataList.size());
    }

    @Override
    public void onViewAttachedToWindow(@NotNull MenuView holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NotNull MenuView holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    @Override
    public int getItemCount() {
        return productsItemList.size();
    }

    public class MenuView extends RecyclerView.ViewHolder {
        public ItemPartServiceBinding itemMenuBinding;

        MenuView(View itemView) {
            super(itemView);
            bind();
        }

        void bind() {
            if (itemMenuBinding == null) {
                itemMenuBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemMenuBinding != null) {
                itemMenuBinding.unbind();
            }
        }

        void setViewModel(ItemProductsViewModel itemViewModels) {
            if (itemMenuBinding != null) {
                itemMenuBinding.setItemViewModel(itemViewModels);
            }
        }
    }
}
