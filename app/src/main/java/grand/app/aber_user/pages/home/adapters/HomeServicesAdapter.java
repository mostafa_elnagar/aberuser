package grand.app.aber_user.pages.home.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import grand.app.aber_user.PassingObject;
import grand.app.aber_user.R;
import grand.app.aber_user.databinding.ItemCategoryBinding;
import grand.app.aber_user.pages.home.models.HomeServices;
import grand.app.aber_user.pages.home.viewModels.ItemMainViewModel;
import grand.app.aber_user.pages.parts.PartsFragment;
import grand.app.aber_user.pages.services.AberBoxFragment;
import grand.app.aber_user.pages.services.BatteriesFragment;
import grand.app.aber_user.pages.services.CarCheckFragment;
import grand.app.aber_user.pages.services.CarWashFragment;
import grand.app.aber_user.pages.services.FuelFragment;
import grand.app.aber_user.pages.services.HiddenFragment;
import grand.app.aber_user.pages.services.OilsFragment;
import grand.app.aber_user.pages.services.OpenCarFragment;
import grand.app.aber_user.pages.services.TiersFragment;
import grand.app.aber_user.pages.services.WaterBallonFragment;
import grand.app.aber_user.pages.services.WinchFragment;
import grand.app.aber_user.utils.helper.MovementHelper;

public class HomeServicesAdapter extends RecyclerView.Adapter<HomeServicesAdapter.MenuView> {
    List<HomeServices> postDataList;
    private Context context;
    public int lastSelected = -1;
    public MutableLiveData<Object> liveData = new MutableLiveData<>();

    public HomeServicesAdapter() {
        this.postDataList = new ArrayList<>();
    }

    @NonNull
    @Override
    public MenuView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category,
                parent, false);
        this.context = parent.getContext();
        return new MenuView(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull final MenuView holder, @SuppressLint("RecyclerView") final int position) {
        HomeServices menuModel = postDataList.get(position);
        ItemMainViewModel itemMenuViewModel = new ItemMainViewModel(menuModel);
        itemMenuViewModel.getLiveData().observe(((LifecycleOwner) context), o -> {
            this.lastSelected = menuModel.getId();
            if (menuModel.getId() == 1) {
                MovementHelper.startActivityWithBundle(MovementHelper.unwrap(context), new PassingObject(menuModel.getId()), menuModel.getName(), WinchFragment.class.getName(), null);
            } else if (menuModel.getId() == 3) {
                MovementHelper.startActivityWithBundle(MovementHelper.unwrap(context), new PassingObject(menuModel.getId()), menuModel.getName(), CarCheckFragment.class.getName(), null);
            } else if (menuModel.getId() == 5) {
                MovementHelper.startActivityWithBundle(MovementHelper.unwrap(context), new PassingObject(menuModel.getId()), menuModel.getName(), PartsFragment.class.getName(), null);
            } else if (menuModel.getId() == 7) {
                MovementHelper.startActivityWithBundle(MovementHelper.unwrap(context), new PassingObject(menuModel.getId()), menuModel.getName(), CarWashFragment.class.getName(), null);
            } else if (menuModel.getId() == 9) {
                MovementHelper.startActivityWithBundle(MovementHelper.unwrap(context), new PassingObject(menuModel.getId()), menuModel.getName(), TiersFragment.class.getName(), null);
            } else if (menuModel.getId() == 11) {
                MovementHelper.startActivityWithBundle(MovementHelper.unwrap(context), new PassingObject(menuModel.getId()), menuModel.getName(), OilsFragment.class.getName(), null);
            } else if (menuModel.getId() == 13) {
                MovementHelper.startActivityWithBundle(MovementHelper.unwrap(context), new PassingObject(menuModel.getId()), menuModel.getName(), HiddenFragment.class.getName(), null);
            } else if (menuModel.getId() == 15) {
                MovementHelper.startActivityWithBundle(MovementHelper.unwrap(context), new PassingObject(menuModel.getId()), menuModel.getName(), FuelFragment.class.getName(), null);
            } else if (menuModel.getId() == 17) {
                MovementHelper.startActivityWithBundle(MovementHelper.unwrap(context), new PassingObject(menuModel.getId()), menuModel.getName(), WaterBallonFragment.class.getName(), null);
            } else if (menuModel.getId() == 19) {
                MovementHelper.startActivityWithBundle(MovementHelper.unwrap(context), new PassingObject(menuModel.getId()), menuModel.getName(), OpenCarFragment.class.getName(), null);
            } else if (menuModel.getId() == 21) {
                MovementHelper.startActivityWithBundle(MovementHelper.unwrap(context), new PassingObject(menuModel.getId()), menuModel.getName(), BatteriesFragment.class.getName(), null);
            }else if (menuModel.getId() == 23) {
                MovementHelper.startActivityWithBundle(MovementHelper.unwrap(context), new PassingObject(menuModel.getId()), menuModel.getName(), AberBoxFragment.class.getName(), null);
            }
        });
        holder.setViewModel(itemMenuViewModel);
    }

    public void update(List<HomeServices> dataList) {
        this.postDataList.clear();
        postDataList.addAll(dataList);
        notifyDataSetChanged();
    }

    @Override
    public void onViewAttachedToWindow(@NotNull MenuView holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NotNull MenuView holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    @Override
    public int getItemCount() {
        return postDataList.size();
    }

    public class MenuView extends RecyclerView.ViewHolder {
        public ItemCategoryBinding itemMenuBinding;

        MenuView(View itemView) {
            super(itemView);
            bind();
        }

        void bind() {
            if (itemMenuBinding == null) {
                itemMenuBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemMenuBinding != null) {
                itemMenuBinding.unbind();
            }
        }

        void setViewModel(ItemMainViewModel itemViewModels) {
            if (itemMenuBinding != null) {
                itemMenuBinding.setItemPostViewModel(itemViewModels);
            }
        }
    }
}
