package grand.app.aber_user.pages.parts.viewModels;

import androidx.databinding.Bindable;
import androidx.lifecycle.MutableLiveData;

import javax.inject.Inject;

import grand.app.aber_user.BR;
import grand.app.aber_user.base.BaseViewModel;
import grand.app.aber_user.base.MyApplication;
import grand.app.aber_user.model.base.Mutable;
import grand.app.aber_user.pages.parts.adapters.PartsAdapter;
import grand.app.aber_user.pages.services.models.ServiceDetails;
import grand.app.aber_user.repository.ServicesRepository;
import grand.app.aber_user.utils.session.UserHelper;
import io.reactivex.disposables.CompositeDisposable;

public class PartsViewModels extends BaseViewModel {

    public MutableLiveData<Mutable> liveData;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    PartsAdapter partsAdapter;
    @Inject
    ServicesRepository postRepository;
    ServiceDetails details;

    @Inject
    public PartsViewModels(ServicesRepository postRepository) {
        details = new ServiceDetails();
        this.postRepository = postRepository;
        this.liveData = new MutableLiveData<>();
        postRepository.setLiveData(liveData);
    }

    public void getServiceDetails() {
        compositeDisposable.add(postRepository.getServiceDetails(getPassingObject().getId(), UserHelper.getInstance(MyApplication.getInstance()).getCountriesData().getId()));
    }

    public void liveDataActions(String action) {
        liveData.setValue(new Mutable(action));
    }

    @Bindable
    public PartsAdapter getPartsAdapter() {
        return this.partsAdapter == null ? this.partsAdapter = new PartsAdapter() : this.partsAdapter;
    }

    @Bindable
    public ServiceDetails getDetails() {
        return details;
    }

    @Bindable
    public void setDetails(ServiceDetails details) {
        getPartsAdapter().update(details.getServices());
        notifyChange(BR.extraAdapter);
        notifyChange(BR.details);
        this.details = details;
    }

    public ServicesRepository getPostRepository() {
        return postRepository;
    }


    protected void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        unSubscribeFromObservable();
    }

}
