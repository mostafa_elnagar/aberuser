package grand.app.aber_user.pages.myOrders.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import grand.app.aber_user.R;
import grand.app.aber_user.databinding.ItemOrderDetailBinding;
import grand.app.aber_user.pages.myOrders.viewModels.ItemMyOrderProductsViewModel;
import grand.app.aber_user.pages.parts.models.ProductsItem;

public class MyOrderProductsAdapter extends RecyclerView.Adapter<MyOrderProductsAdapter.MenuView> {
    List<ProductsItem> productsItemList;
    Context context;

    public MyOrderProductsAdapter() {
        this.productsItemList = new ArrayList<>();
    }

    @NonNull
    @Override
    public MenuView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_order_detail,
                parent, false);
        this.context = parent.getContext();
        return new MenuView(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull final MenuView holder, final int position) {
        ProductsItem menuModel = productsItemList.get(position);
        ItemMyOrderProductsViewModel itemMenuViewModel = new ItemMyOrderProductsViewModel(menuModel);
        holder.setViewModel(itemMenuViewModel);
    }

    public void update(@NotNull List<ProductsItem> dataList) {
        Log.e("update", "update: "+dataList.size() );
        this.productsItemList.clear();
        productsItemList.addAll(dataList);
        notifyDataSetChanged();
    }

    @Override
    public void onViewAttachedToWindow(@NotNull MenuView holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NotNull MenuView holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    @Override
    public int getItemCount() {
        return productsItemList.size();
    }

    public class MenuView extends RecyclerView.ViewHolder {
        public ItemOrderDetailBinding itemMenuBinding;

        MenuView(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemMenuBinding == null) {
                itemMenuBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemMenuBinding != null) {
                itemMenuBinding.unbind();
            }
        }

        void setViewModel(ItemMyOrderProductsViewModel itemViewModels) {
            if (itemMenuBinding != null) {
                itemMenuBinding.setItemViewModel(itemViewModels);
            }
        }
    }
}
