package grand.app.aber_user.pages.parts.models.productDetails;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class AttributesItem {

    @SerializedName("sizes")
    private List<SizesItem> sizes;

    @SerializedName("name")
    private String name;

    @SerializedName("id")
    private int id;
    private boolean checked;
    @SerializedName("value")
    private String value;

    public List<SizesItem> getSizes() {
        return sizes;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public String getValue() {
        return value;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }
}