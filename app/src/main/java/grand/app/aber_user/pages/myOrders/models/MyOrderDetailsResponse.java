package grand.app.aber_user.pages.myOrders.models;

import com.google.gson.annotations.SerializedName;

import grand.app.aber_user.model.base.StatusMessage;

public class MyOrderDetailsResponse extends StatusMessage {

    @SerializedName("data")
    private MyOrderDetails data;

    public MyOrderDetails getData() {
        return data;
    }

}