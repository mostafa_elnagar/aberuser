package grand.app.aber_user.pages.chat.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import grand.app.aber_user.model.base.StatusMessage;
import grand.app.aber_user.pages.conversations.models.ConversationsData;


public class ChatResponse extends StatusMessage {
    @SerializedName("data")
    @Expose
    private List<ConversationsData> chats;

    public List<ConversationsData> getChats() {
        return chats;
    }
}
