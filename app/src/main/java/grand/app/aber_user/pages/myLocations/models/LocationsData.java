package grand.app.aber_user.pages.myLocations.models;

import com.google.gson.annotations.SerializedName;

public class LocationsData {
    @SerializedName("delivery_cost")
    private double deliveryCost;

    @SerializedName("phone")
    private String phone;

    @SerializedName("street")
    private String street;

    @SerializedName("name")
    private String name;

    @SerializedName("id")
    private int id;

    @SerializedName("is_default")
    private int isDefault;
    public boolean isRemove;

    public double getDeliveryCost() {
        return deliveryCost;
    }

    public String getPhone() {
        return phone;
    }

    public String getStreet() {
        return street;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public int getIsDefault() {
        return isDefault;
    }
}