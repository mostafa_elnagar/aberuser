package grand.app.aber_user.pages.services.viewModels;


import androidx.databinding.Bindable;

import grand.app.aber_user.base.BaseViewModel;
import grand.app.aber_user.pages.home.models.HomeServices;
import grand.app.aber_user.pages.services.models.ServicesItem;
import grand.app.aber_user.utils.Constants;

public class ItemServicesViewModel extends BaseViewModel {
    public ServicesItem servicesItem;

    public ItemServicesViewModel(ServicesItem servicesItem) {
        this.servicesItem = servicesItem;
    }

    @Bindable
    public ServicesItem getServicesItem() {
        return servicesItem;
    }

    public void itemAction() {
        getLiveData().setValue(Constants.MENu);
    }

}
