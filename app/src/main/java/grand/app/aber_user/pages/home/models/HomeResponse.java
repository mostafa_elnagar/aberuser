package grand.app.aber_user.pages.home.models;

import com.google.gson.annotations.SerializedName;

import grand.app.aber_user.model.base.StatusMessage;

public class HomeResponse extends StatusMessage {

	@SerializedName("data")
	private MainData mainData;

	public MainData getMainData() {
		return mainData;
	}
}