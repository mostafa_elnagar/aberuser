package grand.app.aber_user.pages.services.viewModels;


import androidx.databinding.Bindable;

import grand.app.aber_user.base.BaseViewModel;
import grand.app.aber_user.model.DropDownsObject;

public class ItemOilPriceListViewModel extends BaseViewModel {
    public DropDownsObject dropDownsObject;

    public ItemOilPriceListViewModel(DropDownsObject dropDownsObject) {
        this.dropDownsObject = dropDownsObject;
    }

    @Bindable
    public DropDownsObject getDropDownsObject() {
        return dropDownsObject;
    }

}
