package grand.app.aber_user.pages.auth.login;

import androidx.lifecycle.MutableLiveData;

import javax.inject.Inject;

import grand.app.aber_user.base.BaseViewModel;
import grand.app.aber_user.base.MyApplication;
import grand.app.aber_user.model.base.Mutable;
import grand.app.aber_user.pages.auth.models.LoginRequest;
import grand.app.aber_user.repository.AuthRepository;
import grand.app.aber_user.utils.Constants;
import grand.app.aber_user.utils.session.UserHelper;
import io.reactivex.disposables.CompositeDisposable;

public class LoginViewModel extends BaseViewModel {
    public MutableLiveData<Mutable> liveData;
    @Inject
    AuthRepository repository;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    LoginRequest loginRequest;

    @Inject
    public LoginViewModel(AuthRepository repository) {
        this.repository = repository;
        this.liveData = new MutableLiveData<>();
        repository.setLiveData(liveData);
        loginRequest = new LoginRequest();
    }

    public void loginPassword() {
        getLoginRequest().setToken(UserHelper.getInstance(MyApplication.getInstance()).getToken());
        if (getLoginRequest().isValid()) {
            setMessage(Constants.SHOW_PROGRESS);
            compositeDisposable.add(repository.loginPassword(loginRequest));
        }
    }
    public void logout() {
        compositeDisposable.add(repository.logOut());
    }
    public void loginWithSocial() {
        compositeDisposable.add(repository.loginWithSocial(loginRequest));
    }

    public void toFacebook() {
        liveData.setValue(new Mutable(Constants.FACE_BOOK));
    }

    public void googleSignIn() {
        liveData.setValue(new Mutable(Constants.GOOGLE_SIGN_IN));
    }

    public void register() {
        liveData.setValue(new Mutable(Constants.REGISTER));
    }

    public void forgetPassword() {
        liveData.setValue(new Mutable(Constants.FORGET_PASSWORD));
    }

    public void skip() {
        liveData.setValue(new Mutable(Constants.COUNTRIES));
    }

    public void terms() {
        liveData.setValue(new Mutable(Constants.TERMS));
    }

    public void policy() {
        liveData.setValue(new Mutable(Constants.PRIVACY));
    }

    private void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    public AuthRepository getRepository() {
        return repository;
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        unSubscribeFromObservable();
    }

    public LoginRequest getLoginRequest() {
        return loginRequest;
    }

}
