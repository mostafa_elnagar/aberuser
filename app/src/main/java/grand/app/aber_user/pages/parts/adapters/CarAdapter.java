package grand.app.aber_user.pages.parts.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import grand.app.aber_user.R;
import grand.app.aber_user.databinding.ItemCarBinding;
import grand.app.aber_user.model.DropDownsObject;
import grand.app.aber_user.pages.parts.viewModels.ItemCarViewModel;

public class CarAdapter extends RecyclerView.Adapter<CarAdapter.MenuView> {
    List<DropDownsObject> carList;
    Context context;
    public int lastSelected = -1, lastPosition = -1;
    public MutableLiveData<Object> liveData = new MutableLiveData<>();

    public CarAdapter() {
        this.carList = new ArrayList<>();
    }

    public List<DropDownsObject> getCarList() {
        return carList;
    }

    @NonNull
    @Override
    public MenuView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_car,
                parent, false);
        this.context = parent.getContext();
        return new MenuView(itemView);
    }

    @SuppressLint("RecyclerView")
    @Override
    public void onBindViewHolder(@NonNull final MenuView holder, final int position) {
        DropDownsObject menuModel = carList.get(position);
        ItemCarViewModel itemMenuViewModel = new ItemCarViewModel(menuModel);
        itemMenuViewModel.getLiveData().observe((LifecycleOwner) context, o -> {
            lastPosition = position;
            lastSelected = menuModel.getId();
            liveData.setValue(o);
            notifyDataSetChanged();
        });
        menuModel.setChecked(lastSelected == menuModel.getId());
        if (position == carList.size() - 1)
            holder.itemMenuBinding.v.setVisibility(View.INVISIBLE);
        holder.setViewModel(itemMenuViewModel);
    }

    public void update(@NotNull List<DropDownsObject> dataList) {
        this.carList.clear();
        carList.addAll(dataList);
//        lastSelected = dataList.get(0).getId();
//        lastPosition = 0;
        notifyDataSetChanged();
    }

    @Override
    public void onViewAttachedToWindow(@NotNull MenuView holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NotNull MenuView holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    @Override
    public int getItemCount() {
        return carList.size();
    }

    public class MenuView extends RecyclerView.ViewHolder {
        public ItemCarBinding itemMenuBinding;

        MenuView(View itemView) {
            super(itemView);
            bind();
        }

        void bind() {
            if (itemMenuBinding == null) {
                itemMenuBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemMenuBinding != null) {
                itemMenuBinding.unbind();
            }
        }

        void setViewModel(ItemCarViewModel itemViewModels) {
            if (itemMenuBinding != null) {
                itemMenuBinding.setItemViewModel(itemViewModels);
            }
        }
    }
}
