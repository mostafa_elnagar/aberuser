package grand.app.aber_user.pages.reviews.models;

import com.google.gson.annotations.SerializedName;

import grand.app.aber_user.model.base.StatusMessage;

public class SendReviewResponse extends StatusMessage {
    @SerializedName("data")
    private RatesItem ratesItem;

    public RatesItem getRatesItem() {
        return ratesItem;
    }
}
