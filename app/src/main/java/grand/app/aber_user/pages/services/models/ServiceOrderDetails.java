package grand.app.aber_user.pages.services.models;

import android.text.TextUtils;
import android.util.Log;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import grand.app.aber_user.R;
import grand.app.aber_user.utils.resources.ResourceManager;

public class ServiceOrderDetails {
    private int mainService;
    @SerializedName("service_ids")
    private List<ServicesItem> servicesIds;
    @SerializedName("extra")
    private List<Extra> extraList;
    @SerializedName("from_latitude")
    private double fromLatitude;
    @SerializedName("from_longitude")
    private double fromLongitude;
    @SerializedName("from_address")
    private String fromAddress;
    @SerializedName("to_latitude")
    private double toLatitude;
    @SerializedName("to_longitude")
    private double toLongitude;
    @SerializedName("to_address")
    private String toAddress;
    @SerializedName("emeregency")
    private boolean isEmerengcy;
    @SerializedName("date")
    private String date;
    @SerializedName("time")
    private String time;
    @SerializedName("desc")
    private String desc;
    @SerializedName("car_type")
    private String carType;
    private int carTypeId;
    @SerializedName("car_cat")
    private String carCat;
    private int carCatId;
    @SerializedName("car_model")
    private String carModel;
    private int carModelId;
    @SerializedName("tyer_type")
    private String tyerType;
    private int tyerTypeId;
    @SerializedName("tyer_desc")
    private String tyerDesc;
    private int tyerDescId;
    @SerializedName("oil_type")
    private String oilType;
    private int oilTypeId;
    @SerializedName("oil_liquid")
    private String oilLiquid;
    private int oilLiquidId;
    @SerializedName("km")
    private String km;
    private int kmId;
    //HIDDEN
    @SerializedName("hidden_type")
    private String hiddenType;
    private int hiddenTypeId;
    @SerializedName("hidden_color")
    private String hiddenColor;
    private int hiddenColorId;
    @SerializedName("hidden_percentage")
    private String hiddenPercentage;
    private int hiddenPercentageId;
    //fuel
    @SerializedName("fuel_type")
    private String fuelType;
    private int fuelTypeId;
    @SerializedName("fuel_cat")
    private String fuelCat;
    private int fuelCatId;
    //WaterBallonFragment
    @SerializedName("litre")
    private String litre;
    private int litreId;
    @SerializedName("gallon")
    private String gallon;
    private int gallonId;
    @SerializedName("service_id")
    private String tinkerServiceName;
    private int tinkerServiceId;

    //open car
    @SerializedName("building_year")
    private String buildingYear;
    @SerializedName("car_motor")
    private String carMotor;
    //battery car
    @SerializedName("battery_type")
    private String batteryType;
    private String batteryTypeId;
    @SerializedName("battery_size")
    private String batterySize;
    private String batterySizeId;
    private double totalServices;
    private double total;
    private double extraTotalServices;
    private double movingPriceServices;
    private double emePriceServices;
    private String currency;
    private String tyreImage;
    private String frontOwnerImage;
    private String backOwnerImage;
    private String boxType;
    private int boxTypeId;
    private float kiloCost;
    private float taxes;

    public String isWinchValid() {
        if (servicesIds.size() == 0)
            return ResourceManager.getString(R.string.service_warning);
        else if (TextUtils.isEmpty(carType))
            return ResourceManager.getString(R.string.car_types);
        else if (TextUtils.isEmpty(carCat))
            return ResourceManager.getString(R.string.car_categories);
        else if (TextUtils.isEmpty(carModel))
            return ResourceManager.getString(R.string.car_models);
        else if (fromLatitude == 0.0 & fromLongitude == 0.0)
            return ResourceManager.getString(R.string.up_location);
        else if (toLatitude == 0.0 & toLongitude == 0.0)
            return ResourceManager.getString(R.string.down_location);
        else if (!isEmerengcy && TextUtils.isEmpty(date) && TextUtils.isEmpty(time))
            return ResourceManager.getString(R.string.service_time);
        else
            return null;
    }

    public String isCarCheckValid() {
        if (servicesIds.size() == 0)
            return ResourceManager.getString(R.string.service_warning);
        else if (TextUtils.isEmpty(carType))
            return ResourceManager.getString(R.string.car_types);
        else if (TextUtils.isEmpty(carCat))
            return ResourceManager.getString(R.string.car_categories);
        else if (TextUtils.isEmpty(carModel))
            return ResourceManager.getString(R.string.car_models);
        else if (toLatitude == 0.0 & toLongitude == 0.0) {
            Log.e("isCarCheckValid", "isCarCheckValid: " + ResourceManager.getString(R.string.detect_location));
            return ResourceManager.getString(R.string.detect_location);
        } else if (!isEmerengcy && TextUtils.isEmpty(date) && TextUtils.isEmpty(time))
            return ResourceManager.getString(R.string.service_time);
        else
            return null;
    }

    public String isTyreValid() {
        if (servicesIds.size() == 0)
            return ResourceManager.getString(R.string.service_warning);
        else if (TextUtils.isEmpty(carType))
            return ResourceManager.getString(R.string.car_types);
        else if (TextUtils.isEmpty(carCat))
            return ResourceManager.getString(R.string.car_categories);
        else if (TextUtils.isEmpty(carModel))
            return ResourceManager.getString(R.string.car_models);
        else if (TextUtils.isEmpty(tyerType))
            return ResourceManager.getString(R.string.tire_type);
        else if (TextUtils.isEmpty(tyerDesc))
            return ResourceManager.getString(R.string.tire_desc);
        else if (toLongitude == 0.0 & toLatitude == 0.0)
            return ResourceManager.getString(R.string.detect_location);
        else if (!isEmerengcy && TextUtils.isEmpty(date) && TextUtils.isEmpty(time))
            return ResourceManager.getString(R.string.service_time);
        else
            return null;
    }

    public String isOilValid() {
        if (TextUtils.isEmpty(oilType))
            return ResourceManager.getString(R.string.oil_types);
        else if (TextUtils.isEmpty(oilLiquid))
            return ResourceManager.getString(R.string.oil_liguid);
        else if (TextUtils.isEmpty(carType))
            return ResourceManager.getString(R.string.car_types);
        else if (TextUtils.isEmpty(carCat))
            return ResourceManager.getString(R.string.car_categories);
        else if (TextUtils.isEmpty(carModel))
            return ResourceManager.getString(R.string.car_models);
        else if (TextUtils.isEmpty(km))
            return ResourceManager.getString(R.string.km);
        else if (toLongitude == 0.0 & toLatitude == 0.0)
            return ResourceManager.getString(R.string.detect_location);
        else if (!isEmerengcy && TextUtils.isEmpty(date) && TextUtils.isEmpty(time))
            return ResourceManager.getString(R.string.service_time);
        else
            return null;
    }

    public String isHiddenValid() {
        if (TextUtils.isEmpty(hiddenType))
            return ResourceManager.getString(R.string.hidden_type);
        else if (TextUtils.isEmpty(hiddenColor))
            return ResourceManager.getString(R.string.hidden_color);
        else if (TextUtils.isEmpty(hiddenPercentage))
            return ResourceManager.getString(R.string.hidden_percentage);
        else if (TextUtils.isEmpty(carType))
            return ResourceManager.getString(R.string.car_types);
        else if (TextUtils.isEmpty(carCat))
            return ResourceManager.getString(R.string.car_categories);
        else if (TextUtils.isEmpty(carModel))
            return ResourceManager.getString(R.string.car_models);
        else if (toLongitude == 0.0 & toLatitude == 0.0)
            return ResourceManager.getString(R.string.detect_location);
        else if (!isEmerengcy && TextUtils.isEmpty(date) && TextUtils.isEmpty(time))
            return ResourceManager.getString(R.string.service_time);
        else
            return null;
    }

    public String isFuelValid() {
        if (TextUtils.isEmpty(fuelType))
            return ResourceManager.getString(R.string.fuel_type);
        else if (TextUtils.isEmpty(fuelCat))
            return ResourceManager.getString(R.string.fuel_categories);
        else if (TextUtils.isEmpty(carType))
            return ResourceManager.getString(R.string.car_types);
        else if (TextUtils.isEmpty(carCat))
            return ResourceManager.getString(R.string.car_categories);
        else if (TextUtils.isEmpty(carModel))
            return ResourceManager.getString(R.string.car_models);
        else if (toLatitude == 0.0 & toLongitude == 0.0)
            return ResourceManager.getString(R.string.detect_location);
        else if (!isEmerengcy && TextUtils.isEmpty(date) && TextUtils.isEmpty(time))
            return ResourceManager.getString(R.string.service_time);
        else
            return null;
    }

    public String isLitreValid() {
        if (TextUtils.isEmpty(litre))
            return ResourceManager.getString(R.string.place_type);
        else if (TextUtils.isEmpty(tinkerServiceName))
            return ResourceManager.getString(R.string.service_type);
        else if (TextUtils.isEmpty(gallon))
            return ResourceManager.getString(R.string.water_gallwon_amount);
        else if (fromLatitude == 0.0 & fromLongitude == 0.0)
            return ResourceManager.getString(R.string.up_location);
        else if (toLatitude == 0.0 & toLongitude == 0.0)
            return ResourceManager.getString(R.string.down_location);
        else if (!isEmerengcy && TextUtils.isEmpty(date) && TextUtils.isEmpty(time))
            return ResourceManager.getString(R.string.service_time);
        else
            return null;
    }

    public String isOpenCarValid() {
        if (servicesIds.size() == 0)
            return ResourceManager.getString(R.string.service_warning);
        else if (TextUtils.isEmpty(carType))
            return ResourceManager.getString(R.string.car_types);
        else if (TextUtils.isEmpty(carCat))
            return ResourceManager.getString(R.string.car_categories);
        else if (TextUtils.isEmpty(carModel))
            return ResourceManager.getString(R.string.making_year);
        else if (TextUtils.isEmpty(tyreImage))
            return ResourceManager.getString(R.string.license_civil_image_hint);
        else if (TextUtils.isEmpty(carMotor))
            return ResourceManager.getString(R.string.car_motor_number);
        else if (TextUtils.isEmpty(frontOwnerImage))
            return ResourceManager.getString(R.string.paper_front);
        else if (TextUtils.isEmpty(backOwnerImage))
            return ResourceManager.getString(R.string.paper_back);
        else if (toLatitude == 0.0 & toLongitude == 0.0)
            return ResourceManager.getString(R.string.detect_location);
        else if (!isEmerengcy && TextUtils.isEmpty(date) && TextUtils.isEmpty(time))
            return ResourceManager.getString(R.string.service_time);
        else
            return null;
    }

    public String isBatteryValid() {
        if (servicesIds.size() == 0)
            return ResourceManager.getString(R.string.service_warning);
        else if (TextUtils.isEmpty(carType))
            return ResourceManager.getString(R.string.car_types);
        else if (TextUtils.isEmpty(carCat))
            return ResourceManager.getString(R.string.car_categories);
        else if (TextUtils.isEmpty(carModel))
            return ResourceManager.getString(R.string.making_year);
        else if (TextUtils.isEmpty(batteryType))
            return ResourceManager.getString(R.string.battery_type);
        else if (TextUtils.isEmpty(batterySize))
            return ResourceManager.getString(R.string.battery_size);
        else if (toLatitude == 0.0 & toLongitude == 0.0)
            return ResourceManager.getString(R.string.detect_location);
        else if (!isEmerengcy && TextUtils.isEmpty(date) && TextUtils.isEmpty(time))
            return ResourceManager.getString(R.string.service_time);
        else
            return null;
    }

    public String isAberBoxValid() {
        if (TextUtils.isEmpty(boxType))
            return ResourceManager.getString(R.string.package_type);
        else if (TextUtils.isEmpty(desc))
            return ResourceManager.getString(R.string.service_info_hint);
        else if (fromLatitude == 0.0 & fromLongitude == 0.0)
            return ResourceManager.getString(R.string.up_location);
        else if (toLatitude == 0.0 & toLongitude == 0.0)
            return ResourceManager.getString(R.string.down_location);
        else if (!isEmerengcy && TextUtils.isEmpty(date) && TextUtils.isEmpty(time))
            return ResourceManager.getString(R.string.service_time);
        else
            return null;
    }

    public String getBoxType() {
        return boxType;
    }

    public void setBoxType(String boxType) {
        this.boxType = boxType;
    }

    public int getBoxTypeId() {
        return boxTypeId;
    }

    public void setBoxTypeId(int boxTypeId) {
        this.boxTypeId = boxTypeId;
    }

    public List<ServicesItem> getServicesIds() {
        return servicesIds;
    }

    public void setServicesIds(List<ServicesItem> servicesIds) {
        this.servicesIds = servicesIds;
    }

    public double getFromLatitude() {
        return fromLatitude;
    }

    public void setFromLatitude(double fromLatitude) {
        this.fromLatitude = fromLatitude;
    }

    public double getFromLongitude() {
        return fromLongitude;
    }

    public void setFromLongitude(double fromLongitude) {
        this.fromLongitude = fromLongitude;
    }

    public String getFromAddress() {
        return fromAddress;
    }

    public void setFromAddress(String fromAddress) {
        this.fromAddress = fromAddress;
    }

    public double getToLatitude() {
        return toLatitude;
    }

    public void setToLatitude(double toLatitude) {
        this.toLatitude = toLatitude;
    }

    public double getToLongitude() {
        return toLongitude;
    }

    public void setToLongitude(double toLongitude) {
        this.toLongitude = toLongitude;
    }

    public String getToAddress() {
        return toAddress;
    }

    public void setToAddress(String toAddress) {
        this.toAddress = toAddress;
    }

    public boolean isEmerengcy() {
        return isEmerengcy;
    }

    public void setEmerengcy(boolean emerengcy) {
        isEmerengcy = emerengcy;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public List<Extra> getExtraList() {
        return extraList;
    }

    public void setExtraList(List<Extra> extraList) {
        this.extraList = extraList;
    }

    public String getCarType() {
        return carType;
    }

    public void setCarType(String carType) {
        this.carType = carType;
    }

    public int getCarTypeId() {
        return carTypeId;
    }

    public void setCarTypeId(int carTypeId) {
        this.carTypeId = carTypeId;
    }

    public String getCarCat() {
        return carCat;
    }

    public void setCarCat(String carCat) {
        this.carCat = carCat;
    }

    public int getCarCatId() {
        return carCatId;
    }

    public void setCarCatId(int carCatId) {
        this.carCatId = carCatId;
    }

    public String getCarModel() {
        return carModel;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public int getCarModelId() {
        return carModelId;
    }

    public void setCarModelId(int carModelId) {
        this.carModelId = carModelId;
    }

    public String getTyerType() {
        return tyerType;
    }

    public void setTyerType(String tyerType) {
        this.tyerType = tyerType;
    }

    public int getTyerTypeId() {
        return tyerTypeId;
    }

    public void setTyerTypeId(int tyerTypeId) {
        this.tyerTypeId = tyerTypeId;
    }

    public String getTyerDesc() {
        return tyerDesc;
    }

    public void setTyerDesc(String tyerDesc) {
        this.tyerDesc = tyerDesc;
    }

    public int getTyerDescId() {
        return tyerDescId;
    }

    public void setTyerDescId(int tyerDescId) {
        this.tyerDescId = tyerDescId;
    }

    public String getTyreImage() {
        return tyreImage;
    }

    public void setTyreImage(String tyreImage) {
        this.tyreImage = tyreImage;
    }

    public String getOilType() {
        return oilType;
    }

    public void setOilType(String oilType) {
        this.oilType = oilType;
    }

    public int getOilTypeId() {
        return oilTypeId;
    }

    public void setOilTypeId(int oilTypeId) {
        this.oilTypeId = oilTypeId;
    }

    public String getOilLiquid() {
        return oilLiquid;
    }

    public void setOilLiquid(String oilLiquid) {
        this.oilLiquid = oilLiquid;
    }

    public int getOilLiquidId() {
        return oilLiquidId;
    }

    public void setOilLiquidId(int oilLiquidId) {
        this.oilLiquidId = oilLiquidId;
    }

    public String getKm() {
        return km;
    }

    public void setKm(String km) {
        this.km = km;
    }

    public int getKmId() {
        return kmId;
    }

    public void setKmId(int kmId) {
        this.kmId = kmId;
    }

    public String getHiddenType() {
        return hiddenType;
    }

    public void setHiddenType(String hiddenType) {
        this.hiddenType = hiddenType;
    }

    public int getHiddenTypeId() {
        return hiddenTypeId;
    }

    public void setHiddenTypeId(int hiddenTypeId) {
        this.hiddenTypeId = hiddenTypeId;
    }

    public String getHiddenColor() {
        return hiddenColor;
    }

    public void setHiddenColor(String hiddenColor) {
        this.hiddenColor = hiddenColor;
    }

    public int getHiddenColorId() {
        return hiddenColorId;
    }

    public void setHiddenColorId(int hiddenColorId) {
        this.hiddenColorId = hiddenColorId;
    }

    public String getHiddenPercentage() {
        return hiddenPercentage;
    }

    public void setHiddenPercentage(String hiddenPercentage) {
        this.hiddenPercentage = hiddenPercentage;
    }

    public int getHiddenPercentageId() {
        return hiddenPercentageId;
    }

    public void setHiddenPercentageId(int hiddenPercentageId) {
        this.hiddenPercentageId = hiddenPercentageId;
    }

    public String getFuelType() {
        return fuelType;
    }

    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }

    public int getFuelTypeId() {
        return fuelTypeId;
    }

    public void setFuelTypeId(int fuelTypeId) {
        this.fuelTypeId = fuelTypeId;
    }

    public String getFuelCat() {
        return fuelCat;
    }

    public void setFuelCat(String fuelCat) {
        this.fuelCat = fuelCat;
    }

    public int getFuelCatId() {
        return fuelCatId;
    }

    public void setFuelCatId(int fuelCatId) {
        this.fuelCatId = fuelCatId;
    }

    public String getLitre() {
        return litre;
    }

    public void setLitre(String litre) {
        this.litre = litre;
    }

    public int getLitreId() {
        return litreId;
    }

    public void setLitreId(int litreId) {
        this.litreId = litreId;
    }

    public String getGallon() {
        return gallon;
    }

    public void setGallon(String gallon) {
        this.gallon = gallon;
    }

    public int getGallonId() {
        return gallonId;
    }

    public void setGallonId(int gallonId) {
        this.gallonId = gallonId;
    }

    public String getTinkerServiceName() {
        return tinkerServiceName;
    }

    public void setTinkerServiceName(String tinkerServiceName) {
        this.tinkerServiceName = tinkerServiceName;
    }

    public int getTinkerServiceId() {
        return tinkerServiceId;
    }

    public void setTinkerServiceId(int tinkerServiceId) {
        this.tinkerServiceId = tinkerServiceId;
    }

    public String getBuildingYear() {
        return buildingYear;
    }

    public void setBuildingYear(String buildingYear) {
        this.buildingYear = buildingYear;
    }

    public String getCarMotor() {
        return carMotor;
    }

    public void setCarMotor(String carMotor) {
        this.carMotor = carMotor;
    }

    public String getBatteryType() {
        return batteryType;
    }

    public void setBatteryType(String batteryType) {
        this.batteryType = batteryType;
    }

    public String getBatteryTypeId() {
        return batteryTypeId;
    }

    public void setBatteryTypeId(String batteryTypeId) {
        this.batteryTypeId = batteryTypeId;
    }

    public String getBatterySize() {
        return batterySize;
    }

    public void setBatterySize(String batterySize) {
        this.batterySize = batterySize;
    }

    public String getBatterySizeId() {
        return batterySizeId;
    }

    public void setBatterySizeId(String batterySizeId) {
        this.batterySizeId = batterySizeId;
    }

    public double getTotalServices() {
        return totalServices;
    }

    public void setTotalServices(double totalServices) {
        this.totalServices = totalServices;
    }

    public double getExtraTotalServices() {
        return extraTotalServices;
    }

    public void setExtraTotalServices(double extraTotalServices) {
        this.extraTotalServices = extraTotalServices;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public double getMovingPriceServices() {
        return movingPriceServices;
    }

    public void setMovingPriceServices(double movingPriceServices) {
        this.movingPriceServices = movingPriceServices;
    }

    public double getEmePriceServices() {
        return emePriceServices;
    }

    public void setEmePriceServices(double emePriceServices) {
        this.emePriceServices = emePriceServices;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public int getMainService() {
        return mainService;
    }

    public void setMainService(int mainService) {
        this.mainService = mainService;
    }

    public String getFrontOwnerImage() {
        return frontOwnerImage;
    }

    public void setFrontOwnerImage(String frontOwnerImage) {
        this.frontOwnerImage = frontOwnerImage;
    }

    public String getBackOwnerImage() {
        return backOwnerImage;
    }

    public void setBackOwnerImage(String backOwnerImage) {
        this.backOwnerImage = backOwnerImage;
    }

    public float getKiloCost() {
        return kiloCost;
    }

    public void setKiloCost(float kiloCost) {
        this.kiloCost = kiloCost;
    }

    public float getTaxes() {
        return taxes;
    }

    public void setTaxes(float taxes) {
        this.taxes = taxes;
    }
}
