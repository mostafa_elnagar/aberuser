package grand.app.aber_user.pages.myOrders.models;

import com.google.gson.annotations.SerializedName;

import grand.app.aber_user.model.base.StatusMessage;

public class MyOrdersResponse extends StatusMessage {

	@SerializedName("data")
	private MyOrdersMainData myOrdersMainData;

	public MyOrdersMainData getMyOrdersMainData(){
		return myOrdersMainData;
	}

}