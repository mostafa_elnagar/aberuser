package grand.app.aber_user.pages.parts.models;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import grand.app.aber_user.pages.myOrders.models.HistoryItem;

@Entity(tableName = "ProductsItem")
public class ProductsItem {

    @SerializedName("image")
    private String image;
    @Ignore
    @SerializedName("is_favorite")
    private boolean isFavorite;
    @Ignore
    @SerializedName("rate")
    private String rate;

    @SerializedName("price")
    private String price;
    @SerializedName("price_item")
    private double priceItem;
    @SerializedName("currency")
    private String currency;

    @SerializedName("name")
    private String name;
    @Ignore
    @SerializedName("created_at")
    private String createdAt;

    @SerializedName("id")
    private int id;
    @SerializedName("quantity")
    private int quantity;
    @SerializedName("stock_quantity")
    @Ignore
    private int stockQuantity;

    @SerializedName("total")
    @Ignore
    private String total;

    @SerializedName("attribute_parent_id")
    private int attributeParentId;

    @SerializedName("attribute_child_id")
    private int attributeChildId;

    @SerializedName("status")
    @Ignore
    private int status;
    @SerializedName("have_attributes")
    @Ignore
    private int attributes;

    @PrimaryKey(autoGenerate = true)
    private int product_room_id;
    private transient boolean isConfirmed;

    public String getImage() {
        return image;
    }

    public boolean isIsFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

    public String getRate() {
        return rate;
    }

    public String getPrice() {
        return price;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public String getCurrency() {
        return currency;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getProduct_room_id() {
        return product_room_id;
    }

    public void setProduct_room_id(int product_room_id) {
        this.product_room_id = product_room_id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPriceItem() {
        return priceItem;
    }

    public void setPriceItem(double priceItem) {
        this.priceItem = priceItem;
    }

    public boolean isConfirmed() {
        return isConfirmed;
    }

    public void setConfirmed(boolean confirmed) {
        isConfirmed = confirmed;
    }

    public int getAttributeParentId() {
        return attributeParentId;
    }

    public void setAttributeParentId(int attributeParentId) {
        this.attributeParentId = attributeParentId;
    }

    public int getAttributeChildId() {
        return attributeChildId;
    }

    public void setAttributeChildId(int attributeChildId) {
        this.attributeChildId = attributeChildId;
    }

    public int getStatus() {
        return status;
    }

    public String getTotal() {
        return total;
    }

    public int getStockQuantity() {
        return stockQuantity;
    }

    public int getAttributes() {
        return attributes;
    }
}