package grand.app.aber_user.pages.conversations.models;

import com.google.gson.annotations.SerializedName;

import grand.app.aber_user.model.base.StatusMessage;


public class ConversationsResponse extends StatusMessage {
    @SerializedName("data")
    private ConversationsMain conversationsMain;

    public ConversationsMain getConversationsMain() {
        return conversationsMain;
    }
}