package grand.app.aber_user.pages.myOrders.viewModels;

import androidx.databinding.Bindable;

import grand.app.aber_user.base.BaseViewModel;
import grand.app.aber_user.pages.myOrders.adapter.MyOrderProductsAdapter;
import grand.app.aber_user.pages.myOrders.models.MyOrderData;
import grand.app.aber_user.utils.Constants;

public class ItemMyOrdersViewModel extends BaseViewModel {
    public MyOrderData productsItem;
    MyOrderProductsAdapter productsAdapter;

    public ItemMyOrdersViewModel(MyOrderData productsItem) {
        this.productsItem = productsItem;
        getProductsAdapter().update(productsItem.getProducts());
    }

    @Bindable
    public MyOrderData getProductsItem() {
        return productsItem;
    }

    @Bindable
    public MyOrderProductsAdapter getProductsAdapter() {
        return this.productsAdapter == null ? this.productsAdapter = new MyOrderProductsAdapter() : this.productsAdapter;
    }

    public void itemAction() {
        getLiveData().setValue(Constants.DELETE);
    }
}
