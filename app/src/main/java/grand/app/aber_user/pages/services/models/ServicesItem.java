package grand.app.aber_user.pages.services.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ServicesItem {

    @SerializedName("image")
    private String image;

    @SerializedName("multi_select")
    private int multiSelect;

    @SerializedName("price")
    private String price;

    @SerializedName("name")
    private String name;

    @SerializedName("currency")
    private String currency;

    @SerializedName("id")
    private int id;
    private boolean checked;
    @SerializedName("sub_services")
    private List<ServicesItem> subServicesList;

    public String getImage() {
        return image;
    }

    public int getMultiSelect() {
        return multiSelect;
    }

    public String getPrice() {
        return price;
    }

    public String getName() {
        return name;
    }

    public String getCurrency() {
        return currency;
    }

    public int getId() {
        return id;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public List<ServicesItem> getSubServicesList() {
        return subServicesList;
    }
}