package grand.app.aber_user.pages.parts.viewModels;


import androidx.databinding.Bindable;

import grand.app.aber_user.base.BaseViewModel;
import grand.app.aber_user.pages.parts.models.productDetails.AttributesItem;
import grand.app.aber_user.utils.Constants;

public class ItemProductColorsViewModel extends BaseViewModel {
    public AttributesItem colorItem;

    public ItemProductColorsViewModel(AttributesItem colorItem) {
        this.colorItem = colorItem;
    }

    @Bindable
    public AttributesItem getColorItem() {
        return colorItem;
    }

    public void itemAction() {
        getLiveData().setValue(Constants.MENu);
    }

}
