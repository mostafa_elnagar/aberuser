package grand.app.aber_user.pages.home.models;

import com.google.gson.annotations.SerializedName;

public class HomeSliderData {
    @SerializedName("id")
    private int id;
    @SerializedName("service_id")
    private int serviceId;
    @SerializedName("image")
    private String image;
    @SerializedName("service_name")
    private String serviceName;

    public int getId() {
        return id;
    }

    public int getServiceId() {
        return serviceId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public String getImage() {
        return image;
    }
}
