package grand.app.aber_user.pages.favorites.viewModels;


import androidx.databinding.Bindable;

import grand.app.aber_user.base.BaseViewModel;
import grand.app.aber_user.pages.home.models.HomeServices;

public class ItemFavoritesViewModel extends BaseViewModel {
    public HomeServices postData;

    public ItemFavoritesViewModel(HomeServices postData) {
        this.postData = postData;
    }

    @Bindable
    public HomeServices getPostData() {
        return postData;
    }

    public void itemAction(String action) {
        getLiveData().setValue(action);
    }

}
