package grand.app.aber_user.pages.parts.models.productDetails;

import com.google.gson.annotations.SerializedName;

public class SizesItem {

    @SerializedName("name")
    private String name;
    @SerializedName("currency")
    private String currency;
    @SerializedName("price")
    private String price;
    private boolean isChecked;
    @SerializedName("id")
    private int id;

    public String getName() {
        return name;
    }

    public String getCurrency() {
        return currency;
    }

    public String getPrice() {
        return price;
    }

    public int getId() {
        return id;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }
}