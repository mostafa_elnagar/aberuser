package grand.app.aber_user.pages.services;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;

import com.directions.route.AbstractRouting;
import com.directions.route.Route;
import com.directions.route.RouteException;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Objects;

import javax.inject.Inject;

import grand.app.aber_user.PassingObject;
import grand.app.aber_user.R;
import grand.app.aber_user.base.BaseFragment;
import grand.app.aber_user.base.IApplicationComponent;
import grand.app.aber_user.base.MyApplication;
import grand.app.aber_user.databinding.FragmentConfirmOrderBinding;
import grand.app.aber_user.databinding.OrderSuccessDialogBinding;
import grand.app.aber_user.model.base.Mutable;
import grand.app.aber_user.pages.services.models.ServiceOrderDetails;
import grand.app.aber_user.pages.services.viewModels.ServiceConfirmOrderViewModels;
import grand.app.aber_user.utils.Constants;
import grand.app.aber_user.utils.helper.MovementHelper;


public class FragmentConfirmOrder extends BaseFragment implements RoutingListener {
    @Inject
    ServiceConfirmOrderViewModels viewModel;
    FragmentConfirmOrderBinding binding;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_confirm_order, container, false);
        IApplicationComponent component = ((MyApplication) requireActivity().getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setViewModel(viewModel);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            String passingObject = bundle.getString(Constants.BUNDLE);
            viewModel.setPassingObject(new Gson().fromJson(passingObject, PassingObject.class));
            viewModel.setServiceOrderRequest(new Gson().fromJson(String.valueOf(viewModel.getPassingObject().getObjectClass()), ServiceOrderDetails.class));
        }
        setEvent();
        return binding.getRoot();
    }

    private void setEvent() {
        viewModel.liveData.observe(requireActivity(), (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            viewModel.setMessage(mutable.message.equals(Constants.HIDE_PROGRESS) ? mutable.message : "");
            if (Constants.SERVICE_ORDER.equals(((Mutable) o).message)) {
                showSuccessDialog();
            } else if (Constants.DISTANCE.equals(((Mutable) o).message)) {
               findDistance();
            }
        });
    }

    private void showSuccessDialog() {
        Dialog deleteDialog = new Dialog(requireContext(), R.style.PauseDialog);
        deleteDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Objects.requireNonNull(deleteDialog.getWindow()).getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        deleteDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        OrderSuccessDialogBinding binding = DataBindingUtil.inflate(LayoutInflater.from(deleteDialog.getContext()), R.layout.order_success_dialog, null, false);
        deleteDialog.setContentView(binding.getRoot());
        binding.rcFilter.setText(getString(R.string.thanks));
        binding.tvDesc.setText(getString(R.string.service_order_success));
        deleteDialog.setOnCancelListener(dialog -> MovementHelper.finishWithResult(new PassingObject(), requireActivity(), Constants.ORDER_REQUEST));
        deleteDialog.show();
    }

    @Override
    public void onResume() {
        super.onResume();
        viewModel.getPostRepository().setLiveData(viewModel.liveData);
    }

    // function to find Routes.
    private void findDistance() {
        Routing routing;//also define your api key here.
        routing = new Routing.Builder()
                .travelMode(AbstractRouting.TravelMode.DRIVING)
                .withListener(this)
                .alternativeRoutes(true)
                .waypoints(new LatLng(viewModel.getServiceOrderRequest().getFromLatitude(), viewModel.getServiceOrderRequest().getFromLongitude()), new LatLng(viewModel.getServiceOrderRequest().getToLatitude(), viewModel.getServiceOrderRequest().getToLongitude()))
                .key(getResources().getString(R.string.google_map))  //also define your api key here.
                .build();
        routing.execute();
    }

    @Override
    public void onRoutingFailure(RouteException e) {
        findDistance();
    }

    @Override
    public void onRoutingStart() {
    }

    @Override
    public void onRoutingSuccess(ArrayList<Route> route, int shortestRouteIndex) {
        for (int i = 0; i < route.size(); i++) {
            if (i == shortestRouteIndex) {
                viewModel.getCreateServiceOrder().setDistance((float) (route.get(i).getDistanceValue() / 1000));
            }
        }
        viewModel.calcTransferPrice();
    }

    @Override
    public void onRoutingCancelled() {
        findDistance();
    }

}
