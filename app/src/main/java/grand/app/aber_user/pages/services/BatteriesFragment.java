package grand.app.aber_user.pages.services;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;

import com.google.gson.Gson;

import java.io.File;

import javax.inject.Inject;

import grand.app.aber_user.BR;
import grand.app.aber_user.PassingObject;
import grand.app.aber_user.R;
import grand.app.aber_user.base.BaseFragment;
import grand.app.aber_user.base.IApplicationComponent;
import grand.app.aber_user.base.MyApplication;
import grand.app.aber_user.connection.FileObject;
import grand.app.aber_user.databinding.FragmentBatteriesBinding;
import grand.app.aber_user.model.DropDownResponse;
import grand.app.aber_user.model.base.Mutable;
import grand.app.aber_user.pages.services.models.ServiceDetailsResponse;
import grand.app.aber_user.pages.services.viewModels.ServicesViewModels;
import grand.app.aber_user.utils.Constants;
import grand.app.aber_user.utils.PopUp.PopUpMenuHelper;
import grand.app.aber_user.utils.helper.LauncherHelper;
import grand.app.aber_user.utils.helper.MovementHelper;
import grand.app.aber_user.utils.upload.FileOperations;


public class BatteriesFragment extends BaseFragment {
    @Inject
    ServicesViewModels viewModel;
    FragmentBatteriesBinding binding;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_batteries, container, false);
        IApplicationComponent component = ((MyApplication) requireActivity().getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setViewModel(viewModel);
        binding.searchLocation.setIconTint(null);
        binding.picTime.setIconTint(null);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            String passingObject = bundle.getString(Constants.BUNDLE);
            viewModel.setPassingObject(new Gson().fromJson(passingObject, PassingObject.class));
            viewModel.getServiceDetails();
        }
        setEvent();
        return binding.getRoot();
    }

    private void setEvent() {
        viewModel.liveData.observe(requireActivity(), (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            viewModel.setMessage(mutable.message.equals(Constants.HIDE_PROGRESS) ? mutable.message : "");
            switch (((Mutable) o).message) {
                case Constants.SERVICE_DETAILS:
                    viewModel.setDetails(((ServiceDetailsResponse) (mutable).object).getData());
                    break;
                case Constants.SEARCH_LOCATION:
                    MovementHelper.startMapActivityForResultWithBundle(requireActivity(), new PassingObject(), getResources().getString(R.string.detect_location), Constants.LOCATION_REQUEST);
                    break;
                case Constants.IMAGE:
                    LauncherHelper.execute(LauncherHelper.storage);
                    break;
                case Constants.CAR_TYPE:
                    showCarType();
                    break;
                case Constants.CAR_CAT:
                    showCarCat();
                    break;
                case Constants.CAR_MODEL:
                    showCarModel();
                    break;
                case Constants.BATTERY_TYPE:
                    showBatteryType();
                    break;
                case Constants.BATTERY_SIZE:
                    showBatterySize();
                    break;
                case Constants.BATTERY_SIZE_REQUEST:
                    viewModel.batterySizeList = ((DropDownResponse) mutable.object).getDropDownsObjects();
                    break;
                case Constants.DELIVERY_TIME:
                    MovementHelper.startActivityForResultWithBundle(requireActivity(), new PassingObject(), null, ChooseServiceTimeFragment.class.getName(), Constants.DELIVERY_TIME_REQUEST);
                    break;
                case Constants.ORDER:
                    MovementHelper.startActivityForResultWithBundle(requireActivity(), new PassingObject(viewModel.getServiceOrderRequest()), null, FragmentConfirmOrder.class.getName(), Constants.ORDER_REQUEST);
                    break;
                case Constants.CHILD_VEHICLE:
                    if (viewModel.getServiceOrderRequest().getCarCat() == null)
                        viewModel.carCatList = ((DropDownResponse) mutable.object).getDropDownsObjects();
                    if (viewModel.getServiceOrderRequest().getCarModel() == null)
                        viewModel.carModelList = ((DropDownResponse) mutable.object).getDropDownsObjects();
                    break;

            }
        });
    }


    private void showBatteryType() {
        PopUpMenuHelper.showCarTypePopUp(requireActivity(), binding.inputBatteryType, viewModel.getDetails().getBatteryList()).setOnMenuItemClickListener(item -> {
            viewModel.getServiceOrderRequest().setBatteryType(viewModel.getDetails().getBatteryList().get(item.getItemId()).getName());
            viewModel.getServiceOrderRequest().setBatteryTypeId(String.valueOf(viewModel.getDetails().getBatteryList().get(item.getItemId()).getId()));
            viewModel.getBatteryData(viewModel.getDetails().getBatteryList().get(item.getItemId()).getId());
            viewModel.notifyChange(BR.serviceOrderRequest);
            return false;
        });
    }

    private void showBatterySize() {
        PopUpMenuHelper.showCarTypePopUp(requireActivity(), binding.inputBatterySize, viewModel.batterySizeList).setOnMenuItemClickListener(item -> {
            viewModel.getServiceOrderRequest().setBatterySize(viewModel.batterySizeList.get(item.getItemId()).getName());
            viewModel.getServiceOrderRequest().setBatterySizeId(String.valueOf(viewModel.batterySizeList.get(item.getItemId()).getId()));
            viewModel.getServiceOrderRequest().setExtraTotalServices(Double.parseDouble(viewModel.batterySizeList.get(item.getItemId()).getPrice()));
            viewModel.notifyChange(BR.serviceOrderRequest);
            return false;
        });
    }

    private void showCarModel() {
        PopUpMenuHelper.showCarTypePopUp(requireActivity(), binding.inputCarModels, viewModel.carModelList).setOnMenuItemClickListener(item -> {
            viewModel.getServiceOrderRequest().setCarModel(viewModel.carModelList.get(item.getItemId()).getName());
            viewModel.getServiceOrderRequest().setCarModelId(viewModel.carModelList.get(item.getItemId()).getId());
            viewModel.notifyChange(BR.serviceOrderRequest);
            return false;
        });
    }

    private void showCarCat() {
        PopUpMenuHelper.showCarTypePopUp(requireActivity(), binding.inputCarCategories, viewModel.carCatList).setOnMenuItemClickListener(item -> {
            viewModel.getServiceOrderRequest().setCarCat(viewModel.carCatList.get(item.getItemId()).getName());
            viewModel.getServiceOrderRequest().setCarCatId(viewModel.carCatList.get(item.getItemId()).getId());
            viewModel.getCarChildData(viewModel.carCatList.get(item.getItemId()).getId());
            viewModel.notifyChange(BR.serviceOrderRequest);
            return false;
        });
    }

    private void showCarType() {
        PopUpMenuHelper.showCarTypePopUp(requireActivity(), binding.inputCarTypes, viewModel.getDetails().getVehicleList()).setOnMenuItemClickListener(item -> {
            viewModel.getServiceOrderRequest().setCarType(viewModel.getDetails().getVehicleList().get(item.getItemId()).getName());
            viewModel.getServiceOrderRequest().setCarTypeId(viewModel.getDetails().getVehicleList().get(item.getItemId()).getId());
            viewModel.getCarChildData(viewModel.getDetails().getVehicleList().get(item.getItemId()).getId());
            viewModel.notifyChange(BR.serviceOrderRequest);
            return false;
        });
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LauncherHelper.checkPermission(this, Constants.FILE_TYPE_IMAGE, (request, result) -> {
            if (result)
                pickImageDialogSelect(request);
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        viewModel.getPostRepository().setLiveData(viewModel.liveData);
    }

    @Override
    public void launchActivityResult(int request, int resultCode, Intent result) {
        super.launchActivityResult(request, resultCode, result);
        if (request == Constants.LOCATION_REQUEST) {
            viewModel.getServiceOrderRequest().setFromLatitude(result.getDoubleExtra(Constants.LAT, 0.0));
            viewModel.getServiceOrderRequest().setFromLongitude(result.getDoubleExtra(Constants.LNG, 0.0));
            viewModel.getServiceOrderRequest().setFromAddress(result.getStringExtra(Constants.ADDRESS));
        } else if (request == Constants.DELIVERY_TIME_REQUEST) {
            Bundle bundle = result.getBundleExtra(Constants.BUNDLE);
            if (bundle != null && bundle.containsKey(Constants.BUNDLE)) {
                PassingObject passingObject = (PassingObject) bundle.getSerializable(Constants.BUNDLE);
                viewModel.getServiceOrderRequest().setTime(passingObject.getObject2());
                viewModel.getServiceOrderRequest().setDate(passingObject.getObject());
            }
        } else if (request == Constants.ORDER_REQUEST) {
            finishActivity();
        } else if (request == Constants.FILE_TYPE_IMAGE) {
            toastMessage(getString(R.string.image_selected));
            FileObject fileObject = FileOperations.getFileObject(requireActivity(), result, Constants.IMAGE, Constants.FILE_TYPE_IMAGE);
            binding.icBattery.setImageURI(Uri.parse(String.valueOf(new File(fileObject.getFilePath()))));
            viewModel.getServiceOrderRequest().setTyreImage(fileObject.getFilePath());
            viewModel.notifyChange(BR.serviceOrderRequest);
        }
    }
}
