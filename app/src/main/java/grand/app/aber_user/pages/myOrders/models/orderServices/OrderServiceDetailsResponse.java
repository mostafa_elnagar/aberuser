package grand.app.aber_user.pages.myOrders.models.orderServices;

import com.google.gson.annotations.SerializedName;

import grand.app.aber_user.model.base.StatusMessage;

public class OrderServiceDetailsResponse extends StatusMessage {
    @SerializedName("data")
    private OrderDetailsMain orderDetailsMain;

    public OrderDetailsMain getOrderDetailsMain() {
        return orderDetailsMain;
    }
}
