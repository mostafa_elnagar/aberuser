package grand.app.aber_user.pages.cart.models;

import com.google.gson.annotations.SerializedName;

public class PromoData {

    @SerializedName("id")
    private int id;

    @SerializedName("amount")
    private String discount;
    @SerializedName("promo_type")
    private String promo_type;

    public int getId() {
        return id;
    }

    public String getDiscount() {
        return discount;
    }

    public String getPromo_type() {
        return promo_type;
    }
}