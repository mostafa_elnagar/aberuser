package grand.app.aber_user.pages.favorites;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;

import javax.inject.Inject;

import grand.app.aber_user.BR;
import grand.app.aber_user.R;
import grand.app.aber_user.base.BaseFragment;
import grand.app.aber_user.base.IApplicationComponent;
import grand.app.aber_user.base.MyApplication;
import grand.app.aber_user.databinding.FragmentFavoritesBinding;
import grand.app.aber_user.model.base.Mutable;
import grand.app.aber_user.pages.favorites.models.FavoritesResponse;
import grand.app.aber_user.pages.favorites.viewModels.FavoritesViewModels;
import grand.app.aber_user.utils.Constants;

public class FavoritesFragment extends BaseFragment {
    @Inject
    FavoritesViewModels viewModel;
    FragmentFavoritesBinding binding;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_favorites, container, false);
        IApplicationComponent component = ((MyApplication) requireContext().getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setViewModel(viewModel);
        viewModel.getFavorites();
        setEvent();
        return binding.getRoot();
    }

    private void setEvent() {
        viewModel.liveData.observe((LifecycleOwner) requireContext(), (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            viewModel.setMessage(mutable.message.equals(Constants.HIDE_PROGRESS) ? mutable.message : "");
            if (Constants.FAVORITE.equals(((Mutable) o).message)) {
                viewModel.getProductsAdapter().update(((FavoritesResponse) mutable.object).getProductsItemList());
                viewModel.notifyChange(BR.productsAdapter);
            } else if (Constants.LIKES_REACTION.equals(((Mutable) o).message)) {
                viewModel.getProductsAdapter().getProductsItemList().remove(viewModel.getProductsAdapter().lastPosition);
                viewModel.getProductsAdapter().notifyDataSetChanged();
            }
        });
        viewModel.getProductsAdapter().liveData.observe(requireActivity(), integer -> viewModel.changeLike(integer));
    }

    @Override
    public void onResume() {
        super.onResume();
        viewModel.getPostRepository().setLiveData(viewModel.liveData);
    }
}
