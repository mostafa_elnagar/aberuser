package grand.app.aber_user.pages.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;

import javax.inject.Inject;

import grand.app.aber_user.PassingObject;
import grand.app.aber_user.R;
import grand.app.aber_user.base.BaseFragment;
import grand.app.aber_user.base.IApplicationComponent;
import grand.app.aber_user.base.MyApplication;
import grand.app.aber_user.databinding.FragmentHomeBinding;
import grand.app.aber_user.model.base.Mutable;
import grand.app.aber_user.pages.home.models.HomeResponse;
import grand.app.aber_user.pages.home.viewModels.HomeViewModels;
import grand.app.aber_user.pages.favorites.FavoritesFragment;
import grand.app.aber_user.utils.Constants;
import grand.app.aber_user.utils.helper.MovementHelper;

public class HomeFragment extends BaseFragment {
    @Inject
    HomeViewModels viewModel;
    FragmentHomeBinding binding;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false);
        IApplicationComponent component = ((MyApplication) requireContext().getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setViewModel(viewModel);
        viewModel.home();
        setEvent();
        return binding.getRoot();
    }

    private void setEvent() {
        viewModel.liveData.observe((LifecycleOwner) requireContext(), (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            switch (((Mutable) o).message) {
                case Constants.HOME:
                    viewModel.setMainData(((HomeResponse) (mutable).object).getMainData());
                    viewModel.setupSlider(binding.imageSlider);
                    mainActivity().navigationDrawerView.menuViewModel.setMainData(viewModel.getMainData());
                    break;
                case Constants.NEW_RATE:
                    MovementHelper.startActivityForResultWithBundle(requireContext(), new PassingObject(), null, FavoritesFragment.class.getName(), Constants.LOCATION_REQUEST);
                    break;
                case Constants.SEARCH:
                    MovementHelper.startActivity(requireContext(), SearchFragment.class.getName(), getString(R.string.search), null);
                    break;

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        viewModel.getPostRepository().setLiveData(viewModel.liveData);
        mainActivity().enableRefresh(false);
    }
}
