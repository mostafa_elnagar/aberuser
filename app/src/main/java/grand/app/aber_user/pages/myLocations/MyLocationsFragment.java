package grand.app.aber_user.pages.myLocations;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;

import com.google.gson.Gson;

import javax.inject.Inject;

import grand.app.aber_user.BR;
import grand.app.aber_user.PassingObject;
import grand.app.aber_user.R;
import grand.app.aber_user.base.BaseFragment;
import grand.app.aber_user.base.IApplicationComponent;
import grand.app.aber_user.base.MyApplication;
import grand.app.aber_user.databinding.FragmentMyLocationBinding;
import grand.app.aber_user.model.base.Mutable;
import grand.app.aber_user.model.base.StatusMessage;
import grand.app.aber_user.pages.myLocations.models.LocationsData;
import grand.app.aber_user.pages.myLocations.models.LocationsResponse;
import grand.app.aber_user.pages.myLocations.viewModels.MyLocationsViewModel;
import grand.app.aber_user.utils.Constants;
import grand.app.aber_user.utils.helper.MovementHelper;

public class MyLocationsFragment extends BaseFragment {
    FragmentMyLocationBinding binding;
    @Inject
    MyLocationsViewModel viewModel;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_my_location, container, false);
        IApplicationComponent component = ((MyApplication) requireActivity().getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setViewmodel(viewModel);
        viewModel.getLocations();
        setEvent();
        return binding.getRoot();
    }

    private void setEvent() {
        viewModel.liveData.observe(requireActivity(), (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            if (Constants.MY_LOCATIONS.equals(((Mutable) o).message)) {
                viewModel.getLocationsAdapters().updateData(((LocationsResponse) mutable.object).getData(), true);
                viewModel.notifyChange(BR.locationsAdapters);
            } else if (Constants.ADD_PLACE.equals(((Mutable) o).message)) {
                MovementHelper.startActivityForResultWithBundle(requireActivity(), new PassingObject(), getString(R.string.add_place), AddPlaceFragment.class.getName(), Constants.LOCATION_REQUEST);
            } else if (Constants.DELETE.equals(((Mutable) o).message)) {
                toastMessage(((StatusMessage) mutable.object).mMessage);
                viewModel.getLocationsAdapters().locationsDataList.remove(viewModel.getLocationsAdapters().position);
                viewModel.getLocationsAdapters().notifyDataSetChanged();
                viewModel.notifyChange(BR.locationsAdapters);
            }
        });
        viewModel.getLocationsAdapters().locationsDataMutableLiveData.observe(requireActivity(), locationsData -> showAlertCancel(locationsData.getId()));
    }

    public void showAlertCancel(int locationId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(requireActivity());
        builder
                .setMessage(getString(R.string.delete_title))
                .setCancelable(false)
                .setNegativeButton(getString(R.string.cancel), (dialog, which) -> dialog.cancel())
                .setPositiveButton(getString(R.string.send), (dialog, id) -> viewModel.deleteLocation(locationId));
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    public void launchActivityResult(int request, int resultCode, Intent result) {
        super.launchActivityResult(request, resultCode, result);
        if (result != null) {
            if (request == Constants.LOCATION_REQUEST) {
                Bundle bundle = result.getBundleExtra(Constants.BUNDLE);
                if (bundle != null && bundle.containsKey(Constants.BUNDLE)) {
                    PassingObject passingObject = (PassingObject) bundle.getSerializable(Constants.BUNDLE);
                    viewModel.getLocationsAdapters().getLocationsDataList().add(new Gson().fromJson(String.valueOf(passingObject.getObjectClass()), LocationsData.class));
                    viewModel.getLocationsAdapters().notifyItemInserted(viewModel.getLocationsAdapters().getLocationsDataList().size() - 1);
                    binding.rcLocations.scrollToPosition(viewModel.getLocationsAdapters().getLocationsDataList().size() - 1);
                    viewModel.notifyChange(BR.locationsAdapters);
                }
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        viewModel.getRepository().setLiveData(viewModel.liveData);
    }
}