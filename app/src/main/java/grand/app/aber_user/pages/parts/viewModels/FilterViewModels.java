package grand.app.aber_user.pages.parts.viewModels;

import androidx.databinding.Bindable;
import androidx.databinding.ObservableBoolean;
import androidx.lifecycle.MutableLiveData;
import javax.inject.Inject;
import grand.app.aber_user.BR;
import grand.app.aber_user.R;
import grand.app.aber_user.base.BaseViewModel;
import grand.app.aber_user.base.MyApplication;
import grand.app.aber_user.model.base.Mutable;
import grand.app.aber_user.pages.parts.adapters.CarAdapter;
import grand.app.aber_user.pages.parts.adapters.ProductColorsAdapter;
import grand.app.aber_user.pages.parts.models.filter.FilterData;
import grand.app.aber_user.pages.parts.models.filter.FilterRequest;
import grand.app.aber_user.repository.ServicesRepository;
import grand.app.aber_user.utils.Constants;
import grand.app.aber_user.utils.resources.ResourceManager;
import grand.app.aber_user.utils.session.UserHelper;
import io.reactivex.disposables.CompositeDisposable;

public class FilterViewModels extends BaseViewModel {

    public MutableLiveData<Mutable> liveData;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    ProductColorsAdapter colorsAdapter;
    CarAdapter carTypeAdapter, carCatAdapter, carModelAdapter;
    FilterData filterData;
    @Inject
    ServicesRepository postRepository;
    public ObservableBoolean carTypeVis = new ObservableBoolean();
    public ObservableBoolean carCatVis = new ObservableBoolean();
    public ObservableBoolean carModelVis = new ObservableBoolean();
    public ObservableBoolean colorVis = new ObservableBoolean();
    FilterRequest filterRequest;

    @Inject
    public FilterViewModels(ServicesRepository postRepository) {
        this.postRepository = postRepository;
        this.liveData = new MutableLiveData<>();
        postRepository.setLiveData(liveData);
    }

    public void getFilter() {
        compositeDisposable.add(postRepository.getFilterData(getPassingObject().getId(), UserHelper.getInstance(MyApplication.getInstance()).getCountriesData().getId()));
    }

    public void getCarModel(int childId) {
        compositeDisposable.add(postRepository.getCarModel(childId));
    }

    public void getCarCat(int childId) {
        compositeDisposable.add(postRepository.getCarChildData(childId));
    }

    public void filter() {
        getFilterRequest().setIsNew(getPassingObject().getObject());
        getFilterRequest().setSub_service_id(String.valueOf(getPassingObject().getId()));
        getFilterRequest().setCountry_id(String.valueOf(UserHelper.getInstance(MyApplication.getInstance()).getCountriesData().getId()));
        if (getCarModelAdapter().lastSelected >= 1) {
            getFilterRequest().setChild_service_ids(String.valueOf(getCarModelAdapter().lastSelected));
            getFilterRequest().setParent_id(String.valueOf(getColorsAdapter().lastSelected));
            compositeDisposable.add(postRepository.filter(getFilterRequest()));
        } else
            liveData.setValue(new Mutable(Constants.ERROR_TOAST, ResourceManager.getString(R.string.making_year)));

    }

    public void liveDataActions(String action) {
        if (action.equals(Constants.CAR_TYPE))
            carTypeVis.set(!carTypeVis.get());
        if (action.equals(Constants.CAR_CAT))
            carCatVis.set(!carCatVis.get());
        if (action.equals(Constants.CAR_MODEL))
            carModelVis.set(!carModelVis.get());
        if (action.equals(Constants.HIDDEN_COLOR_DROP))
            colorVis.set(!carModelVis.get());
    }

    @Bindable
    public FilterData getFilterData() {
        return this.filterData == null ? this.filterData = new FilterData() : this.filterData;
    }

    public void setFilterData(FilterData filterData) {
        getCarTypeAdapter().update(filterData.getVehicle());
        getColorsAdapter().update(filterData.getAttributes());
        notifyChange(BR.carTypeAdapter);
        notifyChange(BR.colorsAdapter);
        notifyChange(BR.filterData);
        this.filterData = filterData;
    }

    @Bindable
    public FilterRequest getFilterRequest() {
        return this.filterRequest == null ? this.filterRequest = new FilterRequest() : this.filterRequest;
    }

    @Bindable
    public ProductColorsAdapter getColorsAdapter() {
        return this.colorsAdapter == null ? this.colorsAdapter = new ProductColorsAdapter() : this.colorsAdapter;
    }

    @Bindable
    public CarAdapter getCarTypeAdapter() {
        return this.carTypeAdapter == null ? this.carTypeAdapter = new CarAdapter() : this.carTypeAdapter;
    }

    @Bindable
    public CarAdapter getCarCatAdapter() {
        return this.carCatAdapter == null ? this.carCatAdapter = new CarAdapter() : this.carCatAdapter;
    }

    @Bindable
    public CarAdapter getCarModelAdapter() {
        return this.carModelAdapter == null ? this.carModelAdapter = new CarAdapter() : this.carModelAdapter;
    }

    public ServicesRepository getPostRepository() {
        return postRepository;
    }


    protected void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        unSubscribeFromObservable();
    }

}
