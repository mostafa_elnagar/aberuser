package grand.app.aber_user.pages.myOrders.models;

import java.util.List;

import com.google.gson.annotations.SerializedName;

import grand.app.aber_user.pages.parts.models.ProductsItem;

public class MyOrderDetails {

    @SerializedName("addresses")
    private String addresses;

    @SerializedName("total")
    private double total;

    @SerializedName("delivery_fees")
    private double deliveryFees;

    @SerializedName("phone")
    private String phone;

    @SerializedName("subtotal")
    private String subtotal;

    @SerializedName("order_number")
    private String orderNumber;

    @SerializedName("name")
    private String name;

    @SerializedName("discount")
    private String discount;

    @SerializedName("created_at")
    private String createdAt;

    @SerializedName("pay_type")
    private String payType;

    @SerializedName("status")
    private int status;

    @SerializedName("products")
    private List<ProductsItem> products;

    public String getAddresses() {
        return addresses;
    }

    public double getTotal() {
        return total;
    }

    public double getDeliveryFees() {
        return deliveryFees;
    }

    public String getPhone() {
        return phone;
    }

    public String getSubtotal() {
        return subtotal;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public String getName() {
        return name;
    }

    public String getDiscount() {
        return discount;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getPayType() {
        return payType;
    }

    public int getStatus() {
        return status;
    }

    public List<ProductsItem> getProducts() {
        return products;
    }
}