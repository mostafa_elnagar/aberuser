package grand.app.aber_user.pages.myOrders.models.orderServices;

import java.util.List;

import com.google.gson.annotations.SerializedName;

import grand.app.aber_user.pages.auth.models.UserData;
import grand.app.aber_user.pages.reviews.models.RatesItem;

public class ServicesOrderDetails {

    @SerializedName("canceled")
    private int canceled;
    @SerializedName("service_name")
    private String serviceName;

    @SerializedName("sub_services")
    private List<String> subServices;

    @SerializedName("id")
    private int id;

    @SerializedName("history")
    private int history;

    @SerializedName("cost")
    private double cost;

    @SerializedName("is_emergency")
    private int isEmergency;

    @SerializedName("status")
    private String status;
    private String finishCode;

    @SerializedName("scheduled_at")
    private String scheduledAt;
    @SerializedName("address")
    private String address;
    @SerializedName("longitude")
    private String longitude;

    @SerializedName("latitude")
    private String latitude;

    @SerializedName("provider")
    private UserData provider;
    @SerializedName("review")
    private RatesItem review;

    public int getCanceled() {
        return canceled;
    }

    public String getServiceName() {
        return serviceName;
    }

    public List<String> getSubServices() {
        return subServices;
    }

    public int getId() {
        return id;
    }

    public int getHistory() {
        return history;
    }

    public int getIsEmergency() {
        return isEmergency;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFinishCode() {
        return finishCode;
    }

    public void setFinishCode(String finishCode) {
        this.finishCode = finishCode;
    }

    public String getScheduledAt() {
        return scheduledAt;
    }

    public String getAddress() {
        return address;
    }

    public double getCost() {
        return cost;
    }

    public UserData getProvider() {
        return provider;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public RatesItem getReview() {
        return review;
    }

    public void setReview(RatesItem review) {
        this.review = review;
    }
}