package grand.app.aber_user.pages.myOrders.viewModels;

import android.widget.RatingBar;

import androidx.databinding.Bindable;
import androidx.databinding.ObservableBoolean;
import androidx.lifecycle.MutableLiveData;

import javax.inject.Inject;

import grand.app.aber_user.BR;
import grand.app.aber_user.R;
import grand.app.aber_user.base.BaseViewModel;
import grand.app.aber_user.base.MyApplication;
import grand.app.aber_user.model.base.Mutable;
import grand.app.aber_user.pages.myOrders.adapter.HomeServicesAdapter;
import grand.app.aber_user.pages.myOrders.adapter.MyServicesOrdersAdapter;
import grand.app.aber_user.pages.myOrders.adapter.ServicesRequiredAdapter;
import grand.app.aber_user.pages.myOrders.models.orderServices.MyOrdersServicesMainData;
import grand.app.aber_user.pages.myOrders.models.orderServices.OrderDetailsMain;
import grand.app.aber_user.pages.reviews.models.RateRequest;
import grand.app.aber_user.pages.reviews.models.RatesItem;
import grand.app.aber_user.repository.ServicesRepository;
import grand.app.aber_user.utils.Constants;
import grand.app.aber_user.utils.helper.AppHelper;
import grand.app.aber_user.utils.resources.ResourceManager;
import grand.app.aber_user.utils.session.UserHelper;
import io.reactivex.disposables.CompositeDisposable;

public class MyOrdersServicesViewModels extends BaseViewModel {
    public MutableLiveData<Mutable> liveData;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    @Inject
    ServicesRepository servicesRepository;
    public ObservableBoolean searchProgressVisible = new ObservableBoolean();
    public ObservableBoolean orderStatus = new ObservableBoolean();
    MyOrdersServicesMainData mainData;
    MyServicesOrdersAdapter myOrdersAdapter;
    OrderDetailsMain myOrderDetails;
    ServicesRequiredAdapter servicesRequiredAdapter;
    HomeServicesAdapter homeServicesAdapter;
    public String status;
    RateRequest rateRequest;

    @Inject
    public MyOrdersServicesViewModels(ServicesRepository servicesRepository) {
        this.servicesRepository = servicesRepository;
        this.liveData = new MutableLiveData<>();
        servicesRepository.setLiveData(liveData);
    }

    public void myOrders(int status, int page, boolean showProgress) {
        orderStatus.set(status != 0);
        getMyOrdersAdapter().getOrderDetailsList().clear();
        compositeDisposable.add(servicesRepository.myOrderServices(status, page, showProgress));
    }

    public void myOrdersByCat(int page, int catId, boolean showProgress) {
        getMyOrdersAdapter().getOrderDetailsList().clear();
        if (catId != 0)
            compositeDisposable.add(servicesRepository.myOrderServicesByCat(orderStatus.get() ? 1 : 0, catId, page, showProgress));
        else
            myOrders(orderStatus.get() ? 1 : 0, page, showProgress);
    }

    public void orderDetails() {
        compositeDisposable.add(servicesRepository.myOrderServiceDetails(getPassingObject().getId()));
    }

    public void cancelOrder() {
        compositeDisposable.add(servicesRepository.cancelOrder(getPassingObject().getId(), UserHelper.getInstance(MyApplication.getInstance()).getCountriesData().getId()));
    }

    public void generateCode() {
        compositeDisposable.add(servicesRepository.generateCode(getPassingObject().getId(), getMyOrderDetails().getOrders().getProvider().getId()));
    }

    public void sendRate() {
        getRateRequest().setOrderServiceId(String.valueOf(getPassingObject().getId()));
        if (getRateRequest().isValid()) {
            setMessage(Constants.SHOW_PROGRESS);
            compositeDisposable.add(servicesRepository.rateServiceProvider(getRateRequest()));
        }
    }

    public void setMyOrderDetails(OrderDetailsMain myOrderDetails) {
        getServicesRequiredAdapter().update(myOrderDetails.getOrders().getSubServices());
        notifyChange(BR.servicesRequiredAdapter);
        if (myOrderDetails.getOrders().getCanceled() == 1) {
            myOrderDetails.getOrders().setStatus(ResourceManager.getString(R.string.cancel_service));
        } else {
            if (myOrderDetails.getOrders().getStatus() == null)
                myOrderDetails.getOrders().setStatus(ResourceManager.getString(R.string.service_not_accepted));
            else if (myOrderDetails.getOrders().getStatus().equals("1"))
                myOrderDetails.getOrders().setStatus(ResourceManager.getString(R.string.order_accepted));
            else if (myOrderDetails.getOrders().getStatus().equals("2"))
                myOrderDetails.getOrders().setStatus(ResourceManager.getString(R.string.on_way));
            else if (myOrderDetails.getOrders().getStatus().equals("3"))
                myOrderDetails.getOrders().setStatus(ResourceManager.getString(R.string.order_arrived));
            else if (myOrderDetails.getOrders().getStatus().equals("4"))
                myOrderDetails.getOrders().setStatus(ResourceManager.getString(R.string.order_finished));
        }
        notifyChange(BR.myOrderDetails);
        this.myOrderDetails = myOrderDetails;
    }

    public void updateReview() {
        RatesItem ratesItem = new RatesItem();
        ratesItem.setComment(getRateRequest().getComment());
        ratesItem.setRate(getRateRequest().getRate());
        ratesItem.setCreatedAt(AppHelper.getCurrentDate());
        getMyOrderDetails().getOrders().setReview(ratesItem);
        setRateRequest(new RateRequest());
        notifyChange(BR.myOrderDetails);
    }

    @Bindable
    public void setMainData(MyOrdersServicesMainData mainData) {
        if (getMyOrdersAdapter().getOrderDetailsList().size() > 0) {
            getMyOrdersAdapter().loadMore(mainData.getOrders().getData());
        } else {
            getMyOrdersAdapter().update(mainData.getOrders().getData());
            notifyChange(BR.myOrdersAdapter);
        }
        if (mainData.getMainServices() != null) {
            getHomeServicesAdapter().update(mainData.getMainServices());
            notifyChange(BR.homeServicesAdapter);
        }
        searchProgressVisible.set(false);
        notifyChange(BR.mainData);
        this.mainData = mainData;
    }

    public void liveDataActions(String action) {
        liveData.setValue(new Mutable(action));
    }

    public void onRateChange(RatingBar ratingBar, float rating, boolean fromUser) {
        getRateRequest().setRate(String.valueOf(rating));
    }

    @Bindable
    public MyServicesOrdersAdapter getMyOrdersAdapter() {
        return this.myOrdersAdapter == null ? this.myOrdersAdapter = new MyServicesOrdersAdapter() : this.myOrdersAdapter;
    }

    @Bindable
    public ServicesRequiredAdapter getServicesRequiredAdapter() {
        return this.servicesRequiredAdapter == null ? this.servicesRequiredAdapter = new ServicesRequiredAdapter() : this.servicesRequiredAdapter;
    }

    @Bindable
    public HomeServicesAdapter getHomeServicesAdapter() {
        return this.homeServicesAdapter == null ? this.homeServicesAdapter = new HomeServicesAdapter() : this.homeServicesAdapter;
    }

    @Bindable
    public MyOrdersServicesMainData getMainData() {
        return this.mainData == null ? this.mainData = new MyOrdersServicesMainData() : this.mainData;
    }

    @Bindable
    public OrderDetailsMain getMyOrderDetails() {
        return this.myOrderDetails == null ? this.myOrderDetails = new OrderDetailsMain() : this.myOrderDetails;
    }

    @Bindable
    public RateRequest getRateRequest() {
        return this.rateRequest == null ? this.rateRequest = new RateRequest() : this.rateRequest;
    }

    @Bindable
    public void setRateRequest(RateRequest rateRequest) {
        this.rateRequest = rateRequest;
    }

    public ServicesRepository getServicesRepository() {
        return servicesRepository;
    }

    protected void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        unSubscribeFromObservable();
    }

}
