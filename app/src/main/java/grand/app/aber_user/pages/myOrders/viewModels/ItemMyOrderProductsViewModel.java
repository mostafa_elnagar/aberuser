package grand.app.aber_user.pages.myOrders.viewModels;

import androidx.databinding.Bindable;

import grand.app.aber_user.base.BaseViewModel;
import grand.app.aber_user.pages.parts.models.ProductsItem;
import grand.app.aber_user.utils.Constants;

public class ItemMyOrderProductsViewModel extends BaseViewModel {
    public ProductsItem productsItem;

    public ItemMyOrderProductsViewModel(ProductsItem productsItem) {
        this.productsItem = productsItem;
    }

    @Bindable
    public ProductsItem getProductsItem() {
        return productsItem;
    }

    public void itemAction() {
        getLiveData().setValue(Constants.DELETE);
    }
}
