package grand.app.aber_user.pages.myOrders.models;

import com.google.gson.annotations.SerializedName;

public class HistoryItem{

	@SerializedName("name")
	private String name;

	@SerializedName("id")
	private int id;

	public String getName(){
		return name;
	}

	public int getId(){
		return id;
	}
}