package grand.app.aber_user.pages.myOrders.models;

import java.util.List;
import com.google.gson.annotations.SerializedName;

import grand.app.aber_user.model.PaginateMain;

public class MyOrdersMainData extends PaginateMain {

	@SerializedName("data")
	private List<MyOrderData> data;

	public List<MyOrderData> getData(){
		return data;
	}

}