package grand.app.aber_user.pages.parts.viewModels;


import androidx.databinding.Bindable;

import grand.app.aber_user.base.BaseViewModel;
import grand.app.aber_user.pages.parts.models.productDetails.SizesItem;

public class ItemProductColorSizeViewModel extends BaseViewModel {
    public SizesItem sizesItem;

    public ItemProductColorSizeViewModel(SizesItem sizesItem) {
        this.sizesItem = sizesItem;
    }

    @Bindable
    public SizesItem getSizesItem() {
        return sizesItem;
    }

    public void itemAction(String action) {
        getLiveData().setValue(action);
    }

}
