package grand.app.aber_user.pages.reviews.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import grand.app.aber_user.model.PaginateMain;

public class ReviewMainData extends PaginateMain {
    @SerializedName("data")
    private List<RatesItem> rates;

    public List<RatesItem> getRates() {
        return rates;
    }
}
