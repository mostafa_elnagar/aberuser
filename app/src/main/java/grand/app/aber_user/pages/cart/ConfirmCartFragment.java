package grand.app.aber_user.pages.cart;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;

import com.google.gson.Gson;

import javax.inject.Inject;

import grand.app.aber_user.BR;
import grand.app.aber_user.PassingObject;
import grand.app.aber_user.R;
import grand.app.aber_user.base.BaseFragment;
import grand.app.aber_user.base.IApplicationComponent;
import grand.app.aber_user.base.MyApplication;
import grand.app.aber_user.databinding.FragmentConfirmCartBinding;
import grand.app.aber_user.model.base.Mutable;
import grand.app.aber_user.pages.cart.models.CheckPromoResponse;
import grand.app.aber_user.pages.cart.models.NewOrderRequest;
import grand.app.aber_user.pages.cart.viewModels.CartViewModel;
import grand.app.aber_user.pages.myLocations.AddPlaceFragment;
import grand.app.aber_user.pages.myLocations.models.LocationsData;
import grand.app.aber_user.pages.myLocations.models.LocationsResponse;
import grand.app.aber_user.pages.parts.PartsFragment;
import grand.app.aber_user.utils.Constants;
import grand.app.aber_user.utils.helper.MovementHelper;

public class ConfirmCartFragment extends BaseFragment {
    FragmentConfirmCartBinding binding;
    @Inject
    CartViewModel viewModel;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_confirm_cart, container, false);
        IApplicationComponent component = ((MyApplication) requireActivity().getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setViewmodel(viewModel);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            String passingObject = bundle.getString(Constants.BUNDLE);
            viewModel.setPassingObject(new Gson().fromJson(passingObject, PassingObject.class));
            viewModel.setNewOrderRequest(new Gson().fromJson(String.valueOf(viewModel.getPassingObject().getObjectClass()), NewOrderRequest.class));
        }
        viewModel.getLocations();
        setEvent();
        return binding.getRoot();
    }

    private void setEvent() {
        viewModel.liveData.observe(requireActivity(), (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            if (Constants.SERVICES.equals(((Mutable) o).message)) {
                MovementHelper.startActivityWithBundle(MovementHelper.unwrap(requireActivity()), new PassingObject(5), getString(R.string.continue_shopping), PartsFragment.class.getName(), null);
            } else if (Constants.MY_LOCATIONS.equals(((Mutable) o).message)) {
                viewModel.getLocationsAdapters().updateData(((LocationsResponse) mutable.object).getData(), false);
                viewModel.notifyChange(BR.locationsAdapters);
                viewModel.setDeliveryCost();
            } else if (Constants.ADD_PLACE.equals(((Mutable) o).message)) {
                MovementHelper.startActivityForResultWithBundle(requireActivity(), new PassingObject(), getString(R.string.add_place), AddPlaceFragment.class.getName(), Constants.LOCATION_REQUEST);
            } else if (Constants.PAYMENT.equals(((Mutable) o).message)) {
                MovementHelper.startActivityForResultWithBundle(requireActivity(), new PassingObject(viewModel.getNewOrderRequest()), getString(R.string.choose_payment), PaymentFragment.class.getName(), Constants.ORDER_REQUEST);
            } else if (Constants.CHECK_PROMO.equals(((Mutable) o).message)) {
                toastMessage(((CheckPromoResponse) mutable.object).mMessage);
                viewModel.setPromoDiscount(((CheckPromoResponse) mutable.object).getData());
            }
        });
        viewModel.getCartLiveData().observe(requireActivity(), productDetails -> {
            viewModel.getCartAdapter().update(productDetails, true);
            viewModel.notifyChange(BR.cartAdapter);
        });
        viewModel.getLocationsAdapters().locationsDataMutableLiveData.observe(requireActivity(), locationsData -> viewModel.setDeliveryCost());
    }

    @Override
    public void onResume() {
        viewModel.getServicesRepository().setLiveData(viewModel.liveData);
        super.onResume();
    }

    @Override
    public void launchActivityResult(int request, int resultCode, Intent result) {
        super.launchActivityResult(request, resultCode, result);
        if (result != null) {
            if (request == Constants.LOCATION_REQUEST) {
                Bundle bundle = result.getBundleExtra(Constants.BUNDLE);
                if (bundle != null && bundle.containsKey(Constants.BUNDLE)) {
                    PassingObject passingObject = (PassingObject) bundle.getSerializable(Constants.BUNDLE);
                    viewModel.getLocationsAdapters().getLocationsDataList().add(new Gson().fromJson(String.valueOf(passingObject.getObjectClass()), LocationsData.class));
                    viewModel.getLocationsAdapters().notifyItemInserted(viewModel.getLocationsAdapters().getLocationsDataList().size() - 1);
                    binding.rcLocation.scrollToPosition(viewModel.getLocationsAdapters().getLocationsDataList().size() - 1);
                    if (viewModel.getLocationsAdapters().getLocationsDataList().size() == 1)
                        viewModel.setDeliveryCost();
                }
            } else if (request == Constants.ORDER_REQUEST) {
                MovementHelper.finishWithResult(new PassingObject(), requireActivity(), Constants.ORDER_REQUEST);
            }
        }
    }

}
