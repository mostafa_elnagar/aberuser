package grand.app.aber_user.pages.chat.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.aber_user.model.base.StatusMessage;
import grand.app.aber_user.pages.conversations.models.ConversationsData;

public class ChatSendResponse extends StatusMessage {

    @SerializedName("data")
    @Expose
    private ConversationsData data;

    public ConversationsData getData() {
        return data;
    }
}
