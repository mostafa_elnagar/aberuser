package grand.app.aber_user.pages.parts.viewModels;

import android.text.TextUtils;
import android.widget.RadioGroup;

import androidx.databinding.Bindable;
import androidx.databinding.ObservableBoolean;
import androidx.lifecycle.MutableLiveData;

import javax.inject.Inject;

import grand.app.aber_user.BR;
import grand.app.aber_user.R;
import grand.app.aber_user.base.BaseViewModel;
import grand.app.aber_user.base.MyApplication;
import grand.app.aber_user.model.base.Mutable;
import grand.app.aber_user.pages.parts.adapters.PartProductsAdapter;
import grand.app.aber_user.pages.parts.models.ProductMain;
import grand.app.aber_user.pages.parts.models.filter.FilterRequest;
import grand.app.aber_user.repository.ServicesRepository;
import grand.app.aber_user.utils.Constants;
import grand.app.aber_user.utils.session.UserHelper;
import io.reactivex.disposables.CompositeDisposable;

public class PartServicesViewModels extends BaseViewModel {

    public MutableLiveData<Mutable> liveData;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    private PartProductsAdapter productsAdapter;
    @Inject
    ServicesRepository postRepository;
    public ObservableBoolean searchProgressVisible = new ObservableBoolean();
    ProductMain mainData;
    FilterRequest filterRequest;

    @Inject
    public PartServicesViewModels(ServicesRepository postRepository) {
        mainData = new ProductMain();
        this.postRepository = postRepository;
        this.liveData = new MutableLiveData<>();
        postRepository.setLiveData(liveData);
    }

    public void partServices(int page, boolean showProgress, int isNew) {
        if (getPassingObject().getObject().equals(Constants.COMPANY))
            compositeDisposable.add(postRepository.getCompanyPartServices(getPassingObject().getId(), UserHelper.getInstance(MyApplication.getInstance()).getCountriesData().getId(), isNew, page, showProgress));
        else
            compositeDisposable.add(postRepository.getPartServices(getPassingObject().getId(), UserHelper.getInstance(MyApplication.getInstance()).getCountriesData().getId(), isNew, page, showProgress));
    }

    public void sortBy() {
        getFilterRequest().setSub_service_id(String.valueOf(getPassingObject().getId()));
        getFilterRequest().setCountry_id(String.valueOf(UserHelper.getInstance(MyApplication.getInstance()).getCountriesData().getId()));
        getFilterRequest().setIsNew(!TextUtils.isEmpty(getFilterRequest().getIsNew()) ? getFilterRequest().getIsNew() : "1");
        compositeDisposable.add(postRepository.sortBy(getFilterRequest()));
    }

    public void changeLike(int productId) {
        compositeDisposable.add(postRepository.changeLike(productId));
    }

    public void liveDataActions(String action) {
        liveData.setValue(new Mutable(action));
    }

    @Bindable
    public PartProductsAdapter getProductsAdapter() {
        return this.productsAdapter == null ? this.productsAdapter = new PartProductsAdapter() : this.productsAdapter;
    }

    @Bindable
    public ProductMain getMainData() {
        return mainData;
    }

    @Bindable
    public void setMainData(ProductMain mainData) {
        if (getProductsAdapter().getProductsItemList().size() > 0) {
            getProductsAdapter().loadMore(mainData.getProducts().getData());
        } else {
            getProductsAdapter().update(mainData.getProducts().getData());
            notifyChange(BR.productsAdapter);
        }
        searchProgressVisible.set(false);
        notifyChange(BR.mainData);
        this.mainData = mainData;
    }

    public void sortType(RadioGroup radioGroup, int id) {
        liveData.setValue(new Mutable(Constants.DIALOG_CLOSE));
        if (id == R.id.radioTopRated) {
            getFilterRequest().setOrder_by("top_rated");
        } else if (id == R.id.radioPriceHighLow) {
            getFilterRequest().setOrder_by("highest");
        } else if (id == R.id.radioPriceLowHigh) {
            getFilterRequest().setOrder_by("lowest");
        } else if (id == R.id.radioLatest) {
            getFilterRequest().setOrder_by("newest");
        }
        sortBy();
    }

    public ServicesRepository getPostRepository() {
        return postRepository;
    }

    @Bindable
    public FilterRequest getFilterRequest() {
        return this.filterRequest == null ? this.filterRequest = new FilterRequest() : this.filterRequest;
    }

    protected void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        unSubscribeFromObservable();
    }

}
