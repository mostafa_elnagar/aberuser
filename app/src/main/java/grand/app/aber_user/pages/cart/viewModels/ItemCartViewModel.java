package grand.app.aber_user.pages.cart.viewModels;

import androidx.databinding.Bindable;

import grand.app.aber_user.base.BaseViewModel;
import grand.app.aber_user.base.MyApplication;
import grand.app.aber_user.pages.parts.models.ProductsItem;
import grand.app.aber_user.repository.CartRepository;
import grand.app.aber_user.utils.Constants;

public class ItemCartViewModel extends BaseViewModel {
    public ProductsItem productsItem;

    public ItemCartViewModel(ProductsItem productsItem) {
        this.productsItem = productsItem;
    }

    @Bindable
    public ProductsItem getProductsItem() {
        return productsItem;
    }

    public void minusItem() {
        if (productsItem.getQuantity() > 1) {
            productsItem.setQuantity(productsItem.getQuantity() - 1);
            productsItem.setPrice(String.valueOf(Double.parseDouble(productsItem.getPrice()) - productsItem.getPriceItem()));
            new CartRepository(MyApplication.getInstance()).update(productsItem);
        }
    }

    public void plusItem() {
        productsItem.setQuantity(productsItem.getQuantity() + 1);
        productsItem.setPrice(String.valueOf(Double.parseDouble(productsItem.getPrice()) + productsItem.getPriceItem()));
        new CartRepository(MyApplication.getInstance()).update(productsItem);
    }

    public void itemAction() {
        getLiveData().setValue(Constants.DELETE);
    }
}
