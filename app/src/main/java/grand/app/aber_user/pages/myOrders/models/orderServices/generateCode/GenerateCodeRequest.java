package grand.app.aber_user.pages.myOrders.models.orderServices.generateCode;

import com.google.gson.annotations.SerializedName;

public class GenerateCodeRequest {
    @SerializedName("provider_id")
    private int providerId;
    @SerializedName("order_service_id")
    private int orderServiceId;

    public GenerateCodeRequest(int providerId, int orderServiceId) {
        this.providerId = providerId;
        this.orderServiceId = orderServiceId;
    }

    public int getProviderId() {
        return providerId;
    }

    public void setProviderId(int providerId) {
        this.providerId = providerId;
    }

    public int getOrderServiceId() {
        return orderServiceId;
    }

    public void setOrderServiceId(int orderServiceId) {
        this.orderServiceId = orderServiceId;
    }
}
