package grand.app.aber_user.pages.services.viewModels;


import androidx.databinding.Bindable;

import grand.app.aber_user.base.BaseViewModel;
import grand.app.aber_user.pages.services.models.Extra;
import grand.app.aber_user.pages.services.models.ServicesItem;
import grand.app.aber_user.utils.Constants;

public class ItemExtraServicesViewModel extends BaseViewModel {
    public Extra extra;

    public ItemExtraServicesViewModel(Extra extra) {
        this.extra = extra;
    }

    @Bindable
    public Extra getExtra() {
        return extra;
    }

    public void itemAction() {
        getExtra().setChecked(!getExtra().isChecked());
    }

}
