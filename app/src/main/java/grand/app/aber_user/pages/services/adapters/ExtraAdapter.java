package grand.app.aber_user.pages.services.adapters;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import grand.app.aber_user.R;
import grand.app.aber_user.databinding.ItemExtraServicesBinding;
import grand.app.aber_user.pages.services.models.Extra;
import grand.app.aber_user.pages.services.viewModels.ItemExtraServicesViewModel;

public class ExtraAdapter extends RecyclerView.Adapter<ExtraAdapter.MenuView> {
    List<Extra> servicesItemList;
    public MutableLiveData<Object> liveData = new MutableLiveData<>();

    public ExtraAdapter() {
        this.servicesItemList = new ArrayList<>();
    }


    public List<Extra> getServicesItemList() {
        return servicesItemList;
    }

    @NonNull
    @Override
    public MenuView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_extra_services,
                parent, false);
        return new MenuView(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull final MenuView holder, @SuppressLint("RecyclerView") final int position) {
        Extra menuModel = servicesItemList.get(position);
        ItemExtraServicesViewModel itemMenuViewModel = new ItemExtraServicesViewModel(menuModel);
        holder.setViewModel(itemMenuViewModel);
    }

    public void update(@NotNull List<Extra> dataList) {
        this.servicesItemList.clear();
        servicesItemList.addAll(dataList);
        notifyDataSetChanged();
    }

    @Override
    public void onViewAttachedToWindow(@NotNull MenuView holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NotNull MenuView holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    @Override
    public int getItemCount() {
        return servicesItemList.size();
    }

    public class MenuView extends RecyclerView.ViewHolder {
        public ItemExtraServicesBinding itemMenuBinding;

        MenuView(View itemView) {
            super(itemView);
            bind();
        }

        void bind() {
            if (itemMenuBinding == null) {
                itemMenuBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemMenuBinding != null) {
                itemMenuBinding.unbind();
            }
        }

        void setViewModel(ItemExtraServicesViewModel itemViewModels) {
            if (itemMenuBinding != null) {
                itemMenuBinding.setItemViewModel(itemViewModels);
            }
        }
    }
}
