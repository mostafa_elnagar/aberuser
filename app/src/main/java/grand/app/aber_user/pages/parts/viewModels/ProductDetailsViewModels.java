package grand.app.aber_user.pages.parts.viewModels;

import androidx.databinding.Bindable;
import androidx.lifecycle.MutableLiveData;

import com.smarteist.autoimageslider.SliderView;

import javax.inject.Inject;

import grand.app.aber_user.BR;
import grand.app.aber_user.base.BaseViewModel;
import grand.app.aber_user.base.MyApplication;
import grand.app.aber_user.model.base.Mutable;
import grand.app.aber_user.pages.parts.adapters.ProductColorSizesAdapter;
import grand.app.aber_user.pages.parts.adapters.ProductColorsAdapter;
import grand.app.aber_user.pages.parts.adapters.ProductImagesSliderAdapter;
import grand.app.aber_user.pages.parts.models.ProductsItem;
import grand.app.aber_user.pages.parts.models.productDetails.ProductDetails;
import grand.app.aber_user.repository.CartRepository;
import grand.app.aber_user.repository.ServicesRepository;
import grand.app.aber_user.utils.Constants;
import grand.app.aber_user.utils.session.UserHelper;
import io.reactivex.disposables.CompositeDisposable;

public class ProductDetailsViewModels extends BaseViewModel {

    public MutableLiveData<Mutable> liveData;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    ProductColorsAdapter productColorsAdapter;
    ProductColorSizesAdapter colorSizesAdapter;
    ProductImagesSliderAdapter sliderAdapter;
    @Inject
    ServicesRepository servicesRepository;
    ProductDetails productDetails;

    @Inject
    public ProductDetailsViewModels(ServicesRepository servicesRepository) {
        this.servicesRepository = servicesRepository;
        this.liveData = new MutableLiveData<>();
        servicesRepository.setLiveData(liveData);
    }

    public void productDetails() {
        compositeDisposable.add(servicesRepository.getProductDetails(getPassingObject().getId()));
    }

    public void changeLike() {
        getProductDetails().setFavorite(!getProductDetails().isFavorite());
        notifyChange(BR.productDetails);
        compositeDisposable.add(servicesRepository.changeLike(getPassingObject().getId()));
    }

    public void liveDataActions(String action) {
        liveData.setValue(new Mutable(action));
    }

    public void addToCart() {
        if (userData != null) {
            if (getProductDetails() != null) {
                ProductsItem productsItem = new ProductsItem();
                productsItem.setId(productDetails.getId());
                productsItem.setName(productDetails.getName());
                productsItem.setPriceItem(Double.parseDouble(productDetails.getPrice())); // for calculate total form one item
                productsItem.setPrice(productDetails.getPrice());
                productsItem.setCurrency(productDetails.getCurrency());
                productsItem.setQuantity(1);
                productsItem.setAttributeParentId(getProductColorsAdapter().getColorsItemList().size() > 0 ? getProductColorsAdapter().lastSelected : 0);
                productsItem.setAttributeChildId(getProductColorsAdapter().getColorsItemList().size() > 0 ? getColorSizesAdapter().lastSelected : 0);
                productsItem.setImage(productDetails.getImages() != null && productDetails.getImages().size() > 0 ? productDetails.getImages().get(0).getImage() : "");
                new CartRepository(MyApplication.getInstance()).insert(productsItem);
                liveDataActions(Constants.CART);
            }
        } else
            liveDataActions((Constants.LOGOUT));
    }

    @Bindable
    public ProductColorsAdapter getProductColorsAdapter() {
        return this.productColorsAdapter == null ? this.productColorsAdapter = new ProductColorsAdapter() : this.productColorsAdapter;
    }

    @Bindable
    public ProductColorSizesAdapter getColorSizesAdapter() {
        return this.colorSizesAdapter == null ? this.colorSizesAdapter = new ProductColorSizesAdapter() : this.colorSizesAdapter;
    }

    @Bindable
    public ProductImagesSliderAdapter getSliderAdapter() {
        return this.sliderAdapter == null ? this.sliderAdapter = new ProductImagesSliderAdapter() : this.sliderAdapter;
    }

    @Bindable
    public ProductDetails getProductDetails() {
        return this.productDetails == null ? this.productDetails = new ProductDetails() : this.productDetails;
    }

    public void setProductDetails(ProductDetails productDetails) {
        if (productDetails.getAttributes() != null && productDetails.getAttributes().size() > 0) {
            getProductColorsAdapter().update(productDetails.getAttributes());
            if (productDetails.getAttributes() != null && productDetails.getAttributes().size() > 0)
                getColorSizesAdapter().update(productDetails.getAttributes().get(0).getSizes());
            notifyChange(BR.productColorsAdapter);
            notifyChange(BR.colorSizesAdapter);
        }
        if (productDetails.getImages() != null) {
            getSliderAdapter().updateData(productDetails.getImages());
            notifyChange(BR.sliderAdapter);
        }
        notifyChange(BR.productDetails);
        this.productDetails = productDetails;
    }

    public void updateSizes() {
        getColorSizesAdapter().update(getProductColorsAdapter().getColorsItemList().get(getProductColorsAdapter().lastPosition).getSizes());
    }

    public void setupSlider(SliderView sliderView) {
        sliderView.setSliderAdapter(getSliderAdapter());
    }

    public ServicesRepository getServicesRepository() {
        return servicesRepository;
    }

    protected void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        unSubscribeFromObservable();
    }

}
