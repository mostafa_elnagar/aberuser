package grand.app.aber_user.pages.myOrders;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;

import com.google.gson.Gson;

import javax.inject.Inject;

import grand.app.aber_user.PassingObject;
import grand.app.aber_user.R;
import grand.app.aber_user.base.BaseFragment;
import grand.app.aber_user.base.IApplicationComponent;
import grand.app.aber_user.base.MyApplication;
import grand.app.aber_user.databinding.FragmentMyOrderDetailsBinding;
import grand.app.aber_user.model.base.Mutable;
import grand.app.aber_user.pages.myOrders.models.MyOrderDetailsResponse;
import grand.app.aber_user.pages.myOrders.viewModels.MyOrdersViewModels;
import grand.app.aber_user.utils.Constants;

public class MyOrderDetailsFragment extends BaseFragment {
    @Inject
    MyOrdersViewModels viewModel;
    FragmentMyOrderDetailsBinding binding;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_my_order_details, container, false);
        IApplicationComponent component = ((MyApplication) requireActivity().getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setViewmodel(viewModel);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            String passingObject = bundle.getString(Constants.BUNDLE);
            viewModel.setPassingObject(new Gson().fromJson(passingObject, PassingObject.class));
            viewModel.orderDetails();
        }
        setEvent();
        return binding.getRoot();
    }

    private void setEvent() {
        viewModel.liveData.observe(requireActivity(), (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            if (Constants.My_ORDER_DETAILS.equals(((Mutable) o).message)) {
                viewModel.setMyOrderDetails(((MyOrderDetailsResponse) mutable.object).getData());
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        baseActivity().enableRefresh(false);
        viewModel.getServicesRepository().setLiveData(viewModel.liveData);
    }

}
