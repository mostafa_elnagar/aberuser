package grand.app.aber_user.pages.parts.viewModels;


import androidx.databinding.Bindable;

import grand.app.aber_user.base.BaseViewModel;
import grand.app.aber_user.model.DropDownsObject;

public class ItemCarViewModel extends BaseViewModel {
    public DropDownsObject downsObject;

    public ItemCarViewModel(DropDownsObject downsObject) {
        this.downsObject = downsObject;
    }

    @Bindable
    public DropDownsObject getDownsObject() {
        return downsObject;
    }

    public void itemAction(String action) {
        getLiveData().setValue(action);
    }

}
