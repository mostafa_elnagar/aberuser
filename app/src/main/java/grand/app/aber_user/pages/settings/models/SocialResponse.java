package grand.app.aber_user.pages.settings.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import grand.app.aber_user.model.base.StatusMessage;

public class SocialResponse extends StatusMessage {
    @SerializedName("data")
    private List<SocialMediaData> mediaDataList;

    public List<SocialMediaData> getMediaDataList() {
        return mediaDataList;
    }
}
