package grand.app.aber_user.pages.services.models;

import com.google.gson.annotations.SerializedName;

import grand.app.aber_user.model.base.StatusMessage;

public class ServiceDetailsResponse extends StatusMessage {

    @SerializedName("data")
    private ServiceDetails data;

    public ServiceDetails getData() {
        return data;
    }

}