package grand.app.aber_user.pages.notifications.itemViewModels;

import androidx.databinding.Bindable;

import grand.app.aber_user.base.BaseViewModel;
import grand.app.aber_user.pages.notifications.models.NotificationsData;
import grand.app.aber_user.utils.Constants;


public class NotificationsItemViewModels extends BaseViewModel {
    NotificationsData notificationsData;

    public NotificationsItemViewModels(NotificationsData notificationsData) {
        this.notificationsData = notificationsData;
    }

    @Bindable
    public NotificationsData getNotificationsData() {
        return notificationsData;
    }


    public void itemAction() {
        getLiveData().setValue(Constants.MENu);
    }

}
