package grand.app.aber_user.pages.myOrders;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import javax.inject.Inject;

import grand.app.aber_user.R;
import grand.app.aber_user.base.BaseFragment;
import grand.app.aber_user.base.IApplicationComponent;
import grand.app.aber_user.base.MyApplication;
import grand.app.aber_user.databinding.FragmentServicesOrdersBinding;
import grand.app.aber_user.model.base.Mutable;
import grand.app.aber_user.pages.myOrders.models.orderServices.MyOrderServicesResponse;
import grand.app.aber_user.pages.myOrders.viewModels.MyOrdersServicesViewModels;
import grand.app.aber_user.utils.Constants;

public class MyServicesOrdersFragment extends BaseFragment {
    FragmentServicesOrdersBinding binding;
    @Inject
    MyOrdersServicesViewModels viewModel;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_services_orders, container, false);
        IApplicationComponent component = ((MyApplication) requireActivity().getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setViewModel(viewModel);
        viewModel.myOrders(0, 1, true);
        setEvent();
        return binding.getRoot();
    }

    private void setEvent() {
        viewModel.liveData.observe(requireActivity(), (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            if (Constants.SERVICES_ORDERS.equals(((Mutable) o).message)) {
                viewModel.setMainData(((MyOrderServicesResponse) (mutable).object).getData());
            }
        });
        viewModel.getHomeServicesAdapter().liveData.observe(requireActivity(), o -> viewModel.myOrdersByCat(1, o, true));
        binding.rcServices.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                if (!viewModel.searchProgressVisible.get() && !TextUtils.isEmpty(viewModel.getMainData().getOrders().getLinks().getNext())) {
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == viewModel.getMyOrdersAdapter().getOrderDetailsList().size() - 1) {
                        viewModel.searchProgressVisible.set(true);
                        viewModel.myOrdersByCat((viewModel.getMainData().getOrders().getMeta().getCurrentPage() + 1), viewModel.getHomeServicesAdapter().lastSelected, false);
                    }
                }
            }
        });
    }

    @Override
    public void onResume() {
        viewModel.getServicesRepository().setLiveData(viewModel.liveData);
        super.onResume();
        if (Constants.DATA_CHANGED) {
            Constants.DATA_CHANGED = false;
            viewModel.myOrders(0, 1, true);
        }
    }
}
