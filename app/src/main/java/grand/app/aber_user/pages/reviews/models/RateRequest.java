package grand.app.aber_user.pages.reviews.models;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

public class RateRequest {

    @SerializedName("rate")
    private String rate;
    @SerializedName("review")
    private String comment;
    @SerializedName("order_id")
    private String orderId;
    @SerializedName("product_id")
    private String productId;
    @SerializedName("order_service_id")
    private String orderServiceId;

    public RateRequest() {
    }

    public RateRequest(String productId) {
        this.productId = productId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean isValid() {
        return (!TextUtils.isEmpty(comment) && !TextUtils.isEmpty(rate));
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderServiceId() {
        return orderServiceId;
    }

    public void setOrderServiceId(String orderServiceId) {
        this.orderServiceId = orderServiceId;
    }
}
