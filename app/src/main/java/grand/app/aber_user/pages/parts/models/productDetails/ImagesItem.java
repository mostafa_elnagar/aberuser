package grand.app.aber_user.pages.parts.models.productDetails;

import com.google.gson.annotations.SerializedName;

public class ImagesItem{

	@SerializedName("image")
	private String image;

	@SerializedName("id")
	private int id;

	public String getImage(){
		return image;
	}

	public int getId(){
		return id;
	}
}