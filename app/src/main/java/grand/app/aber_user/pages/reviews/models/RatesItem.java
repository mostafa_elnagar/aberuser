package grand.app.aber_user.pages.reviews.models;

import com.google.gson.annotations.SerializedName;


public class RatesItem {

    @SerializedName("rate")
    private String rate;

    @SerializedName("date")
    private String createdAt;

    @SerializedName("review")
    private String comment;
    @SerializedName("name")
    private String name;

    @SerializedName("id")
    private int id;


    public String getRate() {
        return rate;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getComment() {
        return comment;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}