package grand.app.aber_user.pages.auth.models;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import grand.app.aber_user.R;
import grand.app.aber_user.utils.resources.ResourceManager;

public class UserData {

    @SerializedName("image")
    private String image;

    @SerializedName("account_type")
    private String accountType;

    @SerializedName("active")
    private int active;

    @SerializedName("token")
    private String token;

    @SerializedName("phone")
    private String phone;

    @SerializedName("device_token")
    private String deviceToken;

    @SerializedName("name")
    private String name;
    @SerializedName("company")
    private String company;

    @SerializedName("provider_id")
    private String providerId;

    @SerializedName("id")
    private int id;

    @SerializedName("is_promoted")
    private int isPromoted;

    @SerializedName("job")
    private String job;

    @SerializedName("email")
    private String email;
    @SerializedName("longitude")
    private String longitude;

    @SerializedName("latitude")
    private String latitude;
    @SerializedName("avg_rate")
    private String rate;

    public String getImage() {
        return image;
    }

    public String getAccountType() {
        return accountType;
    }

    public int getActive() {
        return active;
    }


    public String getToken() {
        return token;
    }


    public String getPhone() {
        return phone;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public String getName() {
        return name;
    }

    public String getProviderId() {
        return providerId;
    }

    public int getId() {
        return id;
    }

    public String getJob() {
        return job;
    }

    public String getEmail() {
        return email;
    }

    public int getIsPromoted() {
        return isPromoted;
    }

    public String getCompany() {
        return company;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getRate() {
        return rate;
    }
}