package grand.app.aber_user.pages.myOrders.models.orderServices.generateCode;

import com.google.gson.annotations.SerializedName;

public class GenerateCodeData {
    @SerializedName("code")
    private String code;

    public String getCode() {
        return code;
    }
}
