package grand.app.aber_user.pages.cart.viewModels;

import android.text.TextUtils;
import android.widget.RadioGroup;

import androidx.databinding.Bindable;
import androidx.databinding.ObservableField;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import javax.inject.Inject;

import grand.app.aber_user.BR;
import grand.app.aber_user.R;
import grand.app.aber_user.base.BaseViewModel;
import grand.app.aber_user.base.MyApplication;
import grand.app.aber_user.model.base.Mutable;
import grand.app.aber_user.pages.cart.adapters.CartAdapter;
import grand.app.aber_user.pages.cart.models.CheckPromoRequest;
import grand.app.aber_user.pages.cart.models.NewOrderRequest;
import grand.app.aber_user.pages.cart.models.PromoData;
import grand.app.aber_user.pages.myLocations.adapters.LocationsAdapters;
import grand.app.aber_user.pages.parts.models.ProductsItem;
import grand.app.aber_user.repository.CartRepository;
import grand.app.aber_user.repository.ServicesRepository;
import grand.app.aber_user.utils.Constants;
import grand.app.aber_user.utils.resources.ResourceManager;
import grand.app.aber_user.utils.session.UserHelper;
import io.reactivex.disposables.CompositeDisposable;

public class CartViewModel extends BaseViewModel {
    public MutableLiveData<Mutable> liveData;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    CartRepository cartRepository;
    @Inject
    ServicesRepository servicesRepository;
    LiveData<List<ProductsItem>> cartLiveData;
    LiveData<String> cartTotal;
    CartAdapter cartAdapter;
    LocationsAdapters locationsAdapters;
    CheckPromoRequest checkPromoRequest;
    NewOrderRequest newOrderRequest;
    public ObservableField<String> cartTotalCurrency = new ObservableField<>();
    public ObservableField<String> discount = new ObservableField<>();

    @Inject
    public CartViewModel(ServicesRepository servicesRepository) {
        this.servicesRepository = servicesRepository;
        this.liveData = new MutableLiveData<>();
        servicesRepository.setLiveData(liveData);
        cartRepository = new CartRepository(MyApplication.getInstance());
        cartLiveData = cartRepository.getAllProducts();
        cartTotal = cartRepository.getCartTotal();
    }

    public void getLocations() {
        compositeDisposable.add(servicesRepository.myLocations());
    }

    public void checkPromo() {
        if (!TextUtils.isEmpty(getCheckPromoRequest().getCode()))
            compositeDisposable.add(servicesRepository.checkPromo(getCheckPromoRequest()));
    }

    public void finishOrder() {
        if (!TextUtils.isEmpty(getNewOrderRequest().getPaymentMethod())) {
            setMessage(Constants.SHOW_PROGRESS);
            compositeDisposable.add(servicesRepository.orderProduct(getNewOrderRequest()));
        }
    }

    public void toConfirmOrder() {
        if (getCartAdapter().getMenuModels().size() > 0) {
            liveData.setValue(new Mutable(Constants.CART));
        } else
            liveData.setValue(new Mutable(Constants.SERVICES));
    }

    public void toPayment() {
        if (UserHelper.getInstance(MyApplication.getInstance()).getSaveLastKnownLocation() != null) {
            getNewOrderRequest().setLocationId(String.valueOf(UserHelper.getInstance(MyApplication.getInstance()).getSaveLastKnownLocation().getId()));
            getNewOrderRequest().setAddress(UserHelper.getInstance(MyApplication.getInstance()).getSaveLastKnownLocation().getStreet());
            getNewOrderRequest().setProductsItems(getCartAdapter().getMenuModels());
            liveData.setValue(new Mutable(Constants.PAYMENT));
        } else {
            liveData.setValue(new Mutable(Constants.ERROR_TOAST, ResourceManager.getString(R.string.location_warning)));
            liveData.setValue(new Mutable(Constants.ADD_PLACE));
        }
    }

    public void addLocation() {
        liveData.setValue(new Mutable(Constants.ADD_PLACE));
    }

    public void setDeliveryCost() {
        if (getLocationsAdapters().getLocationsDataList().size() > 0) {
            getNewOrderRequest().setDeliveryFee(String.valueOf(getLocationsAdapters().getLocationsDataList().get(getLocationsAdapters().position).getDeliveryCost()));
            getNewOrderRequest().setTotal(String.valueOf(Double.parseDouble(getNewOrderRequest().getTotalCart()) + Double.parseDouble(getNewOrderRequest().getDeliveryFee())));
        } else
            getNewOrderRequest().setTotal(String.valueOf(Double.parseDouble(getNewOrderRequest().getTotalCart())));
        notifyChange(BR.newOrderRequest);
    }

    public void setPromoDiscount(PromoData promoData) {
        getNewOrderRequest().setDiscount(promoData.getDiscount());
        getNewOrderRequest().setPromoId(String.valueOf(promoData.getId()));
        if (promoData.getPromo_type().equals("fixed")) {
            getNewOrderRequest().setTotal(String.valueOf(Double.parseDouble(getNewOrderRequest().getTotal()) - Double.parseDouble(promoData.getDiscount())));
            discount.set(currency);
        } else {
            double totalAfterDiscount = Double.parseDouble(getNewOrderRequest().getTotal()) * (Double.parseDouble(promoData.getDiscount()) / 100);
            getNewOrderRequest().setTotal(String.valueOf(totalAfterDiscount));
            discount.set(" % ");
        }
        notifyChange(BR.newOrderRequest);
    }

    public void onPaymentChange(RadioGroup radioGroup, int id) {
        getNewOrderRequest().setPaymentMethod(id == R.id.radioOnHand ? "cash" : "online");
    }

    @Bindable
    public NewOrderRequest getNewOrderRequest() {
        return this.newOrderRequest == null ? this.newOrderRequest = new NewOrderRequest() : this.newOrderRequest;
    }

    public void setNewOrderRequest(NewOrderRequest newOrderRequest) {
        notifyChange(BR.newOrderRequest);
        this.newOrderRequest = newOrderRequest;
    }

    @Bindable
    public CheckPromoRequest getCheckPromoRequest() {
        return this.checkPromoRequest == null ? this.checkPromoRequest = new CheckPromoRequest() : this.checkPromoRequest;
    }

    public CartRepository getCartRepository() {
        return cartRepository;
    }

    public LiveData<List<ProductsItem>> getCartLiveData() {
        return cartLiveData;
    }

    public LiveData<String> getCartTotal() {
        return cartTotal;
    }

    public ServicesRepository getServicesRepository() {
        return servicesRepository;
    }


    @Bindable
    public CartAdapter getCartAdapter() {
        return this.cartAdapter == null ? this.cartAdapter = new CartAdapter() : this.cartAdapter;
    }

    @Bindable
    public LocationsAdapters getLocationsAdapters() {
        return this.locationsAdapters == null ? this.locationsAdapters = new LocationsAdapters() : this.locationsAdapters;
    }

    protected void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        unSubscribeFromObservable();
    }
}
