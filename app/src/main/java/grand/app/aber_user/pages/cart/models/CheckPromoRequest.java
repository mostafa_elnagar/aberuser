package grand.app.aber_user.pages.cart.models;

import com.google.gson.annotations.SerializedName;

public class CheckPromoRequest {
    @SerializedName("code")
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
