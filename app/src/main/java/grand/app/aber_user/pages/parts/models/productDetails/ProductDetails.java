package grand.app.aber_user.pages.parts.models.productDetails;

import java.util.List;

import com.google.gson.annotations.SerializedName;

import grand.app.aber_user.pages.auth.models.UserData;

public class ProductDetails {

    @SerializedName("images")
    private List<ImagesItem> images;

    @SerializedName("is_favorite")
    private boolean isFavorite;

    @SerializedName("validity_period")
    private String validityPeriod;

    @SerializedName("description")
    private String description;

    @SerializedName("weight")
    private String weight;

    @SerializedName("type")
    private String type;

    @SerializedName("rate_count")
    private int rateCount;
    @SerializedName("stock_quantity")
    private int stockQuantity;

    @SerializedName("rate")
    private String rate;

    @SerializedName("price")
    private String price;

    @SerializedName("name")
    private String name;

    @SerializedName("attributes")
    private List<AttributesItem> attributes;

    @SerializedName("id")
    private int id;

    @SerializedName("brand")
    private String brand;
    @SerializedName("serial")
    private String serial;
    @SerializedName("currency")
    private String currency;
    @SerializedName("company")
    private UserData company;

    public List<ImagesItem> getImages() {
        return images;
    }

    public boolean isIsFavorite() {
        return isFavorite;
    }

    public String getValidityPeriod() {
        return validityPeriod;
    }

    public String getDescription() {
        return description;
    }

    public String getWeight() {
        return weight;
    }

    public String getType() {
        return type;
    }

    public int getRateCount() {
        return rateCount;
    }

    public String getRate() {
        return rate;
    }

    public String getPrice() {
        return price;
    }

    public String getName() {
        return name;
    }

    public List<AttributesItem> getAttributes() {
        return attributes;
    }

    public int getId() {
        return id;
    }

    public String getBrand() {
        return brand;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

    public String getSerial() {
        return serial;
    }

    public String getCurrency() {
        return currency;
    }

    public UserData getCompany() {
        return company;
    }

    public int getStockQuantity() {
        return stockQuantity;
    }
}