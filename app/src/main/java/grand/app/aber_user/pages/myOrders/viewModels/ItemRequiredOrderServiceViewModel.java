package grand.app.aber_user.pages.myOrders.viewModels;

import androidx.databinding.Bindable;

import grand.app.aber_user.base.BaseViewModel;

public class ItemRequiredOrderServiceViewModel extends BaseViewModel {
    public String service;

    public ItemRequiredOrderServiceViewModel(String service) {
        this.service = service;
    }

    @Bindable
    public String getService() {
        return service;
    }

}
