package grand.app.aber_user.pages.myOrders;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;

import javax.inject.Inject;

import grand.app.aber_user.R;
import grand.app.aber_user.base.BaseFragment;
import grand.app.aber_user.base.IApplicationComponent;
import grand.app.aber_user.base.MyApplication;
import grand.app.aber_user.databinding.FragmentMyOrdersBinding;
import grand.app.aber_user.model.base.Mutable;
import grand.app.aber_user.pages.myOrders.models.MyOrdersResponse;
import grand.app.aber_user.pages.myOrders.viewModels.MyOrdersViewModels;
import grand.app.aber_user.utils.Constants;

public class MyOrdersFragment extends BaseFragment {
    FragmentMyOrdersBinding binding;
    @Inject
    MyOrdersViewModels viewModel;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_my_orders, container, false);
        IApplicationComponent component = ((MyApplication) requireActivity().getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setViewModel(viewModel);
        viewModel.myOrders(1, true);
        setEvent();
        return binding.getRoot();
    }

    private void setEvent() {
        viewModel.liveData.observe(requireActivity(), (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            if (Constants.My_ORDERS.equals(((Mutable) o).message)) {
                viewModel.setMainData(((MyOrdersResponse) mutable.object).getMyOrdersMainData());
            }
        });

    }

    @Override
    public void onResume() {
        viewModel.getServicesRepository().setLiveData(viewModel.liveData);
        super.onResume();
    }
}
