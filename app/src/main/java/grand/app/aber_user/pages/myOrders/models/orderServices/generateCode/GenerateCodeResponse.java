package grand.app.aber_user.pages.myOrders.models.orderServices.generateCode;

import com.google.gson.annotations.SerializedName;

import grand.app.aber_user.model.base.StatusMessage;

public class GenerateCodeResponse extends StatusMessage {
    @SerializedName("data")
    private GenerateCodeData generateCodeData;

    public GenerateCodeData getGenerateCodeData() {
        return generateCodeData;
    }
}
