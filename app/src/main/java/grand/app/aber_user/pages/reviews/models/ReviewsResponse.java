package grand.app.aber_user.pages.reviews.models;

import com.google.gson.annotations.SerializedName;

import grand.app.aber_user.model.base.StatusMessage;


public class ReviewsResponse extends StatusMessage {
    @SerializedName("data")
    private ReviewMainData reviewMainData;

    public ReviewMainData getReviewMainData() {
        return reviewMainData;
    }
}