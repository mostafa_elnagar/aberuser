package grand.app.aber_user.pages.auth.countries.viewModels;

import androidx.databinding.Bindable;
import androidx.lifecycle.MutableLiveData;

import javax.inject.Inject;

import grand.app.aber_user.base.BaseViewModel;
import grand.app.aber_user.model.base.Mutable;
import grand.app.aber_user.pages.auth.countries.adapters.CountriesAdapter;
import grand.app.aber_user.repository.AuthRepository;
import grand.app.aber_user.utils.Constants;
import io.reactivex.disposables.CompositeDisposable;

public class CountriesViewModel extends BaseViewModel {
    public MutableLiveData<Mutable> liveData;
    @Inject
    AuthRepository repository;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    CountriesAdapter countriesAdapter;

    @Inject
    public CountriesViewModel(AuthRepository repository) {
        this.repository = repository;
        this.liveData = new MutableLiveData<>();
        repository.setLiveData(liveData);

    }

    public void getCountries() {
        compositeDisposable.add(repository.getCountries());
    }

    public void toHome() {
        if (getCountriesAdapter().lastSelected != -1)
            liveData.setValue(new Mutable(Constants.HOME));
    }

    @Bindable
    public CountriesAdapter getCountriesAdapter() {
        return this.countriesAdapter == null ? this.countriesAdapter = new CountriesAdapter() : this.countriesAdapter;
    }

    private void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        unSubscribeFromObservable();
    }

    public AuthRepository getRepository() {
        return repository;
    }


}
