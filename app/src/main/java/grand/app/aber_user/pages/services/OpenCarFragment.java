package grand.app.aber_user.pages.services;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;

import com.google.gson.Gson;

import javax.inject.Inject;

import grand.app.aber_user.BR;
import grand.app.aber_user.PassingObject;
import grand.app.aber_user.R;
import grand.app.aber_user.base.BaseFragment;
import grand.app.aber_user.base.IApplicationComponent;
import grand.app.aber_user.base.MyApplication;
import grand.app.aber_user.connection.FileObject;
import grand.app.aber_user.databinding.FragmentOpenCarBinding;
import grand.app.aber_user.model.DropDownResponse;
import grand.app.aber_user.model.base.Mutable;
import grand.app.aber_user.pages.services.models.ServiceDetailsResponse;
import grand.app.aber_user.pages.services.viewModels.ServicesViewModels;
import grand.app.aber_user.utils.Constants;
import grand.app.aber_user.utils.PopUp.PopUpMenuHelper;
import grand.app.aber_user.utils.helper.LauncherHelper;
import grand.app.aber_user.utils.helper.MovementHelper;
import grand.app.aber_user.utils.upload.FileOperations;


public class OpenCarFragment extends BaseFragment {
    @Inject
    ServicesViewModels viewModel;
    FragmentOpenCarBinding binding;
    int startRequest;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_open_car, container, false);
        IApplicationComponent component = ((MyApplication) requireActivity().getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setViewModel(viewModel);
        binding.searchLocation.setIconTint(null);
        binding.picTime.setIconTint(null);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            String passingObject = bundle.getString(Constants.BUNDLE);
            viewModel.setPassingObject(new Gson().fromJson(passingObject, PassingObject.class));
            viewModel.getServiceDetails();
        }
        setEvent();
        return binding.getRoot();
    }

    private void setEvent() {
        viewModel.liveData.observe(requireActivity(), (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            viewModel.setMessage(mutable.message.equals(Constants.HIDE_PROGRESS) ? mutable.message : "");
            switch (((Mutable) o).message) {
                case Constants.SERVICE_DETAILS:
                    viewModel.setDetails(((ServiceDetailsResponse) (mutable).object).getData());
                    break;
                case Constants.SEARCH_LOCATION:
                    MovementHelper.startMapActivityForResultWithBundle(requireActivity(), new PassingObject(), getResources().getString(R.string.detect_location), Constants.LOCATION_REQUEST);
                    break;
                case Constants.IMAGE:
                    startRequest = Constants.FILE_TYPE_IMAGE;
                    LauncherHelper.execute(LauncherHelper.storage);
                    break;
                case Constants.front_car_ownership:
                    startRequest = Constants.FILE_TYPE_FRONT_OWNER;
                    LauncherHelper.execute(LauncherHelper.storage);
                    break;
                case Constants.back_car_ownership:
                    startRequest = Constants.FILE_TYPE_BACk_OWNER;
                    LauncherHelper.execute(LauncherHelper.storage);
                    break;
                case Constants.CAR_TYPE:
                    showCarType();
                    break;
                case Constants.CAR_CAT:
                    showCarCat();
                    break;
                case Constants.CAR_MODEL:
                    showCarModel();
                    break;
                case Constants.DELIVERY_TIME:
                    MovementHelper.startActivityForResultWithBundle(requireActivity(), new PassingObject(), null, ChooseServiceTimeFragment.class.getName(), Constants.DELIVERY_TIME_REQUEST);
                    break;
                case Constants.ORDER:
                    MovementHelper.startActivityForResultWithBundle(requireActivity(), new PassingObject(viewModel.getServiceOrderRequest()), null, FragmentConfirmOrder.class.getName(), Constants.ORDER_REQUEST);
                    break;
                case Constants.CHILD_VEHICLE:
                    if (viewModel.getServiceOrderRequest().getCarCat() == null)
                        viewModel.carCatList = ((DropDownResponse) mutable.object).getDropDownsObjects();
                    if (viewModel.getServiceOrderRequest().getCarModel() == null)
                        viewModel.carModelList = ((DropDownResponse) mutable.object).getDropDownsObjects();
                    break;

            }
        });
    }

    private void showCarModel() {
        PopUpMenuHelper.showCarTypePopUp(requireActivity(), binding.inputCarModels, viewModel.carModelList).setOnMenuItemClickListener(item -> {
            viewModel.getServiceOrderRequest().setCarModel(viewModel.carModelList.get(item.getItemId()).getName());
            viewModel.getServiceOrderRequest().setCarModelId(viewModel.carModelList.get(item.getItemId()).getId());
            viewModel.notifyChange(BR.serviceOrderRequest);
            return false;
        });
    }

    private void showCarCat() {
        PopUpMenuHelper.showCarTypePopUp(requireActivity(), binding.inputCarCategories, viewModel.carCatList).setOnMenuItemClickListener(item -> {
            viewModel.getServiceOrderRequest().setCarCat(viewModel.carCatList.get(item.getItemId()).getName());
            viewModel.getServiceOrderRequest().setCarCatId(viewModel.carCatList.get(item.getItemId()).getId());
            viewModel.getCarChildData(viewModel.carCatList.get(item.getItemId()).getId());
            viewModel.notifyChange(BR.serviceOrderRequest);
            return false;
        });
    }

    private void showCarType() {
        PopUpMenuHelper.showCarTypePopUp(requireActivity(), binding.inputCarTypes, viewModel.getDetails().getVehicleList()).setOnMenuItemClickListener(item -> {
            viewModel.getServiceOrderRequest().setCarType(viewModel.getDetails().getVehicleList().get(item.getItemId()).getName());
            viewModel.getServiceOrderRequest().setCarTypeId(viewModel.getDetails().getVehicleList().get(item.getItemId()).getId());
            viewModel.getCarChildData(viewModel.getDetails().getVehicleList().get(item.getItemId()).getId());
            viewModel.notifyChange(BR.serviceOrderRequest);
            return false;
        });
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LauncherHelper.checkPermission(this, startRequest, (request, result) -> {
            if (result)
                pickImageDialogSelect(startRequest);
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        viewModel.getPostRepository().setLiveData(viewModel.liveData);
    }

    @Override
    public void launchActivityResult(int request, int resultCode, Intent result) {
        super.launchActivityResult(request, resultCode, result);
        if (request == Constants.LOCATION_REQUEST) {
            viewModel.getServiceOrderRequest().setToLatitude(result.getDoubleExtra(Constants.LAT, 0.0));
            viewModel.getServiceOrderRequest().setToLongitude(result.getDoubleExtra(Constants.LNG, 0.0));
            viewModel.getServiceOrderRequest().setToAddress(result.getStringExtra(Constants.ADDRESS));
        } else if (request == Constants.DELIVERY_TIME_REQUEST) {
            Bundle bundle = result.getBundleExtra(Constants.BUNDLE);
            if (bundle != null && bundle.containsKey(Constants.BUNDLE)) {
                PassingObject passingObject = (PassingObject) bundle.getSerializable(Constants.BUNDLE);
                viewModel.getServiceOrderRequest().setTime(passingObject.getObject2());
                viewModel.getServiceOrderRequest().setDate(passingObject.getObject());
            }
        } else if (request == Constants.ORDER_REQUEST) {
            finishActivity();
        } else if (request == Constants.FILE_TYPE_IMAGE) {
            toastMessage(getString(R.string.image_selected));
            FileObject fileObject = FileOperations.getFileObject(requireActivity(), result, Constants.IMAGE, Constants.FILE_TYPE_IMAGE);
            binding.carLicense.setText(fileObject.getFile().getName());
            viewModel.getServiceOrderRequest().setTyreImage(fileObject.getFilePath());
        } else if (request == Constants.FILE_TYPE_FRONT_OWNER) {
            toastMessage(getString(R.string.image_front_selected));
            FileObject fileObject = FileOperations.getFileObject(requireActivity(), result, Constants.front_car_ownership, Constants.FILE_TYPE_IMAGE);
            viewModel.getServiceOrderRequest().setFrontOwnerImage(fileObject.getFilePath());
        } else if (request == Constants.FILE_TYPE_BACk_OWNER) {
            toastMessage(getString(R.string.image_back_selected));
            FileObject fileObject = FileOperations.getFileObject(requireActivity(), result, Constants.back_car_ownership, Constants.FILE_TYPE_IMAGE);
            viewModel.getServiceOrderRequest().setBackOwnerImage(fileObject.getFilePath());
        }
    }
}
