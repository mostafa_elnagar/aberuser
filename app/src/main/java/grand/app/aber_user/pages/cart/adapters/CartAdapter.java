package grand.app.aber_user.pages.cart.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import grand.app.aber_user.R;
import grand.app.aber_user.databinding.ItemCartBinding;
import grand.app.aber_user.pages.cart.viewModels.ItemCartViewModel;
import grand.app.aber_user.pages.parts.models.ProductsItem;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.MenuView> {
    List<ProductsItem> menuModels;
    MutableLiveData<Integer> liveDataAdapter = new MutableLiveData<>();
    Context context;
    public boolean isConfirm;

    public CartAdapter() {
        this.menuModels = new ArrayList<>();
    }

    public List<ProductsItem> getMenuModels() {
        return menuModels;
    }

    public MutableLiveData<Integer> getLiveDataAdapter() {
        return liveDataAdapter;
    }

    @NonNull
    @Override
    public MenuView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cart,
                parent, false);
        this.context = parent.getContext();
        return new MenuView(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull final MenuView holder, final int position) {
        ProductsItem menuModel = menuModels.get(position);
        menuModel.setConfirmed(isConfirm);
        ItemCartViewModel itemMenuViewModel = new ItemCartViewModel(menuModel);
        itemMenuViewModel.getLiveData().observe((LifecycleOwner) context, o -> getLiveDataAdapter().setValue(menuModel.getProduct_room_id()));
        holder.setViewModel(itemMenuViewModel);
    }

    public void update(@NotNull List<ProductsItem> dataList, boolean isConfirm) {
        this.menuModels.clear();
        menuModels.addAll(dataList);
        this.isConfirm = isConfirm;
        notifyDataSetChanged();
    }

    @Override
    public void onViewAttachedToWindow(@NotNull MenuView holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NotNull MenuView holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    @Override
    public int getItemCount() {
        return menuModels.size();
    }

    public class MenuView extends RecyclerView.ViewHolder {
        public ItemCartBinding itemMenuBinding;

        MenuView(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemMenuBinding == null) {
                itemMenuBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemMenuBinding != null) {
                itemMenuBinding.unbind();
            }
        }

        void setViewModel(ItemCartViewModel itemViewModels) {
            if (itemMenuBinding != null) {
                itemMenuBinding.setItemViewModel(itemViewModels);
            }
        }
    }
}
