package grand.app.aber_user.pages.parts.viewModels;


import androidx.databinding.Bindable;

import grand.app.aber_user.BR;
import grand.app.aber_user.base.BaseViewModel;
import grand.app.aber_user.base.MyApplication;
import grand.app.aber_user.model.base.Mutable;
import grand.app.aber_user.pages.parts.models.ProductsItem;
import grand.app.aber_user.repository.CartRepository;
import grand.app.aber_user.utils.Constants;
import grand.app.aber_user.utils.session.UserHelper;

public class ItemProductsViewModel extends BaseViewModel {
    public ProductsItem productsItem;

    public ItemProductsViewModel(ProductsItem productsItem) {
        this.productsItem = productsItem;
    }

    @Bindable
    public ProductsItem getProductsItem() {
        return productsItem;
    }

    public void itemAction(String action) {
        if (action.equals(Constants.LIKES_REACTION) && userData != null) {
            productsItem.setFavorite(!productsItem.isIsFavorite());
            notifyChange(BR.productsItem);
        } else if (action.equals(Constants.CART) && userData != null) {
            if (getProductsItem().getAttributes() != 1) { // not have colors
                ProductsItem productsItem = new ProductsItem();
                productsItem.setId(getProductsItem().getId());
                productsItem.setName(getProductsItem().getName());
                productsItem.setPriceItem(Double.parseDouble(getProductsItem().getPrice())); // for calculate total form one item
                productsItem.setPrice(getProductsItem().getPrice());
                productsItem.setCurrency(getProductsItem().getCurrency());
                productsItem.setQuantity(1);
                productsItem.setImage(getProductsItem().getImage());
                new CartRepository(MyApplication.getInstance()).insert(productsItem);
            } else
                action = Constants.SERVICE_DETAILS;
        }
        getLiveData().setValue(action);
    }

}
