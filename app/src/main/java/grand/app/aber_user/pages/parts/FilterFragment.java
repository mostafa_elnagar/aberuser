package grand.app.aber_user.pages.parts;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;

import com.github.guilhe.views.SeekBarRangedView;
import com.google.gson.Gson;

import javax.inject.Inject;

import grand.app.aber_user.BR;
import grand.app.aber_user.PassingObject;
import grand.app.aber_user.R;
import grand.app.aber_user.base.BaseFragment;
import grand.app.aber_user.base.IApplicationComponent;
import grand.app.aber_user.base.MyApplication;
import grand.app.aber_user.databinding.FragmentFilterBinding;
import grand.app.aber_user.model.DropDownResponse;
import grand.app.aber_user.model.base.Mutable;
import grand.app.aber_user.pages.parts.models.ProductsResponse;
import grand.app.aber_user.pages.parts.models.filter.FilterDataResponse;
import grand.app.aber_user.pages.parts.viewModels.FilterViewModels;
import grand.app.aber_user.utils.Constants;
import grand.app.aber_user.utils.helper.MovementHelper;

public class FilterFragment extends BaseFragment {
    @Inject
    FilterViewModels viewModel;
    FragmentFilterBinding binding;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_filter, container, false);
        IApplicationComponent component = ((MyApplication) requireActivity().getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setViewModel(viewModel);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            String passingObject = bundle.getString(Constants.BUNDLE);
            viewModel.setPassingObject(new Gson().fromJson(passingObject, PassingObject.class));
            viewModel.getFilter();
        }
        setEvent();
        return binding.getRoot();
    }

    private void setEvent() {
        viewModel.liveData.observe(requireActivity(), (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            viewModel.setMessage(mutable.message.equals(Constants.HIDE_PROGRESS) ? mutable.message : "");
            if (Constants.FILTER.equals(((Mutable) o).message)) {
                viewModel.setFilterData(((FilterDataResponse) mutable.object).getData());
                setUpProgressBar();
            } else if (Constants.CHILD_VEHICLE.equals(((Mutable) o).message)) {
                viewModel.getCarCatAdapter().update(((DropDownResponse) mutable.object).getDropDownsObjects());
                viewModel.notifyChange(BR.carCatAdapter);
            } else if (Constants.CAR_MODEL.equals(((Mutable) o).message)) {
                viewModel.getCarModelAdapter().update(((DropDownResponse) mutable.object).getDropDownsObjects());
                viewModel.notifyChange(BR.carModelAdapter);
            } else if (Constants.FILTER_RESULT.equals(((Mutable) o).message)) {
                if (!((ProductsResponse) mutable.object).getProductMain().getProductsCount().equals("0"))
                    MovementHelper.finishWithResult(new PassingObject(((ProductsResponse) mutable.object).getProductMain()), requireActivity(), Constants.ORDER_REQUEST);
                else
                    toastErrorMessage(getString(R.string.empty_data));
            }
        });
        viewModel.getCarTypeAdapter().liveData.observe(requireActivity(), o -> viewModel.getCarCat(viewModel.getCarTypeAdapter().lastSelected));
        viewModel.getCarCatAdapter().liveData.observe(requireActivity(), o -> viewModel.getCarModel(viewModel.getCarCatAdapter().lastSelected));
    }

    private void setUpProgressBar() {
//        price bar
        binding.tvMinPrice.setText(viewModel.getFilterData().getMinPrice().concat(" ").concat(viewModel.currency));
        binding.tvMaxPrice.setText(viewModel.getFilterData().getMaxPrice().concat(" ").concat(viewModel.currency));
        binding.rangeBar.setMinValue(Float.parseFloat(viewModel.getFilterData().getMinPrice()));
        binding.rangeBar.setMaxValue(Float.parseFloat(viewModel.getFilterData().getMaxPrice()));
        viewModel.getFilterRequest().setMin_price(String.valueOf(viewModel.getFilterData().getMinPrice()));
        viewModel.getFilterRequest().setMax_price(String.valueOf(viewModel.getFilterData().getMaxPrice()));
        binding.rangeBar.setOnSeekBarRangedChangeListener(new SeekBarRangedView.OnSeekBarRangedChangeListener() {
            @Override
            public void onChanged(SeekBarRangedView view, float minValue, float maxValue) {

            }

            @Override
            public void onChanging(SeekBarRangedView view, float minValue, float maxValue) {
                binding.tvMinPrice.setText(String.valueOf((int) minValue).concat(" ").concat(viewModel.currency));
                binding.tvMaxPrice.setText(String.valueOf((int) maxValue).concat(" ").concat(viewModel.currency));
                viewModel.getFilterRequest().setMin_price(String.valueOf((int) minValue));
                viewModel.getFilterRequest().setMax_price(String.valueOf((int) maxValue));

            }
        });

    }


    @Override
    public void onResume() {
        super.onResume();
        viewModel.getPostRepository().setLiveData(viewModel.liveData);
    }
}
