package grand.app.aber_user.pages.parts.models.filter;

import java.util.List;
import com.google.gson.annotations.SerializedName;

import grand.app.aber_user.model.DropDownsObject;
import grand.app.aber_user.pages.parts.models.productDetails.AttributesItem;

public class FilterData {

	@SerializedName("max_price")
	private String maxPrice;

	@SerializedName("min_price")
	private String minPrice;

	@SerializedName("attributes")
	private List<AttributesItem> attributes;

	@SerializedName("vehicle")
	private List<DropDownsObject> vehicle;

	public String getMaxPrice(){
		return maxPrice;
	}

	public String getMinPrice(){
		return minPrice;
	}

	public List<AttributesItem> getAttributes(){
		return attributes;
	}

	public List<DropDownsObject> getVehicle(){
		return vehicle;
	}
}