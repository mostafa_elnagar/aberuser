
package grand.app.aber_user.pages.chat.viewmodel;

import android.text.TextUtils;

import androidx.databinding.Bindable;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import grand.app.aber_user.BR;
import grand.app.aber_user.base.BaseViewModel;
import grand.app.aber_user.connection.FileObject;
import grand.app.aber_user.model.base.Mutable;
import grand.app.aber_user.pages.auth.models.UserData;
import grand.app.aber_user.pages.chat.adapter.ChatAdapter;
import grand.app.aber_user.pages.chat.model.ChatRequest;
import grand.app.aber_user.pages.conversations.models.ConversationsData;
import grand.app.aber_user.pages.conversations.models.ConversationsMain;
import grand.app.aber_user.repository.ChatRepository;
import grand.app.aber_user.utils.Constants;
import io.reactivex.disposables.CompositeDisposable;

public class ChatViewModel extends BaseViewModel {
    public
    MutableLiveData<Mutable> liveData;
    @Inject
    public ChatRepository repository;
    ChatAdapter adapter;
    public ChatRequest request = new ChatRequest();
    public List<FileObject> fileObjectList = new ArrayList<>();
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    UserData conversationsData;
    ConversationsMain conversationsMain;

    @Inject
    public ChatViewModel(ChatRepository repository) {
        this.repository = repository;
        this.liveData = new MutableLiveData<>();
        repository.setLiveData(liveData);
    }

    public void chat() {
        compositeDisposable.add(repository.getChat(getConversationsData().getId()));
    }

    @Bindable
    public ConversationsMain getConversationsMain() {
        return this.conversationsMain == null ? this.conversationsMain = new ConversationsMain() : this.conversationsMain;
    }

    @Bindable
    public void setConversationsMain(ConversationsMain conversationsMain) {
        if (getAdapter().getChatList().size() > 0) {
            getAdapter().loadMore(conversationsMain.getData());
        } else {
            getAdapter().update(conversationsMain.getData());
            notifyChange(BR.adapter);
        }
        searchProgressVisible.set(false);
        notifyChange(BR.conversationsMain);
        this.conversationsMain = conversationsMain;
    }

    @Bindable
    public UserData getConversationsData() {
        return this.conversationsData == null ? this.conversationsData = new UserData() : this.conversationsData;
    }

    @Bindable
    public ChatAdapter getAdapter() {
        return this.adapter == null ? this.adapter = new ChatAdapter() : this.adapter;
    }

    @Bindable
    public void setConversationsData(UserData conversationsData) {
        notifyChange(BR.conversationsData);
        this.conversationsData = conversationsData;
        chat();
    }

    private void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        unSubscribeFromObservable();
    }

    public void select() {
        liveData.setValue(new Mutable(Constants.IMAGE));
    }

    public void sendMessage() {
        request.setListing_id(String.valueOf(getPassingObject().getId()));
        request.setUserId(String.valueOf(getConversationsData().getId()));
        if (fileObjectList.size() > 0 || !TextUtils.isEmpty(request.getMessage())) {
            setMessage(Constants.SHOW_PROGRESS);
            repository.sendChat(request, fileObjectList);
        }
    }
}
