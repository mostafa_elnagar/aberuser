package grand.app.aber_user.pages.services.adapters;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import grand.app.aber_user.R;
import grand.app.aber_user.databinding.ItemOilPricesListBinding;
import grand.app.aber_user.model.DropDownsObject;
import grand.app.aber_user.pages.services.viewModels.ItemOilPriceListViewModel;

public class OilPricesAdapter extends RecyclerView.Adapter<OilPricesAdapter.MenuView> {
    List<DropDownsObject> dropDownsObjectList;
    public OilPricesAdapter() {
        this.dropDownsObjectList = new ArrayList<>();
    }

    @NonNull
    @Override
    public MenuView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_oil_prices_list,
                parent, false);
        return new MenuView(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull final MenuView holder, @SuppressLint("RecyclerView") final int position) {
        DropDownsObject menuModel = dropDownsObjectList.get(position);
        ItemOilPriceListViewModel itemMenuViewModel = new ItemOilPriceListViewModel(menuModel);
        holder.setViewModel(itemMenuViewModel);
    }

    public void update(@NotNull List<DropDownsObject> dataList) {
        this.dropDownsObjectList.clear();
        dropDownsObjectList.addAll(dataList);
        notifyDataSetChanged();
    }

    @Override
    public void onViewAttachedToWindow(@NotNull MenuView holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NotNull MenuView holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    @Override
    public int getItemCount() {
        return dropDownsObjectList.size();
    }

    public class MenuView extends RecyclerView.ViewHolder {
        public ItemOilPricesListBinding itemMenuBinding;

        MenuView(View itemView) {
            super(itemView);
            bind();
        }

        void bind() {
            if (itemMenuBinding == null) {
                itemMenuBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemMenuBinding != null) {
                itemMenuBinding.unbind();
            }
        }

        void setViewModel(ItemOilPriceListViewModel itemViewModels) {
            if (itemMenuBinding != null) {
                itemMenuBinding.setItemViewModel(itemViewModels);
            }
        }
    }
}
