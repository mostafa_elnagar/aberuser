package grand.app.aber_user.pages.myOrders.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import grand.app.aber_user.PassingObject;
import grand.app.aber_user.R;
import grand.app.aber_user.databinding.ItemMainMyOrderBinding;
import grand.app.aber_user.pages.myOrders.MyOrderDetailsFragment;
import grand.app.aber_user.pages.myOrders.models.MyOrderData;
import grand.app.aber_user.pages.myOrders.viewModels.ItemMyOrdersViewModel;
import grand.app.aber_user.utils.helper.MovementHelper;
import grand.app.aber_user.utils.resources.ResourceManager;

public class MyOrdersAdapter extends RecyclerView.Adapter<MyOrdersAdapter.MenuView> {
    List<MyOrderData> menuModels;
    Context context;

    public MyOrdersAdapter() {
        this.menuModels = new ArrayList<>();
    }

    public List<MyOrderData> getMenuModels() {
        return menuModels;
    }

    @NonNull
    @Override
    public MenuView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_main_my_order,
                parent, false);
        this.context = parent.getContext();
        return new MenuView(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull final MenuView holder, final int position) {
        MyOrderData menuModel = menuModels.get(position);
        ItemMyOrdersViewModel itemMenuViewModel = new ItemMyOrdersViewModel(menuModel);
        itemMenuViewModel.getLiveData().observe((LifecycleOwner) context, o -> MovementHelper.startActivityWithBundle(context, new PassingObject(menuModel.getId()), ResourceManager.getString(R.string.my_orders), MyOrderDetailsFragment.class.getName(), null));
        holder.setViewModel(itemMenuViewModel);
    }

    public void update(@NotNull List<MyOrderData> dataList) {
        this.menuModels.clear();
        menuModels.addAll(dataList);
        notifyDataSetChanged();
    }

    public void loadMore(@NotNull List<MyOrderData> dataList) {
        int start = menuModels.size();
        menuModels.addAll(dataList);
        notifyItemRangeInserted(start, dataList.size());
    }

    @Override
    public void onViewAttachedToWindow(@NotNull MenuView holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NotNull MenuView holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    @Override
    public int getItemCount() {
        return menuModels.size();
    }

    public class MenuView extends RecyclerView.ViewHolder {
        public ItemMainMyOrderBinding itemMenuBinding;

        MenuView(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemMenuBinding == null) {
                itemMenuBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemMenuBinding != null) {
                itemMenuBinding.unbind();
            }
        }

        void setViewModel(ItemMyOrdersViewModel itemViewModels) {
            if (itemMenuBinding != null) {
                itemMenuBinding.setItemOrderViewModel(itemViewModels);
            }
        }
    }
}
