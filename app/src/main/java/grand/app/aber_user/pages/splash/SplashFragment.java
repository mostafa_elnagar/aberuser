package grand.app.aber_user.pages.splash;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;


import com.bumptech.glide.Glide;

import javax.inject.Inject;

import grand.app.aber_user.base.BaseFragment;
import grand.app.aber_user.base.IApplicationComponent;
import grand.app.aber_user.base.MyApplication;
import grand.app.aber_user.model.base.Mutable;
import grand.app.aber_user.R;
import grand.app.aber_user.databinding.FragmentSplashBinding;
import grand.app.aber_user.pages.auth.countries.CountriesFragment;
import grand.app.aber_user.pages.auth.login.LoginFragment;
import grand.app.aber_user.pages.onBoard.OnBoardFragment;
import grand.app.aber_user.utils.Constants;
import grand.app.aber_user.utils.helper.MovementHelper;
import grand.app.aber_user.utils.session.UserHelper;

public class SplashFragment extends BaseFragment {
    FragmentSplashBinding fragmentSplashBinding;
    @Inject
    SplashViewModel viewModel;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentSplashBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_splash, container, false);
        IApplicationComponent component = ((MyApplication) requireActivity().getApplicationContext()).getApplicationComponent();
        component.inject(this);
        fragmentSplashBinding.setViewmodel(viewModel);
        setEvent();
        viewModel.runSplash();
        return fragmentSplashBinding.getRoot();
    }

    private void setEvent() {
        viewModel.liveData.observe(requireActivity(), (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
//            if (((Mutable) o).message.equals(Constants.HOME)) {
            if (!UserHelper.getInstance(MyApplication.getInstance()).getIsFirst()) {
                if (UserHelper.getInstance(MyApplication.getInstance()).getCountriesData() != null)
                    MovementHelper.startActivityMain(requireActivity());
                else
                    MovementHelper.startActivityBase(requireActivity(), CountriesFragment.class.getName(), getString(R.string.country), null);
            }else {
                MovementHelper.startActivityBase(requireActivity(), OnBoardFragment.class.getName(), null, null);
            }
//            } else if (((Mutable) o).message.equals(Constants.BACKGROUND_API)) {
//                if (UserHelper.getInstance(MyApplication.getInstance()).getIsFirst()) {
//                    MovementHelper.startActivityBase(requireActivity(), OnBoardFragment.class.getName(), null, null);
//                }
//            }
        });
    }

}
