package grand.app.aber_user.pages.services.viewModels;

import android.widget.CompoundButton;

import androidx.databinding.Bindable;
import androidx.databinding.ObservableBoolean;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import grand.app.aber_user.BR;
import grand.app.aber_user.base.BaseViewModel;
import grand.app.aber_user.base.MyApplication;
import grand.app.aber_user.model.DropDownsObject;
import grand.app.aber_user.model.base.Mutable;
import grand.app.aber_user.pages.services.adapters.ExtraAdapter;
import grand.app.aber_user.pages.services.adapters.OilPricesAdapter;
import grand.app.aber_user.pages.services.adapters.ServiceDetailsAdapter;
import grand.app.aber_user.pages.services.models.Extra;
import grand.app.aber_user.pages.services.models.ServiceDetails;
import grand.app.aber_user.pages.services.models.ServiceOrderDetails;
import grand.app.aber_user.pages.services.models.ServicesItem;
import grand.app.aber_user.repository.ServicesRepository;
import grand.app.aber_user.utils.Constants;
import grand.app.aber_user.utils.session.UserHelper;
import io.reactivex.disposables.CompositeDisposable;

public class ServicesViewModels extends BaseViewModel {

    public MutableLiveData<Mutable> liveData;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    ServiceDetailsAdapter detailsAdapter;
    ExtraAdapter extraAdapter;
    OilPricesAdapter oilPricesAdapter;
    @Inject
    ServicesRepository postRepository;
    ServiceDetails details;
    public ObservableBoolean isEmergencyAccepted = new ObservableBoolean();
    ServiceOrderDetails serviceOrderRequest;
    public List<DropDownsObject> carModelList, carCatList,tyres, tyreDesc, oilLiquidList, hiddenColorList, hiddenPercentageList, fuelCatList, batterySizeList;

    @Inject
    public ServicesViewModels(ServicesRepository postRepository) {
        carModelList = new ArrayList<>();
        carCatList = new ArrayList<>();
        tyreDesc = new ArrayList<>();
        tyres = new ArrayList<>();
        oilLiquidList = new ArrayList<>();
        hiddenColorList = new ArrayList<>();
        hiddenPercentageList = new ArrayList<>();
        fuelCatList = new ArrayList<>();
        batterySizeList = new ArrayList<>();
        serviceOrderRequest = new ServiceOrderDetails();
        details = new ServiceDetails();
        this.postRepository = postRepository;
        this.liveData = new MutableLiveData<>();
        postRepository.setLiveData(liveData);
    }

    public void getServiceDetails() {
        compositeDisposable.add(postRepository.getServiceDetails(getPassingObject().getId(), UserHelper.getInstance(MyApplication.getInstance()).getCountriesData().getId()));
    }

    public void getCarChildData(int childId) {
        compositeDisposable.add(postRepository.getCarChildData(childId));
    }

    public void getTyredData(int childId) {
        compositeDisposable.add(postRepository.getTyredData(childId));
    }

    public void getTyres(int parentId) {
        compositeDisposable.add(postRepository.getTyres(parentId));
    }

    public void getOilLiquidData(int childId) {
        compositeDisposable.add(postRepository.getOilLiquidData(childId));
    }

    public void getHiddenColorData(int childId) {
        compositeDisposable.add(postRepository.getHiddenColorData(childId, Constants.HIDDEN_COLOR_REQUEST));
    }

    public void getHiddenPercentageData(int childId) {
        compositeDisposable.add(postRepository.getHiddenColorData(childId, Constants.HIDDEN_PERCENTAGE_REQUEST));
    }

    public void getFuelData(int childId) {
        compositeDisposable.add(postRepository.getFuelCatData(childId));
    }

    public void getBatteryData(int childId) {
        compositeDisposable.add(postRepository.getBatteryData(childId));
    }

    public void toConfirmServiceOrder() {
        if (userData != null) {
            setMessage(Constants.SHOW_PROGRESS);
            List<ServicesItem> selectedIds = new ArrayList<>();
            for (int i = 0; i < getDetailsAdapter().getServicesItemList().size(); i++) {
                if (getDetailsAdapter().getServicesItemList().get(i).isChecked()) {
                    serviceOrderRequest.setCurrency(getDetailsAdapter().getServicesItemList().get(i).getCurrency());
                    serviceOrderRequest.setTotalServices(serviceOrderRequest.getTotalServices() + Double.parseDouble(getDetailsAdapter().getServicesItemList().get(i).getPrice()));
                    selectedIds.add(getDetailsAdapter().getServicesItemList().get(i));
                }
            }
            getServiceOrderRequest().setServicesIds(selectedIds);
            getServiceOrderRequest().setTotal(getServiceOrderRequest().getTotalServices() + getServiceOrderRequest().getMovingPriceServices());
            if (getServiceOrderRequest().isWinchValid() != null) {
                serviceOrderRequest.setTotalServices(0.0); // reset total services
                setMessage(Constants.HIDE_PROGRESS);
                liveData.setValue(new Mutable(Constants.ERROR_TOAST, getServiceOrderRequest().isWinchValid()));
            } else {
                calcTotal();
                setMessage(Constants.HIDE_PROGRESS);
                liveDataActions(Constants.ORDER);
            }
        } else liveData.setValue(new Mutable(Constants.LOGIN_WARNING));
    }

    public void toConfirmServiceCheckOrder() {
        if (userData != null) {
            setMessage(Constants.SHOW_PROGRESS);
            List<ServicesItem> selectedIds = new ArrayList<>();
            for (int i = 0; i < getDetailsAdapter().getServicesItemList().size(); i++) {
                if (getDetailsAdapter().getServicesItemList().get(i).isChecked()) {
                    serviceOrderRequest.setCurrency(getDetailsAdapter().getServicesItemList().get(i).getCurrency());
                    serviceOrderRequest.setTotalServices(serviceOrderRequest.getTotalServices() + Double.parseDouble(getDetailsAdapter().getServicesItemList().get(i).getPrice()));
                    selectedIds.add(getDetailsAdapter().getServicesItemList().get(i));
                }
            }
            getServiceOrderRequest().setServicesIds(selectedIds);
            getServiceOrderRequest().setTotal(getServiceOrderRequest().getTotalServices() + getServiceOrderRequest().getMovingPriceServices());
            if (getServiceOrderRequest().isCarCheckValid() != null) {
                serviceOrderRequest.setTotalServices(0.0); // reset total services
                setMessage(Constants.HIDE_PROGRESS);
                liveData.setValue(new Mutable(Constants.ERROR_TOAST, getServiceOrderRequest().isCarCheckValid()));
            } else {
                calcTotal();
                setMessage(Constants.HIDE_PROGRESS);
                liveDataActions(Constants.ORDER);
            }
        } else liveData.setValue(new Mutable(Constants.LOGIN_WARNING));
    }

    public void toConfirmServiceWash() {
        if (userData != null) {
            setMessage(Constants.SHOW_PROGRESS);
            List<ServicesItem> selectedIds = new ArrayList<>();
            List<Extra> extraIds = new ArrayList<>();
            for (int i = 0; i < getDetailsAdapter().getServicesItemList().size(); i++) {
                if (getDetailsAdapter().getServicesItemList().get(i).isChecked()) {
                    serviceOrderRequest.setCurrency(getDetailsAdapter().getServicesItemList().get(i).getCurrency());
                    serviceOrderRequest.setTotalServices(serviceOrderRequest.getTotalServices() + Double.parseDouble(getDetailsAdapter().getServicesItemList().get(i).getPrice()));
                    selectedIds.add(getDetailsAdapter().getServicesItemList().get(i));
                }
            }
            for (int i = 0; i < getExtraAdapter().getServicesItemList().size(); i++) {
                if (getExtraAdapter().getServicesItemList().get(i).isChecked()) {
                    serviceOrderRequest.setExtraTotalServices(serviceOrderRequest.getExtraTotalServices() + Double.parseDouble(getExtraAdapter().getServicesItemList().get(i).getPrice()));
                    extraIds.add(getExtraAdapter().getServicesItemList().get(i));
                }
            }
            getServiceOrderRequest().setServicesIds(selectedIds);
            getServiceOrderRequest().setExtraList(extraIds);
            if (getServiceOrderRequest().isCarCheckValid() != null) {
                serviceOrderRequest.setTotalServices(0.0); // reset total services
                serviceOrderRequest.setExtraTotalServices(0.0); // reset extra total services
                setMessage(Constants.HIDE_PROGRESS);
                liveData.setValue(new Mutable(Constants.ERROR_TOAST, getServiceOrderRequest().isCarCheckValid()));
            } else {
                calcTotal();
                setMessage(Constants.HIDE_PROGRESS);
                liveDataActions(Constants.ORDER);
            }
        } else liveData.setValue(new Mutable(Constants.LOGIN_WARNING));
    }

    public void toConfirmServiceTyer() {
        if (userData != null) {
            setMessage(Constants.SHOW_PROGRESS);
            List<ServicesItem> selectedIds = new ArrayList<>();
            for (int i = 0; i < getDetailsAdapter().getServicesItemList().size(); i++) {
                if (getDetailsAdapter().getServicesItemList().get(i).isChecked()) {
                    serviceOrderRequest.setCurrency(getDetailsAdapter().getServicesItemList().get(i).getCurrency());
                    serviceOrderRequest.setTotalServices(serviceOrderRequest.getTotalServices() + Double.parseDouble(getDetailsAdapter().getServicesItemList().get(i).getPrice()));
                    selectedIds.add(getDetailsAdapter().getServicesItemList().get(i));
                }
            }
            getServiceOrderRequest().setServicesIds(selectedIds);
            getServiceOrderRequest().setExtraList(new ArrayList<>());
            if (getServiceOrderRequest().isTyreValid() != null) {
                serviceOrderRequest.setTotalServices(0.0); // reset total services
                setMessage(Constants.HIDE_PROGRESS);
                liveData.setValue(new Mutable(Constants.ERROR_TOAST, getServiceOrderRequest().isTyreValid()));
            } else {
                calcTotal();
                setMessage(Constants.HIDE_PROGRESS);
                liveDataActions(Constants.ORDER);
            }
        } else liveData.setValue(new Mutable(Constants.LOGIN_WARNING));
    }

    public void toConfirmServiceOil() {
        if (userData != null) {
            setMessage(Constants.SHOW_PROGRESS);
            List<Extra> extraIds = new ArrayList<>();
            for (int i = 0; i < getExtraAdapter().getServicesItemList().size(); i++) {
                if (getExtraAdapter().getServicesItemList().get(i).isChecked()) {
                    serviceOrderRequest.setCurrency(getExtraAdapter().getServicesItemList().get(i).getCurrency());
                    serviceOrderRequest.setExtraTotalServices(serviceOrderRequest.getExtraTotalServices() + Double.parseDouble(getExtraAdapter().getServicesItemList().get(i).getPrice()));
                    extraIds.add(getExtraAdapter().getServicesItemList().get(i));
                }
            }
            getServiceOrderRequest().setExtraList(extraIds);
            if (getServiceOrderRequest().isOilValid() != null) {
                setMessage(Constants.HIDE_PROGRESS);
                liveData.setValue(new Mutable(Constants.ERROR_TOAST, getServiceOrderRequest().isOilValid()));
            } else {
                calcTotal();
                setMessage(Constants.HIDE_PROGRESS);
                liveDataActions(Constants.ORDER);
            }
        } else liveData.setValue(new Mutable(Constants.LOGIN_WARNING));
    }

    public void toConfirmServiceHidden() {
        if (userData != null) {
            setMessage(Constants.SHOW_PROGRESS);
            List<ServicesItem> selectedIds = new ArrayList<>();
            for (int i = 0; i < getDetailsAdapter().getServicesItemList().size(); i++) {
                if (getDetailsAdapter().getServicesItemList().get(i).isChecked()) {
                    serviceOrderRequest.setCurrency(getDetailsAdapter().getServicesItemList().get(i).getCurrency());
                    serviceOrderRequest.setTotalServices(serviceOrderRequest.getTotalServices() + Double.parseDouble(getDetailsAdapter().getServicesItemList().get(i).getPrice()));
                    selectedIds.add(getDetailsAdapter().getServicesItemList().get(i));
                }
            }
            getServiceOrderRequest().setServicesIds(selectedIds);
            if (getServiceOrderRequest().isHiddenValid() != null) {
                serviceOrderRequest.setTotalServices(0.0); // reset total services
                setMessage(Constants.HIDE_PROGRESS);
                liveData.setValue(new Mutable(Constants.ERROR_TOAST, getServiceOrderRequest().isHiddenValid()));
            } else {
                calcTotal();
                setMessage(Constants.HIDE_PROGRESS);
                liveDataActions(Constants.ORDER);
            }
        } else liveData.setValue(new Mutable(Constants.LOGIN_WARNING));
    }

    public void toConfirmServiceFuel() {
        if (userData != null) {
            setMessage(Constants.SHOW_PROGRESS);
            if (getServiceOrderRequest().isFuelValid() != null) {
                setMessage(Constants.HIDE_PROGRESS);
                liveData.setValue(new Mutable(Constants.ERROR_TOAST, getServiceOrderRequest().isFuelValid()));
            } else {
                calcTotal();
                setMessage(Constants.HIDE_PROGRESS);
                liveDataActions(Constants.ORDER);
            }
        } else liveData.setValue(new Mutable(Constants.LOGIN_WARNING));
    }

    public void toConfirmServiceWater() {
        if (userData != null) {
            setMessage(Constants.SHOW_PROGRESS);
            if (getServiceOrderRequest().isLitreValid() != null) {
                setMessage(Constants.HIDE_PROGRESS);
                liveData.setValue(new Mutable(Constants.ERROR_TOAST, getServiceOrderRequest().isLitreValid()));
            } else {
                calcTotal();
                setMessage(Constants.HIDE_PROGRESS);
                liveDataActions(Constants.ORDER);
            }
        } else liveData.setValue(new Mutable(Constants.LOGIN_WARNING));
    }

    public void toConfirmServiceOpenCar() {
        if (userData != null) {
            setMessage(Constants.SHOW_PROGRESS);
            List<ServicesItem> selectedIds = new ArrayList<>();
            for (int i = 0; i < getDetailsAdapter().getServicesItemList().size(); i++) {
                if (getDetailsAdapter().getServicesItemList().get(i).isChecked()) {
                    serviceOrderRequest.setCurrency(getDetailsAdapter().getServicesItemList().get(i).getCurrency());
                    serviceOrderRequest.setTotalServices(serviceOrderRequest.getTotalServices() + Double.parseDouble(getDetailsAdapter().getServicesItemList().get(i).getPrice()));
                    selectedIds.add(getDetailsAdapter().getServicesItemList().get(i));
                }
            }
            getServiceOrderRequest().setServicesIds(selectedIds);
            if (getServiceOrderRequest().isOpenCarValid() != null) {
                serviceOrderRequest.setTotalServices(0.0); // reset total services
                setMessage(Constants.HIDE_PROGRESS);
                liveData.setValue(new Mutable(Constants.ERROR_TOAST, getServiceOrderRequest().isOpenCarValid()));
            } else {
                calcTotal();
                setMessage(Constants.HIDE_PROGRESS);
                liveDataActions(Constants.ORDER);
            }
        } else liveData.setValue(new Mutable(Constants.LOGIN_WARNING));
    }

    public void toConfirmServiceBattery() {
        if (userData != null) {
            setMessage(Constants.SHOW_PROGRESS);
            List<ServicesItem> selectedIds = new ArrayList<>();
            for (int i = 0; i < getDetailsAdapter().getServicesItemList().size(); i++) {
                if (getDetailsAdapter().getServicesItemList().get(i).isChecked()) {
                    serviceOrderRequest.setCurrency(getDetailsAdapter().getServicesItemList().get(i).getCurrency());
                    serviceOrderRequest.setTotalServices(serviceOrderRequest.getTotalServices() + Double.parseDouble(getDetailsAdapter().getServicesItemList().get(i).getPrice()));
                    selectedIds.add(getDetailsAdapter().getServicesItemList().get(i));
                }
            }
            getServiceOrderRequest().setServicesIds(selectedIds);
            if (getServiceOrderRequest().isBatteryValid() != null) {
                serviceOrderRequest.setTotalServices(0.0); // reset total services
                setMessage(Constants.HIDE_PROGRESS);
                liveData.setValue(new Mutable(Constants.ERROR_TOAST, getServiceOrderRequest().isBatteryValid()));
            } else {
                calcTotal();
                setMessage(Constants.HIDE_PROGRESS);
                liveDataActions(Constants.ORDER);
            }
        } else liveData.setValue(new Mutable(Constants.LOGIN_WARNING));
    }

    public void toConfirmServiceBoxAber() {
        if (userData != null) {
            setMessage(Constants.SHOW_PROGRESS);
            if (getServiceOrderRequest().isAberBoxValid() != null) {
                setMessage(Constants.HIDE_PROGRESS);
                liveData.setValue(new Mutable(Constants.ERROR_TOAST, getServiceOrderRequest().isAberBoxValid()));
            } else {
                getServiceOrderRequest().setCurrency("ريال");
                calcTotal();
                setMessage(Constants.HIDE_PROGRESS);
                liveDataActions(Constants.ORDER);
            }
        } else liveData.setValue(new Mutable(Constants.LOGIN_WARNING));
    }

    private void calcTotal() {
        getServiceOrderRequest().setTotal(getServiceOrderRequest().getTotalServices() + getServiceOrderRequest().getExtraTotalServices() + getServiceOrderRequest().getMovingPriceServices());
        // if eme if valid
        if (isEmergencyAccepted.get())
            getServiceOrderRequest().setTotal(getServiceOrderRequest().getTotal() + getServiceOrderRequest().getEmePriceServices());
    }

    public void liveDataActions(String action) {
        liveData.setValue(new Mutable(action));
    }

    @Bindable
    public ServiceDetailsAdapter getDetailsAdapter() {
        return this.detailsAdapter == null ? this.detailsAdapter = new ServiceDetailsAdapter() : this.detailsAdapter;
    }

    @Bindable
    public ExtraAdapter getExtraAdapter() {
        return this.extraAdapter == null ? this.extraAdapter = new ExtraAdapter() : this.extraAdapter;
    }

    @Bindable
    public OilPricesAdapter getOilPricesAdapter() {
        return this.oilPricesAdapter == null ? this.oilPricesAdapter = new OilPricesAdapter() : this.oilPricesAdapter;
    }

    @Bindable
    public ServiceDetails getDetails() {
        return details;
    }

    @Bindable
    public void setDetails(ServiceDetails details) {
        getServiceOrderRequest().setMovingPriceServices(details.getAdditions().getTransferService());
        if (getPassingObject().getId() == 7) // car washing
            getDetailsAdapter().update(details.getServices().get(0).getSubServicesList());
        else {
            if (details.getServices() != null)
                getDetailsAdapter().update(details.getServices());
        }
        if (details.getExtraList() != null) { // if extra services exists
            getExtraAdapter().update(details.getExtraList());
            notifyChange(BR.extraAdapter);
        }
        if (details.getOilPrices() != null) { // if prices list exists
            getOilPricesAdapter().update(details.getOilPrices());
            notifyChange(BR.oilPricesAdapter);
        }
        getServiceOrderRequest().setMainService(getPassingObject().getId());
        getServiceOrderRequest().setKiloCost(details.getAdditions().getKiloCost());
        getServiceOrderRequest().setTaxes(details.getAdditions().getTaxes());
        notifyChange(BR.detailsAdapter);
        notifyChange(BR.details);
        if (userData == null)
            liveData.setValue(new Mutable(Constants.LOGIN_WARNING));
        this.details = details;
    }

    public void onCheckedChange(CompoundButton button, boolean check) {
        getServiceOrderRequest().setDate(null);
        getServiceOrderRequest().setTime(null);
        getServiceOrderRequest().setEmerengcy(check);
        getServiceOrderRequest().setEmePriceServices(check ? getDetails().getAdditions().getEmergencyCost() : 0.0);
        isEmergencyAccepted.set(check);
    }

    @Bindable
    public ServiceOrderDetails getServiceOrderRequest() {
        return serviceOrderRequest;
    }

    public void setServiceOrderRequest(ServiceOrderDetails serviceOrderRequest) {
        this.serviceOrderRequest = serviceOrderRequest;
    }

    public ServicesRepository getPostRepository() {
        return postRepository;
    }


    protected void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        unSubscribeFromObservable();
    }

}
