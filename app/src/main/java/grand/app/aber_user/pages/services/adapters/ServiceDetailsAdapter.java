package grand.app.aber_user.pages.services.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import grand.app.aber_user.R;
import grand.app.aber_user.databinding.ItemServiceBinding;
import grand.app.aber_user.pages.services.models.ServicesItem;
import grand.app.aber_user.pages.services.viewModels.ItemServicesViewModel;

public class ServiceDetailsAdapter extends RecyclerView.Adapter<ServiceDetailsAdapter.MenuView> {
    List<ServicesItem> servicesItemList;
    private Context context;
    public int lastSelected = -1;
    public ServiceDetailsAdapter() {
        this.servicesItemList = new ArrayList<>();
    }
    public List<ServicesItem> getServicesItemList() {
        return servicesItemList;
    }

    @NonNull
    @Override
    public MenuView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_service,
                parent, false);
        this.context = parent.getContext();
        return new MenuView(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull final MenuView holder, @SuppressLint("RecyclerView") final int position) {
        ServicesItem menuModel = servicesItemList.get(position);
        ItemServicesViewModel itemMenuViewModel = new ItemServicesViewModel(menuModel);
        itemMenuViewModel.getLiveData().observe(((LifecycleOwner) context), o -> {
            this.lastSelected = menuModel.getId();
            notifyDataSetChanged();
        });
        if (menuModel.getMultiSelect() == 0) {
            menuModel.setChecked(menuModel.getId() == lastSelected);
        } else {
            if (lastSelected == menuModel.getId())
                menuModel.setChecked(!menuModel.isChecked());
        }
        holder.setViewModel(itemMenuViewModel);
    }

    public void update(@NotNull List<ServicesItem> dataList) {
        this.servicesItemList.clear();
        servicesItemList.addAll(dataList);
        notifyDataSetChanged();
    }

    @Override
    public void onViewAttachedToWindow(@NotNull MenuView holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NotNull MenuView holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    @Override
    public int getItemCount() {
        return servicesItemList.size();
    }

    public class MenuView extends RecyclerView.ViewHolder {
        public ItemServiceBinding itemMenuBinding;

        MenuView(View itemView) {
            super(itemView);
            bind();
        }

        void bind() {
            if (itemMenuBinding == null) {
                itemMenuBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemMenuBinding != null) {
                itemMenuBinding.unbind();
            }
        }

        void setViewModel(ItemServicesViewModel itemViewModels) {
            if (itemMenuBinding != null) {
                itemMenuBinding.setItemViewModel(itemViewModels);
            }
        }
    }
}
