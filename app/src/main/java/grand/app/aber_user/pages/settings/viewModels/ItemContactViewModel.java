package grand.app.aber_user.pages.settings.viewModels;


import androidx.databinding.Bindable;

import grand.app.aber_user.base.BaseViewModel;
import grand.app.aber_user.utils.Constants;

public class ItemContactViewModel extends BaseViewModel {
    public String contact;

    public ItemContactViewModel(String contact) {
        this.contact = contact;
    }

    @Bindable
    public String getContact() {
        return contact;
    }

    public void itemAction() {
        getLiveData().setValue(Constants.MENu);
    }

}
