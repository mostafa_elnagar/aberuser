package grand.app.aber_user.pages.parts.models;

import java.util.List;

import com.google.gson.annotations.SerializedName;

import grand.app.aber_user.model.PaginateMain;

public class Products extends PaginateMain {

    @SerializedName("data")
    private List<ProductsItem> data;

    public List<ProductsItem> getData() {
        return data;
    }

}