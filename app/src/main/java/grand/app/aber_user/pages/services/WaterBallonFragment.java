package grand.app.aber_user.pages.services;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;

import com.google.gson.Gson;

import javax.inject.Inject;

import grand.app.aber_user.BR;
import grand.app.aber_user.PassingObject;
import grand.app.aber_user.R;
import grand.app.aber_user.base.BaseFragment;
import grand.app.aber_user.base.IApplicationComponent;
import grand.app.aber_user.base.MyApplication;
import grand.app.aber_user.databinding.FragmentWaterBallonBinding;
import grand.app.aber_user.model.base.Mutable;
import grand.app.aber_user.pages.services.models.ServiceDetailsResponse;
import grand.app.aber_user.pages.services.viewModels.ServicesViewModels;
import grand.app.aber_user.utils.Constants;
import grand.app.aber_user.utils.PopUp.PopUpMenuHelper;
import grand.app.aber_user.utils.helper.MovementHelper;


public class WaterBallonFragment extends BaseFragment {
    @Inject
    ServicesViewModels viewModel;
    FragmentWaterBallonBinding binding;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_water_ballon, container, false);
        IApplicationComponent component = ((MyApplication) requireActivity().getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setViewModel(viewModel);
        binding.searchLocation.setIconTint(null);
        binding.picTime.setIconTint(null);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            String passingObject = bundle.getString(Constants.BUNDLE);
            viewModel.setPassingObject(new Gson().fromJson(passingObject, PassingObject.class));
            viewModel.getServiceDetails();
        }
        setEvent();
        return binding.getRoot();
    }

    private void setEvent() {
        viewModel.liveData.observe(requireActivity(), (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            viewModel.setMessage(mutable.message.equals(Constants.HIDE_PROGRESS) ? mutable.message : "");
            switch (((Mutable) o).message) {
                case Constants.SERVICE_DETAILS:
                    viewModel.setDetails(((ServiceDetailsResponse) (mutable).object).getData());
                    break;
                case Constants.LITER:
                    showLitre();
                    break;
                case Constants.GALLON:
                    showGallon();
                    break;
                case Constants.TINKER_SERVICE_TYPE:
                    showServiceType();
                    break;
                case Constants.SEARCH_LOCATION:
                    MovementHelper.startMapActivityForResultWithBundle(requireActivity(), new PassingObject(), getResources().getString(R.string.up_location), Constants.LOCATION_REQUEST);
                    break;
                case Constants.FROM_SEARCH_LOCATION:
                    MovementHelper.startMapActivityForResultWithBundle(requireActivity(), new PassingObject(), getResources().getString(R.string.down_location), Constants.FROM_LOCATION_REQUEST);
                    break;
                case Constants.DELIVERY_TIME:
                    MovementHelper.startActivityForResultWithBundle(requireActivity(), new PassingObject(), null, ChooseServiceTimeFragment.class.getName(), Constants.DELIVERY_TIME_REQUEST);
                    break;
                case Constants.ORDER:
                    MovementHelper.startActivityForResultWithBundle(requireActivity(), new PassingObject(viewModel.getServiceOrderRequest()), null, FragmentConfirmOrder.class.getName(), Constants.ORDER_REQUEST);
                    break;

            }
        });
    }

    private void showLitre() {
        PopUpMenuHelper.showCarTypePopUp(requireActivity(), binding.inputLitre, viewModel.getDetails().getLiterGallonList().get(0).getValuesList()).setOnMenuItemClickListener(item -> {
            viewModel.getServiceOrderRequest().setLitre(viewModel.getDetails().getLiterGallonList().get(0).getValuesList().get(item.getItemId()).getName());
            viewModel.getServiceOrderRequest().setLitreId(viewModel.getDetails().getLiterGallonList().get(0).getValuesList().get(item.getItemId()).getId());
            viewModel.notifyChange(BR.serviceOrderRequest);
            return false;
        });
    }

    private void showGallon() {
        PopUpMenuHelper.showCarTypePopUp(requireActivity(), binding.inputGallon, viewModel.getDetails().getLiterGallonList().get(1).getValuesList()).setOnMenuItemClickListener(item -> {
            viewModel.getServiceOrderRequest().setGallon(viewModel.getDetails().getLiterGallonList().get(1).getValuesList().get(item.getItemId()).getName());
            viewModel.getServiceOrderRequest().setGallonId(viewModel.getDetails().getLiterGallonList().get(1).getValuesList().get(item.getItemId()).getId());
            viewModel.getServiceOrderRequest().setTotalServices(Double.parseDouble(viewModel.getDetails().getLiterGallonList().get(1).getValuesList().get(item.getItemId()).getPrice()));
            viewModel.getServiceOrderRequest().setCurrency(viewModel.getDetails().getLiterGallonList().get(1).getValuesList().get(item.getItemId()).getCurrency());
            viewModel.notifyChange(BR.serviceOrderRequest);
            return false;
        });
    }

    private void showServiceType() {
        PopUpMenuHelper.showCarTypePopUp(requireActivity(), binding.inputLitreService, viewModel.getDetails().getLiterGallonList().get(2).getValuesList()).setOnMenuItemClickListener(item -> {
            viewModel.getServiceOrderRequest().setTinkerServiceName(viewModel.getDetails().getLiterGallonList().get(2).getValuesList().get(item.getItemId()).getName());
            viewModel.getServiceOrderRequest().setTinkerServiceId(viewModel.getDetails().getLiterGallonList().get(2).getValuesList().get(item.getItemId()).getId());
            viewModel.notifyChange(BR.serviceOrderRequest);
            return false;
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        viewModel.getPostRepository().setLiveData(viewModel.liveData);
    }

    @Override
    public void launchActivityResult(int request, int resultCode, Intent result) {
        super.launchActivityResult(request, resultCode, result);
        if (request == Constants.FROM_LOCATION_REQUEST) {
            viewModel.getServiceOrderRequest().setFromLatitude(result.getDoubleExtra(Constants.LAT, 0.0));
            viewModel.getServiceOrderRequest().setFromLongitude(result.getDoubleExtra(Constants.LNG, 0.0));
            viewModel.getServiceOrderRequest().setFromAddress(result.getStringExtra(Constants.ADDRESS));
        } else if (request == Constants.LOCATION_REQUEST) {
            viewModel.getServiceOrderRequest().setToLatitude(result.getDoubleExtra(Constants.LAT, 0.0));
            viewModel.getServiceOrderRequest().setToLongitude(result.getDoubleExtra(Constants.LNG, 0.0));
            viewModel.getServiceOrderRequest().setToAddress(result.getStringExtra(Constants.ADDRESS));
        } else if (request == Constants.DELIVERY_TIME_REQUEST) {
            Bundle bundle = result.getBundleExtra(Constants.BUNDLE);
            if (bundle != null && bundle.containsKey(Constants.BUNDLE)) {
                PassingObject passingObject = (PassingObject) bundle.getSerializable(Constants.BUNDLE);
                viewModel.getServiceOrderRequest().setTime(passingObject.getObject2());
                viewModel.getServiceOrderRequest().setDate(passingObject.getObject());
            }
        } else if (request == Constants.ORDER_REQUEST) {
            finishActivity();
        }
    }
}
