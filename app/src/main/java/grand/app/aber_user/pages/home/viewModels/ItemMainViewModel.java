package grand.app.aber_user.pages.home.viewModels;


import androidx.databinding.Bindable;

import grand.app.aber_user.base.BaseViewModel;
import grand.app.aber_user.pages.home.models.HomeServices;

public class ItemMainViewModel extends BaseViewModel {
    public HomeServices services;

    public ItemMainViewModel(HomeServices services) {
        this.services = services;
    }

    @Bindable
    public HomeServices getServices() {
        return services;
    }

    public void itemAction(String action) {
        getLiveData().setValue(action);
    }

}
