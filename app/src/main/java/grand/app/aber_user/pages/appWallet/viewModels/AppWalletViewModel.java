package grand.app.aber_user.pages.appWallet.viewModels;

import androidx.databinding.Bindable;
import androidx.lifecycle.MutableLiveData;

import javax.inject.Inject;

import grand.app.aber_user.BR;
import grand.app.aber_user.base.BaseViewModel;
import grand.app.aber_user.model.base.Mutable;
import grand.app.aber_user.pages.appWallet.adapters.AppWalletAdapter;
import grand.app.aber_user.pages.appWallet.models.HistoryWalletData;
import grand.app.aber_user.pages.appWallet.models.RaiseWalletRequest;
import grand.app.aber_user.repository.SettingsRepository;
import grand.app.aber_user.utils.Constants;
import io.reactivex.disposables.CompositeDisposable;

public class AppWalletViewModel extends BaseViewModel {

    public MutableLiveData<Mutable> liveData;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    @Inject
    SettingsRepository settingsRepository;
    RaiseWalletRequest raiseWalletRequest;
    AppWalletAdapter appWalletAdapter;
    HistoryWalletData historyWalletData;

    @Inject
    public AppWalletViewModel(SettingsRepository settingsRepository) {
        this.liveData = new MutableLiveData<>();
        this.settingsRepository = settingsRepository;
        settingsRepository.setLiveData(liveData);
    }

    public void putInWallet() {
//        if (!TextUtils.isEmpty(getRaiseWalletRequest().getAmount()))
//            compositeDisposable.add(settingsRepository.raiseWallet(getRaiseWalletRequest()));
//        liveData.setValue(new Mutable(Constants.EMPTY_DRIVER));
    }

    public void walletHistory() {
        compositeDisposable.add(settingsRepository.walletHistory());
    }

    public SettingsRepository getSettingsRepository() {
        return settingsRepository;
    }

    @Bindable
    public AppWalletAdapter getAppWalletAdapter() {
        return this.appWalletAdapter == null ? this.appWalletAdapter = new AppWalletAdapter() : this.appWalletAdapter;
    }

    protected void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        unSubscribeFromObservable();
    }

    @Bindable
    public HistoryWalletData getHistoryWalletData() {
        return this.historyWalletData == null ? this.historyWalletData = new HistoryWalletData() : this.historyWalletData;
    }

    @Bindable
    public void setHistoryWalletData(HistoryWalletData historyWalletData) {
        getAppWalletAdapter().update(historyWalletData.getUserWalletRecharges());
        notifyChange(BR.appWalletAdapter);
        notifyChange(BR.historyWalletData);
        this.historyWalletData = historyWalletData;
    }

    public RaiseWalletRequest getRaiseWalletRequest() {
        return this.raiseWalletRequest == null ? this.raiseWalletRequest = new RaiseWalletRequest() : this.raiseWalletRequest;
    }

    @Bindable
    public void setRaiseWalletRequest(RaiseWalletRequest raiseWalletRequest) {
        notifyChange(BR.raiseWalletRequest);
        this.raiseWalletRequest = raiseWalletRequest;
    }
}
