package grand.app.aber_user.pages.settings.viewModels;

import android.text.TextUtils;
import android.widget.RadioGroup;

import androidx.databinding.Bindable;
import androidx.databinding.library.baseAdapters.BR;
import androidx.lifecycle.MutableLiveData;

import javax.inject.Inject;

import grand.app.aber_user.R;
import grand.app.aber_user.base.BaseViewModel;
import grand.app.aber_user.model.base.Mutable;
import grand.app.aber_user.pages.settings.adapters.ContactsAdapter;
import grand.app.aber_user.pages.settings.adapters.SocialAdapter;
import grand.app.aber_user.pages.settings.models.AboutData;
import grand.app.aber_user.pages.settings.models.ContactUsRequest;
import grand.app.aber_user.repository.SettingsRepository;
import grand.app.aber_user.utils.Constants;
import io.reactivex.disposables.CompositeDisposable;

public class SettingsViewModel extends BaseViewModel {

    ContactUsRequest contactUsRequest;
    public MutableLiveData<Mutable> liveData;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    @Inject
    SettingsRepository repository;
    AboutData aboutData;
    SocialAdapter socialAdapter;
    ContactsAdapter contactsAdapter;

    @Inject
    public SettingsViewModel(SettingsRepository repository) {
        aboutData = new AboutData();
        contactUsRequest = new ContactUsRequest();
        this.repository = repository;
        this.liveData = new MutableLiveData<>();
        repository.setLiveData(liveData);
    }

    public void sendContact() {
        if (getContactUsRequest().isValid()) {
            setMessage(Constants.SHOW_PROGRESS);
            if (!TextUtils.isEmpty(getPassingObject().getObject()) && getPassingObject().getObject().equals(Constants.CONTACT))
                compositeDisposable.add(repository.sendContact(getContactUsRequest()));
            else
                compositeDisposable.add(repository.sendSuggest(getContactUsRequest()));
        } else
            liveData.setValue(new Mutable(Constants.ERROR_TOAST));
    }

    public void about() {
        compositeDisposable.add(repository.about());
    }

    public void socialMedia() {
        compositeDisposable.add(repository.getSocial());
    }

    public void getContact() {
        compositeDisposable.add(repository.getContact());
    }

    public void terms() {
        if (getPassingObject().getObject().equals(Constants.TERMS))
            compositeDisposable.add(repository.terms());
        else
            compositeDisposable.add(repository.privacy());
    }

    public void onLangChange(RadioGroup radioGroup, int id) {
        if (id == R.id.arabic) {
            lang = "ar";
        } else if (id == R.id.english)
            lang = "en";
        else
            lang = "ur";
    }

    public void changeLang() {
        liveData.setValue(new Mutable(Constants.LANGUAGE));
    }


    @Bindable
    public SocialAdapter getSocialAdapter() {
        return this.socialAdapter == null ? this.socialAdapter = new SocialAdapter() : this.socialAdapter;
    }

    @Bindable
    public ContactsAdapter getContactsAdapter() {
        return this.contactsAdapter == null ? this.contactsAdapter = new ContactsAdapter() : this.contactsAdapter;
    }

    @Bindable
    public AboutData getAboutData() {
        return aboutData;
    }

    @Bindable
    public void setAboutData(AboutData aboutData) {
        notifyChange(BR.aboutData);
        this.aboutData = aboutData;
    }

    protected void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        unSubscribeFromObservable();
    }

    public SettingsRepository getRepository() {
        return repository;
    }


    public ContactUsRequest getContactUsRequest() {
        return contactUsRequest;
    }

}
