package grand.app.aber_user.pages.reviews.itemViewModels;

import androidx.databinding.Bindable;

import grand.app.aber_user.base.BaseViewModel;
import grand.app.aber_user.pages.reviews.models.RatesItem;

public class ItemClientReviewsViewModel extends BaseViewModel {
    public RatesItem ratesItem;

    public ItemClientReviewsViewModel(RatesItem ratesItem) {
        this.ratesItem = ratesItem;
    }

    @Bindable
    public RatesItem getRatesItem() {
        return ratesItem;
    }


}
