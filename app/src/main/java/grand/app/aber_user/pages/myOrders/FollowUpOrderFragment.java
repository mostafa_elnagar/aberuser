package grand.app.aber_user.pages.myOrders;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;

import com.directions.route.AbstractRouting;
import com.directions.route.Route;
import com.directions.route.RouteException;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.GeoQuery;
import com.firebase.geofire.GeoQueryDataEventListener;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import grand.app.aber_user.PassingObject;
import grand.app.aber_user.R;
import grand.app.aber_user.base.BaseFragment;
import grand.app.aber_user.base.IApplicationComponent;
import grand.app.aber_user.base.MyApplication;
import grand.app.aber_user.databinding.FragmentFollowUpOrdersBinding;
import grand.app.aber_user.pages.myOrders.models.orderServices.OrderDetailsMain;
import grand.app.aber_user.pages.myOrders.viewModels.MyOrdersServicesViewModels;
import grand.app.aber_user.utils.Constants;
import grand.app.aber_user.utils.helper.AppHelper;
import grand.app.aber_user.utils.locations.MapConfig;
import grand.app.aber_user.utils.resources.ResourceManager;


public class FollowUpOrderFragment extends BaseFragment implements OnMapReadyCallback, RoutingListener {
    FragmentFollowUpOrdersBinding binding;
    @Inject
    MyOrdersServicesViewModels viewModel;
    GoogleMap mMap;
    List<Polyline> polyline = null;
    Polyline polylineOld = null;
    LatLng start = null, end = null;
    Marker markerDriver;
    DatabaseReference ref;
    GeoQuery geoQuery;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_follow_up_orders, container, false);
        IApplicationComponent component = ((MyApplication) requireActivity().getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setViewModel(viewModel);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            String passingObject = bundle.getString(Constants.BUNDLE);
            viewModel.setPassingObject(new Gson().fromJson(passingObject, PassingObject.class));
            viewModel.setMyOrderDetails(new Gson().fromJson(String.valueOf(viewModel.getPassingObject().getObjectClass()), OrderDetailsMain.class));
        }
        init(savedInstanceState);
        return binding.getRoot();
    }

    private static final String TAG = "FollowUpOrderFragment";

    // function to find Routes.
    private void findRoutes() {
        // new Trip
        Routing routing;
        try {
            routing = new Routing.Builder()
                    .travelMode(AbstractRouting.TravelMode.DRIVING)
                    .withListener(this)
                    .alternativeRoutes(true)
                    .waypoints(start, end)
                    .key(getString(R.string.google_map))  //also define your api key here.
                    .build();
            routing.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Routing call back functions.
    @Override
    public void onRoutingFailure(RouteException e) {
        Log.e(TAG, "onRoutingFailure: " + e.getMessage());
        e.printStackTrace();
        findRoutes();
    }

    @Override
    public void onRoutingStart() {
//        showMessage(getResources().getString(R.string.finding_route), 1, 0);

    }

    @Override
    public void onRoutingSuccess(ArrayList<Route> route, int shortestRouteIndex) {
        if (polyline != null) {
            polyline.clear();
        }
        PolylineOptions polyOptions = new PolylineOptions();
        polyline = new ArrayList<>();
        //add route(s) to the map using polyline
        for (int i = 0; i < route.size(); i++) {
            if (i == shortestRouteIndex) {
                polyOptions.color(getResources().getColor(R.color.black, null));
                polyOptions.width(7);
                polyOptions.addAll(route.get(i).getPoints());
                Polyline polyline = mMap.addPolyline(polyOptions);
                if (polylineOld != null) {
                    polylineOld.remove();
                } else
                    polylineOld = polyline;
                this.polyline.add(polyline);
            }
        }

        MapConfig mapConfig = new MapConfig(requireActivity(), mMap);
        ArrayList<LatLng> latLngs = new ArrayList<>();
        latLngs.add(start);
        latLngs.add(end);
        mapConfig.moveCamera(latLngs);
        addUserMarker(start, ResourceManager.getDrawable(R.drawable.ic_order_location), false);
        addUserMarker(end, ResourceManager.getDrawable(R.drawable.ic_user_location), true);
    }

    @Override
    public void onRoutingCancelled() {
        findRoutes();
    }

    public void addUserMarker(LatLng position, Drawable drawable, boolean myLocation) {
        MarkerOptions markerOptionsFirst = new MarkerOptions();
        markerOptionsFirst.draggable(false);
        markerOptionsFirst.position(position);
        markerOptionsFirst.anchor(1f, 1f);
        markerOptionsFirst.icon(BitmapDescriptorFactory.fromBitmap(Objects.requireNonNull(AppHelper.resizeVectorIcon(drawable, drawable.getMinimumWidth(), drawable.getMinimumHeight()))));
        if (markerDriver != null)
            markerDriver.remove();
        if (myLocation)
            markerDriver = mMap.addMarker(markerOptionsFirst);
        else
            mMap.addMarker(markerOptionsFirst);
    }

    public void getDriverLastLocation() {
        ref = FirebaseDatabase.getInstance().getReference("Drivers");
        GeoFire geoFire = new GeoFire(ref);
        geoQuery = geoFire.queryAtLocation(new GeoLocation(start.latitude, start.longitude), 20000);
        geoQuery.addGeoQueryDataEventListener(new GeoQueryDataEventListener() {

            @Override
            public void onDataEntered(DataSnapshot dataSnapshot, GeoLocation location) {
                if (location != null) {
                    end = new LatLng(location.latitude, location.longitude);
                } else {
                    end = new LatLng(Double.parseDouble(viewModel.getMyOrderDetails().getOrders().getProvider().getLatitude()), Double.parseDouble(viewModel.getMyOrderDetails().getOrders().getProvider().getLongitude()));
                }
                binding.loading.setVisibility(View.GONE);
                findRoutes();
            }

            @Override
            public void onDataExited(DataSnapshot dataSnapshot) {
                // ...
            }

            @Override
            public void onDataMoved(DataSnapshot dataSnapshot, GeoLocation location) {
                // ...
            }

            @Override
            public void onDataChanged(DataSnapshot dataSnapshot, GeoLocation location) {
                if (location != null) {
                    end = new LatLng(location.latitude, location.longitude);
                    addUserMarker(end, ResourceManager.getDrawable(R.drawable.ic_user_location), true);
                }
            }

            @Override
            public void onGeoQueryReady() {

            }

            @Override
            public void onGeoQueryError(DatabaseError error) {
                // ...
            }

        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        binding.followImgMap.getMapAsync(this);
        binding.followImgMap.onResume();
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void init(Bundle savedInstanceState) {
        binding.followImgMap.onCreate(savedInstanceState);
        if (ActivityCompat.checkSelfPermission(requireActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(requireActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(requireActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1001);
        } else {
            binding.followImgMap.getMapAsync(this);
        }
    }

    @Override
    public void onMapReady(@NonNull GoogleMap googleMap) {
        mMap = googleMap;
        start = new LatLng(Double.parseDouble(viewModel.getMyOrderDetails().getOrders().getLatitude()), Double.parseDouble(viewModel.getMyOrderDetails().getOrders().getLongitude()));
        getDriverLastLocation();
    }

    @Override
    public void onResume() {
        binding.followImgMap.onResume();
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        binding.followImgMap.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        binding.followImgMap.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        binding.followImgMap.onLowMemory();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        geoQuery.removeAllListeners();
    }
}
