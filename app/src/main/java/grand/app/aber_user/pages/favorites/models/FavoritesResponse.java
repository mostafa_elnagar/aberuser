package grand.app.aber_user.pages.favorites.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import grand.app.aber_user.model.base.StatusMessage;
import grand.app.aber_user.pages.parts.models.ProductsItem;

public class FavoritesResponse extends StatusMessage {
    @SerializedName("data")
    private List<ProductsItem> productsItemList;

    public List<ProductsItem> getProductsItemList() {
        return productsItemList;
    }
}
