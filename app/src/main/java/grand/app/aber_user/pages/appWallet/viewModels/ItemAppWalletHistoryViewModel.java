package grand.app.aber_user.pages.appWallet.viewModels;

import androidx.databinding.Bindable;

import grand.app.aber_user.base.BaseViewModel;
import grand.app.aber_user.pages.appWallet.models.WalletHistoryItem;

public class ItemAppWalletHistoryViewModel extends BaseViewModel {
    public WalletHistoryItem walletHistoryItem;
    public String title;

    public ItemAppWalletHistoryViewModel(WalletHistoryItem walletHistoryItem) {
        this.walletHistoryItem = walletHistoryItem;
//        title = ResourceManager.getString(R.string.wallet_history_1).concat(" ").concat(walletHistoryItem.getCancelFess()).concat(" ").concat(currency).concat(" ").concat(ResourceManager.getString(R.string.wallet_history_2));
    }

    @Bindable
    public WalletHistoryItem getWalletHistoryItem() {
        return walletHistoryItem;
    }
}
