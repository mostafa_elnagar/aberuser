// Generated by data binding compiler. Do not edit!
package grand.app.aber_user.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import grand.app.aber_user.R;
import grand.app.aber_user.customViews.views.CustomTextViewMedium;
import grand.app.aber_user.pages.parts.viewModels.ItemProductsViewModel;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class ItemFavoriteBinding extends ViewDataBinding {
  @NonNull
  public final AppCompatImageView icClosePage;

  @NonNull
  public final AppCompatImageView icPartsImage;

  @NonNull
  public final CustomTextViewMedium tvPartsName;

  @NonNull
  public final CustomTextViewMedium tvPartsPrice;

  @Bindable
  protected ItemProductsViewModel mItemViewModel;

  protected ItemFavoriteBinding(Object _bindingComponent, View _root, int _localFieldCount,
      AppCompatImageView icClosePage, AppCompatImageView icPartsImage,
      CustomTextViewMedium tvPartsName, CustomTextViewMedium tvPartsPrice) {
    super(_bindingComponent, _root, _localFieldCount);
    this.icClosePage = icClosePage;
    this.icPartsImage = icPartsImage;
    this.tvPartsName = tvPartsName;
    this.tvPartsPrice = tvPartsPrice;
  }

  public abstract void setItemViewModel(@Nullable ItemProductsViewModel itemViewModel);

  @Nullable
  public ItemProductsViewModel getItemViewModel() {
    return mItemViewModel;
  }

  @NonNull
  public static ItemFavoriteBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.item_favorite, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static ItemFavoriteBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<ItemFavoriteBinding>inflateInternal(inflater, R.layout.item_favorite, root, attachToRoot, component);
  }

  @NonNull
  public static ItemFavoriteBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.item_favorite, null, false, component)
   */
  @NonNull
  @Deprecated
  public static ItemFavoriteBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<ItemFavoriteBinding>inflateInternal(inflater, R.layout.item_favorite, null, false, component);
  }

  public static ItemFavoriteBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static ItemFavoriteBinding bind(@NonNull View view, @Nullable Object component) {
    return (ItemFavoriteBinding)bind(component, view, R.layout.item_favorite);
  }
}
