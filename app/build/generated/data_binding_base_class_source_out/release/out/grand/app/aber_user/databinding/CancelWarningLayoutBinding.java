// Generated by data binding compiler. Do not edit!
package grand.app.aber_user.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import com.google.android.material.button.MaterialButton;
import grand.app.aber_user.R;
import grand.app.aber_user.customViews.views.CustomTextViewMedium;
import grand.app.aber_user.pages.myOrders.viewModels.MyOrdersServicesViewModels;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class CancelWarningLayoutBinding extends ViewDataBinding {
  @NonNull
  public final MaterialButton agree;

  @NonNull
  public final MaterialButton decline;

  @NonNull
  public final AppCompatImageView icExitDialog;

  @NonNull
  public final CustomTextViewMedium logoutTxt;

  @NonNull
  public final CustomTextViewMedium logoutTxt1;

  @Bindable
  protected MyOrdersServicesViewModels mViewModel;

  protected CancelWarningLayoutBinding(Object _bindingComponent, View _root, int _localFieldCount,
      MaterialButton agree, MaterialButton decline, AppCompatImageView icExitDialog,
      CustomTextViewMedium logoutTxt, CustomTextViewMedium logoutTxt1) {
    super(_bindingComponent, _root, _localFieldCount);
    this.agree = agree;
    this.decline = decline;
    this.icExitDialog = icExitDialog;
    this.logoutTxt = logoutTxt;
    this.logoutTxt1 = logoutTxt1;
  }

  public abstract void setViewModel(@Nullable MyOrdersServicesViewModels viewModel);

  @Nullable
  public MyOrdersServicesViewModels getViewModel() {
    return mViewModel;
  }

  @NonNull
  public static CancelWarningLayoutBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.cancel_warning_layout, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static CancelWarningLayoutBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<CancelWarningLayoutBinding>inflateInternal(inflater, R.layout.cancel_warning_layout, root, attachToRoot, component);
  }

  @NonNull
  public static CancelWarningLayoutBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.cancel_warning_layout, null, false, component)
   */
  @NonNull
  @Deprecated
  public static CancelWarningLayoutBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<CancelWarningLayoutBinding>inflateInternal(inflater, R.layout.cancel_warning_layout, null, false, component);
  }

  public static CancelWarningLayoutBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static CancelWarningLayoutBinding bind(@NonNull View view, @Nullable Object component) {
    return (CancelWarningLayoutBinding)bind(component, view, R.layout.cancel_warning_layout);
  }
}
