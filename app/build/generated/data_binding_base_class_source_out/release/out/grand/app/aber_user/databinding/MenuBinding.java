// Generated by data binding compiler. Do not edit!
package grand.app.aber_user.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.aber_user.R;
import grand.app.aber_user.customViews.grandDialog.GrandImageDialog;
import grand.app.aber_user.customViews.views.CustomTextViewMedium;
import grand.app.aber_user.customViews.views.MenuViewModel;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class MenuBinding extends ViewDataBinding {
  @NonNull
  public final GrandImageDialog grandLogo;

  @NonNull
  public final RecyclerView rcMenuSocial;

  @NonNull
  public final RecyclerView rcServices;

  @NonNull
  public final CustomTextViewMedium tvAbout;

  @NonNull
  public final CustomTextViewMedium tvAccount;

  @NonNull
  public final CustomTextViewMedium tvCart;

  @NonNull
  public final CustomTextViewMedium tvContact;

  @NonNull
  public final CustomTextViewMedium tvCountry;

  @NonNull
  public final CustomTextViewMedium tvFavorite;

  @NonNull
  public final CustomTextViewMedium tvHome;

  @NonNull
  public final CustomTextViewMedium tvLang;

  @NonNull
  public final CustomTextViewMedium tvLogout;

  @NonNull
  public final CustomTextViewMedium tvMyOrders;

  @NonNull
  public final CustomTextViewMedium tvMyServices;

  @NonNull
  public final CustomTextViewMedium tvProvider;

  @NonNull
  public final CustomTextViewMedium tvRate;

  @NonNull
  public final CustomTextViewMedium tvServices;

  @NonNull
  public final CustomTextViewMedium tvShare;

  @NonNull
  public final CustomTextViewMedium tvSocial;

  @NonNull
  public final CustomTextViewMedium tvSuggest;

  @NonNull
  public final CustomTextViewMedium tvTerms;

  @NonNull
  public final View v1;

  @NonNull
  public final View v10;

  @NonNull
  public final View v12;

  @NonNull
  public final View v13;

  @NonNull
  public final View v14;

  @NonNull
  public final View v15;

  @NonNull
  public final View v16;

  @NonNull
  public final View v17;

  @NonNull
  public final View v18;

  @NonNull
  public final View v2;

  @NonNull
  public final View v21;

  @NonNull
  public final View v3;

  @NonNull
  public final View v4;

  @NonNull
  public final View v5;

  @NonNull
  public final View v6;

  @NonNull
  public final View v7;

  @NonNull
  public final View v8;

  @NonNull
  public final View v9;

  @Bindable
  protected MenuViewModel mMenuViewModel;

  protected MenuBinding(Object _bindingComponent, View _root, int _localFieldCount,
      GrandImageDialog grandLogo, RecyclerView rcMenuSocial, RecyclerView rcServices,
      CustomTextViewMedium tvAbout, CustomTextViewMedium tvAccount, CustomTextViewMedium tvCart,
      CustomTextViewMedium tvContact, CustomTextViewMedium tvCountry,
      CustomTextViewMedium tvFavorite, CustomTextViewMedium tvHome, CustomTextViewMedium tvLang,
      CustomTextViewMedium tvLogout, CustomTextViewMedium tvMyOrders,
      CustomTextViewMedium tvMyServices, CustomTextViewMedium tvProvider,
      CustomTextViewMedium tvRate, CustomTextViewMedium tvServices, CustomTextViewMedium tvShare,
      CustomTextViewMedium tvSocial, CustomTextViewMedium tvSuggest, CustomTextViewMedium tvTerms,
      View v1, View v10, View v12, View v13, View v14, View v15, View v16, View v17, View v18,
      View v2, View v21, View v3, View v4, View v5, View v6, View v7, View v8, View v9) {
    super(_bindingComponent, _root, _localFieldCount);
    this.grandLogo = grandLogo;
    this.rcMenuSocial = rcMenuSocial;
    this.rcServices = rcServices;
    this.tvAbout = tvAbout;
    this.tvAccount = tvAccount;
    this.tvCart = tvCart;
    this.tvContact = tvContact;
    this.tvCountry = tvCountry;
    this.tvFavorite = tvFavorite;
    this.tvHome = tvHome;
    this.tvLang = tvLang;
    this.tvLogout = tvLogout;
    this.tvMyOrders = tvMyOrders;
    this.tvMyServices = tvMyServices;
    this.tvProvider = tvProvider;
    this.tvRate = tvRate;
    this.tvServices = tvServices;
    this.tvShare = tvShare;
    this.tvSocial = tvSocial;
    this.tvSuggest = tvSuggest;
    this.tvTerms = tvTerms;
    this.v1 = v1;
    this.v10 = v10;
    this.v12 = v12;
    this.v13 = v13;
    this.v14 = v14;
    this.v15 = v15;
    this.v16 = v16;
    this.v17 = v17;
    this.v18 = v18;
    this.v2 = v2;
    this.v21 = v21;
    this.v3 = v3;
    this.v4 = v4;
    this.v5 = v5;
    this.v6 = v6;
    this.v7 = v7;
    this.v8 = v8;
    this.v9 = v9;
  }

  public abstract void setMenuViewModel(@Nullable MenuViewModel menuViewModel);

  @Nullable
  public MenuViewModel getMenuViewModel() {
    return mMenuViewModel;
  }

  @NonNull
  public static MenuBinding inflate(@NonNull LayoutInflater inflater, @Nullable ViewGroup root,
      boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.menu, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static MenuBinding inflate(@NonNull LayoutInflater inflater, @Nullable ViewGroup root,
      boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<MenuBinding>inflateInternal(inflater, R.layout.menu, root, attachToRoot, component);
  }

  @NonNull
  public static MenuBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.menu, null, false, component)
   */
  @NonNull
  @Deprecated
  public static MenuBinding inflate(@NonNull LayoutInflater inflater, @Nullable Object component) {
    return ViewDataBinding.<MenuBinding>inflateInternal(inflater, R.layout.menu, null, false, component);
  }

  public static MenuBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static MenuBinding bind(@NonNull View view, @Nullable Object component) {
    return (MenuBinding)bind(component, view, R.layout.menu);
  }
}
