// Generated by data binding compiler. Do not edit!
package grand.app.aber_user.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.airbnb.lottie.LottieAnimationView;
import com.google.android.material.progressindicator.CircularProgressIndicator;
import grand.app.aber_user.R;
import grand.app.aber_user.pages.reviews.viewModels.ReviewsViewModel;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class FragmentReviewsBinding extends ViewDataBinding {
  @NonNull
  public final LottieAnimationView animationView;

  @NonNull
  public final CircularProgressIndicator progress;

  @NonNull
  public final RecyclerView rcReviews;

  @Bindable
  protected ReviewsViewModel mViewmodel;

  protected FragmentReviewsBinding(Object _bindingComponent, View _root, int _localFieldCount,
      LottieAnimationView animationView, CircularProgressIndicator progress,
      RecyclerView rcReviews) {
    super(_bindingComponent, _root, _localFieldCount);
    this.animationView = animationView;
    this.progress = progress;
    this.rcReviews = rcReviews;
  }

  public abstract void setViewmodel(@Nullable ReviewsViewModel viewmodel);

  @Nullable
  public ReviewsViewModel getViewmodel() {
    return mViewmodel;
  }

  @NonNull
  public static FragmentReviewsBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_reviews, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static FragmentReviewsBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<FragmentReviewsBinding>inflateInternal(inflater, R.layout.fragment_reviews, root, attachToRoot, component);
  }

  @NonNull
  public static FragmentReviewsBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_reviews, null, false, component)
   */
  @NonNull
  @Deprecated
  public static FragmentReviewsBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<FragmentReviewsBinding>inflateInternal(inflater, R.layout.fragment_reviews, null, false, component);
  }

  public static FragmentReviewsBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static FragmentReviewsBinding bind(@NonNull View view, @Nullable Object component) {
    return (FragmentReviewsBinding)bind(component, view, R.layout.fragment_reviews);
  }
}
