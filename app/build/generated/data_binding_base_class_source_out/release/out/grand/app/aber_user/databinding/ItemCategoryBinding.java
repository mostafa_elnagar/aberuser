// Generated by data binding compiler. Do not edit!
package grand.app.aber_user.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import grand.app.aber_user.R;
import grand.app.aber_user.customViews.views.CustomTextViewMedium;
import grand.app.aber_user.customViews.views.CustomTextViewRegular;
import grand.app.aber_user.pages.home.viewModels.ItemMainViewModel;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class ItemCategoryBinding extends ViewDataBinding {
  @NonNull
  public final AppCompatImageView imgHomeItem;

  @NonNull
  public final CustomTextViewRegular tvDesc;

  @NonNull
  public final CustomTextViewMedium tvHomeItem;

  @Bindable
  protected ItemMainViewModel mItemPostViewModel;

  protected ItemCategoryBinding(Object _bindingComponent, View _root, int _localFieldCount,
      AppCompatImageView imgHomeItem, CustomTextViewRegular tvDesc,
      CustomTextViewMedium tvHomeItem) {
    super(_bindingComponent, _root, _localFieldCount);
    this.imgHomeItem = imgHomeItem;
    this.tvDesc = tvDesc;
    this.tvHomeItem = tvHomeItem;
  }

  public abstract void setItemPostViewModel(@Nullable ItemMainViewModel itemPostViewModel);

  @Nullable
  public ItemMainViewModel getItemPostViewModel() {
    return mItemPostViewModel;
  }

  @NonNull
  public static ItemCategoryBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.item_category, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static ItemCategoryBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<ItemCategoryBinding>inflateInternal(inflater, R.layout.item_category, root, attachToRoot, component);
  }

  @NonNull
  public static ItemCategoryBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.item_category, null, false, component)
   */
  @NonNull
  @Deprecated
  public static ItemCategoryBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<ItemCategoryBinding>inflateInternal(inflater, R.layout.item_category, null, false, component);
  }

  public static ItemCategoryBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static ItemCategoryBinding bind(@NonNull View view, @Nullable Object component) {
    return (ItemCategoryBinding)bind(component, view, R.layout.item_category);
  }
}
