package grand.app.aber_user.databinding;
import grand.app.aber_user.R;
import grand.app.aber_user.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentMyOrderDetailsBindingImpl extends FragmentMyOrderDetailsBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.card_info, 20);
        sViewsWithIds.put(R.id.tv_services_name, 21);
        sViewsWithIds.put(R.id.v_services_name, 22);
        sViewsWithIds.put(R.id.tv_service_location, 23);
        sViewsWithIds.put(R.id.v_service_location, 24);
        sViewsWithIds.put(R.id.tv_service_time, 25);
        sViewsWithIds.put(R.id.v_service_status, 26);
        sViewsWithIds.put(R.id.tv_service_status, 27);
        sViewsWithIds.put(R.id.v_service, 28);
        sViewsWithIds.put(R.id.tv_delivery, 29);
        sViewsWithIds.put(R.id.v_delivery_price, 30);
        sViewsWithIds.put(R.id.tv_promo_code, 31);
        sViewsWithIds.put(R.id.v_promo_code_price, 32);
        sViewsWithIds.put(R.id.tv_payment, 33);
        sViewsWithIds.put(R.id.v_payment, 34);
        sViewsWithIds.put(R.id.tv_delivery_user_info, 35);
        sViewsWithIds.put(R.id.v_info, 36);
        sViewsWithIds.put(R.id.ic_accept_line, 37);
        sViewsWithIds.put(R.id.ic_shipped_line, 38);
        sViewsWithIds.put(R.id.ic_on_way_line, 39);
        sViewsWithIds.put(R.id.br, 40);
        sViewsWithIds.put(R.id.v_status, 41);
    }
    // views
    @NonNull
    private final androidx.core.widget.NestedScrollView mboundView0;
    @NonNull
    private final grand.app.aber_user.customViews.views.CustomTextViewRegular mboundView12;
    @NonNull
    private final androidx.recyclerview.widget.RecyclerView mboundView19;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentMyOrderDetailsBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 42, sIncludes, sViewsWithIds));
    }
    private FragmentMyOrderDetailsBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 1
            , (androidx.constraintlayout.widget.Barrier) bindings[40]
            , (androidx.cardview.widget.CardView) bindings[20]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[11]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[37]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[17]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[15]
            , (android.view.View) bindings[39]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[13]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[38]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[9]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[18]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[29]
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[35]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[5]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[8]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[16]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[33]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[7]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[10]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[31]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[6]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[23]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[2]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[27]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[4]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[25]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[3]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[21]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[1]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[14]
            , (android.view.View) bindings[30]
            , (android.view.View) bindings[36]
            , (android.view.View) bindings[34]
            , (android.view.View) bindings[32]
            , (android.view.View) bindings[28]
            , (android.view.View) bindings[24]
            , (android.view.View) bindings[26]
            , (android.view.View) bindings[22]
            , (android.view.View) bindings[41]
            );
        this.icAccept.setTag(null);
        this.icDelivered.setTag(null);
        this.icOnWay.setTag(null);
        this.icShipped.setTag(null);
        this.mboundView0 = (androidx.core.widget.NestedScrollView) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView12 = (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[12];
        this.mboundView12.setTag(null);
        this.mboundView19 = (androidx.recyclerview.widget.RecyclerView) bindings[19];
        this.mboundView19.setTag(null);
        this.tvAddress.setTag(null);
        this.tvDelivered.setTag(null);
        this.tvDeliveryValue.setTag(null);
        this.tvName.setTag(null);
        this.tvOnWay.setTag(null);
        this.tvPaymentValue.setTag(null);
        this.tvPhone.setTag(null);
        this.tvPromoCodeValue.setTag(null);
        this.tvServiceLocationValue.setTag(null);
        this.tvServiceStatusValue.setTag(null);
        this.tvServiceTimeValue.setTag(null);
        this.tvServicesNameValue.setTag(null);
        this.tvShipped.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x8L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.viewmodel == variableId) {
            setViewmodel((grand.app.aber_user.pages.myOrders.viewModels.MyOrdersViewModels) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setViewmodel(@Nullable grand.app.aber_user.pages.myOrders.viewModels.MyOrdersViewModels Viewmodel) {
        updateRegistration(0, Viewmodel);
        this.mViewmodel = Viewmodel;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.viewmodel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeViewmodel((grand.app.aber_user.pages.myOrders.viewModels.MyOrdersViewModels) object, fieldId);
        }
        return false;
    }
    private boolean onChangeViewmodel(grand.app.aber_user.pages.myOrders.viewModels.MyOrdersViewModels Viewmodel, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        else if (fieldId == BR.myOrderDetails) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        else if (fieldId == BR.productsAdapter) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        int viewmodelMyOrderDetailsStatusInt2TvShippedAndroidColorColorPrimaryTvShippedAndroidColorColordark = 0;
        java.lang.String viewmodelMyOrderDetailsSubtotal = null;
        boolean viewmodelMyOrderDetailsAddressesJavaLangObjectNull = false;
        java.lang.String viewmodelMyOrderDetailsAddressesJavaLangObjectNullViewmodelMyOrderDetailsAddressesJavaLangString = null;
        int viewmodelMyOrderDetailsStatusInt3TvOnWayAndroidColorColorPrimaryTvOnWayAndroidColorColordark = 0;
        java.lang.String stringValueOfViewmodelMyOrderDetailsSubtotal = null;
        boolean viewmodelMyOrderDetailsStatusInt3 = false;
        android.graphics.drawable.Drawable viewmodelMyOrderDetailsStatusInt4IcDeliveredAndroidDrawableIcFollowSuccessIcDeliveredAndroidDrawableIcFollowWaiting = null;
        java.lang.String viewmodelMyOrderDetailsAddresses = null;
        java.lang.String viewmodelMyOrderDetailsDiscount = null;
        android.graphics.drawable.Drawable viewmodelMyOrderDetailsStatusInt1IcAcceptAndroidDrawableIcFollowSuccessIcAcceptAndroidDrawableIcFollowWaiting = null;
        java.lang.String stringValueOfViewmodelMyOrderDetailsDiscount = null;
        android.graphics.drawable.Drawable viewmodelMyOrderDetailsStatusInt2IcShippedAndroidDrawableIcFollowSuccessIcShippedAndroidDrawableIcFollowWaiting = null;
        java.lang.String viewmodelMyOrderDetailsPhoneJavaLangObjectNullViewmodelMyOrderDetailsPhoneJavaLangString = null;
        boolean textUtilsIsEmptyViewmodelMyOrderDetailsOrderNumber = false;
        grand.app.aber_user.pages.myOrders.viewModels.MyOrdersViewModels viewmodel = mViewmodel;
        boolean viewmodelMyOrderDetailsStatusInt2 = false;
        double viewmodelMyOrderDetailsDeliveryFees = 0.0;
        grand.app.aber_user.pages.myOrders.adapter.MyOrderProductsAdapter viewmodelProductsAdapter = null;
        int textUtilsIsEmptyViewmodelMyOrderDetailsOrderNumberViewVISIBLEViewGONE = 0;
        java.lang.String stringValueOfViewmodelMyOrderDetailsTotal = null;
        java.lang.String viewmodelMyOrderDetailsPhone = null;
        java.lang.String tvPhoneAndroidStringRegisterPhoneConcatJavaLangStringConcatViewmodelMyOrderDetailsPhoneJavaLangObjectNullViewmodelMyOrderDetailsPhoneJavaLangString = null;
        java.lang.String tvAddressAndroidStringOrderAddressConcatJavaLangStringConcatViewmodelMyOrderDetailsAddressesJavaLangObjectNullViewmodelMyOrderDetailsAddressesJavaLangString = null;
        boolean viewmodelMyOrderDetailsPhoneJavaLangObjectNull = false;
        int viewmodelMyOrderDetailsStatusInt4TvDeliveredAndroidColorColorPrimaryTvDeliveredAndroidColorColordark = 0;
        boolean viewmodelMyOrderDetailsStatusInt4 = false;
        boolean TextUtilsIsEmptyViewmodelMyOrderDetailsOrderNumber1 = false;
        java.lang.String viewmodelMyOrderDetailsOrderNumber = null;
        int viewmodelMyOrderDetailsStatus = 0;
        java.lang.String viewmodelMyOrderDetailsName = null;
        java.lang.String viewmodelMyOrderDetailsPayType = null;
        android.graphics.drawable.Drawable viewmodelMyOrderDetailsStatusInt3IcOnWayAndroidDrawableIcFollowSuccessIcOnWayAndroidDrawableIcFollowWaiting = null;
        double viewmodelMyOrderDetailsTotal = 0.0;
        java.lang.String viewmodelMyOrderDetailsCreatedAt = null;
        grand.app.aber_user.pages.myOrders.models.MyOrderDetails viewmodelMyOrderDetails = null;
        java.lang.String stringValueOfViewmodelMyOrderDetailsDeliveryFees = null;
        int viewmodelMyOrderDetailsStatusInt1MboundView12AndroidColorColorPrimaryMboundView12AndroidColorColordark = 0;
        boolean viewmodelMyOrderDetailsStatusInt1 = false;

        if ((dirtyFlags & 0xfL) != 0) {


            if ((dirtyFlags & 0xdL) != 0) {

                    if (viewmodel != null) {
                        // read viewmodel.productsAdapter
                        viewmodelProductsAdapter = viewmodel.getProductsAdapter();
                    }
            }
            if ((dirtyFlags & 0xbL) != 0) {

                    if (viewmodel != null) {
                        // read viewmodel.myOrderDetails
                        viewmodelMyOrderDetails = viewmodel.getMyOrderDetails();
                    }


                    if (viewmodelMyOrderDetails != null) {
                        // read viewmodel.myOrderDetails.subtotal
                        viewmodelMyOrderDetailsSubtotal = viewmodelMyOrderDetails.getSubtotal();
                        // read viewmodel.myOrderDetails.addresses
                        viewmodelMyOrderDetailsAddresses = viewmodelMyOrderDetails.getAddresses();
                        // read viewmodel.myOrderDetails.discount
                        viewmodelMyOrderDetailsDiscount = viewmodelMyOrderDetails.getDiscount();
                        // read viewmodel.myOrderDetails.deliveryFees
                        viewmodelMyOrderDetailsDeliveryFees = viewmodelMyOrderDetails.getDeliveryFees();
                        // read viewmodel.myOrderDetails.phone
                        viewmodelMyOrderDetailsPhone = viewmodelMyOrderDetails.getPhone();
                        // read viewmodel.myOrderDetails.orderNumber
                        viewmodelMyOrderDetailsOrderNumber = viewmodelMyOrderDetails.getOrderNumber();
                        // read viewmodel.myOrderDetails.status
                        viewmodelMyOrderDetailsStatus = viewmodelMyOrderDetails.getStatus();
                        // read viewmodel.myOrderDetails.name
                        viewmodelMyOrderDetailsName = viewmodelMyOrderDetails.getName();
                        // read viewmodel.myOrderDetails.payType
                        viewmodelMyOrderDetailsPayType = viewmodelMyOrderDetails.getPayType();
                        // read viewmodel.myOrderDetails.total
                        viewmodelMyOrderDetailsTotal = viewmodelMyOrderDetails.getTotal();
                        // read viewmodel.myOrderDetails.createdAt
                        viewmodelMyOrderDetailsCreatedAt = viewmodelMyOrderDetails.getCreatedAt();
                    }


                    // read String.valueOf(viewmodel.myOrderDetails.subtotal)
                    stringValueOfViewmodelMyOrderDetailsSubtotal = java.lang.String.valueOf(viewmodelMyOrderDetailsSubtotal);
                    // read viewmodel.myOrderDetails.addresses != null
                    viewmodelMyOrderDetailsAddressesJavaLangObjectNull = (viewmodelMyOrderDetailsAddresses) != (null);
                    // read String.valueOf(viewmodel.myOrderDetails.discount)
                    stringValueOfViewmodelMyOrderDetailsDiscount = java.lang.String.valueOf(viewmodelMyOrderDetailsDiscount);
                    // read String.valueOf(viewmodel.myOrderDetails.deliveryFees)
                    stringValueOfViewmodelMyOrderDetailsDeliveryFees = java.lang.String.valueOf(viewmodelMyOrderDetailsDeliveryFees);
                    // read viewmodel.myOrderDetails.phone != null
                    viewmodelMyOrderDetailsPhoneJavaLangObjectNull = (viewmodelMyOrderDetailsPhone) != (null);
                    // read TextUtils.isEmpty(viewmodel.myOrderDetails.orderNumber)
                    textUtilsIsEmptyViewmodelMyOrderDetailsOrderNumber = android.text.TextUtils.isEmpty(viewmodelMyOrderDetailsOrderNumber);
                    // read viewmodel.myOrderDetails.status >= 3
                    viewmodelMyOrderDetailsStatusInt3 = (viewmodelMyOrderDetailsStatus) >= (3);
                    // read viewmodel.myOrderDetails.status >= 2
                    viewmodelMyOrderDetailsStatusInt2 = (viewmodelMyOrderDetailsStatus) >= (2);
                    // read viewmodel.myOrderDetails.status >= 4
                    viewmodelMyOrderDetailsStatusInt4 = (viewmodelMyOrderDetailsStatus) >= (4);
                    // read viewmodel.myOrderDetails.status >= 1
                    viewmodelMyOrderDetailsStatusInt1 = (viewmodelMyOrderDetailsStatus) >= (1);
                    // read String.valueOf(viewmodel.myOrderDetails.total)
                    stringValueOfViewmodelMyOrderDetailsTotal = java.lang.String.valueOf(viewmodelMyOrderDetailsTotal);
                if((dirtyFlags & 0xbL) != 0) {
                    if(viewmodelMyOrderDetailsAddressesJavaLangObjectNull) {
                            dirtyFlags |= 0x80L;
                    }
                    else {
                            dirtyFlags |= 0x40L;
                    }
                }
                if((dirtyFlags & 0xbL) != 0) {
                    if(viewmodelMyOrderDetailsPhoneJavaLangObjectNull) {
                            dirtyFlags |= 0x20000L;
                    }
                    else {
                            dirtyFlags |= 0x10000L;
                    }
                }
                if((dirtyFlags & 0xbL) != 0) {
                    if(viewmodelMyOrderDetailsStatusInt3) {
                            dirtyFlags |= 0x200L;
                            dirtyFlags |= 0x800000L;
                    }
                    else {
                            dirtyFlags |= 0x100L;
                            dirtyFlags |= 0x400000L;
                    }
                }
                if((dirtyFlags & 0xbL) != 0) {
                    if(viewmodelMyOrderDetailsStatusInt2) {
                            dirtyFlags |= 0x20L;
                            dirtyFlags |= 0x8000L;
                    }
                    else {
                            dirtyFlags |= 0x10L;
                            dirtyFlags |= 0x4000L;
                    }
                }
                if((dirtyFlags & 0xbL) != 0) {
                    if(viewmodelMyOrderDetailsStatusInt4) {
                            dirtyFlags |= 0x800L;
                            dirtyFlags |= 0x200000L;
                    }
                    else {
                            dirtyFlags |= 0x400L;
                            dirtyFlags |= 0x100000L;
                    }
                }
                if((dirtyFlags & 0xbL) != 0) {
                    if(viewmodelMyOrderDetailsStatusInt1) {
                            dirtyFlags |= 0x2000L;
                            dirtyFlags |= 0x2000000L;
                    }
                    else {
                            dirtyFlags |= 0x1000L;
                            dirtyFlags |= 0x1000000L;
                    }
                }


                    // read !TextUtils.isEmpty(viewmodel.myOrderDetails.orderNumber)
                    TextUtilsIsEmptyViewmodelMyOrderDetailsOrderNumber1 = !textUtilsIsEmptyViewmodelMyOrderDetailsOrderNumber;
                    // read viewmodel.myOrderDetails.status >= 3 ? @android:color/colorPrimary : @android:color/colordark
                    viewmodelMyOrderDetailsStatusInt3TvOnWayAndroidColorColorPrimaryTvOnWayAndroidColorColordark = ((viewmodelMyOrderDetailsStatusInt3) ? (getColorFromResource(tvOnWay, R.color.colorPrimary)) : (getColorFromResource(tvOnWay, R.color.colordark)));
                    // read viewmodel.myOrderDetails.status >= 3 ? @android:drawable/ic_follow_success : @android:drawable/ic_follow_waiting
                    viewmodelMyOrderDetailsStatusInt3IcOnWayAndroidDrawableIcFollowSuccessIcOnWayAndroidDrawableIcFollowWaiting = ((viewmodelMyOrderDetailsStatusInt3) ? (androidx.appcompat.content.res.AppCompatResources.getDrawable(icOnWay.getContext(), R.drawable.ic_follow_success)) : (androidx.appcompat.content.res.AppCompatResources.getDrawable(icOnWay.getContext(), R.drawable.ic_follow_waiting)));
                    // read viewmodel.myOrderDetails.status >= 2 ? @android:color/colorPrimary : @android:color/colordark
                    viewmodelMyOrderDetailsStatusInt2TvShippedAndroidColorColorPrimaryTvShippedAndroidColorColordark = ((viewmodelMyOrderDetailsStatusInt2) ? (getColorFromResource(tvShipped, R.color.colorPrimary)) : (getColorFromResource(tvShipped, R.color.colordark)));
                    // read viewmodel.myOrderDetails.status >= 2 ? @android:drawable/ic_follow_success : @android:drawable/ic_follow_waiting
                    viewmodelMyOrderDetailsStatusInt2IcShippedAndroidDrawableIcFollowSuccessIcShippedAndroidDrawableIcFollowWaiting = ((viewmodelMyOrderDetailsStatusInt2) ? (androidx.appcompat.content.res.AppCompatResources.getDrawable(icShipped.getContext(), R.drawable.ic_follow_success)) : (androidx.appcompat.content.res.AppCompatResources.getDrawable(icShipped.getContext(), R.drawable.ic_follow_waiting)));
                    // read viewmodel.myOrderDetails.status >= 4 ? @android:drawable/ic_follow_success : @android:drawable/ic_follow_waiting
                    viewmodelMyOrderDetailsStatusInt4IcDeliveredAndroidDrawableIcFollowSuccessIcDeliveredAndroidDrawableIcFollowWaiting = ((viewmodelMyOrderDetailsStatusInt4) ? (androidx.appcompat.content.res.AppCompatResources.getDrawable(icDelivered.getContext(), R.drawable.ic_follow_success)) : (androidx.appcompat.content.res.AppCompatResources.getDrawable(icDelivered.getContext(), R.drawable.ic_follow_waiting)));
                    // read viewmodel.myOrderDetails.status >= 4 ? @android:color/colorPrimary : @android:color/colordark
                    viewmodelMyOrderDetailsStatusInt4TvDeliveredAndroidColorColorPrimaryTvDeliveredAndroidColorColordark = ((viewmodelMyOrderDetailsStatusInt4) ? (getColorFromResource(tvDelivered, R.color.colorPrimary)) : (getColorFromResource(tvDelivered, R.color.colordark)));
                    // read viewmodel.myOrderDetails.status >= 1 ? @android:drawable/ic_follow_success : @android:drawable/ic_follow_waiting
                    viewmodelMyOrderDetailsStatusInt1IcAcceptAndroidDrawableIcFollowSuccessIcAcceptAndroidDrawableIcFollowWaiting = ((viewmodelMyOrderDetailsStatusInt1) ? (androidx.appcompat.content.res.AppCompatResources.getDrawable(icAccept.getContext(), R.drawable.ic_follow_success)) : (androidx.appcompat.content.res.AppCompatResources.getDrawable(icAccept.getContext(), R.drawable.ic_follow_waiting)));
                    // read viewmodel.myOrderDetails.status >= 1 ? @android:color/colorPrimary : @android:color/colordark
                    viewmodelMyOrderDetailsStatusInt1MboundView12AndroidColorColorPrimaryMboundView12AndroidColorColordark = ((viewmodelMyOrderDetailsStatusInt1) ? (getColorFromResource(mboundView12, R.color.colorPrimary)) : (getColorFromResource(mboundView12, R.color.colordark)));
                if((dirtyFlags & 0xbL) != 0) {
                    if(TextUtilsIsEmptyViewmodelMyOrderDetailsOrderNumber1) {
                            dirtyFlags |= 0x80000L;
                    }
                    else {
                            dirtyFlags |= 0x40000L;
                    }
                }


                    // read !TextUtils.isEmpty(viewmodel.myOrderDetails.orderNumber) ? View.VISIBLE : View.GONE
                    textUtilsIsEmptyViewmodelMyOrderDetailsOrderNumberViewVISIBLEViewGONE = ((TextUtilsIsEmptyViewmodelMyOrderDetailsOrderNumber1) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
        }
        // batch finished

        if ((dirtyFlags & 0xbL) != 0) {

                // read viewmodel.myOrderDetails.addresses != null ? viewmodel.myOrderDetails.addresses : ""
                viewmodelMyOrderDetailsAddressesJavaLangObjectNullViewmodelMyOrderDetailsAddressesJavaLangString = ((viewmodelMyOrderDetailsAddressesJavaLangObjectNull) ? (viewmodelMyOrderDetailsAddresses) : (""));
                // read viewmodel.myOrderDetails.phone != null ? viewmodel.myOrderDetails.phone : ""
                viewmodelMyOrderDetailsPhoneJavaLangObjectNullViewmodelMyOrderDetailsPhoneJavaLangString = ((viewmodelMyOrderDetailsPhoneJavaLangObjectNull) ? (viewmodelMyOrderDetailsPhone) : (""));


                // read @android:string/order_address.concat(" ").concat(viewmodel.myOrderDetails.addresses != null ? viewmodel.myOrderDetails.addresses : "")
                tvAddressAndroidStringOrderAddressConcatJavaLangStringConcatViewmodelMyOrderDetailsAddressesJavaLangObjectNullViewmodelMyOrderDetailsAddressesJavaLangString = tvAddress.getResources().getString(R.string.order_address).concat(" ").concat(viewmodelMyOrderDetailsAddressesJavaLangObjectNullViewmodelMyOrderDetailsAddressesJavaLangString);
                // read @android:string/register_phone.concat(" ").concat(viewmodel.myOrderDetails.phone != null ? viewmodel.myOrderDetails.phone : "")
                tvPhoneAndroidStringRegisterPhoneConcatJavaLangStringConcatViewmodelMyOrderDetailsPhoneJavaLangObjectNullViewmodelMyOrderDetailsPhoneJavaLangString = tvPhone.getResources().getString(R.string.register_phone).concat(" ").concat(viewmodelMyOrderDetailsPhoneJavaLangObjectNullViewmodelMyOrderDetailsPhoneJavaLangString);
        }
        // batch finished
        if ((dirtyFlags & 0xbL) != 0) {
            // api target 1

            androidx.databinding.adapters.ImageViewBindingAdapter.setImageDrawable(this.icAccept, viewmodelMyOrderDetailsStatusInt1IcAcceptAndroidDrawableIcFollowSuccessIcAcceptAndroidDrawableIcFollowWaiting);
            androidx.databinding.adapters.ImageViewBindingAdapter.setImageDrawable(this.icDelivered, viewmodelMyOrderDetailsStatusInt4IcDeliveredAndroidDrawableIcFollowSuccessIcDeliveredAndroidDrawableIcFollowWaiting);
            androidx.databinding.adapters.ImageViewBindingAdapter.setImageDrawable(this.icOnWay, viewmodelMyOrderDetailsStatusInt3IcOnWayAndroidDrawableIcFollowSuccessIcOnWayAndroidDrawableIcFollowWaiting);
            androidx.databinding.adapters.ImageViewBindingAdapter.setImageDrawable(this.icShipped, viewmodelMyOrderDetailsStatusInt2IcShippedAndroidDrawableIcFollowSuccessIcShippedAndroidDrawableIcFollowWaiting);
            this.mboundView0.setVisibility(textUtilsIsEmptyViewmodelMyOrderDetailsOrderNumberViewVISIBLEViewGONE);
            this.mboundView12.setTextColor(viewmodelMyOrderDetailsStatusInt1MboundView12AndroidColorColorPrimaryMboundView12AndroidColorColordark);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvAddress, tvAddressAndroidStringOrderAddressConcatJavaLangStringConcatViewmodelMyOrderDetailsAddressesJavaLangObjectNullViewmodelMyOrderDetailsAddressesJavaLangString);
            this.tvDelivered.setTextColor(viewmodelMyOrderDetailsStatusInt4TvDeliveredAndroidColorColorPrimaryTvDeliveredAndroidColorColordark);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvDeliveryValue, stringValueOfViewmodelMyOrderDetailsDeliveryFees);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvName, viewmodelMyOrderDetailsName);
            this.tvOnWay.setTextColor(viewmodelMyOrderDetailsStatusInt3TvOnWayAndroidColorColorPrimaryTvOnWayAndroidColorColordark);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvPaymentValue, viewmodelMyOrderDetailsPayType);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvPhone, tvPhoneAndroidStringRegisterPhoneConcatJavaLangStringConcatViewmodelMyOrderDetailsPhoneJavaLangObjectNullViewmodelMyOrderDetailsPhoneJavaLangString);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvPromoCodeValue, stringValueOfViewmodelMyOrderDetailsDiscount);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvServiceLocationValue, stringValueOfViewmodelMyOrderDetailsTotal);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvServiceStatusValue, stringValueOfViewmodelMyOrderDetailsSubtotal);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvServiceTimeValue, viewmodelMyOrderDetailsCreatedAt);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvServicesNameValue, viewmodelMyOrderDetailsOrderNumber);
            this.tvShipped.setTextColor(viewmodelMyOrderDetailsStatusInt2TvShippedAndroidColorColorPrimaryTvShippedAndroidColorColordark);
        }
        if ((dirtyFlags & 0xdL) != 0) {
            // api target 1

            grand.app.aber_user.base.ApplicationBinding.getItemsV2Binding(this.mboundView19, viewmodelProductsAdapter, "1", "1");
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): viewmodel
        flag 1 (0x2L): viewmodel.myOrderDetails
        flag 2 (0x3L): viewmodel.productsAdapter
        flag 3 (0x4L): null
        flag 4 (0x5L): viewmodel.myOrderDetails.status >= 2 ? @android:color/colorPrimary : @android:color/colordark
        flag 5 (0x6L): viewmodel.myOrderDetails.status >= 2 ? @android:color/colorPrimary : @android:color/colordark
        flag 6 (0x7L): viewmodel.myOrderDetails.addresses != null ? viewmodel.myOrderDetails.addresses : ""
        flag 7 (0x8L): viewmodel.myOrderDetails.addresses != null ? viewmodel.myOrderDetails.addresses : ""
        flag 8 (0x9L): viewmodel.myOrderDetails.status >= 3 ? @android:color/colorPrimary : @android:color/colordark
        flag 9 (0xaL): viewmodel.myOrderDetails.status >= 3 ? @android:color/colorPrimary : @android:color/colordark
        flag 10 (0xbL): viewmodel.myOrderDetails.status >= 4 ? @android:drawable/ic_follow_success : @android:drawable/ic_follow_waiting
        flag 11 (0xcL): viewmodel.myOrderDetails.status >= 4 ? @android:drawable/ic_follow_success : @android:drawable/ic_follow_waiting
        flag 12 (0xdL): viewmodel.myOrderDetails.status >= 1 ? @android:drawable/ic_follow_success : @android:drawable/ic_follow_waiting
        flag 13 (0xeL): viewmodel.myOrderDetails.status >= 1 ? @android:drawable/ic_follow_success : @android:drawable/ic_follow_waiting
        flag 14 (0xfL): viewmodel.myOrderDetails.status >= 2 ? @android:drawable/ic_follow_success : @android:drawable/ic_follow_waiting
        flag 15 (0x10L): viewmodel.myOrderDetails.status >= 2 ? @android:drawable/ic_follow_success : @android:drawable/ic_follow_waiting
        flag 16 (0x11L): viewmodel.myOrderDetails.phone != null ? viewmodel.myOrderDetails.phone : ""
        flag 17 (0x12L): viewmodel.myOrderDetails.phone != null ? viewmodel.myOrderDetails.phone : ""
        flag 18 (0x13L): !TextUtils.isEmpty(viewmodel.myOrderDetails.orderNumber) ? View.VISIBLE : View.GONE
        flag 19 (0x14L): !TextUtils.isEmpty(viewmodel.myOrderDetails.orderNumber) ? View.VISIBLE : View.GONE
        flag 20 (0x15L): viewmodel.myOrderDetails.status >= 4 ? @android:color/colorPrimary : @android:color/colordark
        flag 21 (0x16L): viewmodel.myOrderDetails.status >= 4 ? @android:color/colorPrimary : @android:color/colordark
        flag 22 (0x17L): viewmodel.myOrderDetails.status >= 3 ? @android:drawable/ic_follow_success : @android:drawable/ic_follow_waiting
        flag 23 (0x18L): viewmodel.myOrderDetails.status >= 3 ? @android:drawable/ic_follow_success : @android:drawable/ic_follow_waiting
        flag 24 (0x19L): viewmodel.myOrderDetails.status >= 1 ? @android:color/colorPrimary : @android:color/colordark
        flag 25 (0x1aL): viewmodel.myOrderDetails.status >= 1 ? @android:color/colorPrimary : @android:color/colordark
    flag mapping end*/
    //end
}