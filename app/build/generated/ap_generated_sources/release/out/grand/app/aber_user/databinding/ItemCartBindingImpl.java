package grand.app.aber_user.databinding;
import grand.app.aber_user.R;
import grand.app.aber_user.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ItemCartBindingImpl extends ItemCartBinding implements grand.app.aber_user.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.br, 10);
    }
    // views
    @NonNull
    private final androidx.cardview.widget.CardView mboundView0;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback227;
    @Nullable
    private final android.view.View.OnClickListener mCallback228;
    @Nullable
    private final android.view.View.OnClickListener mCallback226;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ItemCartBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 11, sIncludes, sViewsWithIds));
    }
    private ItemCartBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 1
            , (androidx.constraintlayout.widget.Barrier) bindings[10]
            , (com.google.android.material.button.MaterialButton) bindings[5]
            , (com.google.android.material.card.MaterialCardView) bindings[6]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[2]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[7]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[1]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[9]
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[4]
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[8]
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[3]
            );
        this.btnCount.setTag(null);
        this.flowActions.setTag(null);
        this.icClosePage.setTag(null);
        this.icMinus.setTag(null);
        this.icPartsImage.setTag(null);
        this.icPlus.setTag(null);
        this.itemDetailOrderPrice.setTag(null);
        this.itemNumber.setTag(null);
        this.mboundView0 = (androidx.cardview.widget.CardView) bindings[0];
        this.mboundView0.setTag(null);
        this.tvPartsName.setTag(null);
        setRootTag(root);
        // listeners
        mCallback227 = new grand.app.aber_user.generated.callback.OnClickListener(this, 2);
        mCallback228 = new grand.app.aber_user.generated.callback.OnClickListener(this, 3);
        mCallback226 = new grand.app.aber_user.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.itemViewModel == variableId) {
            setItemViewModel((grand.app.aber_user.pages.cart.viewModels.ItemCartViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setItemViewModel(@Nullable grand.app.aber_user.pages.cart.viewModels.ItemCartViewModel ItemViewModel) {
        updateRegistration(0, ItemViewModel);
        this.mItemViewModel = ItemViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.itemViewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeItemViewModel((grand.app.aber_user.pages.cart.viewModels.ItemCartViewModel) object, fieldId);
        }
        return false;
    }
    private boolean onChangeItemViewModel(grand.app.aber_user.pages.cart.viewModels.ItemCartViewModel ItemViewModel, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        else if (fieldId == BR.productsItem) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        int itemViewModelProductsItemConfirmedViewVISIBLEViewINVISIBLE = 0;
        java.lang.String stringValueOfItemViewModelProductsItemQuantityConcatJavaLangString = null;
        boolean itemViewModelProductsItemConfirmed = false;
        boolean ItemViewModelProductsItemConfirmed1 = false;
        grand.app.aber_user.pages.parts.models.ProductsItem itemViewModelProductsItem = null;
        java.lang.String itemViewModelProductsItemImage = null;
        int itemViewModelProductsItemQuantity = 0;
        java.lang.String itemViewModelProductsItemName = null;
        java.lang.String itemViewModelProductsItemPrice = null;
        grand.app.aber_user.pages.cart.viewModels.ItemCartViewModel itemViewModel = mItemViewModel;
        java.lang.String itemViewModelProductsItemCurrency = null;
        java.lang.String itemViewModelProductsItemPriceConcatJavaLangStringConcatItemViewModelProductsItemCurrency = null;
        int itemViewModelProductsItemConfirmedViewVISIBLEViewGONE = 0;
        java.lang.String stringValueOfItemViewModelProductsItemQuantity = null;
        java.lang.String stringValueOfItemViewModelProductsItemQuantityConcatJavaLangStringConcatBtnCountAndroidStringCartItemPart = null;
        int ItemViewModelProductsItemConfirmedViewVISIBLEViewGONE1 = 0;
        java.lang.String itemViewModelProductsItemPriceConcatJavaLangString = null;

        if ((dirtyFlags & 0x7L) != 0) {



                if (itemViewModel != null) {
                    // read itemViewModel.productsItem
                    itemViewModelProductsItem = itemViewModel.getProductsItem();
                }


                if (itemViewModelProductsItem != null) {
                    // read itemViewModel.productsItem.confirmed
                    ItemViewModelProductsItemConfirmed1 = itemViewModelProductsItem.isConfirmed();
                    // read itemViewModel.productsItem.image
                    itemViewModelProductsItemImage = itemViewModelProductsItem.getImage();
                    // read itemViewModel.productsItem.quantity
                    itemViewModelProductsItemQuantity = itemViewModelProductsItem.getQuantity();
                    // read itemViewModel.productsItem.name
                    itemViewModelProductsItemName = itemViewModelProductsItem.getName();
                    // read itemViewModel.productsItem.price
                    itemViewModelProductsItemPrice = itemViewModelProductsItem.getPrice();
                    // read itemViewModel.productsItem.currency
                    itemViewModelProductsItemCurrency = itemViewModelProductsItem.getCurrency();
                }
            if((dirtyFlags & 0x7L) != 0) {
                if(ItemViewModelProductsItemConfirmed1) {
                        dirtyFlags |= 0x100L;
                }
                else {
                        dirtyFlags |= 0x80L;
                }
            }


                // read !itemViewModel.productsItem.confirmed
                itemViewModelProductsItemConfirmed = !ItemViewModelProductsItemConfirmed1;
                // read itemViewModel.productsItem.confirmed ? View.VISIBLE : View.GONE
                ItemViewModelProductsItemConfirmedViewVISIBLEViewGONE1 = ((ItemViewModelProductsItemConfirmed1) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
                // read String.valueOf(itemViewModel.productsItem.quantity)
                stringValueOfItemViewModelProductsItemQuantity = java.lang.String.valueOf(itemViewModelProductsItemQuantity);
            if((dirtyFlags & 0x7L) != 0) {
                if(itemViewModelProductsItemConfirmed) {
                        dirtyFlags |= 0x10L;
                        dirtyFlags |= 0x40L;
                }
                else {
                        dirtyFlags |= 0x8L;
                        dirtyFlags |= 0x20L;
                }
            }
                if (itemViewModelProductsItemPrice != null) {
                    // read itemViewModel.productsItem.price.concat(" ")
                    itemViewModelProductsItemPriceConcatJavaLangString = itemViewModelProductsItemPrice.concat(" ");
                }


                // read !itemViewModel.productsItem.confirmed ? View.VISIBLE : View.INVISIBLE
                itemViewModelProductsItemConfirmedViewVISIBLEViewINVISIBLE = ((itemViewModelProductsItemConfirmed) ? (android.view.View.VISIBLE) : (android.view.View.INVISIBLE));
                // read !itemViewModel.productsItem.confirmed ? View.VISIBLE : View.GONE
                itemViewModelProductsItemConfirmedViewVISIBLEViewGONE = ((itemViewModelProductsItemConfirmed) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
                if (stringValueOfItemViewModelProductsItemQuantity != null) {
                    // read String.valueOf(itemViewModel.productsItem.quantity).concat(" ")
                    stringValueOfItemViewModelProductsItemQuantityConcatJavaLangString = stringValueOfItemViewModelProductsItemQuantity.concat(" ");
                }
                if (itemViewModelProductsItemPriceConcatJavaLangString != null) {
                    // read itemViewModel.productsItem.price.concat(" ").concat(itemViewModel.productsItem.currency)
                    itemViewModelProductsItemPriceConcatJavaLangStringConcatItemViewModelProductsItemCurrency = itemViewModelProductsItemPriceConcatJavaLangString.concat(itemViewModelProductsItemCurrency);
                }


                if (stringValueOfItemViewModelProductsItemQuantityConcatJavaLangString != null) {
                    // read String.valueOf(itemViewModel.productsItem.quantity).concat(" ").concat(@android:string/cart_item_part)
                    stringValueOfItemViewModelProductsItemQuantityConcatJavaLangStringConcatBtnCountAndroidStringCartItemPart = stringValueOfItemViewModelProductsItemQuantityConcatJavaLangString.concat(btnCount.getResources().getString(R.string.cart_item_part));
                }
        }
        // batch finished
        if ((dirtyFlags & 0x7L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.btnCount, stringValueOfItemViewModelProductsItemQuantityConcatJavaLangStringConcatBtnCountAndroidStringCartItemPart);
            this.btnCount.setVisibility(ItemViewModelProductsItemConfirmedViewVISIBLEViewGONE1);
            this.flowActions.setVisibility(itemViewModelProductsItemConfirmedViewVISIBLEViewINVISIBLE);
            this.icClosePage.setVisibility(itemViewModelProductsItemConfirmedViewVISIBLEViewGONE);
            grand.app.aber_user.base.ApplicationBinding.loadImage(this.icPartsImage, itemViewModelProductsItemImage);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.itemDetailOrderPrice, itemViewModelProductsItemPriceConcatJavaLangStringConcatItemViewModelProductsItemCurrency);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.itemNumber, stringValueOfItemViewModelProductsItemQuantity);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvPartsName, itemViewModelProductsItemName);
        }
        if ((dirtyFlags & 0x4L) != 0) {
            // api target 1

            this.icClosePage.setOnClickListener(mCallback226);
            this.icMinus.setOnClickListener(mCallback227);
            this.icPlus.setOnClickListener(mCallback228);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        switch(sourceId) {
            case 2: {
                // localize variables for thread safety
                // itemViewModel
                grand.app.aber_user.pages.cart.viewModels.ItemCartViewModel itemViewModel = mItemViewModel;
                // itemViewModel != null
                boolean itemViewModelJavaLangObjectNull = false;



                itemViewModelJavaLangObjectNull = (itemViewModel) != (null);
                if (itemViewModelJavaLangObjectNull) {


                    itemViewModel.minusItem();
                }
                break;
            }
            case 3: {
                // localize variables for thread safety
                // itemViewModel
                grand.app.aber_user.pages.cart.viewModels.ItemCartViewModel itemViewModel = mItemViewModel;
                // itemViewModel != null
                boolean itemViewModelJavaLangObjectNull = false;



                itemViewModelJavaLangObjectNull = (itemViewModel) != (null);
                if (itemViewModelJavaLangObjectNull) {


                    itemViewModel.plusItem();
                }
                break;
            }
            case 1: {
                // localize variables for thread safety
                // itemViewModel
                grand.app.aber_user.pages.cart.viewModels.ItemCartViewModel itemViewModel = mItemViewModel;
                // itemViewModel != null
                boolean itemViewModelJavaLangObjectNull = false;



                itemViewModelJavaLangObjectNull = (itemViewModel) != (null);
                if (itemViewModelJavaLangObjectNull) {


                    itemViewModel.itemAction();
                }
                break;
            }
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): itemViewModel
        flag 1 (0x2L): itemViewModel.productsItem
        flag 2 (0x3L): null
        flag 3 (0x4L): !itemViewModel.productsItem.confirmed ? View.VISIBLE : View.INVISIBLE
        flag 4 (0x5L): !itemViewModel.productsItem.confirmed ? View.VISIBLE : View.INVISIBLE
        flag 5 (0x6L): !itemViewModel.productsItem.confirmed ? View.VISIBLE : View.GONE
        flag 6 (0x7L): !itemViewModel.productsItem.confirmed ? View.VISIBLE : View.GONE
        flag 7 (0x8L): itemViewModel.productsItem.confirmed ? View.VISIBLE : View.GONE
        flag 8 (0x9L): itemViewModel.productsItem.confirmed ? View.VISIBLE : View.GONE
    flag mapping end*/
    //end
}