package grand.app.aber_user.databinding;
import grand.app.aber_user.R;
import grand.app.aber_user.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ItemConversationBindingImpl extends ItemConversationBinding implements grand.app.aber_user.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = null;
    }
    // views
    @NonNull
    private final androidx.cardview.widget.CardView mboundView0;
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView1;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback123;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ItemConversationBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 6, sIncludes, sViewsWithIds));
    }
    private ItemConversationBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 1
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[5]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[3]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[4]
            , (de.hdodenhof.circleimageview.CircleImageView) bindings[2]
            );
        this.conTime.setTag(null);
        this.conTitle.setTag(null);
        this.conTitleAddress.setTag(null);
        this.conUserImage.setTag(null);
        this.mboundView0 = (androidx.cardview.widget.CardView) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[1];
        this.mboundView1.setTag(null);
        setRootTag(root);
        // listeners
        mCallback123 = new grand.app.aber_user.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.itemViewModel == variableId) {
            setItemViewModel((grand.app.aber_user.pages.conversations.viewModels.ItemConversationsViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setItemViewModel(@Nullable grand.app.aber_user.pages.conversations.viewModels.ItemConversationsViewModel ItemViewModel) {
        updateRegistration(0, ItemViewModel);
        this.mItemViewModel = ItemViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.itemViewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeItemViewModel((grand.app.aber_user.pages.conversations.viewModels.ItemConversationsViewModel) object, fieldId);
        }
        return false;
    }
    private boolean onChangeItemViewModel(grand.app.aber_user.pages.conversations.viewModels.ItemConversationsViewModel ItemViewModel, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        else if (fieldId == BR.conversationsData) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        grand.app.aber_user.pages.conversations.viewModels.ItemConversationsViewModel itemViewModel = mItemViewModel;
        java.lang.String itemViewModelConversationsDataReceiverImage = null;
        grand.app.aber_user.pages.auth.models.UserData itemViewModelConversationsDataReceiver = null;
        java.lang.String itemViewModelConversationsDataMessage = null;
        java.lang.String itemViewModelConversationsDataCreatedAt = null;
        grand.app.aber_user.pages.conversations.models.ConversationsData itemViewModelConversationsData = null;
        java.lang.String itemViewModelConversationsDataReceiverName = null;

        if ((dirtyFlags & 0x7L) != 0) {



                if (itemViewModel != null) {
                    // read itemViewModel.conversationsData
                    itemViewModelConversationsData = itemViewModel.getConversationsData();
                }


                if (itemViewModelConversationsData != null) {
                    // read itemViewModel.conversationsData.receiver
                    itemViewModelConversationsDataReceiver = itemViewModelConversationsData.getReceiver();
                    // read itemViewModel.conversationsData.message
                    itemViewModelConversationsDataMessage = itemViewModelConversationsData.getMessage();
                    // read itemViewModel.conversationsData.createdAt
                    itemViewModelConversationsDataCreatedAt = itemViewModelConversationsData.getCreatedAt();
                }


                if (itemViewModelConversationsDataReceiver != null) {
                    // read itemViewModel.conversationsData.receiver.image
                    itemViewModelConversationsDataReceiverImage = itemViewModelConversationsDataReceiver.getImage();
                    // read itemViewModel.conversationsData.receiver.name
                    itemViewModelConversationsDataReceiverName = itemViewModelConversationsDataReceiver.getName();
                }
        }
        // batch finished
        if ((dirtyFlags & 0x7L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.conTime, itemViewModelConversationsDataCreatedAt);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.conTitle, itemViewModelConversationsDataReceiverName);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.conTitleAddress, itemViewModelConversationsDataMessage);
            grand.app.aber_user.base.ApplicationBinding.loadImage(this.conUserImage, itemViewModelConversationsDataReceiverImage);
        }
        if ((dirtyFlags & 0x4L) != 0) {
            // api target 1

            this.mboundView1.setOnClickListener(mCallback123);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        // localize variables for thread safety
        // itemViewModel
        grand.app.aber_user.pages.conversations.viewModels.ItemConversationsViewModel itemViewModel = mItemViewModel;
        // itemViewModel != null
        boolean itemViewModelJavaLangObjectNull = false;



        itemViewModelJavaLangObjectNull = (itemViewModel) != (null);
        if (itemViewModelJavaLangObjectNull) {


            itemViewModel.itemAction();
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): itemViewModel
        flag 1 (0x2L): itemViewModel.conversationsData
        flag 2 (0x3L): null
    flag mapping end*/
    //end
}