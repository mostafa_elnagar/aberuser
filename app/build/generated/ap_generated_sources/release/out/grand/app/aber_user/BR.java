package grand.app.aber_user;

public class BR {
  public static final int _all = 0;

  public static final int aboutData = 1;

  public static final int adapter = 2;

  public static final int addLocationRequest = 3;

  public static final int appWalletAdapter = 4;

  public static final int baseViewModel = 5;

  public static final int carCatAdapter = 6;

  public static final int carModelAdapter = 7;

  public static final int carTypeAdapter = 8;

  public static final int cartAdapter = 9;

  public static final int chat = 10;

  public static final int checkPromoRequest = 11;

  public static final int colorItem = 12;

  public static final int colorSizesAdapter = 13;

  public static final int colorsAdapter = 14;

  public static final int contact = 15;

  public static final int contactsAdapter = 16;

  public static final int conversationsAdapter = 17;

  public static final int conversationsData = 18;

  public static final int conversationsMain = 19;

  public static final int countriesAdapter = 20;

  public static final int countriesData = 21;

  public static final int createServiceOrder = 22;

  public static final int details = 23;

  public static final int detailsAdapter = 24;

  public static final int downsObject = 25;

  public static final int dropDownsObject = 26;

  public static final int extra = 27;

  public static final int extraAdapter = 28;

  public static final int filterData = 29;

  public static final int filterRequest = 30;

  public static final int historyWalletData = 31;

  public static final int homeServicesAdapter = 32;

  public static final int homeSliderAdapter = 33;

  public static final int itemChatViewModel = 34;

  public static final int itemOrderViewModel = 35;

  public static final int itemPostViewModel = 36;

  public static final int itemViewModel = 37;

  public static final int itemWalletViewModel = 38;

  public static final int locationsAdapters = 39;

  public static final int locationsData = 40;

  public static final int mainData = 41;

  public static final int mapAddressViewModel = 42;

  public static final int menuServicesAdapter = 43;

  public static final int menuViewModel = 44;

  public static final int message = 45;

  public static final int myOrderDetails = 46;

  public static final int myOrdersAdapter = 47;

  public static final int newOrderRequest = 48;

  public static final int notificationsAdapter = 49;

  public static final int notificationsData = 50;

  public static final int notifyItemViewModels = 51;

  public static final int notifyViewModel = 52;

  public static final int oilPricesAdapter = 53;

  public static final int onBoardAdapter = 54;

  public static final int onBoardViewModels = 55;

  public static final int partsAdapter = 56;

  public static final int passingObject = 57;

  public static final int postData = 58;

  public static final int productColorsAdapter = 59;

  public static final int productDetails = 60;

  public static final int productsAdapter = 61;

  public static final int productsItem = 62;

  public static final int raiseWalletRequest = 63;

  public static final int rateRequest = 64;

  public static final int ratesItem = 65;

  public static final int reviewMainData = 66;

  public static final int reviewsAdapter = 67;

  public static final int service = 68;

  public static final int serviceOrderRequest = 69;

  public static final int services = 70;

  public static final int servicesItem = 71;

  public static final int servicesRequiredAdapter = 72;

  public static final int sizesItem = 73;

  public static final int sliderAdapter = 74;

  public static final int socialAdapter = 75;

  public static final int socialMediaData = 76;

  public static final int userDocuments = 77;

  public static final int viewModel = 78;

  public static final int viewmodel = 79;

  public static final int walletHistoryItem = 80;
}
