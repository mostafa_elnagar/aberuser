// Generated by Dagger (https://dagger.dev).
package grand.app.aber_user.pages.parts.viewModels;

import dagger.internal.Factory;
import grand.app.aber_user.repository.ServicesRepository;
import javax.inject.Provider;

@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class PartsViewModels_Factory implements Factory<PartsViewModels> {
  private final Provider<ServicesRepository> postRepositoryProvider;

  private final Provider<ServicesRepository> postRepositoryProvider2;

  public PartsViewModels_Factory(Provider<ServicesRepository> postRepositoryProvider,
      Provider<ServicesRepository> postRepositoryProvider2) {
    this.postRepositoryProvider = postRepositoryProvider;
    this.postRepositoryProvider2 = postRepositoryProvider2;
  }

  @Override
  public PartsViewModels get() {
    PartsViewModels instance = newInstance(postRepositoryProvider.get());
    PartsViewModels_MembersInjector.injectPostRepository(instance, postRepositoryProvider2.get());
    return instance;
  }

  public static PartsViewModels_Factory create(Provider<ServicesRepository> postRepositoryProvider,
      Provider<ServicesRepository> postRepositoryProvider2) {
    return new PartsViewModels_Factory(postRepositoryProvider, postRepositoryProvider2);
  }

  public static PartsViewModels newInstance(ServicesRepository postRepository) {
    return new PartsViewModels(postRepository);
  }
}
