package grand.app.aber_user.databinding;
import grand.app.aber_user.R;
import grand.app.aber_user.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentCartBindingImpl extends FragmentCartBinding implements grand.app.aber_user.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.v_cart, 5);
        sViewsWithIds.put(R.id.cart_action, 6);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback155;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentCartBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 7, sIncludes, sViewsWithIds));
    }
    private FragmentCartBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 2
            , (com.google.android.material.button.MaterialButton) bindings[4]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[6]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[2]
            , (androidx.recyclerview.widget.RecyclerView) bindings[1]
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[3]
            , (android.view.View) bindings[5]
            );
        this.btnSaveCart.setTag(null);
        this.icEmptyFavo.setTag(null);
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.recCart.setTag(null);
        this.tvTotal.setTag(null);
        setRootTag(root);
        // listeners
        mCallback155 = new grand.app.aber_user.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x8L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.viewmodel == variableId) {
            setViewmodel((grand.app.aber_user.pages.cart.viewModels.CartViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setViewmodel(@Nullable grand.app.aber_user.pages.cart.viewModels.CartViewModel Viewmodel) {
        updateRegistration(1, Viewmodel);
        this.mViewmodel = Viewmodel;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.viewmodel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeViewmodelCartTotalCurrency((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 1 :
                return onChangeViewmodel((grand.app.aber_user.pages.cart.viewModels.CartViewModel) object, fieldId);
        }
        return false;
    }
    private boolean onChangeViewmodelCartTotalCurrency(androidx.databinding.ObservableField<java.lang.String> ViewmodelCartTotalCurrency, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewmodel(grand.app.aber_user.pages.cart.viewModels.CartViewModel Viewmodel, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        else if (fieldId == BR.cartAdapter) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        androidx.databinding.ObservableField<java.lang.String> viewmodelCartTotalCurrency = null;
        grand.app.aber_user.pages.cart.adapters.CartAdapter viewmodelCartAdapter = null;
        boolean viewmodelCartAdapterMenuModelsSizeInt0 = false;
        java.lang.String viewmodelCartTotalCurrencyGet = null;
        int viewmodelCartAdapterMenuModelsSizeInt0ViewVISIBLEViewGONE = 0;
        java.util.List<grand.app.aber_user.pages.parts.models.ProductsItem> viewmodelCartAdapterMenuModels = null;
        int viewmodelCartAdapterMenuModelsSize = 0;
        grand.app.aber_user.pages.cart.viewModels.CartViewModel viewmodel = mViewmodel;

        if ((dirtyFlags & 0xfL) != 0) {


            if ((dirtyFlags & 0xbL) != 0) {

                    if (viewmodel != null) {
                        // read viewmodel.cartTotalCurrency
                        viewmodelCartTotalCurrency = viewmodel.cartTotalCurrency;
                    }
                    updateRegistration(0, viewmodelCartTotalCurrency);


                    if (viewmodelCartTotalCurrency != null) {
                        // read viewmodel.cartTotalCurrency.get()
                        viewmodelCartTotalCurrencyGet = viewmodelCartTotalCurrency.get();
                    }
            }
            if ((dirtyFlags & 0xeL) != 0) {

                    if (viewmodel != null) {
                        // read viewmodel.cartAdapter
                        viewmodelCartAdapter = viewmodel.getCartAdapter();
                    }


                    if (viewmodelCartAdapter != null) {
                        // read viewmodel.cartAdapter.menuModels
                        viewmodelCartAdapterMenuModels = viewmodelCartAdapter.getMenuModels();
                    }


                    if (viewmodelCartAdapterMenuModels != null) {
                        // read viewmodel.cartAdapter.menuModels.size()
                        viewmodelCartAdapterMenuModelsSize = viewmodelCartAdapterMenuModels.size();
                    }


                    // read viewmodel.cartAdapter.menuModels.size() == 0
                    viewmodelCartAdapterMenuModelsSizeInt0 = (viewmodelCartAdapterMenuModelsSize) == (0);
                if((dirtyFlags & 0xeL) != 0) {
                    if(viewmodelCartAdapterMenuModelsSizeInt0) {
                            dirtyFlags |= 0x20L;
                    }
                    else {
                            dirtyFlags |= 0x10L;
                    }
                }


                    // read viewmodel.cartAdapter.menuModels.size() == 0 ? View.VISIBLE : View.GONE
                    viewmodelCartAdapterMenuModelsSizeInt0ViewVISIBLEViewGONE = ((viewmodelCartAdapterMenuModelsSizeInt0) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
        }
        // batch finished
        if ((dirtyFlags & 0x8L) != 0) {
            // api target 1

            this.btnSaveCart.setOnClickListener(mCallback155);
        }
        if ((dirtyFlags & 0xeL) != 0) {
            // api target 1

            this.icEmptyFavo.setVisibility(viewmodelCartAdapterMenuModelsSizeInt0ViewVISIBLEViewGONE);
            grand.app.aber_user.base.ApplicationBinding.getItemsV2Binding(this.recCart, viewmodelCartAdapter, "1", "1");
        }
        if ((dirtyFlags & 0xbL) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvTotal, viewmodelCartTotalCurrencyGet);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        // localize variables for thread safety
        // viewmodel != null
        boolean viewmodelJavaLangObjectNull = false;
        // viewmodel
        grand.app.aber_user.pages.cart.viewModels.CartViewModel viewmodel = mViewmodel;



        viewmodelJavaLangObjectNull = (viewmodel) != (null);
        if (viewmodelJavaLangObjectNull) {


            viewmodel.toConfirmOrder();
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): viewmodel.cartTotalCurrency
        flag 1 (0x2L): viewmodel
        flag 2 (0x3L): viewmodel.cartAdapter
        flag 3 (0x4L): null
        flag 4 (0x5L): viewmodel.cartAdapter.menuModels.size() == 0 ? View.VISIBLE : View.GONE
        flag 5 (0x6L): viewmodel.cartAdapter.menuModels.size() == 0 ? View.VISIBLE : View.GONE
    flag mapping end*/
    //end
}