package grand.app.aber_user.databinding;
import grand.app.aber_user.R;
import grand.app.aber_user.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ItemCategoryBindingImpl extends ItemCategoryBinding implements grand.app.aber_user.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = null;
    }
    // views
    @NonNull
    private final androidx.cardview.widget.CardView mboundView0;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback108;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ItemCategoryBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 4, sIncludes, sViewsWithIds));
    }
    private ItemCategoryBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 1
            , (androidx.appcompat.widget.AppCompatImageView) bindings[1]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[3]
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[2]
            );
        this.imgHomeItem.setTag(null);
        this.mboundView0 = (androidx.cardview.widget.CardView) bindings[0];
        this.mboundView0.setTag(null);
        this.tvDesc.setTag(null);
        this.tvHomeItem.setTag(null);
        setRootTag(root);
        // listeners
        mCallback108 = new grand.app.aber_user.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.itemPostViewModel == variableId) {
            setItemPostViewModel((grand.app.aber_user.pages.home.viewModels.ItemMainViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setItemPostViewModel(@Nullable grand.app.aber_user.pages.home.viewModels.ItemMainViewModel ItemPostViewModel) {
        updateRegistration(0, ItemPostViewModel);
        this.mItemPostViewModel = ItemPostViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.itemPostViewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeItemPostViewModel((grand.app.aber_user.pages.home.viewModels.ItemMainViewModel) object, fieldId);
        }
        return false;
    }
    private boolean onChangeItemPostViewModel(grand.app.aber_user.pages.home.viewModels.ItemMainViewModel ItemPostViewModel, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        else if (fieldId == BR.services) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        grand.app.aber_user.pages.home.viewModels.ItemMainViewModel itemPostViewModel = mItemPostViewModel;
        grand.app.aber_user.pages.home.models.HomeServices itemPostViewModelServices = null;
        java.lang.String itemPostViewModelServicesName = null;
        java.lang.String itemPostViewModelServicesImage = null;
        java.lang.String itemPostViewModelServicesDescription = null;

        if ((dirtyFlags & 0x7L) != 0) {



                if (itemPostViewModel != null) {
                    // read itemPostViewModel.services
                    itemPostViewModelServices = itemPostViewModel.getServices();
                }


                if (itemPostViewModelServices != null) {
                    // read itemPostViewModel.services.name
                    itemPostViewModelServicesName = itemPostViewModelServices.getName();
                    // read itemPostViewModel.services.image
                    itemPostViewModelServicesImage = itemPostViewModelServices.getImage();
                    // read itemPostViewModel.services.description
                    itemPostViewModelServicesDescription = itemPostViewModelServices.getDescription();
                }
        }
        // batch finished
        if ((dirtyFlags & 0x7L) != 0) {
            // api target 1

            grand.app.aber_user.base.ApplicationBinding.loadMarketImage(this.imgHomeItem, itemPostViewModelServicesImage);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvDesc, itemPostViewModelServicesDescription);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvHomeItem, itemPostViewModelServicesName);
        }
        if ((dirtyFlags & 0x4L) != 0) {
            // api target 1

            this.mboundView0.setOnClickListener(mCallback108);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        // localize variables for thread safety
        // itemPostViewModel
        grand.app.aber_user.pages.home.viewModels.ItemMainViewModel itemPostViewModel = mItemPostViewModel;
        // itemPostViewModel != null
        boolean itemPostViewModelJavaLangObjectNull = false;



        itemPostViewModelJavaLangObjectNull = (itemPostViewModel) != (null);
        if (itemPostViewModelJavaLangObjectNull) {




            itemPostViewModel.itemAction(grand.app.aber_user.utils.Constants.SERVICE_DETAILS);
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): itemPostViewModel
        flag 1 (0x2L): itemPostViewModel.services
        flag 2 (0x3L): null
    flag mapping end*/
    //end
}