package grand.app.aber_user.databinding;
import grand.app.aber_user.R;
import grand.app.aber_user.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ItemCarBindingImpl extends ItemCarBinding implements grand.app.aber_user.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.v, 3);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback182;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ItemCarBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 4, sIncludes, sViewsWithIds));
    }
    private ItemCarBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 1
            , (androidx.appcompat.widget.AppCompatImageView) bindings[1]
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[2]
            , (android.view.View) bindings[3]
            );
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.radio.setTag(null);
        this.tvCar.setTag(null);
        setRootTag(root);
        // listeners
        mCallback182 = new grand.app.aber_user.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.itemViewModel == variableId) {
            setItemViewModel((grand.app.aber_user.pages.parts.viewModels.ItemCarViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setItemViewModel(@Nullable grand.app.aber_user.pages.parts.viewModels.ItemCarViewModel ItemViewModel) {
        updateRegistration(0, ItemViewModel);
        this.mItemViewModel = ItemViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.itemViewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeItemViewModel((grand.app.aber_user.pages.parts.viewModels.ItemCarViewModel) object, fieldId);
        }
        return false;
    }
    private boolean onChangeItemViewModel(grand.app.aber_user.pages.parts.viewModels.ItemCarViewModel ItemViewModel, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        else if (fieldId == BR.downsObject) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        grand.app.aber_user.pages.parts.viewModels.ItemCarViewModel itemViewModel = mItemViewModel;
        boolean itemViewModelDownsObjectCheckedBooleanTrue = false;
        android.graphics.drawable.Drawable itemViewModelDownsObjectCheckedBooleanTrueRadioAndroidDrawableIcCheckCarRadioAndroidDrawableIcButtonUnchecked = null;
        java.lang.String itemViewModelDownsObjectName = null;
        boolean itemViewModelDownsObjectChecked = false;
        grand.app.aber_user.model.DropDownsObject itemViewModelDownsObject = null;

        if ((dirtyFlags & 0x7L) != 0) {



                if (itemViewModel != null) {
                    // read itemViewModel.downsObject
                    itemViewModelDownsObject = itemViewModel.getDownsObject();
                }


                if (itemViewModelDownsObject != null) {
                    // read itemViewModel.downsObject.name
                    itemViewModelDownsObjectName = itemViewModelDownsObject.getName();
                    // read itemViewModel.downsObject.checked
                    itemViewModelDownsObjectChecked = itemViewModelDownsObject.isChecked();
                }


                // read itemViewModel.downsObject.checked == true
                itemViewModelDownsObjectCheckedBooleanTrue = (itemViewModelDownsObjectChecked) == (true);
            if((dirtyFlags & 0x7L) != 0) {
                if(itemViewModelDownsObjectCheckedBooleanTrue) {
                        dirtyFlags |= 0x10L;
                }
                else {
                        dirtyFlags |= 0x8L;
                }
            }


                // read itemViewModel.downsObject.checked == true ? @android:drawable/ic_check_car : @android:drawable/ic_button_unchecked
                itemViewModelDownsObjectCheckedBooleanTrueRadioAndroidDrawableIcCheckCarRadioAndroidDrawableIcButtonUnchecked = ((itemViewModelDownsObjectCheckedBooleanTrue) ? (androidx.appcompat.content.res.AppCompatResources.getDrawable(radio.getContext(), R.drawable.ic_check_car)) : (androidx.appcompat.content.res.AppCompatResources.getDrawable(radio.getContext(), R.drawable.ic_button_unchecked)));
        }
        // batch finished
        if ((dirtyFlags & 0x4L) != 0) {
            // api target 1

            this.mboundView0.setOnClickListener(mCallback182);
        }
        if ((dirtyFlags & 0x7L) != 0) {
            // api target 1

            androidx.databinding.adapters.ImageViewBindingAdapter.setImageDrawable(this.radio, itemViewModelDownsObjectCheckedBooleanTrueRadioAndroidDrawableIcCheckCarRadioAndroidDrawableIcButtonUnchecked);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvCar, itemViewModelDownsObjectName);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        // localize variables for thread safety
        // itemViewModel
        grand.app.aber_user.pages.parts.viewModels.ItemCarViewModel itemViewModel = mItemViewModel;
        // itemViewModel != null
        boolean itemViewModelJavaLangObjectNull = false;



        itemViewModelJavaLangObjectNull = (itemViewModel) != (null);
        if (itemViewModelJavaLangObjectNull) {




            itemViewModel.itemAction(grand.app.aber_user.utils.Constants.SIZE);
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): itemViewModel
        flag 1 (0x2L): itemViewModel.downsObject
        flag 2 (0x3L): null
        flag 3 (0x4L): itemViewModel.downsObject.checked == true ? @android:drawable/ic_check_car : @android:drawable/ic_button_unchecked
        flag 4 (0x5L): itemViewModel.downsObject.checked == true ? @android:drawable/ic_check_car : @android:drawable/ic_button_unchecked
    flag mapping end*/
    //end
}