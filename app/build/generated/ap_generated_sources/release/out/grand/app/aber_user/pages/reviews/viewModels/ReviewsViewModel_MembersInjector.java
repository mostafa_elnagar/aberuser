// Generated by Dagger (https://dagger.dev).
package grand.app.aber_user.pages.reviews.viewModels;

import dagger.MembersInjector;
import dagger.internal.InjectedFieldSignature;
import grand.app.aber_user.repository.ServicesRepository;
import javax.inject.Provider;

@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class ReviewsViewModel_MembersInjector implements MembersInjector<ReviewsViewModel> {
  private final Provider<ServicesRepository> servicesRepositoryProvider;

  public ReviewsViewModel_MembersInjector(Provider<ServicesRepository> servicesRepositoryProvider) {
    this.servicesRepositoryProvider = servicesRepositoryProvider;
  }

  public static MembersInjector<ReviewsViewModel> create(
      Provider<ServicesRepository> servicesRepositoryProvider) {
    return new ReviewsViewModel_MembersInjector(servicesRepositoryProvider);
  }

  @Override
  public void injectMembers(ReviewsViewModel instance) {
    injectServicesRepository(instance, servicesRepositoryProvider.get());
  }

  @InjectedFieldSignature("grand.app.aber_user.pages.reviews.viewModels.ReviewsViewModel.servicesRepository")
  public static void injectServicesRepository(ReviewsViewModel instance,
      ServicesRepository servicesRepository) {
    instance.servicesRepository = servicesRepository;
  }
}
