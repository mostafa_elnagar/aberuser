package grand.app.aber_user.databinding;
import grand.app.aber_user.R;
import grand.app.aber_user.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentServicesOrdersBindingImpl extends FragmentServicesOrdersBinding implements grand.app.aber_user.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.curve, 5);
    }
    // views
    @NonNull
    private final androidx.core.widget.NestedScrollView mboundView0;
    @NonNull
    private final com.google.android.material.button.MaterialButton mboundView2;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback217;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentServicesOrdersBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 6, sIncludes, sViewsWithIds));
    }
    private FragmentServicesOrdersBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 3
            , (androidx.appcompat.widget.AppCompatImageView) bindings[5]
            , (androidx.recyclerview.widget.RecyclerView) bindings[3]
            , (com.google.android.material.progressindicator.CircularProgressIndicator) bindings[4]
            , (androidx.recyclerview.widget.RecyclerView) bindings[1]
            );
        this.frameProfile.setTag(null);
        this.mboundView0 = (androidx.core.widget.NestedScrollView) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView2 = (com.google.android.material.button.MaterialButton) bindings[2];
        this.mboundView2.setTag(null);
        this.progress.setTag(null);
        this.rcServices.setTag(null);
        setRootTag(root);
        // listeners
        mCallback217 = new grand.app.aber_user.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x20L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.viewModel == variableId) {
            setViewModel((grand.app.aber_user.pages.myOrders.viewModels.MyOrdersServicesViewModels) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setViewModel(@Nullable grand.app.aber_user.pages.myOrders.viewModels.MyOrdersServicesViewModels ViewModel) {
        updateRegistration(1, ViewModel);
        this.mViewModel = ViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.viewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeViewModelSearchProgressVisible((androidx.databinding.ObservableBoolean) object, fieldId);
            case 1 :
                return onChangeViewModel((grand.app.aber_user.pages.myOrders.viewModels.MyOrdersServicesViewModels) object, fieldId);
            case 2 :
                return onChangeViewModelOrderStatus((androidx.databinding.ObservableBoolean) object, fieldId);
        }
        return false;
    }
    private boolean onChangeViewModelSearchProgressVisible(androidx.databinding.ObservableBoolean ViewModelSearchProgressVisible, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModel(grand.app.aber_user.pages.myOrders.viewModels.MyOrdersServicesViewModels ViewModel, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        else if (fieldId == BR.homeServicesAdapter) {
            synchronized(this) {
                    mDirtyFlags |= 0x8L;
            }
            return true;
        }
        else if (fieldId == BR.myOrdersAdapter) {
            synchronized(this) {
                    mDirtyFlags |= 0x10L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelOrderStatus(androidx.databinding.ObservableBoolean ViewModelOrderStatus, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        boolean viewModelSearchProgressVisibleGet = false;
        int viewModelSearchProgressVisibleViewVISIBLEViewGONE = 0;
        java.lang.String viewModelOrderStatusBooleanFalseMboundView2AndroidStringCurrentMboundView2AndroidStringPrevious = null;
        grand.app.aber_user.pages.myOrders.adapter.MyServicesOrdersAdapter viewModelMyOrdersAdapter = null;
        boolean viewModelOrderStatusGet = false;
        boolean viewModelOrderStatusBooleanFalse = false;
        androidx.databinding.ObservableBoolean viewModelSearchProgressVisible = null;
        grand.app.aber_user.pages.myOrders.viewModels.MyOrdersServicesViewModels viewModel = mViewModel;
        androidx.databinding.ObservableBoolean viewModelOrderStatus = null;
        grand.app.aber_user.pages.myOrders.adapter.HomeServicesAdapter viewModelHomeServicesAdapter = null;

        if ((dirtyFlags & 0x3fL) != 0) {


            if ((dirtyFlags & 0x32L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.myOrdersAdapter
                        viewModelMyOrdersAdapter = viewModel.getMyOrdersAdapter();
                    }
            }
            if ((dirtyFlags & 0x23L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.searchProgressVisible
                        viewModelSearchProgressVisible = viewModel.searchProgressVisible;
                    }
                    updateRegistration(0, viewModelSearchProgressVisible);


                    if (viewModelSearchProgressVisible != null) {
                        // read viewModel.searchProgressVisible.get()
                        viewModelSearchProgressVisibleGet = viewModelSearchProgressVisible.get();
                    }
                if((dirtyFlags & 0x23L) != 0) {
                    if(viewModelSearchProgressVisibleGet) {
                            dirtyFlags |= 0x80L;
                    }
                    else {
                            dirtyFlags |= 0x40L;
                    }
                }


                    // read viewModel.searchProgressVisible.get() ? View.VISIBLE : View.GONE
                    viewModelSearchProgressVisibleViewVISIBLEViewGONE = ((viewModelSearchProgressVisibleGet) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
            if ((dirtyFlags & 0x26L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.orderStatus
                        viewModelOrderStatus = viewModel.orderStatus;
                    }
                    updateRegistration(2, viewModelOrderStatus);


                    if (viewModelOrderStatus != null) {
                        // read viewModel.orderStatus.get()
                        viewModelOrderStatusGet = viewModelOrderStatus.get();
                    }


                    // read viewModel.orderStatus.get() == false
                    viewModelOrderStatusBooleanFalse = (viewModelOrderStatusGet) == (false);
                if((dirtyFlags & 0x26L) != 0) {
                    if(viewModelOrderStatusBooleanFalse) {
                            dirtyFlags |= 0x200L;
                    }
                    else {
                            dirtyFlags |= 0x100L;
                    }
                }


                    // read viewModel.orderStatus.get() == false ? @android:string/current : @android:string/previous
                    viewModelOrderStatusBooleanFalseMboundView2AndroidStringCurrentMboundView2AndroidStringPrevious = ((viewModelOrderStatusBooleanFalse) ? (mboundView2.getResources().getString(R.string.current)) : (mboundView2.getResources().getString(R.string.previous)));
            }
            if ((dirtyFlags & 0x2aL) != 0) {

                    if (viewModel != null) {
                        // read viewModel.homeServicesAdapter
                        viewModelHomeServicesAdapter = viewModel.getHomeServicesAdapter();
                    }
            }
        }
        // batch finished
        if ((dirtyFlags & 0x32L) != 0) {
            // api target 1

            grand.app.aber_user.base.ApplicationBinding.getItemsV2Binding(this.frameProfile, viewModelMyOrdersAdapter, "1", "1");
        }
        if ((dirtyFlags & 0x20L) != 0) {
            // api target 1

            this.mboundView2.setOnClickListener(mCallback217);
        }
        if ((dirtyFlags & 0x26L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView2, viewModelOrderStatusBooleanFalseMboundView2AndroidStringCurrentMboundView2AndroidStringPrevious);
        }
        if ((dirtyFlags & 0x23L) != 0) {
            // api target 1

            this.progress.setVisibility(viewModelSearchProgressVisibleViewVISIBLEViewGONE);
        }
        if ((dirtyFlags & 0x2aL) != 0) {
            // api target 1

            grand.app.aber_user.base.ApplicationBinding.getItemsV2Binding(this.rcServices, viewModelHomeServicesAdapter, "1", "2");
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        // localize variables for thread safety
        // viewModel.orderStatus.get()
        boolean viewModelOrderStatusGet = false;
        // viewModel.orderStatus.get() == true
        boolean viewModelOrderStatusBooleanTrue = false;
        // viewModel
        grand.app.aber_user.pages.myOrders.viewModels.MyOrdersServicesViewModels viewModel = mViewModel;
        // viewModel.orderStatus
        androidx.databinding.ObservableBoolean viewModelOrderStatus = null;
        // viewModel.orderStatus != null
        boolean viewModelOrderStatusJavaLangObjectNull = false;
        // viewModel.orderStatus.get() == true ? 0 : 1
        int viewModelOrderStatusBooleanTrueInt0Int1 = 0;
        // viewModel != null
        boolean viewModelJavaLangObjectNull = false;



        viewModelJavaLangObjectNull = (viewModel) != (null);
        if (viewModelJavaLangObjectNull) {




            viewModelOrderStatus = viewModel.orderStatus;

            viewModelOrderStatusJavaLangObjectNull = (viewModelOrderStatus) != (null);
            if (viewModelOrderStatusJavaLangObjectNull) {


                viewModelOrderStatusGet = viewModelOrderStatus.get();


                viewModelOrderStatusBooleanTrue = (viewModelOrderStatusGet) == (true);
                if (viewModelOrderStatusBooleanTrue) {



                    viewModelOrderStatusBooleanTrueInt0Int1 = 0;



                    viewModel.myOrders(viewModelOrderStatusBooleanTrueInt0Int1, 1, true);
                }
                else {



                    viewModelOrderStatusBooleanTrueInt0Int1 = 1;



                    viewModel.myOrders(viewModelOrderStatusBooleanTrueInt0Int1, 1, true);
                }
            }
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): viewModel.searchProgressVisible
        flag 1 (0x2L): viewModel
        flag 2 (0x3L): viewModel.orderStatus
        flag 3 (0x4L): viewModel.homeServicesAdapter
        flag 4 (0x5L): viewModel.myOrdersAdapter
        flag 5 (0x6L): null
        flag 6 (0x7L): viewModel.searchProgressVisible.get() ? View.VISIBLE : View.GONE
        flag 7 (0x8L): viewModel.searchProgressVisible.get() ? View.VISIBLE : View.GONE
        flag 8 (0x9L): viewModel.orderStatus.get() == false ? @android:string/current : @android:string/previous
        flag 9 (0xaL): viewModel.orderStatus.get() == false ? @android:string/current : @android:string/previous
    flag mapping end*/
    //end
}