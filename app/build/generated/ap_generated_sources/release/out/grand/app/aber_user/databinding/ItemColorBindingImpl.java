package grand.app.aber_user.databinding;
import grand.app.aber_user.R;
import grand.app.aber_user.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ItemColorBindingImpl extends ItemColorBinding implements grand.app.aber_user.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = null;
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    @NonNull
    private final androidx.appcompat.widget.AppCompatImageView mboundView2;
    @NonNull
    private final grand.app.aber_user.customViews.views.CustomTextViewRegular mboundView3;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback177;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ItemColorBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 4, sIncludes, sViewsWithIds));
    }
    private ItemColorBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 1
            , (de.hdodenhof.circleimageview.CircleImageView) bindings[1]
            );
        this.icColor.setTag(null);
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView2 = (androidx.appcompat.widget.AppCompatImageView) bindings[2];
        this.mboundView2.setTag(null);
        this.mboundView3 = (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[3];
        this.mboundView3.setTag(null);
        setRootTag(root);
        // listeners
        mCallback177 = new grand.app.aber_user.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.itemViewModel == variableId) {
            setItemViewModel((grand.app.aber_user.pages.parts.viewModels.ItemProductColorsViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setItemViewModel(@Nullable grand.app.aber_user.pages.parts.viewModels.ItemProductColorsViewModel ItemViewModel) {
        updateRegistration(0, ItemViewModel);
        this.mItemViewModel = ItemViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.itemViewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeItemViewModel((grand.app.aber_user.pages.parts.viewModels.ItemProductColorsViewModel) object, fieldId);
        }
        return false;
    }
    private boolean onChangeItemViewModel(grand.app.aber_user.pages.parts.viewModels.ItemProductColorsViewModel ItemViewModel, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        else if (fieldId == BR.colorItem) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        boolean itemViewModelColorItemChecked = false;
        grand.app.aber_user.pages.parts.viewModels.ItemProductColorsViewModel itemViewModel = mItemViewModel;
        java.lang.String itemViewModelColorItemValue = null;
        int itemViewModelColorItemCheckedViewVISIBLEViewGONE = 0;
        java.lang.String itemViewModelColorItemName = null;
        grand.app.aber_user.pages.parts.models.productDetails.AttributesItem itemViewModelColorItem = null;

        if ((dirtyFlags & 0x7L) != 0) {



                if (itemViewModel != null) {
                    // read itemViewModel.colorItem
                    itemViewModelColorItem = itemViewModel.getColorItem();
                }


                if (itemViewModelColorItem != null) {
                    // read itemViewModel.colorItem.checked
                    itemViewModelColorItemChecked = itemViewModelColorItem.isChecked();
                    // read itemViewModel.colorItem.value
                    itemViewModelColorItemValue = itemViewModelColorItem.getValue();
                    // read itemViewModel.colorItem.name
                    itemViewModelColorItemName = itemViewModelColorItem.getName();
                }
            if((dirtyFlags & 0x7L) != 0) {
                if(itemViewModelColorItemChecked) {
                        dirtyFlags |= 0x10L;
                }
                else {
                        dirtyFlags |= 0x8L;
                }
            }


                // read itemViewModel.colorItem.checked ? View.VISIBLE : View.GONE
                itemViewModelColorItemCheckedViewVISIBLEViewGONE = ((itemViewModelColorItemChecked) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
        }
        // batch finished
        if ((dirtyFlags & 0x7L) != 0) {
            // api target 1

            grand.app.aber_user.base.ApplicationBinding.loadColorImage(this.icColor, itemViewModelColorItemValue);
            this.mboundView2.setVisibility(itemViewModelColorItemCheckedViewVISIBLEViewGONE);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView3, itemViewModelColorItemName);
        }
        if ((dirtyFlags & 0x4L) != 0) {
            // api target 1

            this.mboundView0.setOnClickListener(mCallback177);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        // localize variables for thread safety
        // itemViewModel
        grand.app.aber_user.pages.parts.viewModels.ItemProductColorsViewModel itemViewModel = mItemViewModel;
        // itemViewModel != null
        boolean itemViewModelJavaLangObjectNull = false;



        itemViewModelJavaLangObjectNull = (itemViewModel) != (null);
        if (itemViewModelJavaLangObjectNull) {


            itemViewModel.itemAction();
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): itemViewModel
        flag 1 (0x2L): itemViewModel.colorItem
        flag 2 (0x3L): null
        flag 3 (0x4L): itemViewModel.colorItem.checked ? View.VISIBLE : View.GONE
        flag 4 (0x5L): itemViewModel.colorItem.checked ? View.VISIBLE : View.GONE
    flag mapping end*/
    //end
}