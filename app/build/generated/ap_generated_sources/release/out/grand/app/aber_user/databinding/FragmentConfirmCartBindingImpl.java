package grand.app.aber_user.databinding;
import grand.app.aber_user.R;
import grand.app.aber_user.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentConfirmCartBindingImpl extends FragmentConfirmCartBinding implements grand.app.aber_user.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.input_message, 12);
        sViewsWithIds.put(R.id.input_promo, 13);
        sViewsWithIds.put(R.id.tv_services_cost, 14);
        sViewsWithIds.put(R.id.v_services_price, 15);
        sViewsWithIds.put(R.id.tv_services_extra_cost, 16);
        sViewsWithIds.put(R.id.v_services__extra_price, 17);
        sViewsWithIds.put(R.id.tv_delivery_cost, 18);
        sViewsWithIds.put(R.id.v_delivery_price, 19);
        sViewsWithIds.put(R.id.tv_total, 20);
    }
    // views
    @NonNull
    private final androidx.core.widget.NestedScrollView mboundView0;
    @NonNull
    private final com.google.android.material.textfield.TextInputEditText mboundView4;
    @NonNull
    private final com.google.android.material.textfield.TextInputEditText mboundView6;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback180;
    @Nullable
    private final android.view.View.OnClickListener mCallback178;
    @Nullable
    private final android.view.View.OnClickListener mCallback179;
    // values
    // listeners
    // Inverse Binding Event Handlers
    private androidx.databinding.InverseBindingListener mboundView4androidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewmodel.newOrderRequest.description
            //         is viewmodel.newOrderRequest.setDescription((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(mboundView4);
            // localize variables for thread safety
            // viewmodel.newOrderRequest != null
            boolean viewmodelNewOrderRequestJavaLangObjectNull = false;
            // viewmodel.newOrderRequest.description
            java.lang.String viewmodelNewOrderRequestDescription = null;
            // viewmodel != null
            boolean viewmodelJavaLangObjectNull = false;
            // viewmodel.newOrderRequest
            grand.app.aber_user.pages.cart.models.NewOrderRequest viewmodelNewOrderRequest = null;
            // viewmodel
            grand.app.aber_user.pages.cart.viewModels.CartViewModel viewmodel = mViewmodel;



            viewmodelJavaLangObjectNull = (viewmodel) != (null);
            if (viewmodelJavaLangObjectNull) {


                viewmodelNewOrderRequest = viewmodel.getNewOrderRequest();

                viewmodelNewOrderRequestJavaLangObjectNull = (viewmodelNewOrderRequest) != (null);
                if (viewmodelNewOrderRequestJavaLangObjectNull) {




                    viewmodelNewOrderRequest.setDescription(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener mboundView6androidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewmodel.checkPromoRequest.code
            //         is viewmodel.checkPromoRequest.setCode((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(mboundView6);
            // localize variables for thread safety
            // viewmodel.checkPromoRequest.code
            java.lang.String viewmodelCheckPromoRequestCode = null;
            // viewmodel != null
            boolean viewmodelJavaLangObjectNull = false;
            // viewmodel.checkPromoRequest
            grand.app.aber_user.pages.cart.models.CheckPromoRequest viewmodelCheckPromoRequest = null;
            // viewmodel.checkPromoRequest != null
            boolean viewmodelCheckPromoRequestJavaLangObjectNull = false;
            // viewmodel
            grand.app.aber_user.pages.cart.viewModels.CartViewModel viewmodel = mViewmodel;



            viewmodelJavaLangObjectNull = (viewmodel) != (null);
            if (viewmodelJavaLangObjectNull) {


                viewmodelCheckPromoRequest = viewmodel.getCheckPromoRequest();

                viewmodelCheckPromoRequestJavaLangObjectNull = (viewmodelCheckPromoRequest) != (null);
                if (viewmodelCheckPromoRequestJavaLangObjectNull) {




                    viewmodelCheckPromoRequest.setCode(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };

    public FragmentConfirmCartBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 21, sIncludes, sViewsWithIds));
    }
    private FragmentConfirmCartBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 2
            , (com.google.android.material.button.MaterialButton) bindings[5]
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[2]
            , (com.google.android.material.button.MaterialButton) bindings[11]
            , (com.google.android.material.textfield.TextInputLayout) bindings[12]
            , (com.google.android.material.textfield.TextInputLayout) bindings[13]
            , (androidx.recyclerview.widget.RecyclerView) bindings[1]
            , (androidx.recyclerview.widget.RecyclerView) bindings[3]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[18]
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[9]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[14]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[16]
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[8]
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[7]
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[20]
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[10]
            , (android.view.View) bindings[19]
            , (android.view.View) bindings[17]
            , (android.view.View) bindings[15]
            );
        this.activePromo.setTag(null);
        this.addAddress.setTag(null);
        this.btnSaveCart.setTag(null);
        this.mboundView0 = (androidx.core.widget.NestedScrollView) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView4 = (com.google.android.material.textfield.TextInputEditText) bindings[4];
        this.mboundView4.setTag(null);
        this.mboundView6 = (com.google.android.material.textfield.TextInputEditText) bindings[6];
        this.mboundView6.setTag(null);
        this.rcLocation.setTag(null);
        this.recCart.setTag(null);
        this.tvDeliveryPrice.setTag(null);
        this.tvServicesExtraPrice.setTag(null);
        this.tvServicesPrice.setTag(null);
        this.tvTotalPrice.setTag(null);
        setRootTag(root);
        // listeners
        mCallback180 = new grand.app.aber_user.generated.callback.OnClickListener(this, 3);
        mCallback178 = new grand.app.aber_user.generated.callback.OnClickListener(this, 1);
        mCallback179 = new grand.app.aber_user.generated.callback.OnClickListener(this, 2);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x40L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.viewmodel == variableId) {
            setViewmodel((grand.app.aber_user.pages.cart.viewModels.CartViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setViewmodel(@Nullable grand.app.aber_user.pages.cart.viewModels.CartViewModel Viewmodel) {
        updateRegistration(1, Viewmodel);
        this.mViewmodel = Viewmodel;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.viewmodel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeViewmodelDiscount((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 1 :
                return onChangeViewmodel((grand.app.aber_user.pages.cart.viewModels.CartViewModel) object, fieldId);
        }
        return false;
    }
    private boolean onChangeViewmodelDiscount(androidx.databinding.ObservableField<java.lang.String> ViewmodelDiscount, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewmodel(grand.app.aber_user.pages.cart.viewModels.CartViewModel Viewmodel, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        else if (fieldId == BR.locationsAdapters) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        else if (fieldId == BR.cartAdapter) {
            synchronized(this) {
                    mDirtyFlags |= 0x8L;
            }
            return true;
        }
        else if (fieldId == BR.newOrderRequest) {
            synchronized(this) {
                    mDirtyFlags |= 0x10L;
            }
            return true;
        }
        else if (fieldId == BR.checkPromoRequest) {
            synchronized(this) {
                    mDirtyFlags |= 0x20L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String textUtilsIsEmptyViewmodelNewOrderRequestDeliveryFeeViewmodelNewOrderRequestDeliveryFeeConcatJavaLangStringConcatViewmodelNewOrderRequestCurrencyJavaLangString0ConcatViewmodelNewOrderRequestCurrency = null;
        grand.app.aber_user.pages.cart.models.CheckPromoRequest viewmodelCheckPromoRequest = null;
        java.lang.String viewmodelNewOrderRequestDiscountConcatJavaLangStringConcatViewmodelDiscount = null;
        java.lang.String viewmodelNewOrderRequestTotalCartConcatJavaLangStringConcatViewmodelNewOrderRequestCurrency = null;
        boolean textUtilsIsEmptyViewmodelNewOrderRequestTotal = false;
        java.lang.String viewmodelNewOrderRequestTotalConcatJavaLangString = null;
        java.lang.String viewmodelNewOrderRequestTotalCartConcatJavaLangString = null;
        boolean textUtilsIsEmptyViewmodelNewOrderRequestDeliveryFee = false;
        java.lang.String viewmodelNewOrderRequestTotalCart = null;
        java.lang.String textUtilsIsEmptyViewmodelNewOrderRequestTotalViewmodelNewOrderRequestTotalConcatJavaLangStringConcatViewmodelNewOrderRequestCurrencyJavaLangString0ConcatViewmodelNewOrderRequestCurrency = null;
        boolean textUtilsIsEmptyViewmodelDiscount = false;
        java.lang.String viewmodelNewOrderRequestCurrency = null;
        java.lang.String viewmodelNewOrderRequestDeliveryFeeConcatJavaLangString = null;
        boolean textUtilsIsEmptyViewmodelDiscountBooleanFalseBooleanTrue = false;
        boolean textUtilsIsEmptyViewmodelNewOrderRequestDiscount = false;
        grand.app.aber_user.pages.cart.models.NewOrderRequest viewmodelNewOrderRequest = null;
        androidx.databinding.ObservableField<java.lang.String> viewmodelDiscount = null;
        java.lang.String viewmodelNewOrderRequestTotalConcatJavaLangStringConcatViewmodelNewOrderRequestCurrency = null;
        java.lang.String textUtilsIsEmptyViewmodelNewOrderRequestDiscountViewmodelNewOrderRequestDiscountConcatJavaLangStringConcatViewmodelDiscountJavaLangString0ConcatViewmodelNewOrderRequestCurrency = null;
        grand.app.aber_user.pages.cart.adapters.CartAdapter viewmodelCartAdapter = null;
        boolean TextUtilsIsEmptyViewmodelNewOrderRequestTotal1 = false;
        java.lang.String viewmodelNewOrderRequestDiscount = null;
        java.lang.String viewmodelDiscountGet = null;
        java.lang.String viewmodelNewOrderRequestDescription = null;
        java.lang.String javaLangString0ConcatViewmodelNewOrderRequestCurrency = null;
        java.lang.String viewmodelCheckPromoRequestCode = null;
        java.lang.String viewmodelNewOrderRequestDeliveryFee = null;
        java.lang.String viewmodelNewOrderRequestDeliveryFeeConcatJavaLangStringConcatViewmodelNewOrderRequestCurrency = null;
        boolean TextUtilsIsEmptyViewmodelNewOrderRequestDiscount1 = false;
        grand.app.aber_user.pages.myLocations.adapters.LocationsAdapters viewmodelLocationsAdapters = null;
        java.lang.String viewmodelNewOrderRequestTotal = null;
        boolean TextUtilsIsEmptyViewmodelNewOrderRequestDeliveryFee1 = false;
        java.lang.String JavaLangString0ConcatViewmodelNewOrderRequestCurrency1 = null;
        java.lang.String viewmodelNewOrderRequestDiscountConcatJavaLangString = null;
        grand.app.aber_user.pages.cart.viewModels.CartViewModel viewmodel = mViewmodel;
        boolean TextUtilsIsEmptyViewmodelDiscount1 = false;

        if ((dirtyFlags & 0x7fL) != 0) {


            if ((dirtyFlags & 0x62L) != 0) {

                    if (viewmodel != null) {
                        // read viewmodel.checkPromoRequest
                        viewmodelCheckPromoRequest = viewmodel.getCheckPromoRequest();
                    }


                    if (viewmodelCheckPromoRequest != null) {
                        // read viewmodel.checkPromoRequest.code
                        viewmodelCheckPromoRequestCode = viewmodelCheckPromoRequest.getCode();
                    }
            }
            if ((dirtyFlags & 0x53L) != 0) {

                    if (viewmodel != null) {
                        // read viewmodel.newOrderRequest
                        viewmodelNewOrderRequest = viewmodel.getNewOrderRequest();
                    }

                if ((dirtyFlags & 0x52L) != 0) {

                        if (viewmodelNewOrderRequest != null) {
                            // read viewmodel.newOrderRequest.totalCart
                            viewmodelNewOrderRequestTotalCart = viewmodelNewOrderRequest.getTotalCart();
                            // read viewmodel.newOrderRequest.currency
                            viewmodelNewOrderRequestCurrency = viewmodelNewOrderRequest.getCurrency();
                            // read viewmodel.newOrderRequest.description
                            viewmodelNewOrderRequestDescription = viewmodelNewOrderRequest.getDescription();
                            // read viewmodel.newOrderRequest.deliveryFee
                            viewmodelNewOrderRequestDeliveryFee = viewmodelNewOrderRequest.getDeliveryFee();
                            // read viewmodel.newOrderRequest.total
                            viewmodelNewOrderRequestTotal = viewmodelNewOrderRequest.getTotal();
                        }


                        if (viewmodelNewOrderRequestTotalCart != null) {
                            // read viewmodel.newOrderRequest.totalCart.concat(" ")
                            viewmodelNewOrderRequestTotalCartConcatJavaLangString = viewmodelNewOrderRequestTotalCart.concat(" ");
                        }
                        // read TextUtils.isEmpty(viewmodel.newOrderRequest.deliveryFee)
                        TextUtilsIsEmptyViewmodelNewOrderRequestDeliveryFee1 = android.text.TextUtils.isEmpty(viewmodelNewOrderRequestDeliveryFee);
                        // read TextUtils.isEmpty(viewmodel.newOrderRequest.total)
                        textUtilsIsEmptyViewmodelNewOrderRequestTotal = android.text.TextUtils.isEmpty(viewmodelNewOrderRequestTotal);


                        if (viewmodelNewOrderRequestTotalCartConcatJavaLangString != null) {
                            // read viewmodel.newOrderRequest.totalCart.concat(" ").concat(viewmodel.newOrderRequest.currency)
                            viewmodelNewOrderRequestTotalCartConcatJavaLangStringConcatViewmodelNewOrderRequestCurrency = viewmodelNewOrderRequestTotalCartConcatJavaLangString.concat(viewmodelNewOrderRequestCurrency);
                        }
                        // read !TextUtils.isEmpty(viewmodel.newOrderRequest.deliveryFee)
                        textUtilsIsEmptyViewmodelNewOrderRequestDeliveryFee = !TextUtilsIsEmptyViewmodelNewOrderRequestDeliveryFee1;
                        // read !TextUtils.isEmpty(viewmodel.newOrderRequest.total)
                        TextUtilsIsEmptyViewmodelNewOrderRequestTotal1 = !textUtilsIsEmptyViewmodelNewOrderRequestTotal;
                    if((dirtyFlags & 0x52L) != 0) {
                        if(textUtilsIsEmptyViewmodelNewOrderRequestDeliveryFee) {
                                dirtyFlags |= 0x100L;
                        }
                        else {
                                dirtyFlags |= 0x80L;
                        }
                    }
                    if((dirtyFlags & 0x52L) != 0) {
                        if(TextUtilsIsEmptyViewmodelNewOrderRequestTotal1) {
                                dirtyFlags |= 0x400L;
                        }
                        else {
                                dirtyFlags |= 0x200L;
                        }
                    }
                }

                    if (viewmodelNewOrderRequest != null) {
                        // read viewmodel.newOrderRequest.discount
                        viewmodelNewOrderRequestDiscount = viewmodelNewOrderRequest.getDiscount();
                    }


                    // read TextUtils.isEmpty(viewmodel.newOrderRequest.discount)
                    TextUtilsIsEmptyViewmodelNewOrderRequestDiscount1 = android.text.TextUtils.isEmpty(viewmodelNewOrderRequestDiscount);


                    // read !TextUtils.isEmpty(viewmodel.newOrderRequest.discount)
                    textUtilsIsEmptyViewmodelNewOrderRequestDiscount = !TextUtilsIsEmptyViewmodelNewOrderRequestDiscount1;
                if((dirtyFlags & 0x53L) != 0) {
                    if(textUtilsIsEmptyViewmodelNewOrderRequestDiscount) {
                            dirtyFlags |= 0x4000L;
                    }
                    else {
                            dirtyFlags |= 0x2000L;
                    }
                }
            }
            if ((dirtyFlags & 0x43L) != 0) {

                    if (viewmodel != null) {
                        // read viewmodel.discount
                        viewmodelDiscount = viewmodel.discount;
                    }
                    updateRegistration(0, viewmodelDiscount);


                    if (viewmodelDiscount != null) {
                        // read viewmodel.discount.get()
                        viewmodelDiscountGet = viewmodelDiscount.get();
                    }


                    // read TextUtils.isEmpty(viewmodel.discount.get())
                    TextUtilsIsEmptyViewmodelDiscount1 = android.text.TextUtils.isEmpty(viewmodelDiscountGet);


                    // read !TextUtils.isEmpty(viewmodel.discount.get())
                    textUtilsIsEmptyViewmodelDiscount = !TextUtilsIsEmptyViewmodelDiscount1;
                if((dirtyFlags & 0x43L) != 0) {
                    if(textUtilsIsEmptyViewmodelDiscount) {
                            dirtyFlags |= 0x1000L;
                    }
                    else {
                            dirtyFlags |= 0x800L;
                    }
                }


                    // read !TextUtils.isEmpty(viewmodel.discount.get()) ? false : true
                    textUtilsIsEmptyViewmodelDiscountBooleanFalseBooleanTrue = ((textUtilsIsEmptyViewmodelDiscount) ? (false) : (true));
            }
            if ((dirtyFlags & 0x4aL) != 0) {

                    if (viewmodel != null) {
                        // read viewmodel.cartAdapter
                        viewmodelCartAdapter = viewmodel.getCartAdapter();
                    }
            }
            if ((dirtyFlags & 0x46L) != 0) {

                    if (viewmodel != null) {
                        // read viewmodel.locationsAdapters
                        viewmodelLocationsAdapters = viewmodel.getLocationsAdapters();
                    }
            }
        }
        // batch finished

        if ((dirtyFlags & 0x400L) != 0) {

                if (viewmodelNewOrderRequestTotal != null) {
                    // read viewmodel.newOrderRequest.total.concat(" ")
                    viewmodelNewOrderRequestTotalConcatJavaLangString = viewmodelNewOrderRequestTotal.concat(" ");
                }
        }
        if ((dirtyFlags & 0x2780L) != 0) {

                if (viewmodelNewOrderRequest != null) {
                    // read viewmodel.newOrderRequest.currency
                    viewmodelNewOrderRequestCurrency = viewmodelNewOrderRequest.getCurrency();
                }

            if ((dirtyFlags & 0x400L) != 0) {

                    if (viewmodelNewOrderRequestTotalConcatJavaLangString != null) {
                        // read viewmodel.newOrderRequest.total.concat(" ").concat(viewmodel.newOrderRequest.currency)
                        viewmodelNewOrderRequestTotalConcatJavaLangStringConcatViewmodelNewOrderRequestCurrency = viewmodelNewOrderRequestTotalConcatJavaLangString.concat(viewmodelNewOrderRequestCurrency);
                    }
            }
            if ((dirtyFlags & 0x2000L) != 0) {

                    // read " 0 ".concat(viewmodel.newOrderRequest.currency)
                    javaLangString0ConcatViewmodelNewOrderRequestCurrency = " 0 ".concat(viewmodelNewOrderRequestCurrency);
            }
            if ((dirtyFlags & 0x280L) != 0) {

                    // read "0 ".concat(viewmodel.newOrderRequest.currency)
                    JavaLangString0ConcatViewmodelNewOrderRequestCurrency1 = "0 ".concat(viewmodelNewOrderRequestCurrency);
            }
        }
        if ((dirtyFlags & 0x100L) != 0) {

                if (viewmodelNewOrderRequestDeliveryFee != null) {
                    // read viewmodel.newOrderRequest.deliveryFee.concat(" ")
                    viewmodelNewOrderRequestDeliveryFeeConcatJavaLangString = viewmodelNewOrderRequestDeliveryFee.concat(" ");
                }


                if (viewmodelNewOrderRequestDeliveryFeeConcatJavaLangString != null) {
                    // read viewmodel.newOrderRequest.deliveryFee.concat(" ").concat(viewmodel.newOrderRequest.currency)
                    viewmodelNewOrderRequestDeliveryFeeConcatJavaLangStringConcatViewmodelNewOrderRequestCurrency = viewmodelNewOrderRequestDeliveryFeeConcatJavaLangString.concat(viewmodelNewOrderRequestCurrency);
                }
        }
        if ((dirtyFlags & 0x4000L) != 0) {

                if (viewmodel != null) {
                    // read viewmodel.discount
                    viewmodelDiscount = viewmodel.discount;
                }
                updateRegistration(0, viewmodelDiscount);
                if (viewmodelNewOrderRequestDiscount != null) {
                    // read viewmodel.newOrderRequest.discount.concat(" ")
                    viewmodelNewOrderRequestDiscountConcatJavaLangString = viewmodelNewOrderRequestDiscount.concat(" ");
                }


                if (viewmodelDiscount != null) {
                    // read viewmodel.discount.get()
                    viewmodelDiscountGet = viewmodelDiscount.get();
                }


                if (viewmodelNewOrderRequestDiscountConcatJavaLangString != null) {
                    // read viewmodel.newOrderRequest.discount.concat(" ").concat(viewmodel.discount.get())
                    viewmodelNewOrderRequestDiscountConcatJavaLangStringConcatViewmodelDiscount = viewmodelNewOrderRequestDiscountConcatJavaLangString.concat(viewmodelDiscountGet);
                }
        }

        if ((dirtyFlags & 0x52L) != 0) {

                // read !TextUtils.isEmpty(viewmodel.newOrderRequest.deliveryFee) ? viewmodel.newOrderRequest.deliveryFee.concat(" ").concat(viewmodel.newOrderRequest.currency) : "0 ".concat(viewmodel.newOrderRequest.currency)
                textUtilsIsEmptyViewmodelNewOrderRequestDeliveryFeeViewmodelNewOrderRequestDeliveryFeeConcatJavaLangStringConcatViewmodelNewOrderRequestCurrencyJavaLangString0ConcatViewmodelNewOrderRequestCurrency = ((textUtilsIsEmptyViewmodelNewOrderRequestDeliveryFee) ? (viewmodelNewOrderRequestDeliveryFeeConcatJavaLangStringConcatViewmodelNewOrderRequestCurrency) : (JavaLangString0ConcatViewmodelNewOrderRequestCurrency1));
                // read !TextUtils.isEmpty(viewmodel.newOrderRequest.total) ? viewmodel.newOrderRequest.total.concat(" ").concat(viewmodel.newOrderRequest.currency) : "0 ".concat(viewmodel.newOrderRequest.currency)
                textUtilsIsEmptyViewmodelNewOrderRequestTotalViewmodelNewOrderRequestTotalConcatJavaLangStringConcatViewmodelNewOrderRequestCurrencyJavaLangString0ConcatViewmodelNewOrderRequestCurrency = ((TextUtilsIsEmptyViewmodelNewOrderRequestTotal1) ? (viewmodelNewOrderRequestTotalConcatJavaLangStringConcatViewmodelNewOrderRequestCurrency) : (JavaLangString0ConcatViewmodelNewOrderRequestCurrency1));
        }
        if ((dirtyFlags & 0x53L) != 0) {

                // read !TextUtils.isEmpty(viewmodel.newOrderRequest.discount) ? viewmodel.newOrderRequest.discount.concat(" ").concat(viewmodel.discount.get()) : " 0 ".concat(viewmodel.newOrderRequest.currency)
                textUtilsIsEmptyViewmodelNewOrderRequestDiscountViewmodelNewOrderRequestDiscountConcatJavaLangStringConcatViewmodelDiscountJavaLangString0ConcatViewmodelNewOrderRequestCurrency = ((textUtilsIsEmptyViewmodelNewOrderRequestDiscount) ? (viewmodelNewOrderRequestDiscountConcatJavaLangStringConcatViewmodelDiscount) : (javaLangString0ConcatViewmodelNewOrderRequestCurrency));
        }
        // batch finished
        if ((dirtyFlags & 0x40L) != 0) {
            // api target 1

            this.activePromo.setOnClickListener(mCallback179);
            this.addAddress.setOnClickListener(mCallback178);
            this.btnSaveCart.setOnClickListener(mCallback180);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.mboundView4, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, mboundView4androidTextAttrChanged);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.mboundView6, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, mboundView6androidTextAttrChanged);
        }
        if ((dirtyFlags & 0x43L) != 0) {
            // api target 1

            this.activePromo.setEnabled(textUtilsIsEmptyViewmodelDiscountBooleanFalseBooleanTrue);
            this.mboundView6.setEnabled(textUtilsIsEmptyViewmodelDiscountBooleanFalseBooleanTrue);
        }
        if ((dirtyFlags & 0x52L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView4, viewmodelNewOrderRequestDescription);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvServicesExtraPrice, textUtilsIsEmptyViewmodelNewOrderRequestDeliveryFeeViewmodelNewOrderRequestDeliveryFeeConcatJavaLangStringConcatViewmodelNewOrderRequestCurrencyJavaLangString0ConcatViewmodelNewOrderRequestCurrency);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvServicesPrice, viewmodelNewOrderRequestTotalCartConcatJavaLangStringConcatViewmodelNewOrderRequestCurrency);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvTotalPrice, textUtilsIsEmptyViewmodelNewOrderRequestTotalViewmodelNewOrderRequestTotalConcatJavaLangStringConcatViewmodelNewOrderRequestCurrencyJavaLangString0ConcatViewmodelNewOrderRequestCurrency);
        }
        if ((dirtyFlags & 0x62L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView6, viewmodelCheckPromoRequestCode);
        }
        if ((dirtyFlags & 0x46L) != 0) {
            // api target 1

            grand.app.aber_user.base.ApplicationBinding.getItemsV2Binding(this.rcLocation, viewmodelLocationsAdapters, "1", "1");
        }
        if ((dirtyFlags & 0x4aL) != 0) {
            // api target 1

            grand.app.aber_user.base.ApplicationBinding.getItemsV2Binding(this.recCart, viewmodelCartAdapter, "1", "1");
        }
        if ((dirtyFlags & 0x53L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvDeliveryPrice, textUtilsIsEmptyViewmodelNewOrderRequestDiscountViewmodelNewOrderRequestDiscountConcatJavaLangStringConcatViewmodelDiscountJavaLangString0ConcatViewmodelNewOrderRequestCurrency);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        switch(sourceId) {
            case 3: {
                // localize variables for thread safety
                // viewmodel != null
                boolean viewmodelJavaLangObjectNull = false;
                // viewmodel
                grand.app.aber_user.pages.cart.viewModels.CartViewModel viewmodel = mViewmodel;



                viewmodelJavaLangObjectNull = (viewmodel) != (null);
                if (viewmodelJavaLangObjectNull) {


                    viewmodel.toPayment();
                }
                break;
            }
            case 1: {
                // localize variables for thread safety
                // viewmodel != null
                boolean viewmodelJavaLangObjectNull = false;
                // viewmodel
                grand.app.aber_user.pages.cart.viewModels.CartViewModel viewmodel = mViewmodel;



                viewmodelJavaLangObjectNull = (viewmodel) != (null);
                if (viewmodelJavaLangObjectNull) {


                    viewmodel.addLocation();
                }
                break;
            }
            case 2: {
                // localize variables for thread safety
                // viewmodel != null
                boolean viewmodelJavaLangObjectNull = false;
                // viewmodel
                grand.app.aber_user.pages.cart.viewModels.CartViewModel viewmodel = mViewmodel;



                viewmodelJavaLangObjectNull = (viewmodel) != (null);
                if (viewmodelJavaLangObjectNull) {


                    viewmodel.checkPromo();
                }
                break;
            }
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): viewmodel.discount
        flag 1 (0x2L): viewmodel
        flag 2 (0x3L): viewmodel.locationsAdapters
        flag 3 (0x4L): viewmodel.cartAdapter
        flag 4 (0x5L): viewmodel.newOrderRequest
        flag 5 (0x6L): viewmodel.checkPromoRequest
        flag 6 (0x7L): null
        flag 7 (0x8L): !TextUtils.isEmpty(viewmodel.newOrderRequest.deliveryFee) ? viewmodel.newOrderRequest.deliveryFee.concat(" ").concat(viewmodel.newOrderRequest.currency) : "0 ".concat(viewmodel.newOrderRequest.currency)
        flag 8 (0x9L): !TextUtils.isEmpty(viewmodel.newOrderRequest.deliveryFee) ? viewmodel.newOrderRequest.deliveryFee.concat(" ").concat(viewmodel.newOrderRequest.currency) : "0 ".concat(viewmodel.newOrderRequest.currency)
        flag 9 (0xaL): !TextUtils.isEmpty(viewmodel.newOrderRequest.total) ? viewmodel.newOrderRequest.total.concat(" ").concat(viewmodel.newOrderRequest.currency) : "0 ".concat(viewmodel.newOrderRequest.currency)
        flag 10 (0xbL): !TextUtils.isEmpty(viewmodel.newOrderRequest.total) ? viewmodel.newOrderRequest.total.concat(" ").concat(viewmodel.newOrderRequest.currency) : "0 ".concat(viewmodel.newOrderRequest.currency)
        flag 11 (0xcL): !TextUtils.isEmpty(viewmodel.discount.get()) ? false : true
        flag 12 (0xdL): !TextUtils.isEmpty(viewmodel.discount.get()) ? false : true
        flag 13 (0xeL): !TextUtils.isEmpty(viewmodel.newOrderRequest.discount) ? viewmodel.newOrderRequest.discount.concat(" ").concat(viewmodel.discount.get()) : " 0 ".concat(viewmodel.newOrderRequest.currency)
        flag 14 (0xfL): !TextUtils.isEmpty(viewmodel.newOrderRequest.discount) ? viewmodel.newOrderRequest.discount.concat(" ").concat(viewmodel.discount.get()) : " 0 ".concat(viewmodel.newOrderRequest.currency)
    flag mapping end*/
    //end
}