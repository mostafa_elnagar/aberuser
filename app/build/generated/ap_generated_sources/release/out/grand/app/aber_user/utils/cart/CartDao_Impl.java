package grand.app.aber_user.utils.cart;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import androidx.room.EntityDeletionOrUpdateAdapter;
import androidx.room.EntityInsertionAdapter;
import androidx.room.RoomDatabase;
import androidx.room.RoomSQLiteQuery;
import androidx.room.SharedSQLiteStatement;
import androidx.room.util.CursorUtil;
import androidx.room.util.DBUtil;
import androidx.sqlite.db.SupportSQLiteStatement;
import grand.app.aber_user.pages.parts.models.ProductsItem;
import java.lang.Class;
import java.lang.Exception;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;

@SuppressWarnings({"unchecked", "deprecation"})
public final class CartDao_Impl implements CartDao {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter<ProductsItem> __insertionAdapterOfProductsItem;

  private final EntityDeletionOrUpdateAdapter<ProductsItem> __updateAdapterOfProductsItem;

  private final SharedSQLiteStatement __preparedStmtOfDeleteItem;

  private final SharedSQLiteStatement __preparedStmtOfEmptyProductCart;

  private final SharedSQLiteStatement __preparedStmtOfUpdateProductQuantity;

  public CartDao_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfProductsItem = new EntityInsertionAdapter<ProductsItem>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR IGNORE INTO `ProductsItem` (`image`,`price`,`priceItem`,`currency`,`name`,`id`,`quantity`,`attributeParentId`,`attributeChildId`,`product_room_id`) VALUES (?,?,?,?,?,?,?,?,?,nullif(?, 0))";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, ProductsItem value) {
        if (value.getImage() == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindString(1, value.getImage());
        }
        if (value.getPrice() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getPrice());
        }
        stmt.bindDouble(3, value.getPriceItem());
        if (value.getCurrency() == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.getCurrency());
        }
        if (value.getName() == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindString(5, value.getName());
        }
        stmt.bindLong(6, value.getId());
        stmt.bindLong(7, value.getQuantity());
        stmt.bindLong(8, value.getAttributeParentId());
        stmt.bindLong(9, value.getAttributeChildId());
        stmt.bindLong(10, value.getProduct_room_id());
      }
    };
    this.__updateAdapterOfProductsItem = new EntityDeletionOrUpdateAdapter<ProductsItem>(__db) {
      @Override
      public String createQuery() {
        return "UPDATE OR ABORT `ProductsItem` SET `image` = ?,`price` = ?,`priceItem` = ?,`currency` = ?,`name` = ?,`id` = ?,`quantity` = ?,`attributeParentId` = ?,`attributeChildId` = ?,`product_room_id` = ? WHERE `product_room_id` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, ProductsItem value) {
        if (value.getImage() == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindString(1, value.getImage());
        }
        if (value.getPrice() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getPrice());
        }
        stmt.bindDouble(3, value.getPriceItem());
        if (value.getCurrency() == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.getCurrency());
        }
        if (value.getName() == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindString(5, value.getName());
        }
        stmt.bindLong(6, value.getId());
        stmt.bindLong(7, value.getQuantity());
        stmt.bindLong(8, value.getAttributeParentId());
        stmt.bindLong(9, value.getAttributeChildId());
        stmt.bindLong(10, value.getProduct_room_id());
        stmt.bindLong(11, value.getProduct_room_id());
      }
    };
    this.__preparedStmtOfDeleteItem = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "DELETE FROM productsitem WHERE product_room_id=?";
        return _query;
      }
    };
    this.__preparedStmtOfEmptyProductCart = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "DELETE FROM productsitem";
        return _query;
      }
    };
    this.__preparedStmtOfUpdateProductQuantity = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "UPDATE productsitem SET quantity=quantity+? where product_room_id=?";
        return _query;
      }
    };
  }

  @Override
  public long addProduct(final ProductsItem productDetails) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      long _result = __insertionAdapterOfProductsItem.insertAndReturnId(productDetails);
      __db.setTransactionSuccessful();
      return _result;
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void updateProduct(final ProductsItem productDetails) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __updateAdapterOfProductsItem.handle(productDetails);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void deleteItem(final int productId) {
    __db.assertNotSuspendingTransaction();
    final SupportSQLiteStatement _stmt = __preparedStmtOfDeleteItem.acquire();
    int _argIndex = 1;
    _stmt.bindLong(_argIndex, productId);
    __db.beginTransaction();
    try {
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfDeleteItem.release(_stmt);
    }
  }

  @Override
  public void emptyProductCart() {
    __db.assertNotSuspendingTransaction();
    final SupportSQLiteStatement _stmt = __preparedStmtOfEmptyProductCart.acquire();
    __db.beginTransaction();
    try {
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfEmptyProductCart.release(_stmt);
    }
  }

  @Override
  public void updateProductQuantity(final int quantity, final int productId) {
    __db.assertNotSuspendingTransaction();
    final SupportSQLiteStatement _stmt = __preparedStmtOfUpdateProductQuantity.acquire();
    int _argIndex = 1;
    _stmt.bindLong(_argIndex, quantity);
    _argIndex = 2;
    _stmt.bindLong(_argIndex, productId);
    __db.beginTransaction();
    try {
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfUpdateProductQuantity.release(_stmt);
    }
  }

  @Override
  public LiveData<List<ProductsItem>> getProducts() {
    final String _sql = "select * from productsitem";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    return __db.getInvalidationTracker().createLiveData(new String[]{"productsitem"}, false, new Callable<List<ProductsItem>>() {
      @Override
      public List<ProductsItem> call() throws Exception {
        final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
        try {
          final int _cursorIndexOfImage = CursorUtil.getColumnIndexOrThrow(_cursor, "image");
          final int _cursorIndexOfPrice = CursorUtil.getColumnIndexOrThrow(_cursor, "price");
          final int _cursorIndexOfPriceItem = CursorUtil.getColumnIndexOrThrow(_cursor, "priceItem");
          final int _cursorIndexOfCurrency = CursorUtil.getColumnIndexOrThrow(_cursor, "currency");
          final int _cursorIndexOfName = CursorUtil.getColumnIndexOrThrow(_cursor, "name");
          final int _cursorIndexOfId = CursorUtil.getColumnIndexOrThrow(_cursor, "id");
          final int _cursorIndexOfQuantity = CursorUtil.getColumnIndexOrThrow(_cursor, "quantity");
          final int _cursorIndexOfAttributeParentId = CursorUtil.getColumnIndexOrThrow(_cursor, "attributeParentId");
          final int _cursorIndexOfAttributeChildId = CursorUtil.getColumnIndexOrThrow(_cursor, "attributeChildId");
          final int _cursorIndexOfProductRoomId = CursorUtil.getColumnIndexOrThrow(_cursor, "product_room_id");
          final List<ProductsItem> _result = new ArrayList<ProductsItem>(_cursor.getCount());
          while(_cursor.moveToNext()) {
            final ProductsItem _item;
            _item = new ProductsItem();
            final String _tmpImage;
            if (_cursor.isNull(_cursorIndexOfImage)) {
              _tmpImage = null;
            } else {
              _tmpImage = _cursor.getString(_cursorIndexOfImage);
            }
            _item.setImage(_tmpImage);
            final String _tmpPrice;
            if (_cursor.isNull(_cursorIndexOfPrice)) {
              _tmpPrice = null;
            } else {
              _tmpPrice = _cursor.getString(_cursorIndexOfPrice);
            }
            _item.setPrice(_tmpPrice);
            final double _tmpPriceItem;
            _tmpPriceItem = _cursor.getDouble(_cursorIndexOfPriceItem);
            _item.setPriceItem(_tmpPriceItem);
            final String _tmpCurrency;
            if (_cursor.isNull(_cursorIndexOfCurrency)) {
              _tmpCurrency = null;
            } else {
              _tmpCurrency = _cursor.getString(_cursorIndexOfCurrency);
            }
            _item.setCurrency(_tmpCurrency);
            final String _tmpName;
            if (_cursor.isNull(_cursorIndexOfName)) {
              _tmpName = null;
            } else {
              _tmpName = _cursor.getString(_cursorIndexOfName);
            }
            _item.setName(_tmpName);
            final int _tmpId;
            _tmpId = _cursor.getInt(_cursorIndexOfId);
            _item.setId(_tmpId);
            final int _tmpQuantity;
            _tmpQuantity = _cursor.getInt(_cursorIndexOfQuantity);
            _item.setQuantity(_tmpQuantity);
            final int _tmpAttributeParentId;
            _tmpAttributeParentId = _cursor.getInt(_cursorIndexOfAttributeParentId);
            _item.setAttributeParentId(_tmpAttributeParentId);
            final int _tmpAttributeChildId;
            _tmpAttributeChildId = _cursor.getInt(_cursorIndexOfAttributeChildId);
            _item.setAttributeChildId(_tmpAttributeChildId);
            final int _tmpProduct_room_id;
            _tmpProduct_room_id = _cursor.getInt(_cursorIndexOfProductRoomId);
            _item.setProduct_room_id(_tmpProduct_room_id);
            _result.add(_item);
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    });
  }

  @Override
  public LiveData<String> getCartTotal() {
    final String _sql = "select sum(price) from productsitem";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    return __db.getInvalidationTracker().createLiveData(new String[]{"productsitem"}, false, new Callable<String>() {
      @Override
      public String call() throws Exception {
        final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
        try {
          final String _result;
          if(_cursor.moveToFirst()) {
            final String _tmp;
            if (_cursor.isNull(0)) {
              _tmp = null;
            } else {
              _tmp = _cursor.getString(0);
            }
            _result = _tmp;
          } else {
            _result = null;
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    });
  }

  @Override
  public int getIfMealExists(final int productId) {
    final String _sql = "SELECT EXISTS (SELECT * FROM productsitem WHERE product_room_id=?)";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    _statement.bindLong(_argIndex, productId);
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _result;
      if(_cursor.moveToFirst()) {
        _result = _cursor.getInt(0);
      } else {
        _result = 0;
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  public static List<Class<?>> getRequiredConverters() {
    return Collections.emptyList();
  }
}
