package grand.app.aber_user.databinding;
import grand.app.aber_user.R;
import grand.app.aber_user.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ItemChatBindingImpl extends ItemChatBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.br, 6);
    }
    // views
    @NonNull
    private final grand.app.aber_user.customViews.views.CustomTextViewMedium mboundView3;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ItemChatBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 7, sIncludes, sViewsWithIds));
    }
    private ItemChatBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 1
            , (androidx.constraintlayout.widget.Barrier) bindings[6]
            , (de.hdodenhof.circleimageview.CircleImageView) bindings[1]
            , (com.google.android.material.imageview.ShapeableImageView) bindings[4]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[2]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[5]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[0]
            );
        this.driverImage.setTag(null);
        this.icImage.setTag(null);
        this.itemMessage.setTag(null);
        this.itemMessageTime.setTag(null);
        this.mboundView3 = (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[3];
        this.mboundView3.setTag(null);
        this.rlItemChat.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.itemChatViewModel == variableId) {
            setItemChatViewModel((grand.app.aber_user.pages.chat.viewmodel.ItemChatViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setItemChatViewModel(@Nullable grand.app.aber_user.pages.chat.viewmodel.ItemChatViewModel ItemChatViewModel) {
        updateRegistration(0, ItemChatViewModel);
        this.mItemChatViewModel = ItemChatViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.itemChatViewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeItemChatViewModel((grand.app.aber_user.pages.chat.viewmodel.ItemChatViewModel) object, fieldId);
        }
        return false;
    }
    private boolean onChangeItemChatViewModel(grand.app.aber_user.pages.chat.viewmodel.ItemChatViewModel ItemChatViewModel, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        else if (fieldId == BR.chat) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        boolean itemChatViewModelChatReceiverIdItemChatViewModelUserDataId = false;
        grand.app.aber_user.pages.auth.models.UserData itemChatViewModelChatReceiver = null;
        grand.app.aber_user.pages.conversations.models.ConversationsData itemChatViewModelChat = null;
        grand.app.aber_user.pages.auth.models.UserData itemChatViewModelUserData = null;
        android.graphics.drawable.Drawable itemChatViewModelChatReceiverIdItemChatViewModelUserDataIdMboundView3AndroidDrawableRightChatMboundView3AndroidDrawableLeftChat = null;
        java.lang.String itemChatViewModelChatReceiverIdItemChatViewModelUserDataIdItemChatViewModelUserDataImageItemChatViewModelChatReceiverImage = null;
        int itemChatViewModelChatReceiverId = 0;
        boolean textUtilsIsEmptyItemChatViewModelChatImage = false;
        int textUtilsIsEmptyItemChatViewModelChatImageViewGONEViewVISIBLE = 0;
        int itemChatViewModelChatReceiverIdItemChatViewModelUserDataIdMboundView3AndroidColorWhiteMboundView3AndroidColorColordark = 0;
        java.lang.String itemChatViewModelChatReceiverImage = null;
        java.lang.String itemChatViewModelChatImage = null;
        java.lang.String itemChatViewModelChatMessage = null;
        int TextUtilsIsEmptyItemChatViewModelChatImageViewGONEViewVISIBLE1 = 0;
        grand.app.aber_user.pages.chat.viewmodel.ItemChatViewModel itemChatViewModel = mItemChatViewModel;
        java.lang.String itemChatViewModelChatCreatedAt = null;
        java.lang.String itemChatViewModelUserDataImage = null;
        boolean TextUtilsIsEmptyItemChatViewModelChatImage1 = false;
        int itemChatViewModelUserDataId = 0;

        if ((dirtyFlags & 0x7L) != 0) {



                if (itemChatViewModel != null) {
                    // read itemChatViewModel.chat
                    itemChatViewModelChat = itemChatViewModel.getChat();
                    // read itemChatViewModel.userData
                    itemChatViewModelUserData = itemChatViewModel.userData;
                }


                if (itemChatViewModelChat != null) {
                    // read itemChatViewModel.chat.receiver
                    itemChatViewModelChatReceiver = itemChatViewModelChat.getReceiver();
                    // read itemChatViewModel.chat.image
                    itemChatViewModelChatImage = itemChatViewModelChat.getImage();
                    // read itemChatViewModel.chat.message
                    itemChatViewModelChatMessage = itemChatViewModelChat.getMessage();
                    // read itemChatViewModel.chat.createdAt
                    itemChatViewModelChatCreatedAt = itemChatViewModelChat.getCreatedAt();
                }
                if (itemChatViewModelUserData != null) {
                    // read itemChatViewModel.userData.id
                    itemChatViewModelUserDataId = itemChatViewModelUserData.getId();
                }


                if (itemChatViewModelChatReceiver != null) {
                    // read itemChatViewModel.chat.receiver.id
                    itemChatViewModelChatReceiverId = itemChatViewModelChatReceiver.getId();
                }
                // read TextUtils.isEmpty(itemChatViewModel.chat.image)
                TextUtilsIsEmptyItemChatViewModelChatImage1 = android.text.TextUtils.isEmpty(itemChatViewModelChatImage);
            if((dirtyFlags & 0x7L) != 0) {
                if(TextUtilsIsEmptyItemChatViewModelChatImage1) {
                        dirtyFlags |= 0x1000L;
                }
                else {
                        dirtyFlags |= 0x800L;
                }
            }


                // read itemChatViewModel.chat.receiver.id == itemChatViewModel.userData.id
                itemChatViewModelChatReceiverIdItemChatViewModelUserDataId = (itemChatViewModelChatReceiverId) == (itemChatViewModelUserDataId);
                // read !TextUtils.isEmpty(itemChatViewModel.chat.image)
                textUtilsIsEmptyItemChatViewModelChatImage = !TextUtilsIsEmptyItemChatViewModelChatImage1;
                // read TextUtils.isEmpty(itemChatViewModel.chat.image) ? View.GONE : View.VISIBLE
                TextUtilsIsEmptyItemChatViewModelChatImageViewGONEViewVISIBLE1 = ((TextUtilsIsEmptyItemChatViewModelChatImage1) ? (android.view.View.GONE) : (android.view.View.VISIBLE));
            if((dirtyFlags & 0x7L) != 0) {
                if(itemChatViewModelChatReceiverIdItemChatViewModelUserDataId) {
                        dirtyFlags |= 0x10L;
                        dirtyFlags |= 0x40L;
                        dirtyFlags |= 0x400L;
                }
                else {
                        dirtyFlags |= 0x8L;
                        dirtyFlags |= 0x20L;
                        dirtyFlags |= 0x200L;
                }
            }
            if((dirtyFlags & 0x7L) != 0) {
                if(textUtilsIsEmptyItemChatViewModelChatImage) {
                        dirtyFlags |= 0x100L;
                }
                else {
                        dirtyFlags |= 0x80L;
                }
            }


                // read itemChatViewModel.chat.receiver.id == itemChatViewModel.userData.id ? @android:drawable/right_chat : @android:drawable/left_chat
                itemChatViewModelChatReceiverIdItemChatViewModelUserDataIdMboundView3AndroidDrawableRightChatMboundView3AndroidDrawableLeftChat = ((itemChatViewModelChatReceiverIdItemChatViewModelUserDataId) ? (androidx.appcompat.content.res.AppCompatResources.getDrawable(mboundView3.getContext(), R.drawable.right_chat)) : (androidx.appcompat.content.res.AppCompatResources.getDrawable(mboundView3.getContext(), R.drawable.left_chat)));
                // read itemChatViewModel.chat.receiver.id == itemChatViewModel.userData.id ? @android:color/white : @android:color/colordark
                itemChatViewModelChatReceiverIdItemChatViewModelUserDataIdMboundView3AndroidColorWhiteMboundView3AndroidColorColordark = ((itemChatViewModelChatReceiverIdItemChatViewModelUserDataId) ? (getColorFromResource(mboundView3, R.color.white)) : (getColorFromResource(mboundView3, R.color.colordark)));
                // read !TextUtils.isEmpty(itemChatViewModel.chat.image) ? View.GONE : View.VISIBLE
                textUtilsIsEmptyItemChatViewModelChatImageViewGONEViewVISIBLE = ((textUtilsIsEmptyItemChatViewModelChatImage) ? (android.view.View.GONE) : (android.view.View.VISIBLE));
        }
        // batch finished

        if ((dirtyFlags & 0x20L) != 0) {

                if (itemChatViewModelChatReceiver != null) {
                    // read itemChatViewModel.chat.receiver.image
                    itemChatViewModelChatReceiverImage = itemChatViewModelChatReceiver.getImage();
                }
        }
        if ((dirtyFlags & 0x40L) != 0) {

                if (itemChatViewModelUserData != null) {
                    // read itemChatViewModel.userData.image
                    itemChatViewModelUserDataImage = itemChatViewModelUserData.getImage();
                }
        }

        if ((dirtyFlags & 0x7L) != 0) {

                // read itemChatViewModel.chat.receiver.id == itemChatViewModel.userData.id ? itemChatViewModel.userData.image : itemChatViewModel.chat.receiver.image
                itemChatViewModelChatReceiverIdItemChatViewModelUserDataIdItemChatViewModelUserDataImageItemChatViewModelChatReceiverImage = ((itemChatViewModelChatReceiverIdItemChatViewModelUserDataId) ? (itemChatViewModelUserDataImage) : (itemChatViewModelChatReceiverImage));
        }
        // batch finished
        if ((dirtyFlags & 0x7L) != 0) {
            // api target 1

            grand.app.aber_user.base.ApplicationBinding.loadImage(this.driverImage, itemChatViewModelChatReceiverIdItemChatViewModelUserDataIdItemChatViewModelUserDataImageItemChatViewModelChatReceiverImage);
            this.icImage.setVisibility(TextUtilsIsEmptyItemChatViewModelChatImageViewGONEViewVISIBLE1);
            grand.app.aber_user.base.ApplicationBinding.loadCommentImage(this.icImage, itemChatViewModelChatImage);
            this.itemMessage.setVisibility(textUtilsIsEmptyItemChatViewModelChatImageViewGONEViewVISIBLE);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.itemMessageTime, itemChatViewModelChatCreatedAt);
            androidx.databinding.adapters.ViewBindingAdapter.setBackground(this.mboundView3, itemChatViewModelChatReceiverIdItemChatViewModelUserDataIdMboundView3AndroidDrawableRightChatMboundView3AndroidDrawableLeftChat);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView3, itemChatViewModelChatMessage);
            this.mboundView3.setTextColor(itemChatViewModelChatReceiverIdItemChatViewModelUserDataIdMboundView3AndroidColorWhiteMboundView3AndroidColorColordark);
            grand.app.aber_user.pages.chat.viewmodel.ItemChatViewModel.chatAdminDirection(this.rlItemChat, itemChatViewModelChatReceiverId);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): itemChatViewModel
        flag 1 (0x2L): itemChatViewModel.chat
        flag 2 (0x3L): null
        flag 3 (0x4L): itemChatViewModel.chat.receiver.id == itemChatViewModel.userData.id ? @android:drawable/right_chat : @android:drawable/left_chat
        flag 4 (0x5L): itemChatViewModel.chat.receiver.id == itemChatViewModel.userData.id ? @android:drawable/right_chat : @android:drawable/left_chat
        flag 5 (0x6L): itemChatViewModel.chat.receiver.id == itemChatViewModel.userData.id ? itemChatViewModel.userData.image : itemChatViewModel.chat.receiver.image
        flag 6 (0x7L): itemChatViewModel.chat.receiver.id == itemChatViewModel.userData.id ? itemChatViewModel.userData.image : itemChatViewModel.chat.receiver.image
        flag 7 (0x8L): !TextUtils.isEmpty(itemChatViewModel.chat.image) ? View.GONE : View.VISIBLE
        flag 8 (0x9L): !TextUtils.isEmpty(itemChatViewModel.chat.image) ? View.GONE : View.VISIBLE
        flag 9 (0xaL): itemChatViewModel.chat.receiver.id == itemChatViewModel.userData.id ? @android:color/white : @android:color/colordark
        flag 10 (0xbL): itemChatViewModel.chat.receiver.id == itemChatViewModel.userData.id ? @android:color/white : @android:color/colordark
        flag 11 (0xcL): TextUtils.isEmpty(itemChatViewModel.chat.image) ? View.GONE : View.VISIBLE
        flag 12 (0xdL): TextUtils.isEmpty(itemChatViewModel.chat.image) ? View.GONE : View.VISIBLE
    flag mapping end*/
    //end
}