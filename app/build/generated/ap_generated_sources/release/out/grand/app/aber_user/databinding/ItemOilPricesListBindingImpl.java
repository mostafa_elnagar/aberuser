package grand.app.aber_user.databinding;
import grand.app.aber_user.R;
import grand.app.aber_user.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ItemOilPricesListBindingImpl extends ItemOilPricesListBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = null;
    }
    // views
    @NonNull
    private final androidx.cardview.widget.CardView mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ItemOilPricesListBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 5, sIncludes, sViewsWithIds));
    }
    private ItemOilPricesListBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 1
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[1]
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[2]
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[3]
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[4]
            );
        this.mboundView0 = (androidx.cardview.widget.CardView) bindings[0];
        this.mboundView0.setTag(null);
        this.tvName.setTag(null);
        this.tvPrice.setTag(null);
        this.tvValuesName.setTag(null);
        this.tvValuesPrice.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.itemViewModel == variableId) {
            setItemViewModel((grand.app.aber_user.pages.services.viewModels.ItemOilPriceListViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setItemViewModel(@Nullable grand.app.aber_user.pages.services.viewModels.ItemOilPriceListViewModel ItemViewModel) {
        updateRegistration(0, ItemViewModel);
        this.mItemViewModel = ItemViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.itemViewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeItemViewModel((grand.app.aber_user.pages.services.viewModels.ItemOilPriceListViewModel) object, fieldId);
        }
        return false;
    }
    private boolean onChangeItemViewModel(grand.app.aber_user.pages.services.viewModels.ItemOilPriceListViewModel ItemViewModel, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        else if (fieldId == BR.dropDownsObject) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String itemViewModelDropDownsObjectValuesListJavaLangObjectNullItemViewModelDropDownsObjectValuesListSizeInt0BooleanFalseItemViewModelDropDownsObjectValuesListGetInt0PriceConcatJavaLangStringConcatItemViewModelDropDownsObjectValuesListGetInt0CurrencyJavaLangString = null;
        java.lang.String itemViewModelDropDownsObjectValuesListGetInt0Currency = null;
        int itemViewModelDropDownsObjectValuesListSize = 0;
        java.lang.String itemViewModelDropDownsObjectValuesListGetInt0Price = null;
        java.lang.String itemViewModelDropDownsObjectPrice = null;
        java.util.List<grand.app.aber_user.model.DropDownsObject> itemViewModelDropDownsObjectValuesList = null;
        boolean itemViewModelDropDownsObjectValuesListJavaLangObjectNull = false;
        java.lang.String itemViewModelDropDownsObjectValuesListGetInt0PriceConcatJavaLangString = null;
        java.lang.String itemViewModelDropDownsObjectPriceConcatJavaLangStringConcatItemViewModelDropDownsObjectCurrency = null;
        grand.app.aber_user.model.DropDownsObject itemViewModelDropDownsObject = null;
        boolean itemViewModelDropDownsObjectValuesListSizeInt0 = false;
        grand.app.aber_user.model.DropDownsObject itemViewModelDropDownsObjectValuesListGetInt0 = null;
        java.lang.String itemViewModelDropDownsObjectValuesListGetInt0PriceConcatJavaLangStringConcatItemViewModelDropDownsObjectValuesListGetInt0Currency = null;
        boolean itemViewModelDropDownsObjectValuesListJavaLangObjectNullItemViewModelDropDownsObjectValuesListSizeInt0BooleanFalse = false;
        grand.app.aber_user.pages.services.viewModels.ItemOilPriceListViewModel itemViewModel = mItemViewModel;
        java.lang.String itemViewModelDropDownsObjectCurrency = null;
        java.lang.String itemViewModelDropDownsObjectPriceConcatJavaLangString = null;
        java.lang.String itemViewModelDropDownsObjectValuesListJavaLangObjectNullItemViewModelDropDownsObjectValuesListSizeInt0BooleanFalseItemViewModelDropDownsObjectValuesListGetInt0NameJavaLangString = null;
        java.lang.String itemViewModelDropDownsObjectValuesListGetInt0Name = null;
        java.lang.String itemViewModelDropDownsObjectName = null;

        if ((dirtyFlags & 0x7L) != 0) {



                if (itemViewModel != null) {
                    // read itemViewModel.dropDownsObject
                    itemViewModelDropDownsObject = itemViewModel.getDropDownsObject();
                }


                if (itemViewModelDropDownsObject != null) {
                    // read itemViewModel.dropDownsObject.price
                    itemViewModelDropDownsObjectPrice = itemViewModelDropDownsObject.getPrice();
                    // read itemViewModel.dropDownsObject.valuesList
                    itemViewModelDropDownsObjectValuesList = itemViewModelDropDownsObject.getValuesList();
                    // read itemViewModel.dropDownsObject.currency
                    itemViewModelDropDownsObjectCurrency = itemViewModelDropDownsObject.getCurrency();
                    // read itemViewModel.dropDownsObject.name
                    itemViewModelDropDownsObjectName = itemViewModelDropDownsObject.getName();
                }


                if (itemViewModelDropDownsObjectPrice != null) {
                    // read itemViewModel.dropDownsObject.price.concat(" ")
                    itemViewModelDropDownsObjectPriceConcatJavaLangString = itemViewModelDropDownsObjectPrice.concat(" ");
                }
                // read itemViewModel.dropDownsObject.valuesList != null
                itemViewModelDropDownsObjectValuesListJavaLangObjectNull = (itemViewModelDropDownsObjectValuesList) != (null);
            if((dirtyFlags & 0x7L) != 0) {
                if(itemViewModelDropDownsObjectValuesListJavaLangObjectNull) {
                        dirtyFlags |= 0x40L;
                }
                else {
                        dirtyFlags |= 0x20L;
                }
            }


                if (itemViewModelDropDownsObjectPriceConcatJavaLangString != null) {
                    // read itemViewModel.dropDownsObject.price.concat(" ").concat(itemViewModel.dropDownsObject.currency)
                    itemViewModelDropDownsObjectPriceConcatJavaLangStringConcatItemViewModelDropDownsObjectCurrency = itemViewModelDropDownsObjectPriceConcatJavaLangString.concat(itemViewModelDropDownsObjectCurrency);
                }
        }
        // batch finished

        if ((dirtyFlags & 0x40L) != 0) {

                if (itemViewModelDropDownsObjectValuesList != null) {
                    // read itemViewModel.dropDownsObject.valuesList.size()
                    itemViewModelDropDownsObjectValuesListSize = itemViewModelDropDownsObjectValuesList.size();
                }


                // read itemViewModel.dropDownsObject.valuesList.size() > 0
                itemViewModelDropDownsObjectValuesListSizeInt0 = (itemViewModelDropDownsObjectValuesListSize) > (0);
        }

        if ((dirtyFlags & 0x7L) != 0) {

                // read itemViewModel.dropDownsObject.valuesList != null ? itemViewModel.dropDownsObject.valuesList.size() > 0 : false
                itemViewModelDropDownsObjectValuesListJavaLangObjectNullItemViewModelDropDownsObjectValuesListSizeInt0BooleanFalse = ((itemViewModelDropDownsObjectValuesListJavaLangObjectNull) ? (itemViewModelDropDownsObjectValuesListSizeInt0) : (false));
            if((dirtyFlags & 0x7L) != 0) {
                if(itemViewModelDropDownsObjectValuesListJavaLangObjectNullItemViewModelDropDownsObjectValuesListSizeInt0BooleanFalse) {
                        dirtyFlags |= 0x10L;
                        dirtyFlags |= 0x100L;
                }
                else {
                        dirtyFlags |= 0x8L;
                        dirtyFlags |= 0x80L;
                }
            }
        }
        // batch finished

        if ((dirtyFlags & 0x110L) != 0) {

                if (itemViewModelDropDownsObjectValuesList != null) {
                    // read itemViewModel.dropDownsObject.valuesList.get(0)
                    itemViewModelDropDownsObjectValuesListGetInt0 = itemViewModelDropDownsObjectValuesList.get(0);
                }

            if ((dirtyFlags & 0x10L) != 0) {

                    if (itemViewModelDropDownsObjectValuesListGetInt0 != null) {
                        // read itemViewModel.dropDownsObject.valuesList.get(0).currency
                        itemViewModelDropDownsObjectValuesListGetInt0Currency = itemViewModelDropDownsObjectValuesListGetInt0.getCurrency();
                        // read itemViewModel.dropDownsObject.valuesList.get(0).price
                        itemViewModelDropDownsObjectValuesListGetInt0Price = itemViewModelDropDownsObjectValuesListGetInt0.getPrice();
                    }


                    if (itemViewModelDropDownsObjectValuesListGetInt0Price != null) {
                        // read itemViewModel.dropDownsObject.valuesList.get(0).price.concat(" ")
                        itemViewModelDropDownsObjectValuesListGetInt0PriceConcatJavaLangString = itemViewModelDropDownsObjectValuesListGetInt0Price.concat(" ");
                    }


                    if (itemViewModelDropDownsObjectValuesListGetInt0PriceConcatJavaLangString != null) {
                        // read itemViewModel.dropDownsObject.valuesList.get(0).price.concat(" ").concat(itemViewModel.dropDownsObject.valuesList.get(0).currency)
                        itemViewModelDropDownsObjectValuesListGetInt0PriceConcatJavaLangStringConcatItemViewModelDropDownsObjectValuesListGetInt0Currency = itemViewModelDropDownsObjectValuesListGetInt0PriceConcatJavaLangString.concat(itemViewModelDropDownsObjectValuesListGetInt0Currency);
                    }
            }
            if ((dirtyFlags & 0x100L) != 0) {

                    if (itemViewModelDropDownsObjectValuesListGetInt0 != null) {
                        // read itemViewModel.dropDownsObject.valuesList.get(0).name
                        itemViewModelDropDownsObjectValuesListGetInt0Name = itemViewModelDropDownsObjectValuesListGetInt0.getName();
                    }
            }
        }

        if ((dirtyFlags & 0x7L) != 0) {

                // read itemViewModel.dropDownsObject.valuesList != null ? itemViewModel.dropDownsObject.valuesList.size() > 0 : false ? itemViewModel.dropDownsObject.valuesList.get(0).price.concat(" ").concat(itemViewModel.dropDownsObject.valuesList.get(0).currency) : ""
                itemViewModelDropDownsObjectValuesListJavaLangObjectNullItemViewModelDropDownsObjectValuesListSizeInt0BooleanFalseItemViewModelDropDownsObjectValuesListGetInt0PriceConcatJavaLangStringConcatItemViewModelDropDownsObjectValuesListGetInt0CurrencyJavaLangString = ((itemViewModelDropDownsObjectValuesListJavaLangObjectNullItemViewModelDropDownsObjectValuesListSizeInt0BooleanFalse) ? (itemViewModelDropDownsObjectValuesListGetInt0PriceConcatJavaLangStringConcatItemViewModelDropDownsObjectValuesListGetInt0Currency) : (""));
                // read itemViewModel.dropDownsObject.valuesList != null ? itemViewModel.dropDownsObject.valuesList.size() > 0 : false ? itemViewModel.dropDownsObject.valuesList.get(0).name : ""
                itemViewModelDropDownsObjectValuesListJavaLangObjectNullItemViewModelDropDownsObjectValuesListSizeInt0BooleanFalseItemViewModelDropDownsObjectValuesListGetInt0NameJavaLangString = ((itemViewModelDropDownsObjectValuesListJavaLangObjectNullItemViewModelDropDownsObjectValuesListSizeInt0BooleanFalse) ? (itemViewModelDropDownsObjectValuesListGetInt0Name) : (""));
        }
        // batch finished
        if ((dirtyFlags & 0x7L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvName, itemViewModelDropDownsObjectName);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvPrice, itemViewModelDropDownsObjectPriceConcatJavaLangStringConcatItemViewModelDropDownsObjectCurrency);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvValuesName, itemViewModelDropDownsObjectValuesListJavaLangObjectNullItemViewModelDropDownsObjectValuesListSizeInt0BooleanFalseItemViewModelDropDownsObjectValuesListGetInt0NameJavaLangString);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvValuesPrice, itemViewModelDropDownsObjectValuesListJavaLangObjectNullItemViewModelDropDownsObjectValuesListSizeInt0BooleanFalseItemViewModelDropDownsObjectValuesListGetInt0PriceConcatJavaLangStringConcatItemViewModelDropDownsObjectValuesListGetInt0CurrencyJavaLangString);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): itemViewModel
        flag 1 (0x2L): itemViewModel.dropDownsObject
        flag 2 (0x3L): null
        flag 3 (0x4L): itemViewModel.dropDownsObject.valuesList != null ? itemViewModel.dropDownsObject.valuesList.size() > 0 : false ? itemViewModel.dropDownsObject.valuesList.get(0).price.concat(" ").concat(itemViewModel.dropDownsObject.valuesList.get(0).currency) : ""
        flag 4 (0x5L): itemViewModel.dropDownsObject.valuesList != null ? itemViewModel.dropDownsObject.valuesList.size() > 0 : false ? itemViewModel.dropDownsObject.valuesList.get(0).price.concat(" ").concat(itemViewModel.dropDownsObject.valuesList.get(0).currency) : ""
        flag 5 (0x6L): itemViewModel.dropDownsObject.valuesList != null ? itemViewModel.dropDownsObject.valuesList.size() > 0 : false
        flag 6 (0x7L): itemViewModel.dropDownsObject.valuesList != null ? itemViewModel.dropDownsObject.valuesList.size() > 0 : false
        flag 7 (0x8L): itemViewModel.dropDownsObject.valuesList != null ? itemViewModel.dropDownsObject.valuesList.size() > 0 : false ? itemViewModel.dropDownsObject.valuesList.get(0).name : ""
        flag 8 (0x9L): itemViewModel.dropDownsObject.valuesList != null ? itemViewModel.dropDownsObject.valuesList.size() > 0 : false ? itemViewModel.dropDownsObject.valuesList.get(0).name : ""
    flag mapping end*/
    //end
}