package grand.app.aber_user.databinding;
import grand.app.aber_user.R;
import grand.app.aber_user.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentOrderServiceDetailsBindingImpl extends FragmentOrderServiceDetailsBinding implements grand.app.aber_user.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.card_info, 35);
        sViewsWithIds.put(R.id.tv_services_name, 36);
        sViewsWithIds.put(R.id.v_services_name, 37);
        sViewsWithIds.put(R.id.tv_service_location, 38);
        sViewsWithIds.put(R.id.v_service_location, 39);
        sViewsWithIds.put(R.id.tv_service_time, 40);
        sViewsWithIds.put(R.id.v_service_status, 41);
        sViewsWithIds.put(R.id.tv_service_status, 42);
        sViewsWithIds.put(R.id.v_service, 43);
        sViewsWithIds.put(R.id.br, 44);
        sViewsWithIds.put(R.id.tv_service_cost, 45);
        sViewsWithIds.put(R.id.flow, 46);
        sViewsWithIds.put(R.id.tv_service_following, 47);
        sViewsWithIds.put(R.id.card_follow_order, 48);
        sViewsWithIds.put(R.id.ic_accept_line, 49);
        sViewsWithIds.put(R.id.ic_on_way_line, 50);
        sViewsWithIds.put(R.id.ic_arrived_line, 51);
        sViewsWithIds.put(R.id.flow2, 52);
        sViewsWithIds.put(R.id.flow3, 53);
    }
    // views
    @NonNull
    private final androidx.core.widget.NestedScrollView mboundView0;
    @NonNull
    private final grand.app.aber_user.customViews.views.CustomTextViewMedium mboundView24;
    @NonNull
    private final androidx.cardview.widget.CardView mboundView29;
    @NonNull
    private final grand.app.aber_user.customViews.views.CustomTextViewRegular mboundView34;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback77;
    @Nullable
    private final android.view.View.OnClickListener mCallback78;
    @Nullable
    private final android.view.View.OnClickListener mCallback74;
    @Nullable
    private final android.view.View.OnClickListener mCallback76;
    @Nullable
    private final android.view.View.OnClickListener mCallback75;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentOrderServiceDetailsBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 54, sIncludes, sViewsWithIds));
    }
    private FragmentOrderServiceDetailsBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 1
            , (androidx.constraintlayout.widget.Barrier) bindings[44]
            , (com.google.android.material.button.MaterialButton) bindings[26]
            , (com.google.android.material.button.MaterialButton) bindings[25]
            , (com.google.android.material.button.MaterialButton) bindings[27]
            , (androidx.cardview.widget.CardView) bindings[48]
            , (androidx.cardview.widget.CardView) bindings[35]
            , (androidx.cardview.widget.CardView) bindings[14]
            , (com.google.android.material.button.MaterialButton) bindings[19]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[33]
            , (androidx.constraintlayout.helper.widget.Flow) bindings[46]
            , (androidx.constraintlayout.helper.widget.Flow) bindings[52]
            , (androidx.constraintlayout.helper.widget.Flow) bindings[53]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[20]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[49]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[22]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[51]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[23]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[21]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[50]
            , (de.hdodenhof.circleimageview.CircleImageView) bindings[15]
            , (com.google.android.material.imageview.ShapeableImageView) bindings[30]
            , (androidx.appcompat.widget.AppCompatRatingBar) bindings[32]
            , (androidx.appcompat.widget.AppCompatRatingBar) bindings[17]
            , (androidx.recyclerview.widget.RecyclerView) bindings[12]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[18]
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[16]
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[28]
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[31]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[5]
            , (com.google.android.material.button.MaterialButton) bindings[7]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[6]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[45]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[9]
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[47]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[38]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[2]
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[13]
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[11]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[42]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[4]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[40]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[3]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[36]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[1]
            , (android.view.View) bindings[8]
            , (android.view.View) bindings[43]
            , (android.view.View) bindings[10]
            , (android.view.View) bindings[39]
            , (android.view.View) bindings[41]
            , (android.view.View) bindings[37]
            );
        this.btnCallProvider.setTag(null);
        this.btnFollowProvider.setTag(null);
        this.btnRateProvider.setTag(null);
        this.cardProvider.setTag(null);
        this.chatBtn.setTag(null);
        this.createAt.setTag(null);
        this.icAccept.setTag(null);
        this.icArrived.setTag(null);
        this.icFinished.setTag(null);
        this.icOnWay.setTag(null);
        this.icProvider.setTag(null);
        this.icProviderRate.setTag(null);
        this.mboundView0 = (androidx.core.widget.NestedScrollView) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView24 = (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[24];
        this.mboundView24.setTag(null);
        this.mboundView29 = (androidx.cardview.widget.CardView) bindings[29];
        this.mboundView29.setTag(null);
        this.mboundView34 = (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[34];
        this.mboundView34.setTag(null);
        this.myRate.setTag(null);
        this.rating.setTag(null);
        this.rcServices.setTag(null);
        this.tvDesc.setTag(null);
        this.tvHomeItem.setTag(null);
        this.tvMyReviewProvider.setTag(null);
        this.tvProviderName.setTag(null);
        this.tvServiceCode.setTag(null);
        this.tvServiceCodeGenerate.setTag(null);
        this.tvServiceCodeValue.setTag(null);
        this.tvServiceCostValue.setTag(null);
        this.tvServiceLocationValue.setTag(null);
        this.tvServiceProvider.setTag(null);
        this.tvServiceRc.setTag(null);
        this.tvServiceStatusValue.setTag(null);
        this.tvServiceTimeValue.setTag(null);
        this.tvServicesNameValue.setTag(null);
        this.vCode.setTag(null);
        this.vServiceCost.setTag(null);
        setRootTag(root);
        // listeners
        mCallback77 = new grand.app.aber_user.generated.callback.OnClickListener(this, 4);
        mCallback78 = new grand.app.aber_user.generated.callback.OnClickListener(this, 5);
        mCallback74 = new grand.app.aber_user.generated.callback.OnClickListener(this, 1);
        mCallback76 = new grand.app.aber_user.generated.callback.OnClickListener(this, 3);
        mCallback75 = new grand.app.aber_user.generated.callback.OnClickListener(this, 2);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x8L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.viewmodel == variableId) {
            setViewmodel((grand.app.aber_user.pages.myOrders.viewModels.MyOrdersServicesViewModels) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setViewmodel(@Nullable grand.app.aber_user.pages.myOrders.viewModels.MyOrdersServicesViewModels Viewmodel) {
        updateRegistration(0, Viewmodel);
        this.mViewmodel = Viewmodel;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.viewmodel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeViewmodel((grand.app.aber_user.pages.myOrders.viewModels.MyOrdersServicesViewModels) object, fieldId);
        }
        return false;
    }
    private boolean onChangeViewmodel(grand.app.aber_user.pages.myOrders.viewModels.MyOrdersServicesViewModels Viewmodel, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        else if (fieldId == BR.myOrderDetails) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        else if (fieldId == BR.servicesRequiredAdapter) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String viewmodelMyOrderDetailsOrdersServiceName = null;
        java.lang.String viewmodelMyOrderDetailsOrdersReviewRate = null;
        boolean viewmodelMyOrderDetailsOrdersReviewJavaLangObjectNull = false;
        boolean viewmodelMyOrderDetailsOrdersProviderJavaLangObjectNullViewmodelMyOrderDetailsOrdersHistoryInt4BooleanFalse = false;
        boolean textUtilsIsEmptyViewmodelMyOrderDetailsOrdersFinishCodeViewmodelMyOrderDetailsOrdersProviderJavaLangObjectNullBooleanFalseViewmodelMyOrderDetailsOrdersHistoryInt3BooleanFalse = false;
        boolean textUtilsIsEmptyViewmodelMyOrderDetailsOrdersServiceName = false;
        java.lang.String viewmodelMyOrderDetailsOrdersProviderName = null;
        double viewmodelMyOrderDetailsOrdersCost = 0.0;
        android.graphics.drawable.Drawable viewmodelMyOrderDetailsOrdersHistoryInt4IcFinishedAndroidDrawableIcFollowSuccessViewmodelMyOrderDetailsOrdersHistoryInt5IcFinishedAndroidDrawableIcNotDoneIcFinishedAndroidDrawableIcFollowWaiting = null;
        java.lang.String viewmodelUserDataImage = null;
        java.lang.String viewmodelMyOrderDetailsOrdersProviderRate = null;
        boolean viewmodelMyOrderDetailsOrdersHistoryInt2 = false;
        java.lang.String viewmodelMyOrderDetailsOrdersScheduledAt = null;
        boolean textUtilsIsEmptyViewmodelMyOrderDetailsOrdersFinishCode = false;
        grand.app.aber_user.pages.myOrders.adapter.ServicesRequiredAdapter viewmodelServicesRequiredAdapter = null;
        java.lang.String viewmodelMyOrderDetailsOrdersFinishCode = null;
        boolean TextUtilsIsEmptyViewmodelMyOrderDetailsOrdersFinishCodeViewmodelMyOrderDetailsOrdersProviderJavaLangObjectNullBooleanFalseViewmodelMyOrderDetailsOrdersHistoryInt3BooleanFalse1 = false;
        boolean viewmodelMyOrderDetailsOrdersHistoryInt3 = false;
        boolean viewmodelMyOrderDetailsOrdersCanceledInt1 = false;
        int textUtilsIsEmptyViewmodelMyOrderDetailsOrdersFinishCodeViewmodelMyOrderDetailsOrdersProviderJavaLangObjectNullBooleanFalseViewmodelMyOrderDetailsOrdersHistoryInt3BooleanFalseViewVISIBLEViewGONE = 0;
        java.lang.String viewmodelMyOrderDetailsOrdersReviewCreatedAt = null;
        java.lang.String viewmodelMyOrderDetailsOrdersHistoryInt5MboundView24AndroidStringOrderNotFinishedMboundView24AndroidStringOrderFinished = null;
        java.lang.String viewmodelMyOrderDetailsOrdersProviderCompany = null;
        android.graphics.drawable.Drawable viewmodelMyOrderDetailsOrdersHistoryInt2IcOnWayAndroidDrawableIcFollowSuccessIcOnWayAndroidDrawableIcFollowWaiting = null;
        int viewmodelServicesRequiredAdapterItemCountInt0ViewINVISIBLEViewVISIBLE = 0;
        boolean viewmodelMyOrderDetailsOrdersHistoryInt0 = false;
        android.graphics.drawable.Drawable viewmodelMyOrderDetailsOrdersHistoryInt5IcFinishedAndroidDrawableIcNotDoneIcFinishedAndroidDrawableIcFollowWaiting = null;
        boolean viewmodelMyOrderDetailsOrdersHistoryInt0BooleanTrueViewmodelMyOrderDetailsOrdersHistoryInt4 = false;
        java.lang.String viewmodelMyOrderDetailsOrdersReviewComment = null;
        boolean textUtilsIsEmptyViewmodelMyOrderDetailsOrdersFinishCodeViewmodelMyOrderDetailsOrdersProviderJavaLangObjectNullBooleanFalse = false;
        boolean TextUtilsIsEmptyViewmodelMyOrderDetailsOrdersServiceName1 = false;
        android.graphics.drawable.Drawable viewmodelMyOrderDetailsOrdersHistoryInt1IcAcceptAndroidDrawableIcFollowSuccessIcAcceptAndroidDrawableIcFollowWaiting = null;
        java.lang.String stringValueOfViewmodelMyOrderDetailsOrdersCostConcatJavaLangString = null;
        int viewmodelServicesRequiredAdapterItemCount = 0;
        int textUtilsIsEmptyViewmodelMyOrderDetailsOrdersServiceNameViewVISIBLEViewGONE = 0;
        grand.app.aber_user.pages.myOrders.viewModels.MyOrdersServicesViewModels viewmodel = mViewmodel;
        int viewmodelMyOrderDetailsOrdersProviderJavaLangObjectNullViewmodelMyOrderDetailsOrdersHistoryInt4BooleanFalseViewGONEViewVISIBLE = 0;
        boolean viewmodelMyOrderDetailsOrdersHistoryInt5 = false;
        int viewmodelServicesRequiredAdapterItemCountInt0ViewGONEViewVISIBLE = 0;
        boolean viewmodelMyOrderDetailsOrdersIsEmergencyInt1 = false;
        java.lang.String viewmodelMyOrderDetailsOrdersStatus = null;
        java.lang.String stringValueOfViewmodelMyOrderDetailsOrdersCostConcatJavaLangStringConcatViewmodelCurrency = null;
        grand.app.aber_user.pages.auth.models.UserData viewmodelMyOrderDetailsOrdersProvider = null;
        int viewmodelMyOrderDetailsOrdersHistoryInt0ViewGONEViewVISIBLE = 0;
        boolean ViewmodelMyOrderDetailsOrdersHistoryInt31 = false;
        boolean TextUtilsIsEmptyViewmodelMyOrderDetailsOrdersFinishCodeViewmodelMyOrderDetailsOrdersProviderJavaLangObjectNullBooleanFalse1 = false;
        boolean viewmodelMyOrderDetailsOrdersProviderJavaLangObjectNullViewmodelMyOrderDetailsOrdersHistoryInt3BooleanFalse = false;
        java.lang.String viewmodelMyOrderDetailsOrdersProviderImage = null;
        grand.app.aber_user.pages.reviews.models.RatesItem viewmodelMyOrderDetailsOrdersReview = null;
        java.lang.String viewmodelCurrency = null;
        int viewmodelMyOrderDetailsOrdersReviewJavaLangObjectNullViewmodelMyOrderDetailsOrdersHistoryInt4BooleanFalseViewVISIBLEViewGONE = 0;
        boolean viewmodelMyOrderDetailsOrdersHistoryInt0BooleanTrueViewmodelMyOrderDetailsOrdersHistoryInt4BooleanTrueViewmodelMyOrderDetailsOrdersHistoryInt5 = false;
        int TextUtilsIsEmptyViewmodelMyOrderDetailsOrdersFinishCodeViewmodelMyOrderDetailsOrdersProviderJavaLangObjectNullBooleanFalseViewmodelMyOrderDetailsOrdersHistoryInt3BooleanFalseViewVISIBLEViewGONE1 = 0;
        int viewmodelMyOrderDetailsOrdersHistory = 0;
        android.graphics.drawable.Drawable viewmodelMyOrderDetailsOrdersHistoryInt3IcArrivedAndroidDrawableIcFollowSuccessIcArrivedAndroidDrawableIcFollowWaiting = null;
        boolean viewmodelMyOrderDetailsOrdersProviderJavaLangObjectNull = false;
        int viewmodelMyOrderDetailsOrdersCanceled = 0;
        int viewmodelMyOrderDetailsOrdersReviewJavaLangObjectNullViewGONEViewVISIBLE = 0;
        int viewmodelMyOrderDetailsOrdersProviderJavaLangObjectNullViewmodelMyOrderDetailsOrdersHistoryInt3BooleanFalseViewVISIBLEViewGONE = 0;
        boolean viewmodelMyOrderDetailsOrdersHistoryInt0BooleanTrueViewmodelMyOrderDetailsOrdersHistoryInt4BooleanTrueViewmodelMyOrderDetailsOrdersHistoryInt5BooleanTrueViewmodelMyOrderDetailsOrdersCanceledInt1 = false;
        java.lang.String viewmodelMyOrderDetailsOrdersAddress = null;
        boolean viewmodelMyOrderDetailsOrdersHistoryInt1 = false;
        boolean viewmodelMyOrderDetailsOrdersReviewJavaLangObjectNullViewmodelMyOrderDetailsOrdersHistoryInt4BooleanFalse = false;
        java.lang.String stringValueOfViewmodelMyOrderDetailsOrdersCost = null;
        java.lang.String viewmodelUserDataName = null;
        grand.app.aber_user.pages.auth.models.UserData viewmodelUserData = null;
        boolean viewmodelServicesRequiredAdapterItemCountInt0 = false;
        boolean viewmodelMyOrderDetailsOrdersHistoryInt4 = false;
        boolean TextUtilsIsEmptyViewmodelMyOrderDetailsOrdersFinishCode1 = false;
        int viewmodelMyOrderDetailsOrdersHistoryInt0BooleanTrueViewmodelMyOrderDetailsOrdersHistoryInt4BooleanTrueViewmodelMyOrderDetailsOrdersHistoryInt5BooleanTrueViewmodelMyOrderDetailsOrdersCanceledInt1ViewGONEViewVISIBLE = 0;
        grand.app.aber_user.pages.myOrders.models.orderServices.OrderDetailsMain viewmodelMyOrderDetails = null;
        int viewmodelMyOrderDetailsOrdersIsEmergency = 0;
        java.lang.String viewmodelMyOrderDetailsOrdersIsEmergencyInt1TvServiceTimeValueAndroidStringEmergencyServicesViewmodelMyOrderDetailsOrdersScheduledAt = null;
        boolean ViewmodelMyOrderDetailsOrdersHistoryInt41 = false;
        grand.app.aber_user.pages.myOrders.models.orderServices.ServicesOrderDetails viewmodelMyOrderDetailsOrders = null;
        java.lang.String viewmodelMyOrderDetailsOrdersHistoryInt4MboundView24AndroidStringOrderFinishedViewmodelMyOrderDetailsOrdersHistoryInt5MboundView24AndroidStringOrderNotFinishedMboundView24AndroidStringOrderFinished = null;

        if ((dirtyFlags & 0xfL) != 0) {


            if ((dirtyFlags & 0xdL) != 0) {

                    if (viewmodel != null) {
                        // read viewmodel.servicesRequiredAdapter
                        viewmodelServicesRequiredAdapter = viewmodel.getServicesRequiredAdapter();
                    }


                    if (viewmodelServicesRequiredAdapter != null) {
                        // read viewmodel.servicesRequiredAdapter.itemCount
                        viewmodelServicesRequiredAdapterItemCount = viewmodelServicesRequiredAdapter.getItemCount();
                    }


                    // read viewmodel.servicesRequiredAdapter.itemCount == 0
                    viewmodelServicesRequiredAdapterItemCountInt0 = (viewmodelServicesRequiredAdapterItemCount) == (0);
                if((dirtyFlags & 0xdL) != 0) {
                    if(viewmodelServicesRequiredAdapterItemCountInt0) {
                            dirtyFlags |= 0x80000L;
                            dirtyFlags |= 0x200000000L;
                    }
                    else {
                            dirtyFlags |= 0x40000L;
                            dirtyFlags |= 0x100000000L;
                    }
                }


                    // read viewmodel.servicesRequiredAdapter.itemCount == 0 ? View.INVISIBLE : View.VISIBLE
                    viewmodelServicesRequiredAdapterItemCountInt0ViewINVISIBLEViewVISIBLE = ((viewmodelServicesRequiredAdapterItemCountInt0) ? (android.view.View.INVISIBLE) : (android.view.View.VISIBLE));
                    // read viewmodel.servicesRequiredAdapter.itemCount == 0 ? View.GONE : View.VISIBLE
                    viewmodelServicesRequiredAdapterItemCountInt0ViewGONEViewVISIBLE = ((viewmodelServicesRequiredAdapterItemCountInt0) ? (android.view.View.GONE) : (android.view.View.VISIBLE));
            }
            if ((dirtyFlags & 0xbL) != 0) {

                    if (viewmodel != null) {
                        // read viewmodel.currency
                        viewmodelCurrency = viewmodel.currency;
                        // read viewmodel.myOrderDetails
                        viewmodelMyOrderDetails = viewmodel.getMyOrderDetails();
                    }


                    if (viewmodelMyOrderDetails != null) {
                        // read viewmodel.myOrderDetails.orders
                        viewmodelMyOrderDetailsOrders = viewmodelMyOrderDetails.getOrders();
                    }


                    if (viewmodelMyOrderDetailsOrders != null) {
                        // read viewmodel.myOrderDetails.orders.serviceName
                        viewmodelMyOrderDetailsOrdersServiceName = viewmodelMyOrderDetailsOrders.getServiceName();
                        // read viewmodel.myOrderDetails.orders.cost
                        viewmodelMyOrderDetailsOrdersCost = viewmodelMyOrderDetailsOrders.getCost();
                        // read viewmodel.myOrderDetails.orders.finishCode
                        viewmodelMyOrderDetailsOrdersFinishCode = viewmodelMyOrderDetailsOrders.getFinishCode();
                        // read viewmodel.myOrderDetails.orders.status
                        viewmodelMyOrderDetailsOrdersStatus = viewmodelMyOrderDetailsOrders.getStatus();
                        // read viewmodel.myOrderDetails.orders.provider
                        viewmodelMyOrderDetailsOrdersProvider = viewmodelMyOrderDetailsOrders.getProvider();
                        // read viewmodel.myOrderDetails.orders.review
                        viewmodelMyOrderDetailsOrdersReview = viewmodelMyOrderDetailsOrders.getReview();
                        // read viewmodel.myOrderDetails.orders.history
                        viewmodelMyOrderDetailsOrdersHistory = viewmodelMyOrderDetailsOrders.getHistory();
                        // read viewmodel.myOrderDetails.orders.address
                        viewmodelMyOrderDetailsOrdersAddress = viewmodelMyOrderDetailsOrders.getAddress();
                        // read viewmodel.myOrderDetails.orders.isEmergency
                        viewmodelMyOrderDetailsOrdersIsEmergency = viewmodelMyOrderDetailsOrders.getIsEmergency();
                    }


                    // read TextUtils.isEmpty(viewmodel.myOrderDetails.orders.serviceName)
                    textUtilsIsEmptyViewmodelMyOrderDetailsOrdersServiceName = android.text.TextUtils.isEmpty(viewmodelMyOrderDetailsOrdersServiceName);
                    // read String.valueOf(viewmodel.myOrderDetails.orders.cost)
                    stringValueOfViewmodelMyOrderDetailsOrdersCost = java.lang.String.valueOf(viewmodelMyOrderDetailsOrdersCost);
                    // read TextUtils.isEmpty(viewmodel.myOrderDetails.orders.finishCode)
                    textUtilsIsEmptyViewmodelMyOrderDetailsOrdersFinishCode = android.text.TextUtils.isEmpty(viewmodelMyOrderDetailsOrdersFinishCode);
                    // read viewmodel.myOrderDetails.orders.provider != null
                    viewmodelMyOrderDetailsOrdersProviderJavaLangObjectNull = (viewmodelMyOrderDetailsOrdersProvider) != (null);
                    // read viewmodel.myOrderDetails.orders.review == null
                    viewmodelMyOrderDetailsOrdersReviewJavaLangObjectNull = (viewmodelMyOrderDetailsOrdersReview) == (null);
                    // read viewmodel.myOrderDetails.orders.history >= 2
                    viewmodelMyOrderDetailsOrdersHistoryInt2 = (viewmodelMyOrderDetailsOrdersHistory) >= (2);
                    // read viewmodel.myOrderDetails.orders.history == 0
                    viewmodelMyOrderDetailsOrdersHistoryInt0 = (viewmodelMyOrderDetailsOrdersHistory) == (0);
                    // read viewmodel.myOrderDetails.orders.history >= 3
                    ViewmodelMyOrderDetailsOrdersHistoryInt31 = (viewmodelMyOrderDetailsOrdersHistory) >= (3);
                    // read viewmodel.myOrderDetails.orders.history >= 1
                    viewmodelMyOrderDetailsOrdersHistoryInt1 = (viewmodelMyOrderDetailsOrdersHistory) >= (1);
                    // read viewmodel.myOrderDetails.orders.history == 4
                    viewmodelMyOrderDetailsOrdersHistoryInt4 = (viewmodelMyOrderDetailsOrdersHistory) == (4);
                    // read viewmodel.myOrderDetails.orders.isEmergency == 1
                    viewmodelMyOrderDetailsOrdersIsEmergencyInt1 = (viewmodelMyOrderDetailsOrdersIsEmergency) == (1);
                if((dirtyFlags & 0xbL) != 0) {
                    if(textUtilsIsEmptyViewmodelMyOrderDetailsOrdersFinishCode) {
                            dirtyFlags |= 0x2000000000L;
                    }
                    else {
                            dirtyFlags |= 0x1000000000L;
                    }
                }
                if((dirtyFlags & 0xbL) != 0) {
                    if(viewmodelMyOrderDetailsOrdersProviderJavaLangObjectNull) {
                            dirtyFlags |= 0x20L;
                            dirtyFlags |= 0x8000000000L;
                    }
                    else {
                            dirtyFlags |= 0x10L;
                            dirtyFlags |= 0x4000000000L;
                    }
                }
                if((dirtyFlags & 0xbL) != 0) {
                    if(viewmodelMyOrderDetailsOrdersReviewJavaLangObjectNull) {
                            dirtyFlags |= 0x2000000000000L;
                            dirtyFlags |= 0x80000000000000L;
                    }
                    else {
                            dirtyFlags |= 0x1000000000000L;
                            dirtyFlags |= 0x40000000000000L;
                    }
                }
                if((dirtyFlags & 0xbL) != 0) {
                    if(viewmodelMyOrderDetailsOrdersHistoryInt2) {
                            dirtyFlags |= 0x20000L;
                    }
                    else {
                            dirtyFlags |= 0x10000L;
                    }
                }
                if((dirtyFlags & 0xbL) != 0) {
                    if(viewmodelMyOrderDetailsOrdersHistoryInt0) {
                            dirtyFlags |= 0x800000L;
                            dirtyFlags |= 0x800000000L;
                    }
                    else {
                            dirtyFlags |= 0x400000L;
                            dirtyFlags |= 0x400000000L;
                    }
                }
                if((dirtyFlags & 0xbL) != 0) {
                    if(ViewmodelMyOrderDetailsOrdersHistoryInt31) {
                            dirtyFlags |= 0x800000000000L;
                    }
                    else {
                            dirtyFlags |= 0x400000000000L;
                    }
                }
                if((dirtyFlags & 0xbL) != 0) {
                    if(viewmodelMyOrderDetailsOrdersHistoryInt1) {
                            dirtyFlags |= 0x8000000L;
                    }
                    else {
                            dirtyFlags |= 0x4000000L;
                    }
                }
                if((dirtyFlags & 0xbL) != 0) {
                    if(viewmodelMyOrderDetailsOrdersHistoryInt4) {
                            dirtyFlags |= 0x200L;
                            dirtyFlags |= 0x2000000000000000L;
                    }
                    else {
                            dirtyFlags |= 0x100L;
                            dirtyFlags |= 0x1000000000000000L;
                    }
                }
                if((dirtyFlags & 0xbL) != 0) {
                    if(viewmodelMyOrderDetailsOrdersIsEmergencyInt1) {
                            dirtyFlags |= 0x800000000000000L;
                    }
                    else {
                            dirtyFlags |= 0x400000000000000L;
                    }
                }
                    if (viewmodelMyOrderDetailsOrdersProvider != null) {
                        // read viewmodel.myOrderDetails.orders.provider.name
                        viewmodelMyOrderDetailsOrdersProviderName = viewmodelMyOrderDetailsOrdersProvider.getName();
                        // read viewmodel.myOrderDetails.orders.provider.rate
                        viewmodelMyOrderDetailsOrdersProviderRate = viewmodelMyOrderDetailsOrdersProvider.getRate();
                        // read viewmodel.myOrderDetails.orders.provider.company
                        viewmodelMyOrderDetailsOrdersProviderCompany = viewmodelMyOrderDetailsOrdersProvider.getCompany();
                        // read viewmodel.myOrderDetails.orders.provider.image
                        viewmodelMyOrderDetailsOrdersProviderImage = viewmodelMyOrderDetailsOrdersProvider.getImage();
                    }
                    if (viewmodelMyOrderDetailsOrdersReview != null) {
                        // read viewmodel.myOrderDetails.orders.review.rate
                        viewmodelMyOrderDetailsOrdersReviewRate = viewmodelMyOrderDetailsOrdersReview.getRate();
                        // read viewmodel.myOrderDetails.orders.review.createdAt
                        viewmodelMyOrderDetailsOrdersReviewCreatedAt = viewmodelMyOrderDetailsOrdersReview.getCreatedAt();
                        // read viewmodel.myOrderDetails.orders.review.comment
                        viewmodelMyOrderDetailsOrdersReviewComment = viewmodelMyOrderDetailsOrdersReview.getComment();
                    }


                    // read !TextUtils.isEmpty(viewmodel.myOrderDetails.orders.serviceName)
                    TextUtilsIsEmptyViewmodelMyOrderDetailsOrdersServiceName1 = !textUtilsIsEmptyViewmodelMyOrderDetailsOrdersServiceName;
                    // read !TextUtils.isEmpty(viewmodel.myOrderDetails.orders.finishCode)
                    TextUtilsIsEmptyViewmodelMyOrderDetailsOrdersFinishCode1 = !textUtilsIsEmptyViewmodelMyOrderDetailsOrdersFinishCode;
                    // read viewmodel.myOrderDetails.orders.review == null ? View.GONE : View.VISIBLE
                    viewmodelMyOrderDetailsOrdersReviewJavaLangObjectNullViewGONEViewVISIBLE = ((viewmodelMyOrderDetailsOrdersReviewJavaLangObjectNull) ? (android.view.View.GONE) : (android.view.View.VISIBLE));
                    // read viewmodel.myOrderDetails.orders.history >= 2 ? @android:drawable/ic_follow_success : @android:drawable/ic_follow_waiting
                    viewmodelMyOrderDetailsOrdersHistoryInt2IcOnWayAndroidDrawableIcFollowSuccessIcOnWayAndroidDrawableIcFollowWaiting = ((viewmodelMyOrderDetailsOrdersHistoryInt2) ? (androidx.appcompat.content.res.AppCompatResources.getDrawable(icOnWay.getContext(), R.drawable.ic_follow_success)) : (androidx.appcompat.content.res.AppCompatResources.getDrawable(icOnWay.getContext(), R.drawable.ic_follow_waiting)));
                    // read viewmodel.myOrderDetails.orders.history == 0 ? View.GONE : View.VISIBLE
                    viewmodelMyOrderDetailsOrdersHistoryInt0ViewGONEViewVISIBLE = ((viewmodelMyOrderDetailsOrdersHistoryInt0) ? (android.view.View.GONE) : (android.view.View.VISIBLE));
                    // read viewmodel.myOrderDetails.orders.history >= 3 ? @android:drawable/ic_follow_success : @android:drawable/ic_follow_waiting
                    viewmodelMyOrderDetailsOrdersHistoryInt3IcArrivedAndroidDrawableIcFollowSuccessIcArrivedAndroidDrawableIcFollowWaiting = ((ViewmodelMyOrderDetailsOrdersHistoryInt31) ? (androidx.appcompat.content.res.AppCompatResources.getDrawable(icArrived.getContext(), R.drawable.ic_follow_success)) : (androidx.appcompat.content.res.AppCompatResources.getDrawable(icArrived.getContext(), R.drawable.ic_follow_waiting)));
                    // read viewmodel.myOrderDetails.orders.history >= 1 ? @android:drawable/ic_follow_success : @android:drawable/ic_follow_waiting
                    viewmodelMyOrderDetailsOrdersHistoryInt1IcAcceptAndroidDrawableIcFollowSuccessIcAcceptAndroidDrawableIcFollowWaiting = ((viewmodelMyOrderDetailsOrdersHistoryInt1) ? (androidx.appcompat.content.res.AppCompatResources.getDrawable(icAccept.getContext(), R.drawable.ic_follow_success)) : (androidx.appcompat.content.res.AppCompatResources.getDrawable(icAccept.getContext(), R.drawable.ic_follow_waiting)));
                if((dirtyFlags & 0xbL) != 0) {
                    if(TextUtilsIsEmptyViewmodelMyOrderDetailsOrdersServiceName1) {
                            dirtyFlags |= 0x20000000L;
                    }
                    else {
                            dirtyFlags |= 0x10000000L;
                    }
                }
                if((dirtyFlags & 0xbL) != 0) {
                    if(TextUtilsIsEmptyViewmodelMyOrderDetailsOrdersFinishCode1) {
                            dirtyFlags |= 0x2000000L;
                    }
                    else {
                            dirtyFlags |= 0x1000000L;
                    }
                }
                    if (stringValueOfViewmodelMyOrderDetailsOrdersCost != null) {
                        // read String.valueOf(viewmodel.myOrderDetails.orders.cost).concat(" ")
                        stringValueOfViewmodelMyOrderDetailsOrdersCostConcatJavaLangString = stringValueOfViewmodelMyOrderDetailsOrdersCost.concat(" ");
                    }


                    // read !TextUtils.isEmpty(viewmodel.myOrderDetails.orders.serviceName) ? View.VISIBLE : View.GONE
                    textUtilsIsEmptyViewmodelMyOrderDetailsOrdersServiceNameViewVISIBLEViewGONE = ((TextUtilsIsEmptyViewmodelMyOrderDetailsOrdersServiceName1) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
                    if (stringValueOfViewmodelMyOrderDetailsOrdersCostConcatJavaLangString != null) {
                        // read String.valueOf(viewmodel.myOrderDetails.orders.cost).concat(" ").concat(viewmodel.currency)
                        stringValueOfViewmodelMyOrderDetailsOrdersCostConcatJavaLangStringConcatViewmodelCurrency = stringValueOfViewmodelMyOrderDetailsOrdersCostConcatJavaLangString.concat(viewmodelCurrency);
                    }
            }
            if ((dirtyFlags & 0x9L) != 0) {

                    if (viewmodel != null) {
                        // read viewmodel.userData
                        viewmodelUserData = viewmodel.userData;
                    }


                    if (viewmodelUserData != null) {
                        // read viewmodel.userData.image
                        viewmodelUserDataImage = viewmodelUserData.getImage();
                        // read viewmodel.userData.name
                        viewmodelUserDataName = viewmodelUserData.getName();
                    }
            }
        }
        // batch finished

        if ((dirtyFlags & 0x400000000000000L) != 0) {

                if (viewmodelMyOrderDetailsOrders != null) {
                    // read viewmodel.myOrderDetails.orders.scheduledAt
                    viewmodelMyOrderDetailsOrdersScheduledAt = viewmodelMyOrderDetailsOrders.getScheduledAt();
                }
        }
        if ((dirtyFlags & 0x8000000000L) != 0) {

                // read viewmodel.myOrderDetails.orders.history == 3
                viewmodelMyOrderDetailsOrdersHistoryInt3 = (viewmodelMyOrderDetailsOrdersHistory) == (3);
        }
        if ((dirtyFlags & 0xbL) != 0) {

                // read viewmodel.myOrderDetails.orders.history == 0 ? true : viewmodel.myOrderDetails.orders.history == 4
                viewmodelMyOrderDetailsOrdersHistoryInt0BooleanTrueViewmodelMyOrderDetailsOrdersHistoryInt4 = ((viewmodelMyOrderDetailsOrdersHistoryInt0) ? (true) : (viewmodelMyOrderDetailsOrdersHistoryInt4));
                // read !TextUtils.isEmpty(viewmodel.myOrderDetails.orders.finishCode) ? viewmodel.myOrderDetails.orders.provider != null : false
                textUtilsIsEmptyViewmodelMyOrderDetailsOrdersFinishCodeViewmodelMyOrderDetailsOrdersProviderJavaLangObjectNullBooleanFalse = ((TextUtilsIsEmptyViewmodelMyOrderDetailsOrdersFinishCode1) ? (viewmodelMyOrderDetailsOrdersProviderJavaLangObjectNull) : (false));
                // read TextUtils.isEmpty(viewmodel.myOrderDetails.orders.finishCode) ? viewmodel.myOrderDetails.orders.provider != null : false
                TextUtilsIsEmptyViewmodelMyOrderDetailsOrdersFinishCodeViewmodelMyOrderDetailsOrdersProviderJavaLangObjectNullBooleanFalse1 = ((textUtilsIsEmptyViewmodelMyOrderDetailsOrdersFinishCode) ? (viewmodelMyOrderDetailsOrdersProviderJavaLangObjectNull) : (false));
            if((dirtyFlags & 0xbL) != 0) {
                if(viewmodelMyOrderDetailsOrdersHistoryInt0BooleanTrueViewmodelMyOrderDetailsOrdersHistoryInt4) {
                        dirtyFlags |= 0x80000000000L;
                }
                else {
                        dirtyFlags |= 0x40000000000L;
                }
            }
            if((dirtyFlags & 0xbL) != 0) {
                if(textUtilsIsEmptyViewmodelMyOrderDetailsOrdersFinishCodeViewmodelMyOrderDetailsOrdersProviderJavaLangObjectNullBooleanFalse) {
                        dirtyFlags |= 0x800L;
                }
                else {
                        dirtyFlags |= 0x400L;
                }
            }
            if((dirtyFlags & 0xbL) != 0) {
                if(TextUtilsIsEmptyViewmodelMyOrderDetailsOrdersFinishCodeViewmodelMyOrderDetailsOrdersProviderJavaLangObjectNullBooleanFalse1) {
                        dirtyFlags |= 0x80L;
                }
                else {
                        dirtyFlags |= 0x40L;
                }
            }
        }
        if ((dirtyFlags & 0x1000000000000100L) != 0) {

                // read viewmodel.myOrderDetails.orders.history == 5
                viewmodelMyOrderDetailsOrdersHistoryInt5 = (viewmodelMyOrderDetailsOrdersHistory) == (5);
            if((dirtyFlags & 0x1000000000000000L) != 0) {
                if(viewmodelMyOrderDetailsOrdersHistoryInt5) {
                        dirtyFlags |= 0x8000L;
                }
                else {
                        dirtyFlags |= 0x4000L;
                }
            }
            if((dirtyFlags & 0x100L) != 0) {
                if(viewmodelMyOrderDetailsOrdersHistoryInt5) {
                        dirtyFlags |= 0x200000L;
                }
                else {
                        dirtyFlags |= 0x100000L;
                }
            }

            if ((dirtyFlags & 0x1000000000000000L) != 0) {

                    // read viewmodel.myOrderDetails.orders.history == 5 ? @android:string/order_not_finished : @android:string/order_finished
                    viewmodelMyOrderDetailsOrdersHistoryInt5MboundView24AndroidStringOrderNotFinishedMboundView24AndroidStringOrderFinished = ((viewmodelMyOrderDetailsOrdersHistoryInt5) ? (mboundView24.getResources().getString(R.string.order_not_finished)) : (mboundView24.getResources().getString(R.string.order_finished)));
            }
            if ((dirtyFlags & 0x100L) != 0) {

                    // read viewmodel.myOrderDetails.orders.history == 5 ? @android:drawable/ic_not_done : @android:drawable/ic_follow_waiting
                    viewmodelMyOrderDetailsOrdersHistoryInt5IcFinishedAndroidDrawableIcNotDoneIcFinishedAndroidDrawableIcFollowWaiting = ((viewmodelMyOrderDetailsOrdersHistoryInt5) ? (androidx.appcompat.content.res.AppCompatResources.getDrawable(icFinished.getContext(), R.drawable.ic_not_done)) : (androidx.appcompat.content.res.AppCompatResources.getDrawable(icFinished.getContext(), R.drawable.ic_follow_waiting)));
            }
        }
        if ((dirtyFlags & 0x80000000000020L) != 0) {

                // read viewmodel.myOrderDetails.orders.history >= 4
                ViewmodelMyOrderDetailsOrdersHistoryInt41 = (viewmodelMyOrderDetailsOrdersHistory) >= (4);
        }

        if ((dirtyFlags & 0xbL) != 0) {

                // read viewmodel.myOrderDetails.orders.provider != null ? viewmodel.myOrderDetails.orders.history >= 4 : false
                viewmodelMyOrderDetailsOrdersProviderJavaLangObjectNullViewmodelMyOrderDetailsOrdersHistoryInt4BooleanFalse = ((viewmodelMyOrderDetailsOrdersProviderJavaLangObjectNull) ? (ViewmodelMyOrderDetailsOrdersHistoryInt41) : (false));
                // read viewmodel.myOrderDetails.orders.history == 4 ? @android:drawable/ic_follow_success : viewmodel.myOrderDetails.orders.history == 5 ? @android:drawable/ic_not_done : @android:drawable/ic_follow_waiting
                viewmodelMyOrderDetailsOrdersHistoryInt4IcFinishedAndroidDrawableIcFollowSuccessViewmodelMyOrderDetailsOrdersHistoryInt5IcFinishedAndroidDrawableIcNotDoneIcFinishedAndroidDrawableIcFollowWaiting = ((viewmodelMyOrderDetailsOrdersHistoryInt4) ? (androidx.appcompat.content.res.AppCompatResources.getDrawable(icFinished.getContext(), R.drawable.ic_follow_success)) : (viewmodelMyOrderDetailsOrdersHistoryInt5IcFinishedAndroidDrawableIcNotDoneIcFinishedAndroidDrawableIcFollowWaiting));
                // read viewmodel.myOrderDetails.orders.provider != null ? viewmodel.myOrderDetails.orders.history == 3 : false
                viewmodelMyOrderDetailsOrdersProviderJavaLangObjectNullViewmodelMyOrderDetailsOrdersHistoryInt3BooleanFalse = ((viewmodelMyOrderDetailsOrdersProviderJavaLangObjectNull) ? (viewmodelMyOrderDetailsOrdersHistoryInt3) : (false));
                // read viewmodel.myOrderDetails.orders.review == null ? viewmodel.myOrderDetails.orders.history >= 4 : false
                viewmodelMyOrderDetailsOrdersReviewJavaLangObjectNullViewmodelMyOrderDetailsOrdersHistoryInt4BooleanFalse = ((viewmodelMyOrderDetailsOrdersReviewJavaLangObjectNull) ? (ViewmodelMyOrderDetailsOrdersHistoryInt41) : (false));
                // read viewmodel.myOrderDetails.orders.isEmergency == 1 ? @android:string/emergency_services : viewmodel.myOrderDetails.orders.scheduledAt
                viewmodelMyOrderDetailsOrdersIsEmergencyInt1TvServiceTimeValueAndroidStringEmergencyServicesViewmodelMyOrderDetailsOrdersScheduledAt = ((viewmodelMyOrderDetailsOrdersIsEmergencyInt1) ? (tvServiceTimeValue.getResources().getString(R.string.emergency_services)) : (viewmodelMyOrderDetailsOrdersScheduledAt));
                // read viewmodel.myOrderDetails.orders.history == 4 ? @android:string/order_finished : viewmodel.myOrderDetails.orders.history == 5 ? @android:string/order_not_finished : @android:string/order_finished
                viewmodelMyOrderDetailsOrdersHistoryInt4MboundView24AndroidStringOrderFinishedViewmodelMyOrderDetailsOrdersHistoryInt5MboundView24AndroidStringOrderNotFinishedMboundView24AndroidStringOrderFinished = ((viewmodelMyOrderDetailsOrdersHistoryInt4) ? (mboundView24.getResources().getString(R.string.order_finished)) : (viewmodelMyOrderDetailsOrdersHistoryInt5MboundView24AndroidStringOrderNotFinishedMboundView24AndroidStringOrderFinished));
            if((dirtyFlags & 0xbL) != 0) {
                if(viewmodelMyOrderDetailsOrdersProviderJavaLangObjectNullViewmodelMyOrderDetailsOrdersHistoryInt4BooleanFalse) {
                        dirtyFlags |= 0x80000000L;
                }
                else {
                        dirtyFlags |= 0x40000000L;
                }
            }
            if((dirtyFlags & 0xbL) != 0) {
                if(viewmodelMyOrderDetailsOrdersProviderJavaLangObjectNullViewmodelMyOrderDetailsOrdersHistoryInt3BooleanFalse) {
                        dirtyFlags |= 0x8000000000000L;
                }
                else {
                        dirtyFlags |= 0x4000000000000L;
                }
            }
            if((dirtyFlags & 0xbL) != 0) {
                if(viewmodelMyOrderDetailsOrdersReviewJavaLangObjectNullViewmodelMyOrderDetailsOrdersHistoryInt4BooleanFalse) {
                        dirtyFlags |= 0x20000000000L;
                }
                else {
                        dirtyFlags |= 0x10000000000L;
                }
            }


                // read viewmodel.myOrderDetails.orders.provider != null ? viewmodel.myOrderDetails.orders.history >= 4 : false ? View.GONE : View.VISIBLE
                viewmodelMyOrderDetailsOrdersProviderJavaLangObjectNullViewmodelMyOrderDetailsOrdersHistoryInt4BooleanFalseViewGONEViewVISIBLE = ((viewmodelMyOrderDetailsOrdersProviderJavaLangObjectNullViewmodelMyOrderDetailsOrdersHistoryInt4BooleanFalse) ? (android.view.View.GONE) : (android.view.View.VISIBLE));
                // read viewmodel.myOrderDetails.orders.provider != null ? viewmodel.myOrderDetails.orders.history == 3 : false ? View.VISIBLE : View.GONE
                viewmodelMyOrderDetailsOrdersProviderJavaLangObjectNullViewmodelMyOrderDetailsOrdersHistoryInt3BooleanFalseViewVISIBLEViewGONE = ((viewmodelMyOrderDetailsOrdersProviderJavaLangObjectNullViewmodelMyOrderDetailsOrdersHistoryInt3BooleanFalse) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
                // read viewmodel.myOrderDetails.orders.review == null ? viewmodel.myOrderDetails.orders.history >= 4 : false ? View.VISIBLE : View.GONE
                viewmodelMyOrderDetailsOrdersReviewJavaLangObjectNullViewmodelMyOrderDetailsOrdersHistoryInt4BooleanFalseViewVISIBLEViewGONE = ((viewmodelMyOrderDetailsOrdersReviewJavaLangObjectNullViewmodelMyOrderDetailsOrdersHistoryInt4BooleanFalse) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
        }
        // batch finished

        if ((dirtyFlags & 0x880L) != 0) {

                // read viewmodel.myOrderDetails.orders.history == 3
                viewmodelMyOrderDetailsOrdersHistoryInt3 = (viewmodelMyOrderDetailsOrdersHistory) == (3);
        }
        if ((dirtyFlags & 0x40000000000L) != 0) {

                // read viewmodel.myOrderDetails.orders.history == 5
                viewmodelMyOrderDetailsOrdersHistoryInt5 = (viewmodelMyOrderDetailsOrdersHistory) == (5);
            if((dirtyFlags & 0x1000000000000000L) != 0) {
                if(viewmodelMyOrderDetailsOrdersHistoryInt5) {
                        dirtyFlags |= 0x8000L;
                }
                else {
                        dirtyFlags |= 0x4000L;
                }
            }
            if((dirtyFlags & 0x100L) != 0) {
                if(viewmodelMyOrderDetailsOrdersHistoryInt5) {
                        dirtyFlags |= 0x200000L;
                }
                else {
                        dirtyFlags |= 0x100000L;
                }
            }
        }

        if ((dirtyFlags & 0xbL) != 0) {

                // read TextUtils.isEmpty(viewmodel.myOrderDetails.orders.finishCode) ? viewmodel.myOrderDetails.orders.provider != null : false ? viewmodel.myOrderDetails.orders.history == 3 : false
                textUtilsIsEmptyViewmodelMyOrderDetailsOrdersFinishCodeViewmodelMyOrderDetailsOrdersProviderJavaLangObjectNullBooleanFalseViewmodelMyOrderDetailsOrdersHistoryInt3BooleanFalse = ((TextUtilsIsEmptyViewmodelMyOrderDetailsOrdersFinishCodeViewmodelMyOrderDetailsOrdersProviderJavaLangObjectNullBooleanFalse1) ? (viewmodelMyOrderDetailsOrdersHistoryInt3) : (false));
                // read !TextUtils.isEmpty(viewmodel.myOrderDetails.orders.finishCode) ? viewmodel.myOrderDetails.orders.provider != null : false ? viewmodel.myOrderDetails.orders.history == 3 : false
                TextUtilsIsEmptyViewmodelMyOrderDetailsOrdersFinishCodeViewmodelMyOrderDetailsOrdersProviderJavaLangObjectNullBooleanFalseViewmodelMyOrderDetailsOrdersHistoryInt3BooleanFalse1 = ((textUtilsIsEmptyViewmodelMyOrderDetailsOrdersFinishCodeViewmodelMyOrderDetailsOrdersProviderJavaLangObjectNullBooleanFalse) ? (viewmodelMyOrderDetailsOrdersHistoryInt3) : (false));
                // read viewmodel.myOrderDetails.orders.history == 0 ? true : viewmodel.myOrderDetails.orders.history == 4 ? true : viewmodel.myOrderDetails.orders.history == 5
                viewmodelMyOrderDetailsOrdersHistoryInt0BooleanTrueViewmodelMyOrderDetailsOrdersHistoryInt4BooleanTrueViewmodelMyOrderDetailsOrdersHistoryInt5 = ((viewmodelMyOrderDetailsOrdersHistoryInt0BooleanTrueViewmodelMyOrderDetailsOrdersHistoryInt4) ? (true) : (viewmodelMyOrderDetailsOrdersHistoryInt5));
            if((dirtyFlags & 0xbL) != 0) {
                if(textUtilsIsEmptyViewmodelMyOrderDetailsOrdersFinishCodeViewmodelMyOrderDetailsOrdersProviderJavaLangObjectNullBooleanFalseViewmodelMyOrderDetailsOrdersHistoryInt3BooleanFalse) {
                        dirtyFlags |= 0x2000L;
                }
                else {
                        dirtyFlags |= 0x1000L;
                }
            }
            if((dirtyFlags & 0xbL) != 0) {
                if(TextUtilsIsEmptyViewmodelMyOrderDetailsOrdersFinishCodeViewmodelMyOrderDetailsOrdersProviderJavaLangObjectNullBooleanFalseViewmodelMyOrderDetailsOrdersHistoryInt3BooleanFalse1) {
                        dirtyFlags |= 0x200000000000L;
                }
                else {
                        dirtyFlags |= 0x100000000000L;
                }
            }
            if((dirtyFlags & 0xbL) != 0) {
                if(viewmodelMyOrderDetailsOrdersHistoryInt0BooleanTrueViewmodelMyOrderDetailsOrdersHistoryInt4BooleanTrueViewmodelMyOrderDetailsOrdersHistoryInt5) {
                        dirtyFlags |= 0x20000000000000L;
                }
                else {
                        dirtyFlags |= 0x10000000000000L;
                }
            }


                // read TextUtils.isEmpty(viewmodel.myOrderDetails.orders.finishCode) ? viewmodel.myOrderDetails.orders.provider != null : false ? viewmodel.myOrderDetails.orders.history == 3 : false ? View.VISIBLE : View.GONE
                textUtilsIsEmptyViewmodelMyOrderDetailsOrdersFinishCodeViewmodelMyOrderDetailsOrdersProviderJavaLangObjectNullBooleanFalseViewmodelMyOrderDetailsOrdersHistoryInt3BooleanFalseViewVISIBLEViewGONE = ((textUtilsIsEmptyViewmodelMyOrderDetailsOrdersFinishCodeViewmodelMyOrderDetailsOrdersProviderJavaLangObjectNullBooleanFalseViewmodelMyOrderDetailsOrdersHistoryInt3BooleanFalse) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
                // read !TextUtils.isEmpty(viewmodel.myOrderDetails.orders.finishCode) ? viewmodel.myOrderDetails.orders.provider != null : false ? viewmodel.myOrderDetails.orders.history == 3 : false ? View.VISIBLE : View.GONE
                TextUtilsIsEmptyViewmodelMyOrderDetailsOrdersFinishCodeViewmodelMyOrderDetailsOrdersProviderJavaLangObjectNullBooleanFalseViewmodelMyOrderDetailsOrdersHistoryInt3BooleanFalseViewVISIBLEViewGONE1 = ((TextUtilsIsEmptyViewmodelMyOrderDetailsOrdersFinishCodeViewmodelMyOrderDetailsOrdersProviderJavaLangObjectNullBooleanFalseViewmodelMyOrderDetailsOrdersHistoryInt3BooleanFalse1) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
        }
        // batch finished

        if ((dirtyFlags & 0x10000000000000L) != 0) {

                if (viewmodelMyOrderDetailsOrders != null) {
                    // read viewmodel.myOrderDetails.orders.canceled
                    viewmodelMyOrderDetailsOrdersCanceled = viewmodelMyOrderDetailsOrders.getCanceled();
                }


                // read viewmodel.myOrderDetails.orders.canceled == 1
                viewmodelMyOrderDetailsOrdersCanceledInt1 = (viewmodelMyOrderDetailsOrdersCanceled) == (1);
        }

        if ((dirtyFlags & 0xbL) != 0) {

                // read viewmodel.myOrderDetails.orders.history == 0 ? true : viewmodel.myOrderDetails.orders.history == 4 ? true : viewmodel.myOrderDetails.orders.history == 5 ? true : viewmodel.myOrderDetails.orders.canceled == 1
                viewmodelMyOrderDetailsOrdersHistoryInt0BooleanTrueViewmodelMyOrderDetailsOrdersHistoryInt4BooleanTrueViewmodelMyOrderDetailsOrdersHistoryInt5BooleanTrueViewmodelMyOrderDetailsOrdersCanceledInt1 = ((viewmodelMyOrderDetailsOrdersHistoryInt0BooleanTrueViewmodelMyOrderDetailsOrdersHistoryInt4BooleanTrueViewmodelMyOrderDetailsOrdersHistoryInt5) ? (true) : (viewmodelMyOrderDetailsOrdersCanceledInt1));
            if((dirtyFlags & 0xbL) != 0) {
                if(viewmodelMyOrderDetailsOrdersHistoryInt0BooleanTrueViewmodelMyOrderDetailsOrdersHistoryInt4BooleanTrueViewmodelMyOrderDetailsOrdersHistoryInt5BooleanTrueViewmodelMyOrderDetailsOrdersCanceledInt1) {
                        dirtyFlags |= 0x200000000000000L;
                }
                else {
                        dirtyFlags |= 0x100000000000000L;
                }
            }


                // read viewmodel.myOrderDetails.orders.history == 0 ? true : viewmodel.myOrderDetails.orders.history == 4 ? true : viewmodel.myOrderDetails.orders.history == 5 ? true : viewmodel.myOrderDetails.orders.canceled == 1 ? View.GONE : View.VISIBLE
                viewmodelMyOrderDetailsOrdersHistoryInt0BooleanTrueViewmodelMyOrderDetailsOrdersHistoryInt4BooleanTrueViewmodelMyOrderDetailsOrdersHistoryInt5BooleanTrueViewmodelMyOrderDetailsOrdersCanceledInt1ViewGONEViewVISIBLE = ((viewmodelMyOrderDetailsOrdersHistoryInt0BooleanTrueViewmodelMyOrderDetailsOrdersHistoryInt4BooleanTrueViewmodelMyOrderDetailsOrdersHistoryInt5BooleanTrueViewmodelMyOrderDetailsOrdersCanceledInt1) ? (android.view.View.GONE) : (android.view.View.VISIBLE));
        }
        // batch finished
        if ((dirtyFlags & 0x8L) != 0) {
            // api target 1

            this.btnCallProvider.setOnClickListener(mCallback77);
            this.btnFollowProvider.setOnClickListener(mCallback76);
            this.btnRateProvider.setOnClickListener(mCallback78);
            this.chatBtn.setOnClickListener(mCallback75);
            this.tvServiceCodeGenerate.setOnClickListener(mCallback74);
        }
        if ((dirtyFlags & 0xbL) != 0) {
            // api target 1

            this.btnCallProvider.setVisibility(viewmodelMyOrderDetailsOrdersHistoryInt0BooleanTrueViewmodelMyOrderDetailsOrdersHistoryInt4BooleanTrueViewmodelMyOrderDetailsOrdersHistoryInt5BooleanTrueViewmodelMyOrderDetailsOrdersCanceledInt1ViewGONEViewVISIBLE);
            this.btnFollowProvider.setVisibility(viewmodelMyOrderDetailsOrdersHistoryInt0BooleanTrueViewmodelMyOrderDetailsOrdersHistoryInt4BooleanTrueViewmodelMyOrderDetailsOrdersHistoryInt5BooleanTrueViewmodelMyOrderDetailsOrdersCanceledInt1ViewGONEViewVISIBLE);
            this.btnRateProvider.setVisibility(viewmodelMyOrderDetailsOrdersReviewJavaLangObjectNullViewmodelMyOrderDetailsOrdersHistoryInt4BooleanFalseViewVISIBLEViewGONE);
            this.cardProvider.setVisibility(viewmodelMyOrderDetailsOrdersHistoryInt0ViewGONEViewVISIBLE);
            this.chatBtn.setVisibility(viewmodelMyOrderDetailsOrdersProviderJavaLangObjectNullViewmodelMyOrderDetailsOrdersHistoryInt4BooleanFalseViewGONEViewVISIBLE);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.createAt, viewmodelMyOrderDetailsOrdersReviewCreatedAt);
            androidx.databinding.adapters.ImageViewBindingAdapter.setImageDrawable(this.icAccept, viewmodelMyOrderDetailsOrdersHistoryInt1IcAcceptAndroidDrawableIcFollowSuccessIcAcceptAndroidDrawableIcFollowWaiting);
            androidx.databinding.adapters.ImageViewBindingAdapter.setImageDrawable(this.icArrived, viewmodelMyOrderDetailsOrdersHistoryInt3IcArrivedAndroidDrawableIcFollowSuccessIcArrivedAndroidDrawableIcFollowWaiting);
            androidx.databinding.adapters.ImageViewBindingAdapter.setImageDrawable(this.icFinished, viewmodelMyOrderDetailsOrdersHistoryInt4IcFinishedAndroidDrawableIcFollowSuccessViewmodelMyOrderDetailsOrdersHistoryInt5IcFinishedAndroidDrawableIcNotDoneIcFinishedAndroidDrawableIcFollowWaiting);
            androidx.databinding.adapters.ImageViewBindingAdapter.setImageDrawable(this.icOnWay, viewmodelMyOrderDetailsOrdersHistoryInt2IcOnWayAndroidDrawableIcFollowSuccessIcOnWayAndroidDrawableIcFollowWaiting);
            grand.app.aber_user.base.ApplicationBinding.loadCommentImage(this.icProvider, viewmodelMyOrderDetailsOrdersProviderImage);
            this.mboundView0.setVisibility(textUtilsIsEmptyViewmodelMyOrderDetailsOrdersServiceNameViewVISIBLEViewGONE);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView24, viewmodelMyOrderDetailsOrdersHistoryInt4MboundView24AndroidStringOrderFinishedViewmodelMyOrderDetailsOrdersHistoryInt5MboundView24AndroidStringOrderNotFinishedMboundView24AndroidStringOrderFinished);
            this.mboundView29.setVisibility(viewmodelMyOrderDetailsOrdersReviewJavaLangObjectNullViewGONEViewVISIBLE);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView34, viewmodelMyOrderDetailsOrdersReviewComment);
            grand.app.aber_user.base.ApplicationBinding.setRate(this.myRate, viewmodelMyOrderDetailsOrdersReviewRate);
            grand.app.aber_user.base.ApplicationBinding.setRate(this.rating, viewmodelMyOrderDetailsOrdersProviderRate);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvDesc, viewmodelMyOrderDetailsOrdersProviderCompany);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvHomeItem, viewmodelMyOrderDetailsOrdersProviderName);
            this.tvMyReviewProvider.setVisibility(viewmodelMyOrderDetailsOrdersReviewJavaLangObjectNullViewGONEViewVISIBLE);
            this.tvServiceCode.setVisibility(viewmodelMyOrderDetailsOrdersProviderJavaLangObjectNullViewmodelMyOrderDetailsOrdersHistoryInt3BooleanFalseViewVISIBLEViewGONE);
            this.tvServiceCodeGenerate.setVisibility(textUtilsIsEmptyViewmodelMyOrderDetailsOrdersFinishCodeViewmodelMyOrderDetailsOrdersProviderJavaLangObjectNullBooleanFalseViewmodelMyOrderDetailsOrdersHistoryInt3BooleanFalseViewVISIBLEViewGONE);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvServiceCodeValue, viewmodelMyOrderDetailsOrdersFinishCode);
            this.tvServiceCodeValue.setVisibility(TextUtilsIsEmptyViewmodelMyOrderDetailsOrdersFinishCodeViewmodelMyOrderDetailsOrdersProviderJavaLangObjectNullBooleanFalseViewmodelMyOrderDetailsOrdersHistoryInt3BooleanFalseViewVISIBLEViewGONE1);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvServiceCostValue, stringValueOfViewmodelMyOrderDetailsOrdersCostConcatJavaLangStringConcatViewmodelCurrency);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvServiceLocationValue, viewmodelMyOrderDetailsOrdersAddress);
            this.tvServiceProvider.setVisibility(viewmodelMyOrderDetailsOrdersHistoryInt0ViewGONEViewVISIBLE);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvServiceStatusValue, viewmodelMyOrderDetailsOrdersStatus);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvServiceTimeValue, viewmodelMyOrderDetailsOrdersIsEmergencyInt1TvServiceTimeValueAndroidStringEmergencyServicesViewmodelMyOrderDetailsOrdersScheduledAt);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvServicesNameValue, viewmodelMyOrderDetailsOrdersServiceName);
            this.vCode.setVisibility(TextUtilsIsEmptyViewmodelMyOrderDetailsOrdersFinishCodeViewmodelMyOrderDetailsOrdersProviderJavaLangObjectNullBooleanFalseViewmodelMyOrderDetailsOrdersHistoryInt3BooleanFalseViewVISIBLEViewGONE1);
        }
        if ((dirtyFlags & 0x9L) != 0) {
            // api target 1

            grand.app.aber_user.base.ApplicationBinding.loadCommentImage(this.icProviderRate, viewmodelUserDataImage);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvProviderName, viewmodelUserDataName);
        }
        if ((dirtyFlags & 0xdL) != 0) {
            // api target 1

            grand.app.aber_user.base.ApplicationBinding.getItemsV2Binding(this.rcServices, viewmodelServicesRequiredAdapter, "1", "1");
            this.tvServiceRc.setVisibility(viewmodelServicesRequiredAdapterItemCountInt0ViewGONEViewVISIBLE);
            this.vServiceCost.setVisibility(viewmodelServicesRequiredAdapterItemCountInt0ViewINVISIBLEViewVISIBLE);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        switch(sourceId) {
            case 4: {
                // localize variables for thread safety
                // viewmodel != null
                boolean viewmodelJavaLangObjectNull = false;
                // viewmodel
                grand.app.aber_user.pages.myOrders.viewModels.MyOrdersServicesViewModels viewmodel = mViewmodel;



                viewmodelJavaLangObjectNull = (viewmodel) != (null);
                if (viewmodelJavaLangObjectNull) {




                    viewmodel.liveDataActions(grand.app.aber_user.utils.Constants.CALL);
                }
                break;
            }
            case 5: {
                // localize variables for thread safety
                // viewmodel != null
                boolean viewmodelJavaLangObjectNull = false;
                // viewmodel
                grand.app.aber_user.pages.myOrders.viewModels.MyOrdersServicesViewModels viewmodel = mViewmodel;



                viewmodelJavaLangObjectNull = (viewmodel) != (null);
                if (viewmodelJavaLangObjectNull) {




                    viewmodel.liveDataActions(grand.app.aber_user.utils.Constants.RATE_APP);
                }
                break;
            }
            case 1: {
                // localize variables for thread safety
                // viewmodel != null
                boolean viewmodelJavaLangObjectNull = false;
                // viewmodel
                grand.app.aber_user.pages.myOrders.viewModels.MyOrdersServicesViewModels viewmodel = mViewmodel;



                viewmodelJavaLangObjectNull = (viewmodel) != (null);
                if (viewmodelJavaLangObjectNull) {


                    viewmodel.generateCode();
                }
                break;
            }
            case 3: {
                // localize variables for thread safety
                // viewmodel != null
                boolean viewmodelJavaLangObjectNull = false;
                // viewmodel
                grand.app.aber_user.pages.myOrders.viewModels.MyOrdersServicesViewModels viewmodel = mViewmodel;



                viewmodelJavaLangObjectNull = (viewmodel) != (null);
                if (viewmodelJavaLangObjectNull) {




                    viewmodel.liveDataActions(grand.app.aber_user.utils.Constants.FOLLOW_ORDER);
                }
                break;
            }
            case 2: {
                // localize variables for thread safety
                // viewmodel != null
                boolean viewmodelJavaLangObjectNull = false;
                // viewmodel
                grand.app.aber_user.pages.myOrders.viewModels.MyOrdersServicesViewModels viewmodel = mViewmodel;



                viewmodelJavaLangObjectNull = (viewmodel) != (null);
                if (viewmodelJavaLangObjectNull) {




                    viewmodel.liveDataActions(grand.app.aber_user.utils.Constants.CHAT);
                }
                break;
            }
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): viewmodel
        flag 1 (0x2L): viewmodel.myOrderDetails
        flag 2 (0x3L): viewmodel.servicesRequiredAdapter
        flag 3 (0x4L): null
        flag 4 (0x5L): viewmodel.myOrderDetails.orders.provider != null ? viewmodel.myOrderDetails.orders.history >= 4 : false
        flag 5 (0x6L): viewmodel.myOrderDetails.orders.provider != null ? viewmodel.myOrderDetails.orders.history >= 4 : false
        flag 6 (0x7L): TextUtils.isEmpty(viewmodel.myOrderDetails.orders.finishCode) ? viewmodel.myOrderDetails.orders.provider != null : false ? viewmodel.myOrderDetails.orders.history == 3 : false
        flag 7 (0x8L): TextUtils.isEmpty(viewmodel.myOrderDetails.orders.finishCode) ? viewmodel.myOrderDetails.orders.provider != null : false ? viewmodel.myOrderDetails.orders.history == 3 : false
        flag 8 (0x9L): viewmodel.myOrderDetails.orders.history == 4 ? @android:drawable/ic_follow_success : viewmodel.myOrderDetails.orders.history == 5 ? @android:drawable/ic_not_done : @android:drawable/ic_follow_waiting
        flag 9 (0xaL): viewmodel.myOrderDetails.orders.history == 4 ? @android:drawable/ic_follow_success : viewmodel.myOrderDetails.orders.history == 5 ? @android:drawable/ic_not_done : @android:drawable/ic_follow_waiting
        flag 10 (0xbL): !TextUtils.isEmpty(viewmodel.myOrderDetails.orders.finishCode) ? viewmodel.myOrderDetails.orders.provider != null : false ? viewmodel.myOrderDetails.orders.history == 3 : false
        flag 11 (0xcL): !TextUtils.isEmpty(viewmodel.myOrderDetails.orders.finishCode) ? viewmodel.myOrderDetails.orders.provider != null : false ? viewmodel.myOrderDetails.orders.history == 3 : false
        flag 12 (0xdL): TextUtils.isEmpty(viewmodel.myOrderDetails.orders.finishCode) ? viewmodel.myOrderDetails.orders.provider != null : false ? viewmodel.myOrderDetails.orders.history == 3 : false ? View.VISIBLE : View.GONE
        flag 13 (0xeL): TextUtils.isEmpty(viewmodel.myOrderDetails.orders.finishCode) ? viewmodel.myOrderDetails.orders.provider != null : false ? viewmodel.myOrderDetails.orders.history == 3 : false ? View.VISIBLE : View.GONE
        flag 14 (0xfL): viewmodel.myOrderDetails.orders.history == 5 ? @android:string/order_not_finished : @android:string/order_finished
        flag 15 (0x10L): viewmodel.myOrderDetails.orders.history == 5 ? @android:string/order_not_finished : @android:string/order_finished
        flag 16 (0x11L): viewmodel.myOrderDetails.orders.history >= 2 ? @android:drawable/ic_follow_success : @android:drawable/ic_follow_waiting
        flag 17 (0x12L): viewmodel.myOrderDetails.orders.history >= 2 ? @android:drawable/ic_follow_success : @android:drawable/ic_follow_waiting
        flag 18 (0x13L): viewmodel.servicesRequiredAdapter.itemCount == 0 ? View.INVISIBLE : View.VISIBLE
        flag 19 (0x14L): viewmodel.servicesRequiredAdapter.itemCount == 0 ? View.INVISIBLE : View.VISIBLE
        flag 20 (0x15L): viewmodel.myOrderDetails.orders.history == 5 ? @android:drawable/ic_not_done : @android:drawable/ic_follow_waiting
        flag 21 (0x16L): viewmodel.myOrderDetails.orders.history == 5 ? @android:drawable/ic_not_done : @android:drawable/ic_follow_waiting
        flag 22 (0x17L): viewmodel.myOrderDetails.orders.history == 0 ? true : viewmodel.myOrderDetails.orders.history == 4
        flag 23 (0x18L): viewmodel.myOrderDetails.orders.history == 0 ? true : viewmodel.myOrderDetails.orders.history == 4
        flag 24 (0x19L): !TextUtils.isEmpty(viewmodel.myOrderDetails.orders.finishCode) ? viewmodel.myOrderDetails.orders.provider != null : false
        flag 25 (0x1aL): !TextUtils.isEmpty(viewmodel.myOrderDetails.orders.finishCode) ? viewmodel.myOrderDetails.orders.provider != null : false
        flag 26 (0x1bL): viewmodel.myOrderDetails.orders.history >= 1 ? @android:drawable/ic_follow_success : @android:drawable/ic_follow_waiting
        flag 27 (0x1cL): viewmodel.myOrderDetails.orders.history >= 1 ? @android:drawable/ic_follow_success : @android:drawable/ic_follow_waiting
        flag 28 (0x1dL): !TextUtils.isEmpty(viewmodel.myOrderDetails.orders.serviceName) ? View.VISIBLE : View.GONE
        flag 29 (0x1eL): !TextUtils.isEmpty(viewmodel.myOrderDetails.orders.serviceName) ? View.VISIBLE : View.GONE
        flag 30 (0x1fL): viewmodel.myOrderDetails.orders.provider != null ? viewmodel.myOrderDetails.orders.history >= 4 : false ? View.GONE : View.VISIBLE
        flag 31 (0x20L): viewmodel.myOrderDetails.orders.provider != null ? viewmodel.myOrderDetails.orders.history >= 4 : false ? View.GONE : View.VISIBLE
        flag 32 (0x21L): viewmodel.servicesRequiredAdapter.itemCount == 0 ? View.GONE : View.VISIBLE
        flag 33 (0x22L): viewmodel.servicesRequiredAdapter.itemCount == 0 ? View.GONE : View.VISIBLE
        flag 34 (0x23L): viewmodel.myOrderDetails.orders.history == 0 ? View.GONE : View.VISIBLE
        flag 35 (0x24L): viewmodel.myOrderDetails.orders.history == 0 ? View.GONE : View.VISIBLE
        flag 36 (0x25L): TextUtils.isEmpty(viewmodel.myOrderDetails.orders.finishCode) ? viewmodel.myOrderDetails.orders.provider != null : false
        flag 37 (0x26L): TextUtils.isEmpty(viewmodel.myOrderDetails.orders.finishCode) ? viewmodel.myOrderDetails.orders.provider != null : false
        flag 38 (0x27L): viewmodel.myOrderDetails.orders.provider != null ? viewmodel.myOrderDetails.orders.history == 3 : false
        flag 39 (0x28L): viewmodel.myOrderDetails.orders.provider != null ? viewmodel.myOrderDetails.orders.history == 3 : false
        flag 40 (0x29L): viewmodel.myOrderDetails.orders.review == null ? viewmodel.myOrderDetails.orders.history >= 4 : false ? View.VISIBLE : View.GONE
        flag 41 (0x2aL): viewmodel.myOrderDetails.orders.review == null ? viewmodel.myOrderDetails.orders.history >= 4 : false ? View.VISIBLE : View.GONE
        flag 42 (0x2bL): viewmodel.myOrderDetails.orders.history == 0 ? true : viewmodel.myOrderDetails.orders.history == 4 ? true : viewmodel.myOrderDetails.orders.history == 5
        flag 43 (0x2cL): viewmodel.myOrderDetails.orders.history == 0 ? true : viewmodel.myOrderDetails.orders.history == 4 ? true : viewmodel.myOrderDetails.orders.history == 5
        flag 44 (0x2dL): !TextUtils.isEmpty(viewmodel.myOrderDetails.orders.finishCode) ? viewmodel.myOrderDetails.orders.provider != null : false ? viewmodel.myOrderDetails.orders.history == 3 : false ? View.VISIBLE : View.GONE
        flag 45 (0x2eL): !TextUtils.isEmpty(viewmodel.myOrderDetails.orders.finishCode) ? viewmodel.myOrderDetails.orders.provider != null : false ? viewmodel.myOrderDetails.orders.history == 3 : false ? View.VISIBLE : View.GONE
        flag 46 (0x2fL): viewmodel.myOrderDetails.orders.history >= 3 ? @android:drawable/ic_follow_success : @android:drawable/ic_follow_waiting
        flag 47 (0x30L): viewmodel.myOrderDetails.orders.history >= 3 ? @android:drawable/ic_follow_success : @android:drawable/ic_follow_waiting
        flag 48 (0x31L): viewmodel.myOrderDetails.orders.review == null ? View.GONE : View.VISIBLE
        flag 49 (0x32L): viewmodel.myOrderDetails.orders.review == null ? View.GONE : View.VISIBLE
        flag 50 (0x33L): viewmodel.myOrderDetails.orders.provider != null ? viewmodel.myOrderDetails.orders.history == 3 : false ? View.VISIBLE : View.GONE
        flag 51 (0x34L): viewmodel.myOrderDetails.orders.provider != null ? viewmodel.myOrderDetails.orders.history == 3 : false ? View.VISIBLE : View.GONE
        flag 52 (0x35L): viewmodel.myOrderDetails.orders.history == 0 ? true : viewmodel.myOrderDetails.orders.history == 4 ? true : viewmodel.myOrderDetails.orders.history == 5 ? true : viewmodel.myOrderDetails.orders.canceled == 1
        flag 53 (0x36L): viewmodel.myOrderDetails.orders.history == 0 ? true : viewmodel.myOrderDetails.orders.history == 4 ? true : viewmodel.myOrderDetails.orders.history == 5 ? true : viewmodel.myOrderDetails.orders.canceled == 1
        flag 54 (0x37L): viewmodel.myOrderDetails.orders.review == null ? viewmodel.myOrderDetails.orders.history >= 4 : false
        flag 55 (0x38L): viewmodel.myOrderDetails.orders.review == null ? viewmodel.myOrderDetails.orders.history >= 4 : false
        flag 56 (0x39L): viewmodel.myOrderDetails.orders.history == 0 ? true : viewmodel.myOrderDetails.orders.history == 4 ? true : viewmodel.myOrderDetails.orders.history == 5 ? true : viewmodel.myOrderDetails.orders.canceled == 1 ? View.GONE : View.VISIBLE
        flag 57 (0x3aL): viewmodel.myOrderDetails.orders.history == 0 ? true : viewmodel.myOrderDetails.orders.history == 4 ? true : viewmodel.myOrderDetails.orders.history == 5 ? true : viewmodel.myOrderDetails.orders.canceled == 1 ? View.GONE : View.VISIBLE
        flag 58 (0x3bL): viewmodel.myOrderDetails.orders.isEmergency == 1 ? @android:string/emergency_services : viewmodel.myOrderDetails.orders.scheduledAt
        flag 59 (0x3cL): viewmodel.myOrderDetails.orders.isEmergency == 1 ? @android:string/emergency_services : viewmodel.myOrderDetails.orders.scheduledAt
        flag 60 (0x3dL): viewmodel.myOrderDetails.orders.history == 4 ? @android:string/order_finished : viewmodel.myOrderDetails.orders.history == 5 ? @android:string/order_not_finished : @android:string/order_finished
        flag 61 (0x3eL): viewmodel.myOrderDetails.orders.history == 4 ? @android:string/order_finished : viewmodel.myOrderDetails.orders.history == 5 ? @android:string/order_not_finished : @android:string/order_finished
    flag mapping end*/
    //end
}