package grand.app.aber_user.databinding;
import grand.app.aber_user.R;
import grand.app.aber_user.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentWaterBallonBindingImpl extends FragmentWaterBallonBinding implements grand.app.aber_user.generated.callback.OnClickListener.Listener, grand.app.aber_user.generated.callback.OnCheckedChangeListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.searchContainer, 13);
        sViewsWithIds.put(R.id.input_litre, 14);
        sViewsWithIds.put(R.id.input_litre_service, 15);
        sViewsWithIds.put(R.id.input_gallon, 16);
        sViewsWithIds.put(R.id.input_message, 17);
    }
    // views
    @NonNull
    private final androidx.core.widget.NestedScrollView mboundView0;
    @NonNull
    private final com.google.android.material.textfield.TextInputEditText mboundView1;
    @NonNull
    private final com.google.android.material.textfield.TextInputEditText mboundView2;
    @NonNull
    private final com.google.android.material.textfield.TextInputEditText mboundView3;
    @NonNull
    private final com.google.android.material.textfield.TextInputEditText mboundView4;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback79;
    @Nullable
    private final android.view.View.OnClickListener mCallback86;
    @Nullable
    private final android.view.View.OnClickListener mCallback82;
    @Nullable
    private final android.view.View.OnClickListener mCallback83;
    @Nullable
    private final android.view.View.OnClickListener mCallback84;
    @Nullable
    private final android.view.View.OnClickListener mCallback80;
    @Nullable
    private final android.widget.CompoundButton.OnCheckedChangeListener mCallback85;
    @Nullable
    private final android.view.View.OnClickListener mCallback81;
    // values
    // listeners
    // Inverse Binding Event Handlers
    private androidx.databinding.InverseBindingListener mboundView1androidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.serviceOrderRequest.litre
            //         is viewModel.serviceOrderRequest.setLitre((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(mboundView1);
            // localize variables for thread safety
            // viewModel.serviceOrderRequest != null
            boolean viewModelServiceOrderRequestJavaLangObjectNull = false;
            // viewModel.serviceOrderRequest.litre
            java.lang.String viewModelServiceOrderRequestLitre = null;
            // viewModel.serviceOrderRequest
            grand.app.aber_user.pages.services.models.ServiceOrderDetails viewModelServiceOrderRequest = null;
            // viewModel
            grand.app.aber_user.pages.services.viewModels.ServicesViewModels viewModel = mViewModel;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelServiceOrderRequest = viewModel.getServiceOrderRequest();

                viewModelServiceOrderRequestJavaLangObjectNull = (viewModelServiceOrderRequest) != (null);
                if (viewModelServiceOrderRequestJavaLangObjectNull) {




                    viewModelServiceOrderRequest.setLitre(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener mboundView2androidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.serviceOrderRequest.tinkerServiceName
            //         is viewModel.serviceOrderRequest.setTinkerServiceName((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(mboundView2);
            // localize variables for thread safety
            // viewModel.serviceOrderRequest != null
            boolean viewModelServiceOrderRequestJavaLangObjectNull = false;
            // viewModel.serviceOrderRequest.tinkerServiceName
            java.lang.String viewModelServiceOrderRequestTinkerServiceName = null;
            // viewModel.serviceOrderRequest
            grand.app.aber_user.pages.services.models.ServiceOrderDetails viewModelServiceOrderRequest = null;
            // viewModel
            grand.app.aber_user.pages.services.viewModels.ServicesViewModels viewModel = mViewModel;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelServiceOrderRequest = viewModel.getServiceOrderRequest();

                viewModelServiceOrderRequestJavaLangObjectNull = (viewModelServiceOrderRequest) != (null);
                if (viewModelServiceOrderRequestJavaLangObjectNull) {




                    viewModelServiceOrderRequest.setTinkerServiceName(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener mboundView3androidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.serviceOrderRequest.gallon
            //         is viewModel.serviceOrderRequest.setGallon((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(mboundView3);
            // localize variables for thread safety
            // viewModel.serviceOrderRequest != null
            boolean viewModelServiceOrderRequestJavaLangObjectNull = false;
            // viewModel.serviceOrderRequest.gallon
            java.lang.String viewModelServiceOrderRequestGallon = null;
            // viewModel.serviceOrderRequest
            grand.app.aber_user.pages.services.models.ServiceOrderDetails viewModelServiceOrderRequest = null;
            // viewModel
            grand.app.aber_user.pages.services.viewModels.ServicesViewModels viewModel = mViewModel;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelServiceOrderRequest = viewModel.getServiceOrderRequest();

                viewModelServiceOrderRequestJavaLangObjectNull = (viewModelServiceOrderRequest) != (null);
                if (viewModelServiceOrderRequestJavaLangObjectNull) {




                    viewModelServiceOrderRequest.setGallon(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener mboundView4androidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.serviceOrderRequest.desc
            //         is viewModel.serviceOrderRequest.setDesc((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(mboundView4);
            // localize variables for thread safety
            // viewModel.serviceOrderRequest != null
            boolean viewModelServiceOrderRequestJavaLangObjectNull = false;
            // viewModel.serviceOrderRequest.desc
            java.lang.String viewModelServiceOrderRequestDesc = null;
            // viewModel.serviceOrderRequest
            grand.app.aber_user.pages.services.models.ServiceOrderDetails viewModelServiceOrderRequest = null;
            // viewModel
            grand.app.aber_user.pages.services.viewModels.ServicesViewModels viewModel = mViewModel;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelServiceOrderRequest = viewModel.getServiceOrderRequest();

                viewModelServiceOrderRequestJavaLangObjectNull = (viewModelServiceOrderRequest) != (null);
                if (viewModelServiceOrderRequestJavaLangObjectNull) {




                    viewModelServiceOrderRequest.setDesc(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };

    public FragmentWaterBallonBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 18, sIncludes, sViewsWithIds));
    }
    private FragmentWaterBallonBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 2
            , (androidx.appcompat.widget.AppCompatButton) bindings[11]
            , (com.google.android.material.checkbox.MaterialCheckBox) bindings[10]
            , (com.google.android.material.button.MaterialButton) bindings[8]
            , (androidx.cardview.widget.CardView) bindings[5]
            , (com.google.android.material.textfield.TextInputLayout) bindings[16]
            , (com.google.android.material.textfield.TextInputLayout) bindings[14]
            , (com.google.android.material.textfield.TextInputLayout) bindings[15]
            , (com.google.android.material.textfield.TextInputLayout) bindings[17]
            , (com.google.android.material.button.MaterialButton) bindings[9]
            , (com.google.android.material.progressindicator.CircularProgressIndicator) bindings[12]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[13]
            , (com.google.android.material.button.MaterialButton) bindings[7]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[6]
            );
        this.btnPhone.setTag(null);
        this.checkbox.setTag(null);
        this.downLocation.setTag(null);
        this.infoCard.setTag(null);
        this.mboundView0 = (androidx.core.widget.NestedScrollView) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (com.google.android.material.textfield.TextInputEditText) bindings[1];
        this.mboundView1.setTag(null);
        this.mboundView2 = (com.google.android.material.textfield.TextInputEditText) bindings[2];
        this.mboundView2.setTag(null);
        this.mboundView3 = (com.google.android.material.textfield.TextInputEditText) bindings[3];
        this.mboundView3.setTag(null);
        this.mboundView4 = (com.google.android.material.textfield.TextInputEditText) bindings[4];
        this.mboundView4.setTag(null);
        this.picTime.setTag(null);
        this.progress.setTag(null);
        this.searchLocation.setTag(null);
        this.tvNote.setTag(null);
        setRootTag(root);
        // listeners
        mCallback79 = new grand.app.aber_user.generated.callback.OnClickListener(this, 1);
        mCallback86 = new grand.app.aber_user.generated.callback.OnClickListener(this, 8);
        mCallback82 = new grand.app.aber_user.generated.callback.OnClickListener(this, 4);
        mCallback83 = new grand.app.aber_user.generated.callback.OnClickListener(this, 5);
        mCallback84 = new grand.app.aber_user.generated.callback.OnClickListener(this, 6);
        mCallback80 = new grand.app.aber_user.generated.callback.OnClickListener(this, 2);
        mCallback85 = new grand.app.aber_user.generated.callback.OnCheckedChangeListener(this, 7);
        mCallback81 = new grand.app.aber_user.generated.callback.OnClickListener(this, 3);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x20L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.viewModel == variableId) {
            setViewModel((grand.app.aber_user.pages.services.viewModels.ServicesViewModels) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setViewModel(@Nullable grand.app.aber_user.pages.services.viewModels.ServicesViewModels ViewModel) {
        updateRegistration(0, ViewModel);
        this.mViewModel = ViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.viewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeViewModel((grand.app.aber_user.pages.services.viewModels.ServicesViewModels) object, fieldId);
            case 1 :
                return onChangeViewModelIsEmergencyAccepted((androidx.databinding.ObservableBoolean) object, fieldId);
        }
        return false;
    }
    private boolean onChangeViewModel(grand.app.aber_user.pages.services.viewModels.ServicesViewModels ViewModel, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        else if (fieldId == BR.details) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        else if (fieldId == BR.serviceOrderRequest) {
            synchronized(this) {
                    mDirtyFlags |= 0x8L;
            }
            return true;
        }
        else if (fieldId == BR.message) {
            synchronized(this) {
                    mDirtyFlags |= 0x10L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelIsEmergencyAccepted(androidx.databinding.ObservableBoolean ViewModelIsEmergencyAccepted, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        int viewModelDetailsLiterGallonListJavaLangObjectNullViewVISIBLEViewGONE = 0;
        boolean textUtilsIsEmptyViewModelMessage = false;
        java.lang.String viewModelMessage = null;
        java.lang.String viewModelServiceOrderRequestDesc = null;
        boolean viewModelIsEmergencyAccepted = false;
        boolean TextUtilsIsEmptyViewModelMessage1 = false;
        java.util.List<grand.app.aber_user.model.DropDownsObject> viewModelDetailsLiterGallonList = null;
        boolean textUtilsIsEmptyViewModelDetailsNote = false;
        boolean textUtilsIsEmptyViewModelMessageViewModelMessageEqualsConstantsSHOWPROGRESSBooleanFalse = false;
        int textUtilsIsEmptyViewModelMessageViewModelMessageEqualsConstantsSHOWPROGRESSBooleanFalseViewVISIBLEViewGONE = 0;
        java.lang.String viewModelDetailsNote = null;
        int textUtilsIsEmptyViewModelDetailsNoteViewVISIBLEViewGONE = 0;
        android.graphics.drawable.Drawable textUtilsIsEmptyViewModelMessageBooleanTrueViewModelMessageEqualsConstantsHIDEPROGRESSBtnPhoneAndroidDrawableCornerViewGradientBtnPhoneAndroidDrawableCornerViewPrimaryMedium = null;
        boolean viewModelMessageEqualsConstantsHIDEPROGRESS = false;
        boolean viewModelIsEmergencyAcceptedGet = false;
        boolean viewModelDetailsLiterGallonListJavaLangObjectNull = false;
        grand.app.aber_user.pages.services.models.ServiceDetails viewModelDetails = null;
        java.lang.String viewModelServiceOrderRequestGallon = null;
        java.lang.String viewModelServiceOrderRequestTinkerServiceName = null;
        java.lang.String viewModelServiceOrderRequestLitre = null;
        grand.app.aber_user.pages.services.models.ServiceOrderDetails viewModelServiceOrderRequest = null;
        boolean TextUtilsIsEmptyViewModelDetailsNote1 = false;
        boolean textUtilsIsEmptyViewModelMessageBooleanTrueViewModelMessageEqualsConstantsHIDEPROGRESSBooleanTrueBooleanFalse = false;
        boolean viewModelMessageEqualsConstantsSHOWPROGRESS = false;
        grand.app.aber_user.pages.services.viewModels.ServicesViewModels viewModel = mViewModel;
        boolean textUtilsIsEmptyViewModelMessageBooleanTrueViewModelMessageEqualsConstantsHIDEPROGRESS = false;
        androidx.databinding.ObservableBoolean ViewModelIsEmergencyAccepted1 = null;

        if ((dirtyFlags & 0x3fL) != 0) {


            if ((dirtyFlags & 0x31L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.message
                        viewModelMessage = viewModel.getMessage();
                    }


                    // read TextUtils.isEmpty(viewModel.message)
                    TextUtilsIsEmptyViewModelMessage1 = android.text.TextUtils.isEmpty(viewModelMessage);
                if((dirtyFlags & 0x31L) != 0) {
                    if(TextUtilsIsEmptyViewModelMessage1) {
                            dirtyFlags |= 0x80000L;
                    }
                    else {
                            dirtyFlags |= 0x40000L;
                    }
                }


                    // read !TextUtils.isEmpty(viewModel.message)
                    textUtilsIsEmptyViewModelMessage = !TextUtilsIsEmptyViewModelMessage1;
                if((dirtyFlags & 0x31L) != 0) {
                    if(textUtilsIsEmptyViewModelMessage) {
                            dirtyFlags |= 0x200L;
                    }
                    else {
                            dirtyFlags |= 0x100L;
                    }
                }
            }
            if ((dirtyFlags & 0x25L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.details
                        viewModelDetails = viewModel.getDetails();
                    }


                    if (viewModelDetails != null) {
                        // read viewModel.details.literGallonList
                        viewModelDetailsLiterGallonList = viewModelDetails.getLiterGallonList();
                        // read viewModel.details.note
                        viewModelDetailsNote = viewModelDetails.getNote();
                    }


                    // read viewModel.details.literGallonList != null
                    viewModelDetailsLiterGallonListJavaLangObjectNull = (viewModelDetailsLiterGallonList) != (null);
                    // read TextUtils.isEmpty(viewModel.details.note)
                    textUtilsIsEmptyViewModelDetailsNote = android.text.TextUtils.isEmpty(viewModelDetailsNote);
                if((dirtyFlags & 0x25L) != 0) {
                    if(viewModelDetailsLiterGallonListJavaLangObjectNull) {
                            dirtyFlags |= 0x80L;
                    }
                    else {
                            dirtyFlags |= 0x40L;
                    }
                }


                    // read viewModel.details.literGallonList != null ? View.VISIBLE : View.GONE
                    viewModelDetailsLiterGallonListJavaLangObjectNullViewVISIBLEViewGONE = ((viewModelDetailsLiterGallonListJavaLangObjectNull) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
                    // read !TextUtils.isEmpty(viewModel.details.note)
                    TextUtilsIsEmptyViewModelDetailsNote1 = !textUtilsIsEmptyViewModelDetailsNote;
                if((dirtyFlags & 0x25L) != 0) {
                    if(TextUtilsIsEmptyViewModelDetailsNote1) {
                            dirtyFlags |= 0x2000L;
                    }
                    else {
                            dirtyFlags |= 0x1000L;
                    }
                }


                    // read !TextUtils.isEmpty(viewModel.details.note) ? View.VISIBLE : View.GONE
                    textUtilsIsEmptyViewModelDetailsNoteViewVISIBLEViewGONE = ((TextUtilsIsEmptyViewModelDetailsNote1) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
            if ((dirtyFlags & 0x29L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.serviceOrderRequest
                        viewModelServiceOrderRequest = viewModel.getServiceOrderRequest();
                    }


                    if (viewModelServiceOrderRequest != null) {
                        // read viewModel.serviceOrderRequest.desc
                        viewModelServiceOrderRequestDesc = viewModelServiceOrderRequest.getDesc();
                        // read viewModel.serviceOrderRequest.gallon
                        viewModelServiceOrderRequestGallon = viewModelServiceOrderRequest.getGallon();
                        // read viewModel.serviceOrderRequest.tinkerServiceName
                        viewModelServiceOrderRequestTinkerServiceName = viewModelServiceOrderRequest.getTinkerServiceName();
                        // read viewModel.serviceOrderRequest.litre
                        viewModelServiceOrderRequestLitre = viewModelServiceOrderRequest.getLitre();
                    }
            }
            if ((dirtyFlags & 0x23L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.isEmergencyAccepted
                        ViewModelIsEmergencyAccepted1 = viewModel.isEmergencyAccepted;
                    }
                    updateRegistration(1, ViewModelIsEmergencyAccepted1);


                    if (ViewModelIsEmergencyAccepted1 != null) {
                        // read viewModel.isEmergencyAccepted.get()
                        viewModelIsEmergencyAcceptedGet = ViewModelIsEmergencyAccepted1.get();
                    }


                    // read !viewModel.isEmergencyAccepted.get()
                    viewModelIsEmergencyAccepted = !viewModelIsEmergencyAcceptedGet;
            }
        }
        // batch finished

        if ((dirtyFlags & 0x40000L) != 0) {

                if (viewModelMessage != null) {
                    // read viewModel.message.equals(Constants.HIDE_PROGRESS)
                    viewModelMessageEqualsConstantsHIDEPROGRESS = viewModelMessage.equals(grand.app.aber_user.utils.Constants.HIDE_PROGRESS);
                }
        }
        if ((dirtyFlags & 0x200L) != 0) {

                if (viewModelMessage != null) {
                    // read viewModel.message.equals(Constants.SHOW_PROGRESS)
                    viewModelMessageEqualsConstantsSHOWPROGRESS = viewModelMessage.equals(grand.app.aber_user.utils.Constants.SHOW_PROGRESS);
                }
        }

        if ((dirtyFlags & 0x31L) != 0) {

                // read !TextUtils.isEmpty(viewModel.message) ? viewModel.message.equals(Constants.SHOW_PROGRESS) : false
                textUtilsIsEmptyViewModelMessageViewModelMessageEqualsConstantsSHOWPROGRESSBooleanFalse = ((textUtilsIsEmptyViewModelMessage) ? (viewModelMessageEqualsConstantsSHOWPROGRESS) : (false));
                // read TextUtils.isEmpty(viewModel.message) ? true : viewModel.message.equals(Constants.HIDE_PROGRESS)
                textUtilsIsEmptyViewModelMessageBooleanTrueViewModelMessageEqualsConstantsHIDEPROGRESS = ((TextUtilsIsEmptyViewModelMessage1) ? (true) : (viewModelMessageEqualsConstantsHIDEPROGRESS));
            if((dirtyFlags & 0x31L) != 0) {
                if(textUtilsIsEmptyViewModelMessageViewModelMessageEqualsConstantsSHOWPROGRESSBooleanFalse) {
                        dirtyFlags |= 0x800L;
                }
                else {
                        dirtyFlags |= 0x400L;
                }
            }
            if((dirtyFlags & 0x31L) != 0) {
                if(textUtilsIsEmptyViewModelMessageBooleanTrueViewModelMessageEqualsConstantsHIDEPROGRESS) {
                        dirtyFlags |= 0x8000L;
                        dirtyFlags |= 0x20000L;
                }
                else {
                        dirtyFlags |= 0x4000L;
                        dirtyFlags |= 0x10000L;
                }
            }


                // read !TextUtils.isEmpty(viewModel.message) ? viewModel.message.equals(Constants.SHOW_PROGRESS) : false ? View.VISIBLE : View.GONE
                textUtilsIsEmptyViewModelMessageViewModelMessageEqualsConstantsSHOWPROGRESSBooleanFalseViewVISIBLEViewGONE = ((textUtilsIsEmptyViewModelMessageViewModelMessageEqualsConstantsSHOWPROGRESSBooleanFalse) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
                // read TextUtils.isEmpty(viewModel.message) ? true : viewModel.message.equals(Constants.HIDE_PROGRESS) ? @android:drawable/corner_view_gradient : @android:drawable/corner_view_primary_medium
                textUtilsIsEmptyViewModelMessageBooleanTrueViewModelMessageEqualsConstantsHIDEPROGRESSBtnPhoneAndroidDrawableCornerViewGradientBtnPhoneAndroidDrawableCornerViewPrimaryMedium = ((textUtilsIsEmptyViewModelMessageBooleanTrueViewModelMessageEqualsConstantsHIDEPROGRESS) ? (androidx.appcompat.content.res.AppCompatResources.getDrawable(btnPhone.getContext(), R.drawable.corner_view_gradient)) : (androidx.appcompat.content.res.AppCompatResources.getDrawable(btnPhone.getContext(), R.drawable.corner_view_primary_medium)));
                // read TextUtils.isEmpty(viewModel.message) ? true : viewModel.message.equals(Constants.HIDE_PROGRESS) ? true : false
                textUtilsIsEmptyViewModelMessageBooleanTrueViewModelMessageEqualsConstantsHIDEPROGRESSBooleanTrueBooleanFalse = ((textUtilsIsEmptyViewModelMessageBooleanTrueViewModelMessageEqualsConstantsHIDEPROGRESS) ? (true) : (false));
        }
        // batch finished
        if ((dirtyFlags & 0x31L) != 0) {
            // api target 1

            androidx.databinding.adapters.ViewBindingAdapter.setBackground(this.btnPhone, textUtilsIsEmptyViewModelMessageBooleanTrueViewModelMessageEqualsConstantsHIDEPROGRESSBtnPhoneAndroidDrawableCornerViewGradientBtnPhoneAndroidDrawableCornerViewPrimaryMedium);
            this.btnPhone.setEnabled(textUtilsIsEmptyViewModelMessageBooleanTrueViewModelMessageEqualsConstantsHIDEPROGRESSBooleanTrueBooleanFalse);
            this.progress.setVisibility(textUtilsIsEmptyViewModelMessageViewModelMessageEqualsConstantsSHOWPROGRESSBooleanFalseViewVISIBLEViewGONE);
        }
        if ((dirtyFlags & 0x20L) != 0) {
            // api target 1

            this.btnPhone.setOnClickListener(mCallback86);
            androidx.databinding.adapters.CompoundButtonBindingAdapter.setListeners(this.checkbox, mCallback85, (androidx.databinding.InverseBindingListener)null);
            this.downLocation.setOnClickListener(mCallback83);
            this.mboundView1.setOnClickListener(mCallback79);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.mboundView1, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, mboundView1androidTextAttrChanged);
            this.mboundView2.setOnClickListener(mCallback80);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.mboundView2, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, mboundView2androidTextAttrChanged);
            this.mboundView3.setOnClickListener(mCallback81);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.mboundView3, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, mboundView3androidTextAttrChanged);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.mboundView4, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, mboundView4androidTextAttrChanged);
            this.picTime.setOnClickListener(mCallback84);
            this.searchLocation.setOnClickListener(mCallback82);
        }
        if ((dirtyFlags & 0x25L) != 0) {
            // api target 1

            this.infoCard.setVisibility(textUtilsIsEmptyViewModelDetailsNoteViewVISIBLEViewGONE);
            this.mboundView0.setVisibility(viewModelDetailsLiterGallonListJavaLangObjectNullViewVISIBLEViewGONE);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvNote, viewModelDetailsNote);
        }
        if ((dirtyFlags & 0x29L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView1, viewModelServiceOrderRequestLitre);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView2, viewModelServiceOrderRequestTinkerServiceName);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView3, viewModelServiceOrderRequestGallon);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView4, viewModelServiceOrderRequestDesc);
        }
        if ((dirtyFlags & 0x23L) != 0) {
            // api target 1

            this.picTime.setEnabled(viewModelIsEmergencyAccepted);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        switch(sourceId) {
            case 1: {
                // localize variables for thread safety
                // viewModel
                grand.app.aber_user.pages.services.viewModels.ServicesViewModels viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {




                    viewModel.liveDataActions(grand.app.aber_user.utils.Constants.LITER);
                }
                break;
            }
            case 8: {
                // localize variables for thread safety
                // viewModel
                grand.app.aber_user.pages.services.viewModels.ServicesViewModels viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {


                    viewModel.toConfirmServiceWater();
                }
                break;
            }
            case 4: {
                // localize variables for thread safety
                // viewModel
                grand.app.aber_user.pages.services.viewModels.ServicesViewModels viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {




                    viewModel.liveDataActions(grand.app.aber_user.utils.Constants.FROM_SEARCH_LOCATION);
                }
                break;
            }
            case 5: {
                // localize variables for thread safety
                // viewModel
                grand.app.aber_user.pages.services.viewModels.ServicesViewModels viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {




                    viewModel.liveDataActions(grand.app.aber_user.utils.Constants.SEARCH_LOCATION);
                }
                break;
            }
            case 6: {
                // localize variables for thread safety
                // viewModel
                grand.app.aber_user.pages.services.viewModels.ServicesViewModels viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {




                    viewModel.liveDataActions(grand.app.aber_user.utils.Constants.DELIVERY_TIME);
                }
                break;
            }
            case 2: {
                // localize variables for thread safety
                // viewModel
                grand.app.aber_user.pages.services.viewModels.ServicesViewModels viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {




                    viewModel.liveDataActions(grand.app.aber_user.utils.Constants.TINKER_SERVICE_TYPE);
                }
                break;
            }
            case 3: {
                // localize variables for thread safety
                // viewModel
                grand.app.aber_user.pages.services.viewModels.ServicesViewModels viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {




                    viewModel.liveDataActions(grand.app.aber_user.utils.Constants.GALLON);
                }
                break;
            }
        }
    }
    public final void _internalCallbackOnCheckedChanged(int sourceId , android.widget.CompoundButton callbackArg_0, boolean callbackArg_1) {
        // localize variables for thread safety
        // viewModel
        grand.app.aber_user.pages.services.viewModels.ServicesViewModels viewModel = mViewModel;
        // viewModel != null
        boolean viewModelJavaLangObjectNull = false;



        viewModelJavaLangObjectNull = (viewModel) != (null);
        if (viewModelJavaLangObjectNull) {




            viewModel.onCheckedChange(callbackArg_0, callbackArg_1);
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): viewModel
        flag 1 (0x2L): viewModel.isEmergencyAccepted
        flag 2 (0x3L): viewModel.details
        flag 3 (0x4L): viewModel.serviceOrderRequest
        flag 4 (0x5L): viewModel.message
        flag 5 (0x6L): null
        flag 6 (0x7L): viewModel.details.literGallonList != null ? View.VISIBLE : View.GONE
        flag 7 (0x8L): viewModel.details.literGallonList != null ? View.VISIBLE : View.GONE
        flag 8 (0x9L): !TextUtils.isEmpty(viewModel.message) ? viewModel.message.equals(Constants.SHOW_PROGRESS) : false
        flag 9 (0xaL): !TextUtils.isEmpty(viewModel.message) ? viewModel.message.equals(Constants.SHOW_PROGRESS) : false
        flag 10 (0xbL): !TextUtils.isEmpty(viewModel.message) ? viewModel.message.equals(Constants.SHOW_PROGRESS) : false ? View.VISIBLE : View.GONE
        flag 11 (0xcL): !TextUtils.isEmpty(viewModel.message) ? viewModel.message.equals(Constants.SHOW_PROGRESS) : false ? View.VISIBLE : View.GONE
        flag 12 (0xdL): !TextUtils.isEmpty(viewModel.details.note) ? View.VISIBLE : View.GONE
        flag 13 (0xeL): !TextUtils.isEmpty(viewModel.details.note) ? View.VISIBLE : View.GONE
        flag 14 (0xfL): TextUtils.isEmpty(viewModel.message) ? true : viewModel.message.equals(Constants.HIDE_PROGRESS) ? @android:drawable/corner_view_gradient : @android:drawable/corner_view_primary_medium
        flag 15 (0x10L): TextUtils.isEmpty(viewModel.message) ? true : viewModel.message.equals(Constants.HIDE_PROGRESS) ? @android:drawable/corner_view_gradient : @android:drawable/corner_view_primary_medium
        flag 16 (0x11L): TextUtils.isEmpty(viewModel.message) ? true : viewModel.message.equals(Constants.HIDE_PROGRESS) ? true : false
        flag 17 (0x12L): TextUtils.isEmpty(viewModel.message) ? true : viewModel.message.equals(Constants.HIDE_PROGRESS) ? true : false
        flag 18 (0x13L): TextUtils.isEmpty(viewModel.message) ? true : viewModel.message.equals(Constants.HIDE_PROGRESS)
        flag 19 (0x14L): TextUtils.isEmpty(viewModel.message) ? true : viewModel.message.equals(Constants.HIDE_PROGRESS)
    flag mapping end*/
    //end
}