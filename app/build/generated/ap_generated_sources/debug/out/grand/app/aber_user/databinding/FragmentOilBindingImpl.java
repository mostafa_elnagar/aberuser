package grand.app.aber_user.databinding;
import grand.app.aber_user.R;
import grand.app.aber_user.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentOilBindingImpl extends FragmentOilBinding implements grand.app.aber_user.generated.callback.OnClickListener.Listener, grand.app.aber_user.generated.callback.OnCheckedChangeListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.searchContainer, 16);
        sViewsWithIds.put(R.id.input_oil_types, 17);
        sViewsWithIds.put(R.id.input_oil_liguied, 18);
        sViewsWithIds.put(R.id.input_car_types, 19);
        sViewsWithIds.put(R.id.input_car_categories, 20);
        sViewsWithIds.put(R.id.input_car_models, 21);
        sViewsWithIds.put(R.id.input_km, 22);
        sViewsWithIds.put(R.id.input_message, 23);
        sViewsWithIds.put(R.id.info_card, 24);
    }
    // views
    @NonNull
    private final androidx.core.widget.NestedScrollView mboundView0;
    @NonNull
    private final com.google.android.material.textfield.TextInputEditText mboundView1;
    @NonNull
    private final com.google.android.material.textfield.TextInputEditText mboundView2;
    @NonNull
    private final com.google.android.material.textfield.TextInputEditText mboundView3;
    @NonNull
    private final com.google.android.material.textfield.TextInputEditText mboundView4;
    @NonNull
    private final com.google.android.material.textfield.TextInputEditText mboundView5;
    @NonNull
    private final com.google.android.material.textfield.TextInputEditText mboundView6;
    @NonNull
    private final com.google.android.material.textfield.TextInputEditText mboundView8;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback128;
    @Nullable
    private final android.view.View.OnClickListener mCallback124;
    @Nullable
    private final android.widget.CompoundButton.OnCheckedChangeListener mCallback132;
    @Nullable
    private final android.view.View.OnClickListener mCallback129;
    @Nullable
    private final android.view.View.OnClickListener mCallback125;
    @Nullable
    private final android.view.View.OnClickListener mCallback133;
    @Nullable
    private final android.view.View.OnClickListener mCallback126;
    @Nullable
    private final android.view.View.OnClickListener mCallback134;
    @Nullable
    private final android.view.View.OnClickListener mCallback130;
    @Nullable
    private final android.view.View.OnClickListener mCallback127;
    @Nullable
    private final android.view.View.OnClickListener mCallback131;
    // values
    // listeners
    // Inverse Binding Event Handlers
    private androidx.databinding.InverseBindingListener mboundView1androidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.serviceOrderRequest.oilType
            //         is viewModel.serviceOrderRequest.setOilType((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(mboundView1);
            // localize variables for thread safety
            // viewModel.serviceOrderRequest != null
            boolean viewModelServiceOrderRequestJavaLangObjectNull = false;
            // viewModel.serviceOrderRequest
            grand.app.aber_user.pages.services.models.ServiceOrderDetails viewModelServiceOrderRequest = null;
            // viewModel
            grand.app.aber_user.pages.services.viewModels.ServicesViewModels viewModel = mViewModel;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;
            // viewModel.serviceOrderRequest.oilType
            java.lang.String viewModelServiceOrderRequestOilType = null;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelServiceOrderRequest = viewModel.getServiceOrderRequest();

                viewModelServiceOrderRequestJavaLangObjectNull = (viewModelServiceOrderRequest) != (null);
                if (viewModelServiceOrderRequestJavaLangObjectNull) {




                    viewModelServiceOrderRequest.setOilType(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener mboundView2androidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.serviceOrderRequest.oilLiquid
            //         is viewModel.serviceOrderRequest.setOilLiquid((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(mboundView2);
            // localize variables for thread safety
            // viewModel.serviceOrderRequest.oilLiquid
            java.lang.String viewModelServiceOrderRequestOilLiquid = null;
            // viewModel.serviceOrderRequest != null
            boolean viewModelServiceOrderRequestJavaLangObjectNull = false;
            // viewModel.serviceOrderRequest
            grand.app.aber_user.pages.services.models.ServiceOrderDetails viewModelServiceOrderRequest = null;
            // viewModel
            grand.app.aber_user.pages.services.viewModels.ServicesViewModels viewModel = mViewModel;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelServiceOrderRequest = viewModel.getServiceOrderRequest();

                viewModelServiceOrderRequestJavaLangObjectNull = (viewModelServiceOrderRequest) != (null);
                if (viewModelServiceOrderRequestJavaLangObjectNull) {




                    viewModelServiceOrderRequest.setOilLiquid(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener mboundView3androidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.serviceOrderRequest.carType
            //         is viewModel.serviceOrderRequest.setCarType((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(mboundView3);
            // localize variables for thread safety
            // viewModel.serviceOrderRequest.carType
            java.lang.String viewModelServiceOrderRequestCarType = null;
            // viewModel.serviceOrderRequest != null
            boolean viewModelServiceOrderRequestJavaLangObjectNull = false;
            // viewModel.serviceOrderRequest
            grand.app.aber_user.pages.services.models.ServiceOrderDetails viewModelServiceOrderRequest = null;
            // viewModel
            grand.app.aber_user.pages.services.viewModels.ServicesViewModels viewModel = mViewModel;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelServiceOrderRequest = viewModel.getServiceOrderRequest();

                viewModelServiceOrderRequestJavaLangObjectNull = (viewModelServiceOrderRequest) != (null);
                if (viewModelServiceOrderRequestJavaLangObjectNull) {




                    viewModelServiceOrderRequest.setCarType(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener mboundView4androidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.serviceOrderRequest.carCat
            //         is viewModel.serviceOrderRequest.setCarCat((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(mboundView4);
            // localize variables for thread safety
            // viewModel.serviceOrderRequest != null
            boolean viewModelServiceOrderRequestJavaLangObjectNull = false;
            // viewModel.serviceOrderRequest
            grand.app.aber_user.pages.services.models.ServiceOrderDetails viewModelServiceOrderRequest = null;
            // viewModel
            grand.app.aber_user.pages.services.viewModels.ServicesViewModels viewModel = mViewModel;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;
            // viewModel.serviceOrderRequest.carCat
            java.lang.String viewModelServiceOrderRequestCarCat = null;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelServiceOrderRequest = viewModel.getServiceOrderRequest();

                viewModelServiceOrderRequestJavaLangObjectNull = (viewModelServiceOrderRequest) != (null);
                if (viewModelServiceOrderRequestJavaLangObjectNull) {




                    viewModelServiceOrderRequest.setCarCat(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener mboundView5androidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.serviceOrderRequest.carModel
            //         is viewModel.serviceOrderRequest.setCarModel((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(mboundView5);
            // localize variables for thread safety
            // viewModel.serviceOrderRequest != null
            boolean viewModelServiceOrderRequestJavaLangObjectNull = false;
            // viewModel.serviceOrderRequest.carModel
            java.lang.String viewModelServiceOrderRequestCarModel = null;
            // viewModel.serviceOrderRequest
            grand.app.aber_user.pages.services.models.ServiceOrderDetails viewModelServiceOrderRequest = null;
            // viewModel
            grand.app.aber_user.pages.services.viewModels.ServicesViewModels viewModel = mViewModel;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelServiceOrderRequest = viewModel.getServiceOrderRequest();

                viewModelServiceOrderRequestJavaLangObjectNull = (viewModelServiceOrderRequest) != (null);
                if (viewModelServiceOrderRequestJavaLangObjectNull) {




                    viewModelServiceOrderRequest.setCarModel(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener mboundView6androidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.serviceOrderRequest.km
            //         is viewModel.serviceOrderRequest.setKm((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(mboundView6);
            // localize variables for thread safety
            // viewModel.serviceOrderRequest.km
            java.lang.String viewModelServiceOrderRequestKm = null;
            // viewModel.serviceOrderRequest != null
            boolean viewModelServiceOrderRequestJavaLangObjectNull = false;
            // viewModel.serviceOrderRequest
            grand.app.aber_user.pages.services.models.ServiceOrderDetails viewModelServiceOrderRequest = null;
            // viewModel
            grand.app.aber_user.pages.services.viewModels.ServicesViewModels viewModel = mViewModel;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelServiceOrderRequest = viewModel.getServiceOrderRequest();

                viewModelServiceOrderRequestJavaLangObjectNull = (viewModelServiceOrderRequest) != (null);
                if (viewModelServiceOrderRequestJavaLangObjectNull) {




                    viewModelServiceOrderRequest.setKm(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener mboundView8androidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.serviceOrderRequest.desc
            //         is viewModel.serviceOrderRequest.setDesc((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(mboundView8);
            // localize variables for thread safety
            // viewModel.serviceOrderRequest != null
            boolean viewModelServiceOrderRequestJavaLangObjectNull = false;
            // viewModel.serviceOrderRequest.desc
            java.lang.String viewModelServiceOrderRequestDesc = null;
            // viewModel.serviceOrderRequest
            grand.app.aber_user.pages.services.models.ServiceOrderDetails viewModelServiceOrderRequest = null;
            // viewModel
            grand.app.aber_user.pages.services.viewModels.ServicesViewModels viewModel = mViewModel;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelServiceOrderRequest = viewModel.getServiceOrderRequest();

                viewModelServiceOrderRequestJavaLangObjectNull = (viewModelServiceOrderRequest) != (null);
                if (viewModelServiceOrderRequestJavaLangObjectNull) {




                    viewModelServiceOrderRequest.setDesc(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };

    public FragmentOilBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 25, sIncludes, sViewsWithIds));
    }
    private FragmentOilBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 2
            , (androidx.appcompat.widget.AppCompatButton) bindings[14]
            , (com.google.android.material.checkbox.MaterialCheckBox) bindings[11]
            , (androidx.cardview.widget.CardView) bindings[24]
            , (com.google.android.material.textfield.TextInputLayout) bindings[20]
            , (com.google.android.material.textfield.TextInputLayout) bindings[21]
            , (com.google.android.material.textfield.TextInputLayout) bindings[19]
            , (com.google.android.material.textfield.TextInputLayout) bindings[22]
            , (com.google.android.material.textfield.TextInputLayout) bindings[23]
            , (com.google.android.material.textfield.TextInputLayout) bindings[18]
            , (com.google.android.material.textfield.TextInputLayout) bindings[17]
            , (com.google.android.material.button.MaterialButton) bindings[10]
            , (com.google.android.material.progressindicator.CircularProgressIndicator) bindings[15]
            , (androidx.recyclerview.widget.RecyclerView) bindings[7]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[16]
            , (com.google.android.material.button.MaterialButton) bindings[9]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[12]
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[13]
            );
        this.btnPhone.setTag(null);
        this.checkbox.setTag(null);
        this.mboundView0 = (androidx.core.widget.NestedScrollView) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (com.google.android.material.textfield.TextInputEditText) bindings[1];
        this.mboundView1.setTag(null);
        this.mboundView2 = (com.google.android.material.textfield.TextInputEditText) bindings[2];
        this.mboundView2.setTag(null);
        this.mboundView3 = (com.google.android.material.textfield.TextInputEditText) bindings[3];
        this.mboundView3.setTag(null);
        this.mboundView4 = (com.google.android.material.textfield.TextInputEditText) bindings[4];
        this.mboundView4.setTag(null);
        this.mboundView5 = (com.google.android.material.textfield.TextInputEditText) bindings[5];
        this.mboundView5.setTag(null);
        this.mboundView6 = (com.google.android.material.textfield.TextInputEditText) bindings[6];
        this.mboundView6.setTag(null);
        this.mboundView8 = (com.google.android.material.textfield.TextInputEditText) bindings[8];
        this.mboundView8.setTag(null);
        this.picTime.setTag(null);
        this.progress.setTag(null);
        this.rcExtraServices.setTag(null);
        this.searchLocation.setTag(null);
        this.tvNote.setTag(null);
        this.tvPrice.setTag(null);
        setRootTag(root);
        // listeners
        mCallback128 = new grand.app.aber_user.generated.callback.OnClickListener(this, 5);
        mCallback124 = new grand.app.aber_user.generated.callback.OnClickListener(this, 1);
        mCallback132 = new grand.app.aber_user.generated.callback.OnCheckedChangeListener(this, 9);
        mCallback129 = new grand.app.aber_user.generated.callback.OnClickListener(this, 6);
        mCallback125 = new grand.app.aber_user.generated.callback.OnClickListener(this, 2);
        mCallback133 = new grand.app.aber_user.generated.callback.OnClickListener(this, 10);
        mCallback126 = new grand.app.aber_user.generated.callback.OnClickListener(this, 3);
        mCallback134 = new grand.app.aber_user.generated.callback.OnClickListener(this, 11);
        mCallback130 = new grand.app.aber_user.generated.callback.OnClickListener(this, 7);
        mCallback127 = new grand.app.aber_user.generated.callback.OnClickListener(this, 4);
        mCallback131 = new grand.app.aber_user.generated.callback.OnClickListener(this, 8);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x40L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.viewModel == variableId) {
            setViewModel((grand.app.aber_user.pages.services.viewModels.ServicesViewModels) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setViewModel(@Nullable grand.app.aber_user.pages.services.viewModels.ServicesViewModels ViewModel) {
        updateRegistration(0, ViewModel);
        this.mViewModel = ViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.viewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeViewModel((grand.app.aber_user.pages.services.viewModels.ServicesViewModels) object, fieldId);
            case 1 :
                return onChangeViewModelIsEmergencyAccepted((androidx.databinding.ObservableBoolean) object, fieldId);
        }
        return false;
    }
    private boolean onChangeViewModel(grand.app.aber_user.pages.services.viewModels.ServicesViewModels ViewModel, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        else if (fieldId == BR.details) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        else if (fieldId == BR.serviceOrderRequest) {
            synchronized(this) {
                    mDirtyFlags |= 0x8L;
            }
            return true;
        }
        else if (fieldId == BR.extraAdapter) {
            synchronized(this) {
                    mDirtyFlags |= 0x10L;
            }
            return true;
        }
        else if (fieldId == BR.message) {
            synchronized(this) {
                    mDirtyFlags |= 0x20L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelIsEmergencyAccepted(androidx.databinding.ObservableBoolean ViewModelIsEmergencyAccepted, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        boolean textUtilsIsEmptyViewModelMessage = false;
        java.lang.String viewModelMessage = null;
        java.lang.String viewModelServiceOrderRequestDesc = null;
        boolean viewModelIsEmergencyAccepted = false;
        boolean TextUtilsIsEmptyViewModelMessage1 = false;
        boolean textUtilsIsEmptyViewModelMessageViewModelMessageEqualsConstantsSHOWPROGRESSBooleanFalse = false;
        int textUtilsIsEmptyViewModelMessageViewModelMessageEqualsConstantsSHOWPROGRESSBooleanFalseViewVISIBLEViewGONE = 0;
        java.util.List<grand.app.aber_user.model.DropDownsObject> viewModelDetailsOilList = null;
        java.lang.String viewModelServiceOrderRequestOilLiquid = null;
        java.lang.String viewModelDetailsNote = null;
        android.graphics.drawable.Drawable textUtilsIsEmptyViewModelMessageBooleanTrueViewModelMessageEqualsConstantsHIDEPROGRESSBtnPhoneAndroidDrawableCornerViewGradientBtnPhoneAndroidDrawableCornerViewPrimaryMedium = null;
        boolean viewModelMessageEqualsConstantsHIDEPROGRESS = false;
        boolean viewModelIsEmergencyAcceptedGet = false;
        grand.app.aber_user.pages.services.models.ServiceDetails viewModelDetails = null;
        int viewModelDetailsOilListSizeInt0ViewVISIBLEViewGONE = 0;
        java.lang.String viewModelServiceOrderRequestKm = null;
        java.lang.String viewModelServiceOrderRequestCarModel = null;
        grand.app.aber_user.pages.services.models.ServiceOrderDetails viewModelServiceOrderRequest = null;
        java.lang.String viewModelServiceOrderRequestOilType = null;
        grand.app.aber_user.pages.services.adapters.ExtraAdapter viewModelExtraAdapter = null;
        boolean viewModelDetailsOilListSizeInt0 = false;
        boolean textUtilsIsEmptyViewModelMessageBooleanTrueViewModelMessageEqualsConstantsHIDEPROGRESSBooleanTrueBooleanFalse = false;
        boolean viewModelMessageEqualsConstantsSHOWPROGRESS = false;
        java.lang.String viewModelServiceOrderRequestCarType = null;
        int viewModelDetailsOilListSize = 0;
        grand.app.aber_user.pages.services.viewModels.ServicesViewModels viewModel = mViewModel;
        boolean textUtilsIsEmptyViewModelMessageBooleanTrueViewModelMessageEqualsConstantsHIDEPROGRESS = false;
        java.lang.String viewModelServiceOrderRequestCarCat = null;
        androidx.databinding.ObservableBoolean ViewModelIsEmergencyAccepted1 = null;

        if ((dirtyFlags & 0x7fL) != 0) {


            if ((dirtyFlags & 0x61L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.message
                        viewModelMessage = viewModel.getMessage();
                    }


                    // read TextUtils.isEmpty(viewModel.message)
                    TextUtilsIsEmptyViewModelMessage1 = android.text.TextUtils.isEmpty(viewModelMessage);
                if((dirtyFlags & 0x61L) != 0) {
                    if(TextUtilsIsEmptyViewModelMessage1) {
                            dirtyFlags |= 0x40000L;
                    }
                    else {
                            dirtyFlags |= 0x20000L;
                    }
                }


                    // read !TextUtils.isEmpty(viewModel.message)
                    textUtilsIsEmptyViewModelMessage = !TextUtilsIsEmptyViewModelMessage1;
                if((dirtyFlags & 0x61L) != 0) {
                    if(textUtilsIsEmptyViewModelMessage) {
                            dirtyFlags |= 0x100L;
                    }
                    else {
                            dirtyFlags |= 0x80L;
                    }
                }
            }
            if ((dirtyFlags & 0x45L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.details
                        viewModelDetails = viewModel.getDetails();
                    }


                    if (viewModelDetails != null) {
                        // read viewModel.details.oilList
                        viewModelDetailsOilList = viewModelDetails.getOilList();
                        // read viewModel.details.note
                        viewModelDetailsNote = viewModelDetails.getNote();
                    }


                    if (viewModelDetailsOilList != null) {
                        // read viewModel.details.oilList.size()
                        viewModelDetailsOilListSize = viewModelDetailsOilList.size();
                    }


                    // read viewModel.details.oilList.size() > 0
                    viewModelDetailsOilListSizeInt0 = (viewModelDetailsOilListSize) > (0);
                if((dirtyFlags & 0x45L) != 0) {
                    if(viewModelDetailsOilListSizeInt0) {
                            dirtyFlags |= 0x4000L;
                    }
                    else {
                            dirtyFlags |= 0x2000L;
                    }
                }


                    // read viewModel.details.oilList.size() > 0 ? View.VISIBLE : View.GONE
                    viewModelDetailsOilListSizeInt0ViewVISIBLEViewGONE = ((viewModelDetailsOilListSizeInt0) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
            if ((dirtyFlags & 0x49L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.serviceOrderRequest
                        viewModelServiceOrderRequest = viewModel.getServiceOrderRequest();
                    }


                    if (viewModelServiceOrderRequest != null) {
                        // read viewModel.serviceOrderRequest.desc
                        viewModelServiceOrderRequestDesc = viewModelServiceOrderRequest.getDesc();
                        // read viewModel.serviceOrderRequest.oilLiquid
                        viewModelServiceOrderRequestOilLiquid = viewModelServiceOrderRequest.getOilLiquid();
                        // read viewModel.serviceOrderRequest.km
                        viewModelServiceOrderRequestKm = viewModelServiceOrderRequest.getKm();
                        // read viewModel.serviceOrderRequest.carModel
                        viewModelServiceOrderRequestCarModel = viewModelServiceOrderRequest.getCarModel();
                        // read viewModel.serviceOrderRequest.oilType
                        viewModelServiceOrderRequestOilType = viewModelServiceOrderRequest.getOilType();
                        // read viewModel.serviceOrderRequest.carType
                        viewModelServiceOrderRequestCarType = viewModelServiceOrderRequest.getCarType();
                        // read viewModel.serviceOrderRequest.carCat
                        viewModelServiceOrderRequestCarCat = viewModelServiceOrderRequest.getCarCat();
                    }
            }
            if ((dirtyFlags & 0x51L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.extraAdapter
                        viewModelExtraAdapter = viewModel.getExtraAdapter();
                    }
            }
            if ((dirtyFlags & 0x43L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.isEmergencyAccepted
                        ViewModelIsEmergencyAccepted1 = viewModel.isEmergencyAccepted;
                    }
                    updateRegistration(1, ViewModelIsEmergencyAccepted1);


                    if (ViewModelIsEmergencyAccepted1 != null) {
                        // read viewModel.isEmergencyAccepted.get()
                        viewModelIsEmergencyAcceptedGet = ViewModelIsEmergencyAccepted1.get();
                    }


                    // read !viewModel.isEmergencyAccepted.get()
                    viewModelIsEmergencyAccepted = !viewModelIsEmergencyAcceptedGet;
            }
        }
        // batch finished

        if ((dirtyFlags & 0x20000L) != 0) {

                if (viewModelMessage != null) {
                    // read viewModel.message.equals(Constants.HIDE_PROGRESS)
                    viewModelMessageEqualsConstantsHIDEPROGRESS = viewModelMessage.equals(grand.app.aber_user.utils.Constants.HIDE_PROGRESS);
                }
        }
        if ((dirtyFlags & 0x100L) != 0) {

                if (viewModelMessage != null) {
                    // read viewModel.message.equals(Constants.SHOW_PROGRESS)
                    viewModelMessageEqualsConstantsSHOWPROGRESS = viewModelMessage.equals(grand.app.aber_user.utils.Constants.SHOW_PROGRESS);
                }
        }

        if ((dirtyFlags & 0x61L) != 0) {

                // read !TextUtils.isEmpty(viewModel.message) ? viewModel.message.equals(Constants.SHOW_PROGRESS) : false
                textUtilsIsEmptyViewModelMessageViewModelMessageEqualsConstantsSHOWPROGRESSBooleanFalse = ((textUtilsIsEmptyViewModelMessage) ? (viewModelMessageEqualsConstantsSHOWPROGRESS) : (false));
                // read TextUtils.isEmpty(viewModel.message) ? true : viewModel.message.equals(Constants.HIDE_PROGRESS)
                textUtilsIsEmptyViewModelMessageBooleanTrueViewModelMessageEqualsConstantsHIDEPROGRESS = ((TextUtilsIsEmptyViewModelMessage1) ? (true) : (viewModelMessageEqualsConstantsHIDEPROGRESS));
            if((dirtyFlags & 0x61L) != 0) {
                if(textUtilsIsEmptyViewModelMessageViewModelMessageEqualsConstantsSHOWPROGRESSBooleanFalse) {
                        dirtyFlags |= 0x400L;
                }
                else {
                        dirtyFlags |= 0x200L;
                }
            }
            if((dirtyFlags & 0x61L) != 0) {
                if(textUtilsIsEmptyViewModelMessageBooleanTrueViewModelMessageEqualsConstantsHIDEPROGRESS) {
                        dirtyFlags |= 0x1000L;
                        dirtyFlags |= 0x10000L;
                }
                else {
                        dirtyFlags |= 0x800L;
                        dirtyFlags |= 0x8000L;
                }
            }


                // read !TextUtils.isEmpty(viewModel.message) ? viewModel.message.equals(Constants.SHOW_PROGRESS) : false ? View.VISIBLE : View.GONE
                textUtilsIsEmptyViewModelMessageViewModelMessageEqualsConstantsSHOWPROGRESSBooleanFalseViewVISIBLEViewGONE = ((textUtilsIsEmptyViewModelMessageViewModelMessageEqualsConstantsSHOWPROGRESSBooleanFalse) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
                // read TextUtils.isEmpty(viewModel.message) ? true : viewModel.message.equals(Constants.HIDE_PROGRESS) ? @android:drawable/corner_view_gradient : @android:drawable/corner_view_primary_medium
                textUtilsIsEmptyViewModelMessageBooleanTrueViewModelMessageEqualsConstantsHIDEPROGRESSBtnPhoneAndroidDrawableCornerViewGradientBtnPhoneAndroidDrawableCornerViewPrimaryMedium = ((textUtilsIsEmptyViewModelMessageBooleanTrueViewModelMessageEqualsConstantsHIDEPROGRESS) ? (androidx.appcompat.content.res.AppCompatResources.getDrawable(btnPhone.getContext(), R.drawable.corner_view_gradient)) : (androidx.appcompat.content.res.AppCompatResources.getDrawable(btnPhone.getContext(), R.drawable.corner_view_primary_medium)));
                // read TextUtils.isEmpty(viewModel.message) ? true : viewModel.message.equals(Constants.HIDE_PROGRESS) ? true : false
                textUtilsIsEmptyViewModelMessageBooleanTrueViewModelMessageEqualsConstantsHIDEPROGRESSBooleanTrueBooleanFalse = ((textUtilsIsEmptyViewModelMessageBooleanTrueViewModelMessageEqualsConstantsHIDEPROGRESS) ? (true) : (false));
        }
        // batch finished
        if ((dirtyFlags & 0x61L) != 0) {
            // api target 1

            androidx.databinding.adapters.ViewBindingAdapter.setBackground(this.btnPhone, textUtilsIsEmptyViewModelMessageBooleanTrueViewModelMessageEqualsConstantsHIDEPROGRESSBtnPhoneAndroidDrawableCornerViewGradientBtnPhoneAndroidDrawableCornerViewPrimaryMedium);
            this.btnPhone.setEnabled(textUtilsIsEmptyViewModelMessageBooleanTrueViewModelMessageEqualsConstantsHIDEPROGRESSBooleanTrueBooleanFalse);
            this.progress.setVisibility(textUtilsIsEmptyViewModelMessageViewModelMessageEqualsConstantsSHOWPROGRESSBooleanFalseViewVISIBLEViewGONE);
        }
        if ((dirtyFlags & 0x40L) != 0) {
            // api target 1

            this.btnPhone.setOnClickListener(mCallback134);
            androidx.databinding.adapters.CompoundButtonBindingAdapter.setListeners(this.checkbox, mCallback132, (androidx.databinding.InverseBindingListener)null);
            this.mboundView1.setOnClickListener(mCallback124);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.mboundView1, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, mboundView1androidTextAttrChanged);
            this.mboundView2.setOnClickListener(mCallback125);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.mboundView2, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, mboundView2androidTextAttrChanged);
            this.mboundView3.setOnClickListener(mCallback126);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.mboundView3, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, mboundView3androidTextAttrChanged);
            this.mboundView4.setOnClickListener(mCallback127);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.mboundView4, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, mboundView4androidTextAttrChanged);
            this.mboundView5.setOnClickListener(mCallback128);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.mboundView5, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, mboundView5androidTextAttrChanged);
            this.mboundView6.setOnClickListener(mCallback129);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.mboundView6, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, mboundView6androidTextAttrChanged);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.mboundView8, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, mboundView8androidTextAttrChanged);
            this.picTime.setOnClickListener(mCallback131);
            this.searchLocation.setOnClickListener(mCallback130);
            this.tvPrice.setOnClickListener(mCallback133);
        }
        if ((dirtyFlags & 0x45L) != 0) {
            // api target 1

            this.mboundView0.setVisibility(viewModelDetailsOilListSizeInt0ViewVISIBLEViewGONE);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvNote, viewModelDetailsNote);
        }
        if ((dirtyFlags & 0x49L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView1, viewModelServiceOrderRequestOilType);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView2, viewModelServiceOrderRequestOilLiquid);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView3, viewModelServiceOrderRequestCarType);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView4, viewModelServiceOrderRequestCarCat);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView5, viewModelServiceOrderRequestCarModel);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView6, viewModelServiceOrderRequestKm);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView8, viewModelServiceOrderRequestDesc);
        }
        if ((dirtyFlags & 0x43L) != 0) {
            // api target 1

            this.picTime.setEnabled(viewModelIsEmergencyAccepted);
        }
        if ((dirtyFlags & 0x51L) != 0) {
            // api target 1

            grand.app.aber_user.base.ApplicationBinding.getItemsV2Binding(this.rcExtraServices, viewModelExtraAdapter, "1", "1");
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        switch(sourceId) {
            case 5: {
                // localize variables for thread safety
                // viewModel
                grand.app.aber_user.pages.services.viewModels.ServicesViewModels viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {




                    viewModel.liveDataActions(grand.app.aber_user.utils.Constants.CAR_MODEL);
                }
                break;
            }
            case 1: {
                // localize variables for thread safety
                // viewModel
                grand.app.aber_user.pages.services.viewModels.ServicesViewModels viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {




                    viewModel.liveDataActions(grand.app.aber_user.utils.Constants.OIL);
                }
                break;
            }
            case 6: {
                // localize variables for thread safety
                // viewModel
                grand.app.aber_user.pages.services.viewModels.ServicesViewModels viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {




                    viewModel.liveDataActions(grand.app.aber_user.utils.Constants.KM);
                }
                break;
            }
            case 2: {
                // localize variables for thread safety
                // viewModel
                grand.app.aber_user.pages.services.viewModels.ServicesViewModels viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {




                    viewModel.liveDataActions(grand.app.aber_user.utils.Constants.OIL_LIQUID);
                }
                break;
            }
            case 10: {
                // localize variables for thread safety
                // viewModel
                grand.app.aber_user.pages.services.viewModels.ServicesViewModels viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {




                    viewModel.liveDataActions(grand.app.aber_user.utils.Constants.PRICES_LIST);
                }
                break;
            }
            case 3: {
                // localize variables for thread safety
                // viewModel
                grand.app.aber_user.pages.services.viewModels.ServicesViewModels viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {




                    viewModel.liveDataActions(grand.app.aber_user.utils.Constants.CAR_TYPE);
                }
                break;
            }
            case 11: {
                // localize variables for thread safety
                // viewModel
                grand.app.aber_user.pages.services.viewModels.ServicesViewModels viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {


                    viewModel.toConfirmServiceOil();
                }
                break;
            }
            case 7: {
                // localize variables for thread safety
                // viewModel
                grand.app.aber_user.pages.services.viewModels.ServicesViewModels viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {




                    viewModel.liveDataActions(grand.app.aber_user.utils.Constants.SEARCH_LOCATION);
                }
                break;
            }
            case 4: {
                // localize variables for thread safety
                // viewModel
                grand.app.aber_user.pages.services.viewModels.ServicesViewModels viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {




                    viewModel.liveDataActions(grand.app.aber_user.utils.Constants.CAR_CAT);
                }
                break;
            }
            case 8: {
                // localize variables for thread safety
                // viewModel
                grand.app.aber_user.pages.services.viewModels.ServicesViewModels viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {




                    viewModel.liveDataActions(grand.app.aber_user.utils.Constants.DELIVERY_TIME);
                }
                break;
            }
        }
    }
    public final void _internalCallbackOnCheckedChanged(int sourceId , android.widget.CompoundButton callbackArg_0, boolean callbackArg_1) {
        // localize variables for thread safety
        // viewModel
        grand.app.aber_user.pages.services.viewModels.ServicesViewModels viewModel = mViewModel;
        // viewModel != null
        boolean viewModelJavaLangObjectNull = false;



        viewModelJavaLangObjectNull = (viewModel) != (null);
        if (viewModelJavaLangObjectNull) {




            viewModel.onCheckedChange(callbackArg_0, callbackArg_1);
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): viewModel
        flag 1 (0x2L): viewModel.isEmergencyAccepted
        flag 2 (0x3L): viewModel.details
        flag 3 (0x4L): viewModel.serviceOrderRequest
        flag 4 (0x5L): viewModel.extraAdapter
        flag 5 (0x6L): viewModel.message
        flag 6 (0x7L): null
        flag 7 (0x8L): !TextUtils.isEmpty(viewModel.message) ? viewModel.message.equals(Constants.SHOW_PROGRESS) : false
        flag 8 (0x9L): !TextUtils.isEmpty(viewModel.message) ? viewModel.message.equals(Constants.SHOW_PROGRESS) : false
        flag 9 (0xaL): !TextUtils.isEmpty(viewModel.message) ? viewModel.message.equals(Constants.SHOW_PROGRESS) : false ? View.VISIBLE : View.GONE
        flag 10 (0xbL): !TextUtils.isEmpty(viewModel.message) ? viewModel.message.equals(Constants.SHOW_PROGRESS) : false ? View.VISIBLE : View.GONE
        flag 11 (0xcL): TextUtils.isEmpty(viewModel.message) ? true : viewModel.message.equals(Constants.HIDE_PROGRESS) ? @android:drawable/corner_view_gradient : @android:drawable/corner_view_primary_medium
        flag 12 (0xdL): TextUtils.isEmpty(viewModel.message) ? true : viewModel.message.equals(Constants.HIDE_PROGRESS) ? @android:drawable/corner_view_gradient : @android:drawable/corner_view_primary_medium
        flag 13 (0xeL): viewModel.details.oilList.size() > 0 ? View.VISIBLE : View.GONE
        flag 14 (0xfL): viewModel.details.oilList.size() > 0 ? View.VISIBLE : View.GONE
        flag 15 (0x10L): TextUtils.isEmpty(viewModel.message) ? true : viewModel.message.equals(Constants.HIDE_PROGRESS) ? true : false
        flag 16 (0x11L): TextUtils.isEmpty(viewModel.message) ? true : viewModel.message.equals(Constants.HIDE_PROGRESS) ? true : false
        flag 17 (0x12L): TextUtils.isEmpty(viewModel.message) ? true : viewModel.message.equals(Constants.HIDE_PROGRESS)
        flag 18 (0x13L): TextUtils.isEmpty(viewModel.message) ? true : viewModel.message.equals(Constants.HIDE_PROGRESS)
    flag mapping end*/
    //end
}