// Generated by Dagger (https://dagger.dev).
package grand.app.aber_user.pages.parts.viewModels;

import dagger.MembersInjector;
import dagger.internal.InjectedFieldSignature;
import grand.app.aber_user.repository.ServicesRepository;
import javax.inject.Provider;

@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class ProductDetailsViewModels_MembersInjector implements MembersInjector<ProductDetailsViewModels> {
  private final Provider<ServicesRepository> servicesRepositoryProvider;

  public ProductDetailsViewModels_MembersInjector(
      Provider<ServicesRepository> servicesRepositoryProvider) {
    this.servicesRepositoryProvider = servicesRepositoryProvider;
  }

  public static MembersInjector<ProductDetailsViewModels> create(
      Provider<ServicesRepository> servicesRepositoryProvider) {
    return new ProductDetailsViewModels_MembersInjector(servicesRepositoryProvider);
  }

  @Override
  public void injectMembers(ProductDetailsViewModels instance) {
    injectServicesRepository(instance, servicesRepositoryProvider.get());
  }

  @InjectedFieldSignature("grand.app.aber_user.pages.parts.viewModels.ProductDetailsViewModels.servicesRepository")
  public static void injectServicesRepository(ProductDetailsViewModels instance,
      ServicesRepository servicesRepository) {
    instance.servicesRepository = servicesRepository;
  }
}
