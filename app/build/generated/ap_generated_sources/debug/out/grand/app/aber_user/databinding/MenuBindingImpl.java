package grand.app.aber_user.databinding;
import grand.app.aber_user.R;
import grand.app.aber_user.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class MenuBindingImpl extends MenuBinding implements grand.app.aber_user.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.v1, 21);
        sViewsWithIds.put(R.id.v2, 22);
        sViewsWithIds.put(R.id.v3, 23);
        sViewsWithIds.put(R.id.v4, 24);
        sViewsWithIds.put(R.id.v5, 25);
        sViewsWithIds.put(R.id.v18, 26);
        sViewsWithIds.put(R.id.v6, 27);
        sViewsWithIds.put(R.id.v7, 28);
        sViewsWithIds.put(R.id.v8, 29);
        sViewsWithIds.put(R.id.v9, 30);
        sViewsWithIds.put(R.id.v10, 31);
        sViewsWithIds.put(R.id.v12, 32);
        sViewsWithIds.put(R.id.v13, 33);
        sViewsWithIds.put(R.id.v14, 34);
        sViewsWithIds.put(R.id.v15, 35);
        sViewsWithIds.put(R.id.v16, 36);
        sViewsWithIds.put(R.id.v17, 37);
        sViewsWithIds.put(R.id.v21, 38);
        sViewsWithIds.put(R.id.grand_logo, 39);
    }
    // views
    @NonNull
    private final androidx.core.widget.NestedScrollView mboundView0;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback201;
    @Nullable
    private final android.view.View.OnClickListener mCallback188;
    @Nullable
    private final android.view.View.OnClickListener mCallback184;
    @Nullable
    private final android.view.View.OnClickListener mCallback196;
    @Nullable
    private final android.view.View.OnClickListener mCallback192;
    @Nullable
    private final android.view.View.OnClickListener mCallback189;
    @Nullable
    private final android.view.View.OnClickListener mCallback197;
    @Nullable
    private final android.view.View.OnClickListener mCallback185;
    @Nullable
    private final android.view.View.OnClickListener mCallback193;
    @Nullable
    private final android.view.View.OnClickListener mCallback186;
    @Nullable
    private final android.view.View.OnClickListener mCallback198;
    @Nullable
    private final android.view.View.OnClickListener mCallback194;
    @Nullable
    private final android.view.View.OnClickListener mCallback199;
    @Nullable
    private final android.view.View.OnClickListener mCallback200;
    @Nullable
    private final android.view.View.OnClickListener mCallback187;
    @Nullable
    private final android.view.View.OnClickListener mCallback195;
    @Nullable
    private final android.view.View.OnClickListener mCallback191;
    @Nullable
    private final android.view.View.OnClickListener mCallback190;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public MenuBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 40, sIncludes, sViewsWithIds));
    }
    private MenuBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 2
            , (grand.app.aber_user.customViews.grandDialog.GrandImageDialog) bindings[39]
            , (androidx.recyclerview.widget.RecyclerView) bindings[20]
            , (androidx.recyclerview.widget.RecyclerView) bindings[5]
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[10]
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[8]
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[3]
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[12]
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[18]
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[2]
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[1]
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[17]
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[19]
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[7]
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[6]
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[9]
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[15]
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[4]
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[16]
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[14]
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[13]
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[11]
            , (android.view.View) bindings[21]
            , (android.view.View) bindings[31]
            , (android.view.View) bindings[32]
            , (android.view.View) bindings[33]
            , (android.view.View) bindings[34]
            , (android.view.View) bindings[35]
            , (android.view.View) bindings[36]
            , (android.view.View) bindings[37]
            , (android.view.View) bindings[26]
            , (android.view.View) bindings[22]
            , (android.view.View) bindings[38]
            , (android.view.View) bindings[23]
            , (android.view.View) bindings[24]
            , (android.view.View) bindings[25]
            , (android.view.View) bindings[27]
            , (android.view.View) bindings[28]
            , (android.view.View) bindings[29]
            , (android.view.View) bindings[30]
            );
        this.mboundView0 = (androidx.core.widget.NestedScrollView) bindings[0];
        this.mboundView0.setTag(null);
        this.rcMenuSocial.setTag(null);
        this.rcServices.setTag(null);
        this.tvAbout.setTag(null);
        this.tvAccount.setTag(null);
        this.tvCart.setTag(null);
        this.tvContact.setTag(null);
        this.tvCountry.setTag(null);
        this.tvFavorite.setTag(null);
        this.tvHome.setTag(null);
        this.tvLang.setTag(null);
        this.tvLogout.setTag(null);
        this.tvMyOrders.setTag(null);
        this.tvMyServices.setTag(null);
        this.tvProvider.setTag(null);
        this.tvRate.setTag(null);
        this.tvServices.setTag(null);
        this.tvShare.setTag(null);
        this.tvSocial.setTag(null);
        this.tvSuggest.setTag(null);
        this.tvTerms.setTag(null);
        setRootTag(root);
        // listeners
        mCallback201 = new grand.app.aber_user.generated.callback.OnClickListener(this, 18);
        mCallback188 = new grand.app.aber_user.generated.callback.OnClickListener(this, 5);
        mCallback184 = new grand.app.aber_user.generated.callback.OnClickListener(this, 1);
        mCallback196 = new grand.app.aber_user.generated.callback.OnClickListener(this, 13);
        mCallback192 = new grand.app.aber_user.generated.callback.OnClickListener(this, 9);
        mCallback189 = new grand.app.aber_user.generated.callback.OnClickListener(this, 6);
        mCallback197 = new grand.app.aber_user.generated.callback.OnClickListener(this, 14);
        mCallback185 = new grand.app.aber_user.generated.callback.OnClickListener(this, 2);
        mCallback193 = new grand.app.aber_user.generated.callback.OnClickListener(this, 10);
        mCallback186 = new grand.app.aber_user.generated.callback.OnClickListener(this, 3);
        mCallback198 = new grand.app.aber_user.generated.callback.OnClickListener(this, 15);
        mCallback194 = new grand.app.aber_user.generated.callback.OnClickListener(this, 11);
        mCallback199 = new grand.app.aber_user.generated.callback.OnClickListener(this, 16);
        mCallback200 = new grand.app.aber_user.generated.callback.OnClickListener(this, 17);
        mCallback187 = new grand.app.aber_user.generated.callback.OnClickListener(this, 4);
        mCallback195 = new grand.app.aber_user.generated.callback.OnClickListener(this, 12);
        mCallback191 = new grand.app.aber_user.generated.callback.OnClickListener(this, 8);
        mCallback190 = new grand.app.aber_user.generated.callback.OnClickListener(this, 7);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x10L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.menuViewModel == variableId) {
            setMenuViewModel((grand.app.aber_user.customViews.views.MenuViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setMenuViewModel(@Nullable grand.app.aber_user.customViews.views.MenuViewModel MenuViewModel) {
        updateRegistration(0, MenuViewModel);
        this.mMenuViewModel = MenuViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.menuViewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeMenuViewModel((grand.app.aber_user.customViews.views.MenuViewModel) object, fieldId);
            case 1 :
                return onChangeMenuViewModelShowMenu((androidx.databinding.ObservableBoolean) object, fieldId);
        }
        return false;
    }
    private boolean onChangeMenuViewModel(grand.app.aber_user.customViews.views.MenuViewModel MenuViewModel, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        else if (fieldId == BR.menuServicesAdapter) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        else if (fieldId == BR.socialAdapter) {
            synchronized(this) {
                    mDirtyFlags |= 0x8L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeMenuViewModelShowMenu(androidx.databinding.ObservableBoolean MenuViewModelShowMenu, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        grand.app.aber_user.customViews.views.MenuViewModel menuViewModel = mMenuViewModel;
        boolean menuViewModelShowMenuGet = false;
        boolean menuViewModelShowMenuBooleanTrue = false;
        androidx.databinding.ObservableBoolean menuViewModelShowMenu = null;
        grand.app.aber_user.pages.auth.models.UserData menuViewModelUserData = null;
        boolean menuViewModelUserDataJavaLangObjectNull = false;
        java.lang.String menuViewModelUserDataJavaLangObjectNullTvLogoutAndroidStringLogoutTvLogoutAndroidStringLogin = null;
        grand.app.aber_user.pages.settings.adapters.MenuSocialAdapter menuViewModelSocialAdapter = null;
        grand.app.aber_user.pages.home.adapters.MenuServicesAdapter menuViewModelMenuServicesAdapter = null;
        int menuViewModelShowMenuBooleanTrueViewVISIBLEViewGONE = 0;

        if ((dirtyFlags & 0x1fL) != 0) {


            if ((dirtyFlags & 0x13L) != 0) {

                    if (menuViewModel != null) {
                        // read menuViewModel.showMenu
                        menuViewModelShowMenu = menuViewModel.showMenu;
                    }
                    updateRegistration(1, menuViewModelShowMenu);


                    if (menuViewModelShowMenu != null) {
                        // read menuViewModel.showMenu.get()
                        menuViewModelShowMenuGet = menuViewModelShowMenu.get();
                    }


                    // read menuViewModel.showMenu.get() == true
                    menuViewModelShowMenuBooleanTrue = (menuViewModelShowMenuGet) == (true);
                if((dirtyFlags & 0x13L) != 0) {
                    if(menuViewModelShowMenuBooleanTrue) {
                            dirtyFlags |= 0x100L;
                    }
                    else {
                            dirtyFlags |= 0x80L;
                    }
                }


                    // read menuViewModel.showMenu.get() == true ? View.VISIBLE : View.GONE
                    menuViewModelShowMenuBooleanTrueViewVISIBLEViewGONE = ((menuViewModelShowMenuBooleanTrue) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
            if ((dirtyFlags & 0x11L) != 0) {

                    if (menuViewModel != null) {
                        // read menuViewModel.userData
                        menuViewModelUserData = menuViewModel.userData;
                    }


                    // read menuViewModel.userData != null
                    menuViewModelUserDataJavaLangObjectNull = (menuViewModelUserData) != (null);
                if((dirtyFlags & 0x11L) != 0) {
                    if(menuViewModelUserDataJavaLangObjectNull) {
                            dirtyFlags |= 0x40L;
                    }
                    else {
                            dirtyFlags |= 0x20L;
                    }
                }


                    // read menuViewModel.userData != null ? @android:string/logout : @android:string/login
                    menuViewModelUserDataJavaLangObjectNullTvLogoutAndroidStringLogoutTvLogoutAndroidStringLogin = ((menuViewModelUserDataJavaLangObjectNull) ? (tvLogout.getResources().getString(R.string.logout)) : (tvLogout.getResources().getString(R.string.login)));
            }
            if ((dirtyFlags & 0x19L) != 0) {

                    if (menuViewModel != null) {
                        // read menuViewModel.socialAdapter
                        menuViewModelSocialAdapter = menuViewModel.getSocialAdapter();
                    }
            }
            if ((dirtyFlags & 0x15L) != 0) {

                    if (menuViewModel != null) {
                        // read menuViewModel.menuServicesAdapter
                        menuViewModelMenuServicesAdapter = menuViewModel.getMenuServicesAdapter();
                    }
            }
        }
        // batch finished
        if ((dirtyFlags & 0x19L) != 0) {
            // api target 1

            grand.app.aber_user.base.ApplicationBinding.getItemsV2Binding(this.rcMenuSocial, menuViewModelSocialAdapter, "1", "2");
        }
        if ((dirtyFlags & 0x13L) != 0) {
            // api target 1

            this.rcServices.setVisibility(menuViewModelShowMenuBooleanTrueViewVISIBLEViewGONE);
        }
        if ((dirtyFlags & 0x15L) != 0) {
            // api target 1

            grand.app.aber_user.base.ApplicationBinding.getItemsV2Binding(this.rcServices, menuViewModelMenuServicesAdapter, "1", "1");
        }
        if ((dirtyFlags & 0x10L) != 0) {
            // api target 1

            this.tvAbout.setOnClickListener(mCallback192);
            this.tvAccount.setOnClickListener(mCallback190);
            this.tvCart.setOnClickListener(mCallback186);
            this.tvContact.setOnClickListener(mCallback194);
            this.tvCountry.setOnClickListener(mCallback200);
            this.tvFavorite.setOnClickListener(mCallback185);
            this.tvHome.setOnClickListener(mCallback184);
            this.tvLang.setOnClickListener(mCallback199);
            this.tvLogout.setOnClickListener(mCallback201);
            this.tvMyOrders.setOnClickListener(mCallback189);
            this.tvMyServices.setOnClickListener(mCallback188);
            this.tvProvider.setOnClickListener(mCallback191);
            this.tvRate.setOnClickListener(mCallback197);
            this.tvServices.setOnClickListener(mCallback187);
            this.tvShare.setOnClickListener(mCallback198);
            this.tvSocial.setOnClickListener(mCallback196);
            this.tvSuggest.setOnClickListener(mCallback195);
            this.tvTerms.setOnClickListener(mCallback193);
        }
        if ((dirtyFlags & 0x11L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvLogout, menuViewModelUserDataJavaLangObjectNullTvLogoutAndroidStringLogoutTvLogoutAndroidStringLogin);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        switch(sourceId) {
            case 18: {
                // localize variables for thread safety
                // menuViewModel
                grand.app.aber_user.customViews.views.MenuViewModel menuViewModel = mMenuViewModel;
                // menuViewModel.userData != null
                boolean menuViewModelUserDataJavaLangObjectNull = false;
                // menuViewModel != null
                boolean menuViewModelJavaLangObjectNull = false;
                // menuViewModel.userData
                grand.app.aber_user.pages.auth.models.UserData menuViewModelUserData = null;
                // menuViewModel.userData != null ? Constants.SHOW_LOGOUT_WARNING : Constants.LOGOUT
                java.lang.String menuViewModelUserDataJavaLangObjectNullConstantsSHOWLOGOUTWARNINGConstantsLOGOUT = null;



                menuViewModelJavaLangObjectNull = (menuViewModel) != (null);
                if (menuViewModelJavaLangObjectNull) {




                    menuViewModelUserData = menuViewModel.userData;


                    menuViewModelUserDataJavaLangObjectNull = (menuViewModelUserData) != (null);
                    if (menuViewModelUserDataJavaLangObjectNull) {




                        menuViewModelUserDataJavaLangObjectNullConstantsSHOWLOGOUTWARNINGConstantsLOGOUT = grand.app.aber_user.utils.Constants.SHOW_LOGOUT_WARNING;

                        menuViewModel.liveDataActions(menuViewModelUserDataJavaLangObjectNullConstantsSHOWLOGOUTWARNINGConstantsLOGOUT);
                    }
                    else {




                        menuViewModelUserDataJavaLangObjectNullConstantsSHOWLOGOUTWARNINGConstantsLOGOUT = grand.app.aber_user.utils.Constants.LOGOUT;

                        menuViewModel.liveDataActions(menuViewModelUserDataJavaLangObjectNullConstantsSHOWLOGOUTWARNINGConstantsLOGOUT);
                    }
                }
                break;
            }
            case 5: {
                // localize variables for thread safety
                // menuViewModel.userData != null ? Constants.SERVICES : Constants.LOGOUT
                java.lang.String menuViewModelUserDataJavaLangObjectNullConstantsSERVICESConstantsLOGOUT = null;
                // menuViewModel
                grand.app.aber_user.customViews.views.MenuViewModel menuViewModel = mMenuViewModel;
                // menuViewModel.userData != null
                boolean menuViewModelUserDataJavaLangObjectNull = false;
                // menuViewModel != null
                boolean menuViewModelJavaLangObjectNull = false;
                // menuViewModel.userData
                grand.app.aber_user.pages.auth.models.UserData menuViewModelUserData = null;



                menuViewModelJavaLangObjectNull = (menuViewModel) != (null);
                if (menuViewModelJavaLangObjectNull) {




                    menuViewModelUserData = menuViewModel.userData;


                    menuViewModelUserDataJavaLangObjectNull = (menuViewModelUserData) != (null);
                    if (menuViewModelUserDataJavaLangObjectNull) {




                        menuViewModelUserDataJavaLangObjectNullConstantsSERVICESConstantsLOGOUT = grand.app.aber_user.utils.Constants.SERVICES;

                        menuViewModel.liveDataActions(menuViewModelUserDataJavaLangObjectNullConstantsSERVICESConstantsLOGOUT);
                    }
                    else {




                        menuViewModelUserDataJavaLangObjectNullConstantsSERVICESConstantsLOGOUT = grand.app.aber_user.utils.Constants.LOGOUT;

                        menuViewModel.liveDataActions(menuViewModelUserDataJavaLangObjectNullConstantsSERVICESConstantsLOGOUT);
                    }
                }
                break;
            }
            case 1: {
                // localize variables for thread safety
                // menuViewModel
                grand.app.aber_user.customViews.views.MenuViewModel menuViewModel = mMenuViewModel;
                // menuViewModel != null
                boolean menuViewModelJavaLangObjectNull = false;



                menuViewModelJavaLangObjectNull = (menuViewModel) != (null);
                if (menuViewModelJavaLangObjectNull) {




                    menuViewModel.liveDataActions(grand.app.aber_user.utils.Constants.MENU_HOME);
                }
                break;
            }
            case 13: {
                // localize variables for thread safety
                // menuViewModel
                grand.app.aber_user.customViews.views.MenuViewModel menuViewModel = mMenuViewModel;
                // menuViewModel != null
                boolean menuViewModelJavaLangObjectNull = false;



                menuViewModelJavaLangObjectNull = (menuViewModel) != (null);
                if (menuViewModelJavaLangObjectNull) {




                    menuViewModel.liveDataActions(grand.app.aber_user.utils.Constants.SOCIAL);
                }
                break;
            }
            case 9: {
                // localize variables for thread safety
                // menuViewModel
                grand.app.aber_user.customViews.views.MenuViewModel menuViewModel = mMenuViewModel;
                // menuViewModel != null
                boolean menuViewModelJavaLangObjectNull = false;



                menuViewModelJavaLangObjectNull = (menuViewModel) != (null);
                if (menuViewModelJavaLangObjectNull) {




                    menuViewModel.liveDataActions(grand.app.aber_user.utils.Constants.ABOUT);
                }
                break;
            }
            case 6: {
                // localize variables for thread safety
                // menuViewModel
                grand.app.aber_user.customViews.views.MenuViewModel menuViewModel = mMenuViewModel;
                // menuViewModel.userData != null
                boolean menuViewModelUserDataJavaLangObjectNull = false;
                // menuViewModel != null
                boolean menuViewModelJavaLangObjectNull = false;
                // menuViewModel.userData
                grand.app.aber_user.pages.auth.models.UserData menuViewModelUserData = null;
                // menuViewModel.userData != null ? Constants.My_ORDERS : Constants.LOGOUT
                java.lang.String menuViewModelUserDataJavaLangObjectNullConstantsMyORDERSConstantsLOGOUT = null;



                menuViewModelJavaLangObjectNull = (menuViewModel) != (null);
                if (menuViewModelJavaLangObjectNull) {




                    menuViewModelUserData = menuViewModel.userData;


                    menuViewModelUserDataJavaLangObjectNull = (menuViewModelUserData) != (null);
                    if (menuViewModelUserDataJavaLangObjectNull) {




                        menuViewModelUserDataJavaLangObjectNullConstantsMyORDERSConstantsLOGOUT = grand.app.aber_user.utils.Constants.My_ORDERS;

                        menuViewModel.liveDataActions(menuViewModelUserDataJavaLangObjectNullConstantsMyORDERSConstantsLOGOUT);
                    }
                    else {




                        menuViewModelUserDataJavaLangObjectNullConstantsMyORDERSConstantsLOGOUT = grand.app.aber_user.utils.Constants.LOGOUT;

                        menuViewModel.liveDataActions(menuViewModelUserDataJavaLangObjectNullConstantsMyORDERSConstantsLOGOUT);
                    }
                }
                break;
            }
            case 14: {
                // localize variables for thread safety
                // menuViewModel
                grand.app.aber_user.customViews.views.MenuViewModel menuViewModel = mMenuViewModel;
                // menuViewModel != null
                boolean menuViewModelJavaLangObjectNull = false;



                menuViewModelJavaLangObjectNull = (menuViewModel) != (null);
                if (menuViewModelJavaLangObjectNull) {




                    menuViewModel.liveDataActions(grand.app.aber_user.utils.Constants.RATE_APP);
                }
                break;
            }
            case 2: {
                // localize variables for thread safety
                // menuViewModel
                grand.app.aber_user.customViews.views.MenuViewModel menuViewModel = mMenuViewModel;
                // menuViewModel.userData != null
                boolean menuViewModelUserDataJavaLangObjectNull = false;
                // menuViewModel.userData != null ? Constants.FAVORITE : Constants.LOGOUT
                java.lang.String menuViewModelUserDataJavaLangObjectNullConstantsFAVORITEConstantsLOGOUT = null;
                // menuViewModel != null
                boolean menuViewModelJavaLangObjectNull = false;
                // menuViewModel.userData
                grand.app.aber_user.pages.auth.models.UserData menuViewModelUserData = null;



                menuViewModelJavaLangObjectNull = (menuViewModel) != (null);
                if (menuViewModelJavaLangObjectNull) {




                    menuViewModelUserData = menuViewModel.userData;


                    menuViewModelUserDataJavaLangObjectNull = (menuViewModelUserData) != (null);
                    if (menuViewModelUserDataJavaLangObjectNull) {




                        menuViewModelUserDataJavaLangObjectNullConstantsFAVORITEConstantsLOGOUT = grand.app.aber_user.utils.Constants.FAVORITE;

                        menuViewModel.liveDataActions(menuViewModelUserDataJavaLangObjectNullConstantsFAVORITEConstantsLOGOUT);
                    }
                    else {




                        menuViewModelUserDataJavaLangObjectNullConstantsFAVORITEConstantsLOGOUT = grand.app.aber_user.utils.Constants.LOGOUT;

                        menuViewModel.liveDataActions(menuViewModelUserDataJavaLangObjectNullConstantsFAVORITEConstantsLOGOUT);
                    }
                }
                break;
            }
            case 10: {
                // localize variables for thread safety
                // menuViewModel
                grand.app.aber_user.customViews.views.MenuViewModel menuViewModel = mMenuViewModel;
                // menuViewModel != null
                boolean menuViewModelJavaLangObjectNull = false;



                menuViewModelJavaLangObjectNull = (menuViewModel) != (null);
                if (menuViewModelJavaLangObjectNull) {




                    menuViewModel.liveDataActions(grand.app.aber_user.utils.Constants.TERMS);
                }
                break;
            }
            case 3: {
                // localize variables for thread safety
                // menuViewModel.userData != null ? Constants.CART : Constants.LOGOUT
                java.lang.String menuViewModelUserDataJavaLangObjectNullConstantsCARTConstantsLOGOUT = null;
                // menuViewModel
                grand.app.aber_user.customViews.views.MenuViewModel menuViewModel = mMenuViewModel;
                // menuViewModel.userData != null
                boolean menuViewModelUserDataJavaLangObjectNull = false;
                // menuViewModel != null
                boolean menuViewModelJavaLangObjectNull = false;
                // menuViewModel.userData
                grand.app.aber_user.pages.auth.models.UserData menuViewModelUserData = null;



                menuViewModelJavaLangObjectNull = (menuViewModel) != (null);
                if (menuViewModelJavaLangObjectNull) {




                    menuViewModelUserData = menuViewModel.userData;


                    menuViewModelUserDataJavaLangObjectNull = (menuViewModelUserData) != (null);
                    if (menuViewModelUserDataJavaLangObjectNull) {




                        menuViewModelUserDataJavaLangObjectNullConstantsCARTConstantsLOGOUT = grand.app.aber_user.utils.Constants.CART;

                        menuViewModel.liveDataActions(menuViewModelUserDataJavaLangObjectNullConstantsCARTConstantsLOGOUT);
                    }
                    else {




                        menuViewModelUserDataJavaLangObjectNullConstantsCARTConstantsLOGOUT = grand.app.aber_user.utils.Constants.LOGOUT;

                        menuViewModel.liveDataActions(menuViewModelUserDataJavaLangObjectNullConstantsCARTConstantsLOGOUT);
                    }
                }
                break;
            }
            case 15: {
                // localize variables for thread safety
                // menuViewModel
                grand.app.aber_user.customViews.views.MenuViewModel menuViewModel = mMenuViewModel;
                // menuViewModel != null
                boolean menuViewModelJavaLangObjectNull = false;



                menuViewModelJavaLangObjectNull = (menuViewModel) != (null);
                if (menuViewModelJavaLangObjectNull) {




                    menuViewModel.liveDataActions(grand.app.aber_user.utils.Constants.SHARE_BAR);
                }
                break;
            }
            case 11: {
                // localize variables for thread safety
                // menuViewModel
                grand.app.aber_user.customViews.views.MenuViewModel menuViewModel = mMenuViewModel;
                // menuViewModel != null
                boolean menuViewModelJavaLangObjectNull = false;



                menuViewModelJavaLangObjectNull = (menuViewModel) != (null);
                if (menuViewModelJavaLangObjectNull) {




                    menuViewModel.liveDataActions(grand.app.aber_user.utils.Constants.CONTACT);
                }
                break;
            }
            case 16: {
                // localize variables for thread safety
                // menuViewModel
                grand.app.aber_user.customViews.views.MenuViewModel menuViewModel = mMenuViewModel;
                // menuViewModel != null
                boolean menuViewModelJavaLangObjectNull = false;



                menuViewModelJavaLangObjectNull = (menuViewModel) != (null);
                if (menuViewModelJavaLangObjectNull) {




                    menuViewModel.liveDataActions(grand.app.aber_user.utils.Constants.LANGUAGE);
                }
                break;
            }
            case 17: {
                // localize variables for thread safety
                // menuViewModel
                grand.app.aber_user.customViews.views.MenuViewModel menuViewModel = mMenuViewModel;
                // menuViewModel != null
                boolean menuViewModelJavaLangObjectNull = false;



                menuViewModelJavaLangObjectNull = (menuViewModel) != (null);
                if (menuViewModelJavaLangObjectNull) {




                    menuViewModel.liveDataActions(grand.app.aber_user.utils.Constants.COUNTRIES);
                }
                break;
            }
            case 4: {
                // localize variables for thread safety
                // menuViewModel
                grand.app.aber_user.customViews.views.MenuViewModel menuViewModel = mMenuViewModel;
                // menuViewModel != null
                boolean menuViewModelJavaLangObjectNull = false;



                menuViewModelJavaLangObjectNull = (menuViewModel) != (null);
                if (menuViewModelJavaLangObjectNull) {


                    menuViewModel.showCategories();
                }
                break;
            }
            case 12: {
                // localize variables for thread safety
                // menuViewModel
                grand.app.aber_user.customViews.views.MenuViewModel menuViewModel = mMenuViewModel;
                // menuViewModel != null
                boolean menuViewModelJavaLangObjectNull = false;



                menuViewModelJavaLangObjectNull = (menuViewModel) != (null);
                if (menuViewModelJavaLangObjectNull) {




                    menuViewModel.liveDataActions(grand.app.aber_user.utils.Constants.SUGGESTION);
                }
                break;
            }
            case 8: {
                // localize variables for thread safety
                // menuViewModel
                grand.app.aber_user.customViews.views.MenuViewModel menuViewModel = mMenuViewModel;
                // menuViewModel != null
                boolean menuViewModelJavaLangObjectNull = false;



                menuViewModelJavaLangObjectNull = (menuViewModel) != (null);
                if (menuViewModelJavaLangObjectNull) {




                    menuViewModel.liveDataActions(grand.app.aber_user.utils.Constants.PROVIDER);
                }
                break;
            }
            case 7: {
                // localize variables for thread safety
                // menuViewModel
                grand.app.aber_user.customViews.views.MenuViewModel menuViewModel = mMenuViewModel;
                // menuViewModel.userData != null
                boolean menuViewModelUserDataJavaLangObjectNull = false;
                // menuViewModel != null
                boolean menuViewModelJavaLangObjectNull = false;
                // menuViewModel.userData
                grand.app.aber_user.pages.auth.models.UserData menuViewModelUserData = null;
                // menuViewModel.userData != null ? Constants.MENU_ACCOUNT : Constants.LOGOUT
                java.lang.String menuViewModelUserDataJavaLangObjectNullConstantsMENUACCOUNTConstantsLOGOUT = null;



                menuViewModelJavaLangObjectNull = (menuViewModel) != (null);
                if (menuViewModelJavaLangObjectNull) {




                    menuViewModelUserData = menuViewModel.userData;


                    menuViewModelUserDataJavaLangObjectNull = (menuViewModelUserData) != (null);
                    if (menuViewModelUserDataJavaLangObjectNull) {




                        menuViewModelUserDataJavaLangObjectNullConstantsMENUACCOUNTConstantsLOGOUT = grand.app.aber_user.utils.Constants.MENU_ACCOUNT;

                        menuViewModel.liveDataActions(menuViewModelUserDataJavaLangObjectNullConstantsMENUACCOUNTConstantsLOGOUT);
                    }
                    else {




                        menuViewModelUserDataJavaLangObjectNullConstantsMENUACCOUNTConstantsLOGOUT = grand.app.aber_user.utils.Constants.LOGOUT;

                        menuViewModel.liveDataActions(menuViewModelUserDataJavaLangObjectNullConstantsMENUACCOUNTConstantsLOGOUT);
                    }
                }
                break;
            }
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): menuViewModel
        flag 1 (0x2L): menuViewModel.showMenu
        flag 2 (0x3L): menuViewModel.menuServicesAdapter
        flag 3 (0x4L): menuViewModel.socialAdapter
        flag 4 (0x5L): null
        flag 5 (0x6L): menuViewModel.userData != null ? @android:string/logout : @android:string/login
        flag 6 (0x7L): menuViewModel.userData != null ? @android:string/logout : @android:string/login
        flag 7 (0x8L): menuViewModel.showMenu.get() == true ? View.VISIBLE : View.GONE
        flag 8 (0x9L): menuViewModel.showMenu.get() == true ? View.VISIBLE : View.GONE
    flag mapping end*/
    //end
}