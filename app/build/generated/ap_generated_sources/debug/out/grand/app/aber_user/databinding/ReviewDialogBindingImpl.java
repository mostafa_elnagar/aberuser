package grand.app.aber_user.databinding;
import grand.app.aber_user.R;
import grand.app.aber_user.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ReviewDialogBindingImpl extends ReviewDialogBinding implements grand.app.aber_user.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.dialog_header, 5);
        sViewsWithIds.put(R.id.review_dialog_comment, 6);
    }
    // views
    @NonNull
    private final androidx.cardview.widget.CardView mboundView0;
    @NonNull
    private final com.google.android.material.textfield.TextInputEditText mboundView3;
    @NonNull
    private final androidx.appcompat.widget.AppCompatButton mboundView4;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback234;
    @Nullable
    private final android.view.View.OnClickListener mCallback233;
    // values
    // listeners
    private OnRatingBarChangeListenerImpl mViewModelOnRateChangeAndroidWidgetRatingBarOnRatingBarChangeListener;
    // Inverse Binding Event Handlers
    private androidx.databinding.InverseBindingListener mboundView3androidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.rateRequest.comment
            //         is viewModel.rateRequest.setComment((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(mboundView3);
            // localize variables for thread safety
            // viewModel.rateRequest
            grand.app.aber_user.pages.reviews.models.RateRequest viewModelRateRequest = null;
            // viewModel
            grand.app.aber_user.pages.reviews.viewModels.ReviewsViewModel viewModel = mViewModel;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;
            // viewModel.rateRequest.comment
            java.lang.String viewModelRateRequestComment = null;
            // viewModel.rateRequest != null
            boolean viewModelRateRequestJavaLangObjectNull = false;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelRateRequest = viewModel.getRateRequest();

                viewModelRateRequestJavaLangObjectNull = (viewModelRateRequest) != (null);
                if (viewModelRateRequestJavaLangObjectNull) {




                    viewModelRateRequest.setComment(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };

    public ReviewDialogBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 7, sIncludes, sViewsWithIds));
    }
    private ReviewDialogBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 1
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[5]
            , (androidx.appcompat.widget.AppCompatRatingBar) bindings[2]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[1]
            , (androidx.cardview.widget.CardView) bindings[6]
            );
        this.dialogReviewRate.setTag(null);
        this.icClosePage.setTag(null);
        this.mboundView0 = (androidx.cardview.widget.CardView) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView3 = (com.google.android.material.textfield.TextInputEditText) bindings[3];
        this.mboundView3.setTag(null);
        this.mboundView4 = (androidx.appcompat.widget.AppCompatButton) bindings[4];
        this.mboundView4.setTag(null);
        setRootTag(root);
        // listeners
        mCallback234 = new grand.app.aber_user.generated.callback.OnClickListener(this, 2);
        mCallback233 = new grand.app.aber_user.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.viewModel == variableId) {
            setViewModel((grand.app.aber_user.pages.reviews.viewModels.ReviewsViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setViewModel(@Nullable grand.app.aber_user.pages.reviews.viewModels.ReviewsViewModel ViewModel) {
        updateRegistration(0, ViewModel);
        this.mViewModel = ViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.viewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeViewModel((grand.app.aber_user.pages.reviews.viewModels.ReviewsViewModel) object, fieldId);
        }
        return false;
    }
    private boolean onChangeViewModel(grand.app.aber_user.pages.reviews.viewModels.ReviewsViewModel ViewModel, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String viewModelRateRequestComment = null;
        grand.app.aber_user.pages.reviews.models.RateRequest viewModelRateRequest = null;
        grand.app.aber_user.pages.reviews.viewModels.ReviewsViewModel viewModel = mViewModel;
        android.widget.RatingBar.OnRatingBarChangeListener viewModelOnRateChangeAndroidWidgetRatingBarOnRatingBarChangeListener = null;

        if ((dirtyFlags & 0x3L) != 0) {



                if (viewModel != null) {
                    // read viewModel.rateRequest
                    viewModelRateRequest = viewModel.getRateRequest();
                    // read viewModel::onRateChange
                    viewModelOnRateChangeAndroidWidgetRatingBarOnRatingBarChangeListener = (((mViewModelOnRateChangeAndroidWidgetRatingBarOnRatingBarChangeListener == null) ? (mViewModelOnRateChangeAndroidWidgetRatingBarOnRatingBarChangeListener = new OnRatingBarChangeListenerImpl()) : mViewModelOnRateChangeAndroidWidgetRatingBarOnRatingBarChangeListener).setValue(viewModel));
                }


                if (viewModelRateRequest != null) {
                    // read viewModel.rateRequest.comment
                    viewModelRateRequestComment = viewModelRateRequest.getComment();
                }
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            androidx.databinding.adapters.RatingBarBindingAdapter.setListeners(this.dialogReviewRate, (android.widget.RatingBar.OnRatingBarChangeListener)viewModelOnRateChangeAndroidWidgetRatingBarOnRatingBarChangeListener, (androidx.databinding.InverseBindingListener)null);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView3, viewModelRateRequestComment);
        }
        if ((dirtyFlags & 0x2L) != 0) {
            // api target 1

            this.icClosePage.setOnClickListener(mCallback233);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.mboundView3, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, mboundView3androidTextAttrChanged);
            this.mboundView4.setOnClickListener(mCallback234);
        }
    }
    // Listener Stub Implementations
    public static class OnRatingBarChangeListenerImpl implements android.widget.RatingBar.OnRatingBarChangeListener{
        private grand.app.aber_user.pages.reviews.viewModels.ReviewsViewModel value;
        public OnRatingBarChangeListenerImpl setValue(grand.app.aber_user.pages.reviews.viewModels.ReviewsViewModel value) {
            this.value = value;
            return value == null ? null : this;
        }
        @Override
        public void onRatingChanged(android.widget.RatingBar arg0, float arg1, boolean arg2) {
            this.value.onRateChange(arg0, arg1, arg2); 
        }
    }
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        switch(sourceId) {
            case 2: {
                // localize variables for thread safety
                // viewModel
                grand.app.aber_user.pages.reviews.viewModels.ReviewsViewModel viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {


                    viewModel.sendRate();
                }
                break;
            }
            case 1: {
                // localize variables for thread safety
                // viewModel
                grand.app.aber_user.pages.reviews.viewModels.ReviewsViewModel viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {


                    viewModel.close();
                }
                break;
            }
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): viewModel
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}