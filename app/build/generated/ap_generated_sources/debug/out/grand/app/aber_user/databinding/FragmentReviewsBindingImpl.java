package grand.app.aber_user.databinding;
import grand.app.aber_user.R;
import grand.app.aber_user.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentReviewsBindingImpl extends FragmentReviewsBinding implements grand.app.aber_user.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = null;
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    @NonNull
    private final com.google.android.material.floatingactionbutton.FloatingActionButton mboundView4;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback91;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentReviewsBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 5, sIncludes, sViewsWithIds));
    }
    private FragmentReviewsBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 2
            , (com.airbnb.lottie.LottieAnimationView) bindings[2]
            , (com.google.android.material.progressindicator.CircularProgressIndicator) bindings[3]
            , (androidx.recyclerview.widget.RecyclerView) bindings[1]
            );
        this.animationView.setTag(null);
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView4 = (com.google.android.material.floatingactionbutton.FloatingActionButton) bindings[4];
        this.mboundView4.setTag(null);
        this.progress.setTag(null);
        this.rcReviews.setTag(null);
        setRootTag(root);
        // listeners
        mCallback91 = new grand.app.aber_user.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x10L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.viewmodel == variableId) {
            setViewmodel((grand.app.aber_user.pages.reviews.viewModels.ReviewsViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setViewmodel(@Nullable grand.app.aber_user.pages.reviews.viewModels.ReviewsViewModel Viewmodel) {
        updateRegistration(1, Viewmodel);
        this.mViewmodel = Viewmodel;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.viewmodel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeViewmodelSearchProgressVisible((androidx.databinding.ObservableBoolean) object, fieldId);
            case 1 :
                return onChangeViewmodel((grand.app.aber_user.pages.reviews.viewModels.ReviewsViewModel) object, fieldId);
        }
        return false;
    }
    private boolean onChangeViewmodelSearchProgressVisible(androidx.databinding.ObservableBoolean ViewmodelSearchProgressVisible, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewmodel(grand.app.aber_user.pages.reviews.viewModels.ReviewsViewModel Viewmodel, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        else if (fieldId == BR.reviewsAdapter) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        else if (fieldId == BR.reviewMainData) {
            synchronized(this) {
                    mDirtyFlags |= 0x8L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        grand.app.aber_user.pages.reviews.adapters.ClientReviewsAdapter viewmodelReviewsAdapter = null;
        boolean viewmodelReviewMainDataRatesSizeInt0 = false;
        grand.app.aber_user.pages.reviews.models.ReviewMainData viewmodelReviewMainData = null;
        int viewmodelReviewMainDataRatesSizeInt0ViewGONEViewVISIBLE = 0;
        boolean viewmodelSearchProgressVisibleGet = false;
        androidx.databinding.ObservableBoolean viewmodelSearchProgressVisible = null;
        java.util.List<grand.app.aber_user.pages.reviews.models.RatesItem> viewmodelReviewMainDataRates = null;
        int viewmodelSearchProgressVisibleViewVISIBLEViewGONE = 0;
        int viewmodelReviewMainDataRatesSize = 0;
        grand.app.aber_user.pages.reviews.viewModels.ReviewsViewModel viewmodel = mViewmodel;

        if ((dirtyFlags & 0x1fL) != 0) {


            if ((dirtyFlags & 0x16L) != 0) {

                    if (viewmodel != null) {
                        // read viewmodel.reviewsAdapter
                        viewmodelReviewsAdapter = viewmodel.getReviewsAdapter();
                    }
            }
            if ((dirtyFlags & 0x1aL) != 0) {

                    if (viewmodel != null) {
                        // read viewmodel.reviewMainData
                        viewmodelReviewMainData = viewmodel.getReviewMainData();
                    }


                    if (viewmodelReviewMainData != null) {
                        // read viewmodel.reviewMainData.rates
                        viewmodelReviewMainDataRates = viewmodelReviewMainData.getRates();
                    }


                    if (viewmodelReviewMainDataRates != null) {
                        // read viewmodel.reviewMainData.rates.size()
                        viewmodelReviewMainDataRatesSize = viewmodelReviewMainDataRates.size();
                    }


                    // read viewmodel.reviewMainData.rates.size() > 0
                    viewmodelReviewMainDataRatesSizeInt0 = (viewmodelReviewMainDataRatesSize) > (0);
                if((dirtyFlags & 0x1aL) != 0) {
                    if(viewmodelReviewMainDataRatesSizeInt0) {
                            dirtyFlags |= 0x40L;
                    }
                    else {
                            dirtyFlags |= 0x20L;
                    }
                }


                    // read viewmodel.reviewMainData.rates.size() > 0 ? View.GONE : View.VISIBLE
                    viewmodelReviewMainDataRatesSizeInt0ViewGONEViewVISIBLE = ((viewmodelReviewMainDataRatesSizeInt0) ? (android.view.View.GONE) : (android.view.View.VISIBLE));
            }
            if ((dirtyFlags & 0x13L) != 0) {

                    if (viewmodel != null) {
                        // read viewmodel.searchProgressVisible
                        viewmodelSearchProgressVisible = viewmodel.searchProgressVisible;
                    }
                    updateRegistration(0, viewmodelSearchProgressVisible);


                    if (viewmodelSearchProgressVisible != null) {
                        // read viewmodel.searchProgressVisible.get()
                        viewmodelSearchProgressVisibleGet = viewmodelSearchProgressVisible.get();
                    }
                if((dirtyFlags & 0x13L) != 0) {
                    if(viewmodelSearchProgressVisibleGet) {
                            dirtyFlags |= 0x100L;
                    }
                    else {
                            dirtyFlags |= 0x80L;
                    }
                }


                    // read viewmodel.searchProgressVisible.get() ? View.VISIBLE : View.GONE
                    viewmodelSearchProgressVisibleViewVISIBLEViewGONE = ((viewmodelSearchProgressVisibleGet) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
        }
        // batch finished
        if ((dirtyFlags & 0x1aL) != 0) {
            // api target 1

            this.animationView.setVisibility(viewmodelReviewMainDataRatesSizeInt0ViewGONEViewVISIBLE);
        }
        if ((dirtyFlags & 0x10L) != 0) {
            // api target 1

            this.mboundView4.setOnClickListener(mCallback91);
        }
        if ((dirtyFlags & 0x13L) != 0) {
            // api target 1

            this.progress.setVisibility(viewmodelSearchProgressVisibleViewVISIBLEViewGONE);
        }
        if ((dirtyFlags & 0x16L) != 0) {
            // api target 1

            grand.app.aber_user.base.ApplicationBinding.getItemsV2Binding(this.rcReviews, viewmodelReviewsAdapter, "1", "1");
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        // localize variables for thread safety
        // viewmodel != null
        boolean viewmodelJavaLangObjectNull = false;
        // viewmodel
        grand.app.aber_user.pages.reviews.viewModels.ReviewsViewModel viewmodel = mViewmodel;



        viewmodelJavaLangObjectNull = (viewmodel) != (null);
        if (viewmodelJavaLangObjectNull) {


            viewmodel.toRate();
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): viewmodel.searchProgressVisible
        flag 1 (0x2L): viewmodel
        flag 2 (0x3L): viewmodel.reviewsAdapter
        flag 3 (0x4L): viewmodel.reviewMainData
        flag 4 (0x5L): null
        flag 5 (0x6L): viewmodel.reviewMainData.rates.size() > 0 ? View.GONE : View.VISIBLE
        flag 6 (0x7L): viewmodel.reviewMainData.rates.size() > 0 ? View.GONE : View.VISIBLE
        flag 7 (0x8L): viewmodel.searchProgressVisible.get() ? View.VISIBLE : View.GONE
        flag 8 (0x9L): viewmodel.searchProgressVisible.get() ? View.VISIBLE : View.GONE
    flag mapping end*/
    //end
}