package grand.app.aber_user.databinding;
import grand.app.aber_user.R;
import grand.app.aber_user.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ItemReviewBindingImpl extends ItemReviewBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = null;
    }
    // views
    @NonNull
    private final androidx.cardview.widget.CardView mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ItemReviewBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 5, sIncludes, sViewsWithIds));
    }
    private ItemReviewBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 1
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[4]
            , (androidx.appcompat.widget.AppCompatRatingBar) bindings[2]
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[1]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[3]
            );
        this.mboundView0 = (androidx.cardview.widget.CardView) bindings[0];
        this.mboundView0.setTag(null);
        this.reviewItemDec.setTag(null);
        this.userItemCode.setTag(null);
        this.userItemName.setTag(null);
        this.userItemTime.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.itemViewModel == variableId) {
            setItemViewModel((grand.app.aber_user.pages.reviews.itemViewModels.ItemClientReviewsViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setItemViewModel(@Nullable grand.app.aber_user.pages.reviews.itemViewModels.ItemClientReviewsViewModel ItemViewModel) {
        updateRegistration(0, ItemViewModel);
        this.mItemViewModel = ItemViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.itemViewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeItemViewModel((grand.app.aber_user.pages.reviews.itemViewModels.ItemClientReviewsViewModel) object, fieldId);
        }
        return false;
    }
    private boolean onChangeItemViewModel(grand.app.aber_user.pages.reviews.itemViewModels.ItemClientReviewsViewModel ItemViewModel, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        else if (fieldId == BR.ratesItem) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        grand.app.aber_user.pages.reviews.itemViewModels.ItemClientReviewsViewModel itemViewModel = mItemViewModel;
        java.lang.String itemViewModelRatesItemRate = null;
        java.lang.String itemViewModelRatesItemCreatedAt = null;
        java.lang.String itemViewModelRatesItemComment = null;
        grand.app.aber_user.pages.reviews.models.RatesItem itemViewModelRatesItem = null;
        java.lang.String itemViewModelRatesItemName = null;

        if ((dirtyFlags & 0x7L) != 0) {



                if (itemViewModel != null) {
                    // read itemViewModel.ratesItem
                    itemViewModelRatesItem = itemViewModel.getRatesItem();
                }


                if (itemViewModelRatesItem != null) {
                    // read itemViewModel.ratesItem.rate
                    itemViewModelRatesItemRate = itemViewModelRatesItem.getRate();
                    // read itemViewModel.ratesItem.createdAt
                    itemViewModelRatesItemCreatedAt = itemViewModelRatesItem.getCreatedAt();
                    // read itemViewModel.ratesItem.comment
                    itemViewModelRatesItemComment = itemViewModelRatesItem.getComment();
                    // read itemViewModel.ratesItem.name
                    itemViewModelRatesItemName = itemViewModelRatesItem.getName();
                }
        }
        // batch finished
        if ((dirtyFlags & 0x7L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.reviewItemDec, itemViewModelRatesItemComment);
            grand.app.aber_user.base.ApplicationBinding.setRate(this.userItemCode, itemViewModelRatesItemRate);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.userItemName, itemViewModelRatesItemName);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.userItemTime, itemViewModelRatesItemCreatedAt);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): itemViewModel
        flag 1 (0x2L): itemViewModel.ratesItem
        flag 2 (0x3L): null
    flag mapping end*/
    //end
}