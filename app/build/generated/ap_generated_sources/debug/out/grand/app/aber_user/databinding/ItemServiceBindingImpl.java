package grand.app.aber_user.databinding;
import grand.app.aber_user.R;
import grand.app.aber_user.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ItemServiceBindingImpl extends ItemServiceBinding implements grand.app.aber_user.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = null;
    }
    // views
    @NonNull
    private final android.widget.RelativeLayout mboundView0;
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView1;
    @NonNull
    private final grand.app.aber_user.customViews.views.CustomTextViewRegular mboundView4;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback122;
    @Nullable
    private final android.view.View.OnClickListener mCallback121;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ItemServiceBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 5, sIncludes, sViewsWithIds));
    }
    private ItemServiceBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 1
            , (com.google.android.material.checkbox.MaterialCheckBox) bindings[2]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[3]
            );
        this.checkbox.setTag(null);
        this.itemImg.setTag(null);
        this.mboundView0 = (android.widget.RelativeLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[1];
        this.mboundView1.setTag(null);
        this.mboundView4 = (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[4];
        this.mboundView4.setTag(null);
        setRootTag(root);
        // listeners
        mCallback122 = new grand.app.aber_user.generated.callback.OnClickListener(this, 2);
        mCallback121 = new grand.app.aber_user.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.itemViewModel == variableId) {
            setItemViewModel((grand.app.aber_user.pages.services.viewModels.ItemServicesViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setItemViewModel(@Nullable grand.app.aber_user.pages.services.viewModels.ItemServicesViewModel ItemViewModel) {
        updateRegistration(0, ItemViewModel);
        this.mItemViewModel = ItemViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.itemViewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeItemViewModel((grand.app.aber_user.pages.services.viewModels.ItemServicesViewModel) object, fieldId);
        }
        return false;
    }
    private boolean onChangeItemViewModel(grand.app.aber_user.pages.services.viewModels.ItemServicesViewModel ItemViewModel, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        else if (fieldId == BR.servicesItem) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        grand.app.aber_user.pages.services.viewModels.ItemServicesViewModel itemViewModel = mItemViewModel;
        android.graphics.drawable.Drawable itemViewModelServicesItemCheckedMboundView0AndroidDrawableCornerViewPrimaryBorderMboundView0AndroidDrawableCornerViewGrayBorde = null;
        java.lang.String itemViewModelServicesItemPriceConcatJavaLangStringConcatItemViewModelCurrency = null;
        java.lang.String itemViewModelServicesItemPriceConcatJavaLangString = null;
        android.graphics.drawable.Drawable itemViewModelServicesItemCheckedMboundView1AndroidDrawableCornerViewPrimaryBorderMboundView1AndroidDrawableCornerViewGrayBorde = null;
        java.lang.String itemViewModelCurrency = null;
        java.lang.String itemViewModelServicesItemPrice = null;
        boolean itemViewModelServicesItemChecked = false;
        java.lang.String itemViewModelServicesItemName = null;
        grand.app.aber_user.pages.services.models.ServicesItem itemViewModelServicesItem = null;
        java.lang.String itemViewModelServicesItemImage = null;

        if ((dirtyFlags & 0x7L) != 0) {



                if (itemViewModel != null) {
                    // read itemViewModel.currency
                    itemViewModelCurrency = itemViewModel.currency;
                    // read itemViewModel.servicesItem
                    itemViewModelServicesItem = itemViewModel.getServicesItem();
                }


                if (itemViewModelServicesItem != null) {
                    // read itemViewModel.servicesItem.price
                    itemViewModelServicesItemPrice = itemViewModelServicesItem.getPrice();
                    // read itemViewModel.servicesItem.checked
                    itemViewModelServicesItemChecked = itemViewModelServicesItem.isChecked();
                    // read itemViewModel.servicesItem.name
                    itemViewModelServicesItemName = itemViewModelServicesItem.getName();
                    // read itemViewModel.servicesItem.image
                    itemViewModelServicesItemImage = itemViewModelServicesItem.getImage();
                }
            if((dirtyFlags & 0x7L) != 0) {
                if(itemViewModelServicesItemChecked) {
                        dirtyFlags |= 0x10L;
                        dirtyFlags |= 0x40L;
                }
                else {
                        dirtyFlags |= 0x8L;
                        dirtyFlags |= 0x20L;
                }
            }


                if (itemViewModelServicesItemPrice != null) {
                    // read itemViewModel.servicesItem.price.concat(" ")
                    itemViewModelServicesItemPriceConcatJavaLangString = itemViewModelServicesItemPrice.concat(" ");
                }
                // read itemViewModel.servicesItem.checked ? @android:drawable/corner_view_primary_border : @android:drawable/corner_view_gray_borde
                itemViewModelServicesItemCheckedMboundView0AndroidDrawableCornerViewPrimaryBorderMboundView0AndroidDrawableCornerViewGrayBorde = ((itemViewModelServicesItemChecked) ? (androidx.appcompat.content.res.AppCompatResources.getDrawable(mboundView0.getContext(), R.drawable.corner_view_primary_border)) : (androidx.appcompat.content.res.AppCompatResources.getDrawable(mboundView0.getContext(), R.drawable.corner_view_gray_borde)));
                // read itemViewModel.servicesItem.checked ? @android:drawable/corner_view_primary_border : @android:drawable/corner_view_gray_borde
                itemViewModelServicesItemCheckedMboundView1AndroidDrawableCornerViewPrimaryBorderMboundView1AndroidDrawableCornerViewGrayBorde = ((itemViewModelServicesItemChecked) ? (androidx.appcompat.content.res.AppCompatResources.getDrawable(mboundView1.getContext(), R.drawable.corner_view_primary_border)) : (androidx.appcompat.content.res.AppCompatResources.getDrawable(mboundView1.getContext(), R.drawable.corner_view_gray_borde)));


                if (itemViewModelServicesItemPriceConcatJavaLangString != null) {
                    // read itemViewModel.servicesItem.price.concat(" ").concat(itemViewModel.currency)
                    itemViewModelServicesItemPriceConcatJavaLangStringConcatItemViewModelCurrency = itemViewModelServicesItemPriceConcatJavaLangString.concat(itemViewModelCurrency);
                }
        }
        // batch finished
        if ((dirtyFlags & 0x7L) != 0) {
            // api target 1

            androidx.databinding.adapters.CompoundButtonBindingAdapter.setChecked(this.checkbox, itemViewModelServicesItemChecked);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.checkbox, itemViewModelServicesItemPriceConcatJavaLangStringConcatItemViewModelCurrency);
            grand.app.aber_user.base.ApplicationBinding.loadMarketImage(this.itemImg, itemViewModelServicesItemImage);
            androidx.databinding.adapters.ViewBindingAdapter.setBackground(this.mboundView0, itemViewModelServicesItemCheckedMboundView0AndroidDrawableCornerViewPrimaryBorderMboundView0AndroidDrawableCornerViewGrayBorde);
            androidx.databinding.adapters.ViewBindingAdapter.setBackground(this.mboundView1, itemViewModelServicesItemCheckedMboundView1AndroidDrawableCornerViewPrimaryBorderMboundView1AndroidDrawableCornerViewGrayBorde);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView4, itemViewModelServicesItemName);
        }
        if ((dirtyFlags & 0x4L) != 0) {
            // api target 1

            this.checkbox.setOnClickListener(mCallback122);
            this.mboundView0.setOnClickListener(mCallback121);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        switch(sourceId) {
            case 2: {
                // localize variables for thread safety
                // itemViewModel
                grand.app.aber_user.pages.services.viewModels.ItemServicesViewModel itemViewModel = mItemViewModel;
                // itemViewModel != null
                boolean itemViewModelJavaLangObjectNull = false;



                itemViewModelJavaLangObjectNull = (itemViewModel) != (null);
                if (itemViewModelJavaLangObjectNull) {


                    itemViewModel.itemAction();
                }
                break;
            }
            case 1: {
                // localize variables for thread safety
                // itemViewModel
                grand.app.aber_user.pages.services.viewModels.ItemServicesViewModel itemViewModel = mItemViewModel;
                // itemViewModel != null
                boolean itemViewModelJavaLangObjectNull = false;



                itemViewModelJavaLangObjectNull = (itemViewModel) != (null);
                if (itemViewModelJavaLangObjectNull) {


                    itemViewModel.itemAction();
                }
                break;
            }
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): itemViewModel
        flag 1 (0x2L): itemViewModel.servicesItem
        flag 2 (0x3L): null
        flag 3 (0x4L): itemViewModel.servicesItem.checked ? @android:drawable/corner_view_primary_border : @android:drawable/corner_view_gray_borde
        flag 4 (0x5L): itemViewModel.servicesItem.checked ? @android:drawable/corner_view_primary_border : @android:drawable/corner_view_gray_borde
        flag 5 (0x6L): itemViewModel.servicesItem.checked ? @android:drawable/corner_view_primary_border : @android:drawable/corner_view_gray_borde
        flag 6 (0x7L): itemViewModel.servicesItem.checked ? @android:drawable/corner_view_primary_border : @android:drawable/corner_view_gray_borde
    flag mapping end*/
    //end
}