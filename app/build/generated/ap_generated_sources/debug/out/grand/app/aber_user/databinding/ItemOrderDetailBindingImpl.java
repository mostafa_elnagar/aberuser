package grand.app.aber_user.databinding;
import grand.app.aber_user.R;
import grand.app.aber_user.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ItemOrderDetailBindingImpl extends ItemOrderDetailBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.br, 6);
    }
    // views
    @NonNull
    private final androidx.cardview.widget.CardView mboundView0;
    @NonNull
    private final grand.app.aber_user.customViews.views.CustomTextViewMedium mboundView5;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ItemOrderDetailBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 7, sIncludes, sViewsWithIds));
    }
    private ItemOrderDetailBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 1
            , (androidx.constraintlayout.widget.Barrier) bindings[6]
            , (com.google.android.material.button.MaterialButton) bindings[3]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[1]
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[2]
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[4]
            );
        this.btnCount.setTag(null);
        this.icPartsImage.setTag(null);
        this.mboundView0 = (androidx.cardview.widget.CardView) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView5 = (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[5];
        this.mboundView5.setTag(null);
        this.tvPartsName.setTag(null);
        this.tvPartsPrice.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.itemViewModel == variableId) {
            setItemViewModel((grand.app.aber_user.pages.myOrders.viewModels.ItemMyOrderProductsViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setItemViewModel(@Nullable grand.app.aber_user.pages.myOrders.viewModels.ItemMyOrderProductsViewModel ItemViewModel) {
        updateRegistration(0, ItemViewModel);
        this.mItemViewModel = ItemViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.itemViewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeItemViewModel((grand.app.aber_user.pages.myOrders.viewModels.ItemMyOrderProductsViewModel) object, fieldId);
        }
        return false;
    }
    private boolean onChangeItemViewModel(grand.app.aber_user.pages.myOrders.viewModels.ItemMyOrderProductsViewModel ItemViewModel, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        else if (fieldId == BR.productsItem) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String stringValueOfItemViewModelProductsItemQuantityConcatJavaLangString = null;
        java.lang.String itemViewModelProductsItemTotal = null;
        grand.app.aber_user.pages.parts.models.ProductsItem itemViewModelProductsItem = null;
        java.lang.String itemViewModelProductsItemImage = null;
        java.lang.String itemViewModelProductsItemStatusInt3MboundView5AndroidStringOrderOnWayMboundView5AndroidStringOrderDelivered = null;
        int itemViewModelProductsItemQuantity = 0;
        int itemViewModelProductsItemStatus = 0;
        java.lang.String itemViewModelProductsItemName = null;
        boolean itemViewModelProductsItemStatusInt2 = false;
        grand.app.aber_user.pages.myOrders.viewModels.ItemMyOrderProductsViewModel itemViewModel = mItemViewModel;
        java.lang.String itemViewModelProductsItemStatusInt1MboundView5AndroidStringOrderAcceptedItemViewModelProductsItemStatusInt2MboundView5AndroidStringOrderShippedItemViewModelProductsItemStatusInt3MboundView5AndroidStringOrderOnWayMboundView5AndroidStringOrderDelivered = null;
        java.lang.String stringValueOfItemViewModelProductsItemQuantity = null;
        java.lang.String stringValueOfItemViewModelProductsItemQuantityConcatJavaLangStringConcatBtnCountAndroidStringCartItemPart = null;
        boolean itemViewModelProductsItemStatusInt3 = false;
        boolean itemViewModelProductsItemStatusInt1 = false;
        java.lang.String itemViewModelProductsItemStatusInt2MboundView5AndroidStringOrderShippedItemViewModelProductsItemStatusInt3MboundView5AndroidStringOrderOnWayMboundView5AndroidStringOrderDelivered = null;

        if ((dirtyFlags & 0x7L) != 0) {



                if (itemViewModel != null) {
                    // read itemViewModel.productsItem
                    itemViewModelProductsItem = itemViewModel.getProductsItem();
                }


                if (itemViewModelProductsItem != null) {
                    // read itemViewModel.productsItem.total
                    itemViewModelProductsItemTotal = itemViewModelProductsItem.getTotal();
                    // read itemViewModel.productsItem.image
                    itemViewModelProductsItemImage = itemViewModelProductsItem.getImage();
                    // read itemViewModel.productsItem.quantity
                    itemViewModelProductsItemQuantity = itemViewModelProductsItem.getQuantity();
                    // read itemViewModel.productsItem.status
                    itemViewModelProductsItemStatus = itemViewModelProductsItem.getStatus();
                    // read itemViewModel.productsItem.name
                    itemViewModelProductsItemName = itemViewModelProductsItem.getName();
                }


                // read String.valueOf(itemViewModel.productsItem.quantity)
                stringValueOfItemViewModelProductsItemQuantity = java.lang.String.valueOf(itemViewModelProductsItemQuantity);
                // read itemViewModel.productsItem.status >= 1
                itemViewModelProductsItemStatusInt1 = (itemViewModelProductsItemStatus) >= (1);
            if((dirtyFlags & 0x7L) != 0) {
                if(itemViewModelProductsItemStatusInt1) {
                        dirtyFlags |= 0x40L;
                }
                else {
                        dirtyFlags |= 0x20L;
                }
            }


                if (stringValueOfItemViewModelProductsItemQuantity != null) {
                    // read String.valueOf(itemViewModel.productsItem.quantity).concat(" ")
                    stringValueOfItemViewModelProductsItemQuantityConcatJavaLangString = stringValueOfItemViewModelProductsItemQuantity.concat(" ");
                }


                if (stringValueOfItemViewModelProductsItemQuantityConcatJavaLangString != null) {
                    // read String.valueOf(itemViewModel.productsItem.quantity).concat(" ").concat(@android:string/cart_item_part)
                    stringValueOfItemViewModelProductsItemQuantityConcatJavaLangStringConcatBtnCountAndroidStringCartItemPart = stringValueOfItemViewModelProductsItemQuantityConcatJavaLangString.concat(btnCount.getResources().getString(R.string.cart_item_part));
                }
        }
        // batch finished

        if ((dirtyFlags & 0x20L) != 0) {



                if (itemViewModel != null) {
                    // read itemViewModel.productsItem
                    itemViewModelProductsItem = itemViewModel.getProductsItem();
                }


                if (itemViewModelProductsItem != null) {
                    // read itemViewModel.productsItem.status
                    itemViewModelProductsItemStatus = itemViewModelProductsItem.getStatus();
                }


                // read itemViewModel.productsItem.status >= 2
                itemViewModelProductsItemStatusInt2 = (itemViewModelProductsItemStatus) >= (2);
            if((dirtyFlags & 0x20L) != 0) {
                if(itemViewModelProductsItemStatusInt2) {
                        dirtyFlags |= 0x100L;
                }
                else {
                        dirtyFlags |= 0x80L;
                }
            }
        }
        // batch finished

        if ((dirtyFlags & 0x80L) != 0) {

                // read itemViewModel.productsItem.status >= 3
                itemViewModelProductsItemStatusInt3 = (itemViewModelProductsItemStatus) >= (3);
            if((dirtyFlags & 0x80L) != 0) {
                if(itemViewModelProductsItemStatusInt3) {
                        dirtyFlags |= 0x10L;
                }
                else {
                        dirtyFlags |= 0x8L;
                }
            }


                // read itemViewModel.productsItem.status >= 3 ? @android:string/order_on_way : @android:string/order_delivered
                itemViewModelProductsItemStatusInt3MboundView5AndroidStringOrderOnWayMboundView5AndroidStringOrderDelivered = ((itemViewModelProductsItemStatusInt3) ? (mboundView5.getResources().getString(R.string.order_on_way)) : (mboundView5.getResources().getString(R.string.order_delivered)));
        }

        if ((dirtyFlags & 0x20L) != 0) {

                // read itemViewModel.productsItem.status >= 2 ? @android:string/order_shipped : itemViewModel.productsItem.status >= 3 ? @android:string/order_on_way : @android:string/order_delivered
                itemViewModelProductsItemStatusInt2MboundView5AndroidStringOrderShippedItemViewModelProductsItemStatusInt3MboundView5AndroidStringOrderOnWayMboundView5AndroidStringOrderDelivered = ((itemViewModelProductsItemStatusInt2) ? (mboundView5.getResources().getString(R.string.order_shipped)) : (itemViewModelProductsItemStatusInt3MboundView5AndroidStringOrderOnWayMboundView5AndroidStringOrderDelivered));
        }

        if ((dirtyFlags & 0x7L) != 0) {

                // read itemViewModel.productsItem.status >= 1 ? @android:string/order_accepted : itemViewModel.productsItem.status >= 2 ? @android:string/order_shipped : itemViewModel.productsItem.status >= 3 ? @android:string/order_on_way : @android:string/order_delivered
                itemViewModelProductsItemStatusInt1MboundView5AndroidStringOrderAcceptedItemViewModelProductsItemStatusInt2MboundView5AndroidStringOrderShippedItemViewModelProductsItemStatusInt3MboundView5AndroidStringOrderOnWayMboundView5AndroidStringOrderDelivered = ((itemViewModelProductsItemStatusInt1) ? (mboundView5.getResources().getString(R.string.order_accepted)) : (itemViewModelProductsItemStatusInt2MboundView5AndroidStringOrderShippedItemViewModelProductsItemStatusInt3MboundView5AndroidStringOrderOnWayMboundView5AndroidStringOrderDelivered));
        }
        // batch finished
        if ((dirtyFlags & 0x7L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.btnCount, stringValueOfItemViewModelProductsItemQuantityConcatJavaLangStringConcatBtnCountAndroidStringCartItemPart);
            grand.app.aber_user.base.ApplicationBinding.loadImage(this.icPartsImage, itemViewModelProductsItemImage);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView5, itemViewModelProductsItemStatusInt1MboundView5AndroidStringOrderAcceptedItemViewModelProductsItemStatusInt2MboundView5AndroidStringOrderShippedItemViewModelProductsItemStatusInt3MboundView5AndroidStringOrderOnWayMboundView5AndroidStringOrderDelivered);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvPartsName, itemViewModelProductsItemName);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvPartsPrice, itemViewModelProductsItemTotal);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): itemViewModel
        flag 1 (0x2L): itemViewModel.productsItem
        flag 2 (0x3L): null
        flag 3 (0x4L): itemViewModel.productsItem.status >= 3 ? @android:string/order_on_way : @android:string/order_delivered
        flag 4 (0x5L): itemViewModel.productsItem.status >= 3 ? @android:string/order_on_way : @android:string/order_delivered
        flag 5 (0x6L): itemViewModel.productsItem.status >= 1 ? @android:string/order_accepted : itemViewModel.productsItem.status >= 2 ? @android:string/order_shipped : itemViewModel.productsItem.status >= 3 ? @android:string/order_on_way : @android:string/order_delivered
        flag 6 (0x7L): itemViewModel.productsItem.status >= 1 ? @android:string/order_accepted : itemViewModel.productsItem.status >= 2 ? @android:string/order_shipped : itemViewModel.productsItem.status >= 3 ? @android:string/order_on_way : @android:string/order_delivered
        flag 7 (0x8L): itemViewModel.productsItem.status >= 2 ? @android:string/order_shipped : itemViewModel.productsItem.status >= 3 ? @android:string/order_on_way : @android:string/order_delivered
        flag 8 (0x9L): itemViewModel.productsItem.status >= 2 ? @android:string/order_shipped : itemViewModel.productsItem.status >= 3 ? @android:string/order_on_way : @android:string/order_delivered
    flag mapping end*/
    //end
}