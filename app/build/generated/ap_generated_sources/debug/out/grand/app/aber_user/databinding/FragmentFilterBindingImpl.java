package grand.app.aber_user.databinding;
import grand.app.aber_user.R;
import grand.app.aber_user.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentFilterBindingImpl extends FragmentFilterBinding implements grand.app.aber_user.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.card_car_types, 7);
        sViewsWithIds.put(R.id.tv_car_types, 8);
        sViewsWithIds.put(R.id.card_car_cat, 9);
        sViewsWithIds.put(R.id.tv_car_cat, 10);
        sViewsWithIds.put(R.id.card_car_model, 11);
        sViewsWithIds.put(R.id.tv_car_model, 12);
        sViewsWithIds.put(R.id.card_car_colors, 13);
        sViewsWithIds.put(R.id.tv_car_color, 14);
        sViewsWithIds.put(R.id.tv_price, 15);
        sViewsWithIds.put(R.id.tv_min_price, 16);
        sViewsWithIds.put(R.id.tv_dash, 17);
        sViewsWithIds.put(R.id.tv_max_price, 18);
        sViewsWithIds.put(R.id.rangeBar, 19);
    }
    // views
    @NonNull
    private final androidx.core.widget.NestedScrollView mboundView0;
    @NonNull
    private final androidx.recyclerview.widget.RecyclerView mboundView1;
    @NonNull
    private final androidx.recyclerview.widget.RecyclerView mboundView2;
    @NonNull
    private final androidx.recyclerview.widget.RecyclerView mboundView3;
    @NonNull
    private final androidx.recyclerview.widget.RecyclerView mboundView4;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback181;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentFilterBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 20, sIncludes, sViewsWithIds));
    }
    private FragmentFilterBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 1
            , (androidx.appcompat.widget.AppCompatButton) bindings[5]
            , (androidx.cardview.widget.CardView) bindings[9]
            , (androidx.cardview.widget.CardView) bindings[13]
            , (androidx.cardview.widget.CardView) bindings[11]
            , (androidx.cardview.widget.CardView) bindings[7]
            , (com.google.android.material.progressindicator.CircularProgressIndicator) bindings[6]
            , (com.github.guilhe.views.SeekBarRangedView) bindings[19]
            , (androidx.cardview.widget.CardView) bindings[10]
            , (androidx.cardview.widget.CardView) bindings[14]
            , (androidx.cardview.widget.CardView) bindings[12]
            , (androidx.cardview.widget.CardView) bindings[8]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[17]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[18]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[16]
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[15]
            );
        this.btnPhone.setTag(null);
        this.mboundView0 = (androidx.core.widget.NestedScrollView) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (androidx.recyclerview.widget.RecyclerView) bindings[1];
        this.mboundView1.setTag(null);
        this.mboundView2 = (androidx.recyclerview.widget.RecyclerView) bindings[2];
        this.mboundView2.setTag(null);
        this.mboundView3 = (androidx.recyclerview.widget.RecyclerView) bindings[3];
        this.mboundView3.setTag(null);
        this.mboundView4 = (androidx.recyclerview.widget.RecyclerView) bindings[4];
        this.mboundView4.setTag(null);
        this.progress.setTag(null);
        setRootTag(root);
        // listeners
        mCallback181 = new grand.app.aber_user.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x80L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.viewModel == variableId) {
            setViewModel((grand.app.aber_user.pages.parts.viewModels.FilterViewModels) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setViewModel(@Nullable grand.app.aber_user.pages.parts.viewModels.FilterViewModels ViewModel) {
        updateRegistration(0, ViewModel);
        this.mViewModel = ViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.viewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeViewModel((grand.app.aber_user.pages.parts.viewModels.FilterViewModels) object, fieldId);
        }
        return false;
    }
    private boolean onChangeViewModel(grand.app.aber_user.pages.parts.viewModels.FilterViewModels ViewModel, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        else if (fieldId == BR.filterData) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        else if (fieldId == BR.carTypeAdapter) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        else if (fieldId == BR.carCatAdapter) {
            synchronized(this) {
                    mDirtyFlags |= 0x8L;
            }
            return true;
        }
        else if (fieldId == BR.carModelAdapter) {
            synchronized(this) {
                    mDirtyFlags |= 0x10L;
            }
            return true;
        }
        else if (fieldId == BR.colorsAdapter) {
            synchronized(this) {
                    mDirtyFlags |= 0x20L;
            }
            return true;
        }
        else if (fieldId == BR.message) {
            synchronized(this) {
                    mDirtyFlags |= 0x40L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        int viewModelCarCatAdapterItemCount = 0;
        boolean viewModelCarCatAdapterItemCountInt0 = false;
        int viewModelCarCatAdapterItemCountInt0ViewGONEViewVISIBLE = 0;
        grand.app.aber_user.pages.parts.adapters.CarAdapter viewModelCarModelAdapter = null;
        boolean textUtilsIsEmptyViewModelMessage = false;
        java.lang.String viewModelMessage = null;
        boolean TextUtilsIsEmptyViewModelMessage1 = false;
        int viewModelCarModelAdapterItemCountInt0ViewGONEViewVISIBLE = 0;
        int viewModelCarModelAdapterItemCount = 0;
        boolean viewModelCarModelAdapterItemCountInt0 = false;
        grand.app.aber_user.pages.parts.adapters.ProductColorsAdapter viewModelColorsAdapter = null;
        boolean textUtilsIsEmptyViewModelMessageViewModelMessageEqualsConstantsSHOWPROGRESSBooleanFalse = false;
        int textUtilsIsEmptyViewModelMessageViewModelMessageEqualsConstantsSHOWPROGRESSBooleanFalseViewVISIBLEViewGONE = 0;
        boolean viewModelFilterDataAttributesJavaLangObjectNull = false;
        int viewModelFilterDataAttributesJavaLangObjectNullViewVISIBLEViewGONE = 0;
        grand.app.aber_user.pages.parts.adapters.CarAdapter viewModelCarTypeAdapter = null;
        grand.app.aber_user.pages.parts.models.filter.FilterData viewModelFilterData = null;
        android.graphics.drawable.Drawable textUtilsIsEmptyViewModelMessageBooleanTrueViewModelMessageEqualsConstantsHIDEPROGRESSBtnPhoneAndroidDrawableCornerViewGradientBtnPhoneAndroidDrawableCornerViewPrimaryMedium = null;
        boolean viewModelMessageEqualsConstantsHIDEPROGRESS = false;
        grand.app.aber_user.pages.parts.adapters.CarAdapter viewModelCarCatAdapter = null;
        boolean textUtilsIsEmptyViewModelMessageBooleanTrueViewModelMessageEqualsConstantsHIDEPROGRESSBooleanTrueBooleanFalse = false;
        boolean viewModelMessageEqualsConstantsSHOWPROGRESS = false;
        java.util.List<grand.app.aber_user.pages.parts.models.productDetails.AttributesItem> viewModelFilterDataAttributes = null;
        grand.app.aber_user.pages.parts.viewModels.FilterViewModels viewModel = mViewModel;
        boolean textUtilsIsEmptyViewModelMessageBooleanTrueViewModelMessageEqualsConstantsHIDEPROGRESS = false;

        if ((dirtyFlags & 0xffL) != 0) {


            if ((dirtyFlags & 0x91L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.carModelAdapter
                        viewModelCarModelAdapter = viewModel.getCarModelAdapter();
                    }


                    if (viewModelCarModelAdapter != null) {
                        // read viewModel.carModelAdapter.itemCount
                        viewModelCarModelAdapterItemCount = viewModelCarModelAdapter.getItemCount();
                    }


                    // read viewModel.carModelAdapter.itemCount == 0
                    viewModelCarModelAdapterItemCountInt0 = (viewModelCarModelAdapterItemCount) == (0);
                if((dirtyFlags & 0x91L) != 0) {
                    if(viewModelCarModelAdapterItemCountInt0) {
                            dirtyFlags |= 0x800L;
                    }
                    else {
                            dirtyFlags |= 0x400L;
                    }
                }


                    // read viewModel.carModelAdapter.itemCount == 0 ? View.GONE : View.VISIBLE
                    viewModelCarModelAdapterItemCountInt0ViewGONEViewVISIBLE = ((viewModelCarModelAdapterItemCountInt0) ? (android.view.View.GONE) : (android.view.View.VISIBLE));
            }
            if ((dirtyFlags & 0xc1L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.message
                        viewModelMessage = viewModel.getMessage();
                    }


                    // read TextUtils.isEmpty(viewModel.message)
                    TextUtilsIsEmptyViewModelMessage1 = android.text.TextUtils.isEmpty(viewModelMessage);
                if((dirtyFlags & 0xc1L) != 0) {
                    if(TextUtilsIsEmptyViewModelMessage1) {
                            dirtyFlags |= 0x800000L;
                    }
                    else {
                            dirtyFlags |= 0x400000L;
                    }
                }


                    // read !TextUtils.isEmpty(viewModel.message)
                    textUtilsIsEmptyViewModelMessage = !TextUtilsIsEmptyViewModelMessage1;
                if((dirtyFlags & 0xc1L) != 0) {
                    if(textUtilsIsEmptyViewModelMessage) {
                            dirtyFlags |= 0x2000L;
                    }
                    else {
                            dirtyFlags |= 0x1000L;
                    }
                }
            }
            if ((dirtyFlags & 0xa1L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.colorsAdapter
                        viewModelColorsAdapter = viewModel.getColorsAdapter();
                    }
            }
            if ((dirtyFlags & 0x85L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.carTypeAdapter
                        viewModelCarTypeAdapter = viewModel.getCarTypeAdapter();
                    }
            }
            if ((dirtyFlags & 0x83L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.filterData
                        viewModelFilterData = viewModel.getFilterData();
                    }


                    if (viewModelFilterData != null) {
                        // read viewModel.filterData.attributes
                        viewModelFilterDataAttributes = viewModelFilterData.getAttributes();
                    }


                    // read viewModel.filterData.attributes != null
                    viewModelFilterDataAttributesJavaLangObjectNull = (viewModelFilterDataAttributes) != (null);
                if((dirtyFlags & 0x83L) != 0) {
                    if(viewModelFilterDataAttributesJavaLangObjectNull) {
                            dirtyFlags |= 0x20000L;
                    }
                    else {
                            dirtyFlags |= 0x10000L;
                    }
                }


                    // read viewModel.filterData.attributes != null ? View.VISIBLE : View.GONE
                    viewModelFilterDataAttributesJavaLangObjectNullViewVISIBLEViewGONE = ((viewModelFilterDataAttributesJavaLangObjectNull) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
            if ((dirtyFlags & 0x89L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.carCatAdapter
                        viewModelCarCatAdapter = viewModel.getCarCatAdapter();
                    }


                    if (viewModelCarCatAdapter != null) {
                        // read viewModel.carCatAdapter.itemCount
                        viewModelCarCatAdapterItemCount = viewModelCarCatAdapter.getItemCount();
                    }


                    // read viewModel.carCatAdapter.itemCount == 0
                    viewModelCarCatAdapterItemCountInt0 = (viewModelCarCatAdapterItemCount) == (0);
                if((dirtyFlags & 0x89L) != 0) {
                    if(viewModelCarCatAdapterItemCountInt0) {
                            dirtyFlags |= 0x200L;
                    }
                    else {
                            dirtyFlags |= 0x100L;
                    }
                }


                    // read viewModel.carCatAdapter.itemCount == 0 ? View.GONE : View.VISIBLE
                    viewModelCarCatAdapterItemCountInt0ViewGONEViewVISIBLE = ((viewModelCarCatAdapterItemCountInt0) ? (android.view.View.GONE) : (android.view.View.VISIBLE));
            }
        }
        // batch finished

        if ((dirtyFlags & 0x400000L) != 0) {

                if (viewModelMessage != null) {
                    // read viewModel.message.equals(Constants.HIDE_PROGRESS)
                    viewModelMessageEqualsConstantsHIDEPROGRESS = viewModelMessage.equals(grand.app.aber_user.utils.Constants.HIDE_PROGRESS);
                }
        }
        if ((dirtyFlags & 0x2000L) != 0) {

                if (viewModelMessage != null) {
                    // read viewModel.message.equals(Constants.SHOW_PROGRESS)
                    viewModelMessageEqualsConstantsSHOWPROGRESS = viewModelMessage.equals(grand.app.aber_user.utils.Constants.SHOW_PROGRESS);
                }
        }

        if ((dirtyFlags & 0xc1L) != 0) {

                // read !TextUtils.isEmpty(viewModel.message) ? viewModel.message.equals(Constants.SHOW_PROGRESS) : false
                textUtilsIsEmptyViewModelMessageViewModelMessageEqualsConstantsSHOWPROGRESSBooleanFalse = ((textUtilsIsEmptyViewModelMessage) ? (viewModelMessageEqualsConstantsSHOWPROGRESS) : (false));
                // read TextUtils.isEmpty(viewModel.message) ? true : viewModel.message.equals(Constants.HIDE_PROGRESS)
                textUtilsIsEmptyViewModelMessageBooleanTrueViewModelMessageEqualsConstantsHIDEPROGRESS = ((TextUtilsIsEmptyViewModelMessage1) ? (true) : (viewModelMessageEqualsConstantsHIDEPROGRESS));
            if((dirtyFlags & 0xc1L) != 0) {
                if(textUtilsIsEmptyViewModelMessageViewModelMessageEqualsConstantsSHOWPROGRESSBooleanFalse) {
                        dirtyFlags |= 0x8000L;
                }
                else {
                        dirtyFlags |= 0x4000L;
                }
            }
            if((dirtyFlags & 0xc1L) != 0) {
                if(textUtilsIsEmptyViewModelMessageBooleanTrueViewModelMessageEqualsConstantsHIDEPROGRESS) {
                        dirtyFlags |= 0x80000L;
                        dirtyFlags |= 0x200000L;
                }
                else {
                        dirtyFlags |= 0x40000L;
                        dirtyFlags |= 0x100000L;
                }
            }


                // read !TextUtils.isEmpty(viewModel.message) ? viewModel.message.equals(Constants.SHOW_PROGRESS) : false ? View.VISIBLE : View.GONE
                textUtilsIsEmptyViewModelMessageViewModelMessageEqualsConstantsSHOWPROGRESSBooleanFalseViewVISIBLEViewGONE = ((textUtilsIsEmptyViewModelMessageViewModelMessageEqualsConstantsSHOWPROGRESSBooleanFalse) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
                // read TextUtils.isEmpty(viewModel.message) ? true : viewModel.message.equals(Constants.HIDE_PROGRESS) ? @android:drawable/corner_view_gradient : @android:drawable/corner_view_primary_medium
                textUtilsIsEmptyViewModelMessageBooleanTrueViewModelMessageEqualsConstantsHIDEPROGRESSBtnPhoneAndroidDrawableCornerViewGradientBtnPhoneAndroidDrawableCornerViewPrimaryMedium = ((textUtilsIsEmptyViewModelMessageBooleanTrueViewModelMessageEqualsConstantsHIDEPROGRESS) ? (androidx.appcompat.content.res.AppCompatResources.getDrawable(btnPhone.getContext(), R.drawable.corner_view_gradient)) : (androidx.appcompat.content.res.AppCompatResources.getDrawable(btnPhone.getContext(), R.drawable.corner_view_primary_medium)));
                // read TextUtils.isEmpty(viewModel.message) ? true : viewModel.message.equals(Constants.HIDE_PROGRESS) ? true : false
                textUtilsIsEmptyViewModelMessageBooleanTrueViewModelMessageEqualsConstantsHIDEPROGRESSBooleanTrueBooleanFalse = ((textUtilsIsEmptyViewModelMessageBooleanTrueViewModelMessageEqualsConstantsHIDEPROGRESS) ? (true) : (false));
        }
        // batch finished
        if ((dirtyFlags & 0xc1L) != 0) {
            // api target 1

            androidx.databinding.adapters.ViewBindingAdapter.setBackground(this.btnPhone, textUtilsIsEmptyViewModelMessageBooleanTrueViewModelMessageEqualsConstantsHIDEPROGRESSBtnPhoneAndroidDrawableCornerViewGradientBtnPhoneAndroidDrawableCornerViewPrimaryMedium);
            this.btnPhone.setEnabled(textUtilsIsEmptyViewModelMessageBooleanTrueViewModelMessageEqualsConstantsHIDEPROGRESSBooleanTrueBooleanFalse);
            this.progress.setVisibility(textUtilsIsEmptyViewModelMessageViewModelMessageEqualsConstantsSHOWPROGRESSBooleanFalseViewVISIBLEViewGONE);
        }
        if ((dirtyFlags & 0x80L) != 0) {
            // api target 1

            this.btnPhone.setOnClickListener(mCallback181);
        }
        if ((dirtyFlags & 0x83L) != 0) {
            // api target 1

            this.mboundView0.setVisibility(viewModelFilterDataAttributesJavaLangObjectNullViewVISIBLEViewGONE);
        }
        if ((dirtyFlags & 0x85L) != 0) {
            // api target 1

            grand.app.aber_user.base.ApplicationBinding.getItemsV2Binding(this.mboundView1, viewModelCarTypeAdapter, "1", "1");
        }
        if ((dirtyFlags & 0x89L) != 0) {
            // api target 1

            this.mboundView2.setVisibility(viewModelCarCatAdapterItemCountInt0ViewGONEViewVISIBLE);
            grand.app.aber_user.base.ApplicationBinding.getItemsV2Binding(this.mboundView2, viewModelCarCatAdapter, "1", "1");
        }
        if ((dirtyFlags & 0x91L) != 0) {
            // api target 1

            this.mboundView3.setVisibility(viewModelCarModelAdapterItemCountInt0ViewGONEViewVISIBLE);
            grand.app.aber_user.base.ApplicationBinding.getItemsV2Binding(this.mboundView3, viewModelCarModelAdapter, "1", "1");
        }
        if ((dirtyFlags & 0xa1L) != 0) {
            // api target 1

            grand.app.aber_user.base.ApplicationBinding.getItemsV2Binding(this.mboundView4, viewModelColorsAdapter, "1", "2");
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        // localize variables for thread safety
        // viewModel
        grand.app.aber_user.pages.parts.viewModels.FilterViewModels viewModel = mViewModel;
        // viewModel != null
        boolean viewModelJavaLangObjectNull = false;



        viewModelJavaLangObjectNull = (viewModel) != (null);
        if (viewModelJavaLangObjectNull) {


            viewModel.filter();
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): viewModel
        flag 1 (0x2L): viewModel.filterData
        flag 2 (0x3L): viewModel.carTypeAdapter
        flag 3 (0x4L): viewModel.carCatAdapter
        flag 4 (0x5L): viewModel.carModelAdapter
        flag 5 (0x6L): viewModel.colorsAdapter
        flag 6 (0x7L): viewModel.message
        flag 7 (0x8L): null
        flag 8 (0x9L): viewModel.carCatAdapter.itemCount == 0 ? View.GONE : View.VISIBLE
        flag 9 (0xaL): viewModel.carCatAdapter.itemCount == 0 ? View.GONE : View.VISIBLE
        flag 10 (0xbL): viewModel.carModelAdapter.itemCount == 0 ? View.GONE : View.VISIBLE
        flag 11 (0xcL): viewModel.carModelAdapter.itemCount == 0 ? View.GONE : View.VISIBLE
        flag 12 (0xdL): !TextUtils.isEmpty(viewModel.message) ? viewModel.message.equals(Constants.SHOW_PROGRESS) : false
        flag 13 (0xeL): !TextUtils.isEmpty(viewModel.message) ? viewModel.message.equals(Constants.SHOW_PROGRESS) : false
        flag 14 (0xfL): !TextUtils.isEmpty(viewModel.message) ? viewModel.message.equals(Constants.SHOW_PROGRESS) : false ? View.VISIBLE : View.GONE
        flag 15 (0x10L): !TextUtils.isEmpty(viewModel.message) ? viewModel.message.equals(Constants.SHOW_PROGRESS) : false ? View.VISIBLE : View.GONE
        flag 16 (0x11L): viewModel.filterData.attributes != null ? View.VISIBLE : View.GONE
        flag 17 (0x12L): viewModel.filterData.attributes != null ? View.VISIBLE : View.GONE
        flag 18 (0x13L): TextUtils.isEmpty(viewModel.message) ? true : viewModel.message.equals(Constants.HIDE_PROGRESS) ? @android:drawable/corner_view_gradient : @android:drawable/corner_view_primary_medium
        flag 19 (0x14L): TextUtils.isEmpty(viewModel.message) ? true : viewModel.message.equals(Constants.HIDE_PROGRESS) ? @android:drawable/corner_view_gradient : @android:drawable/corner_view_primary_medium
        flag 20 (0x15L): TextUtils.isEmpty(viewModel.message) ? true : viewModel.message.equals(Constants.HIDE_PROGRESS) ? true : false
        flag 21 (0x16L): TextUtils.isEmpty(viewModel.message) ? true : viewModel.message.equals(Constants.HIDE_PROGRESS) ? true : false
        flag 22 (0x17L): TextUtils.isEmpty(viewModel.message) ? true : viewModel.message.equals(Constants.HIDE_PROGRESS)
        flag 23 (0x18L): TextUtils.isEmpty(viewModel.message) ? true : viewModel.message.equals(Constants.HIDE_PROGRESS)
    flag mapping end*/
    //end
}