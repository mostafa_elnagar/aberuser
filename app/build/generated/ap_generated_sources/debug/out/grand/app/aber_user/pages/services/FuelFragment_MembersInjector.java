// Generated by Dagger (https://dagger.dev).
package grand.app.aber_user.pages.services;

import dagger.MembersInjector;
import dagger.internal.InjectedFieldSignature;
import grand.app.aber_user.pages.services.viewModels.ServicesViewModels;
import javax.inject.Provider;

@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class FuelFragment_MembersInjector implements MembersInjector<FuelFragment> {
  private final Provider<ServicesViewModels> viewModelProvider;

  public FuelFragment_MembersInjector(Provider<ServicesViewModels> viewModelProvider) {
    this.viewModelProvider = viewModelProvider;
  }

  public static MembersInjector<FuelFragment> create(
      Provider<ServicesViewModels> viewModelProvider) {
    return new FuelFragment_MembersInjector(viewModelProvider);
  }

  @Override
  public void injectMembers(FuelFragment instance) {
    injectViewModel(instance, viewModelProvider.get());
  }

  @InjectedFieldSignature("grand.app.aber_user.pages.services.FuelFragment.viewModel")
  public static void injectViewModel(FuelFragment instance, ServicesViewModels viewModel) {
    instance.viewModel = viewModel;
  }
}
