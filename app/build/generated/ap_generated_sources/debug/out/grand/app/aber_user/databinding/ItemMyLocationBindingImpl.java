package grand.app.aber_user.databinding;
import grand.app.aber_user.R;
import grand.app.aber_user.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ItemMyLocationBindingImpl extends ItemMyLocationBinding implements grand.app.aber_user.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.slider_container, 7);
    }
    // views
    @NonNull
    private final androidx.cardview.widget.CardView mboundView0;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback52;
    @Nullable
    private final android.view.View.OnClickListener mCallback50;
    @Nullable
    private final android.view.View.OnClickListener mCallback51;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ItemMyLocationBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 8, sIncludes, sViewsWithIds));
    }
    private ItemMyLocationBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 1
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[6]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[5]
            , (android.widget.RadioButton) bindings[2]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[7]
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[1]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[3]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[4]
            );
        this.icDelete.setTag(null);
        this.icEdit.setTag(null);
        this.mboundView0 = (androidx.cardview.widget.CardView) bindings[0];
        this.mboundView0.setTag(null);
        this.radio.setTag(null);
        this.tvFirstLocation.setTag(null);
        this.tvYourLocation.setTag(null);
        this.yourLocation.setTag(null);
        setRootTag(root);
        // listeners
        mCallback52 = new grand.app.aber_user.generated.callback.OnClickListener(this, 3);
        mCallback50 = new grand.app.aber_user.generated.callback.OnClickListener(this, 1);
        mCallback51 = new grand.app.aber_user.generated.callback.OnClickListener(this, 2);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.itemViewModel == variableId) {
            setItemViewModel((grand.app.aber_user.pages.myLocations.viewModels.LocationsItemViewModels) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setItemViewModel(@Nullable grand.app.aber_user.pages.myLocations.viewModels.LocationsItemViewModels ItemViewModel) {
        updateRegistration(0, ItemViewModel);
        this.mItemViewModel = ItemViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.itemViewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeItemViewModel((grand.app.aber_user.pages.myLocations.viewModels.LocationsItemViewModels) object, fieldId);
        }
        return false;
    }
    private boolean onChangeItemViewModel(grand.app.aber_user.pages.myLocations.viewModels.LocationsItemViewModels ItemViewModel, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        else if (fieldId == BR.locationsData) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        boolean itemViewModelLocationsDataIsRemove = false;
        grand.app.aber_user.pages.myLocations.viewModels.LocationsItemViewModels itemViewModel = mItemViewModel;
        java.lang.String itemViewModelLocationsDataName = null;
        boolean itemViewModelLocationsDataIsRemoveBooleanTrue = false;
        java.lang.String itemViewModelLocationsDataPhone = null;
        grand.app.aber_user.pages.myLocations.models.LocationsData itemViewModelLocationsData = null;
        java.lang.String itemViewModelLocationsDataStreet = null;
        int itemViewModelLocationsDataIsRemoveBooleanTrueViewVISIBLEViewGONE = 0;

        if ((dirtyFlags & 0x7L) != 0) {



                if (itemViewModel != null) {
                    // read itemViewModel.locationsData
                    itemViewModelLocationsData = itemViewModel.getLocationsData();
                }


                if (itemViewModelLocationsData != null) {
                    // read itemViewModel.locationsData.isRemove
                    itemViewModelLocationsDataIsRemove = itemViewModelLocationsData.isRemove;
                    // read itemViewModel.locationsData.name
                    itemViewModelLocationsDataName = itemViewModelLocationsData.getName();
                    // read itemViewModel.locationsData.phone
                    itemViewModelLocationsDataPhone = itemViewModelLocationsData.getPhone();
                    // read itemViewModel.locationsData.street
                    itemViewModelLocationsDataStreet = itemViewModelLocationsData.getStreet();
                }


                // read itemViewModel.locationsData.isRemove == true
                itemViewModelLocationsDataIsRemoveBooleanTrue = (itemViewModelLocationsDataIsRemove) == (true);
            if((dirtyFlags & 0x7L) != 0) {
                if(itemViewModelLocationsDataIsRemoveBooleanTrue) {
                        dirtyFlags |= 0x10L;
                }
                else {
                        dirtyFlags |= 0x8L;
                }
            }


                // read itemViewModel.locationsData.isRemove == true ? View.VISIBLE : View.GONE
                itemViewModelLocationsDataIsRemoveBooleanTrueViewVISIBLEViewGONE = ((itemViewModelLocationsDataIsRemoveBooleanTrue) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
        }
        // batch finished
        if ((dirtyFlags & 0x4L) != 0) {
            // api target 1

            this.icDelete.setOnClickListener(mCallback52);
            this.icEdit.setOnClickListener(mCallback51);
            this.radio.setOnClickListener(mCallback50);
        }
        if ((dirtyFlags & 0x7L) != 0) {
            // api target 1

            this.icDelete.setVisibility(itemViewModelLocationsDataIsRemoveBooleanTrueViewVISIBLEViewGONE);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvFirstLocation, itemViewModelLocationsDataName);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvYourLocation, itemViewModelLocationsDataStreet);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.yourLocation, itemViewModelLocationsDataPhone);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        switch(sourceId) {
            case 3: {
                // localize variables for thread safety
                // itemViewModel
                grand.app.aber_user.pages.myLocations.viewModels.LocationsItemViewModels itemViewModel = mItemViewModel;
                // itemViewModel != null
                boolean itemViewModelJavaLangObjectNull = false;



                itemViewModelJavaLangObjectNull = (itemViewModel) != (null);
                if (itemViewModelJavaLangObjectNull) {




                    itemViewModel.itemAction(grand.app.aber_user.utils.Constants.DELETE);
                }
                break;
            }
            case 1: {
                // localize variables for thread safety
                // itemViewModel
                grand.app.aber_user.pages.myLocations.viewModels.LocationsItemViewModels itemViewModel = mItemViewModel;
                // itemViewModel != null
                boolean itemViewModelJavaLangObjectNull = false;



                itemViewModelJavaLangObjectNull = (itemViewModel) != (null);
                if (itemViewModelJavaLangObjectNull) {




                    itemViewModel.itemAction(grand.app.aber_user.utils.Constants.MENu);
                }
                break;
            }
            case 2: {
                // localize variables for thread safety
                // itemViewModel
                grand.app.aber_user.pages.myLocations.viewModels.LocationsItemViewModels itemViewModel = mItemViewModel;
                // itemViewModel != null
                boolean itemViewModelJavaLangObjectNull = false;



                itemViewModelJavaLangObjectNull = (itemViewModel) != (null);
                if (itemViewModelJavaLangObjectNull) {




                    itemViewModel.itemAction(grand.app.aber_user.utils.Constants.EDIT);
                }
                break;
            }
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): itemViewModel
        flag 1 (0x2L): itemViewModel.locationsData
        flag 2 (0x3L): null
        flag 3 (0x4L): itemViewModel.locationsData.isRemove == true ? View.VISIBLE : View.GONE
        flag 4 (0x5L): itemViewModel.locationsData.isRemove == true ? View.VISIBLE : View.GONE
    flag mapping end*/
    //end
}