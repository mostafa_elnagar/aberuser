package grand.app.aber_user.databinding;
import grand.app.aber_user.R;
import grand.app.aber_user.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentAddPlaceBindingImpl extends FragmentAddPlaceBinding implements grand.app.aber_user.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.curve, 23);
        sViewsWithIds.put(R.id.br, 24);
        sViewsWithIds.put(R.id.input_phone_ad, 25);
    }
    // views
    @NonNull
    private final androidx.core.widget.NestedScrollView mboundView0;
    @NonNull
    private final com.google.android.material.textfield.TextInputEditText mboundView12;
    @NonNull
    private final com.google.android.material.textfield.TextInputEditText mboundView18;
    @NonNull
    private final com.google.android.material.textfield.TextInputEditText mboundView2;
    @NonNull
    private final com.google.android.material.textfield.TextInputEditText mboundView20;
    @NonNull
    private final com.google.android.material.textfield.TextInputEditText mboundView21;
    @NonNull
    private final com.google.android.material.button.MaterialButton mboundView22;
    @NonNull
    private final com.google.android.material.textfield.TextInputEditText mboundView4;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback120;
    @Nullable
    private final android.view.View.OnClickListener mCallback117;
    @Nullable
    private final android.view.View.OnClickListener mCallback118;
    @Nullable
    private final android.view.View.OnClickListener mCallback119;
    // values
    // listeners
    // Inverse Binding Event Handlers
    private androidx.databinding.InverseBindingListener cityandroidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.addLocationRequest.building_no
            //         is viewModel.addLocationRequest.setBuilding_no((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(city);
            // localize variables for thread safety
            // viewModel.addLocationRequest != null
            boolean viewModelAddLocationRequestJavaLangObjectNull = false;
            // viewModel
            grand.app.aber_user.pages.myLocations.viewModels.MyLocationsViewModel viewModel = mViewModel;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;
            // viewModel.addLocationRequest.building_no
            java.lang.String viewModelAddLocationRequestBuildingNo = null;
            // viewModel.addLocationRequest
            grand.app.aber_user.pages.myLocations.models.AddLocationRequest viewModelAddLocationRequest = null;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelAddLocationRequest = viewModel.getAddLocationRequest();

                viewModelAddLocationRequestJavaLangObjectNull = (viewModelAddLocationRequest) != (null);
                if (viewModelAddLocationRequestJavaLangObjectNull) {




                    viewModelAddLocationRequest.setBuilding_no(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener mboundView12androidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.addLocationRequest.street
            //         is viewModel.addLocationRequest.setStreet((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(mboundView12);
            // localize variables for thread safety
            // viewModel.addLocationRequest.street
            java.lang.String viewModelAddLocationRequestStreet = null;
            // viewModel.addLocationRequest != null
            boolean viewModelAddLocationRequestJavaLangObjectNull = false;
            // viewModel
            grand.app.aber_user.pages.myLocations.viewModels.MyLocationsViewModel viewModel = mViewModel;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;
            // viewModel.addLocationRequest
            grand.app.aber_user.pages.myLocations.models.AddLocationRequest viewModelAddLocationRequest = null;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelAddLocationRequest = viewModel.getAddLocationRequest();

                viewModelAddLocationRequestJavaLangObjectNull = (viewModelAddLocationRequest) != (null);
                if (viewModelAddLocationRequestJavaLangObjectNull) {




                    viewModelAddLocationRequest.setStreet(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener mboundView18androidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.addLocationRequest.special_marque
            //         is viewModel.addLocationRequest.setSpecial_marque((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(mboundView18);
            // localize variables for thread safety
            // viewModel.addLocationRequest.special_marque
            java.lang.String viewModelAddLocationRequestSpecialMarque = null;
            // viewModel.addLocationRequest != null
            boolean viewModelAddLocationRequestJavaLangObjectNull = false;
            // viewModel
            grand.app.aber_user.pages.myLocations.viewModels.MyLocationsViewModel viewModel = mViewModel;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;
            // viewModel.addLocationRequest
            grand.app.aber_user.pages.myLocations.models.AddLocationRequest viewModelAddLocationRequest = null;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelAddLocationRequest = viewModel.getAddLocationRequest();

                viewModelAddLocationRequestJavaLangObjectNull = (viewModelAddLocationRequest) != (null);
                if (viewModelAddLocationRequestJavaLangObjectNull) {




                    viewModelAddLocationRequest.setSpecial_marque(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener mboundView2androidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.addLocationRequest.name
            //         is viewModel.addLocationRequest.setName((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(mboundView2);
            // localize variables for thread safety
            // viewModel.addLocationRequest != null
            boolean viewModelAddLocationRequestJavaLangObjectNull = false;
            // viewModel.addLocationRequest.name
            java.lang.String viewModelAddLocationRequestName = null;
            // viewModel
            grand.app.aber_user.pages.myLocations.viewModels.MyLocationsViewModel viewModel = mViewModel;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;
            // viewModel.addLocationRequest
            grand.app.aber_user.pages.myLocations.models.AddLocationRequest viewModelAddLocationRequest = null;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelAddLocationRequest = viewModel.getAddLocationRequest();

                viewModelAddLocationRequestJavaLangObjectNull = (viewModelAddLocationRequest) != (null);
                if (viewModelAddLocationRequestJavaLangObjectNull) {




                    viewModelAddLocationRequest.setName(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener mboundView20androidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.addLocationRequest.phone
            //         is viewModel.addLocationRequest.setPhone((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(mboundView20);
            // localize variables for thread safety
            // viewModel.addLocationRequest != null
            boolean viewModelAddLocationRequestJavaLangObjectNull = false;
            // viewModel.addLocationRequest.phone
            java.lang.String viewModelAddLocationRequestPhone = null;
            // viewModel
            grand.app.aber_user.pages.myLocations.viewModels.MyLocationsViewModel viewModel = mViewModel;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;
            // viewModel.addLocationRequest
            grand.app.aber_user.pages.myLocations.models.AddLocationRequest viewModelAddLocationRequest = null;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelAddLocationRequest = viewModel.getAddLocationRequest();

                viewModelAddLocationRequestJavaLangObjectNull = (viewModelAddLocationRequest) != (null);
                if (viewModelAddLocationRequestJavaLangObjectNull) {




                    viewModelAddLocationRequest.setPhone(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener mboundView21androidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.addLocationRequest.additional_phone
            //         is viewModel.addLocationRequest.setAdditional_phone((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(mboundView21);
            // localize variables for thread safety
            // viewModel.addLocationRequest.additional_phone
            java.lang.String viewModelAddLocationRequestAdditionalPhone = null;
            // viewModel.addLocationRequest != null
            boolean viewModelAddLocationRequestJavaLangObjectNull = false;
            // viewModel
            grand.app.aber_user.pages.myLocations.viewModels.MyLocationsViewModel viewModel = mViewModel;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;
            // viewModel.addLocationRequest
            grand.app.aber_user.pages.myLocations.models.AddLocationRequest viewModelAddLocationRequest = null;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelAddLocationRequest = viewModel.getAddLocationRequest();

                viewModelAddLocationRequestJavaLangObjectNull = (viewModelAddLocationRequest) != (null);
                if (viewModelAddLocationRequestJavaLangObjectNull) {




                    viewModelAddLocationRequest.setAdditional_phone(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener mboundView4androidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.addLocationRequest.last_name
            //         is viewModel.addLocationRequest.setLast_name((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(mboundView4);
            // localize variables for thread safety
            // viewModel.addLocationRequest != null
            boolean viewModelAddLocationRequestJavaLangObjectNull = false;
            // viewModel
            grand.app.aber_user.pages.myLocations.viewModels.MyLocationsViewModel viewModel = mViewModel;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;
            // viewModel.addLocationRequest.last_name
            java.lang.String viewModelAddLocationRequestLastName = null;
            // viewModel.addLocationRequest
            grand.app.aber_user.pages.myLocations.models.AddLocationRequest viewModelAddLocationRequest = null;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelAddLocationRequest = viewModel.getAddLocationRequest();

                viewModelAddLocationRequestJavaLangObjectNull = (viewModelAddLocationRequest) != (null);
                if (viewModelAddLocationRequestJavaLangObjectNull) {




                    viewModelAddLocationRequest.setLast_name(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener regionandroidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.addLocationRequest.floor
            //         is viewModel.addLocationRequest.setFloor((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(region);
            // localize variables for thread safety
            // viewModel.addLocationRequest.floor
            java.lang.String viewModelAddLocationRequestFloor = null;
            // viewModel.addLocationRequest != null
            boolean viewModelAddLocationRequestJavaLangObjectNull = false;
            // viewModel
            grand.app.aber_user.pages.myLocations.viewModels.MyLocationsViewModel viewModel = mViewModel;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;
            // viewModel.addLocationRequest
            grand.app.aber_user.pages.myLocations.models.AddLocationRequest viewModelAddLocationRequest = null;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelAddLocationRequest = viewModel.getAddLocationRequest();

                viewModelAddLocationRequestJavaLangObjectNull = (viewModelAddLocationRequest) != (null);
                if (viewModelAddLocationRequestJavaLangObjectNull) {




                    viewModelAddLocationRequest.setFloor(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };

    public FragmentAddPlaceBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 26, sIncludes, sViewsWithIds));
    }
    private FragmentAddPlaceBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 11
            , (androidx.constraintlayout.widget.Barrier) bindings[24]
            , (com.google.android.material.textfield.TextInputEditText) bindings[14]
            , (com.google.android.material.textfield.TextInputEditText) bindings[6]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[23]
            , (com.google.android.material.textfield.TextInputEditText) bindings[8]
            , (com.google.android.material.textfield.TextInputLayout) bindings[13]
            , (com.google.android.material.textfield.TextInputLayout) bindings[9]
            , (com.google.android.material.textfield.TextInputLayout) bindings[5]
            , (com.google.android.material.textfield.TextInputLayout) bindings[15]
            , (com.google.android.material.textfield.TextInputLayout) bindings[7]
            , (com.google.android.material.textfield.TextInputLayout) bindings[17]
            , (com.google.android.material.textfield.TextInputLayout) bindings[19]
            , (com.google.android.material.textfield.TextInputLayout) bindings[25]
            , (com.google.android.material.textfield.TextInputLayout) bindings[11]
            , (com.google.android.material.textfield.TextInputLayout) bindings[1]
            , (com.google.android.material.textfield.TextInputLayout) bindings[3]
            , (com.google.android.material.textfield.TextInputEditText) bindings[10]
            , (com.google.android.material.textfield.TextInputEditText) bindings[16]
            );
        this.city.setTag(null);
        this.country.setTag(null);
        this.govern.setTag(null);
        this.inputBuildingNumber.setTag(null);
        this.inputCity.setTag(null);
        this.inputCountry.setTag(null);
        this.inputFloor.setTag(null);
        this.inputGovern.setTag(null);
        this.inputMark.setTag(null);
        this.inputPhone.setTag(null);
        this.inputStreet.setTag(null);
        this.inputTitle.setTag(null);
        this.inputTitle2.setTag(null);
        this.location.setTag(null);
        this.mboundView0 = (androidx.core.widget.NestedScrollView) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView12 = (com.google.android.material.textfield.TextInputEditText) bindings[12];
        this.mboundView12.setTag(null);
        this.mboundView18 = (com.google.android.material.textfield.TextInputEditText) bindings[18];
        this.mboundView18.setTag(null);
        this.mboundView2 = (com.google.android.material.textfield.TextInputEditText) bindings[2];
        this.mboundView2.setTag(null);
        this.mboundView20 = (com.google.android.material.textfield.TextInputEditText) bindings[20];
        this.mboundView20.setTag(null);
        this.mboundView21 = (com.google.android.material.textfield.TextInputEditText) bindings[21];
        this.mboundView21.setTag(null);
        this.mboundView22 = (com.google.android.material.button.MaterialButton) bindings[22];
        this.mboundView22.setTag(null);
        this.mboundView4 = (com.google.android.material.textfield.TextInputEditText) bindings[4];
        this.mboundView4.setTag(null);
        this.region.setTag(null);
        setRootTag(root);
        // listeners
        mCallback120 = new grand.app.aber_user.generated.callback.OnClickListener(this, 4);
        mCallback117 = new grand.app.aber_user.generated.callback.OnClickListener(this, 1);
        mCallback118 = new grand.app.aber_user.generated.callback.OnClickListener(this, 2);
        mCallback119 = new grand.app.aber_user.generated.callback.OnClickListener(this, 3);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x1000L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.viewModel == variableId) {
            setViewModel((grand.app.aber_user.pages.myLocations.viewModels.MyLocationsViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setViewModel(@Nullable grand.app.aber_user.pages.myLocations.viewModels.MyLocationsViewModel ViewModel) {
        updateRegistration(10, ViewModel);
        this.mViewModel = ViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x400L;
        }
        notifyPropertyChanged(BR.viewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeViewModelAddLocationRequestCityError((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 1 :
                return onChangeViewModelAddLocationRequestStreetError((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 2 :
                return onChangeViewModelAddLocationRequestLastNameError((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 3 :
                return onChangeViewModelAddLocationRequestPhoneError((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 4 :
                return onChangeViewModelAddLocationRequestFloorError((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 5 :
                return onChangeViewModelAddLocationRequestCountryError((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 6 :
                return onChangeViewModelAddLocationRequestGovernError((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 7 :
                return onChangeViewModelAddLocationRequestBuildingNoError((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 8 :
                return onChangeViewModelAddLocationRequestNameError((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 9 :
                return onChangeViewModelAddLocationRequestSpecialMarqueError((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 10 :
                return onChangeViewModel((grand.app.aber_user.pages.myLocations.viewModels.MyLocationsViewModel) object, fieldId);
        }
        return false;
    }
    private boolean onChangeViewModelAddLocationRequestCityError(androidx.databinding.ObservableField<java.lang.String> ViewModelAddLocationRequestCityError, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelAddLocationRequestStreetError(androidx.databinding.ObservableField<java.lang.String> ViewModelAddLocationRequestStreetError, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelAddLocationRequestLastNameError(androidx.databinding.ObservableField<java.lang.String> ViewModelAddLocationRequestLastNameError, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelAddLocationRequestPhoneError(androidx.databinding.ObservableField<java.lang.String> ViewModelAddLocationRequestPhoneError, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x8L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelAddLocationRequestFloorError(androidx.databinding.ObservableField<java.lang.String> ViewModelAddLocationRequestFloorError, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x10L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelAddLocationRequestCountryError(androidx.databinding.ObservableField<java.lang.String> ViewModelAddLocationRequestCountryError, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x20L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelAddLocationRequestGovernError(androidx.databinding.ObservableField<java.lang.String> ViewModelAddLocationRequestGovernError, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x40L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelAddLocationRequestBuildingNoError(androidx.databinding.ObservableField<java.lang.String> ViewModelAddLocationRequestBuildingNoError, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x80L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelAddLocationRequestNameError(androidx.databinding.ObservableField<java.lang.String> ViewModelAddLocationRequestNameError, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x100L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelAddLocationRequestSpecialMarqueError(androidx.databinding.ObservableField<java.lang.String> ViewModelAddLocationRequestSpecialMarqueError, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x200L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModel(grand.app.aber_user.pages.myLocations.viewModels.MyLocationsViewModel ViewModel, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x400L;
            }
            return true;
        }
        else if (fieldId == BR.addLocationRequest) {
            synchronized(this) {
                    mDirtyFlags |= 0x800L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String viewModelAddLocationRequestGovernErrorGet = null;
        java.lang.String viewModelAddLocationRequestLastNameErrorGet = null;
        java.lang.String viewModelAddLocationRequestBuildingNo = null;
        androidx.databinding.ObservableField<java.lang.String> viewModelAddLocationRequestCityError = null;
        java.lang.String viewModelAddLocationRequestSpecialMarque = null;
        java.lang.String viewModelAddLocationRequestNameErrorGet = null;
        androidx.databinding.ObservableField<java.lang.String> viewModelAddLocationRequestStreetError = null;
        androidx.databinding.ObservableField<java.lang.String> viewModelAddLocationRequestLastNameError = null;
        java.lang.String viewModelAddLocationRequestFloor = null;
        java.lang.String viewModelAddLocationRequestPhoneErrorGet = null;
        java.lang.String viewModelAddLocationRequestStreet = null;
        androidx.databinding.ObservableField<java.lang.String> viewModelAddLocationRequestPhoneError = null;
        java.lang.String viewModelAddLocationRequestCityErrorGet = null;
        java.lang.String viewModelAddLocationRequestLastName = null;
        java.lang.String viewModelAddLocationRequestPhone = null;
        androidx.databinding.ObservableField<java.lang.String> viewModelAddLocationRequestFloorError = null;
        androidx.databinding.ObservableField<java.lang.String> viewModelAddLocationRequestCountryError = null;
        java.lang.String viewModelAddLocationRequestCountryErrorGet = null;
        androidx.databinding.ObservableField<java.lang.String> viewModelAddLocationRequestGovernError = null;
        java.lang.String viewModelAddLocationRequestFloorErrorGet = null;
        grand.app.aber_user.pages.myLocations.models.AddLocationRequest viewModelAddLocationRequest = null;
        java.lang.String viewModelAddLocationRequestSpecialMarqueErrorGet = null;
        androidx.databinding.ObservableField<java.lang.String> viewModelAddLocationRequestBuildingNoError = null;
        androidx.databinding.ObservableField<java.lang.String> viewModelAddLocationRequestNameError = null;
        java.lang.String viewModelAddLocationRequestAdditionalPhone = null;
        java.lang.String viewModelAddLocationRequestStreetErrorGet = null;
        java.lang.String viewModelAddLocationRequestName = null;
        androidx.databinding.ObservableField<java.lang.String> viewModelAddLocationRequestSpecialMarqueError = null;
        java.lang.String viewModelAddLocationRequestBuildingNoErrorGet = null;
        grand.app.aber_user.pages.myLocations.viewModels.MyLocationsViewModel viewModel = mViewModel;

        if ((dirtyFlags & 0x1fffL) != 0) {



                if (viewModel != null) {
                    // read viewModel.addLocationRequest
                    viewModelAddLocationRequest = viewModel.getAddLocationRequest();
                }

            if ((dirtyFlags & 0x1c00L) != 0) {

                    if (viewModelAddLocationRequest != null) {
                        // read viewModel.addLocationRequest.building_no
                        viewModelAddLocationRequestBuildingNo = viewModelAddLocationRequest.getBuilding_no();
                        // read viewModel.addLocationRequest.special_marque
                        viewModelAddLocationRequestSpecialMarque = viewModelAddLocationRequest.getSpecial_marque();
                        // read viewModel.addLocationRequest.floor
                        viewModelAddLocationRequestFloor = viewModelAddLocationRequest.getFloor();
                        // read viewModel.addLocationRequest.street
                        viewModelAddLocationRequestStreet = viewModelAddLocationRequest.getStreet();
                        // read viewModel.addLocationRequest.last_name
                        viewModelAddLocationRequestLastName = viewModelAddLocationRequest.getLast_name();
                        // read viewModel.addLocationRequest.phone
                        viewModelAddLocationRequestPhone = viewModelAddLocationRequest.getPhone();
                        // read viewModel.addLocationRequest.additional_phone
                        viewModelAddLocationRequestAdditionalPhone = viewModelAddLocationRequest.getAdditional_phone();
                        // read viewModel.addLocationRequest.name
                        viewModelAddLocationRequestName = viewModelAddLocationRequest.getName();
                    }
            }
            if ((dirtyFlags & 0x1c01L) != 0) {

                    if (viewModelAddLocationRequest != null) {
                        // read viewModel.addLocationRequest.cityError
                        viewModelAddLocationRequestCityError = viewModelAddLocationRequest.cityError;
                    }
                    updateRegistration(0, viewModelAddLocationRequestCityError);


                    if (viewModelAddLocationRequestCityError != null) {
                        // read viewModel.addLocationRequest.cityError.get()
                        viewModelAddLocationRequestCityErrorGet = viewModelAddLocationRequestCityError.get();
                    }
            }
            if ((dirtyFlags & 0x1c02L) != 0) {

                    if (viewModelAddLocationRequest != null) {
                        // read viewModel.addLocationRequest.streetError
                        viewModelAddLocationRequestStreetError = viewModelAddLocationRequest.streetError;
                    }
                    updateRegistration(1, viewModelAddLocationRequestStreetError);


                    if (viewModelAddLocationRequestStreetError != null) {
                        // read viewModel.addLocationRequest.streetError.get()
                        viewModelAddLocationRequestStreetErrorGet = viewModelAddLocationRequestStreetError.get();
                    }
            }
            if ((dirtyFlags & 0x1c04L) != 0) {

                    if (viewModelAddLocationRequest != null) {
                        // read viewModel.addLocationRequest.lastNameError
                        viewModelAddLocationRequestLastNameError = viewModelAddLocationRequest.lastNameError;
                    }
                    updateRegistration(2, viewModelAddLocationRequestLastNameError);


                    if (viewModelAddLocationRequestLastNameError != null) {
                        // read viewModel.addLocationRequest.lastNameError.get()
                        viewModelAddLocationRequestLastNameErrorGet = viewModelAddLocationRequestLastNameError.get();
                    }
            }
            if ((dirtyFlags & 0x1c08L) != 0) {

                    if (viewModelAddLocationRequest != null) {
                        // read viewModel.addLocationRequest.phoneError
                        viewModelAddLocationRequestPhoneError = viewModelAddLocationRequest.phoneError;
                    }
                    updateRegistration(3, viewModelAddLocationRequestPhoneError);


                    if (viewModelAddLocationRequestPhoneError != null) {
                        // read viewModel.addLocationRequest.phoneError.get()
                        viewModelAddLocationRequestPhoneErrorGet = viewModelAddLocationRequestPhoneError.get();
                    }
            }
            if ((dirtyFlags & 0x1c10L) != 0) {

                    if (viewModelAddLocationRequest != null) {
                        // read viewModel.addLocationRequest.floorError
                        viewModelAddLocationRequestFloorError = viewModelAddLocationRequest.floorError;
                    }
                    updateRegistration(4, viewModelAddLocationRequestFloorError);


                    if (viewModelAddLocationRequestFloorError != null) {
                        // read viewModel.addLocationRequest.floorError.get()
                        viewModelAddLocationRequestFloorErrorGet = viewModelAddLocationRequestFloorError.get();
                    }
            }
            if ((dirtyFlags & 0x1c20L) != 0) {

                    if (viewModelAddLocationRequest != null) {
                        // read viewModel.addLocationRequest.countryError
                        viewModelAddLocationRequestCountryError = viewModelAddLocationRequest.countryError;
                    }
                    updateRegistration(5, viewModelAddLocationRequestCountryError);


                    if (viewModelAddLocationRequestCountryError != null) {
                        // read viewModel.addLocationRequest.countryError.get()
                        viewModelAddLocationRequestCountryErrorGet = viewModelAddLocationRequestCountryError.get();
                    }
            }
            if ((dirtyFlags & 0x1c40L) != 0) {

                    if (viewModelAddLocationRequest != null) {
                        // read viewModel.addLocationRequest.governError
                        viewModelAddLocationRequestGovernError = viewModelAddLocationRequest.governError;
                    }
                    updateRegistration(6, viewModelAddLocationRequestGovernError);


                    if (viewModelAddLocationRequestGovernError != null) {
                        // read viewModel.addLocationRequest.governError.get()
                        viewModelAddLocationRequestGovernErrorGet = viewModelAddLocationRequestGovernError.get();
                    }
            }
            if ((dirtyFlags & 0x1c80L) != 0) {

                    if (viewModelAddLocationRequest != null) {
                        // read viewModel.addLocationRequest.buildingNoError
                        viewModelAddLocationRequestBuildingNoError = viewModelAddLocationRequest.buildingNoError;
                    }
                    updateRegistration(7, viewModelAddLocationRequestBuildingNoError);


                    if (viewModelAddLocationRequestBuildingNoError != null) {
                        // read viewModel.addLocationRequest.buildingNoError.get()
                        viewModelAddLocationRequestBuildingNoErrorGet = viewModelAddLocationRequestBuildingNoError.get();
                    }
            }
            if ((dirtyFlags & 0x1d00L) != 0) {

                    if (viewModelAddLocationRequest != null) {
                        // read viewModel.addLocationRequest.nameError
                        viewModelAddLocationRequestNameError = viewModelAddLocationRequest.nameError;
                    }
                    updateRegistration(8, viewModelAddLocationRequestNameError);


                    if (viewModelAddLocationRequestNameError != null) {
                        // read viewModel.addLocationRequest.nameError.get()
                        viewModelAddLocationRequestNameErrorGet = viewModelAddLocationRequestNameError.get();
                    }
            }
            if ((dirtyFlags & 0x1e00L) != 0) {

                    if (viewModelAddLocationRequest != null) {
                        // read viewModel.addLocationRequest.specialMarqueError
                        viewModelAddLocationRequestSpecialMarqueError = viewModelAddLocationRequest.specialMarqueError;
                    }
                    updateRegistration(9, viewModelAddLocationRequestSpecialMarqueError);


                    if (viewModelAddLocationRequestSpecialMarqueError != null) {
                        // read viewModel.addLocationRequest.specialMarqueError.get()
                        viewModelAddLocationRequestSpecialMarqueErrorGet = viewModelAddLocationRequestSpecialMarqueError.get();
                    }
            }
        }
        // batch finished
        if ((dirtyFlags & 0x1c00L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.city, viewModelAddLocationRequestBuildingNo);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView12, viewModelAddLocationRequestStreet);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView18, viewModelAddLocationRequestSpecialMarque);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView2, viewModelAddLocationRequestName);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView20, viewModelAddLocationRequestPhone);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView21, viewModelAddLocationRequestAdditionalPhone);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView4, viewModelAddLocationRequestLastName);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.region, viewModelAddLocationRequestFloor);
        }
        if ((dirtyFlags & 0x1000L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.city, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, cityandroidTextAttrChanged);
            this.country.setOnClickListener(mCallback117);
            this.govern.setOnClickListener(mCallback118);
            this.location.setOnClickListener(mCallback119);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.mboundView12, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, mboundView12androidTextAttrChanged);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.mboundView18, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, mboundView18androidTextAttrChanged);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.mboundView2, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, mboundView2androidTextAttrChanged);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.mboundView20, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, mboundView20androidTextAttrChanged);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.mboundView21, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, mboundView21androidTextAttrChanged);
            this.mboundView22.setOnClickListener(mCallback120);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.mboundView4, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, mboundView4androidTextAttrChanged);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.region, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, regionandroidTextAttrChanged);
        }
        if ((dirtyFlags & 0x1c80L) != 0) {
            // api target 1

            this.inputBuildingNumber.setError(viewModelAddLocationRequestBuildingNoErrorGet);
        }
        if ((dirtyFlags & 0x1c01L) != 0) {
            // api target 1

            this.inputCity.setError(viewModelAddLocationRequestCityErrorGet);
        }
        if ((dirtyFlags & 0x1c20L) != 0) {
            // api target 1

            this.inputCountry.setError(viewModelAddLocationRequestCountryErrorGet);
        }
        if ((dirtyFlags & 0x1c10L) != 0) {
            // api target 1

            this.inputFloor.setError(viewModelAddLocationRequestFloorErrorGet);
        }
        if ((dirtyFlags & 0x1c40L) != 0) {
            // api target 1

            this.inputGovern.setError(viewModelAddLocationRequestGovernErrorGet);
        }
        if ((dirtyFlags & 0x1e00L) != 0) {
            // api target 1

            this.inputMark.setError(viewModelAddLocationRequestSpecialMarqueErrorGet);
        }
        if ((dirtyFlags & 0x1c08L) != 0) {
            // api target 1

            this.inputPhone.setError(viewModelAddLocationRequestPhoneErrorGet);
        }
        if ((dirtyFlags & 0x1c02L) != 0) {
            // api target 1

            this.inputStreet.setError(viewModelAddLocationRequestStreetErrorGet);
        }
        if ((dirtyFlags & 0x1d00L) != 0) {
            // api target 1

            this.inputTitle.setError(viewModelAddLocationRequestNameErrorGet);
        }
        if ((dirtyFlags & 0x1c04L) != 0) {
            // api target 1

            this.inputTitle2.setError(viewModelAddLocationRequestLastNameErrorGet);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        switch(sourceId) {
            case 4: {
                // localize variables for thread safety
                // viewModel
                grand.app.aber_user.pages.myLocations.viewModels.MyLocationsViewModel viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {


                    viewModel.addPlace();
                }
                break;
            }
            case 1: {
                // localize variables for thread safety
                // viewModel
                grand.app.aber_user.pages.myLocations.viewModels.MyLocationsViewModel viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {




                    viewModel.inputAction(grand.app.aber_user.utils.Constants.SHOW_COUNTRY);
                }
                break;
            }
            case 2: {
                // localize variables for thread safety
                // viewModel
                grand.app.aber_user.pages.myLocations.viewModels.MyLocationsViewModel viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {




                    viewModel.inputAction(grand.app.aber_user.utils.Constants.GOVERN);
                }
                break;
            }
            case 3: {
                // localize variables for thread safety
                // viewModel
                grand.app.aber_user.pages.myLocations.viewModels.MyLocationsViewModel viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {




                    viewModel.inputAction(grand.app.aber_user.utils.Constants.CITY);
                }
                break;
            }
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): viewModel.addLocationRequest.cityError
        flag 1 (0x2L): viewModel.addLocationRequest.streetError
        flag 2 (0x3L): viewModel.addLocationRequest.lastNameError
        flag 3 (0x4L): viewModel.addLocationRequest.phoneError
        flag 4 (0x5L): viewModel.addLocationRequest.floorError
        flag 5 (0x6L): viewModel.addLocationRequest.countryError
        flag 6 (0x7L): viewModel.addLocationRequest.governError
        flag 7 (0x8L): viewModel.addLocationRequest.buildingNoError
        flag 8 (0x9L): viewModel.addLocationRequest.nameError
        flag 9 (0xaL): viewModel.addLocationRequest.specialMarqueError
        flag 10 (0xbL): viewModel
        flag 11 (0xcL): viewModel.addLocationRequest
        flag 12 (0xdL): null
    flag mapping end*/
    //end
}