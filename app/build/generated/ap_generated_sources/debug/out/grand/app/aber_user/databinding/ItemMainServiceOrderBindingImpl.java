package grand.app.aber_user.databinding;
import grand.app.aber_user.R;
import grand.app.aber_user.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ItemMainServiceOrderBindingImpl extends ItemMainServiceOrderBinding implements grand.app.aber_user.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.tv_services_name, 10);
        sViewsWithIds.put(R.id.v_services_name, 11);
        sViewsWithIds.put(R.id.tv_service_location, 12);
        sViewsWithIds.put(R.id.v_service_location, 13);
        sViewsWithIds.put(R.id.tv_service_time, 14);
        sViewsWithIds.put(R.id.v_service_status, 15);
        sViewsWithIds.put(R.id.tv_service_status, 16);
    }
    // views
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback219;
    @Nullable
    private final android.view.View.OnClickListener mCallback220;
    @Nullable
    private final android.view.View.OnClickListener mCallback218;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ItemMainServiceOrderBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 17, sIncludes, sViewsWithIds));
    }
    private ItemMainServiceOrderBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 1
            , (androidx.cardview.widget.CardView) bindings[0]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[9]
            , (androidx.recyclerview.widget.RecyclerView) bindings[7]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[8]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[12]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[2]
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[6]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[16]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[4]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[14]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[3]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[10]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[1]
            , (android.view.View) bindings[5]
            , (android.view.View) bindings[13]
            , (android.view.View) bindings[15]
            , (android.view.View) bindings[11]
            );
        this.cardInfo.setTag(null);
        this.icServiceDetalis.setTag(null);
        this.rcServices.setTag(null);
        this.tvServiceDetails.setTag(null);
        this.tvServiceLocationValue.setTag(null);
        this.tvServiceRc.setTag(null);
        this.tvServiceStatusValue.setTag(null);
        this.tvServiceTimeValue.setTag(null);
        this.tvServicesNameValue.setTag(null);
        this.vService.setTag(null);
        setRootTag(root);
        // listeners
        mCallback219 = new grand.app.aber_user.generated.callback.OnClickListener(this, 2);
        mCallback220 = new grand.app.aber_user.generated.callback.OnClickListener(this, 3);
        mCallback218 = new grand.app.aber_user.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x8L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.itemOrderViewModel == variableId) {
            setItemOrderViewModel((grand.app.aber_user.pages.myOrders.viewModels.ItemMyOrderServiceViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setItemOrderViewModel(@Nullable grand.app.aber_user.pages.myOrders.viewModels.ItemMyOrderServiceViewModel ItemOrderViewModel) {
        updateRegistration(0, ItemOrderViewModel);
        this.mItemOrderViewModel = ItemOrderViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.itemOrderViewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeItemOrderViewModel((grand.app.aber_user.pages.myOrders.viewModels.ItemMyOrderServiceViewModel) object, fieldId);
        }
        return false;
    }
    private boolean onChangeItemOrderViewModel(grand.app.aber_user.pages.myOrders.viewModels.ItemMyOrderServiceViewModel ItemOrderViewModel, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        else if (fieldId == BR.productsItem) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        else if (fieldId == BR.servicesRequiredAdapter) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String itemOrderViewModelProductsItemIsEmergencyInt1TvServiceTimeValueAndroidStringEmergencyServicesItemOrderViewModelProductsItemScheduledAt = null;
        grand.app.aber_user.pages.myOrders.viewModels.ItemMyOrderServiceViewModel itemOrderViewModel = mItemOrderViewModel;
        int itemOrderViewModelServicesRequiredAdapterItemCount = 0;
        java.lang.String itemOrderViewModelProductsItemServiceName = null;
        int itemOrderViewModelServicesRequiredAdapterItemCountInt0ViewGONEViewVISIBLE = 0;
        int itemOrderViewModelProductsItemIsEmergency = 0;
        java.lang.String itemOrderViewModelStatus = null;
        boolean itemOrderViewModelProductsItemIsEmergencyInt1 = false;
        boolean itemOrderViewModelServicesRequiredAdapterItemCountInt0 = false;
        java.lang.String itemOrderViewModelProductsItemScheduledAt = null;
        java.lang.String itemOrderViewModelProductsItemAddress = null;
        grand.app.aber_user.pages.myOrders.adapter.ServicesRequiredAdapter itemOrderViewModelServicesRequiredAdapter = null;
        grand.app.aber_user.pages.myOrders.models.orderServices.ServicesOrderDetails itemOrderViewModelProductsItem = null;

        if ((dirtyFlags & 0xfL) != 0) {


            if ((dirtyFlags & 0x9L) != 0) {

                    if (itemOrderViewModel != null) {
                        // read itemOrderViewModel.status
                        itemOrderViewModelStatus = itemOrderViewModel.status;
                    }
            }
            if ((dirtyFlags & 0xdL) != 0) {

                    if (itemOrderViewModel != null) {
                        // read itemOrderViewModel.servicesRequiredAdapter
                        itemOrderViewModelServicesRequiredAdapter = itemOrderViewModel.getServicesRequiredAdapter();
                    }


                    if (itemOrderViewModelServicesRequiredAdapter != null) {
                        // read itemOrderViewModel.servicesRequiredAdapter.itemCount
                        itemOrderViewModelServicesRequiredAdapterItemCount = itemOrderViewModelServicesRequiredAdapter.getItemCount();
                    }


                    // read itemOrderViewModel.servicesRequiredAdapter.itemCount == 0
                    itemOrderViewModelServicesRequiredAdapterItemCountInt0 = (itemOrderViewModelServicesRequiredAdapterItemCount) == (0);
                if((dirtyFlags & 0xdL) != 0) {
                    if(itemOrderViewModelServicesRequiredAdapterItemCountInt0) {
                            dirtyFlags |= 0x80L;
                    }
                    else {
                            dirtyFlags |= 0x40L;
                    }
                }


                    // read itemOrderViewModel.servicesRequiredAdapter.itemCount == 0 ? View.GONE : View.VISIBLE
                    itemOrderViewModelServicesRequiredAdapterItemCountInt0ViewGONEViewVISIBLE = ((itemOrderViewModelServicesRequiredAdapterItemCountInt0) ? (android.view.View.GONE) : (android.view.View.VISIBLE));
            }
            if ((dirtyFlags & 0xbL) != 0) {

                    if (itemOrderViewModel != null) {
                        // read itemOrderViewModel.productsItem
                        itemOrderViewModelProductsItem = itemOrderViewModel.getProductsItem();
                    }


                    if (itemOrderViewModelProductsItem != null) {
                        // read itemOrderViewModel.productsItem.serviceName
                        itemOrderViewModelProductsItemServiceName = itemOrderViewModelProductsItem.getServiceName();
                        // read itemOrderViewModel.productsItem.isEmergency
                        itemOrderViewModelProductsItemIsEmergency = itemOrderViewModelProductsItem.getIsEmergency();
                        // read itemOrderViewModel.productsItem.address
                        itemOrderViewModelProductsItemAddress = itemOrderViewModelProductsItem.getAddress();
                    }


                    // read itemOrderViewModel.productsItem.isEmergency == 1
                    itemOrderViewModelProductsItemIsEmergencyInt1 = (itemOrderViewModelProductsItemIsEmergency) == (1);
                if((dirtyFlags & 0xbL) != 0) {
                    if(itemOrderViewModelProductsItemIsEmergencyInt1) {
                            dirtyFlags |= 0x20L;
                    }
                    else {
                            dirtyFlags |= 0x10L;
                    }
                }
            }
        }
        // batch finished

        if ((dirtyFlags & 0x10L) != 0) {

                if (itemOrderViewModelProductsItem != null) {
                    // read itemOrderViewModel.productsItem.scheduledAt
                    itemOrderViewModelProductsItemScheduledAt = itemOrderViewModelProductsItem.getScheduledAt();
                }
        }

        if ((dirtyFlags & 0xbL) != 0) {

                // read itemOrderViewModel.productsItem.isEmergency == 1 ? @android:string/emergency_services : itemOrderViewModel.productsItem.scheduledAt
                itemOrderViewModelProductsItemIsEmergencyInt1TvServiceTimeValueAndroidStringEmergencyServicesItemOrderViewModelProductsItemScheduledAt = ((itemOrderViewModelProductsItemIsEmergencyInt1) ? (tvServiceTimeValue.getResources().getString(R.string.emergency_services)) : (itemOrderViewModelProductsItemScheduledAt));
        }
        // batch finished
        if ((dirtyFlags & 0x8L) != 0) {
            // api target 1

            this.cardInfo.setOnClickListener(mCallback218);
            this.icServiceDetalis.setOnClickListener(mCallback220);
            this.tvServiceDetails.setOnClickListener(mCallback219);
        }
        if ((dirtyFlags & 0xdL) != 0) {
            // api target 1

            grand.app.aber_user.base.ApplicationBinding.getItemsV2Binding(this.rcServices, itemOrderViewModelServicesRequiredAdapter, "1", "1");
            this.tvServiceRc.setVisibility(itemOrderViewModelServicesRequiredAdapterItemCountInt0ViewGONEViewVISIBLE);
            this.vService.setVisibility(itemOrderViewModelServicesRequiredAdapterItemCountInt0ViewGONEViewVISIBLE);
        }
        if ((dirtyFlags & 0xbL) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvServiceLocationValue, itemOrderViewModelProductsItemAddress);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvServiceTimeValue, itemOrderViewModelProductsItemIsEmergencyInt1TvServiceTimeValueAndroidStringEmergencyServicesItemOrderViewModelProductsItemScheduledAt);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvServicesNameValue, itemOrderViewModelProductsItemServiceName);
        }
        if ((dirtyFlags & 0x9L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvServiceStatusValue, itemOrderViewModelStatus);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        switch(sourceId) {
            case 2: {
                // localize variables for thread safety
                // itemOrderViewModel
                grand.app.aber_user.pages.myOrders.viewModels.ItemMyOrderServiceViewModel itemOrderViewModel = mItemOrderViewModel;
                // itemOrderViewModel != null
                boolean itemOrderViewModelJavaLangObjectNull = false;



                itemOrderViewModelJavaLangObjectNull = (itemOrderViewModel) != (null);
                if (itemOrderViewModelJavaLangObjectNull) {


                    itemOrderViewModel.itemAction();
                }
                break;
            }
            case 3: {
                // localize variables for thread safety
                // itemOrderViewModel
                grand.app.aber_user.pages.myOrders.viewModels.ItemMyOrderServiceViewModel itemOrderViewModel = mItemOrderViewModel;
                // itemOrderViewModel != null
                boolean itemOrderViewModelJavaLangObjectNull = false;



                itemOrderViewModelJavaLangObjectNull = (itemOrderViewModel) != (null);
                if (itemOrderViewModelJavaLangObjectNull) {


                    itemOrderViewModel.itemAction();
                }
                break;
            }
            case 1: {
                // localize variables for thread safety
                // itemOrderViewModel
                grand.app.aber_user.pages.myOrders.viewModels.ItemMyOrderServiceViewModel itemOrderViewModel = mItemOrderViewModel;
                // itemOrderViewModel != null
                boolean itemOrderViewModelJavaLangObjectNull = false;



                itemOrderViewModelJavaLangObjectNull = (itemOrderViewModel) != (null);
                if (itemOrderViewModelJavaLangObjectNull) {


                    itemOrderViewModel.itemAction();
                }
                break;
            }
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): itemOrderViewModel
        flag 1 (0x2L): itemOrderViewModel.productsItem
        flag 2 (0x3L): itemOrderViewModel.servicesRequiredAdapter
        flag 3 (0x4L): null
        flag 4 (0x5L): itemOrderViewModel.productsItem.isEmergency == 1 ? @android:string/emergency_services : itemOrderViewModel.productsItem.scheduledAt
        flag 5 (0x6L): itemOrderViewModel.productsItem.isEmergency == 1 ? @android:string/emergency_services : itemOrderViewModel.productsItem.scheduledAt
        flag 6 (0x7L): itemOrderViewModel.servicesRequiredAdapter.itemCount == 0 ? View.GONE : View.VISIBLE
        flag 7 (0x8L): itemOrderViewModel.servicesRequiredAdapter.itemCount == 0 ? View.GONE : View.VISIBLE
    flag mapping end*/
    //end
}