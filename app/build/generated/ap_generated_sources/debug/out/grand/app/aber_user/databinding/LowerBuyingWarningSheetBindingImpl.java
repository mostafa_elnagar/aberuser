package grand.app.aber_user.databinding;
import grand.app.aber_user.R;
import grand.app.aber_user.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class LowerBuyingWarningSheetBindingImpl extends LowerBuyingWarningSheetBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.tv_login_warning, 2);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public LowerBuyingWarningSheetBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 3, sIncludes, sViewsWithIds));
    }
    private LowerBuyingWarningSheetBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 1
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[2]
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[1]
            );
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.tvWarningBody.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.viewmodel == variableId) {
            setViewmodel((grand.app.aber_user.pages.cart.viewModels.CartViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setViewmodel(@Nullable grand.app.aber_user.pages.cart.viewModels.CartViewModel Viewmodel) {
        updateRegistration(0, Viewmodel);
        this.mViewmodel = Viewmodel;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.viewmodel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeViewmodel((grand.app.aber_user.pages.cart.viewModels.CartViewModel) object, fieldId);
        }
        return false;
    }
    private boolean onChangeViewmodel(grand.app.aber_user.pages.cart.viewModels.CartViewModel Viewmodel, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String tvWarningBodyAndroidStringBuyingWarningBodyConcatJavaLangStringConcatUserHelperGetInstanceContextBuyingWarningConcatJavaLangStringConcatViewmodelCurrency = null;
        java.lang.String tvWarningBodyAndroidStringBuyingWarningBodyConcatJavaLangStringConcatUserHelperGetInstanceContextBuyingWarningConcatJavaLangString = null;
        java.lang.String viewmodelCurrency = null;
        java.lang.String userHelperGetInstanceContextBuyingWarning = null;
        java.lang.String tvWarningBodyAndroidStringBuyingWarningBodyConcatJavaLangStringConcatUserHelperGetInstanceContextBuyingWarning = null;
        grand.app.aber_user.pages.cart.viewModels.CartViewModel viewmodel = mViewmodel;

        if ((dirtyFlags & 0x3L) != 0) {

                // read UserHelper.getInstance(context).buyingWarning
                userHelperGetInstanceContextBuyingWarning = grand.app.aber_user.utils.session.UserHelper.getInstance(getRoot().getContext()).getBuyingWarning();
                updateRegistration(0, viewmodel);


                // read @android:string/buying_warning_body.concat(" ").concat(UserHelper.getInstance(context).buyingWarning)
                tvWarningBodyAndroidStringBuyingWarningBodyConcatJavaLangStringConcatUserHelperGetInstanceContextBuyingWarning = tvWarningBody.getResources().getString(R.string.buying_warning_body).concat(" ").concat(userHelperGetInstanceContextBuyingWarning);
                if (viewmodel != null) {
                    // read viewmodel.currency
                    viewmodelCurrency = viewmodel.currency;
                }


                if (tvWarningBodyAndroidStringBuyingWarningBodyConcatJavaLangStringConcatUserHelperGetInstanceContextBuyingWarning != null) {
                    // read @android:string/buying_warning_body.concat(" ").concat(UserHelper.getInstance(context).buyingWarning).concat(" ")
                    tvWarningBodyAndroidStringBuyingWarningBodyConcatJavaLangStringConcatUserHelperGetInstanceContextBuyingWarningConcatJavaLangString = tvWarningBodyAndroidStringBuyingWarningBodyConcatJavaLangStringConcatUserHelperGetInstanceContextBuyingWarning.concat(" ");
                }


                if (tvWarningBodyAndroidStringBuyingWarningBodyConcatJavaLangStringConcatUserHelperGetInstanceContextBuyingWarningConcatJavaLangString != null) {
                    // read @android:string/buying_warning_body.concat(" ").concat(UserHelper.getInstance(context).buyingWarning).concat(" ").concat(viewmodel.currency)
                    tvWarningBodyAndroidStringBuyingWarningBodyConcatJavaLangStringConcatUserHelperGetInstanceContextBuyingWarningConcatJavaLangStringConcatViewmodelCurrency = tvWarningBodyAndroidStringBuyingWarningBodyConcatJavaLangStringConcatUserHelperGetInstanceContextBuyingWarningConcatJavaLangString.concat(viewmodelCurrency);
                }
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvWarningBody, tvWarningBodyAndroidStringBuyingWarningBodyConcatJavaLangStringConcatUserHelperGetInstanceContextBuyingWarningConcatJavaLangStringConcatViewmodelCurrency);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): viewmodel
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}