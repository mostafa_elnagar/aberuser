package grand.app.aber_user;

import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.DataBinderMapper;
import androidx.databinding.DataBindingComponent;
import androidx.databinding.ViewDataBinding;
import grand.app.aber_user.databinding.ActivityBaseBindingImpl;
import grand.app.aber_user.databinding.ActivityMainBindingImpl;
import grand.app.aber_user.databinding.ActivityMapAddressBindingImpl;
import grand.app.aber_user.databinding.CancelWarningLayoutBindingImpl;
import grand.app.aber_user.databinding.ExistLayoutBindingImpl;
import grand.app.aber_user.databinding.FragmentAberBoxBindingImpl;
import grand.app.aber_user.databinding.FragmentAboutBindingImpl;
import grand.app.aber_user.databinding.FragmentAddPlaceBindingImpl;
import grand.app.aber_user.databinding.FragmentAppWalletBindingImpl;
import grand.app.aber_user.databinding.FragmentBatteriesBindingImpl;
import grand.app.aber_user.databinding.FragmentCarCheckBindingImpl;
import grand.app.aber_user.databinding.FragmentCartBindingImpl;
import grand.app.aber_user.databinding.FragmentChangePasswordBindingImpl;
import grand.app.aber_user.databinding.FragmentChatBindingImpl;
import grand.app.aber_user.databinding.FragmentChooseServiceTimeBindingImpl;
import grand.app.aber_user.databinding.FragmentConfirmCartBindingImpl;
import grand.app.aber_user.databinding.FragmentConfirmCodeBindingImpl;
import grand.app.aber_user.databinding.FragmentConfirmOrderBindingImpl;
import grand.app.aber_user.databinding.FragmentContactBindingImpl;
import grand.app.aber_user.databinding.FragmentConversationsBindingImpl;
import grand.app.aber_user.databinding.FragmentCountriesBindingImpl;
import grand.app.aber_user.databinding.FragmentEditProfileBindingImpl;
import grand.app.aber_user.databinding.FragmentFavoritesBindingImpl;
import grand.app.aber_user.databinding.FragmentFilterBindingImpl;
import grand.app.aber_user.databinding.FragmentFollowUpOrdersBindingImpl;
import grand.app.aber_user.databinding.FragmentForgetPasswordBindingImpl;
import grand.app.aber_user.databinding.FragmentFuelBindingImpl;
import grand.app.aber_user.databinding.FragmentHiddenBindingImpl;
import grand.app.aber_user.databinding.FragmentHomeBindingImpl;
import grand.app.aber_user.databinding.FragmentLangBindingImpl;
import grand.app.aber_user.databinding.FragmentLoginBindingImpl;
import grand.app.aber_user.databinding.FragmentMyAccountSettingsBindingImpl;
import grand.app.aber_user.databinding.FragmentMyLocationBindingImpl;
import grand.app.aber_user.databinding.FragmentMyOrderDetailsBindingImpl;
import grand.app.aber_user.databinding.FragmentMyOrdersBindingImpl;
import grand.app.aber_user.databinding.FragmentNotificationsBindingImpl;
import grand.app.aber_user.databinding.FragmentOilBindingImpl;
import grand.app.aber_user.databinding.FragmentOnboardBindingImpl;
import grand.app.aber_user.databinding.FragmentOpenCarBindingImpl;
import grand.app.aber_user.databinding.FragmentOrderServiceDetailsBindingImpl;
import grand.app.aber_user.databinding.FragmentPartServicesBindingImpl;
import grand.app.aber_user.databinding.FragmentPartsBindingImpl;
import grand.app.aber_user.databinding.FragmentPaymentBindingImpl;
import grand.app.aber_user.databinding.FragmentProductDetailsBindingImpl;
import grand.app.aber_user.databinding.FragmentRegisterBindingImpl;
import grand.app.aber_user.databinding.FragmentReviewsBindingImpl;
import grand.app.aber_user.databinding.FragmentSearchBindingImpl;
import grand.app.aber_user.databinding.FragmentServicesOrdersBindingImpl;
import grand.app.aber_user.databinding.FragmentSocialMediaBindingImpl;
import grand.app.aber_user.databinding.FragmentSplashBindingImpl;
import grand.app.aber_user.databinding.FragmentTermsBindingImpl;
import grand.app.aber_user.databinding.FragmentTiersBindingImpl;
import grand.app.aber_user.databinding.FragmentWashBindingImpl;
import grand.app.aber_user.databinding.FragmentWaterBallonBindingImpl;
import grand.app.aber_user.databinding.FragmentWinchBindingImpl;
import grand.app.aber_user.databinding.ItemCarBindingImpl;
import grand.app.aber_user.databinding.ItemCartBindingImpl;
import grand.app.aber_user.databinding.ItemCategoryBindingImpl;
import grand.app.aber_user.databinding.ItemChatBindingImpl;
import grand.app.aber_user.databinding.ItemColorBindingImpl;
import grand.app.aber_user.databinding.ItemContactBindingImpl;
import grand.app.aber_user.databinding.ItemConversationBindingImpl;
import grand.app.aber_user.databinding.ItemCountryBindingImpl;
import grand.app.aber_user.databinding.ItemExtraConfirmServicesBindingImpl;
import grand.app.aber_user.databinding.ItemExtraServicesBindingImpl;
import grand.app.aber_user.databinding.ItemFavoriteBindingImpl;
import grand.app.aber_user.databinding.ItemMainMyOrderBindingImpl;
import grand.app.aber_user.databinding.ItemMainServiceOrderBindingImpl;
import grand.app.aber_user.databinding.ItemMyLocationBindingImpl;
import grand.app.aber_user.databinding.ItemOilPricesListBindingImpl;
import grand.app.aber_user.databinding.ItemOrderConfirmedBindingImpl;
import grand.app.aber_user.databinding.ItemOrderDetailBindingImpl;
import grand.app.aber_user.databinding.ItemOrderServiceBindingImpl;
import grand.app.aber_user.databinding.ItemPartBindingImpl;
import grand.app.aber_user.databinding.ItemPartServiceBindingImpl;
import grand.app.aber_user.databinding.ItemRequiredServiceBindingImpl;
import grand.app.aber_user.databinding.ItemReviewBindingImpl;
import grand.app.aber_user.databinding.ItemSaleDescBindingImpl;
import grand.app.aber_user.databinding.ItemSearchBindingImpl;
import grand.app.aber_user.databinding.ItemServiceBindingImpl;
import grand.app.aber_user.databinding.ItemServiceMenuBindingImpl;
import grand.app.aber_user.databinding.ItemSizeBindingImpl;
import grand.app.aber_user.databinding.ItemSocialBindingImpl;
import grand.app.aber_user.databinding.ItemSocialMenuBindingImpl;
import grand.app.aber_user.databinding.ItemWalletHistoryBindingImpl;
import grand.app.aber_user.databinding.LayoutActionBarBackBindingImpl;
import grand.app.aber_user.databinding.LayoutActionBarHomeBindingImpl;
import grand.app.aber_user.databinding.LayoutGrandBindingImpl;
import grand.app.aber_user.databinding.LoginWarningSheetBindingImpl;
import grand.app.aber_user.databinding.LowerBuyingWarningSheetBindingImpl;
import grand.app.aber_user.databinding.MenuBindingImpl;
import grand.app.aber_user.databinding.NotifyItemBindingImpl;
import grand.app.aber_user.databinding.OptionDialogBindingImpl;
import grand.app.aber_user.databinding.OrderSuccessDialogBindingImpl;
import grand.app.aber_user.databinding.PricesListBindingImpl;
import grand.app.aber_user.databinding.ProductSortBindingImpl;
import grand.app.aber_user.databinding.RateDialogBindingImpl;
import grand.app.aber_user.databinding.ReviewDialogBindingImpl;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Object;
import java.lang.Override;
import java.lang.RuntimeException;
import java.lang.String;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DataBinderMapperImpl extends DataBinderMapper {
  private static final int LAYOUT_ACTIVITYBASE = 1;

  private static final int LAYOUT_ACTIVITYMAIN = 2;

  private static final int LAYOUT_ACTIVITYMAPADDRESS = 3;

  private static final int LAYOUT_CANCELWARNINGLAYOUT = 4;

  private static final int LAYOUT_EXISTLAYOUT = 5;

  private static final int LAYOUT_FRAGMENTABERBOX = 6;

  private static final int LAYOUT_FRAGMENTABOUT = 7;

  private static final int LAYOUT_FRAGMENTADDPLACE = 8;

  private static final int LAYOUT_FRAGMENTAPPWALLET = 9;

  private static final int LAYOUT_FRAGMENTBATTERIES = 10;

  private static final int LAYOUT_FRAGMENTCARCHECK = 11;

  private static final int LAYOUT_FRAGMENTCART = 12;

  private static final int LAYOUT_FRAGMENTCHANGEPASSWORD = 13;

  private static final int LAYOUT_FRAGMENTCHAT = 14;

  private static final int LAYOUT_FRAGMENTCHOOSESERVICETIME = 15;

  private static final int LAYOUT_FRAGMENTCONFIRMCART = 16;

  private static final int LAYOUT_FRAGMENTCONFIRMCODE = 17;

  private static final int LAYOUT_FRAGMENTCONFIRMORDER = 18;

  private static final int LAYOUT_FRAGMENTCONTACT = 19;

  private static final int LAYOUT_FRAGMENTCONVERSATIONS = 20;

  private static final int LAYOUT_FRAGMENTCOUNTRIES = 21;

  private static final int LAYOUT_FRAGMENTEDITPROFILE = 22;

  private static final int LAYOUT_FRAGMENTFAVORITES = 23;

  private static final int LAYOUT_FRAGMENTFILTER = 24;

  private static final int LAYOUT_FRAGMENTFOLLOWUPORDERS = 25;

  private static final int LAYOUT_FRAGMENTFORGETPASSWORD = 26;

  private static final int LAYOUT_FRAGMENTFUEL = 27;

  private static final int LAYOUT_FRAGMENTHIDDEN = 28;

  private static final int LAYOUT_FRAGMENTHOME = 29;

  private static final int LAYOUT_FRAGMENTLANG = 30;

  private static final int LAYOUT_FRAGMENTLOGIN = 31;

  private static final int LAYOUT_FRAGMENTMYACCOUNTSETTINGS = 32;

  private static final int LAYOUT_FRAGMENTMYLOCATION = 33;

  private static final int LAYOUT_FRAGMENTMYORDERDETAILS = 34;

  private static final int LAYOUT_FRAGMENTMYORDERS = 35;

  private static final int LAYOUT_FRAGMENTNOTIFICATIONS = 36;

  private static final int LAYOUT_FRAGMENTOIL = 37;

  private static final int LAYOUT_FRAGMENTONBOARD = 38;

  private static final int LAYOUT_FRAGMENTOPENCAR = 39;

  private static final int LAYOUT_FRAGMENTORDERSERVICEDETAILS = 40;

  private static final int LAYOUT_FRAGMENTPARTSERVICES = 41;

  private static final int LAYOUT_FRAGMENTPARTS = 42;

  private static final int LAYOUT_FRAGMENTPAYMENT = 43;

  private static final int LAYOUT_FRAGMENTPRODUCTDETAILS = 44;

  private static final int LAYOUT_FRAGMENTREGISTER = 45;

  private static final int LAYOUT_FRAGMENTREVIEWS = 46;

  private static final int LAYOUT_FRAGMENTSEARCH = 47;

  private static final int LAYOUT_FRAGMENTSERVICESORDERS = 48;

  private static final int LAYOUT_FRAGMENTSOCIALMEDIA = 49;

  private static final int LAYOUT_FRAGMENTSPLASH = 50;

  private static final int LAYOUT_FRAGMENTTERMS = 51;

  private static final int LAYOUT_FRAGMENTTIERS = 52;

  private static final int LAYOUT_FRAGMENTWASH = 53;

  private static final int LAYOUT_FRAGMENTWATERBALLON = 54;

  private static final int LAYOUT_FRAGMENTWINCH = 55;

  private static final int LAYOUT_ITEMCAR = 56;

  private static final int LAYOUT_ITEMCART = 57;

  private static final int LAYOUT_ITEMCATEGORY = 58;

  private static final int LAYOUT_ITEMCHAT = 59;

  private static final int LAYOUT_ITEMCOLOR = 60;

  private static final int LAYOUT_ITEMCONTACT = 61;

  private static final int LAYOUT_ITEMCONVERSATION = 62;

  private static final int LAYOUT_ITEMCOUNTRY = 63;

  private static final int LAYOUT_ITEMEXTRACONFIRMSERVICES = 64;

  private static final int LAYOUT_ITEMEXTRASERVICES = 65;

  private static final int LAYOUT_ITEMFAVORITE = 66;

  private static final int LAYOUT_ITEMMAINMYORDER = 67;

  private static final int LAYOUT_ITEMMAINSERVICEORDER = 68;

  private static final int LAYOUT_ITEMMYLOCATION = 69;

  private static final int LAYOUT_ITEMOILPRICESLIST = 70;

  private static final int LAYOUT_ITEMORDERCONFIRMED = 71;

  private static final int LAYOUT_ITEMORDERDETAIL = 72;

  private static final int LAYOUT_ITEMORDERSERVICE = 73;

  private static final int LAYOUT_ITEMPART = 74;

  private static final int LAYOUT_ITEMPARTSERVICE = 75;

  private static final int LAYOUT_ITEMREQUIREDSERVICE = 76;

  private static final int LAYOUT_ITEMREVIEW = 77;

  private static final int LAYOUT_ITEMSALEDESC = 78;

  private static final int LAYOUT_ITEMSEARCH = 79;

  private static final int LAYOUT_ITEMSERVICE = 80;

  private static final int LAYOUT_ITEMSERVICEMENU = 81;

  private static final int LAYOUT_ITEMSIZE = 82;

  private static final int LAYOUT_ITEMSOCIAL = 83;

  private static final int LAYOUT_ITEMSOCIALMENU = 84;

  private static final int LAYOUT_ITEMWALLETHISTORY = 85;

  private static final int LAYOUT_LAYOUTACTIONBARBACK = 86;

  private static final int LAYOUT_LAYOUTACTIONBARHOME = 87;

  private static final int LAYOUT_LAYOUTGRAND = 88;

  private static final int LAYOUT_LOGINWARNINGSHEET = 89;

  private static final int LAYOUT_LOWERBUYINGWARNINGSHEET = 90;

  private static final int LAYOUT_MENU = 91;

  private static final int LAYOUT_NOTIFYITEM = 92;

  private static final int LAYOUT_OPTIONDIALOG = 93;

  private static final int LAYOUT_ORDERSUCCESSDIALOG = 94;

  private static final int LAYOUT_PRICESLIST = 95;

  private static final int LAYOUT_PRODUCTSORT = 96;

  private static final int LAYOUT_RATEDIALOG = 97;

  private static final int LAYOUT_REVIEWDIALOG = 98;

  private static final SparseIntArray INTERNAL_LAYOUT_ID_LOOKUP = new SparseIntArray(98);

  static {
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.activity_base, LAYOUT_ACTIVITYBASE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.activity_main, LAYOUT_ACTIVITYMAIN);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.activity_map_address, LAYOUT_ACTIVITYMAPADDRESS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.cancel_warning_layout, LAYOUT_CANCELWARNINGLAYOUT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.exist_layout, LAYOUT_EXISTLAYOUT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.fragment_aber_box, LAYOUT_FRAGMENTABERBOX);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.fragment_about, LAYOUT_FRAGMENTABOUT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.fragment_add_place, LAYOUT_FRAGMENTADDPLACE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.fragment_app_wallet, LAYOUT_FRAGMENTAPPWALLET);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.fragment_batteries, LAYOUT_FRAGMENTBATTERIES);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.fragment_car_check, LAYOUT_FRAGMENTCARCHECK);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.fragment_cart, LAYOUT_FRAGMENTCART);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.fragment_change_password, LAYOUT_FRAGMENTCHANGEPASSWORD);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.fragment_chat, LAYOUT_FRAGMENTCHAT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.fragment_choose_service_time, LAYOUT_FRAGMENTCHOOSESERVICETIME);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.fragment_confirm_cart, LAYOUT_FRAGMENTCONFIRMCART);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.fragment_confirm_code, LAYOUT_FRAGMENTCONFIRMCODE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.fragment_confirm_order, LAYOUT_FRAGMENTCONFIRMORDER);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.fragment_contact, LAYOUT_FRAGMENTCONTACT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.fragment_conversations, LAYOUT_FRAGMENTCONVERSATIONS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.fragment_countries, LAYOUT_FRAGMENTCOUNTRIES);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.fragment_edit_profile, LAYOUT_FRAGMENTEDITPROFILE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.fragment_favorites, LAYOUT_FRAGMENTFAVORITES);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.fragment_filter, LAYOUT_FRAGMENTFILTER);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.fragment_follow_up_orders, LAYOUT_FRAGMENTFOLLOWUPORDERS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.fragment_forget_password, LAYOUT_FRAGMENTFORGETPASSWORD);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.fragment_fuel, LAYOUT_FRAGMENTFUEL);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.fragment_hidden, LAYOUT_FRAGMENTHIDDEN);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.fragment_home, LAYOUT_FRAGMENTHOME);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.fragment_lang, LAYOUT_FRAGMENTLANG);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.fragment_login, LAYOUT_FRAGMENTLOGIN);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.fragment_my_account_settings, LAYOUT_FRAGMENTMYACCOUNTSETTINGS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.fragment_my_location, LAYOUT_FRAGMENTMYLOCATION);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.fragment_my_order_details, LAYOUT_FRAGMENTMYORDERDETAILS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.fragment_my_orders, LAYOUT_FRAGMENTMYORDERS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.fragment_notifications, LAYOUT_FRAGMENTNOTIFICATIONS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.fragment_oil, LAYOUT_FRAGMENTOIL);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.fragment_onboard, LAYOUT_FRAGMENTONBOARD);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.fragment_open_car, LAYOUT_FRAGMENTOPENCAR);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.fragment_order_service_details, LAYOUT_FRAGMENTORDERSERVICEDETAILS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.fragment_part_services, LAYOUT_FRAGMENTPARTSERVICES);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.fragment_parts, LAYOUT_FRAGMENTPARTS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.fragment_payment, LAYOUT_FRAGMENTPAYMENT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.fragment_product_details, LAYOUT_FRAGMENTPRODUCTDETAILS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.fragment_register, LAYOUT_FRAGMENTREGISTER);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.fragment_reviews, LAYOUT_FRAGMENTREVIEWS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.fragment_search, LAYOUT_FRAGMENTSEARCH);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.fragment_services_orders, LAYOUT_FRAGMENTSERVICESORDERS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.fragment_social_media, LAYOUT_FRAGMENTSOCIALMEDIA);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.fragment_splash, LAYOUT_FRAGMENTSPLASH);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.fragment_terms, LAYOUT_FRAGMENTTERMS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.fragment_tiers, LAYOUT_FRAGMENTTIERS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.fragment_wash, LAYOUT_FRAGMENTWASH);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.fragment_water_ballon, LAYOUT_FRAGMENTWATERBALLON);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.fragment_winch, LAYOUT_FRAGMENTWINCH);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.item_car, LAYOUT_ITEMCAR);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.item_cart, LAYOUT_ITEMCART);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.item_category, LAYOUT_ITEMCATEGORY);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.item_chat, LAYOUT_ITEMCHAT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.item_color, LAYOUT_ITEMCOLOR);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.item_contact, LAYOUT_ITEMCONTACT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.item_conversation, LAYOUT_ITEMCONVERSATION);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.item_country, LAYOUT_ITEMCOUNTRY);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.item_extra_confirm_services, LAYOUT_ITEMEXTRACONFIRMSERVICES);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.item_extra_services, LAYOUT_ITEMEXTRASERVICES);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.item_favorite, LAYOUT_ITEMFAVORITE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.item_main_my_order, LAYOUT_ITEMMAINMYORDER);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.item_main_service_order, LAYOUT_ITEMMAINSERVICEORDER);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.item_my_location, LAYOUT_ITEMMYLOCATION);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.item_oil_prices_list, LAYOUT_ITEMOILPRICESLIST);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.item_order_confirmed, LAYOUT_ITEMORDERCONFIRMED);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.item_order_detail, LAYOUT_ITEMORDERDETAIL);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.item_order_service, LAYOUT_ITEMORDERSERVICE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.item_part, LAYOUT_ITEMPART);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.item_part_service, LAYOUT_ITEMPARTSERVICE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.item_required_service, LAYOUT_ITEMREQUIREDSERVICE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.item_review, LAYOUT_ITEMREVIEW);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.item_sale_desc, LAYOUT_ITEMSALEDESC);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.item_search, LAYOUT_ITEMSEARCH);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.item_service, LAYOUT_ITEMSERVICE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.item_service_menu, LAYOUT_ITEMSERVICEMENU);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.item_size, LAYOUT_ITEMSIZE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.item_social, LAYOUT_ITEMSOCIAL);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.item_social_menu, LAYOUT_ITEMSOCIALMENU);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.item_wallet_history, LAYOUT_ITEMWALLETHISTORY);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.layout_action_bar_back, LAYOUT_LAYOUTACTIONBARBACK);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.layout_action_bar_home, LAYOUT_LAYOUTACTIONBARHOME);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.layout_grand, LAYOUT_LAYOUTGRAND);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.login_warning_sheet, LAYOUT_LOGINWARNINGSHEET);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.lower_buying_warning_sheet, LAYOUT_LOWERBUYINGWARNINGSHEET);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.menu, LAYOUT_MENU);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.notify_item, LAYOUT_NOTIFYITEM);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.option_dialog, LAYOUT_OPTIONDIALOG);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.order_success_dialog, LAYOUT_ORDERSUCCESSDIALOG);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.prices_list, LAYOUT_PRICESLIST);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.product_sort, LAYOUT_PRODUCTSORT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.rate_dialog, LAYOUT_RATEDIALOG);
    INTERNAL_LAYOUT_ID_LOOKUP.put(grand.app.aber_user.R.layout.review_dialog, LAYOUT_REVIEWDIALOG);
  }

  private final ViewDataBinding internalGetViewDataBinding0(DataBindingComponent component,
      View view, int internalId, Object tag) {
    switch(internalId) {
      case  LAYOUT_ACTIVITYBASE: {
        if ("layout/activity_base_0".equals(tag)) {
          return new ActivityBaseBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for activity_base is invalid. Received: " + tag);
      }
      case  LAYOUT_ACTIVITYMAIN: {
        if ("layout/activity_main_0".equals(tag)) {
          return new ActivityMainBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for activity_main is invalid. Received: " + tag);
      }
      case  LAYOUT_ACTIVITYMAPADDRESS: {
        if ("layout/activity_map_address_0".equals(tag)) {
          return new ActivityMapAddressBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for activity_map_address is invalid. Received: " + tag);
      }
      case  LAYOUT_CANCELWARNINGLAYOUT: {
        if ("layout/cancel_warning_layout_0".equals(tag)) {
          return new CancelWarningLayoutBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for cancel_warning_layout is invalid. Received: " + tag);
      }
      case  LAYOUT_EXISTLAYOUT: {
        if ("layout/exist_layout_0".equals(tag)) {
          return new ExistLayoutBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for exist_layout is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTABERBOX: {
        if ("layout/fragment_aber_box_0".equals(tag)) {
          return new FragmentAberBoxBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_aber_box is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTABOUT: {
        if ("layout/fragment_about_0".equals(tag)) {
          return new FragmentAboutBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_about is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTADDPLACE: {
        if ("layout/fragment_add_place_0".equals(tag)) {
          return new FragmentAddPlaceBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_add_place is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTAPPWALLET: {
        if ("layout/fragment_app_wallet_0".equals(tag)) {
          return new FragmentAppWalletBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_app_wallet is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTBATTERIES: {
        if ("layout/fragment_batteries_0".equals(tag)) {
          return new FragmentBatteriesBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_batteries is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTCARCHECK: {
        if ("layout/fragment_car_check_0".equals(tag)) {
          return new FragmentCarCheckBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_car_check is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTCART: {
        if ("layout/fragment_cart_0".equals(tag)) {
          return new FragmentCartBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_cart is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTCHANGEPASSWORD: {
        if ("layout/fragment_change_password_0".equals(tag)) {
          return new FragmentChangePasswordBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_change_password is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTCHAT: {
        if ("layout/fragment_chat_0".equals(tag)) {
          return new FragmentChatBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_chat is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTCHOOSESERVICETIME: {
        if ("layout/fragment_choose_service_time_0".equals(tag)) {
          return new FragmentChooseServiceTimeBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_choose_service_time is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTCONFIRMCART: {
        if ("layout/fragment_confirm_cart_0".equals(tag)) {
          return new FragmentConfirmCartBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_confirm_cart is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTCONFIRMCODE: {
        if ("layout/fragment_confirm_code_0".equals(tag)) {
          return new FragmentConfirmCodeBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_confirm_code is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTCONFIRMORDER: {
        if ("layout/fragment_confirm_order_0".equals(tag)) {
          return new FragmentConfirmOrderBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_confirm_order is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTCONTACT: {
        if ("layout/fragment_contact_0".equals(tag)) {
          return new FragmentContactBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_contact is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTCONVERSATIONS: {
        if ("layout/fragment_conversations_0".equals(tag)) {
          return new FragmentConversationsBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_conversations is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTCOUNTRIES: {
        if ("layout/fragment_countries_0".equals(tag)) {
          return new FragmentCountriesBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_countries is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTEDITPROFILE: {
        if ("layout/fragment_edit_profile_0".equals(tag)) {
          return new FragmentEditProfileBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_edit_profile is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTFAVORITES: {
        if ("layout/fragment_favorites_0".equals(tag)) {
          return new FragmentFavoritesBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_favorites is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTFILTER: {
        if ("layout/fragment_filter_0".equals(tag)) {
          return new FragmentFilterBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_filter is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTFOLLOWUPORDERS: {
        if ("layout/fragment_follow_up_orders_0".equals(tag)) {
          return new FragmentFollowUpOrdersBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_follow_up_orders is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTFORGETPASSWORD: {
        if ("layout/fragment_forget_password_0".equals(tag)) {
          return new FragmentForgetPasswordBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_forget_password is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTFUEL: {
        if ("layout/fragment_fuel_0".equals(tag)) {
          return new FragmentFuelBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_fuel is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTHIDDEN: {
        if ("layout/fragment_hidden_0".equals(tag)) {
          return new FragmentHiddenBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_hidden is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTHOME: {
        if ("layout/fragment_home_0".equals(tag)) {
          return new FragmentHomeBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_home is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTLANG: {
        if ("layout/fragment_lang_0".equals(tag)) {
          return new FragmentLangBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_lang is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTLOGIN: {
        if ("layout/fragment_login_0".equals(tag)) {
          return new FragmentLoginBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_login is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTMYACCOUNTSETTINGS: {
        if ("layout/fragment_my_account_settings_0".equals(tag)) {
          return new FragmentMyAccountSettingsBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_my_account_settings is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTMYLOCATION: {
        if ("layout/fragment_my_location_0".equals(tag)) {
          return new FragmentMyLocationBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_my_location is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTMYORDERDETAILS: {
        if ("layout/fragment_my_order_details_0".equals(tag)) {
          return new FragmentMyOrderDetailsBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_my_order_details is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTMYORDERS: {
        if ("layout/fragment_my_orders_0".equals(tag)) {
          return new FragmentMyOrdersBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_my_orders is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTNOTIFICATIONS: {
        if ("layout/fragment_notifications_0".equals(tag)) {
          return new FragmentNotificationsBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_notifications is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTOIL: {
        if ("layout/fragment_oil_0".equals(tag)) {
          return new FragmentOilBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_oil is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTONBOARD: {
        if ("layout/fragment_onboard_0".equals(tag)) {
          return new FragmentOnboardBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_onboard is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTOPENCAR: {
        if ("layout/fragment_open_car_0".equals(tag)) {
          return new FragmentOpenCarBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_open_car is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTORDERSERVICEDETAILS: {
        if ("layout/fragment_order_service_details_0".equals(tag)) {
          return new FragmentOrderServiceDetailsBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_order_service_details is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTPARTSERVICES: {
        if ("layout/fragment_part_services_0".equals(tag)) {
          return new FragmentPartServicesBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_part_services is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTPARTS: {
        if ("layout/fragment_parts_0".equals(tag)) {
          return new FragmentPartsBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_parts is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTPAYMENT: {
        if ("layout/fragment_payment_0".equals(tag)) {
          return new FragmentPaymentBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_payment is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTPRODUCTDETAILS: {
        if ("layout/fragment_product_details_0".equals(tag)) {
          return new FragmentProductDetailsBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_product_details is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTREGISTER: {
        if ("layout/fragment_register_0".equals(tag)) {
          return new FragmentRegisterBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_register is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTREVIEWS: {
        if ("layout/fragment_reviews_0".equals(tag)) {
          return new FragmentReviewsBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_reviews is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTSEARCH: {
        if ("layout/fragment_search_0".equals(tag)) {
          return new FragmentSearchBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_search is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTSERVICESORDERS: {
        if ("layout/fragment_services_orders_0".equals(tag)) {
          return new FragmentServicesOrdersBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_services_orders is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTSOCIALMEDIA: {
        if ("layout/fragment_social_media_0".equals(tag)) {
          return new FragmentSocialMediaBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_social_media is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTSPLASH: {
        if ("layout/fragment_splash_0".equals(tag)) {
          return new FragmentSplashBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_splash is invalid. Received: " + tag);
      }
    }
    return null;
  }

  private final ViewDataBinding internalGetViewDataBinding1(DataBindingComponent component,
      View view, int internalId, Object tag) {
    switch(internalId) {
      case  LAYOUT_FRAGMENTTERMS: {
        if ("layout/fragment_terms_0".equals(tag)) {
          return new FragmentTermsBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_terms is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTTIERS: {
        if ("layout/fragment_tiers_0".equals(tag)) {
          return new FragmentTiersBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_tiers is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTWASH: {
        if ("layout/fragment_wash_0".equals(tag)) {
          return new FragmentWashBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_wash is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTWATERBALLON: {
        if ("layout/fragment_water_ballon_0".equals(tag)) {
          return new FragmentWaterBallonBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_water_ballon is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTWINCH: {
        if ("layout/fragment_winch_0".equals(tag)) {
          return new FragmentWinchBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_winch is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMCAR: {
        if ("layout/item_car_0".equals(tag)) {
          return new ItemCarBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_car is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMCART: {
        if ("layout/item_cart_0".equals(tag)) {
          return new ItemCartBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_cart is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMCATEGORY: {
        if ("layout/item_category_0".equals(tag)) {
          return new ItemCategoryBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_category is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMCHAT: {
        if ("layout/item_chat_0".equals(tag)) {
          return new ItemChatBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_chat is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMCOLOR: {
        if ("layout/item_color_0".equals(tag)) {
          return new ItemColorBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_color is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMCONTACT: {
        if ("layout/item_contact_0".equals(tag)) {
          return new ItemContactBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_contact is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMCONVERSATION: {
        if ("layout/item_conversation_0".equals(tag)) {
          return new ItemConversationBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_conversation is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMCOUNTRY: {
        if ("layout/item_country_0".equals(tag)) {
          return new ItemCountryBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_country is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMEXTRACONFIRMSERVICES: {
        if ("layout/item_extra_confirm_services_0".equals(tag)) {
          return new ItemExtraConfirmServicesBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_extra_confirm_services is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMEXTRASERVICES: {
        if ("layout/item_extra_services_0".equals(tag)) {
          return new ItemExtraServicesBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_extra_services is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMFAVORITE: {
        if ("layout/item_favorite_0".equals(tag)) {
          return new ItemFavoriteBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_favorite is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMMAINMYORDER: {
        if ("layout/item_main_my_order_0".equals(tag)) {
          return new ItemMainMyOrderBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_main_my_order is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMMAINSERVICEORDER: {
        if ("layout/item_main_service_order_0".equals(tag)) {
          return new ItemMainServiceOrderBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_main_service_order is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMMYLOCATION: {
        if ("layout/item_my_location_0".equals(tag)) {
          return new ItemMyLocationBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_my_location is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMOILPRICESLIST: {
        if ("layout/item_oil_prices_list_0".equals(tag)) {
          return new ItemOilPricesListBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_oil_prices_list is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMORDERCONFIRMED: {
        if ("layout/item_order_confirmed_0".equals(tag)) {
          return new ItemOrderConfirmedBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_order_confirmed is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMORDERDETAIL: {
        if ("layout/item_order_detail_0".equals(tag)) {
          return new ItemOrderDetailBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_order_detail is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMORDERSERVICE: {
        if ("layout/item_order_service_0".equals(tag)) {
          return new ItemOrderServiceBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_order_service is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMPART: {
        if ("layout/item_part_0".equals(tag)) {
          return new ItemPartBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_part is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMPARTSERVICE: {
        if ("layout/item_part_service_0".equals(tag)) {
          return new ItemPartServiceBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_part_service is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMREQUIREDSERVICE: {
        if ("layout/item_required_service_0".equals(tag)) {
          return new ItemRequiredServiceBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_required_service is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMREVIEW: {
        if ("layout/item_review_0".equals(tag)) {
          return new ItemReviewBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_review is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMSALEDESC: {
        if ("layout/item_sale_desc_0".equals(tag)) {
          return new ItemSaleDescBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_sale_desc is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMSEARCH: {
        if ("layout/item_search_0".equals(tag)) {
          return new ItemSearchBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_search is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMSERVICE: {
        if ("layout/item_service_0".equals(tag)) {
          return new ItemServiceBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_service is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMSERVICEMENU: {
        if ("layout/item_service_menu_0".equals(tag)) {
          return new ItemServiceMenuBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_service_menu is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMSIZE: {
        if ("layout/item_size_0".equals(tag)) {
          return new ItemSizeBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_size is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMSOCIAL: {
        if ("layout/item_social_0".equals(tag)) {
          return new ItemSocialBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_social is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMSOCIALMENU: {
        if ("layout/item_social_menu_0".equals(tag)) {
          return new ItemSocialMenuBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_social_menu is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMWALLETHISTORY: {
        if ("layout/item_wallet_history_0".equals(tag)) {
          return new ItemWalletHistoryBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_wallet_history is invalid. Received: " + tag);
      }
      case  LAYOUT_LAYOUTACTIONBARBACK: {
        if ("layout/layout_action_bar_back_0".equals(tag)) {
          return new LayoutActionBarBackBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for layout_action_bar_back is invalid. Received: " + tag);
      }
      case  LAYOUT_LAYOUTACTIONBARHOME: {
        if ("layout/layout_action_bar_home_0".equals(tag)) {
          return new LayoutActionBarHomeBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for layout_action_bar_home is invalid. Received: " + tag);
      }
      case  LAYOUT_LAYOUTGRAND: {
        if ("layout/layout_grand_0".equals(tag)) {
          return new LayoutGrandBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for layout_grand is invalid. Received: " + tag);
      }
      case  LAYOUT_LOGINWARNINGSHEET: {
        if ("layout/login_warning_sheet_0".equals(tag)) {
          return new LoginWarningSheetBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for login_warning_sheet is invalid. Received: " + tag);
      }
      case  LAYOUT_LOWERBUYINGWARNINGSHEET: {
        if ("layout/lower_buying_warning_sheet_0".equals(tag)) {
          return new LowerBuyingWarningSheetBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for lower_buying_warning_sheet is invalid. Received: " + tag);
      }
      case  LAYOUT_MENU: {
        if ("layout/menu_0".equals(tag)) {
          return new MenuBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for menu is invalid. Received: " + tag);
      }
      case  LAYOUT_NOTIFYITEM: {
        if ("layout/notify_item_0".equals(tag)) {
          return new NotifyItemBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for notify_item is invalid. Received: " + tag);
      }
      case  LAYOUT_OPTIONDIALOG: {
        if ("layout/option_dialog_0".equals(tag)) {
          return new OptionDialogBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for option_dialog is invalid. Received: " + tag);
      }
      case  LAYOUT_ORDERSUCCESSDIALOG: {
        if ("layout/order_success_dialog_0".equals(tag)) {
          return new OrderSuccessDialogBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for order_success_dialog is invalid. Received: " + tag);
      }
      case  LAYOUT_PRICESLIST: {
        if ("layout/prices_list_0".equals(tag)) {
          return new PricesListBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for prices_list is invalid. Received: " + tag);
      }
      case  LAYOUT_PRODUCTSORT: {
        if ("layout/product_sort_0".equals(tag)) {
          return new ProductSortBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for product_sort is invalid. Received: " + tag);
      }
      case  LAYOUT_RATEDIALOG: {
        if ("layout/rate_dialog_0".equals(tag)) {
          return new RateDialogBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for rate_dialog is invalid. Received: " + tag);
      }
      case  LAYOUT_REVIEWDIALOG: {
        if ("layout/review_dialog_0".equals(tag)) {
          return new ReviewDialogBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for review_dialog is invalid. Received: " + tag);
      }
    }
    return null;
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View view, int layoutId) {
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = view.getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      // find which method will have it. -1 is necessary becausefirst id starts with 1;
      int methodIndex = (localizedLayoutId - 1) / 50;
      switch(methodIndex) {
        case 0: {
          return internalGetViewDataBinding0(component, view, localizedLayoutId, tag);
        }
        case 1: {
          return internalGetViewDataBinding1(component, view, localizedLayoutId, tag);
        }
      }
    }
    return null;
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View[] views, int layoutId) {
    if(views == null || views.length == 0) {
      return null;
    }
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = views[0].getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
      }
    }
    return null;
  }

  @Override
  public int getLayoutId(String tag) {
    if (tag == null) {
      return 0;
    }
    Integer tmpVal = InnerLayoutIdLookup.sKeys.get(tag);
    return tmpVal == null ? 0 : tmpVal;
  }

  @Override
  public String convertBrIdToString(int localId) {
    String tmpVal = InnerBrLookup.sKeys.get(localId);
    return tmpVal;
  }

  @Override
  public List<DataBinderMapper> collectDependencies() {
    ArrayList<DataBinderMapper> result = new ArrayList<DataBinderMapper>(1);
    result.add(new androidx.databinding.library.baseAdapters.DataBinderMapperImpl());
    return result;
  }

  private static class InnerBrLookup {
    static final SparseArray<String> sKeys = new SparseArray<String>(81);

    static {
      sKeys.put(0, "_all");
      sKeys.put(1, "aboutData");
      sKeys.put(2, "adapter");
      sKeys.put(3, "addLocationRequest");
      sKeys.put(4, "appWalletAdapter");
      sKeys.put(5, "baseViewModel");
      sKeys.put(6, "carCatAdapter");
      sKeys.put(7, "carModelAdapter");
      sKeys.put(8, "carTypeAdapter");
      sKeys.put(9, "cartAdapter");
      sKeys.put(10, "chat");
      sKeys.put(11, "checkPromoRequest");
      sKeys.put(12, "colorItem");
      sKeys.put(13, "colorSizesAdapter");
      sKeys.put(14, "colorsAdapter");
      sKeys.put(15, "contact");
      sKeys.put(16, "contactsAdapter");
      sKeys.put(17, "conversationsAdapter");
      sKeys.put(18, "conversationsData");
      sKeys.put(19, "conversationsMain");
      sKeys.put(20, "countriesAdapter");
      sKeys.put(21, "countriesData");
      sKeys.put(22, "createServiceOrder");
      sKeys.put(23, "details");
      sKeys.put(24, "detailsAdapter");
      sKeys.put(25, "downsObject");
      sKeys.put(26, "dropDownsObject");
      sKeys.put(27, "extra");
      sKeys.put(28, "extraAdapter");
      sKeys.put(29, "filterData");
      sKeys.put(30, "filterRequest");
      sKeys.put(31, "historyWalletData");
      sKeys.put(32, "homeServicesAdapter");
      sKeys.put(33, "homeSliderAdapter");
      sKeys.put(34, "itemChatViewModel");
      sKeys.put(35, "itemOrderViewModel");
      sKeys.put(36, "itemPostViewModel");
      sKeys.put(37, "itemViewModel");
      sKeys.put(38, "itemWalletViewModel");
      sKeys.put(39, "locationsAdapters");
      sKeys.put(40, "locationsData");
      sKeys.put(41, "mainData");
      sKeys.put(42, "mapAddressViewModel");
      sKeys.put(43, "menuServicesAdapter");
      sKeys.put(44, "menuViewModel");
      sKeys.put(45, "message");
      sKeys.put(46, "myOrderDetails");
      sKeys.put(47, "myOrdersAdapter");
      sKeys.put(48, "newOrderRequest");
      sKeys.put(49, "notificationsAdapter");
      sKeys.put(50, "notificationsData");
      sKeys.put(51, "notifyItemViewModels");
      sKeys.put(52, "notifyViewModel");
      sKeys.put(53, "oilPricesAdapter");
      sKeys.put(54, "onBoardAdapter");
      sKeys.put(55, "onBoardViewModels");
      sKeys.put(56, "partsAdapter");
      sKeys.put(57, "passingObject");
      sKeys.put(58, "postData");
      sKeys.put(59, "productColorsAdapter");
      sKeys.put(60, "productDetails");
      sKeys.put(61, "productsAdapter");
      sKeys.put(62, "productsItem");
      sKeys.put(63, "raiseWalletRequest");
      sKeys.put(64, "rateRequest");
      sKeys.put(65, "ratesItem");
      sKeys.put(66, "reviewMainData");
      sKeys.put(67, "reviewsAdapter");
      sKeys.put(68, "service");
      sKeys.put(69, "serviceOrderRequest");
      sKeys.put(70, "services");
      sKeys.put(71, "servicesItem");
      sKeys.put(72, "servicesRequiredAdapter");
      sKeys.put(73, "sizesItem");
      sKeys.put(74, "sliderAdapter");
      sKeys.put(75, "socialAdapter");
      sKeys.put(76, "socialMediaData");
      sKeys.put(77, "userDocuments");
      sKeys.put(78, "viewModel");
      sKeys.put(79, "viewmodel");
      sKeys.put(80, "walletHistoryItem");
    }
  }

  private static class InnerLayoutIdLookup {
    static final HashMap<String, Integer> sKeys = new HashMap<String, Integer>(98);

    static {
      sKeys.put("layout/activity_base_0", grand.app.aber_user.R.layout.activity_base);
      sKeys.put("layout/activity_main_0", grand.app.aber_user.R.layout.activity_main);
      sKeys.put("layout/activity_map_address_0", grand.app.aber_user.R.layout.activity_map_address);
      sKeys.put("layout/cancel_warning_layout_0", grand.app.aber_user.R.layout.cancel_warning_layout);
      sKeys.put("layout/exist_layout_0", grand.app.aber_user.R.layout.exist_layout);
      sKeys.put("layout/fragment_aber_box_0", grand.app.aber_user.R.layout.fragment_aber_box);
      sKeys.put("layout/fragment_about_0", grand.app.aber_user.R.layout.fragment_about);
      sKeys.put("layout/fragment_add_place_0", grand.app.aber_user.R.layout.fragment_add_place);
      sKeys.put("layout/fragment_app_wallet_0", grand.app.aber_user.R.layout.fragment_app_wallet);
      sKeys.put("layout/fragment_batteries_0", grand.app.aber_user.R.layout.fragment_batteries);
      sKeys.put("layout/fragment_car_check_0", grand.app.aber_user.R.layout.fragment_car_check);
      sKeys.put("layout/fragment_cart_0", grand.app.aber_user.R.layout.fragment_cart);
      sKeys.put("layout/fragment_change_password_0", grand.app.aber_user.R.layout.fragment_change_password);
      sKeys.put("layout/fragment_chat_0", grand.app.aber_user.R.layout.fragment_chat);
      sKeys.put("layout/fragment_choose_service_time_0", grand.app.aber_user.R.layout.fragment_choose_service_time);
      sKeys.put("layout/fragment_confirm_cart_0", grand.app.aber_user.R.layout.fragment_confirm_cart);
      sKeys.put("layout/fragment_confirm_code_0", grand.app.aber_user.R.layout.fragment_confirm_code);
      sKeys.put("layout/fragment_confirm_order_0", grand.app.aber_user.R.layout.fragment_confirm_order);
      sKeys.put("layout/fragment_contact_0", grand.app.aber_user.R.layout.fragment_contact);
      sKeys.put("layout/fragment_conversations_0", grand.app.aber_user.R.layout.fragment_conversations);
      sKeys.put("layout/fragment_countries_0", grand.app.aber_user.R.layout.fragment_countries);
      sKeys.put("layout/fragment_edit_profile_0", grand.app.aber_user.R.layout.fragment_edit_profile);
      sKeys.put("layout/fragment_favorites_0", grand.app.aber_user.R.layout.fragment_favorites);
      sKeys.put("layout/fragment_filter_0", grand.app.aber_user.R.layout.fragment_filter);
      sKeys.put("layout/fragment_follow_up_orders_0", grand.app.aber_user.R.layout.fragment_follow_up_orders);
      sKeys.put("layout/fragment_forget_password_0", grand.app.aber_user.R.layout.fragment_forget_password);
      sKeys.put("layout/fragment_fuel_0", grand.app.aber_user.R.layout.fragment_fuel);
      sKeys.put("layout/fragment_hidden_0", grand.app.aber_user.R.layout.fragment_hidden);
      sKeys.put("layout/fragment_home_0", grand.app.aber_user.R.layout.fragment_home);
      sKeys.put("layout/fragment_lang_0", grand.app.aber_user.R.layout.fragment_lang);
      sKeys.put("layout/fragment_login_0", grand.app.aber_user.R.layout.fragment_login);
      sKeys.put("layout/fragment_my_account_settings_0", grand.app.aber_user.R.layout.fragment_my_account_settings);
      sKeys.put("layout/fragment_my_location_0", grand.app.aber_user.R.layout.fragment_my_location);
      sKeys.put("layout/fragment_my_order_details_0", grand.app.aber_user.R.layout.fragment_my_order_details);
      sKeys.put("layout/fragment_my_orders_0", grand.app.aber_user.R.layout.fragment_my_orders);
      sKeys.put("layout/fragment_notifications_0", grand.app.aber_user.R.layout.fragment_notifications);
      sKeys.put("layout/fragment_oil_0", grand.app.aber_user.R.layout.fragment_oil);
      sKeys.put("layout/fragment_onboard_0", grand.app.aber_user.R.layout.fragment_onboard);
      sKeys.put("layout/fragment_open_car_0", grand.app.aber_user.R.layout.fragment_open_car);
      sKeys.put("layout/fragment_order_service_details_0", grand.app.aber_user.R.layout.fragment_order_service_details);
      sKeys.put("layout/fragment_part_services_0", grand.app.aber_user.R.layout.fragment_part_services);
      sKeys.put("layout/fragment_parts_0", grand.app.aber_user.R.layout.fragment_parts);
      sKeys.put("layout/fragment_payment_0", grand.app.aber_user.R.layout.fragment_payment);
      sKeys.put("layout/fragment_product_details_0", grand.app.aber_user.R.layout.fragment_product_details);
      sKeys.put("layout/fragment_register_0", grand.app.aber_user.R.layout.fragment_register);
      sKeys.put("layout/fragment_reviews_0", grand.app.aber_user.R.layout.fragment_reviews);
      sKeys.put("layout/fragment_search_0", grand.app.aber_user.R.layout.fragment_search);
      sKeys.put("layout/fragment_services_orders_0", grand.app.aber_user.R.layout.fragment_services_orders);
      sKeys.put("layout/fragment_social_media_0", grand.app.aber_user.R.layout.fragment_social_media);
      sKeys.put("layout/fragment_splash_0", grand.app.aber_user.R.layout.fragment_splash);
      sKeys.put("layout/fragment_terms_0", grand.app.aber_user.R.layout.fragment_terms);
      sKeys.put("layout/fragment_tiers_0", grand.app.aber_user.R.layout.fragment_tiers);
      sKeys.put("layout/fragment_wash_0", grand.app.aber_user.R.layout.fragment_wash);
      sKeys.put("layout/fragment_water_ballon_0", grand.app.aber_user.R.layout.fragment_water_ballon);
      sKeys.put("layout/fragment_winch_0", grand.app.aber_user.R.layout.fragment_winch);
      sKeys.put("layout/item_car_0", grand.app.aber_user.R.layout.item_car);
      sKeys.put("layout/item_cart_0", grand.app.aber_user.R.layout.item_cart);
      sKeys.put("layout/item_category_0", grand.app.aber_user.R.layout.item_category);
      sKeys.put("layout/item_chat_0", grand.app.aber_user.R.layout.item_chat);
      sKeys.put("layout/item_color_0", grand.app.aber_user.R.layout.item_color);
      sKeys.put("layout/item_contact_0", grand.app.aber_user.R.layout.item_contact);
      sKeys.put("layout/item_conversation_0", grand.app.aber_user.R.layout.item_conversation);
      sKeys.put("layout/item_country_0", grand.app.aber_user.R.layout.item_country);
      sKeys.put("layout/item_extra_confirm_services_0", grand.app.aber_user.R.layout.item_extra_confirm_services);
      sKeys.put("layout/item_extra_services_0", grand.app.aber_user.R.layout.item_extra_services);
      sKeys.put("layout/item_favorite_0", grand.app.aber_user.R.layout.item_favorite);
      sKeys.put("layout/item_main_my_order_0", grand.app.aber_user.R.layout.item_main_my_order);
      sKeys.put("layout/item_main_service_order_0", grand.app.aber_user.R.layout.item_main_service_order);
      sKeys.put("layout/item_my_location_0", grand.app.aber_user.R.layout.item_my_location);
      sKeys.put("layout/item_oil_prices_list_0", grand.app.aber_user.R.layout.item_oil_prices_list);
      sKeys.put("layout/item_order_confirmed_0", grand.app.aber_user.R.layout.item_order_confirmed);
      sKeys.put("layout/item_order_detail_0", grand.app.aber_user.R.layout.item_order_detail);
      sKeys.put("layout/item_order_service_0", grand.app.aber_user.R.layout.item_order_service);
      sKeys.put("layout/item_part_0", grand.app.aber_user.R.layout.item_part);
      sKeys.put("layout/item_part_service_0", grand.app.aber_user.R.layout.item_part_service);
      sKeys.put("layout/item_required_service_0", grand.app.aber_user.R.layout.item_required_service);
      sKeys.put("layout/item_review_0", grand.app.aber_user.R.layout.item_review);
      sKeys.put("layout/item_sale_desc_0", grand.app.aber_user.R.layout.item_sale_desc);
      sKeys.put("layout/item_search_0", grand.app.aber_user.R.layout.item_search);
      sKeys.put("layout/item_service_0", grand.app.aber_user.R.layout.item_service);
      sKeys.put("layout/item_service_menu_0", grand.app.aber_user.R.layout.item_service_menu);
      sKeys.put("layout/item_size_0", grand.app.aber_user.R.layout.item_size);
      sKeys.put("layout/item_social_0", grand.app.aber_user.R.layout.item_social);
      sKeys.put("layout/item_social_menu_0", grand.app.aber_user.R.layout.item_social_menu);
      sKeys.put("layout/item_wallet_history_0", grand.app.aber_user.R.layout.item_wallet_history);
      sKeys.put("layout/layout_action_bar_back_0", grand.app.aber_user.R.layout.layout_action_bar_back);
      sKeys.put("layout/layout_action_bar_home_0", grand.app.aber_user.R.layout.layout_action_bar_home);
      sKeys.put("layout/layout_grand_0", grand.app.aber_user.R.layout.layout_grand);
      sKeys.put("layout/login_warning_sheet_0", grand.app.aber_user.R.layout.login_warning_sheet);
      sKeys.put("layout/lower_buying_warning_sheet_0", grand.app.aber_user.R.layout.lower_buying_warning_sheet);
      sKeys.put("layout/menu_0", grand.app.aber_user.R.layout.menu);
      sKeys.put("layout/notify_item_0", grand.app.aber_user.R.layout.notify_item);
      sKeys.put("layout/option_dialog_0", grand.app.aber_user.R.layout.option_dialog);
      sKeys.put("layout/order_success_dialog_0", grand.app.aber_user.R.layout.order_success_dialog);
      sKeys.put("layout/prices_list_0", grand.app.aber_user.R.layout.prices_list);
      sKeys.put("layout/product_sort_0", grand.app.aber_user.R.layout.product_sort);
      sKeys.put("layout/rate_dialog_0", grand.app.aber_user.R.layout.rate_dialog);
      sKeys.put("layout/review_dialog_0", grand.app.aber_user.R.layout.review_dialog);
    }
  }
}
