package grand.app.aber_user.databinding;
import grand.app.aber_user.R;
import grand.app.aber_user.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ItemMainMyOrderBindingImpl extends ItemMainMyOrderBinding implements grand.app.aber_user.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.ic_accept_line, 10);
        sViewsWithIds.put(R.id.ic_shipped_line, 11);
        sViewsWithIds.put(R.id.ic_on_way_line, 12);
        sViewsWithIds.put(R.id.br, 13);
        sViewsWithIds.put(R.id.tv_service_details, 14);
        sViewsWithIds.put(R.id.ic_service_detalis, 15);
    }
    // views
    @NonNull
    private final grand.app.aber_user.customViews.views.CustomTextViewRegular mboundView2;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback206;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ItemMainMyOrderBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 16, sIncludes, sViewsWithIds));
    }
    private ItemMainMyOrderBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 1
            , (androidx.constraintlayout.widget.Barrier) bindings[13]
            , (androidx.cardview.widget.CardView) bindings[0]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[1]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[10]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[7]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[5]
            , (android.view.View) bindings[12]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[15]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[3]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[11]
            , (androidx.recyclerview.widget.RecyclerView) bindings[9]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[8]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[6]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[14]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[4]
            );
        this.cardInfo.setTag(null);
        this.icAccept.setTag(null);
        this.icDelivered.setTag(null);
        this.icOnWay.setTag(null);
        this.icShipped.setTag(null);
        this.mboundView2 = (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[2];
        this.mboundView2.setTag(null);
        this.rcOrder.setTag(null);
        this.tvDelivered.setTag(null);
        this.tvOnWay.setTag(null);
        this.tvShipped.setTag(null);
        setRootTag(root);
        // listeners
        mCallback206 = new grand.app.aber_user.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x8L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.itemOrderViewModel == variableId) {
            setItemOrderViewModel((grand.app.aber_user.pages.myOrders.viewModels.ItemMyOrdersViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setItemOrderViewModel(@Nullable grand.app.aber_user.pages.myOrders.viewModels.ItemMyOrdersViewModel ItemOrderViewModel) {
        updateRegistration(0, ItemOrderViewModel);
        this.mItemOrderViewModel = ItemOrderViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.itemOrderViewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeItemOrderViewModel((grand.app.aber_user.pages.myOrders.viewModels.ItemMyOrdersViewModel) object, fieldId);
        }
        return false;
    }
    private boolean onChangeItemOrderViewModel(grand.app.aber_user.pages.myOrders.viewModels.ItemMyOrdersViewModel ItemOrderViewModel, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        else if (fieldId == BR.productsItem) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        else if (fieldId == BR.productsAdapter) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        int itemOrderViewModelProductsItemStatus = 0;
        boolean itemOrderViewModelProductsItemStatusInt1 = false;
        android.graphics.drawable.Drawable itemOrderViewModelProductsItemStatusInt3IcOnWayAndroidDrawableIcFollowSuccessIcOnWayAndroidDrawableIcFollowWaiting = null;
        int itemOrderViewModelProductsItemStatusInt3TvOnWayAndroidColorColorPrimaryTvOnWayAndroidColorColordark = 0;
        boolean itemOrderViewModelProductsItemStatusInt2 = false;
        grand.app.aber_user.pages.myOrders.viewModels.ItemMyOrdersViewModel itemOrderViewModel = mItemOrderViewModel;
        android.graphics.drawable.Drawable itemOrderViewModelProductsItemStatusInt2IcShippedAndroidDrawableIcFollowSuccessIcShippedAndroidDrawableIcFollowWaiting = null;
        boolean itemOrderViewModelProductsItemStatusInt3 = false;
        android.graphics.drawable.Drawable itemOrderViewModelProductsItemStatusInt4IcDeliveredAndroidDrawableIcFollowSuccessIcDeliveredAndroidDrawableIcFollowWaiting = null;
        grand.app.aber_user.pages.myOrders.adapter.MyOrderProductsAdapter itemOrderViewModelProductsAdapter = null;
        int itemOrderViewModelProductsItemStatusInt1MboundView2AndroidColorColorPrimaryMboundView2AndroidColorColordark = 0;
        boolean itemOrderViewModelProductsItemStatusInt4 = false;
        android.graphics.drawable.Drawable itemOrderViewModelProductsItemStatusInt1IcAcceptAndroidDrawableIcFollowSuccessIcAcceptAndroidDrawableIcFollowWaiting = null;
        int itemOrderViewModelProductsItemStatusInt4TvDeliveredAndroidColorColorPrimaryTvDeliveredAndroidColorColordark = 0;
        grand.app.aber_user.pages.myOrders.models.MyOrderData itemOrderViewModelProductsItem = null;
        int itemOrderViewModelProductsItemStatusInt2TvShippedAndroidColorColorPrimaryTvShippedAndroidColorColordark = 0;

        if ((dirtyFlags & 0xfL) != 0) {


            if ((dirtyFlags & 0xdL) != 0) {

                    if (itemOrderViewModel != null) {
                        // read itemOrderViewModel.productsAdapter
                        itemOrderViewModelProductsAdapter = itemOrderViewModel.getProductsAdapter();
                    }
            }
            if ((dirtyFlags & 0xbL) != 0) {

                    if (itemOrderViewModel != null) {
                        // read itemOrderViewModel.productsItem
                        itemOrderViewModelProductsItem = itemOrderViewModel.getProductsItem();
                    }


                    if (itemOrderViewModelProductsItem != null) {
                        // read itemOrderViewModel.productsItem.status
                        itemOrderViewModelProductsItemStatus = itemOrderViewModelProductsItem.getStatus();
                    }


                    // read itemOrderViewModel.productsItem.status >= 1
                    itemOrderViewModelProductsItemStatusInt1 = (itemOrderViewModelProductsItemStatus) >= (1);
                    // read itemOrderViewModel.productsItem.status >= 2
                    itemOrderViewModelProductsItemStatusInt2 = (itemOrderViewModelProductsItemStatus) >= (2);
                    // read itemOrderViewModel.productsItem.status >= 3
                    itemOrderViewModelProductsItemStatusInt3 = (itemOrderViewModelProductsItemStatus) >= (3);
                    // read itemOrderViewModel.productsItem.status >= 4
                    itemOrderViewModelProductsItemStatusInt4 = (itemOrderViewModelProductsItemStatus) >= (4);
                if((dirtyFlags & 0xbL) != 0) {
                    if(itemOrderViewModelProductsItemStatusInt1) {
                            dirtyFlags |= 0x2000L;
                            dirtyFlags |= 0x8000L;
                    }
                    else {
                            dirtyFlags |= 0x1000L;
                            dirtyFlags |= 0x4000L;
                    }
                }
                if((dirtyFlags & 0xbL) != 0) {
                    if(itemOrderViewModelProductsItemStatusInt2) {
                            dirtyFlags |= 0x200L;
                            dirtyFlags |= 0x80000L;
                    }
                    else {
                            dirtyFlags |= 0x100L;
                            dirtyFlags |= 0x40000L;
                    }
                }
                if((dirtyFlags & 0xbL) != 0) {
                    if(itemOrderViewModelProductsItemStatusInt3) {
                            dirtyFlags |= 0x20L;
                            dirtyFlags |= 0x80L;
                    }
                    else {
                            dirtyFlags |= 0x10L;
                            dirtyFlags |= 0x40L;
                    }
                }
                if((dirtyFlags & 0xbL) != 0) {
                    if(itemOrderViewModelProductsItemStatusInt4) {
                            dirtyFlags |= 0x800L;
                            dirtyFlags |= 0x20000L;
                    }
                    else {
                            dirtyFlags |= 0x400L;
                            dirtyFlags |= 0x10000L;
                    }
                }


                    // read itemOrderViewModel.productsItem.status >= 1 ? @android:color/colorPrimary : @android:color/colordark
                    itemOrderViewModelProductsItemStatusInt1MboundView2AndroidColorColorPrimaryMboundView2AndroidColorColordark = ((itemOrderViewModelProductsItemStatusInt1) ? (getColorFromResource(mboundView2, R.color.colorPrimary)) : (getColorFromResource(mboundView2, R.color.colordark)));
                    // read itemOrderViewModel.productsItem.status >= 1 ? @android:drawable/ic_follow_success : @android:drawable/ic_follow_waiting
                    itemOrderViewModelProductsItemStatusInt1IcAcceptAndroidDrawableIcFollowSuccessIcAcceptAndroidDrawableIcFollowWaiting = ((itemOrderViewModelProductsItemStatusInt1) ? (androidx.appcompat.content.res.AppCompatResources.getDrawable(icAccept.getContext(), R.drawable.ic_follow_success)) : (androidx.appcompat.content.res.AppCompatResources.getDrawable(icAccept.getContext(), R.drawable.ic_follow_waiting)));
                    // read itemOrderViewModel.productsItem.status >= 2 ? @android:drawable/ic_follow_success : @android:drawable/ic_follow_waiting
                    itemOrderViewModelProductsItemStatusInt2IcShippedAndroidDrawableIcFollowSuccessIcShippedAndroidDrawableIcFollowWaiting = ((itemOrderViewModelProductsItemStatusInt2) ? (androidx.appcompat.content.res.AppCompatResources.getDrawable(icShipped.getContext(), R.drawable.ic_follow_success)) : (androidx.appcompat.content.res.AppCompatResources.getDrawable(icShipped.getContext(), R.drawable.ic_follow_waiting)));
                    // read itemOrderViewModel.productsItem.status >= 2 ? @android:color/colorPrimary : @android:color/colordark
                    itemOrderViewModelProductsItemStatusInt2TvShippedAndroidColorColorPrimaryTvShippedAndroidColorColordark = ((itemOrderViewModelProductsItemStatusInt2) ? (getColorFromResource(tvShipped, R.color.colorPrimary)) : (getColorFromResource(tvShipped, R.color.colordark)));
                    // read itemOrderViewModel.productsItem.status >= 3 ? @android:drawable/ic_follow_success : @android:drawable/ic_follow_waiting
                    itemOrderViewModelProductsItemStatusInt3IcOnWayAndroidDrawableIcFollowSuccessIcOnWayAndroidDrawableIcFollowWaiting = ((itemOrderViewModelProductsItemStatusInt3) ? (androidx.appcompat.content.res.AppCompatResources.getDrawable(icOnWay.getContext(), R.drawable.ic_follow_success)) : (androidx.appcompat.content.res.AppCompatResources.getDrawable(icOnWay.getContext(), R.drawable.ic_follow_waiting)));
                    // read itemOrderViewModel.productsItem.status >= 3 ? @android:color/colorPrimary : @android:color/colordark
                    itemOrderViewModelProductsItemStatusInt3TvOnWayAndroidColorColorPrimaryTvOnWayAndroidColorColordark = ((itemOrderViewModelProductsItemStatusInt3) ? (getColorFromResource(tvOnWay, R.color.colorPrimary)) : (getColorFromResource(tvOnWay, R.color.colordark)));
                    // read itemOrderViewModel.productsItem.status >= 4 ? @android:drawable/ic_follow_success : @android:drawable/ic_follow_waiting
                    itemOrderViewModelProductsItemStatusInt4IcDeliveredAndroidDrawableIcFollowSuccessIcDeliveredAndroidDrawableIcFollowWaiting = ((itemOrderViewModelProductsItemStatusInt4) ? (androidx.appcompat.content.res.AppCompatResources.getDrawable(icDelivered.getContext(), R.drawable.ic_follow_success)) : (androidx.appcompat.content.res.AppCompatResources.getDrawable(icDelivered.getContext(), R.drawable.ic_follow_waiting)));
                    // read itemOrderViewModel.productsItem.status >= 4 ? @android:color/colorPrimary : @android:color/colordark
                    itemOrderViewModelProductsItemStatusInt4TvDeliveredAndroidColorColorPrimaryTvDeliveredAndroidColorColordark = ((itemOrderViewModelProductsItemStatusInt4) ? (getColorFromResource(tvDelivered, R.color.colorPrimary)) : (getColorFromResource(tvDelivered, R.color.colordark)));
            }
        }
        // batch finished
        if ((dirtyFlags & 0x8L) != 0) {
            // api target 1

            this.cardInfo.setOnClickListener(mCallback206);
        }
        if ((dirtyFlags & 0xbL) != 0) {
            // api target 1

            androidx.databinding.adapters.ImageViewBindingAdapter.setImageDrawable(this.icAccept, itemOrderViewModelProductsItemStatusInt1IcAcceptAndroidDrawableIcFollowSuccessIcAcceptAndroidDrawableIcFollowWaiting);
            androidx.databinding.adapters.ImageViewBindingAdapter.setImageDrawable(this.icDelivered, itemOrderViewModelProductsItemStatusInt4IcDeliveredAndroidDrawableIcFollowSuccessIcDeliveredAndroidDrawableIcFollowWaiting);
            androidx.databinding.adapters.ImageViewBindingAdapter.setImageDrawable(this.icOnWay, itemOrderViewModelProductsItemStatusInt3IcOnWayAndroidDrawableIcFollowSuccessIcOnWayAndroidDrawableIcFollowWaiting);
            androidx.databinding.adapters.ImageViewBindingAdapter.setImageDrawable(this.icShipped, itemOrderViewModelProductsItemStatusInt2IcShippedAndroidDrawableIcFollowSuccessIcShippedAndroidDrawableIcFollowWaiting);
            this.mboundView2.setTextColor(itemOrderViewModelProductsItemStatusInt1MboundView2AndroidColorColorPrimaryMboundView2AndroidColorColordark);
            this.tvDelivered.setTextColor(itemOrderViewModelProductsItemStatusInt4TvDeliveredAndroidColorColorPrimaryTvDeliveredAndroidColorColordark);
            this.tvOnWay.setTextColor(itemOrderViewModelProductsItemStatusInt3TvOnWayAndroidColorColorPrimaryTvOnWayAndroidColorColordark);
            this.tvShipped.setTextColor(itemOrderViewModelProductsItemStatusInt2TvShippedAndroidColorColorPrimaryTvShippedAndroidColorColordark);
        }
        if ((dirtyFlags & 0xdL) != 0) {
            // api target 1

            grand.app.aber_user.base.ApplicationBinding.getItemsV2Binding(this.rcOrder, itemOrderViewModelProductsAdapter, "1", "1");
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        // localize variables for thread safety
        // itemOrderViewModel
        grand.app.aber_user.pages.myOrders.viewModels.ItemMyOrdersViewModel itemOrderViewModel = mItemOrderViewModel;
        // itemOrderViewModel != null
        boolean itemOrderViewModelJavaLangObjectNull = false;



        itemOrderViewModelJavaLangObjectNull = (itemOrderViewModel) != (null);
        if (itemOrderViewModelJavaLangObjectNull) {


            itemOrderViewModel.itemAction();
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): itemOrderViewModel
        flag 1 (0x2L): itemOrderViewModel.productsItem
        flag 2 (0x3L): itemOrderViewModel.productsAdapter
        flag 3 (0x4L): null
        flag 4 (0x5L): itemOrderViewModel.productsItem.status >= 3 ? @android:drawable/ic_follow_success : @android:drawable/ic_follow_waiting
        flag 5 (0x6L): itemOrderViewModel.productsItem.status >= 3 ? @android:drawable/ic_follow_success : @android:drawable/ic_follow_waiting
        flag 6 (0x7L): itemOrderViewModel.productsItem.status >= 3 ? @android:color/colorPrimary : @android:color/colordark
        flag 7 (0x8L): itemOrderViewModel.productsItem.status >= 3 ? @android:color/colorPrimary : @android:color/colordark
        flag 8 (0x9L): itemOrderViewModel.productsItem.status >= 2 ? @android:drawable/ic_follow_success : @android:drawable/ic_follow_waiting
        flag 9 (0xaL): itemOrderViewModel.productsItem.status >= 2 ? @android:drawable/ic_follow_success : @android:drawable/ic_follow_waiting
        flag 10 (0xbL): itemOrderViewModel.productsItem.status >= 4 ? @android:drawable/ic_follow_success : @android:drawable/ic_follow_waiting
        flag 11 (0xcL): itemOrderViewModel.productsItem.status >= 4 ? @android:drawable/ic_follow_success : @android:drawable/ic_follow_waiting
        flag 12 (0xdL): itemOrderViewModel.productsItem.status >= 1 ? @android:color/colorPrimary : @android:color/colordark
        flag 13 (0xeL): itemOrderViewModel.productsItem.status >= 1 ? @android:color/colorPrimary : @android:color/colordark
        flag 14 (0xfL): itemOrderViewModel.productsItem.status >= 1 ? @android:drawable/ic_follow_success : @android:drawable/ic_follow_waiting
        flag 15 (0x10L): itemOrderViewModel.productsItem.status >= 1 ? @android:drawable/ic_follow_success : @android:drawable/ic_follow_waiting
        flag 16 (0x11L): itemOrderViewModel.productsItem.status >= 4 ? @android:color/colorPrimary : @android:color/colordark
        flag 17 (0x12L): itemOrderViewModel.productsItem.status >= 4 ? @android:color/colorPrimary : @android:color/colordark
        flag 18 (0x13L): itemOrderViewModel.productsItem.status >= 2 ? @android:color/colorPrimary : @android:color/colordark
        flag 19 (0x14L): itemOrderViewModel.productsItem.status >= 2 ? @android:color/colorPrimary : @android:color/colordark
    flag mapping end*/
    //end
}