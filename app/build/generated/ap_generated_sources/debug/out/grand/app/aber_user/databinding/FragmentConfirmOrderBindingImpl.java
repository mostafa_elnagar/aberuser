package grand.app.aber_user.databinding;
import grand.app.aber_user.R;
import grand.app.aber_user.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentConfirmOrderBindingImpl extends FragmentConfirmOrderBinding implements grand.app.aber_user.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.searchContainer, 87);
        sViewsWithIds.put(R.id.header, 88);
        sViewsWithIds.put(R.id.curve, 89);
        sViewsWithIds.put(R.id.card_info, 90);
        sViewsWithIds.put(R.id.tv_choosen_order, 91);
        sViewsWithIds.put(R.id.v_choosen_order, 92);
        sViewsWithIds.put(R.id.tv_services_cost, 93);
        sViewsWithIds.put(R.id.v_services_price, 94);
        sViewsWithIds.put(R.id.tv_services_extra_cost, 95);
        sViewsWithIds.put(R.id.v_services__extra_price, 96);
        sViewsWithIds.put(R.id.tv_delivery_cost, 97);
        sViewsWithIds.put(R.id.v_delivery_price, 98);
        sViewsWithIds.put(R.id.tv_eme_cost, 99);
        sViewsWithIds.put(R.id.v_eme_price, 100);
        sViewsWithIds.put(R.id.tv_taxes_cost, 101);
        sViewsWithIds.put(R.id.v_taxes_price, 102);
        sViewsWithIds.put(R.id.tv_total, 103);
        sViewsWithIds.put(R.id.v_additional_desc, 104);
        sViewsWithIds.put(R.id.card_payment, 105);
        sViewsWithIds.put(R.id.tv_choosen_payment, 106);
        sViewsWithIds.put(R.id.v_choosen_payment, 107);
        sViewsWithIds.put(R.id.ic_cash, 108);
        sViewsWithIds.put(R.id.radio_cash, 109);
        sViewsWithIds.put(R.id.radio_online, 110);
        sViewsWithIds.put(R.id.tv_choosen_to_location, 111);
        sViewsWithIds.put(R.id.v_choosen_to_location, 112);
        sViewsWithIds.put(R.id.card_location, 113);
        sViewsWithIds.put(R.id.tv_choosen_location, 114);
        sViewsWithIds.put(R.id.flow, 115);
        sViewsWithIds.put(R.id.v_choosen_location, 116);
        sViewsWithIds.put(R.id.tv_desc, 117);
        sViewsWithIds.put(R.id.v_desc, 118);
    }
    // views
    @NonNull
    private final androidx.core.widget.NestedScrollView mboundView0;
    @NonNull
    private final android.widget.ProgressBar mboundView36;
    @NonNull
    private final grand.app.aber_user.customViews.views.CustomTextViewRegular mboundView79;
    @NonNull
    private final grand.app.aber_user.customViews.views.CustomTextViewRegular mboundView82;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback221;
    @Nullable
    private final android.view.View.OnClickListener mCallback222;
    // values
    // listeners
    private OnCheckedChangeListenerImpl mViewModelOnPaymentChangeAndroidWidgetRadioGroupOnCheckedChangeListener;
    // Inverse Binding Event Handlers

    public FragmentConfirmOrderBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 119, sIncludes, sViewsWithIds));
    }
    private FragmentConfirmOrderBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 1
            , (androidx.appcompat.widget.AppCompatImageView) bindings[1]
            , (androidx.appcompat.widget.AppCompatButton) bindings[85]
            , (androidx.cardview.widget.CardView) bindings[83]
            , (androidx.cardview.widget.CardView) bindings[90]
            , (androidx.cardview.widget.CardView) bindings[113]
            , (androidx.cardview.widget.CardView) bindings[105]
            , (androidx.cardview.widget.CardView) bindings[40]
            , (androidx.cardview.widget.CardView) bindings[78]
            , (android.widget.TextView) bindings[89]
            , (androidx.constraintlayout.helper.widget.Flow) bindings[115]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[88]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[108]
            , (android.widget.RadioGroup) bindings[77]
            , (com.google.android.material.progressindicator.CircularProgressIndicator) bindings[86]
            , (android.widget.RadioButton) bindings[109]
            , (android.widget.RadioButton) bindings[110]
            , (androidx.recyclerview.widget.RecyclerView) bindings[29]
            , (androidx.recyclerview.widget.RecyclerView) bindings[2]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[87]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[63]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[64]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[60]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[61]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[24]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[25]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[57]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[58]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[54]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[55]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[67]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[69]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[70]
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[72]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[73]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[51]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[52]
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[27]
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[114]
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[91]
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[106]
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[111]
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[81]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[97]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[35]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[117]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[84]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[32]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[33]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[99]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[37]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[12]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[13]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[9]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[10]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[21]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[22]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[45]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[46]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[48]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[49]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[42]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[43]
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[75]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[76]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[15]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[16]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[66]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[6]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[7]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[3]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[4]
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[41]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[93]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[95]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[31]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[30]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[101]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[38]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[18]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[19]
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[103]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[39]
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[80]
            , (android.view.View) bindings[104]
            , (android.view.View) bindings[65]
            , (android.view.View) bindings[62]
            , (android.view.View) bindings[26]
            , (android.view.View) bindings[59]
            , (android.view.View) bindings[56]
            , (android.view.View) bindings[71]
            , (android.view.View) bindings[53]
            , (android.view.View) bindings[28]
            , (android.view.View) bindings[116]
            , (android.view.View) bindings[92]
            , (android.view.View) bindings[107]
            , (android.view.View) bindings[112]
            , (android.view.View) bindings[98]
            , (android.view.View) bindings[118]
            , (android.view.View) bindings[34]
            , (android.view.View) bindings[100]
            , (android.view.View) bindings[14]
            , (android.view.View) bindings[11]
            , (android.view.View) bindings[23]
            , (android.view.View) bindings[47]
            , (android.view.View) bindings[50]
            , (android.view.View) bindings[44]
            , (android.view.View) bindings[17]
            , (android.view.View) bindings[68]
            , (android.view.View) bindings[8]
            , (android.view.View) bindings[5]
            , (android.view.View) bindings[96]
            , (android.view.View) bindings[94]
            , (android.view.View) bindings[102]
            , (android.view.View) bindings[20]
            , (android.view.View) bindings[74]
            );
        this.back.setTag(null);
        this.btnPhone.setTag(null);
        this.cardDesc.setTag(null);
        this.cardRequiredAdditional.setTag(null);
        this.cardToLocation.setTag(null);
        this.mboundView0 = (androidx.core.widget.NestedScrollView) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView36 = (android.widget.ProgressBar) bindings[36];
        this.mboundView36.setTag(null);
        this.mboundView79 = (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[79];
        this.mboundView79.setTag(null);
        this.mboundView82 = (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[82];
        this.mboundView82.setTag(null);
        this.paymentRadioGroup.setTag(null);
        this.progress.setTag(null);
        this.rcExtraServices.setTag(null);
        this.rcServices.setTag(null);
        this.tvBatterySize.setTag(null);
        this.tvBatterySizeValue.setTag(null);
        this.tvBatteryType.setTag(null);
        this.tvBatteryTypeValue.setTag(null);
        this.tvBoxTypes.setTag(null);
        this.tvBoxValue.setTag(null);
        this.tvBuildingYear.setTag(null);
        this.tvCarBuildingYearValue.setTag(null);
        this.tvCarModel.setTag(null);
        this.tvCarModelValue.setTag(null);
        this.tvCarMotorNumberValue.setTag(null);
        this.tvCarTireDesc.setTag(null);
        this.tvCarTireDescValue.setTag(null);
        this.tvCarTireType.setTag(null);
        this.tvCarTireTypeValue.setTag(null);
        this.tvCarTypes.setTag(null);
        this.tvCarTypesValue.setTag(null);
        this.tvChoosenExtraOrder.setTag(null);
        this.tvDate.setTag(null);
        this.tvDeliveryPrice.setTag(null);
        this.tvDescText.setTag(null);
        this.tvDistance.setTag(null);
        this.tvDistanceValue.setTag(null);
        this.tvEmePrice.setTag(null);
        this.tvFuelCat.setTag(null);
        this.tvFuelCatValue.setTag(null);
        this.tvFuelType.setTag(null);
        this.tvFuelTypeValue.setTag(null);
        this.tvGallon.setTag(null);
        this.tvGallonValue.setTag(null);
        this.tvHiddenColor.setTag(null);
        this.tvHiddenColorValue.setTag(null);
        this.tvHiddenPerDesc.setTag(null);
        this.tvHiddenPerValue.setTag(null);
        this.tvHiddenTypes.setTag(null);
        this.tvHiddenValue.setTag(null);
        this.tvKilo.setTag(null);
        this.tvKiloValue.setTag(null);
        this.tvLitre.setTag(null);
        this.tvLitreValue.setTag(null);
        this.tvMotorNumber.setTag(null);
        this.tvOilLiquid.setTag(null);
        this.tvOilLiquidValue.setTag(null);
        this.tvOilType.setTag(null);
        this.tvOilTypeValue.setTag(null);
        this.tvRequiredAdditional.setTag(null);
        this.tvServicesExtraPrice.setTag(null);
        this.tvServicesPrice.setTag(null);
        this.tvTaxesPrice.setTag(null);
        this.tvTinkerServiceType.setTag(null);
        this.tvTinkerServiceTypeValue.setTag(null);
        this.tvTotalPrice.setTag(null);
        this.tvUrgent.setTag(null);
        this.vBatterySize.setTag(null);
        this.vBatteryType.setTag(null);
        this.vBoxValue.setTag(null);
        this.vBuildingYear.setTag(null);
        this.vCarModelValue.setTag(null);
        this.vCarTireDescValue.setTag(null);
        this.vCarTypesValue.setTag(null);
        this.vChoosenExtraOrder.setTag(null);
        this.vDistance.setTag(null);
        this.vFuelCat.setTag(null);
        this.vFuelType.setTag(null);
        this.vGallon.setTag(null);
        this.vHiddenColorValue.setTag(null);
        this.vHiddenPerValue.setTag(null);
        this.vHiddenValue.setTag(null);
        this.vLitre.setTag(null);
        this.vMotorNumber.setTag(null);
        this.vOilLiquid.setTag(null);
        this.vOilType.setTag(null);
        this.vTinkerServiceType.setTag(null);
        this.vTyreValue.setTag(null);
        setRootTag(root);
        // listeners
        mCallback221 = new grand.app.aber_user.generated.callback.OnClickListener(this, 1);
        mCallback222 = new grand.app.aber_user.generated.callback.OnClickListener(this, 2);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x20L;
                mDirtyFlags_1 = 0x0L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0 || mDirtyFlags_1 != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.viewModel == variableId) {
            setViewModel((grand.app.aber_user.pages.services.viewModels.ServiceConfirmOrderViewModels) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setViewModel(@Nullable grand.app.aber_user.pages.services.viewModels.ServiceConfirmOrderViewModels ViewModel) {
        updateRegistration(0, ViewModel);
        this.mViewModel = ViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.viewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeViewModel((grand.app.aber_user.pages.services.viewModels.ServiceConfirmOrderViewModels) object, fieldId);
        }
        return false;
    }
    private boolean onChangeViewModel(grand.app.aber_user.pages.services.viewModels.ServiceConfirmOrderViewModels ViewModel, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        else if (fieldId == BR.detailsAdapter) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        else if (fieldId == BR.extraAdapter) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        else if (fieldId == BR.createServiceOrder) {
            synchronized(this) {
                    mDirtyFlags |= 0x8L;
            }
            return true;
        }
        else if (fieldId == BR.message) {
            synchronized(this) {
                    mDirtyFlags |= 0x10L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        long dirtyFlags_1 = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
            dirtyFlags_1 = mDirtyFlags_1;
            mDirtyFlags_1 = 0;
        }
        boolean textUtilsIsEmptyViewModelServiceOrderRequestGallon = false;
        int viewModelCreateServiceOrderTransferServiceFloat00TextUtilsIsEmptyViewModelMessageBooleanFalseViewModelMessageEqualsConstantsSHOWPROGRESSBooleanFalseViewVISIBLEViewGONE = 0;
        boolean textUtilsIsEmptyViewModelServiceOrderRequestBatterySize = false;
        boolean viewModelCreateServiceOrderTransferServiceFloat00TextUtilsIsEmptyViewModelMessageBooleanFalse = false;
        boolean textUtilsIsEmptyViewModelServiceOrderRequestFuelType = false;
        boolean textUtilsIsEmptyViewModelMessageViewModelMessageEqualsConstantsSHOWPROGRESSBooleanFalse = false;
        int textUtilsIsEmptyViewModelServiceOrderRequestHiddenColorViewVISIBLEViewGONE = 0;
        int textUtilsIsEmptyViewModelServiceOrderRequestCarCatViewVISIBLEViewGONE = 0;
        int textUtilsIsEmptyViewModelServiceOrderRequestBatteryTypeViewVISIBLEViewGONE = 0;
        double viewModelServiceOrderRequestTotalServices = 0.0;
        float viewModelCreateServiceOrderDistance = 0f;
        int textUtilsIsEmptyViewModelServiceOrderRequestCarModelViewVISIBLEViewGONE = 0;
        boolean textUtilsIsEmptyViewModelServiceOrderRequestOilLiquid = false;
        java.lang.String stringValueOfViewModelCreateServiceOrderTaxesConcatJavaLangStringConcatViewModelCurrency = null;
        java.lang.String viewModelServiceOrderRequestTyerType = null;
        java.lang.String stringValueOfViewModelCreateServiceOrderTransferServiceConcatJavaLangStringConcatViewModelCurrency = null;
        boolean textUtilsIsEmptyViewModelServiceOrderRequestHiddenPercentage = false;
        java.lang.String viewModelServiceOrderRequestBatteryType = null;
        java.lang.String stringValueOfViewModelServiceOrderRequestExtraTotalServices = null;
        int viewModelServiceOrderRequestEmerengcyViewGONEViewVISIBLE = 0;
        java.lang.String viewModelServiceOrderRequestLitre = null;
        java.lang.String viewModelServiceOrderRequestTyerDesc = null;
        double viewModelCreateServiceOrderTaxes = 0.0;
        java.lang.String viewModelServiceOrderRequestOilType = null;
        boolean viewModelCreateServiceOrderTransferServiceFloat00TextUtilsIsEmptyViewModelMessageBooleanFalseViewModelMessageEqualsConstantsSHOWPROGRESSBooleanFalse = false;
        boolean textUtilsIsEmptyViewModelServiceOrderRequestCarMotor = false;
        int viewModelCreateServiceOrderDistanceFloat00ViewGONEViewVISIBLE = 0;
        boolean textUtilsIsEmptyViewModelMessageBooleanTrueViewModelMessageEqualsConstantsHIDEPROGRESSBooleanTrueBooleanFalse = false;
        java.lang.String stringValueOfViewModelCreateServiceOrderTotal = null;
        int textUtilsIsEmptyViewModelServiceOrderRequestCarTypeViewVISIBLEViewGONE = 0;
        int viewModelServiceOrderRequestExtraListJavaLangObjectNullViewModelServiceOrderRequestExtraListSizeInt0BooleanFalseViewVISIBLEViewGONE = 0;
        java.lang.String stringValueOfViewModelCreateServiceOrderTaxes = null;
        boolean viewModelCreateServiceOrderTransferServiceFloat00 = false;
        java.lang.String stringValueOfViewModelServiceOrderRequestEmePriceServicesConcatJavaLangString = null;
        boolean textUtilsIsEmptyViewModelServiceOrderRequestKm = false;
        java.lang.String viewModelServiceOrderRequestCarCat = null;
        boolean TextUtilsIsEmptyViewModelServiceOrderRequestKm1 = false;
        boolean viewModelServiceOrderRequestExtraListSizeInt0 = false;
        java.lang.String viewModelMessage = null;
        int textUtilsIsEmptyViewModelServiceOrderRequestTyerDescViewVISIBLEViewGONE = 0;
        java.lang.String stringValueOfViewModelServiceOrderRequestExtraTotalServicesConcatJavaLangStringConcatViewModelCurrency = null;
        boolean textUtilsIsEmptyViewModelMessage = false;
        boolean textUtilsIsEmptyViewModelServiceOrderRequestLitre = false;
        double viewModelCreateServiceOrderTotal = 0.0;
        int textUtilsIsEmptyViewModelServiceOrderRequestTyerTypeViewVISIBLEViewGONE = 0;
        boolean TextUtilsIsEmptyViewModelServiceOrderRequestFuelType1 = false;
        java.lang.String viewModelServiceOrderRequestMainServiceInt11TvBuildingYearAndroidStringMakingYearTvBuildingYearAndroidStringCarModels = null;
        java.util.List<grand.app.aber_user.pages.services.models.Extra> viewModelServiceOrderRequestExtraList = null;
        int textUtilsIsEmptyViewModelMessageViewModelMessageEqualsConstantsSHOWPROGRESSBooleanFalseViewVISIBLEViewGONE = 0;
        int textUtilsIsEmptyViewModelServiceOrderRequestGallonViewVISIBLEViewGONE = 0;
        boolean textUtilsIsEmptyViewModelServiceOrderRequestTyerDesc = false;
        int viewModelServiceOrderRequestExtraListSize = 0;
        java.lang.String viewModelServiceOrderRequestCarMotor = null;
        int textUtilsIsEmptyViewModelServiceOrderRequestCarMotorViewVISIBLEViewGONE = 0;
        android.widget.RadioGroup.OnCheckedChangeListener viewModelOnPaymentChangeAndroidWidgetRadioGroupOnCheckedChangeListener = null;
        boolean viewModelServiceOrderRequestExtraListJavaLangObjectNull = false;
        float viewModelCreateServiceOrderTransferService = 0f;
        boolean textUtilsIsEmptyViewModelServiceOrderRequestCarCat = false;
        int textUtilsIsEmptyViewModelServiceOrderRequestOilLiquidViewVISIBLEViewGONE = 0;
        java.lang.String viewModelServiceOrderRequestGallon = null;
        java.lang.String stringValueOfViewModelCreateServiceOrderDistanceConcatJavaLangStringConcatTvDistanceValueAndroidStringKmValue = null;
        java.lang.String viewModelServiceOrderRequestKm = null;
        java.lang.String stringValueOfViewModelServiceOrderRequestTotalServicesConcatJavaLangStringConcatViewModelCurrency = null;
        java.lang.String viewModelServiceOrderRequestTime = null;
        grand.app.aber_user.pages.services.models.ServiceOrderDetails viewModelServiceOrderRequest = null;
        boolean viewModelServiceOrderRequestExtraListJavaLangObjectNullViewModelServiceOrderRequestExtraListSizeInt0BooleanFalse = false;
        int textUtilsIsEmptyViewModelServiceOrderRequestHiddenPercentageViewVISIBLEViewGONE = 0;
        boolean viewModelCreateServiceOrderDistanceFloat00 = false;
        boolean textUtilsIsEmptyViewModelServiceOrderRequestHiddenColor = false;
        boolean textUtilsIsEmptyViewModelServiceOrderRequestCarType = false;
        java.lang.String stringValueOfViewModelCreateServiceOrderDistanceConcatJavaLangString = null;
        java.lang.String stringValueOfViewModelCreateServiceOrderTaxesConcatJavaLangString = null;
        boolean textUtilsIsEmptyViewModelServiceOrderRequestDesc = false;
        java.lang.String viewModelServiceOrderRequestToAddress = null;
        java.lang.String stringValueOfViewModelServiceOrderRequestTotalServices = null;
        grand.app.aber_user.pages.services.viewModels.ServiceConfirmOrderViewModels viewModel = mViewModel;
        boolean textUtilsIsEmptyViewModelMessageBooleanTrueViewModelMessageEqualsConstantsHIDEPROGRESS = false;
        boolean TextUtilsIsEmptyViewModelServiceOrderRequestHiddenPercentage1 = false;
        int viewModelServiceOrderRequestEmerengcyViewVISIBLEViewGONE = 0;
        java.lang.String viewModelServiceOrderRequestHiddenPercentage = null;
        int textUtilsIsEmptyViewModelServiceOrderRequestFromAddressViewVISIBLEViewGONE = 0;
        java.lang.String viewModelServiceOrderRequestBuildingYear = null;
        java.lang.String viewModelServiceOrderRequestDateConcatJavaLangString = null;
        java.lang.String viewModelServiceOrderRequestDesc = null;
        boolean TextUtilsIsEmptyViewModelServiceOrderRequestBatterySize1 = false;
        boolean textUtilsIsEmptyViewModelServiceOrderRequestTyerType = false;
        boolean textUtilsIsEmptyViewModelServiceOrderRequestBatteryType = false;
        java.lang.String stringValueOfViewModelServiceOrderRequestTotalServicesConcatJavaLangString = null;
        boolean TextUtilsIsEmptyViewModelServiceOrderRequestCarCat1 = false;
        java.lang.String viewModelServiceOrderRequestDate = null;
        boolean TextUtilsIsEmptyViewModelServiceOrderRequestBatteryType1 = false;
        int textUtilsIsEmptyViewModelServiceOrderRequestKmViewVISIBLEViewGONE = 0;
        double viewModelServiceOrderRequestEmePriceServices = 0.0;
        boolean textUtilsIsEmptyViewModelServiceOrderRequestHiddenType = false;
        android.graphics.drawable.Drawable textUtilsIsEmptyViewModelMessageBooleanTrueViewModelMessageEqualsConstantsHIDEPROGRESSBtnPhoneAndroidDrawableCornerViewGradientBtnPhoneAndroidDrawableCornerViewPrimaryMedium = null;
        java.lang.String viewModelServiceOrderRequestFuelCat = null;
        java.lang.String viewModelServiceOrderRequestHiddenColor = null;
        boolean TextUtilsIsEmptyViewModelServiceOrderRequestDesc1 = false;
        int textUtilsIsEmptyViewModelServiceOrderRequestLitreViewVISIBLEViewGONE = 0;
        boolean TextUtilsIsEmptyViewModelServiceOrderRequestLitre1 = false;
        boolean TextUtilsIsEmptyViewModelServiceOrderRequestHiddenType1 = false;
        java.lang.String stringValueOfViewModelServiceOrderRequestExtraTotalServicesConcatJavaLangString = null;
        boolean textUtilsIsEmptyViewModelServiceOrderRequestCarModel = false;
        boolean textUtilsIsEmptyViewModelServiceOrderRequestFuelCat = false;
        boolean viewModelMessageEqualsConstantsSHOWPROGRESS = false;
        java.lang.String stringValueOfViewModelCreateServiceOrderDistance = null;
        boolean TextUtilsIsEmptyViewModelServiceOrderRequestCarMotor1 = false;
        boolean textUtilsIsEmptyViewModelServiceOrderRequestOilType = false;
        java.lang.String viewModelServiceOrderRequestFuelType = null;
        grand.app.aber_user.pages.services.models.CreateServiceOrder viewModelCreateServiceOrder = null;
        boolean TextUtilsIsEmptyViewModelServiceOrderRequestHiddenColor1 = false;
        int viewModelServiceOrderRequestMainService = 0;
        java.lang.String viewModelServiceOrderRequestHiddenType = null;
        boolean textUtilsIsEmptyViewModelServiceOrderRequestBoxType = false;
        java.lang.String viewModelCurrency = null;
        boolean textUtilsIsEmptyViewModelServiceOrderRequestFromAddress = false;
        boolean TextUtilsIsEmptyViewModelServiceOrderRequestFuelCat1 = false;
        boolean TextUtilsIsEmptyViewModelMessage1 = false;
        java.lang.String stringValueOfViewModelCreateServiceOrderTotalConcatJavaLangString = null;
        double viewModelServiceOrderRequestExtraTotalServices = 0.0;
        boolean viewModelServiceOrderRequestMainServiceInt11 = false;
        boolean TextUtilsIsEmptyViewModelServiceOrderRequestOilType1 = false;
        boolean TextUtilsIsEmptyViewModelServiceOrderRequestFromAddress1 = false;
        int textUtilsIsEmptyViewModelServiceOrderRequestHiddenTypeViewVISIBLEViewGONE = 0;
        grand.app.aber_user.pages.services.adapters.OrderConfirmAdapter viewModelDetailsAdapter = null;
        boolean textUtilsIsEmptyViewModelServiceOrderRequestBuildingYear = false;
        java.lang.String viewModelServiceOrderRequestFromAddress = null;
        java.lang.String stringValueOfViewModelServiceOrderRequestEmePriceServicesConcatJavaLangStringConcatViewModelCurrency = null;
        java.lang.String textUtilsIsEmptyViewModelServiceOrderRequestBuildingYearTvRequiredAdditionalAndroidStringCarDescTvRequiredAdditionalAndroidStringAdditionalDesc = null;
        java.lang.String viewModelServiceOrderRequestBoxType = null;
        java.lang.String viewModelServiceOrderRequestOilLiquid = null;
        int textUtilsIsEmptyViewModelServiceOrderRequestBoxTypeViewVISIBLEViewGONE = 0;
        boolean viewModelMessageEqualsConstantsHIDEPROGRESS = false;
        boolean TextUtilsIsEmptyViewModelServiceOrderRequestTyerType1 = false;
        boolean viewModelServiceOrderRequestEmerengcy = false;
        java.lang.String viewModelServiceOrderRequestBatterySize = null;
        int textUtilsIsEmptyViewModelServiceOrderRequestFuelCatViewVISIBLEViewGONE = 0;
        int textUtilsIsEmptyViewModelServiceOrderRequestFuelTypeViewVISIBLEViewGONE = 0;
        int textUtilsIsEmptyViewModelServiceOrderRequestBatterySizeViewVISIBLEViewGONE = 0;
        java.lang.String viewModelServiceOrderRequestTinkerServiceName = null;
        boolean TextUtilsIsEmptyViewModelServiceOrderRequestOilLiquid1 = false;
        int textUtilsIsEmptyViewModelServiceOrderRequestDescViewVISIBLEViewGONE = 0;
        java.lang.String viewModelServiceOrderRequestCarModel = null;
        grand.app.aber_user.pages.services.adapters.ExtraConfirmAdapter viewModelExtraAdapter = null;
        java.lang.String viewModelServiceOrderRequestDateConcatJavaLangStringConcatViewModelServiceOrderRequestTime = null;
        java.lang.String stringValueOfViewModelCreateServiceOrderTotalConcatJavaLangStringConcatViewModelCurrency = null;
        boolean TextUtilsIsEmptyViewModelServiceOrderRequestGallon1 = false;
        boolean TextUtilsIsEmptyViewModelServiceOrderRequestTyerDesc1 = false;
        java.lang.String stringValueOfViewModelCreateServiceOrderTransferService = null;
        boolean TextUtilsIsEmptyViewModelServiceOrderRequestBuildingYear1 = false;
        java.lang.String viewModelServiceOrderRequestCarType = null;
        boolean TextUtilsIsEmptyViewModelServiceOrderRequestCarType1 = false;
        int textUtilsIsEmptyViewModelServiceOrderRequestOilTypeViewVISIBLEViewGONE = 0;
        boolean TextUtilsIsEmptyViewModelServiceOrderRequestCarModel1 = false;
        boolean textUtilsIsEmptyViewModelServiceOrderRequestCarTypeBooleanTrueTextUtilsIsEmptyViewModelServiceOrderRequestHiddenType = false;
        int textUtilsIsEmptyViewModelServiceOrderRequestCarTypeBooleanTrueTextUtilsIsEmptyViewModelServiceOrderRequestHiddenTypeViewVISIBLEViewGONE = 0;
        java.lang.String stringValueOfViewModelServiceOrderRequestEmePriceServices = null;
        java.lang.String stringValueOfViewModelCreateServiceOrderTransferServiceConcatJavaLangString = null;
        boolean TextUtilsIsEmptyViewModelServiceOrderRequestBoxType1 = false;

        if ((dirtyFlags & 0x3fL) != 0) {


            if ((dirtyFlags & 0x31L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.message
                        viewModelMessage = viewModel.getMessage();
                    }


                    // read TextUtils.isEmpty(viewModel.message)
                    textUtilsIsEmptyViewModelMessage = android.text.TextUtils.isEmpty(viewModelMessage);
                if((dirtyFlags & 0x31L) != 0) {
                    if(textUtilsIsEmptyViewModelMessage) {
                            dirtyFlags |= 0x8000000000000L;
                    }
                    else {
                            dirtyFlags |= 0x4000000000000L;
                    }
                }


                    // read !TextUtils.isEmpty(viewModel.message)
                    TextUtilsIsEmptyViewModelMessage1 = !textUtilsIsEmptyViewModelMessage;
                if((dirtyFlags & 0x31L) != 0) {
                    if(TextUtilsIsEmptyViewModelMessage1) {
                            dirtyFlags |= 0x800L;
                    }
                    else {
                            dirtyFlags |= 0x400L;
                    }
                }
            }
            if ((dirtyFlags & 0x21L) != 0) {

                    if (viewModel != null) {
                        // read viewModel::onPaymentChange
                        viewModelOnPaymentChangeAndroidWidgetRadioGroupOnCheckedChangeListener = (((mViewModelOnPaymentChangeAndroidWidgetRadioGroupOnCheckedChangeListener == null) ? (mViewModelOnPaymentChangeAndroidWidgetRadioGroupOnCheckedChangeListener = new OnCheckedChangeListenerImpl()) : mViewModelOnPaymentChangeAndroidWidgetRadioGroupOnCheckedChangeListener).setValue(viewModel));
                        // read viewModel.serviceOrderRequest
                        viewModelServiceOrderRequest = viewModel.getServiceOrderRequest();
                    }


                    if (viewModelServiceOrderRequest != null) {
                        // read viewModel.serviceOrderRequest.totalServices
                        viewModelServiceOrderRequestTotalServices = viewModelServiceOrderRequest.getTotalServices();
                        // read viewModel.serviceOrderRequest.tyerType
                        viewModelServiceOrderRequestTyerType = viewModelServiceOrderRequest.getTyerType();
                        // read viewModel.serviceOrderRequest.batteryType
                        viewModelServiceOrderRequestBatteryType = viewModelServiceOrderRequest.getBatteryType();
                        // read viewModel.serviceOrderRequest.litre
                        viewModelServiceOrderRequestLitre = viewModelServiceOrderRequest.getLitre();
                        // read viewModel.serviceOrderRequest.tyerDesc
                        viewModelServiceOrderRequestTyerDesc = viewModelServiceOrderRequest.getTyerDesc();
                        // read viewModel.serviceOrderRequest.oilType
                        viewModelServiceOrderRequestOilType = viewModelServiceOrderRequest.getOilType();
                        // read viewModel.serviceOrderRequest.carCat
                        viewModelServiceOrderRequestCarCat = viewModelServiceOrderRequest.getCarCat();
                        // read viewModel.serviceOrderRequest.extraList
                        viewModelServiceOrderRequestExtraList = viewModelServiceOrderRequest.getExtraList();
                        // read viewModel.serviceOrderRequest.carMotor
                        viewModelServiceOrderRequestCarMotor = viewModelServiceOrderRequest.getCarMotor();
                        // read viewModel.serviceOrderRequest.gallon
                        viewModelServiceOrderRequestGallon = viewModelServiceOrderRequest.getGallon();
                        // read viewModel.serviceOrderRequest.km
                        viewModelServiceOrderRequestKm = viewModelServiceOrderRequest.getKm();
                        // read viewModel.serviceOrderRequest.time
                        viewModelServiceOrderRequestTime = viewModelServiceOrderRequest.getTime();
                        // read viewModel.serviceOrderRequest.toAddress
                        viewModelServiceOrderRequestToAddress = viewModelServiceOrderRequest.getToAddress();
                        // read viewModel.serviceOrderRequest.hiddenPercentage
                        viewModelServiceOrderRequestHiddenPercentage = viewModelServiceOrderRequest.getHiddenPercentage();
                        // read viewModel.serviceOrderRequest.buildingYear
                        viewModelServiceOrderRequestBuildingYear = viewModelServiceOrderRequest.getBuildingYear();
                        // read viewModel.serviceOrderRequest.desc
                        viewModelServiceOrderRequestDesc = viewModelServiceOrderRequest.getDesc();
                        // read viewModel.serviceOrderRequest.date
                        viewModelServiceOrderRequestDate = viewModelServiceOrderRequest.getDate();
                        // read viewModel.serviceOrderRequest.emePriceServices
                        viewModelServiceOrderRequestEmePriceServices = viewModelServiceOrderRequest.getEmePriceServices();
                        // read viewModel.serviceOrderRequest.fuelCat
                        viewModelServiceOrderRequestFuelCat = viewModelServiceOrderRequest.getFuelCat();
                        // read viewModel.serviceOrderRequest.hiddenColor
                        viewModelServiceOrderRequestHiddenColor = viewModelServiceOrderRequest.getHiddenColor();
                        // read viewModel.serviceOrderRequest.fuelType
                        viewModelServiceOrderRequestFuelType = viewModelServiceOrderRequest.getFuelType();
                        // read viewModel.serviceOrderRequest.mainService
                        viewModelServiceOrderRequestMainService = viewModelServiceOrderRequest.getMainService();
                        // read viewModel.serviceOrderRequest.hiddenType
                        viewModelServiceOrderRequestHiddenType = viewModelServiceOrderRequest.getHiddenType();
                        // read viewModel.serviceOrderRequest.extraTotalServices
                        viewModelServiceOrderRequestExtraTotalServices = viewModelServiceOrderRequest.getExtraTotalServices();
                        // read viewModel.serviceOrderRequest.fromAddress
                        viewModelServiceOrderRequestFromAddress = viewModelServiceOrderRequest.getFromAddress();
                        // read viewModel.serviceOrderRequest.boxType
                        viewModelServiceOrderRequestBoxType = viewModelServiceOrderRequest.getBoxType();
                        // read viewModel.serviceOrderRequest.oilLiquid
                        viewModelServiceOrderRequestOilLiquid = viewModelServiceOrderRequest.getOilLiquid();
                        // read viewModel.serviceOrderRequest.emerengcy
                        viewModelServiceOrderRequestEmerengcy = viewModelServiceOrderRequest.isEmerengcy();
                        // read viewModel.serviceOrderRequest.batterySize
                        viewModelServiceOrderRequestBatterySize = viewModelServiceOrderRequest.getBatterySize();
                        // read viewModel.serviceOrderRequest.tinkerServiceName
                        viewModelServiceOrderRequestTinkerServiceName = viewModelServiceOrderRequest.getTinkerServiceName();
                        // read viewModel.serviceOrderRequest.carModel
                        viewModelServiceOrderRequestCarModel = viewModelServiceOrderRequest.getCarModel();
                        // read viewModel.serviceOrderRequest.carType
                        viewModelServiceOrderRequestCarType = viewModelServiceOrderRequest.getCarType();
                    }
                if((dirtyFlags & 0x21L) != 0) {
                    if(viewModelServiceOrderRequestEmerengcy) {
                            dirtyFlags |= 0x200000L;
                            dirtyFlags |= 0x20000000000000L;
                    }
                    else {
                            dirtyFlags |= 0x100000L;
                            dirtyFlags |= 0x10000000000000L;
                    }
                }


                    // read String.valueOf(viewModel.serviceOrderRequest.totalServices)
                    stringValueOfViewModelServiceOrderRequestTotalServices = java.lang.String.valueOf(viewModelServiceOrderRequestTotalServices);
                    // read TextUtils.isEmpty(viewModel.serviceOrderRequest.tyerType)
                    textUtilsIsEmptyViewModelServiceOrderRequestTyerType = android.text.TextUtils.isEmpty(viewModelServiceOrderRequestTyerType);
                    // read TextUtils.isEmpty(viewModel.serviceOrderRequest.batteryType)
                    textUtilsIsEmptyViewModelServiceOrderRequestBatteryType = android.text.TextUtils.isEmpty(viewModelServiceOrderRequestBatteryType);
                    // read TextUtils.isEmpty(viewModel.serviceOrderRequest.litre)
                    TextUtilsIsEmptyViewModelServiceOrderRequestLitre1 = android.text.TextUtils.isEmpty(viewModelServiceOrderRequestLitre);
                    // read TextUtils.isEmpty(viewModel.serviceOrderRequest.tyerDesc)
                    textUtilsIsEmptyViewModelServiceOrderRequestTyerDesc = android.text.TextUtils.isEmpty(viewModelServiceOrderRequestTyerDesc);
                    // read TextUtils.isEmpty(viewModel.serviceOrderRequest.oilType)
                    TextUtilsIsEmptyViewModelServiceOrderRequestOilType1 = android.text.TextUtils.isEmpty(viewModelServiceOrderRequestOilType);
                    // read TextUtils.isEmpty(viewModel.serviceOrderRequest.carCat)
                    TextUtilsIsEmptyViewModelServiceOrderRequestCarCat1 = android.text.TextUtils.isEmpty(viewModelServiceOrderRequestCarCat);
                    // read viewModel.serviceOrderRequest.extraList != null
                    viewModelServiceOrderRequestExtraListJavaLangObjectNull = (viewModelServiceOrderRequestExtraList) != (null);
                    // read TextUtils.isEmpty(viewModel.serviceOrderRequest.carMotor)
                    TextUtilsIsEmptyViewModelServiceOrderRequestCarMotor1 = android.text.TextUtils.isEmpty(viewModelServiceOrderRequestCarMotor);
                    // read TextUtils.isEmpty(viewModel.serviceOrderRequest.gallon)
                    textUtilsIsEmptyViewModelServiceOrderRequestGallon = android.text.TextUtils.isEmpty(viewModelServiceOrderRequestGallon);
                    // read TextUtils.isEmpty(viewModel.serviceOrderRequest.km)
                    textUtilsIsEmptyViewModelServiceOrderRequestKm = android.text.TextUtils.isEmpty(viewModelServiceOrderRequestKm);
                    // read TextUtils.isEmpty(viewModel.serviceOrderRequest.hiddenPercentage)
                    textUtilsIsEmptyViewModelServiceOrderRequestHiddenPercentage = android.text.TextUtils.isEmpty(viewModelServiceOrderRequestHiddenPercentage);
                    // read TextUtils.isEmpty(viewModel.serviceOrderRequest.buildingYear)
                    textUtilsIsEmptyViewModelServiceOrderRequestBuildingYear = android.text.TextUtils.isEmpty(viewModelServiceOrderRequestBuildingYear);
                    // read TextUtils.isEmpty(viewModel.serviceOrderRequest.desc)
                    TextUtilsIsEmptyViewModelServiceOrderRequestDesc1 = android.text.TextUtils.isEmpty(viewModelServiceOrderRequestDesc);
                    // read String.valueOf(viewModel.serviceOrderRequest.emePriceServices)
                    stringValueOfViewModelServiceOrderRequestEmePriceServices = java.lang.String.valueOf(viewModelServiceOrderRequestEmePriceServices);
                    // read TextUtils.isEmpty(viewModel.serviceOrderRequest.fuelCat)
                    TextUtilsIsEmptyViewModelServiceOrderRequestFuelCat1 = android.text.TextUtils.isEmpty(viewModelServiceOrderRequestFuelCat);
                    // read TextUtils.isEmpty(viewModel.serviceOrderRequest.hiddenColor)
                    textUtilsIsEmptyViewModelServiceOrderRequestHiddenColor = android.text.TextUtils.isEmpty(viewModelServiceOrderRequestHiddenColor);
                    // read TextUtils.isEmpty(viewModel.serviceOrderRequest.fuelType)
                    TextUtilsIsEmptyViewModelServiceOrderRequestFuelType1 = android.text.TextUtils.isEmpty(viewModelServiceOrderRequestFuelType);
                    // read viewModel.serviceOrderRequest.mainService == 11
                    viewModelServiceOrderRequestMainServiceInt11 = (viewModelServiceOrderRequestMainService) == (11);
                    // read TextUtils.isEmpty(viewModel.serviceOrderRequest.hiddenType)
                    TextUtilsIsEmptyViewModelServiceOrderRequestHiddenType1 = android.text.TextUtils.isEmpty(viewModelServiceOrderRequestHiddenType);
                    // read String.valueOf(viewModel.serviceOrderRequest.extraTotalServices)
                    stringValueOfViewModelServiceOrderRequestExtraTotalServices = java.lang.String.valueOf(viewModelServiceOrderRequestExtraTotalServices);
                    // read TextUtils.isEmpty(viewModel.serviceOrderRequest.fromAddress)
                    textUtilsIsEmptyViewModelServiceOrderRequestFromAddress = android.text.TextUtils.isEmpty(viewModelServiceOrderRequestFromAddress);
                    // read TextUtils.isEmpty(viewModel.serviceOrderRequest.boxType)
                    textUtilsIsEmptyViewModelServiceOrderRequestBoxType = android.text.TextUtils.isEmpty(viewModelServiceOrderRequestBoxType);
                    // read TextUtils.isEmpty(viewModel.serviceOrderRequest.oilLiquid)
                    TextUtilsIsEmptyViewModelServiceOrderRequestOilLiquid1 = android.text.TextUtils.isEmpty(viewModelServiceOrderRequestOilLiquid);
                    // read viewModel.serviceOrderRequest.emerengcy ? View.GONE : View.VISIBLE
                    viewModelServiceOrderRequestEmerengcyViewGONEViewVISIBLE = ((viewModelServiceOrderRequestEmerengcy) ? (android.view.View.GONE) : (android.view.View.VISIBLE));
                    // read viewModel.serviceOrderRequest.emerengcy ? View.VISIBLE : View.GONE
                    viewModelServiceOrderRequestEmerengcyViewVISIBLEViewGONE = ((viewModelServiceOrderRequestEmerengcy) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
                    // read TextUtils.isEmpty(viewModel.serviceOrderRequest.batterySize)
                    textUtilsIsEmptyViewModelServiceOrderRequestBatterySize = android.text.TextUtils.isEmpty(viewModelServiceOrderRequestBatterySize);
                    // read TextUtils.isEmpty(viewModel.serviceOrderRequest.carModel)
                    TextUtilsIsEmptyViewModelServiceOrderRequestCarModel1 = android.text.TextUtils.isEmpty(viewModelServiceOrderRequestCarModel);
                    // read TextUtils.isEmpty(viewModel.serviceOrderRequest.carType)
                    TextUtilsIsEmptyViewModelServiceOrderRequestCarType1 = android.text.TextUtils.isEmpty(viewModelServiceOrderRequestCarType);
                if((dirtyFlags & 0x21L) != 0) {
                    if(viewModelServiceOrderRequestExtraListJavaLangObjectNull) {
                            dirtyFlags |= 0x800000000000L;
                    }
                    else {
                            dirtyFlags |= 0x400000000000L;
                    }
                }
                if((dirtyFlags & 0x21L) != 0) {
                    if(viewModelServiceOrderRequestMainServiceInt11) {
                            dirtyFlags |= 0x2000000000L;
                    }
                    else {
                            dirtyFlags |= 0x1000000000L;
                    }
                }
                    if (viewModelServiceOrderRequestDate != null) {
                        // read viewModel.serviceOrderRequest.date.concat(" ")
                        viewModelServiceOrderRequestDateConcatJavaLangString = viewModelServiceOrderRequestDate.concat(" ");
                    }


                    if (stringValueOfViewModelServiceOrderRequestTotalServices != null) {
                        // read String.valueOf(viewModel.serviceOrderRequest.totalServices).concat(" ")
                        stringValueOfViewModelServiceOrderRequestTotalServicesConcatJavaLangString = stringValueOfViewModelServiceOrderRequestTotalServices.concat(" ");
                    }
                    // read !TextUtils.isEmpty(viewModel.serviceOrderRequest.tyerType)
                    TextUtilsIsEmptyViewModelServiceOrderRequestTyerType1 = !textUtilsIsEmptyViewModelServiceOrderRequestTyerType;
                    // read !TextUtils.isEmpty(viewModel.serviceOrderRequest.batteryType)
                    TextUtilsIsEmptyViewModelServiceOrderRequestBatteryType1 = !textUtilsIsEmptyViewModelServiceOrderRequestBatteryType;
                    // read !TextUtils.isEmpty(viewModel.serviceOrderRequest.litre)
                    textUtilsIsEmptyViewModelServiceOrderRequestLitre = !TextUtilsIsEmptyViewModelServiceOrderRequestLitre1;
                    // read !TextUtils.isEmpty(viewModel.serviceOrderRequest.tyerDesc)
                    TextUtilsIsEmptyViewModelServiceOrderRequestTyerDesc1 = !textUtilsIsEmptyViewModelServiceOrderRequestTyerDesc;
                    // read !TextUtils.isEmpty(viewModel.serviceOrderRequest.oilType)
                    textUtilsIsEmptyViewModelServiceOrderRequestOilType = !TextUtilsIsEmptyViewModelServiceOrderRequestOilType1;
                    // read !TextUtils.isEmpty(viewModel.serviceOrderRequest.carCat)
                    textUtilsIsEmptyViewModelServiceOrderRequestCarCat = !TextUtilsIsEmptyViewModelServiceOrderRequestCarCat1;
                    // read !TextUtils.isEmpty(viewModel.serviceOrderRequest.carMotor)
                    textUtilsIsEmptyViewModelServiceOrderRequestCarMotor = !TextUtilsIsEmptyViewModelServiceOrderRequestCarMotor1;
                    // read !TextUtils.isEmpty(viewModel.serviceOrderRequest.gallon)
                    TextUtilsIsEmptyViewModelServiceOrderRequestGallon1 = !textUtilsIsEmptyViewModelServiceOrderRequestGallon;
                    // read !TextUtils.isEmpty(viewModel.serviceOrderRequest.km)
                    TextUtilsIsEmptyViewModelServiceOrderRequestKm1 = !textUtilsIsEmptyViewModelServiceOrderRequestKm;
                    // read !TextUtils.isEmpty(viewModel.serviceOrderRequest.hiddenPercentage)
                    TextUtilsIsEmptyViewModelServiceOrderRequestHiddenPercentage1 = !textUtilsIsEmptyViewModelServiceOrderRequestHiddenPercentage;
                    // read !TextUtils.isEmpty(viewModel.serviceOrderRequest.buildingYear)
                    TextUtilsIsEmptyViewModelServiceOrderRequestBuildingYear1 = !textUtilsIsEmptyViewModelServiceOrderRequestBuildingYear;
                    // read !TextUtils.isEmpty(viewModel.serviceOrderRequest.desc)
                    textUtilsIsEmptyViewModelServiceOrderRequestDesc = !TextUtilsIsEmptyViewModelServiceOrderRequestDesc1;
                    // read !TextUtils.isEmpty(viewModel.serviceOrderRequest.fuelCat)
                    textUtilsIsEmptyViewModelServiceOrderRequestFuelCat = !TextUtilsIsEmptyViewModelServiceOrderRequestFuelCat1;
                    // read !TextUtils.isEmpty(viewModel.serviceOrderRequest.hiddenColor)
                    TextUtilsIsEmptyViewModelServiceOrderRequestHiddenColor1 = !textUtilsIsEmptyViewModelServiceOrderRequestHiddenColor;
                    // read !TextUtils.isEmpty(viewModel.serviceOrderRequest.fuelType)
                    textUtilsIsEmptyViewModelServiceOrderRequestFuelType = !TextUtilsIsEmptyViewModelServiceOrderRequestFuelType1;
                    // read viewModel.serviceOrderRequest.mainService == 11 ? @android:string/making_year : @android:string/car_models
                    viewModelServiceOrderRequestMainServiceInt11TvBuildingYearAndroidStringMakingYearTvBuildingYearAndroidStringCarModels = ((viewModelServiceOrderRequestMainServiceInt11) ? (tvBuildingYear.getResources().getString(R.string.making_year)) : (tvBuildingYear.getResources().getString(R.string.car_models)));
                    // read !TextUtils.isEmpty(viewModel.serviceOrderRequest.hiddenType)
                    textUtilsIsEmptyViewModelServiceOrderRequestHiddenType = !TextUtilsIsEmptyViewModelServiceOrderRequestHiddenType1;
                    // read !TextUtils.isEmpty(viewModel.serviceOrderRequest.fromAddress)
                    TextUtilsIsEmptyViewModelServiceOrderRequestFromAddress1 = !textUtilsIsEmptyViewModelServiceOrderRequestFromAddress;
                    // read !TextUtils.isEmpty(viewModel.serviceOrderRequest.boxType)
                    TextUtilsIsEmptyViewModelServiceOrderRequestBoxType1 = !textUtilsIsEmptyViewModelServiceOrderRequestBoxType;
                    // read !TextUtils.isEmpty(viewModel.serviceOrderRequest.oilLiquid)
                    textUtilsIsEmptyViewModelServiceOrderRequestOilLiquid = !TextUtilsIsEmptyViewModelServiceOrderRequestOilLiquid1;
                    // read !TextUtils.isEmpty(viewModel.serviceOrderRequest.batterySize)
                    TextUtilsIsEmptyViewModelServiceOrderRequestBatterySize1 = !textUtilsIsEmptyViewModelServiceOrderRequestBatterySize;
                    // read !TextUtils.isEmpty(viewModel.serviceOrderRequest.carModel)
                    textUtilsIsEmptyViewModelServiceOrderRequestCarModel = !TextUtilsIsEmptyViewModelServiceOrderRequestCarModel1;
                    // read !TextUtils.isEmpty(viewModel.serviceOrderRequest.carType)
                    textUtilsIsEmptyViewModelServiceOrderRequestCarType = !TextUtilsIsEmptyViewModelServiceOrderRequestCarType1;
                if((dirtyFlags & 0x21L) != 0) {
                    if(TextUtilsIsEmptyViewModelServiceOrderRequestTyerType1) {
                            dirtyFlags |= 0x800000000L;
                    }
                    else {
                            dirtyFlags |= 0x400000000L;
                    }
                }
                if((dirtyFlags & 0x21L) != 0) {
                    if(TextUtilsIsEmptyViewModelServiceOrderRequestBatteryType1) {
                            dirtyFlags |= 0x20000L;
                    }
                    else {
                            dirtyFlags |= 0x10000L;
                    }
                }
                if((dirtyFlags & 0x21L) != 0) {
                    if(textUtilsIsEmptyViewModelServiceOrderRequestLitre) {
                            dirtyFlags |= 0x2000000000000000L;
                    }
                    else {
                            dirtyFlags |= 0x1000000000000000L;
                    }
                }
                if((dirtyFlags & 0x21L) != 0) {
                    if(TextUtilsIsEmptyViewModelServiceOrderRequestTyerDesc1) {
                            dirtyFlags |= 0x200000000L;
                    }
                    else {
                            dirtyFlags |= 0x100000000L;
                    }
                }
                if((dirtyFlags & 0x21L) != 0) {
                    if(textUtilsIsEmptyViewModelServiceOrderRequestOilType) {
                            dirtyFlags_1 |= 0x2000L;
                    }
                    else {
                            dirtyFlags_1 |= 0x1000L;
                    }
                }
                if((dirtyFlags & 0x21L) != 0) {
                    if(textUtilsIsEmptyViewModelServiceOrderRequestCarCat) {
                            dirtyFlags |= 0x8000L;
                    }
                    else {
                            dirtyFlags |= 0x4000L;
                    }
                }
                if((dirtyFlags & 0x21L) != 0) {
                    if(textUtilsIsEmptyViewModelServiceOrderRequestCarMotor) {
                            dirtyFlags |= 0x80000000000L;
                    }
                    else {
                            dirtyFlags |= 0x40000000000L;
                    }
                }
                if((dirtyFlags & 0x21L) != 0) {
                    if(TextUtilsIsEmptyViewModelServiceOrderRequestGallon1) {
                            dirtyFlags |= 0x20000000000L;
                    }
                    else {
                            dirtyFlags |= 0x10000000000L;
                    }
                }
                if((dirtyFlags & 0x21L) != 0) {
                    if(TextUtilsIsEmptyViewModelServiceOrderRequestKm1) {
                            dirtyFlags |= 0x200000000000000L;
                    }
                    else {
                            dirtyFlags |= 0x100000000000000L;
                    }
                }
                if((dirtyFlags & 0x21L) != 0) {
                    if(TextUtilsIsEmptyViewModelServiceOrderRequestHiddenPercentage1) {
                            dirtyFlags |= 0x2000000000000L;
                    }
                    else {
                            dirtyFlags |= 0x1000000000000L;
                    }
                }
                if((dirtyFlags & 0x21L) != 0) {
                    if(TextUtilsIsEmptyViewModelServiceOrderRequestBuildingYear1) {
                            dirtyFlags_1 |= 0x2L;
                    }
                    else {
                            dirtyFlags_1 |= 0x1L;
                    }
                }
                if((dirtyFlags & 0x21L) != 0) {
                    if(textUtilsIsEmptyViewModelServiceOrderRequestDesc) {
                            dirtyFlags_1 |= 0x800L;
                    }
                    else {
                            dirtyFlags_1 |= 0x400L;
                    }
                }
                if((dirtyFlags & 0x21L) != 0) {
                    if(textUtilsIsEmptyViewModelServiceOrderRequestFuelCat) {
                            dirtyFlags_1 |= 0x20L;
                    }
                    else {
                            dirtyFlags_1 |= 0x10L;
                    }
                }
                if((dirtyFlags & 0x21L) != 0) {
                    if(TextUtilsIsEmptyViewModelServiceOrderRequestHiddenColor1) {
                            dirtyFlags |= 0x2000L;
                    }
                    else {
                            dirtyFlags |= 0x1000L;
                    }
                }
                if((dirtyFlags & 0x21L) != 0) {
                    if(textUtilsIsEmptyViewModelServiceOrderRequestFuelType) {
                            dirtyFlags_1 |= 0x80L;
                    }
                    else {
                            dirtyFlags_1 |= 0x40L;
                    }
                }
                if((dirtyFlags & 0x21L) != 0) {
                    if(textUtilsIsEmptyViewModelServiceOrderRequestHiddenType) {
                            dirtyFlags |= 0x8000000000000000L;
                    }
                    else {
                            dirtyFlags |= 0x4000000000000000L;
                    }
                }
                if((dirtyFlags & 0x21L) != 0) {
                    if(TextUtilsIsEmptyViewModelServiceOrderRequestFromAddress1) {
                            dirtyFlags |= 0x80000000000000L;
                    }
                    else {
                            dirtyFlags |= 0x40000000000000L;
                    }
                }
                if((dirtyFlags & 0x21L) != 0) {
                    if(TextUtilsIsEmptyViewModelServiceOrderRequestBoxType1) {
                            dirtyFlags_1 |= 0x8L;
                    }
                    else {
                            dirtyFlags_1 |= 0x4L;
                    }
                }
                if((dirtyFlags & 0x21L) != 0) {
                    if(textUtilsIsEmptyViewModelServiceOrderRequestOilLiquid) {
                            dirtyFlags |= 0x200000000000L;
                    }
                    else {
                            dirtyFlags |= 0x100000000000L;
                    }
                }
                if((dirtyFlags & 0x21L) != 0) {
                    if(TextUtilsIsEmptyViewModelServiceOrderRequestBatterySize1) {
                            dirtyFlags_1 |= 0x200L;
                    }
                    else {
                            dirtyFlags_1 |= 0x100L;
                    }
                }
                if((dirtyFlags & 0x21L) != 0) {
                    if(textUtilsIsEmptyViewModelServiceOrderRequestCarModel) {
                            dirtyFlags |= 0x80000L;
                    }
                    else {
                            dirtyFlags |= 0x40000L;
                    }
                }
                if((dirtyFlags & 0x21L) != 0) {
                    if(textUtilsIsEmptyViewModelServiceOrderRequestCarType) {
                            dirtyFlags |= 0x20000000L;
                            dirtyFlags_1 |= 0x8000L;
                    }
                    else {
                            dirtyFlags |= 0x10000000L;
                            dirtyFlags_1 |= 0x4000L;
                    }
                }
                    if (stringValueOfViewModelServiceOrderRequestEmePriceServices != null) {
                        // read String.valueOf(viewModel.serviceOrderRequest.emePriceServices).concat(" ")
                        stringValueOfViewModelServiceOrderRequestEmePriceServicesConcatJavaLangString = stringValueOfViewModelServiceOrderRequestEmePriceServices.concat(" ");
                    }
                    if (stringValueOfViewModelServiceOrderRequestExtraTotalServices != null) {
                        // read String.valueOf(viewModel.serviceOrderRequest.extraTotalServices).concat(" ")
                        stringValueOfViewModelServiceOrderRequestExtraTotalServicesConcatJavaLangString = stringValueOfViewModelServiceOrderRequestExtraTotalServices.concat(" ");
                    }
                    if (viewModelServiceOrderRequestDateConcatJavaLangString != null) {
                        // read viewModel.serviceOrderRequest.date.concat(" ").concat(viewModel.serviceOrderRequest.time)
                        viewModelServiceOrderRequestDateConcatJavaLangStringConcatViewModelServiceOrderRequestTime = viewModelServiceOrderRequestDateConcatJavaLangString.concat(viewModelServiceOrderRequestTime);
                    }


                    // read !TextUtils.isEmpty(viewModel.serviceOrderRequest.tyerType) ? View.VISIBLE : View.GONE
                    textUtilsIsEmptyViewModelServiceOrderRequestTyerTypeViewVISIBLEViewGONE = ((TextUtilsIsEmptyViewModelServiceOrderRequestTyerType1) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
                    // read !TextUtils.isEmpty(viewModel.serviceOrderRequest.batteryType) ? View.VISIBLE : View.GONE
                    textUtilsIsEmptyViewModelServiceOrderRequestBatteryTypeViewVISIBLEViewGONE = ((TextUtilsIsEmptyViewModelServiceOrderRequestBatteryType1) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
                    // read !TextUtils.isEmpty(viewModel.serviceOrderRequest.litre) ? View.VISIBLE : View.GONE
                    textUtilsIsEmptyViewModelServiceOrderRequestLitreViewVISIBLEViewGONE = ((textUtilsIsEmptyViewModelServiceOrderRequestLitre) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
                    // read !TextUtils.isEmpty(viewModel.serviceOrderRequest.tyerDesc) ? View.VISIBLE : View.GONE
                    textUtilsIsEmptyViewModelServiceOrderRequestTyerDescViewVISIBLEViewGONE = ((TextUtilsIsEmptyViewModelServiceOrderRequestTyerDesc1) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
                    // read !TextUtils.isEmpty(viewModel.serviceOrderRequest.oilType) ? View.VISIBLE : View.GONE
                    textUtilsIsEmptyViewModelServiceOrderRequestOilTypeViewVISIBLEViewGONE = ((textUtilsIsEmptyViewModelServiceOrderRequestOilType) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
                    // read !TextUtils.isEmpty(viewModel.serviceOrderRequest.carCat) ? View.VISIBLE : View.GONE
                    textUtilsIsEmptyViewModelServiceOrderRequestCarCatViewVISIBLEViewGONE = ((textUtilsIsEmptyViewModelServiceOrderRequestCarCat) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
                    // read !TextUtils.isEmpty(viewModel.serviceOrderRequest.carMotor) ? View.VISIBLE : View.GONE
                    textUtilsIsEmptyViewModelServiceOrderRequestCarMotorViewVISIBLEViewGONE = ((textUtilsIsEmptyViewModelServiceOrderRequestCarMotor) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
                    // read !TextUtils.isEmpty(viewModel.serviceOrderRequest.gallon) ? View.VISIBLE : View.GONE
                    textUtilsIsEmptyViewModelServiceOrderRequestGallonViewVISIBLEViewGONE = ((TextUtilsIsEmptyViewModelServiceOrderRequestGallon1) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
                    // read !TextUtils.isEmpty(viewModel.serviceOrderRequest.km) ? View.VISIBLE : View.GONE
                    textUtilsIsEmptyViewModelServiceOrderRequestKmViewVISIBLEViewGONE = ((TextUtilsIsEmptyViewModelServiceOrderRequestKm1) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
                    // read !TextUtils.isEmpty(viewModel.serviceOrderRequest.hiddenPercentage) ? View.VISIBLE : View.GONE
                    textUtilsIsEmptyViewModelServiceOrderRequestHiddenPercentageViewVISIBLEViewGONE = ((TextUtilsIsEmptyViewModelServiceOrderRequestHiddenPercentage1) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
                    // read !TextUtils.isEmpty(viewModel.serviceOrderRequest.buildingYear) ? @android:string/car_desc : @android:string/additional_desc
                    textUtilsIsEmptyViewModelServiceOrderRequestBuildingYearTvRequiredAdditionalAndroidStringCarDescTvRequiredAdditionalAndroidStringAdditionalDesc = ((TextUtilsIsEmptyViewModelServiceOrderRequestBuildingYear1) ? (tvRequiredAdditional.getResources().getString(R.string.car_desc)) : (tvRequiredAdditional.getResources().getString(R.string.additional_desc)));
                    // read !TextUtils.isEmpty(viewModel.serviceOrderRequest.desc) ? View.VISIBLE : View.GONE
                    textUtilsIsEmptyViewModelServiceOrderRequestDescViewVISIBLEViewGONE = ((textUtilsIsEmptyViewModelServiceOrderRequestDesc) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
                    // read !TextUtils.isEmpty(viewModel.serviceOrderRequest.fuelCat) ? View.VISIBLE : View.GONE
                    textUtilsIsEmptyViewModelServiceOrderRequestFuelCatViewVISIBLEViewGONE = ((textUtilsIsEmptyViewModelServiceOrderRequestFuelCat) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
                    // read !TextUtils.isEmpty(viewModel.serviceOrderRequest.hiddenColor) ? View.VISIBLE : View.GONE
                    textUtilsIsEmptyViewModelServiceOrderRequestHiddenColorViewVISIBLEViewGONE = ((TextUtilsIsEmptyViewModelServiceOrderRequestHiddenColor1) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
                    // read !TextUtils.isEmpty(viewModel.serviceOrderRequest.fuelType) ? View.VISIBLE : View.GONE
                    textUtilsIsEmptyViewModelServiceOrderRequestFuelTypeViewVISIBLEViewGONE = ((textUtilsIsEmptyViewModelServiceOrderRequestFuelType) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
                    // read !TextUtils.isEmpty(viewModel.serviceOrderRequest.hiddenType) ? View.VISIBLE : View.GONE
                    textUtilsIsEmptyViewModelServiceOrderRequestHiddenTypeViewVISIBLEViewGONE = ((textUtilsIsEmptyViewModelServiceOrderRequestHiddenType) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
                    // read !TextUtils.isEmpty(viewModel.serviceOrderRequest.fromAddress) ? View.VISIBLE : View.GONE
                    textUtilsIsEmptyViewModelServiceOrderRequestFromAddressViewVISIBLEViewGONE = ((TextUtilsIsEmptyViewModelServiceOrderRequestFromAddress1) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
                    // read !TextUtils.isEmpty(viewModel.serviceOrderRequest.boxType) ? View.VISIBLE : View.GONE
                    textUtilsIsEmptyViewModelServiceOrderRequestBoxTypeViewVISIBLEViewGONE = ((TextUtilsIsEmptyViewModelServiceOrderRequestBoxType1) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
                    // read !TextUtils.isEmpty(viewModel.serviceOrderRequest.oilLiquid) ? View.VISIBLE : View.GONE
                    textUtilsIsEmptyViewModelServiceOrderRequestOilLiquidViewVISIBLEViewGONE = ((textUtilsIsEmptyViewModelServiceOrderRequestOilLiquid) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
                    // read !TextUtils.isEmpty(viewModel.serviceOrderRequest.batterySize) ? View.VISIBLE : View.GONE
                    textUtilsIsEmptyViewModelServiceOrderRequestBatterySizeViewVISIBLEViewGONE = ((TextUtilsIsEmptyViewModelServiceOrderRequestBatterySize1) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
                    // read !TextUtils.isEmpty(viewModel.serviceOrderRequest.carModel) ? View.VISIBLE : View.GONE
                    textUtilsIsEmptyViewModelServiceOrderRequestCarModelViewVISIBLEViewGONE = ((textUtilsIsEmptyViewModelServiceOrderRequestCarModel) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
                    // read !TextUtils.isEmpty(viewModel.serviceOrderRequest.carType) ? View.VISIBLE : View.GONE
                    textUtilsIsEmptyViewModelServiceOrderRequestCarTypeViewVISIBLEViewGONE = ((textUtilsIsEmptyViewModelServiceOrderRequestCarType) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
            if ((dirtyFlags & 0x39L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.createServiceOrder
                        viewModelCreateServiceOrder = viewModel.getCreateServiceOrder();
                    }

                if ((dirtyFlags & 0x29L) != 0) {

                        if (viewModelCreateServiceOrder != null) {
                            // read viewModel.createServiceOrder.distance
                            viewModelCreateServiceOrderDistance = viewModelCreateServiceOrder.getDistance();
                            // read viewModel.createServiceOrder.taxes
                            viewModelCreateServiceOrderTaxes = viewModelCreateServiceOrder.getTaxes();
                            // read viewModel.createServiceOrder.total
                            viewModelCreateServiceOrderTotal = viewModelCreateServiceOrder.getTotal();
                        }


                        // read viewModel.createServiceOrder.distance == 0.0
                        viewModelCreateServiceOrderDistanceFloat00 = (viewModelCreateServiceOrderDistance) == (0.0);
                        // read String.valueOf(viewModel.createServiceOrder.distance)
                        stringValueOfViewModelCreateServiceOrderDistance = java.lang.String.valueOf(viewModelCreateServiceOrderDistance);
                        // read String.valueOf(viewModel.createServiceOrder.taxes)
                        stringValueOfViewModelCreateServiceOrderTaxes = java.lang.String.valueOf(viewModelCreateServiceOrderTaxes);
                        // read String.valueOf(viewModel.createServiceOrder.total)
                        stringValueOfViewModelCreateServiceOrderTotal = java.lang.String.valueOf(viewModelCreateServiceOrderTotal);
                    if((dirtyFlags & 0x29L) != 0) {
                        if(viewModelCreateServiceOrderDistanceFloat00) {
                                dirtyFlags |= 0x2000000L;
                        }
                        else {
                                dirtyFlags |= 0x1000000L;
                        }
                    }


                        // read viewModel.createServiceOrder.distance == 0.0 ? View.GONE : View.VISIBLE
                        viewModelCreateServiceOrderDistanceFloat00ViewGONEViewVISIBLE = ((viewModelCreateServiceOrderDistanceFloat00) ? (android.view.View.GONE) : (android.view.View.VISIBLE));
                        if (stringValueOfViewModelCreateServiceOrderDistance != null) {
                            // read String.valueOf(viewModel.createServiceOrder.distance).concat(" ")
                            stringValueOfViewModelCreateServiceOrderDistanceConcatJavaLangString = stringValueOfViewModelCreateServiceOrderDistance.concat(" ");
                        }
                        if (stringValueOfViewModelCreateServiceOrderTaxes != null) {
                            // read String.valueOf(viewModel.createServiceOrder.taxes).concat(" ")
                            stringValueOfViewModelCreateServiceOrderTaxesConcatJavaLangString = stringValueOfViewModelCreateServiceOrderTaxes.concat(" ");
                        }
                        if (stringValueOfViewModelCreateServiceOrderTotal != null) {
                            // read String.valueOf(viewModel.createServiceOrder.total).concat(" ")
                            stringValueOfViewModelCreateServiceOrderTotalConcatJavaLangString = stringValueOfViewModelCreateServiceOrderTotal.concat(" ");
                        }


                        if (stringValueOfViewModelCreateServiceOrderDistanceConcatJavaLangString != null) {
                            // read String.valueOf(viewModel.createServiceOrder.distance).concat(" ").concat(@android:string/km_value)
                            stringValueOfViewModelCreateServiceOrderDistanceConcatJavaLangStringConcatTvDistanceValueAndroidStringKmValue = stringValueOfViewModelCreateServiceOrderDistanceConcatJavaLangString.concat(tvDistanceValue.getResources().getString(R.string.km_value));
                        }
                }

                    if (viewModelCreateServiceOrder != null) {
                        // read viewModel.createServiceOrder.transferService
                        viewModelCreateServiceOrderTransferService = viewModelCreateServiceOrder.getTransferService();
                    }


                    // read viewModel.createServiceOrder.transferService == 0.0
                    viewModelCreateServiceOrderTransferServiceFloat00 = (viewModelCreateServiceOrderTransferService) == (0.0);
                if((dirtyFlags & 0x39L) != 0) {
                    if(viewModelCreateServiceOrderTransferServiceFloat00) {
                            dirtyFlags |= 0x200L;
                    }
                    else {
                            dirtyFlags |= 0x100L;
                    }
                }
                if ((dirtyFlags & 0x29L) != 0) {

                        // read String.valueOf(viewModel.createServiceOrder.transferService)
                        stringValueOfViewModelCreateServiceOrderTransferService = java.lang.String.valueOf(viewModelCreateServiceOrderTransferService);


                        if (stringValueOfViewModelCreateServiceOrderTransferService != null) {
                            // read String.valueOf(viewModel.createServiceOrder.transferService).concat(" ")
                            stringValueOfViewModelCreateServiceOrderTransferServiceConcatJavaLangString = stringValueOfViewModelCreateServiceOrderTransferService.concat(" ");
                        }
                }
            }
            if ((dirtyFlags & 0x29L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.currency
                        viewModelCurrency = viewModel.currency;
                    }


                    if (stringValueOfViewModelCreateServiceOrderTaxesConcatJavaLangString != null) {
                        // read String.valueOf(viewModel.createServiceOrder.taxes).concat(" ").concat(viewModel.currency)
                        stringValueOfViewModelCreateServiceOrderTaxesConcatJavaLangStringConcatViewModelCurrency = stringValueOfViewModelCreateServiceOrderTaxesConcatJavaLangString.concat(viewModelCurrency);
                    }
                    if (stringValueOfViewModelCreateServiceOrderTransferServiceConcatJavaLangString != null) {
                        // read String.valueOf(viewModel.createServiceOrder.transferService).concat(" ").concat(viewModel.currency)
                        stringValueOfViewModelCreateServiceOrderTransferServiceConcatJavaLangStringConcatViewModelCurrency = stringValueOfViewModelCreateServiceOrderTransferServiceConcatJavaLangString.concat(viewModelCurrency);
                    }
                    if (stringValueOfViewModelCreateServiceOrderTotalConcatJavaLangString != null) {
                        // read String.valueOf(viewModel.createServiceOrder.total).concat(" ").concat(viewModel.currency)
                        stringValueOfViewModelCreateServiceOrderTotalConcatJavaLangStringConcatViewModelCurrency = stringValueOfViewModelCreateServiceOrderTotalConcatJavaLangString.concat(viewModelCurrency);
                    }
                if ((dirtyFlags & 0x21L) != 0) {

                        if (stringValueOfViewModelServiceOrderRequestExtraTotalServicesConcatJavaLangString != null) {
                            // read String.valueOf(viewModel.serviceOrderRequest.extraTotalServices).concat(" ").concat(viewModel.currency)
                            stringValueOfViewModelServiceOrderRequestExtraTotalServicesConcatJavaLangStringConcatViewModelCurrency = stringValueOfViewModelServiceOrderRequestExtraTotalServicesConcatJavaLangString.concat(viewModelCurrency);
                        }
                        if (stringValueOfViewModelServiceOrderRequestTotalServicesConcatJavaLangString != null) {
                            // read String.valueOf(viewModel.serviceOrderRequest.totalServices).concat(" ").concat(viewModel.currency)
                            stringValueOfViewModelServiceOrderRequestTotalServicesConcatJavaLangStringConcatViewModelCurrency = stringValueOfViewModelServiceOrderRequestTotalServicesConcatJavaLangString.concat(viewModelCurrency);
                        }
                        if (stringValueOfViewModelServiceOrderRequestEmePriceServicesConcatJavaLangString != null) {
                            // read String.valueOf(viewModel.serviceOrderRequest.emePriceServices).concat(" ").concat(viewModel.currency)
                            stringValueOfViewModelServiceOrderRequestEmePriceServicesConcatJavaLangStringConcatViewModelCurrency = stringValueOfViewModelServiceOrderRequestEmePriceServicesConcatJavaLangString.concat(viewModelCurrency);
                        }
                }
            }
            if ((dirtyFlags & 0x23L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.detailsAdapter
                        viewModelDetailsAdapter = viewModel.getDetailsAdapter();
                    }
            }
            if ((dirtyFlags & 0x25L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.extraAdapter
                        viewModelExtraAdapter = viewModel.getExtraAdapter();
                    }
            }
        }
        // batch finished

        if ((dirtyFlags & 0x200L) != 0) {

                if (viewModel != null) {
                    // read viewModel.message
                    viewModelMessage = viewModel.getMessage();
                }


                // read TextUtils.isEmpty(viewModel.message)
                textUtilsIsEmptyViewModelMessage = android.text.TextUtils.isEmpty(viewModelMessage);
            if((dirtyFlags & 0x31L) != 0) {
                if(textUtilsIsEmptyViewModelMessage) {
                        dirtyFlags |= 0x8000000000000L;
                }
                else {
                        dirtyFlags |= 0x4000000000000L;
                }
            }


                // read !TextUtils.isEmpty(viewModel.message)
                TextUtilsIsEmptyViewModelMessage1 = !textUtilsIsEmptyViewModelMessage;
            if((dirtyFlags & 0x31L) != 0) {
                if(TextUtilsIsEmptyViewModelMessage1) {
                        dirtyFlags |= 0x800L;
                }
                else {
                        dirtyFlags |= 0x400L;
                }
            }
        }
        if ((dirtyFlags & 0x800000000000L) != 0) {

                if (viewModelServiceOrderRequestExtraList != null) {
                    // read viewModel.serviceOrderRequest.extraList.size()
                    viewModelServiceOrderRequestExtraListSize = viewModelServiceOrderRequestExtraList.size();
                }


                // read viewModel.serviceOrderRequest.extraList.size() > 0
                viewModelServiceOrderRequestExtraListSizeInt0 = (viewModelServiceOrderRequestExtraListSize) > (0);
        }
        if ((dirtyFlags & 0x21L) != 0) {

                // read !TextUtils.isEmpty(viewModel.serviceOrderRequest.carType) ? true : !TextUtils.isEmpty(viewModel.serviceOrderRequest.hiddenType)
                textUtilsIsEmptyViewModelServiceOrderRequestCarTypeBooleanTrueTextUtilsIsEmptyViewModelServiceOrderRequestHiddenType = ((textUtilsIsEmptyViewModelServiceOrderRequestCarType) ? (true) : (textUtilsIsEmptyViewModelServiceOrderRequestHiddenType));
            if((dirtyFlags & 0x21L) != 0) {
                if(textUtilsIsEmptyViewModelServiceOrderRequestCarTypeBooleanTrueTextUtilsIsEmptyViewModelServiceOrderRequestHiddenType) {
                        dirtyFlags_1 |= 0x20000L;
                }
                else {
                        dirtyFlags_1 |= 0x10000L;
                }
            }


                // read !TextUtils.isEmpty(viewModel.serviceOrderRequest.carType) ? true : !TextUtils.isEmpty(viewModel.serviceOrderRequest.hiddenType) ? View.VISIBLE : View.GONE
                textUtilsIsEmptyViewModelServiceOrderRequestCarTypeBooleanTrueTextUtilsIsEmptyViewModelServiceOrderRequestHiddenTypeViewVISIBLEViewGONE = ((textUtilsIsEmptyViewModelServiceOrderRequestCarTypeBooleanTrueTextUtilsIsEmptyViewModelServiceOrderRequestHiddenType) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
        }

        if ((dirtyFlags & 0x39L) != 0) {

                // read viewModel.createServiceOrder.transferService == 0.0 ? !TextUtils.isEmpty(viewModel.message) : false
                viewModelCreateServiceOrderTransferServiceFloat00TextUtilsIsEmptyViewModelMessageBooleanFalse = ((viewModelCreateServiceOrderTransferServiceFloat00) ? (TextUtilsIsEmptyViewModelMessage1) : (false));
            if((dirtyFlags & 0x39L) != 0) {
                if(viewModelCreateServiceOrderTransferServiceFloat00TextUtilsIsEmptyViewModelMessageBooleanFalse) {
                        dirtyFlags |= 0x800000L;
                }
                else {
                        dirtyFlags |= 0x400000L;
                }
            }
        }
        if ((dirtyFlags & 0x21L) != 0) {

                // read viewModel.serviceOrderRequest.extraList != null ? viewModel.serviceOrderRequest.extraList.size() > 0 : false
                viewModelServiceOrderRequestExtraListJavaLangObjectNullViewModelServiceOrderRequestExtraListSizeInt0BooleanFalse = ((viewModelServiceOrderRequestExtraListJavaLangObjectNull) ? (viewModelServiceOrderRequestExtraListSizeInt0) : (false));
            if((dirtyFlags & 0x21L) != 0) {
                if(viewModelServiceOrderRequestExtraListJavaLangObjectNullViewModelServiceOrderRequestExtraListSizeInt0BooleanFalse) {
                        dirtyFlags |= 0x80000000L;
                }
                else {
                        dirtyFlags |= 0x40000000L;
                }
            }


                // read viewModel.serviceOrderRequest.extraList != null ? viewModel.serviceOrderRequest.extraList.size() > 0 : false ? View.VISIBLE : View.GONE
                viewModelServiceOrderRequestExtraListJavaLangObjectNullViewModelServiceOrderRequestExtraListSizeInt0BooleanFalseViewVISIBLEViewGONE = ((viewModelServiceOrderRequestExtraListJavaLangObjectNullViewModelServiceOrderRequestExtraListSizeInt0BooleanFalse) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
        }
        // batch finished

        if ((dirtyFlags & 0x4000000800800L) != 0) {

                if (viewModel != null) {
                    // read viewModel.message
                    viewModelMessage = viewModel.getMessage();
                }

            if ((dirtyFlags & 0x800800L) != 0) {

                    if (viewModelMessage != null) {
                        // read viewModel.message.equals(Constants.SHOW_PROGRESS)
                        viewModelMessageEqualsConstantsSHOWPROGRESS = viewModelMessage.equals(grand.app.aber_user.utils.Constants.SHOW_PROGRESS);
                    }
            }
            if ((dirtyFlags & 0x4000000000000L) != 0) {

                    if (viewModelMessage != null) {
                        // read viewModel.message.equals(Constants.HIDE_PROGRESS)
                        viewModelMessageEqualsConstantsHIDEPROGRESS = viewModelMessage.equals(grand.app.aber_user.utils.Constants.HIDE_PROGRESS);
                    }
            }
        }

        if ((dirtyFlags & 0x31L) != 0) {

                // read !TextUtils.isEmpty(viewModel.message) ? viewModel.message.equals(Constants.SHOW_PROGRESS) : false
                textUtilsIsEmptyViewModelMessageViewModelMessageEqualsConstantsSHOWPROGRESSBooleanFalse = ((TextUtilsIsEmptyViewModelMessage1) ? (viewModelMessageEqualsConstantsSHOWPROGRESS) : (false));
                // read TextUtils.isEmpty(viewModel.message) ? true : viewModel.message.equals(Constants.HIDE_PROGRESS)
                textUtilsIsEmptyViewModelMessageBooleanTrueViewModelMessageEqualsConstantsHIDEPROGRESS = ((textUtilsIsEmptyViewModelMessage) ? (true) : (viewModelMessageEqualsConstantsHIDEPROGRESS));
            if((dirtyFlags & 0x31L) != 0) {
                if(textUtilsIsEmptyViewModelMessageViewModelMessageEqualsConstantsSHOWPROGRESSBooleanFalse) {
                        dirtyFlags |= 0x8000000000L;
                }
                else {
                        dirtyFlags |= 0x4000000000L;
                }
            }
            if((dirtyFlags & 0x31L) != 0) {
                if(textUtilsIsEmptyViewModelMessageBooleanTrueViewModelMessageEqualsConstantsHIDEPROGRESS) {
                        dirtyFlags |= 0x8000000L;
                        dirtyFlags |= 0x800000000000000L;
                }
                else {
                        dirtyFlags |= 0x4000000L;
                        dirtyFlags |= 0x400000000000000L;
                }
            }


                // read !TextUtils.isEmpty(viewModel.message) ? viewModel.message.equals(Constants.SHOW_PROGRESS) : false ? View.VISIBLE : View.GONE
                textUtilsIsEmptyViewModelMessageViewModelMessageEqualsConstantsSHOWPROGRESSBooleanFalseViewVISIBLEViewGONE = ((textUtilsIsEmptyViewModelMessageViewModelMessageEqualsConstantsSHOWPROGRESSBooleanFalse) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
                // read TextUtils.isEmpty(viewModel.message) ? true : viewModel.message.equals(Constants.HIDE_PROGRESS) ? true : false
                textUtilsIsEmptyViewModelMessageBooleanTrueViewModelMessageEqualsConstantsHIDEPROGRESSBooleanTrueBooleanFalse = ((textUtilsIsEmptyViewModelMessageBooleanTrueViewModelMessageEqualsConstantsHIDEPROGRESS) ? (true) : (false));
                // read TextUtils.isEmpty(viewModel.message) ? true : viewModel.message.equals(Constants.HIDE_PROGRESS) ? @android:drawable/corner_view_gradient : @android:drawable/corner_view_primary_medium
                textUtilsIsEmptyViewModelMessageBooleanTrueViewModelMessageEqualsConstantsHIDEPROGRESSBtnPhoneAndroidDrawableCornerViewGradientBtnPhoneAndroidDrawableCornerViewPrimaryMedium = ((textUtilsIsEmptyViewModelMessageBooleanTrueViewModelMessageEqualsConstantsHIDEPROGRESS) ? (androidx.appcompat.content.res.AppCompatResources.getDrawable(btnPhone.getContext(), R.drawable.corner_view_gradient)) : (androidx.appcompat.content.res.AppCompatResources.getDrawable(btnPhone.getContext(), R.drawable.corner_view_primary_medium)));
        }
        if ((dirtyFlags & 0x39L) != 0) {

                // read viewModel.createServiceOrder.transferService == 0.0 ? !TextUtils.isEmpty(viewModel.message) : false ? viewModel.message.equals(Constants.SHOW_PROGRESS) : false
                viewModelCreateServiceOrderTransferServiceFloat00TextUtilsIsEmptyViewModelMessageBooleanFalseViewModelMessageEqualsConstantsSHOWPROGRESSBooleanFalse = ((viewModelCreateServiceOrderTransferServiceFloat00TextUtilsIsEmptyViewModelMessageBooleanFalse) ? (viewModelMessageEqualsConstantsSHOWPROGRESS) : (false));
            if((dirtyFlags & 0x39L) != 0) {
                if(viewModelCreateServiceOrderTransferServiceFloat00TextUtilsIsEmptyViewModelMessageBooleanFalseViewModelMessageEqualsConstantsSHOWPROGRESSBooleanFalse) {
                        dirtyFlags |= 0x80L;
                }
                else {
                        dirtyFlags |= 0x40L;
                }
            }


                // read viewModel.createServiceOrder.transferService == 0.0 ? !TextUtils.isEmpty(viewModel.message) : false ? viewModel.message.equals(Constants.SHOW_PROGRESS) : false ? View.VISIBLE : View.GONE
                viewModelCreateServiceOrderTransferServiceFloat00TextUtilsIsEmptyViewModelMessageBooleanFalseViewModelMessageEqualsConstantsSHOWPROGRESSBooleanFalseViewVISIBLEViewGONE = ((viewModelCreateServiceOrderTransferServiceFloat00TextUtilsIsEmptyViewModelMessageBooleanFalseViewModelMessageEqualsConstantsSHOWPROGRESSBooleanFalse) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
        }
        // batch finished
        if ((dirtyFlags & 0x20L) != 0) {
            // api target 1

            this.back.setOnClickListener(mCallback221);
            this.btnPhone.setOnClickListener(mCallback222);
        }
        if ((dirtyFlags & 0x31L) != 0) {
            // api target 1

            androidx.databinding.adapters.ViewBindingAdapter.setBackground(this.btnPhone, textUtilsIsEmptyViewModelMessageBooleanTrueViewModelMessageEqualsConstantsHIDEPROGRESSBtnPhoneAndroidDrawableCornerViewGradientBtnPhoneAndroidDrawableCornerViewPrimaryMedium);
            this.btnPhone.setEnabled(textUtilsIsEmptyViewModelMessageBooleanTrueViewModelMessageEqualsConstantsHIDEPROGRESSBooleanTrueBooleanFalse);
            this.progress.setVisibility(textUtilsIsEmptyViewModelMessageViewModelMessageEqualsConstantsSHOWPROGRESSBooleanFalseViewVISIBLEViewGONE);
        }
        if ((dirtyFlags & 0x21L) != 0) {
            // api target 1

            this.cardDesc.setVisibility(textUtilsIsEmptyViewModelServiceOrderRequestDescViewVISIBLEViewGONE);
            this.cardRequiredAdditional.setVisibility(textUtilsIsEmptyViewModelServiceOrderRequestCarTypeBooleanTrueTextUtilsIsEmptyViewModelServiceOrderRequestHiddenTypeViewVISIBLEViewGONE);
            this.cardToLocation.setVisibility(textUtilsIsEmptyViewModelServiceOrderRequestFromAddressViewVISIBLEViewGONE);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView79, viewModelServiceOrderRequestFromAddress);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView82, viewModelServiceOrderRequestToAddress);
            androidx.databinding.adapters.RadioGroupBindingAdapter.setListeners(this.paymentRadioGroup, (android.widget.RadioGroup.OnCheckedChangeListener)viewModelOnPaymentChangeAndroidWidgetRadioGroupOnCheckedChangeListener, (androidx.databinding.InverseBindingListener)null);
            this.rcExtraServices.setVisibility(viewModelServiceOrderRequestExtraListJavaLangObjectNullViewModelServiceOrderRequestExtraListSizeInt0BooleanFalseViewVISIBLEViewGONE);
            this.tvBatterySize.setVisibility(textUtilsIsEmptyViewModelServiceOrderRequestBatterySizeViewVISIBLEViewGONE);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvBatterySizeValue, viewModelServiceOrderRequestBatterySize);
            this.tvBatterySizeValue.setVisibility(textUtilsIsEmptyViewModelServiceOrderRequestBatterySizeViewVISIBLEViewGONE);
            this.tvBatteryType.setVisibility(textUtilsIsEmptyViewModelServiceOrderRequestBatteryTypeViewVISIBLEViewGONE);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvBatteryTypeValue, viewModelServiceOrderRequestBatteryType);
            this.tvBatteryTypeValue.setVisibility(textUtilsIsEmptyViewModelServiceOrderRequestBatteryTypeViewVISIBLEViewGONE);
            this.tvBoxTypes.setVisibility(textUtilsIsEmptyViewModelServiceOrderRequestBoxTypeViewVISIBLEViewGONE);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvBoxValue, viewModelServiceOrderRequestBoxType);
            this.tvBoxValue.setVisibility(textUtilsIsEmptyViewModelServiceOrderRequestBoxTypeViewVISIBLEViewGONE);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvBuildingYear, viewModelServiceOrderRequestMainServiceInt11TvBuildingYearAndroidStringMakingYearTvBuildingYearAndroidStringCarModels);
            this.tvBuildingYear.setVisibility(textUtilsIsEmptyViewModelServiceOrderRequestCarModelViewVISIBLEViewGONE);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvCarBuildingYearValue, viewModelServiceOrderRequestCarModel);
            this.tvCarBuildingYearValue.setVisibility(textUtilsIsEmptyViewModelServiceOrderRequestCarModelViewVISIBLEViewGONE);
            this.tvCarModel.setVisibility(textUtilsIsEmptyViewModelServiceOrderRequestCarCatViewVISIBLEViewGONE);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvCarModelValue, viewModelServiceOrderRequestCarCat);
            this.tvCarModelValue.setVisibility(textUtilsIsEmptyViewModelServiceOrderRequestCarCatViewVISIBLEViewGONE);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvCarMotorNumberValue, viewModelServiceOrderRequestCarMotor);
            this.tvCarMotorNumberValue.setVisibility(textUtilsIsEmptyViewModelServiceOrderRequestCarMotorViewVISIBLEViewGONE);
            this.tvCarTireDesc.setVisibility(textUtilsIsEmptyViewModelServiceOrderRequestTyerDescViewVISIBLEViewGONE);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvCarTireDescValue, viewModelServiceOrderRequestTyerDesc);
            this.tvCarTireDescValue.setVisibility(textUtilsIsEmptyViewModelServiceOrderRequestTyerDescViewVISIBLEViewGONE);
            this.tvCarTireType.setVisibility(textUtilsIsEmptyViewModelServiceOrderRequestTyerTypeViewVISIBLEViewGONE);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvCarTireTypeValue, viewModelServiceOrderRequestTyerType);
            this.tvCarTireTypeValue.setVisibility(textUtilsIsEmptyViewModelServiceOrderRequestTyerTypeViewVISIBLEViewGONE);
            this.tvCarTypes.setVisibility(textUtilsIsEmptyViewModelServiceOrderRequestCarTypeViewVISIBLEViewGONE);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvCarTypesValue, viewModelServiceOrderRequestCarType);
            this.tvCarTypesValue.setVisibility(textUtilsIsEmptyViewModelServiceOrderRequestCarTypeViewVISIBLEViewGONE);
            this.tvChoosenExtraOrder.setVisibility(viewModelServiceOrderRequestExtraListJavaLangObjectNullViewModelServiceOrderRequestExtraListSizeInt0BooleanFalseViewVISIBLEViewGONE);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvDate, viewModelServiceOrderRequestDateConcatJavaLangStringConcatViewModelServiceOrderRequestTime);
            this.tvDate.setVisibility(viewModelServiceOrderRequestEmerengcyViewGONEViewVISIBLE);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvDescText, viewModelServiceOrderRequestDesc);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvEmePrice, stringValueOfViewModelServiceOrderRequestEmePriceServicesConcatJavaLangStringConcatViewModelCurrency);
            this.tvFuelCat.setVisibility(textUtilsIsEmptyViewModelServiceOrderRequestFuelCatViewVISIBLEViewGONE);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvFuelCatValue, viewModelServiceOrderRequestFuelCat);
            this.tvFuelCatValue.setVisibility(textUtilsIsEmptyViewModelServiceOrderRequestFuelCatViewVISIBLEViewGONE);
            this.tvFuelType.setVisibility(textUtilsIsEmptyViewModelServiceOrderRequestFuelTypeViewVISIBLEViewGONE);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvFuelTypeValue, viewModelServiceOrderRequestFuelType);
            this.tvFuelTypeValue.setVisibility(textUtilsIsEmptyViewModelServiceOrderRequestFuelTypeViewVISIBLEViewGONE);
            this.tvGallon.setVisibility(textUtilsIsEmptyViewModelServiceOrderRequestGallonViewVISIBLEViewGONE);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvGallonValue, viewModelServiceOrderRequestGallon);
            this.tvGallonValue.setVisibility(textUtilsIsEmptyViewModelServiceOrderRequestGallonViewVISIBLEViewGONE);
            this.tvHiddenColor.setVisibility(textUtilsIsEmptyViewModelServiceOrderRequestHiddenColorViewVISIBLEViewGONE);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvHiddenColorValue, viewModelServiceOrderRequestHiddenColor);
            this.tvHiddenColorValue.setVisibility(textUtilsIsEmptyViewModelServiceOrderRequestHiddenColorViewVISIBLEViewGONE);
            this.tvHiddenPerDesc.setVisibility(textUtilsIsEmptyViewModelServiceOrderRequestHiddenPercentageViewVISIBLEViewGONE);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvHiddenPerValue, viewModelServiceOrderRequestHiddenPercentage);
            this.tvHiddenPerValue.setVisibility(textUtilsIsEmptyViewModelServiceOrderRequestHiddenPercentageViewVISIBLEViewGONE);
            this.tvHiddenTypes.setVisibility(textUtilsIsEmptyViewModelServiceOrderRequestHiddenTypeViewVISIBLEViewGONE);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvHiddenValue, viewModelServiceOrderRequestHiddenType);
            this.tvHiddenValue.setVisibility(textUtilsIsEmptyViewModelServiceOrderRequestHiddenTypeViewVISIBLEViewGONE);
            this.tvKilo.setVisibility(textUtilsIsEmptyViewModelServiceOrderRequestKmViewVISIBLEViewGONE);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvKiloValue, viewModelServiceOrderRequestKm);
            this.tvKiloValue.setVisibility(textUtilsIsEmptyViewModelServiceOrderRequestKmViewVISIBLEViewGONE);
            this.tvLitre.setVisibility(textUtilsIsEmptyViewModelServiceOrderRequestLitreViewVISIBLEViewGONE);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvLitreValue, viewModelServiceOrderRequestLitre);
            this.tvLitreValue.setVisibility(textUtilsIsEmptyViewModelServiceOrderRequestLitreViewVISIBLEViewGONE);
            this.tvMotorNumber.setVisibility(textUtilsIsEmptyViewModelServiceOrderRequestCarMotorViewVISIBLEViewGONE);
            this.tvOilLiquid.setVisibility(textUtilsIsEmptyViewModelServiceOrderRequestOilLiquidViewVISIBLEViewGONE);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvOilLiquidValue, viewModelServiceOrderRequestOilLiquid);
            this.tvOilLiquidValue.setVisibility(textUtilsIsEmptyViewModelServiceOrderRequestOilLiquidViewVISIBLEViewGONE);
            this.tvOilType.setVisibility(textUtilsIsEmptyViewModelServiceOrderRequestOilTypeViewVISIBLEViewGONE);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvOilTypeValue, viewModelServiceOrderRequestOilType);
            this.tvOilTypeValue.setVisibility(textUtilsIsEmptyViewModelServiceOrderRequestOilTypeViewVISIBLEViewGONE);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvRequiredAdditional, textUtilsIsEmptyViewModelServiceOrderRequestBuildingYearTvRequiredAdditionalAndroidStringCarDescTvRequiredAdditionalAndroidStringAdditionalDesc);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvServicesExtraPrice, stringValueOfViewModelServiceOrderRequestExtraTotalServicesConcatJavaLangStringConcatViewModelCurrency);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvServicesPrice, stringValueOfViewModelServiceOrderRequestTotalServicesConcatJavaLangStringConcatViewModelCurrency);
            this.tvTinkerServiceType.setVisibility(textUtilsIsEmptyViewModelServiceOrderRequestLitreViewVISIBLEViewGONE);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvTinkerServiceTypeValue, viewModelServiceOrderRequestTinkerServiceName);
            this.tvTinkerServiceTypeValue.setVisibility(textUtilsIsEmptyViewModelServiceOrderRequestLitreViewVISIBLEViewGONE);
            this.tvUrgent.setVisibility(viewModelServiceOrderRequestEmerengcyViewVISIBLEViewGONE);
            this.vBatterySize.setVisibility(textUtilsIsEmptyViewModelServiceOrderRequestBatterySizeViewVISIBLEViewGONE);
            this.vBatteryType.setVisibility(textUtilsIsEmptyViewModelServiceOrderRequestBatteryTypeViewVISIBLEViewGONE);
            this.vBoxValue.setVisibility(textUtilsIsEmptyViewModelServiceOrderRequestBoxTypeViewVISIBLEViewGONE);
            this.vBuildingYear.setVisibility(textUtilsIsEmptyViewModelServiceOrderRequestCarModelViewVISIBLEViewGONE);
            this.vCarModelValue.setVisibility(textUtilsIsEmptyViewModelServiceOrderRequestCarCatViewVISIBLEViewGONE);
            this.vCarTireDescValue.setVisibility(textUtilsIsEmptyViewModelServiceOrderRequestTyerDescViewVISIBLEViewGONE);
            this.vCarTypesValue.setVisibility(textUtilsIsEmptyViewModelServiceOrderRequestCarTypeViewVISIBLEViewGONE);
            this.vChoosenExtraOrder.setVisibility(viewModelServiceOrderRequestExtraListJavaLangObjectNullViewModelServiceOrderRequestExtraListSizeInt0BooleanFalseViewVISIBLEViewGONE);
            this.vFuelCat.setVisibility(textUtilsIsEmptyViewModelServiceOrderRequestFuelCatViewVISIBLEViewGONE);
            this.vFuelType.setVisibility(textUtilsIsEmptyViewModelServiceOrderRequestFuelTypeViewVISIBLEViewGONE);
            this.vGallon.setVisibility(textUtilsIsEmptyViewModelServiceOrderRequestLitreViewVISIBLEViewGONE);
            this.vHiddenColorValue.setVisibility(textUtilsIsEmptyViewModelServiceOrderRequestHiddenColorViewVISIBLEViewGONE);
            this.vHiddenPerValue.setVisibility(textUtilsIsEmptyViewModelServiceOrderRequestHiddenPercentageViewVISIBLEViewGONE);
            this.vHiddenValue.setVisibility(textUtilsIsEmptyViewModelServiceOrderRequestHiddenTypeViewVISIBLEViewGONE);
            this.vLitre.setVisibility(textUtilsIsEmptyViewModelServiceOrderRequestLitreViewVISIBLEViewGONE);
            this.vMotorNumber.setVisibility(textUtilsIsEmptyViewModelServiceOrderRequestCarMotorViewVISIBLEViewGONE);
            this.vOilLiquid.setVisibility(textUtilsIsEmptyViewModelServiceOrderRequestOilLiquidViewVISIBLEViewGONE);
            this.vOilType.setVisibility(textUtilsIsEmptyViewModelServiceOrderRequestOilTypeViewVISIBLEViewGONE);
            this.vTinkerServiceType.setVisibility(textUtilsIsEmptyViewModelServiceOrderRequestLitreViewVISIBLEViewGONE);
            this.vTyreValue.setVisibility(textUtilsIsEmptyViewModelServiceOrderRequestTyerTypeViewVISIBLEViewGONE);
        }
        if ((dirtyFlags & 0x39L) != 0) {
            // api target 1

            this.mboundView36.setVisibility(viewModelCreateServiceOrderTransferServiceFloat00TextUtilsIsEmptyViewModelMessageBooleanFalseViewModelMessageEqualsConstantsSHOWPROGRESSBooleanFalseViewVISIBLEViewGONE);
        }
        if ((dirtyFlags & 0x25L) != 0) {
            // api target 1

            grand.app.aber_user.base.ApplicationBinding.getItemsV2Binding(this.rcExtraServices, viewModelExtraAdapter, "1", "1");
        }
        if ((dirtyFlags & 0x23L) != 0) {
            // api target 1

            grand.app.aber_user.base.ApplicationBinding.getItemsV2Binding(this.rcServices, viewModelDetailsAdapter, "1", "1");
        }
        if ((dirtyFlags & 0x29L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvDeliveryPrice, stringValueOfViewModelCreateServiceOrderTransferServiceConcatJavaLangStringConcatViewModelCurrency);
            this.tvDistance.setVisibility(viewModelCreateServiceOrderDistanceFloat00ViewGONEViewVISIBLE);
            this.tvDistanceValue.setVisibility(viewModelCreateServiceOrderDistanceFloat00ViewGONEViewVISIBLE);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvDistanceValue, stringValueOfViewModelCreateServiceOrderDistanceConcatJavaLangStringConcatTvDistanceValueAndroidStringKmValue);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvTaxesPrice, stringValueOfViewModelCreateServiceOrderTaxesConcatJavaLangStringConcatViewModelCurrency);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvTotalPrice, stringValueOfViewModelCreateServiceOrderTotalConcatJavaLangStringConcatViewModelCurrency);
            this.vDistance.setVisibility(viewModelCreateServiceOrderDistanceFloat00ViewGONEViewVISIBLE);
        }
    }
    // Listener Stub Implementations
    public static class OnCheckedChangeListenerImpl implements android.widget.RadioGroup.OnCheckedChangeListener{
        private grand.app.aber_user.pages.services.viewModels.ServiceConfirmOrderViewModels value;
        public OnCheckedChangeListenerImpl setValue(grand.app.aber_user.pages.services.viewModels.ServiceConfirmOrderViewModels value) {
            this.value = value;
            return value == null ? null : this;
        }
        @Override
        public void onCheckedChanged(android.widget.RadioGroup arg0, int arg1) {
            this.value.onPaymentChange(arg0, arg1); 
        }
    }
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        switch(sourceId) {
            case 1: {
                // localize variables for thread safety
                // viewModel
                grand.app.aber_user.pages.services.viewModels.ServiceConfirmOrderViewModels viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {



                    viewModel.goBack(getRoot().getContext());
                }
                break;
            }
            case 2: {
                // localize variables for thread safety
                // viewModel
                grand.app.aber_user.pages.services.viewModels.ServiceConfirmOrderViewModels viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {


                    viewModel.confirmServiceOrder();
                }
                break;
            }
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    private  long mDirtyFlags_1 = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): viewModel
        flag 1 (0x2L): viewModel.detailsAdapter
        flag 2 (0x3L): viewModel.extraAdapter
        flag 3 (0x4L): viewModel.createServiceOrder
        flag 4 (0x5L): viewModel.message
        flag 5 (0x6L): null
        flag 6 (0x7L): viewModel.createServiceOrder.transferService == 0.0 ? !TextUtils.isEmpty(viewModel.message) : false ? viewModel.message.equals(Constants.SHOW_PROGRESS) : false ? View.VISIBLE : View.GONE
        flag 7 (0x8L): viewModel.createServiceOrder.transferService == 0.0 ? !TextUtils.isEmpty(viewModel.message) : false ? viewModel.message.equals(Constants.SHOW_PROGRESS) : false ? View.VISIBLE : View.GONE
        flag 8 (0x9L): viewModel.createServiceOrder.transferService == 0.0 ? !TextUtils.isEmpty(viewModel.message) : false
        flag 9 (0xaL): viewModel.createServiceOrder.transferService == 0.0 ? !TextUtils.isEmpty(viewModel.message) : false
        flag 10 (0xbL): !TextUtils.isEmpty(viewModel.message) ? viewModel.message.equals(Constants.SHOW_PROGRESS) : false
        flag 11 (0xcL): !TextUtils.isEmpty(viewModel.message) ? viewModel.message.equals(Constants.SHOW_PROGRESS) : false
        flag 12 (0xdL): !TextUtils.isEmpty(viewModel.serviceOrderRequest.hiddenColor) ? View.VISIBLE : View.GONE
        flag 13 (0xeL): !TextUtils.isEmpty(viewModel.serviceOrderRequest.hiddenColor) ? View.VISIBLE : View.GONE
        flag 14 (0xfL): !TextUtils.isEmpty(viewModel.serviceOrderRequest.carCat) ? View.VISIBLE : View.GONE
        flag 15 (0x10L): !TextUtils.isEmpty(viewModel.serviceOrderRequest.carCat) ? View.VISIBLE : View.GONE
        flag 16 (0x11L): !TextUtils.isEmpty(viewModel.serviceOrderRequest.batteryType) ? View.VISIBLE : View.GONE
        flag 17 (0x12L): !TextUtils.isEmpty(viewModel.serviceOrderRequest.batteryType) ? View.VISIBLE : View.GONE
        flag 18 (0x13L): !TextUtils.isEmpty(viewModel.serviceOrderRequest.carModel) ? View.VISIBLE : View.GONE
        flag 19 (0x14L): !TextUtils.isEmpty(viewModel.serviceOrderRequest.carModel) ? View.VISIBLE : View.GONE
        flag 20 (0x15L): viewModel.serviceOrderRequest.emerengcy ? View.GONE : View.VISIBLE
        flag 21 (0x16L): viewModel.serviceOrderRequest.emerengcy ? View.GONE : View.VISIBLE
        flag 22 (0x17L): viewModel.createServiceOrder.transferService == 0.0 ? !TextUtils.isEmpty(viewModel.message) : false ? viewModel.message.equals(Constants.SHOW_PROGRESS) : false
        flag 23 (0x18L): viewModel.createServiceOrder.transferService == 0.0 ? !TextUtils.isEmpty(viewModel.message) : false ? viewModel.message.equals(Constants.SHOW_PROGRESS) : false
        flag 24 (0x19L): viewModel.createServiceOrder.distance == 0.0 ? View.GONE : View.VISIBLE
        flag 25 (0x1aL): viewModel.createServiceOrder.distance == 0.0 ? View.GONE : View.VISIBLE
        flag 26 (0x1bL): TextUtils.isEmpty(viewModel.message) ? true : viewModel.message.equals(Constants.HIDE_PROGRESS) ? true : false
        flag 27 (0x1cL): TextUtils.isEmpty(viewModel.message) ? true : viewModel.message.equals(Constants.HIDE_PROGRESS) ? true : false
        flag 28 (0x1dL): !TextUtils.isEmpty(viewModel.serviceOrderRequest.carType) ? View.VISIBLE : View.GONE
        flag 29 (0x1eL): !TextUtils.isEmpty(viewModel.serviceOrderRequest.carType) ? View.VISIBLE : View.GONE
        flag 30 (0x1fL): viewModel.serviceOrderRequest.extraList != null ? viewModel.serviceOrderRequest.extraList.size() > 0 : false ? View.VISIBLE : View.GONE
        flag 31 (0x20L): viewModel.serviceOrderRequest.extraList != null ? viewModel.serviceOrderRequest.extraList.size() > 0 : false ? View.VISIBLE : View.GONE
        flag 32 (0x21L): !TextUtils.isEmpty(viewModel.serviceOrderRequest.tyerDesc) ? View.VISIBLE : View.GONE
        flag 33 (0x22L): !TextUtils.isEmpty(viewModel.serviceOrderRequest.tyerDesc) ? View.VISIBLE : View.GONE
        flag 34 (0x23L): !TextUtils.isEmpty(viewModel.serviceOrderRequest.tyerType) ? View.VISIBLE : View.GONE
        flag 35 (0x24L): !TextUtils.isEmpty(viewModel.serviceOrderRequest.tyerType) ? View.VISIBLE : View.GONE
        flag 36 (0x25L): viewModel.serviceOrderRequest.mainService == 11 ? @android:string/making_year : @android:string/car_models
        flag 37 (0x26L): viewModel.serviceOrderRequest.mainService == 11 ? @android:string/making_year : @android:string/car_models
        flag 38 (0x27L): !TextUtils.isEmpty(viewModel.message) ? viewModel.message.equals(Constants.SHOW_PROGRESS) : false ? View.VISIBLE : View.GONE
        flag 39 (0x28L): !TextUtils.isEmpty(viewModel.message) ? viewModel.message.equals(Constants.SHOW_PROGRESS) : false ? View.VISIBLE : View.GONE
        flag 40 (0x29L): !TextUtils.isEmpty(viewModel.serviceOrderRequest.gallon) ? View.VISIBLE : View.GONE
        flag 41 (0x2aL): !TextUtils.isEmpty(viewModel.serviceOrderRequest.gallon) ? View.VISIBLE : View.GONE
        flag 42 (0x2bL): !TextUtils.isEmpty(viewModel.serviceOrderRequest.carMotor) ? View.VISIBLE : View.GONE
        flag 43 (0x2cL): !TextUtils.isEmpty(viewModel.serviceOrderRequest.carMotor) ? View.VISIBLE : View.GONE
        flag 44 (0x2dL): !TextUtils.isEmpty(viewModel.serviceOrderRequest.oilLiquid) ? View.VISIBLE : View.GONE
        flag 45 (0x2eL): !TextUtils.isEmpty(viewModel.serviceOrderRequest.oilLiquid) ? View.VISIBLE : View.GONE
        flag 46 (0x2fL): viewModel.serviceOrderRequest.extraList != null ? viewModel.serviceOrderRequest.extraList.size() > 0 : false
        flag 47 (0x30L): viewModel.serviceOrderRequest.extraList != null ? viewModel.serviceOrderRequest.extraList.size() > 0 : false
        flag 48 (0x31L): !TextUtils.isEmpty(viewModel.serviceOrderRequest.hiddenPercentage) ? View.VISIBLE : View.GONE
        flag 49 (0x32L): !TextUtils.isEmpty(viewModel.serviceOrderRequest.hiddenPercentage) ? View.VISIBLE : View.GONE
        flag 50 (0x33L): TextUtils.isEmpty(viewModel.message) ? true : viewModel.message.equals(Constants.HIDE_PROGRESS)
        flag 51 (0x34L): TextUtils.isEmpty(viewModel.message) ? true : viewModel.message.equals(Constants.HIDE_PROGRESS)
        flag 52 (0x35L): viewModel.serviceOrderRequest.emerengcy ? View.VISIBLE : View.GONE
        flag 53 (0x36L): viewModel.serviceOrderRequest.emerengcy ? View.VISIBLE : View.GONE
        flag 54 (0x37L): !TextUtils.isEmpty(viewModel.serviceOrderRequest.fromAddress) ? View.VISIBLE : View.GONE
        flag 55 (0x38L): !TextUtils.isEmpty(viewModel.serviceOrderRequest.fromAddress) ? View.VISIBLE : View.GONE
        flag 56 (0x39L): !TextUtils.isEmpty(viewModel.serviceOrderRequest.km) ? View.VISIBLE : View.GONE
        flag 57 (0x3aL): !TextUtils.isEmpty(viewModel.serviceOrderRequest.km) ? View.VISIBLE : View.GONE
        flag 58 (0x3bL): TextUtils.isEmpty(viewModel.message) ? true : viewModel.message.equals(Constants.HIDE_PROGRESS) ? @android:drawable/corner_view_gradient : @android:drawable/corner_view_primary_medium
        flag 59 (0x3cL): TextUtils.isEmpty(viewModel.message) ? true : viewModel.message.equals(Constants.HIDE_PROGRESS) ? @android:drawable/corner_view_gradient : @android:drawable/corner_view_primary_medium
        flag 60 (0x3dL): !TextUtils.isEmpty(viewModel.serviceOrderRequest.litre) ? View.VISIBLE : View.GONE
        flag 61 (0x3eL): !TextUtils.isEmpty(viewModel.serviceOrderRequest.litre) ? View.VISIBLE : View.GONE
        flag 62 (0x3fL): !TextUtils.isEmpty(viewModel.serviceOrderRequest.hiddenType) ? View.VISIBLE : View.GONE
        flag 63 (0x40L): !TextUtils.isEmpty(viewModel.serviceOrderRequest.hiddenType) ? View.VISIBLE : View.GONE
        flag 64 (0x41L): !TextUtils.isEmpty(viewModel.serviceOrderRequest.buildingYear) ? @android:string/car_desc : @android:string/additional_desc
        flag 65 (0x42L): !TextUtils.isEmpty(viewModel.serviceOrderRequest.buildingYear) ? @android:string/car_desc : @android:string/additional_desc
        flag 66 (0x43L): !TextUtils.isEmpty(viewModel.serviceOrderRequest.boxType) ? View.VISIBLE : View.GONE
        flag 67 (0x44L): !TextUtils.isEmpty(viewModel.serviceOrderRequest.boxType) ? View.VISIBLE : View.GONE
        flag 68 (0x45L): !TextUtils.isEmpty(viewModel.serviceOrderRequest.fuelCat) ? View.VISIBLE : View.GONE
        flag 69 (0x46L): !TextUtils.isEmpty(viewModel.serviceOrderRequest.fuelCat) ? View.VISIBLE : View.GONE
        flag 70 (0x47L): !TextUtils.isEmpty(viewModel.serviceOrderRequest.fuelType) ? View.VISIBLE : View.GONE
        flag 71 (0x48L): !TextUtils.isEmpty(viewModel.serviceOrderRequest.fuelType) ? View.VISIBLE : View.GONE
        flag 72 (0x49L): !TextUtils.isEmpty(viewModel.serviceOrderRequest.batterySize) ? View.VISIBLE : View.GONE
        flag 73 (0x4aL): !TextUtils.isEmpty(viewModel.serviceOrderRequest.batterySize) ? View.VISIBLE : View.GONE
        flag 74 (0x4bL): !TextUtils.isEmpty(viewModel.serviceOrderRequest.desc) ? View.VISIBLE : View.GONE
        flag 75 (0x4cL): !TextUtils.isEmpty(viewModel.serviceOrderRequest.desc) ? View.VISIBLE : View.GONE
        flag 76 (0x4dL): !TextUtils.isEmpty(viewModel.serviceOrderRequest.oilType) ? View.VISIBLE : View.GONE
        flag 77 (0x4eL): !TextUtils.isEmpty(viewModel.serviceOrderRequest.oilType) ? View.VISIBLE : View.GONE
        flag 78 (0x4fL): !TextUtils.isEmpty(viewModel.serviceOrderRequest.carType) ? true : !TextUtils.isEmpty(viewModel.serviceOrderRequest.hiddenType)
        flag 79 (0x50L): !TextUtils.isEmpty(viewModel.serviceOrderRequest.carType) ? true : !TextUtils.isEmpty(viewModel.serviceOrderRequest.hiddenType)
        flag 80 (0x51L): !TextUtils.isEmpty(viewModel.serviceOrderRequest.carType) ? true : !TextUtils.isEmpty(viewModel.serviceOrderRequest.hiddenType) ? View.VISIBLE : View.GONE
        flag 81 (0x52L): !TextUtils.isEmpty(viewModel.serviceOrderRequest.carType) ? true : !TextUtils.isEmpty(viewModel.serviceOrderRequest.hiddenType) ? View.VISIBLE : View.GONE
    flag mapping end*/
    //end
}