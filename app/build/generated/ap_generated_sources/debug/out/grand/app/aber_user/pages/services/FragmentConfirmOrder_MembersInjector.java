// Generated by Dagger (https://dagger.dev).
package grand.app.aber_user.pages.services;

import dagger.MembersInjector;
import dagger.internal.InjectedFieldSignature;
import grand.app.aber_user.pages.services.viewModels.ServiceConfirmOrderViewModels;
import javax.inject.Provider;

@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class FragmentConfirmOrder_MembersInjector implements MembersInjector<FragmentConfirmOrder> {
  private final Provider<ServiceConfirmOrderViewModels> viewModelProvider;

  public FragmentConfirmOrder_MembersInjector(
      Provider<ServiceConfirmOrderViewModels> viewModelProvider) {
    this.viewModelProvider = viewModelProvider;
  }

  public static MembersInjector<FragmentConfirmOrder> create(
      Provider<ServiceConfirmOrderViewModels> viewModelProvider) {
    return new FragmentConfirmOrder_MembersInjector(viewModelProvider);
  }

  @Override
  public void injectMembers(FragmentConfirmOrder instance) {
    injectViewModel(instance, viewModelProvider.get());
  }

  @InjectedFieldSignature("grand.app.aber_user.pages.services.FragmentConfirmOrder.viewModel")
  public static void injectViewModel(FragmentConfirmOrder instance,
      ServiceConfirmOrderViewModels viewModel) {
    instance.viewModel = viewModel;
  }
}
