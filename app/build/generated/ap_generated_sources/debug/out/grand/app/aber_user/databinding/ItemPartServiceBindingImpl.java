package grand.app.aber_user.databinding;
import grand.app.aber_user.R;
import grand.app.aber_user.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ItemPartServiceBindingImpl extends ItemPartServiceBinding implements grand.app.aber_user.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = null;
    }
    // views
    @NonNull
    private final androidx.cardview.widget.CardView mboundView0;
    @NonNull
    private final grand.app.aber_user.customViews.views.CustomTextViewMedium mboundView6;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback39;
    @Nullable
    private final android.view.View.OnClickListener mCallback37;
    @Nullable
    private final android.view.View.OnClickListener mCallback38;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ItemPartServiceBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 7, sIncludes, sViewsWithIds));
    }
    private ItemPartServiceBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 1
            , (androidx.appcompat.widget.AppCompatImageView) bindings[1]
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[3]
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[5]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[2]
            , (androidx.appcompat.widget.AppCompatRatingBar) bindings[4]
            );
        this.icPartsImage.setTag(null);
        this.mboundView0 = (androidx.cardview.widget.CardView) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView6 = (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[6];
        this.mboundView6.setTag(null);
        this.tvPartsName.setTag(null);
        this.tvPartsPrice.setTag(null);
        this.ucFavorite.setTag(null);
        this.userItemCode.setTag(null);
        setRootTag(root);
        // listeners
        mCallback39 = new grand.app.aber_user.generated.callback.OnClickListener(this, 3);
        mCallback37 = new grand.app.aber_user.generated.callback.OnClickListener(this, 1);
        mCallback38 = new grand.app.aber_user.generated.callback.OnClickListener(this, 2);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.itemViewModel == variableId) {
            setItemViewModel((grand.app.aber_user.pages.parts.viewModels.ItemProductsViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setItemViewModel(@Nullable grand.app.aber_user.pages.parts.viewModels.ItemProductsViewModel ItemViewModel) {
        updateRegistration(0, ItemViewModel);
        this.mItemViewModel = ItemViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.itemViewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeItemViewModel((grand.app.aber_user.pages.parts.viewModels.ItemProductsViewModel) object, fieldId);
        }
        return false;
    }
    private boolean onChangeItemViewModel(grand.app.aber_user.pages.parts.viewModels.ItemProductsViewModel ItemViewModel, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        else if (fieldId == BR.productsItem) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        boolean itemViewModelProductsItemIsFavorite = false;
        grand.app.aber_user.pages.parts.models.ProductsItem itemViewModelProductsItem = null;
        android.graphics.drawable.Drawable itemViewModelProductsItemIsFavoriteUcFavoriteAndroidDrawableIcLikeFillUcFavoriteAndroidDrawableIcLikeEmpty = null;
        java.lang.String itemViewModelProductsItemImage = null;
        java.lang.String itemViewModelProductsItemName = null;
        java.lang.String itemViewModelProductsItemPrice = null;
        grand.app.aber_user.pages.parts.viewModels.ItemProductsViewModel itemViewModel = mItemViewModel;
        java.lang.String itemViewModelProductsItemCurrency = null;
        java.lang.String itemViewModelProductsItemPriceConcatJavaLangStringConcatItemViewModelProductsItemCurrency = null;
        java.lang.String itemViewModelProductsItemRate = null;
        java.lang.String itemViewModelProductsItemPriceConcatJavaLangString = null;

        if ((dirtyFlags & 0x7L) != 0) {



                if (itemViewModel != null) {
                    // read itemViewModel.productsItem
                    itemViewModelProductsItem = itemViewModel.getProductsItem();
                }


                if (itemViewModelProductsItem != null) {
                    // read itemViewModel.productsItem.isFavorite
                    itemViewModelProductsItemIsFavorite = itemViewModelProductsItem.isIsFavorite();
                    // read itemViewModel.productsItem.image
                    itemViewModelProductsItemImage = itemViewModelProductsItem.getImage();
                    // read itemViewModel.productsItem.name
                    itemViewModelProductsItemName = itemViewModelProductsItem.getName();
                    // read itemViewModel.productsItem.price
                    itemViewModelProductsItemPrice = itemViewModelProductsItem.getPrice();
                    // read itemViewModel.productsItem.currency
                    itemViewModelProductsItemCurrency = itemViewModelProductsItem.getCurrency();
                    // read itemViewModel.productsItem.rate
                    itemViewModelProductsItemRate = itemViewModelProductsItem.getRate();
                }
            if((dirtyFlags & 0x7L) != 0) {
                if(itemViewModelProductsItemIsFavorite) {
                        dirtyFlags |= 0x10L;
                }
                else {
                        dirtyFlags |= 0x8L;
                }
            }


                // read itemViewModel.productsItem.isFavorite ? @android:drawable/ic_like_fill : @android:drawable/ic_like_empty
                itemViewModelProductsItemIsFavoriteUcFavoriteAndroidDrawableIcLikeFillUcFavoriteAndroidDrawableIcLikeEmpty = ((itemViewModelProductsItemIsFavorite) ? (androidx.appcompat.content.res.AppCompatResources.getDrawable(ucFavorite.getContext(), R.drawable.ic_like_fill)) : (androidx.appcompat.content.res.AppCompatResources.getDrawable(ucFavorite.getContext(), R.drawable.ic_like_empty)));
                if (itemViewModelProductsItemPrice != null) {
                    // read itemViewModel.productsItem.price.concat(" ")
                    itemViewModelProductsItemPriceConcatJavaLangString = itemViewModelProductsItemPrice.concat(" ");
                }


                if (itemViewModelProductsItemPriceConcatJavaLangString != null) {
                    // read itemViewModel.productsItem.price.concat(" ").concat(itemViewModel.productsItem.currency)
                    itemViewModelProductsItemPriceConcatJavaLangStringConcatItemViewModelProductsItemCurrency = itemViewModelProductsItemPriceConcatJavaLangString.concat(itemViewModelProductsItemCurrency);
                }
        }
        // batch finished
        if ((dirtyFlags & 0x7L) != 0) {
            // api target 1

            grand.app.aber_user.base.ApplicationBinding.loadImage(this.icPartsImage, itemViewModelProductsItemImage);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvPartsName, itemViewModelProductsItemName);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvPartsPrice, itemViewModelProductsItemPriceConcatJavaLangStringConcatItemViewModelProductsItemCurrency);
            androidx.databinding.adapters.ImageViewBindingAdapter.setImageDrawable(this.ucFavorite, itemViewModelProductsItemIsFavoriteUcFavoriteAndroidDrawableIcLikeFillUcFavoriteAndroidDrawableIcLikeEmpty);
            grand.app.aber_user.base.ApplicationBinding.setRate(this.userItemCode, itemViewModelProductsItemRate);
        }
        if ((dirtyFlags & 0x4L) != 0) {
            // api target 1

            this.mboundView0.setOnClickListener(mCallback37);
            this.mboundView6.setOnClickListener(mCallback39);
            this.ucFavorite.setOnClickListener(mCallback38);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        switch(sourceId) {
            case 3: {
                // localize variables for thread safety
                // itemViewModel
                grand.app.aber_user.pages.parts.viewModels.ItemProductsViewModel itemViewModel = mItemViewModel;
                // itemViewModel != null
                boolean itemViewModelJavaLangObjectNull = false;



                itemViewModelJavaLangObjectNull = (itemViewModel) != (null);
                if (itemViewModelJavaLangObjectNull) {




                    itemViewModel.itemAction(grand.app.aber_user.utils.Constants.CART);
                }
                break;
            }
            case 1: {
                // localize variables for thread safety
                // itemViewModel
                grand.app.aber_user.pages.parts.viewModels.ItemProductsViewModel itemViewModel = mItemViewModel;
                // itemViewModel != null
                boolean itemViewModelJavaLangObjectNull = false;



                itemViewModelJavaLangObjectNull = (itemViewModel) != (null);
                if (itemViewModelJavaLangObjectNull) {




                    itemViewModel.itemAction(grand.app.aber_user.utils.Constants.SERVICE_DETAILS);
                }
                break;
            }
            case 2: {
                // localize variables for thread safety
                // itemViewModel
                grand.app.aber_user.pages.parts.viewModels.ItemProductsViewModel itemViewModel = mItemViewModel;
                // itemViewModel != null
                boolean itemViewModelJavaLangObjectNull = false;



                itemViewModelJavaLangObjectNull = (itemViewModel) != (null);
                if (itemViewModelJavaLangObjectNull) {




                    itemViewModel.itemAction(grand.app.aber_user.utils.Constants.LIKES_REACTION);
                }
                break;
            }
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): itemViewModel
        flag 1 (0x2L): itemViewModel.productsItem
        flag 2 (0x3L): null
        flag 3 (0x4L): itemViewModel.productsItem.isFavorite ? @android:drawable/ic_like_fill : @android:drawable/ic_like_empty
        flag 4 (0x5L): itemViewModel.productsItem.isFavorite ? @android:drawable/ic_like_fill : @android:drawable/ic_like_empty
    flag mapping end*/
    //end
}