package grand.app.aber_user.databinding;
import grand.app.aber_user.R;
import grand.app.aber_user.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentProductDetailsBindingImpl extends FragmentProductDetailsBinding implements grand.app.aber_user.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.main_scroll, 20);
        sViewsWithIds.put(R.id.imageSlider, 21);
        sViewsWithIds.put(R.id.v1, 22);
        sViewsWithIds.put(R.id.v2, 23);
        sViewsWithIds.put(R.id.tv_saler_desc, 24);
        sViewsWithIds.put(R.id.tv_seller_name, 25);
        sViewsWithIds.put(R.id.v_seller, 26);
        sViewsWithIds.put(R.id.tv_brand_name, 27);
        sViewsWithIds.put(R.id.v_brand, 28);
        sViewsWithIds.put(R.id.tv_weight, 29);
        sViewsWithIds.put(R.id.v_weight, 30);
        sViewsWithIds.put(R.id.tv_type, 31);
        sViewsWithIds.put(R.id.v_type, 32);
        sViewsWithIds.put(R.id.tv_serial, 33);
        sViewsWithIds.put(R.id.v_serial, 34);
        sViewsWithIds.put(R.id.tv_valid, 35);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback99;
    @Nullable
    private final android.view.View.OnClickListener mCallback95;
    @Nullable
    private final android.view.View.OnClickListener mCallback96;
    @Nullable
    private final android.view.View.OnClickListener mCallback98;
    @Nullable
    private final android.view.View.OnClickListener mCallback97;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentProductDetailsBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 36, sIncludes, sViewsWithIds));
    }
    private FragmentProductDetailsBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 1
            , (androidx.appcompat.widget.AppCompatImageView) bindings[1]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[2]
            , (com.smarteist.autoimageslider.SliderView) bindings[21]
            , (androidx.core.widget.NestedScrollView) bindings[20]
            , (androidx.appcompat.widget.AppCompatRatingBar) bindings[4]
            , (androidx.recyclerview.widget.RecyclerView) bindings[8]
            , (androidx.recyclerview.widget.RecyclerView) bindings[11]
            , (androidx.appcompat.widget.AppCompatButton) bindings[19]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[27]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[14]
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[7]
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[3]
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[6]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[5]
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[24]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[25]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[13]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[33]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[17]
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[10]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[31]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[16]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[35]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[18]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[29]
            , (grand.app.aber_user.customViews.views.CustomTextViewRegular) bindings[15]
            , (android.view.View) bindings[22]
            , (android.view.View) bindings[23]
            , (android.view.View) bindings[9]
            , (android.view.View) bindings[12]
            , (android.view.View) bindings[28]
            , (android.view.View) bindings[26]
            , (android.view.View) bindings[34]
            , (android.view.View) bindings[32]
            , (android.view.View) bindings[30]
            );
        this.icFavorite.setTag(null);
        this.icShare.setTag(null);
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.rate.setTag(null);
        this.rcColors.setTag(null);
        this.rcSizes.setTag(null);
        this.sendContainer.setTag(null);
        this.tvBrandNameValue.setTag(null);
        this.tvColors.setTag(null);
        this.tvPartDesc.setTag(null);
        this.tvPartsPrice.setTag(null);
        this.tvRate.setTag(null);
        this.tvSellerNameValue.setTag(null);
        this.tvSerialValue.setTag(null);
        this.tvSizes.setTag(null);
        this.tvTypeValue.setTag(null);
        this.tvValidValue.setTag(null);
        this.tvWeightValue.setTag(null);
        this.v3.setTag(null);
        this.v4.setTag(null);
        setRootTag(root);
        // listeners
        mCallback99 = new grand.app.aber_user.generated.callback.OnClickListener(this, 5);
        mCallback95 = new grand.app.aber_user.generated.callback.OnClickListener(this, 1);
        mCallback96 = new grand.app.aber_user.generated.callback.OnClickListener(this, 2);
        mCallback98 = new grand.app.aber_user.generated.callback.OnClickListener(this, 4);
        mCallback97 = new grand.app.aber_user.generated.callback.OnClickListener(this, 3);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x10L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.viewModel == variableId) {
            setViewModel((grand.app.aber_user.pages.parts.viewModels.ProductDetailsViewModels) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setViewModel(@Nullable grand.app.aber_user.pages.parts.viewModels.ProductDetailsViewModels ViewModel) {
        updateRegistration(0, ViewModel);
        this.mViewModel = ViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.viewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeViewModel((grand.app.aber_user.pages.parts.viewModels.ProductDetailsViewModels) object, fieldId);
        }
        return false;
    }
    private boolean onChangeViewModel(grand.app.aber_user.pages.parts.viewModels.ProductDetailsViewModels ViewModel, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        else if (fieldId == BR.productDetails) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        else if (fieldId == BR.productColorsAdapter) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        else if (fieldId == BR.colorSizesAdapter) {
            synchronized(this) {
                    mDirtyFlags |= 0x8L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String viewModelProductDetailsValidityPeriod = null;
        java.lang.String viewModelProductDetailsBrand = null;
        android.graphics.drawable.Drawable viewModelProductDetailsIsFavoriteIcFavoriteAndroidDrawableIcLikeFillIcFavoriteAndroidDrawableIcLikeEmpty = null;
        java.lang.String viewModelProductDetailsRate = null;
        int viewModelProductDetailsAttributesSize = 0;
        grand.app.aber_user.pages.parts.models.productDetails.ProductDetails viewModelProductDetails = null;
        java.lang.String viewModelProductDetailsPriceConcatJavaLangString = null;
        grand.app.aber_user.pages.parts.adapters.ProductColorSizesAdapter viewModelColorSizesAdapter = null;
        boolean viewModelProductDetailsNameJavaLangObjectNull = false;
        java.lang.String viewModelProductDetailsSerial = null;
        int viewModelProductDetailsStockQuantity = 0;
        java.lang.String viewModelProductDetailsCurrency = null;
        grand.app.aber_user.pages.auth.models.UserData viewModelProductDetailsCompany = null;
        java.lang.String viewModelProductDetailsName = null;
        java.lang.String viewModelProductDetailsPriceConcatJavaLangStringConcatViewModelProductDetailsCurrency = null;
        grand.app.aber_user.pages.parts.adapters.ProductColorsAdapter viewModelProductColorsAdapter = null;
        java.lang.String viewModelProductDetailsDescription = null;
        java.util.List<grand.app.aber_user.pages.parts.models.productDetails.AttributesItem> viewModelProductDetailsAttributes = null;
        boolean viewModelProductDetailsStockQuantityInt0 = false;
        boolean viewModelProductDetailsIsFavorite = false;
        java.lang.String viewModelProductDetailsWeight = null;
        java.lang.String viewModelProductDetailsCompanyName = null;
        java.lang.String viewModelProductDetailsType = null;
        int viewModelProductDetailsAttributesSizeInt0ViewVISIBLEViewGONE = 0;
        boolean viewModelProductDetailsStockQuantityInt0BooleanFalseBooleanTrue = false;
        int viewModelProductDetailsNameJavaLangObjectNullViewVISIBLEViewGONE = 0;
        boolean viewModelProductDetailsAttributesSizeInt0 = false;
        grand.app.aber_user.pages.parts.viewModels.ProductDetailsViewModels viewModel = mViewModel;
        java.lang.String viewModelProductDetailsPrice = null;

        if ((dirtyFlags & 0x1fL) != 0) {


            if ((dirtyFlags & 0x13L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.productDetails
                        viewModelProductDetails = viewModel.getProductDetails();
                    }


                    if (viewModelProductDetails != null) {
                        // read viewModel.productDetails.validityPeriod
                        viewModelProductDetailsValidityPeriod = viewModelProductDetails.getValidityPeriod();
                        // read viewModel.productDetails.brand
                        viewModelProductDetailsBrand = viewModelProductDetails.getBrand();
                        // read viewModel.productDetails.rate
                        viewModelProductDetailsRate = viewModelProductDetails.getRate();
                        // read viewModel.productDetails.serial
                        viewModelProductDetailsSerial = viewModelProductDetails.getSerial();
                        // read viewModel.productDetails.stockQuantity
                        viewModelProductDetailsStockQuantity = viewModelProductDetails.getStockQuantity();
                        // read viewModel.productDetails.currency
                        viewModelProductDetailsCurrency = viewModelProductDetails.getCurrency();
                        // read viewModel.productDetails.company
                        viewModelProductDetailsCompany = viewModelProductDetails.getCompany();
                        // read viewModel.productDetails.name
                        viewModelProductDetailsName = viewModelProductDetails.getName();
                        // read viewModel.productDetails.description
                        viewModelProductDetailsDescription = viewModelProductDetails.getDescription();
                        // read viewModel.productDetails.attributes
                        viewModelProductDetailsAttributes = viewModelProductDetails.getAttributes();
                        // read viewModel.productDetails.isFavorite
                        viewModelProductDetailsIsFavorite = viewModelProductDetails.isIsFavorite();
                        // read viewModel.productDetails.weight
                        viewModelProductDetailsWeight = viewModelProductDetails.getWeight();
                        // read viewModel.productDetails.type
                        viewModelProductDetailsType = viewModelProductDetails.getType();
                        // read viewModel.productDetails.price
                        viewModelProductDetailsPrice = viewModelProductDetails.getPrice();
                    }
                if((dirtyFlags & 0x13L) != 0) {
                    if(viewModelProductDetailsIsFavorite) {
                            dirtyFlags |= 0x40L;
                    }
                    else {
                            dirtyFlags |= 0x20L;
                    }
                }


                    // read viewModel.productDetails.stockQuantity == 0
                    viewModelProductDetailsStockQuantityInt0 = (viewModelProductDetailsStockQuantity) == (0);
                    // read viewModel.productDetails.name != null
                    viewModelProductDetailsNameJavaLangObjectNull = (viewModelProductDetailsName) != (null);
                    // read viewModel.productDetails.isFavorite ? @android:drawable/ic_like_fill : @android:drawable/ic_like_empty
                    viewModelProductDetailsIsFavoriteIcFavoriteAndroidDrawableIcLikeFillIcFavoriteAndroidDrawableIcLikeEmpty = ((viewModelProductDetailsIsFavorite) ? (androidx.appcompat.content.res.AppCompatResources.getDrawable(icFavorite.getContext(), R.drawable.ic_like_fill)) : (androidx.appcompat.content.res.AppCompatResources.getDrawable(icFavorite.getContext(), R.drawable.ic_like_empty)));
                if((dirtyFlags & 0x13L) != 0) {
                    if(viewModelProductDetailsStockQuantityInt0) {
                            dirtyFlags |= 0x400L;
                    }
                    else {
                            dirtyFlags |= 0x200L;
                    }
                }
                if((dirtyFlags & 0x13L) != 0) {
                    if(viewModelProductDetailsNameJavaLangObjectNull) {
                            dirtyFlags |= 0x1000L;
                    }
                    else {
                            dirtyFlags |= 0x800L;
                    }
                }
                    if (viewModelProductDetailsCompany != null) {
                        // read viewModel.productDetails.company.name
                        viewModelProductDetailsCompanyName = viewModelProductDetailsCompany.getName();
                    }
                    if (viewModelProductDetailsAttributes != null) {
                        // read viewModel.productDetails.attributes.size()
                        viewModelProductDetailsAttributesSize = viewModelProductDetailsAttributes.size();
                    }
                    if (viewModelProductDetailsPrice != null) {
                        // read viewModel.productDetails.price.concat(" ")
                        viewModelProductDetailsPriceConcatJavaLangString = viewModelProductDetailsPrice.concat(" ");
                    }


                    // read viewModel.productDetails.stockQuantity == 0 ? false : true
                    viewModelProductDetailsStockQuantityInt0BooleanFalseBooleanTrue = ((viewModelProductDetailsStockQuantityInt0) ? (false) : (true));
                    // read viewModel.productDetails.name != null ? View.VISIBLE : View.GONE
                    viewModelProductDetailsNameJavaLangObjectNullViewVISIBLEViewGONE = ((viewModelProductDetailsNameJavaLangObjectNull) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
                    // read viewModel.productDetails.attributes.size() > 0
                    viewModelProductDetailsAttributesSizeInt0 = (viewModelProductDetailsAttributesSize) > (0);
                if((dirtyFlags & 0x13L) != 0) {
                    if(viewModelProductDetailsAttributesSizeInt0) {
                            dirtyFlags |= 0x100L;
                    }
                    else {
                            dirtyFlags |= 0x80L;
                    }
                }
                    if (viewModelProductDetailsPriceConcatJavaLangString != null) {
                        // read viewModel.productDetails.price.concat(" ").concat(viewModel.productDetails.currency)
                        viewModelProductDetailsPriceConcatJavaLangStringConcatViewModelProductDetailsCurrency = viewModelProductDetailsPriceConcatJavaLangString.concat(viewModelProductDetailsCurrency);
                    }


                    // read viewModel.productDetails.attributes.size() > 0 ? View.VISIBLE : View.GONE
                    viewModelProductDetailsAttributesSizeInt0ViewVISIBLEViewGONE = ((viewModelProductDetailsAttributesSizeInt0) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
            if ((dirtyFlags & 0x19L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.colorSizesAdapter
                        viewModelColorSizesAdapter = viewModel.getColorSizesAdapter();
                    }
            }
            if ((dirtyFlags & 0x15L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.productColorsAdapter
                        viewModelProductColorsAdapter = viewModel.getProductColorsAdapter();
                    }
            }
        }
        // batch finished
        if ((dirtyFlags & 0x10L) != 0) {
            // api target 1

            this.icFavorite.setOnClickListener(mCallback95);
            this.icShare.setOnClickListener(mCallback96);
            this.sendContainer.setOnClickListener(mCallback99);
            this.tvRate.setOnClickListener(mCallback97);
            this.tvSellerNameValue.setOnClickListener(mCallback98);
        }
        if ((dirtyFlags & 0x13L) != 0) {
            // api target 1

            androidx.databinding.adapters.ImageViewBindingAdapter.setImageDrawable(this.icFavorite, viewModelProductDetailsIsFavoriteIcFavoriteAndroidDrawableIcLikeFillIcFavoriteAndroidDrawableIcLikeEmpty);
            this.mboundView0.setVisibility(viewModelProductDetailsNameJavaLangObjectNullViewVISIBLEViewGONE);
            grand.app.aber_user.base.ApplicationBinding.setRate(this.rate, viewModelProductDetailsRate);
            this.sendContainer.setEnabled(viewModelProductDetailsStockQuantityInt0BooleanFalseBooleanTrue);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvBrandNameValue, viewModelProductDetailsBrand);
            this.tvColors.setVisibility(viewModelProductDetailsAttributesSizeInt0ViewVISIBLEViewGONE);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvPartDesc, viewModelProductDetailsDescription);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvPartsPrice, viewModelProductDetailsPriceConcatJavaLangStringConcatViewModelProductDetailsCurrency);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvSellerNameValue, viewModelProductDetailsCompanyName);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvSerialValue, viewModelProductDetailsSerial);
            this.tvSizes.setVisibility(viewModelProductDetailsAttributesSizeInt0ViewVISIBLEViewGONE);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvTypeValue, viewModelProductDetailsType);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvValidValue, viewModelProductDetailsValidityPeriod);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvWeightValue, viewModelProductDetailsWeight);
            this.v3.setVisibility(viewModelProductDetailsAttributesSizeInt0ViewVISIBLEViewGONE);
            this.v4.setVisibility(viewModelProductDetailsAttributesSizeInt0ViewVISIBLEViewGONE);
        }
        if ((dirtyFlags & 0x15L) != 0) {
            // api target 1

            grand.app.aber_user.base.ApplicationBinding.getItemsV2Binding(this.rcColors, viewModelProductColorsAdapter, "1", "2");
        }
        if ((dirtyFlags & 0x19L) != 0) {
            // api target 1

            grand.app.aber_user.base.ApplicationBinding.getItemsV2Binding(this.rcSizes, viewModelColorSizesAdapter, "1", "2");
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        switch(sourceId) {
            case 5: {
                // localize variables for thread safety
                // viewModel
                grand.app.aber_user.pages.parts.viewModels.ProductDetailsViewModels viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {


                    viewModel.addToCart();
                }
                break;
            }
            case 1: {
                // localize variables for thread safety
                // viewModel
                grand.app.aber_user.pages.parts.viewModels.ProductDetailsViewModels viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {


                    viewModel.changeLike();
                }
                break;
            }
            case 2: {
                // localize variables for thread safety
                // viewModel
                grand.app.aber_user.pages.parts.viewModels.ProductDetailsViewModels viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {




                    viewModel.liveDataActions(grand.app.aber_user.utils.Constants.SHARE_BAR);
                }
                break;
            }
            case 4: {
                // localize variables for thread safety
                // viewModel
                grand.app.aber_user.pages.parts.viewModels.ProductDetailsViewModels viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {




                    viewModel.liveDataActions(grand.app.aber_user.utils.Constants.PRODUCTS);
                }
                break;
            }
            case 3: {
                // localize variables for thread safety
                // viewModel
                grand.app.aber_user.pages.parts.viewModels.ProductDetailsViewModels viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {




                    viewModel.liveDataActions(grand.app.aber_user.utils.Constants.RATE_APP);
                }
                break;
            }
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): viewModel
        flag 1 (0x2L): viewModel.productDetails
        flag 2 (0x3L): viewModel.productColorsAdapter
        flag 3 (0x4L): viewModel.colorSizesAdapter
        flag 4 (0x5L): null
        flag 5 (0x6L): viewModel.productDetails.isFavorite ? @android:drawable/ic_like_fill : @android:drawable/ic_like_empty
        flag 6 (0x7L): viewModel.productDetails.isFavorite ? @android:drawable/ic_like_fill : @android:drawable/ic_like_empty
        flag 7 (0x8L): viewModel.productDetails.attributes.size() > 0 ? View.VISIBLE : View.GONE
        flag 8 (0x9L): viewModel.productDetails.attributes.size() > 0 ? View.VISIBLE : View.GONE
        flag 9 (0xaL): viewModel.productDetails.stockQuantity == 0 ? false : true
        flag 10 (0xbL): viewModel.productDetails.stockQuantity == 0 ? false : true
        flag 11 (0xcL): viewModel.productDetails.name != null ? View.VISIBLE : View.GONE
        flag 12 (0xdL): viewModel.productDetails.name != null ? View.VISIBLE : View.GONE
    flag mapping end*/
    //end
}