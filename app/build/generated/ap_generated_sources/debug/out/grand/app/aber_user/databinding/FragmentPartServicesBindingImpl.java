package grand.app.aber_user.databinding;
import grand.app.aber_user.R;
import grand.app.aber_user.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentPartServicesBindingImpl extends FragmentPartServicesBinding implements grand.app.aber_user.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.br, 6);
        sViewsWithIds.put(R.id.tab_card, 7);
        sViewsWithIds.put(R.id.tabs, 8);
    }
    // views
    @NonNull
    private final androidx.coordinatorlayout.widget.CoordinatorLayout mboundView0;
    @NonNull
    private final com.google.android.material.floatingactionbutton.FloatingActionButton mboundView5;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback223;
    @Nullable
    private final android.view.View.OnClickListener mCallback224;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentPartServicesBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 9, sIncludes, sViewsWithIds));
    }
    private FragmentPartServicesBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 2
            , (androidx.constraintlayout.widget.Barrier) bindings[6]
            , (grand.app.aber_user.customViews.views.CustomTextViewMedium) bindings[1]
            , (com.google.android.material.progressindicator.CircularProgressIndicator) bindings[4]
            , (androidx.recyclerview.widget.RecyclerView) bindings[3]
            , (com.google.android.material.button.MaterialButton) bindings[2]
            , (androidx.cardview.widget.CardView) bindings[7]
            , (com.google.android.material.tabs.TabLayout) bindings[8]
            );
        this.mboundView0 = (androidx.coordinatorlayout.widget.CoordinatorLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView5 = (com.google.android.material.floatingactionbutton.FloatingActionButton) bindings[5];
        this.mboundView5.setTag(null);
        this.productsCount.setTag(null);
        this.progress.setTag(null);
        this.rcProducts.setTag(null);
        this.sortBtn.setTag(null);
        setRootTag(root);
        // listeners
        mCallback223 = new grand.app.aber_user.generated.callback.OnClickListener(this, 1);
        mCallback224 = new grand.app.aber_user.generated.callback.OnClickListener(this, 2);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x20L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.viewModel == variableId) {
            setViewModel((grand.app.aber_user.pages.parts.viewModels.PartServicesViewModels) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setViewModel(@Nullable grand.app.aber_user.pages.parts.viewModels.PartServicesViewModels ViewModel) {
        updateRegistration(1, ViewModel);
        this.mViewModel = ViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.viewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeViewModelSearchProgressVisible((androidx.databinding.ObservableBoolean) object, fieldId);
            case 1 :
                return onChangeViewModel((grand.app.aber_user.pages.parts.viewModels.PartServicesViewModels) object, fieldId);
        }
        return false;
    }
    private boolean onChangeViewModelSearchProgressVisible(androidx.databinding.ObservableBoolean ViewModelSearchProgressVisible, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModel(grand.app.aber_user.pages.parts.viewModels.PartServicesViewModels ViewModel, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        else if (fieldId == BR.mainData) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        else if (fieldId == BR.productsAdapter) {
            synchronized(this) {
                    mDirtyFlags |= 0x8L;
            }
            return true;
        }
        else if (fieldId == BR.passingObject) {
            synchronized(this) {
                    mDirtyFlags |= 0x10L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        boolean viewModelSearchProgressVisibleGet = false;
        int viewModelSearchProgressVisibleViewVISIBLEViewGONE = 0;
        boolean viewModelMainDataProductsCountJavaLangObjectNull = false;
        java.lang.String viewModelMainDataProductsCountJavaLangObjectNullViewModelMainDataProductsCountJavaLangString = null;
        int viewModelPassingObjectObjectEqualsConstantsCOMPANYViewGONEViewVISIBLE = 0;
        grand.app.aber_user.PassingObject viewModelPassingObject = null;
        java.lang.String viewModelPassingObjectObject = null;
        grand.app.aber_user.pages.parts.adapters.PartProductsAdapter viewModelProductsAdapter = null;
        androidx.databinding.ObservableBoolean viewModelSearchProgressVisible = null;
        boolean viewModelPassingObjectObjectEqualsConstantsCOMPANY = false;
        java.lang.String viewModelMainDataProductsCount = null;
        java.lang.String productsCountAndroidStringNumberProductsConcatJavaLangStringConcatViewModelMainDataProductsCountJavaLangObjectNullViewModelMainDataProductsCountJavaLangString = null;
        grand.app.aber_user.pages.parts.models.ProductMain viewModelMainData = null;
        grand.app.aber_user.pages.parts.viewModels.PartServicesViewModels viewModel = mViewModel;

        if ((dirtyFlags & 0x3fL) != 0) {


            if ((dirtyFlags & 0x32L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.passingObject
                        viewModelPassingObject = viewModel.getPassingObject();
                    }


                    if (viewModelPassingObject != null) {
                        // read viewModel.passingObject.object
                        viewModelPassingObjectObject = viewModelPassingObject.getObject();
                    }


                    if (viewModelPassingObjectObject != null) {
                        // read viewModel.passingObject.object.equals(Constants.COMPANY)
                        viewModelPassingObjectObjectEqualsConstantsCOMPANY = viewModelPassingObjectObject.equals(grand.app.aber_user.utils.Constants.COMPANY);
                    }
                if((dirtyFlags & 0x32L) != 0) {
                    if(viewModelPassingObjectObjectEqualsConstantsCOMPANY) {
                            dirtyFlags |= 0x800L;
                    }
                    else {
                            dirtyFlags |= 0x400L;
                    }
                }


                    // read viewModel.passingObject.object.equals(Constants.COMPANY) ? View.GONE : View.VISIBLE
                    viewModelPassingObjectObjectEqualsConstantsCOMPANYViewGONEViewVISIBLE = ((viewModelPassingObjectObjectEqualsConstantsCOMPANY) ? (android.view.View.GONE) : (android.view.View.VISIBLE));
            }
            if ((dirtyFlags & 0x2aL) != 0) {

                    if (viewModel != null) {
                        // read viewModel.productsAdapter
                        viewModelProductsAdapter = viewModel.getProductsAdapter();
                    }
            }
            if ((dirtyFlags & 0x23L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.searchProgressVisible
                        viewModelSearchProgressVisible = viewModel.searchProgressVisible;
                    }
                    updateRegistration(0, viewModelSearchProgressVisible);


                    if (viewModelSearchProgressVisible != null) {
                        // read viewModel.searchProgressVisible.get()
                        viewModelSearchProgressVisibleGet = viewModelSearchProgressVisible.get();
                    }
                if((dirtyFlags & 0x23L) != 0) {
                    if(viewModelSearchProgressVisibleGet) {
                            dirtyFlags |= 0x80L;
                    }
                    else {
                            dirtyFlags |= 0x40L;
                    }
                }


                    // read viewModel.searchProgressVisible.get() ? View.VISIBLE : View.GONE
                    viewModelSearchProgressVisibleViewVISIBLEViewGONE = ((viewModelSearchProgressVisibleGet) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
            if ((dirtyFlags & 0x26L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.mainData
                        viewModelMainData = viewModel.getMainData();
                    }


                    if (viewModelMainData != null) {
                        // read viewModel.mainData.productsCount
                        viewModelMainDataProductsCount = viewModelMainData.getProductsCount();
                    }


                    // read viewModel.mainData.productsCount != null
                    viewModelMainDataProductsCountJavaLangObjectNull = (viewModelMainDataProductsCount) != (null);
                if((dirtyFlags & 0x26L) != 0) {
                    if(viewModelMainDataProductsCountJavaLangObjectNull) {
                            dirtyFlags |= 0x200L;
                    }
                    else {
                            dirtyFlags |= 0x100L;
                    }
                }
            }
        }
        // batch finished

        if ((dirtyFlags & 0x26L) != 0) {

                // read viewModel.mainData.productsCount != null ? viewModel.mainData.productsCount : " "
                viewModelMainDataProductsCountJavaLangObjectNullViewModelMainDataProductsCountJavaLangString = ((viewModelMainDataProductsCountJavaLangObjectNull) ? (viewModelMainDataProductsCount) : (" "));


                // read @android:string/number_products.concat(" ").concat(viewModel.mainData.productsCount != null ? viewModel.mainData.productsCount : " ")
                productsCountAndroidStringNumberProductsConcatJavaLangStringConcatViewModelMainDataProductsCountJavaLangObjectNullViewModelMainDataProductsCountJavaLangString = productsCount.getResources().getString(R.string.number_products).concat(" ").concat(viewModelMainDataProductsCountJavaLangObjectNullViewModelMainDataProductsCountJavaLangString);
        }
        // batch finished
        if ((dirtyFlags & 0x20L) != 0) {
            // api target 1

            this.mboundView5.setOnClickListener(mCallback224);
            this.sortBtn.setOnClickListener(mCallback223);
        }
        if ((dirtyFlags & 0x32L) != 0) {
            // api target 1

            this.mboundView5.setVisibility(viewModelPassingObjectObjectEqualsConstantsCOMPANYViewGONEViewVISIBLE);
        }
        if ((dirtyFlags & 0x26L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.productsCount, productsCountAndroidStringNumberProductsConcatJavaLangStringConcatViewModelMainDataProductsCountJavaLangObjectNullViewModelMainDataProductsCountJavaLangString);
        }
        if ((dirtyFlags & 0x23L) != 0) {
            // api target 1

            this.progress.setVisibility(viewModelSearchProgressVisibleViewVISIBLEViewGONE);
        }
        if ((dirtyFlags & 0x2aL) != 0) {
            // api target 1

            grand.app.aber_user.base.ApplicationBinding.getItemsV2Binding(this.rcProducts, viewModelProductsAdapter, "2", "1");
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        switch(sourceId) {
            case 1: {
                // localize variables for thread safety
                // viewModel
                grand.app.aber_user.pages.parts.viewModels.PartServicesViewModels viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {




                    viewModel.liveDataActions(grand.app.aber_user.utils.Constants.DIALOG_SHOW);
                }
                break;
            }
            case 2: {
                // localize variables for thread safety
                // viewModel
                grand.app.aber_user.pages.parts.viewModels.PartServicesViewModels viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {




                    viewModel.liveDataActions(grand.app.aber_user.utils.Constants.FILTER);
                }
                break;
            }
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): viewModel.searchProgressVisible
        flag 1 (0x2L): viewModel
        flag 2 (0x3L): viewModel.mainData
        flag 3 (0x4L): viewModel.productsAdapter
        flag 4 (0x5L): viewModel.passingObject
        flag 5 (0x6L): null
        flag 6 (0x7L): viewModel.searchProgressVisible.get() ? View.VISIBLE : View.GONE
        flag 7 (0x8L): viewModel.searchProgressVisible.get() ? View.VISIBLE : View.GONE
        flag 8 (0x9L): viewModel.mainData.productsCount != null ? viewModel.mainData.productsCount : " "
        flag 9 (0xaL): viewModel.mainData.productsCount != null ? viewModel.mainData.productsCount : " "
        flag 10 (0xbL): viewModel.passingObject.object.equals(Constants.COMPANY) ? View.GONE : View.VISIBLE
        flag 11 (0xcL): viewModel.passingObject.object.equals(Constants.COMPANY) ? View.GONE : View.VISIBLE
    flag mapping end*/
    //end
}